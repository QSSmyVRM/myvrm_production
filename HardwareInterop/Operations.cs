//ZD 100147 Start
/* Copyright (C) 2015 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
//ZD 100147 ZD 100886 End
/// <remarks>
/// FILE : Operations.cs
/// DESCRIPTION : This is the controller. All operations are directed from here. 
/// AUTHOR : Kapil M
/// </remarks>

#region References 
using System;
using System.Xml;
using System.Collections;
using System.IO;
using System.Net;
using System.Text;
using System.Security.Cryptography.X509Certificates;
using System.Collections.Generic;
using cryptography;
using System.Data; //ZD 100152
#endregion 

namespace NS_OPERATIONS
{
	/// <summary>
	/// All event-triggered operations are stored in this class. 
	/// Each of the methods perform a specific task.
	/// This class is usually called by the ASP/ASPX pages directly.
	/// </summary>
	public class Operations //ZD 104116
	{
		/// <summary>
		/// Public class attributes
		/// </summary>
		internal string errMsg = null;
        internal enum eLayoutType { CONF_AllPARTY_LAYOUT = 1, PARTY_LAYOUT }; //FB 2530 //FB 3073
        internal enum eMuteType {  PARTY_MUTE = 1, PARTYALL_MUTE }; //FB 2530
        internal enum eConnectType { PARTY_CONNECTDISCONNECT = 0, PARTYALL_CONNECTDISCONNECT = 1 }; //FB 2501 Call Monitoring
		/// <summary>
		/// private class attributes
		/// </summary>
		private NS_LOGGER.Log logger;
        NS_POLYCOM.Polycom polycom;
		private NS_MESSENGER.Party masterParty = new NS_MESSENGER.Party();
		private NS_MESSENGER.ConfigParams configParams;
        internal bool iRecurring = false; //FB 2441
        NS_MESSENGER.Alert alert = new NS_MESSENGER.Alert();//FB 2569
        internal int eptProfileID = -1; //ALLDEV-814

        //FB 2441 Starts
        public enum AudioVideoParty
        {
            Audio=0,Video=1
        }
        //FB 2441 Ends

        #region Operations
        /// <summary>
		/// Constructor
		/// </summary>
		/// <param name="config"></param>
		public Operations(NS_MESSENGER.ConfigParams config)
		{
			configParams = config;
			logger = new NS_LOGGER.Log(configParams);
			polycom = new NS_POLYCOM.Polycom(configParams);
        }
        #endregion

        #region TestMCUConnection
        /// <summary>
		/// Test the bridge connectivity
		/// </summary>
		internal bool TestMCUConnection(string inXml)
        {
            try
            {
                cryptography.Crypto crypto = null; //FB 3054
                string encryptpass = "";//FB 3054
                logger.Trace("Entering the Test MCU Connection method...");

                // Load the string into xml parser and retrieve the tag values.
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(inXml);

                // Instantiate the MCU object.
                NS_MESSENGER.MCU mcu = new NS_MESSENGER.MCU();

                // MCU Db ID
                // TODO: Add support for "new"
                mcu.iDbId = Int32.Parse(xd.SelectSingleNode("//setBridge/bridge/bridgeID").InnerXml.Trim());

                // MCU name
                mcu.sName = xd.SelectSingleNode("//setBridge/bridge/name").InnerXml.Trim();

                // MCU login
                mcu.sLogin = xd.SelectSingleNode("//setBridge/bridge/login").InnerXml.Trim();

                // MCU password
                crypto = new Crypto(); //FB 3054
                encryptpass = xd.SelectSingleNode("//setBridge/bridge/password").InnerXml.Trim();
                if(!string.IsNullOrEmpty(encryptpass))
                    encryptpass = crypto.decrypt(encryptpass);
                mcu.sPwd = encryptpass;

                // MCU firmware version
                mcu.sSoftwareVer = xd.SelectSingleNode("//setBridge/bridge/firmwareVersion").InnerXml.Trim();

                mcu.sStation = "MYVRM";

                // Code added for API Port No ... Start

                int mcuAPIPort = 80; //API Port...
                if (xd.SelectSingleNode("//setBridge/bridge/ApiPortNo") != null)
                    int.TryParse(xd.SelectSingleNode("//setBridge/bridge/ApiPortNo").InnerText.Trim(), out mcuAPIPort);

                if (mcuAPIPort <= 0)
                    mcuAPIPort = 80;

                mcu.iHttpPort = mcuAPIPort;
                // Code added for API Port No ... End

                //ZD 104086 START
                int URLAccess = 0;//URL Access http or https
                if(xd.SelectSingleNode("//setBridge/bridge/URLAccess") != null)
                    int.TryParse(xd.SelectSingleNode("//setBridge/bridge/URLAccess").InnerText.Trim(), out URLAccess);

                mcu.iURLAccess = URLAccess;
                //ZD 104086 END
                

                // MCU type
                int bridgeType = Int32.Parse(xd.SelectSingleNode("//setBridge/bridge/bridgeType").InnerXml.Trim());
                bool ret = false;
                switch (bridgeType)
                {
                    case 0:
                    case 1:
                    case 2:
                    case 3:
                    {
                        // Its a Polycom MCU.
                        mcu.sIp = xd.SelectSingleNode("//setBridge/bridge/bridgeDetails/controlPortIPAddress").InnerXml.Trim();
                        mcu.dblSoftwareVer = Double.Parse(mcu.sSoftwareVer);
                        if (mcu.dblSoftwareVer < 7.0)
                        {
                            // Its the older Polycom MCU with firmware version less than 7.0.
                            mcu.etType = NS_MESSENGER.MCU.eType.ACCORDv6;
                        }
                        else
                        {
                            // Its the latest Polycom MCU with firmware version greater than 7.0.
                            mcu.etType = NS_MESSENGER.MCU.eType.ACCORDv7;
                        }
                        
                        // Polycom MCU
                        NS_POLYCOM.Polycom accord = new NS_POLYCOM.Polycom(configParams);
                        NS_MESSENGER.Party dummyParty = new NS_MESSENGER.Party();
                        ret = accord.TestMCUConnection(mcu);
                        if (!ret)
                        {
                            this.errMsg = accord.errMsg;
                        }
                        break;
                    }
                    case 4:
                    case 5: 
                    case 6:
                    {
                        // Its a Codian MCU
                        mcu.sIp = xd.SelectSingleNode("//setBridge/bridge/bridgeDetails/portA").InnerXml.Trim();
                        mcu.etType = NS_MESSENGER.MCU.eType.CODIAN;
                        //mcu.dblSoftwareVer = Double.Parse(mcu.sSoftwareVer);

                        // Check connection to bridge. 				
                        NS_CODIAN.Codian codian = new NS_CODIAN.Codian(configParams);
                        ret = codian.TestMCUConnection(mcu);
                        if (!ret)
                        {
                            this.errMsg = codian.errMsg;
                        }
                        break;
                     }
                    case 7:
                     {
                            // Its a Tandberg MCU
                            mcu.sIp = xd.SelectSingleNode("//setBridge/bridge/bridgeDetails/controlPortIPAddress").InnerXml.Trim();
                            mcu.etType = NS_MESSENGER.MCU.eType.TANDBERG;
                            mcu.dblSoftwareVer = 0.0;

                            // Check connection to bridge. 				
                            NS_TANDBERG.Tandberg tandberg = new NS_TANDBERG.Tandberg(configParams);
                            ret = false;
                            ret = tandberg.TestMCUConnection(mcu);
                            if (!ret)
                            {
                                this.errMsg = tandberg.errMsg;
                            }
                            break;
                     }
                    //It is for polycom RMX ZD 103787
                    case 17:
                    case 18:
                    case 19:
                    case 8:
                        {
                            // Its a Polycom RMX.
                            mcu.sIp = xd.SelectSingleNode("//setBridge/bridge/bridgeDetails/controlPortIPAddress").InnerXml.Trim();
                            mcu.etType = NS_MESSENGER.MCU.eType.RMX;

                            // Check connection to bridge. 				
                            NS_POLYCOM.Polycom rmx = new NS_POLYCOM.Polycom(configParams);
                            NS_MESSENGER.Party dummyParty = new NS_MESSENGER.Party();
                            ret = rmx.TestMCUConnection(mcu);
                            if (!ret)
                            {
                                this.errMsg = rmx.errMsg;
                            }
                            break;
                        }
                    case 9:
                        {
                            // Radvision Scopia.
                            mcu.sIp = xd.SelectSingleNode("//setBridge/bridge/bridgeDetails/controlPortIPAddress").InnerXml.Trim();
                            mcu.etType = NS_MESSENGER.MCU.eType.RADVISION;

                            // Check connection to bridge. 				
                            NS_RADVISION.Radvision rad = new NS_RADVISION.Radvision(configParams);
                            NS_MESSENGER.Party dummyParty = new NS_MESSENGER.Party();
                            ret = rad.TestMCUConnection(mcu);
                            if (!ret)
                            {
                                this.errMsg = rad.errMsg;
                            }
                            break;
                        }
                    /***FB polycom CMA FB 2441**/
                    case 10:
                        {
                            //// Its a polycom CMA MCU
                            //mcu.sIp = xd.SelectSingleNode("//setBridge/bridge/bridgeDetails/controlPortIPAddress").InnerXml.Trim();
                            //mcu.etType = NS_MESSENGER.MCU.eType.Lifesize;
                            //mcu.dblSoftwareVer = 0.0;

                            //// Check connection to bridge. 				
                            //NS_POLYCOMCMA.PolycomCMA polycomCMA = new NS_POLYCOMCMA.PolycomCMA(configParams);
                            //ret = false;
                            //ret = polycomCMA.TestMCUConnection(mcu);
                            //if (!ret)
                            //{
                            //    this.errMsg = tandberg.errMsg;
                            //}
                            break;
                        }
                    /***FB 2261**/
                    case 11:
                        {
                            // Its a lifesize MCU
                            mcu.sIp = xd.SelectSingleNode("//setBridge/bridge/bridgeDetails/controlPortIPAddress").InnerXml.Trim();
                            mcu.etType = NS_MESSENGER.MCU.eType.Lifesize;
                            mcu.dblSoftwareVer = 0.0;

                            // Check connection to bridge. 				
                            NS_LifeSize.LifeSize lifeSize = new NS_LifeSize.LifeSize(configParams);
                            ret = false;
                            ret = lifeSize.TestMCUConnection(mcu);
                            if (!ret)
                            {
                                this.errMsg = lifeSize.errMsg;
                            }
                            break;
                        }
                    //FB 2501 Call Monitoring Start
                    case 12:
                        {
                            mcu.sIp = xd.SelectSingleNode("//setBridge/bridge/bridgeDetails/portA").InnerXml.Trim();
                            mcu.etType = NS_MESSENGER.MCU.eType.CISCOTP;
                           
                            // Check connection to bridge. 				
                            NS_CiscoTPServer.CiscoTPServer CiscoTPServer = new NS_CiscoTPServer.CiscoTPServer(configParams);
                            ret = CiscoTPServer.TestMCUConnection(mcu);
                            if (!ret)
                            {
                                this.errMsg = CiscoTPServer.errMsg;
                            }
                            break;
                        }
                    //FB 2501 Call Monitoring End
                    //FB 2441 Starts
                    case 13:
                        {
                            int type = 0;
                            mcu.sIp = xd.SelectSingleNode("//setBridge/bridge/bridgeDetails/controlPortIPAddress").InnerXml.Trim();
                            int.TryParse(xd.SelectSingleNode("//setBridge/bridge/ApiPortNo").InnerXml.Trim(), out mcu.iHttpPort);
                            //FB 3054 Starts
                            //mcu.sPwd = xd.SelectSingleNode("//setBridge/bridge/password").InnerXml.Trim();
                            crypto = new Crypto();
                            encryptpass = xd.SelectSingleNode("//setBridge/bridge/password").InnerXml.Trim();
                            if(!string.IsNullOrEmpty(encryptpass))
                                encryptpass = crypto.decrypt(encryptpass);
                            mcu.sPwd = encryptpass;
                            //FB 3054 Ends
                            mcu.sLogin = xd.SelectSingleNode("//setBridge/bridge/login").InnerXml.Trim();
                            mcu.etType = NS_MESSENGER.MCU.eType.RPRM;
                            if (xd.SelectSingleNode("//setBridge/bridge/bridgeDetails/PolycomRPRM/DMATestConnection") != null)
                                int.TryParse(xd.SelectSingleNode("//setBridge/bridge/bridgeDetails/PolycomRPRM/DMATestConnection").InnerText, out type);
                            if (type == 1)
                            {
                                mcu.sDMAip = xd.SelectSingleNode("//setBridge/bridge/bridgeDetails/PolycomRPRM/DMAURL").InnerXml.Trim();
                                int.TryParse(xd.SelectSingleNode("//setBridge/bridge/bridgeDetails/PolycomRPRM/DMAPort").InnerXml.Trim(), out mcu.iDMAHttpport);
                                //FB 3054 Starts
                                //mcu.sDMAPassword = xd.SelectSingleNode("//setBridge/bridge/bridgeDetails/PolycomRPRM/DMAPassword").InnerXml.Trim();
                                crypto = new Crypto();
                                encryptpass = xd.SelectSingleNode("//setBridge/bridge/bridgeDetails/PolycomRPRM/DMAPassword").InnerXml.Trim();
                                if (!string.IsNullOrEmpty(encryptpass))
                                    encryptpass = crypto.decrypt(encryptpass);
                                mcu.sDMAPassword = encryptpass;
                                //FB 3054 Ends
                                
                                mcu.sDMALogin = xd.SelectSingleNode("//setBridge/bridge/bridgeDetails/PolycomRPRM/DMALogin").InnerXml.Trim();
                                
                            }

                            NS_POLYCOMRPRM.POLYCOMRPRM PolycomRPRM = new NS_POLYCOMRPRM.POLYCOMRPRM(configParams);
                            ret = PolycomRPRM.TestMCUConnection(mcu,type);
                            if (!ret)
                            {
                               this.errMsg = PolycomRPRM.errMsg;
                            }
                            break;
                        }
                    //FB 2441 Ends
                    /* FB 2556 Starts */
                    case 14:
                        {
                            //Its a RadVision IView MCU

                            mcu.sIp = xd.SelectSingleNode("//setBridge/bridge/bridgeDetails/controlPortIPAddress").InnerXml.Trim();
                            mcu.etType = NS_MESSENGER.MCU.eType.RADVISIONIVIEW;

                            // Check connection to bridge. 				
                            NS_RADVISION_IView.RadVisionIView RadIview = new NS_RADVISION_IView.RadVisionIView(configParams);
                            ret = RadIview.TestMCUConnection(mcu);
                            if (!ret)
                            {
                                this.errMsg = RadIview.errMsg;
                            }

                            break;
                        }
                    /* FB 2556 Ends */
					//FB 2718 Starts
                    case 15:
                        {
                            int type = 0;
                            mcu.sIp = xd.SelectSingleNode("//setBridge/bridge/bridgeDetails/portA").InnerXml.Trim();
                            int.TryParse(xd.SelectSingleNode("//setBridge/bridge/ApiPortNo").InnerXml.Trim(), out mcu.iHttpPort);
                            //FB 3054 Starts
                            //mcu.sPwd = xd.SelectSingleNode("//setBridge/bridge/password").InnerXml.Trim();
                            crypto = new Crypto();
                            encryptpass = xd.SelectSingleNode("//setBridge/bridge/password").InnerXml.Trim();
                            if(!string.IsNullOrEmpty(encryptpass))
                                encryptpass = crypto.decrypt(encryptpass);
                            mcu.sPwd = encryptpass;
                            //FB 3054 Ends
                            mcu.sLogin = xd.SelectSingleNode("//setBridge/bridge/login").InnerXml.Trim();

                            mcu.sRPRMDomain = xd.SelectSingleNode("//setBridge/bridge/bridgeDetails/Domain").InnerXml.Trim(); //ZD 100971
                     
                            NS_TMS.TMSScheduling TMS = new NS_TMS.TMSScheduling(configParams);
                            ret = TMS.TestMCUConnection(mcu);
                            if (!ret)
                                this.errMsg = TMS.errMsg;
                            break;
                        }
                    //FB 2718 Ends
                    //ZD 101217 Start
                    case 16:
                        {
                            // Its a Pexip MCU
                            mcu.sIp = xd.SelectSingleNode("//setBridge/bridge/bridgeDetails/portA").InnerXml.Trim();
                            mcu.etType = NS_MESSENGER.MCU.eType.Pexip;
                            
                            // Check connection to bridge. 				
                            NS_Pexip.pexip pexip = new NS_Pexip.pexip(configParams);
                            ret = pexip.TestMCUConnection(mcu);
                            if (!ret)
                            {
                                this.errMsg = pexip.errMsg;
                            }
                            break;
                        }
                    //ZD 101217 end
                    //ZD 104021 Start
                    case 20:
                        {
                            // Its a BJN MCU
                            mcu.sIp = xd.SelectSingleNode("//setBridge/bridge/bridgeDetails/controlPortIPAddress").InnerXml.Trim();
                            mcu.sBJNAPPkey = xd.SelectSingleNode("//setBridge/bridge/bridgeDetails/BJNAppKey").InnerXml;
                            mcu.sBJNAppsecret = xd.SelectSingleNode("//setBridge/bridge/bridgeDetails/BJNAppSecret").InnerXml;
                            mcu.etType = NS_MESSENGER.MCU.eType.BLUEJEANS;

                            NS_BlueJean.BlueJean BJN = new NS_BlueJean.BlueJean(configParams);
                            NS_DATABASE.Database db = new NS_DATABASE.Database(configParams);

                            if (mcu.sBJNAPPkey != "" && mcu.sBJNAppsecret != "")
                            {
                                //ret = db.CheckBJNToken(ref mcu);
                                //if (!ret)
                                //{
                                    BJN = new NS_BlueJean.BlueJean(configParams);
                                    ret = BJN.UpdateAccessToken(ref mcu);
                                    if (!ret) this.errMsg = BJN.errMsg;
                                //}
                            }
                            else
                            {
                                logger.Trace("BJN is not Configure properly.");
                                return (false);
                            }
                            break;
                        }
                    //ZD 104021 end
                    default:
                        {
                            this.errMsg = "Unknown MCU type.";
                            return false;
                        }
                    }                   

                if (!ret)
                {
                    logger.Trace("Test MCU operation failed.");
                    logger.Trace("Msg = " + this.errMsg);
                    return false;
                }
                return true;
            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
                return (false);
            }
        }
        #endregion

        //ZD 101363 Start

        #region TestEndpointConnection
        /// <summary>
        /// Test Endpoint SSH Connection
        /// </summary>
        /// <param name="inXml"></param>
        /// <returns></returns>
        internal bool TestEndpointConnection(string inXml)
        {
            bool ret = false;
            string encryptpass = "";
            int isSSHEnable = 0, isP2PEnable = 0, isDeleted = 0;
            cryptography.Crypto crypto = null;
            XmlNodeList nodeList = null;
            try
            {
                logger.Trace("Entering the Test Endpoint Connection method...");
                
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(inXml);
                XmlNode node = null;
                NS_SSH.SSH SSH = null;

                NS_MESSENGER.Party Party = new NS_MESSENGER.Party();
                NS_DATABASE.Database db = new NS_DATABASE.Database(configParams);

                int EndpointID = 0;
                if (xd.SelectSingleNode("//SetEndpoint/EndpointID") != null)
                    int.TryParse(xd.SelectSingleNode("//SetEndpoint/EndpointID").InnerXml, out EndpointID);

                nodeList = xd.SelectNodes("//SetEndpoint/Profiles/Profile");

                if (nodeList != null && nodeList.Count > 0)
                {
                    for (int i = 0; i < nodeList.Count; i++)
                    {
                        encryptpass = ""; isSSHEnable = 0;

                        node = nodeList[i];

                        if (node.SelectSingleNode("SSHSupport") != null)
                            int.TryParse(node.SelectSingleNode("SSHSupport").InnerXml, out isSSHEnable);

                        if (node.SelectSingleNode("Deleted") != null)
                            int.TryParse(node.SelectSingleNode("Deleted").InnerXml, out isDeleted);

                        if (node.SelectSingleNode("ProfileType") != null)
                            int.TryParse(node.SelectSingleNode("ProfileType").InnerXml, out isP2PEnable);

                        if (isSSHEnable != 1 || isP2PEnable == 2 || isDeleted != 0)
                            continue;

                        int profileid = 0;
                        if (node.SelectSingleNode("ProfileID") != null)
                            int.TryParse(node.SelectSingleNode("ProfileID").InnerXml, out profileid);

                        if (node.SelectSingleNode("ProfileName") != null)
                            Party.sName = node.SelectSingleNode("ProfileName").InnerXml.Trim();

                        if (node.SelectSingleNode("UserName") != null)
                            Party.sLogin = node.SelectSingleNode("UserName").InnerXml.Trim();

                        crypto = new Crypto();
                        if (node.SelectSingleNode("Password") != null)
                        {
                            encryptpass = node.SelectSingleNode("Password").InnerXml.Trim();
                        }
                        else if (profileid > 0 && EndpointID > 0)
                        {
                            db.FetchRoomPassword(EndpointID, profileid, ref encryptpass);
                        }
                        if (!string.IsNullOrEmpty(encryptpass))
                            encryptpass = crypto.decrypt(encryptpass);
                        Party.sPwd = encryptpass;

                        int APIPort = 22;
                        if (node.SelectSingleNode("ApiPortno") != null)
                            int.TryParse(node.SelectSingleNode("ApiPortno").InnerText.Trim(), out APIPort);

                        if (APIPort <= 0)
                            APIPort = 22;

                        Party.iAPIPortNo = APIPort;

                        logger.Trace("ApiPortno : " + Party.iAPIPortNo);

                        if (node.SelectSingleNode("Address") != null)
                            Party.sAddress = node.SelectSingleNode("Address").InnerXml.Trim();

                        if (node.SelectSingleNode("VideoEquipment") != null)
                        {
                            int videoequipmentid = int.Parse(node.SelectSingleNode("VideoEquipment").InnerXml.Trim());
                            if (videoequipmentid >= 21 && videoequipmentid <= 23)
                            {
                                Party.sGatewayAddress = Party.sAddress.Substring(Party.sAddress.LastIndexOf(":") + 1);
                                Party.sAddress = Party.sAddress.Substring(0, Party.sAddress.LastIndexOf(":"));
                                Party.etVideoEquipment = NS_MESSENGER.Party.eVideoEquipment.RECORDER;
                                Party.etModelType = NS_MESSENGER.Party.eModelType.CODIAN_VCR;
                            }
                            else
                            {
                                db.EndpointModelType(videoequipmentid, ref Party.etModelType);
                            }
                        }
                        ret = false;
                        SSH = new NS_SSH.SSH(configParams);
                        ret = SSH.TestConnectionSSH(Party);
                        if (!ret)
                        {
                            this.errMsg += SSH.errMsg;
                            logger.Trace("Test Endpoint operation failed.");
                            logger.Trace("Msg = " + this.errMsg);
                            return false;
                        }
                    }
                }
                return true;
            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
                return (false);
            }
        }
        #endregion

        //ZD 101363 End

        #region CallDetailRecords
        /// <summary>
        /// Test the bridge connectivity
        /// </summary>
        internal bool CallDetailRecords(string inXml)
        {
            XmlDocument xd = null;
            NS_DATABASE.Database db = null;
            List<NS_MESSENGER.MCU> cMcuList = null;
            NS_MESSENGER.MCU  cMcu = null;
            bool ret1 = false;
            NS_CallDetailRecords.CallDetailRecords clsCDR = null;
            try
            {
                logger.Trace("Entering the Call Detail Records method...");
                xd = new XmlDocument();
                xd.LoadXml(inXml);
                db = new NS_DATABASE.Database(configParams);
                cMcu = new NS_MESSENGER.MCU();

                ret1 = db.FetchCDRMcu(ref cMcuList); //FB 2660
                if (!ret1)
                {
                    logger.Trace("Error in Fetching MCU details.");
                    return false;
                }

                for (int bCnt = 0; bCnt < cMcuList.Count; bCnt++)
                {
                    try
                    {
                        ret1 = false;
                        cMcu = cMcuList[bCnt];
                        db.UpdateLastDateTimeCDRPoll(cMcu.iDbId); //FB 2593 

                        if (cMcu.etType == NS_MESSENGER.MCU.eType.CISCOTP)
                        {
                            //FB 2660 Starts
                            ret1 = db.DeleteCDRRecord(cMcu.iDbId, cMcu.iDeleteCDRDays, "CDR_Cisco_D", cMcu.sMcuType); //FB 2683 
                            if (!ret1)
                            {
                                logger.Trace("Error in deleting Cisco CDR Records");
                            }
                            //FB 2660 Ends
                            clsCDR = new NS_CallDetailRecords.CallDetailRecords(configParams);
                            ret1 = clsCDR.CDRforCisco8710(cMcu);
                            if (!ret1) this.errMsg = clsCDR.errMsg;
                        }
                        //FB 2797 Start
                        else if (cMcu.etType == NS_MESSENGER.MCU.eType.CODIAN)
                        {
                            ret1 = db.DeleteCDRRecord(cMcu.iDbId, cMcu.iDeleteCDRDays, "CDR_Codian_D", cMcu.sMcuType); //FB 2683 
                            if (!ret1)
                            {
                                logger.Trace("Error in deleting Codian CDR Records");
                            }
                            clsCDR = new NS_CallDetailRecords.CallDetailRecords(configParams);
                            ret1 = clsCDR.CDRforCodianMSESeries(cMcu);
                            if (!ret1) this.errMsg = clsCDR.errMsg;
                        }
                        //FB 2797 End
                        // FB 2683 Start
                        else if (cMcu.etType == NS_MESSENGER.MCU.eType.RMX)
                        {
                            ret1 = db.DeleteCDRRecord(cMcu.iDbId, cMcu.iDeleteCDRDays, "CDR_PolycomRMX_D", cMcu.sMcuType); //FB 2683 
                            if (!ret1)
                            {
                                logger.Trace("Error in deleting PolycomRMX CDR Records");
                            }
                            clsCDR = new NS_CallDetailRecords.CallDetailRecords(configParams);
                            ret1 = false;
                            ret1 = clsCDR.CDRforPolycomRMX(cMcu);
                            if (!ret1) this.errMsg = clsCDR.errMsg;
                        }
                        else if (cMcu.etType == NS_MESSENGER.MCU.eType.RPRM)
                        {
                            string ServicePath = "";
                            ret1 = db.DeleteCDRRecord(cMcu.iDbId, cMcu.iDeleteCDRDays, "CDR_PolycomRPRM_D", cMcu.sMcuType); //FB 2683 
                            if (!ret1)
                            {
                                logger.Trace("Error in deleting PolycomRPRM CDR Records");
                            }
                            clsCDR = new NS_CallDetailRecords.CallDetailRecords(configParams);
                            if (xd.SelectSingleNode("//Login/SeriveFilePath") != null)
                                ServicePath = xd.SelectSingleNode("//Login/SeriveFilePath").InnerText.Trim();

                            if (!string.IsNullOrEmpty(ServicePath))
                            {
                                ret1 = false;
                                ret1 = clsCDR.CDRforPolycomRPRM(cMcu, ServicePath);
                                if (!ret1) this.errMsg = clsCDR.errMsg;
                            }
                            else
                            {
                                logger.Trace("Service File Path is empty");
                            }
                        }
                        //ALLDEV-773 Start
                        else if (cMcu.etType == NS_MESSENGER.MCU.eType.Pexip)
                        {
                            clsCDR = new NS_CallDetailRecords.CallDetailRecords(configParams);
                            ret1 = clsCDR.CDRforPexip(cMcu, cMcu.iDeleteCDRDays);
                            if (!ret1) this.errMsg = clsCDR.errMsg;
                        }
                        //ALLDEV-773 End
                        if (!ret1)
                        {
                            logger.Trace("Call Detail Record Command failed.");
                            logger.Trace("Msg = " + this.errMsg);
                            //return false; //FB 2593
                        }
                    }
                    catch (Exception ex)
                    {
                        logger.Trace("Error in fetching the CDR for bridgeid: " + cMcu.iDbId + " Error:"+ex.Message);
                        continue;
                    }
                }
                return true;
            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
                return (false);
            }
        }
        #endregion

		//Method Changed for FB 2566 24 Dec,2012
        #region TerminateConference
        /// <summary>
		/// Terminate the conference on the bridge
		/// </summary>	
		internal bool TerminateConference (string inXml,ref string outXml,ref string msg)
		{
			//FB 2717 Starts
            NS_DATABASE.Database db = null;
            NS_Vidyo.Vidyo vidyoFuncton = null;
            int isEnableVidyo = -1, isCloudConference = 0;
            int orgID = 11;
			//FB 2717 End
			try
			{
				logger.Trace ("Entering the Terminate Conference function...");

                #region parse inXml
                // Load the string into xml parser and retrieve the tag values.
				XmlDocument xd = new XmlDocument();
				xd.LoadXml(inXml);
				
                //FB 2441 Starts
	            string login ="";
		        if(xd.SelectSingleNode("//login/userID")!=null)
				     login = xd.SelectSingleNode("//login/userID").InnerXml.Trim();

                int AccessType = 0; //FB 2556
                if (xd.SelectSingleNode("//login/FromService") != null)
                    int.TryParse(xd.SelectSingleNode("//login/FromService").InnerText.Trim(), out AccessType); //FB 2556
                
                string strConfId = "";
                if (xd.SelectSingleNode("//login/conferenceID") != null)
                    strConfId=xd.SelectSingleNode("//login/conferenceID").InnerXml.Trim();
                //FB 2441 Ends

				int confId = 0 ,instanceId = 0 ;
				bool ret = ExtractConfInstanceIds(strConfId,ref confId,ref instanceId);
				if (!ret)
				{
					logger.Trace ("Invalid conf/instance id.");
					return false;
                }
                #endregion
				//FB 2717 Starts
                if (db == null)
                    db = new NS_DATABASE.Database(configParams);
                
                db.FetchVidyoStatus(ref isEnableVidyo, ref isCloudConference, confId, instanceId, ref orgID);

                if (isEnableVidyo > 0 && isCloudConference > 0)
                {
                    vidyoFuncton = new NS_Vidyo.Vidyo(configParams);

                    ret = vidyoFuncton.TerminateConfernece(confId, instanceId, orgID, ref outXml);

                    if (!ret)
                    {
                        logger.Trace("Terminate conference operation failed.");
                        return false;
                    }

                }//FB 2717 End
                else
                {
                    #region Fetch conf
                    // Fetch the conf details - unique id, users,guests,rooms.
                    NS_MESSENGER.Conference conf = new NS_MESSENGER.Conference();
                    conf.iDbID = confId;
                    conf.iInstanceID = instanceId;
                    conf.iAccessType = AccessType; //FB 2556
                    ret = db.FetchConf(ref conf);
                    if (!ret)
                    {
                        logger.Exception(200, "Database fetch operation failed.");
                        return (false);
                    }
                    logger.Trace("Conf Name :" + conf.iDbNumName.ToString() + "-" + conf.sExternalName);
                    #endregion

                    if ((conf.iIsVMR == 0 && conf.iPermanent == 0) || conf.iBJNConf == 1) //ZD 100522 //ZD 103263 //ZD 104021
                    {
                        #region Fetch the endpoints
                        ret = false;
                        //ret = db.FetchAllRooms(conf.iDbID,conf.iInstanceID,ref conf.qParties);
                        ret = db.FetchRooms(conf.iDbID, conf.iInstanceID, ref conf.qParties, 0);
                        if (!ret)
                        {
                            logger.Exception(100, "Error in fetching rooms from db.");
                            return false;
                        }
                        ret = false;
                        //ret = db.FetchAllUsers(conf.iDbID,conf.iInstanceID,ref conf.qParties);
                        ret = db.FetchUsers(conf.iDbID, conf.iInstanceID, ref conf.qParties, 0);
                        if (!ret)
                        {
                            logger.Exception(100, "Error in fetching users from db.");
                            return false;
                        }
                        ret = db.FetchGuests(conf.iDbID, conf.iInstanceID, ref conf.qParties, 0);
                        if (!ret)
                        {
                            logger.Exception(100, "Error in fetching guests from db.");
                            return false;
                        }
                        ret = false;
                        #endregion

                        #region Check & connect if P2P call
                        if (conf.etType == NS_MESSENGER.Conference.eType.POINT_TO_POINT)
                        {
                            //ZD 101363 Start
                            #region Check Caller connection type

                            bool ret5 = false;
                            System.Collections.IEnumerator partyEnumerator;
                            partyEnumerator = conf.qParties.GetEnumerator();
                            int partyCount = conf.qParties.Count;
                            for (int i = 0; i < partyCount; i++)
                            {
                                // src party 
                                NS_MESSENGER.Party party = new NS_MESSENGER.Party();
                                // go to next party in the queue
                                partyEnumerator.MoveNext();
                                party = (NS_MESSENGER.Party)partyEnumerator.Current;
                                if (party.etCallerCallee == NS_MESSENGER.Party.eCallerCalleeType.CALLER)
                                {
                                    if (party.iSSHSupport == 1)
                                    {
                                        // connect call on ssh 
                                        NS_SSH.SSH SSH = new NS_SSH.SSH(this.configParams);
                                        ret5 = SSH.ConnectDisconnectCall(conf, null, false);
                                        if (!ret5)
                                        {
                                            // conf setup failed
                                            this.errMsg = "Error: " + SSH.errMsg;
                                            return false;
                                        }
                                    }
                                    else
                                    {
                                        // connect call on telnet 
                                        NS_TELNET.Telnet telnet = new NS_TELNET.Telnet(this.configParams);
                                        ret5 = telnet.ConnectDisconnectCall(conf, null, false);
                                        if (!ret5)
                                        {
                                            // conf setup failed
                                            this.errMsg = "Error: " + telnet.errMsg;
                                            return false;
                                        }
                                    }
                                }
                            }

                            #endregion                          
                            //ZD 101363 End
                        }
                        #endregion

                        #region Commented
                        //#region Fetch all the bridge ids on which this conf is running.
                        //System.Collections.IEnumerator partyEnumerator;				
                        //partyEnumerator = conf.qParties.GetEnumerator();
                        //int partyCount = conf.qParties.Count;			
                        //Queue mcuIdList = new Queue();
                        //for (int i = 0 ; i < partyCount ; i++)
                        //{
                        //    // go to next party in the queue
                        //    partyEnumerator.MoveNext();

                        //    // get the bridge which the party is assigned on.
                        //    NS_MESSENGER.Party party = new NS_MESSENGER.Party();
                        //    party = (NS_MESSENGER.Party) partyEnumerator.Current;
                        //    int	mcuId = party.cMcu.iDbId;

                        //    // check if this mcu id is already thr in the queue or not
                        //    System.Collections.IEnumerator mcuIdListEnumerator = mcuIdList.GetEnumerator();
                        //    int mcuCount = mcuIdList.Count;				
                        //    bool mcuIdExisting = false;
                        //    for (int j= 0 ; j < mcuCount; j++)
                        //    {
                        //        // go to the next item in the list
                        //        bool ret7 = mcuIdListEnumerator.MoveNext();
                        //        if (!ret7) break;

                        //        //NS_MESSENGER.MCU mcu = new NS_MESSENGER.MCU();
                        //        if ( mcuId == ((int) mcuIdListEnumerator.Current))
                        //        {
                        //            // mcu id already existing in the list. 
                        //            mcuIdExisting = true;
                        //            break;
                        //        }
                        //    }

                        //    if (!mcuIdExisting)
                        //    {
                        //        mcuIdList.Enqueue (mcuId);
                        //    }
                        //}				
                        //#endregion
                        #endregion

                        //FB 2566 24 Dec,2012
                        List<int> mcuIdList = new List<int>();
                        bool ret2 = db.FetchConfMcu(conf, ref mcuIdList);
                        if (!ret2)
                        {
                            logger.Exception(200, "Database fetch operation failed.");
                            return (false);
                        }

                        #region Connect to all the bridges and terminate the conference, one mcu at a time.
                        //int mcuIdListCount = mcuIdList.Count;
                        logger.Trace("Number of mcu's on which conf is running :" + mcuIdList.Count.ToString());
                        for (int k = 0; k < mcuIdList.Count; k++)
                        {
                            int mcuId = mcuIdList[k];

                            //Fetch the mcu info					
                            NS_MESSENGER.MCU mcu = new NS_MESSENGER.MCU();
                            bool ret1 = db.FetchMcu(mcuId, ref mcu);
                            if (!ret1)
                            {
                                logger.Trace("Error fetching mcu details.");
                                continue;
                            }

                            // Assign the conf bridge to the "mcu".
                            bool ret3 = false;
                            conf.cMcu = mcu;
                            #region Polycom
                            if (mcu.etType == NS_MESSENGER.MCU.eType.ACCORDv6 || mcu.etType == NS_MESSENGER.MCU.eType.ACCORDv7 || mcu.etType == NS_MESSENGER.MCU.eType.RMX)
                            {
                                NS_MESSENGER.Party dummyParty = new NS_MESSENGER.Party();
                                dummyParty.cMcu = conf.cMcu;
                                //ZD 103263 Start
                                if (conf.iBJNConf == 0 || (conf.iBJNConf == 1 && conf.iBJNMeetingType == (int)NS_MESSENGER.BJNMeetingType.PersonalMeeting))
                                {
                                    ret3 = polycom.OngoingConfOps("TerminateConference", conf, dummyParty, ref msg, true, 0, 0, false, ref dummyParty.etStatus, false, false); //FB 2553-RMX
                                    if (!ret3)
                                    {
                                        this.errMsg = polycom.errMsg;
                                        msg = polycom.errMsg;
                                    }
                                }
                                else if (conf.iBJNMeetingType == (int)NS_MESSENGER.BJNMeetingType.OneTimeMeeting)
                                {
                                    NS_BlueJean.BlueJean BJN = new NS_BlueJean.BlueJean(configParams);
                                    ret1 = db.FetchAccessKey(ref mcu);
                                    if (!ret1)
                                    {
                                        logger.Trace("Error in Fetching Rooms.");
                                        return (false);
                                    }

                                    if (mcu.sBJNAPPkey != "" && mcu.sBJNAppsecret != "")
                                    {
                                        ret1 = db.CheckBJNToken(ref mcu);
                                        if (!ret1)
                                        {
                                            BJN = new NS_BlueJean.BlueJean(configParams);
                                            ret1 = BJN.UpdateAccessToken(ref mcu);
                                            if (!ret1) this.errMsg = BJN.errMsg;
                                        }
                                    }
                                    else
                                    {
                                        logger.Trace("BJN is not Configure properly.");
                                        return (false);
                                    }
                                    BJN.isRecurring = iRecurring;
                                    ret2 = BJN.TerminateConference(conf, mcu.sBJNAccessToken);
                                    if (!ret2) this.errMsg = BJN.errMsg;

                                    ret3 = polycom.OngoingConfOps("TerminateConference", conf, dummyParty, ref msg, true, 0, 0, false, ref dummyParty.etStatus, false, false); //FB 2553-RMX
                                    if (!ret3)
                                    {
                                        this.errMsg = polycom.errMsg;
                                        msg = polycom.errMsg;
                                    }
                                }
                                //ZD 103263 End
                            }
                            #endregion
                            else
                            {
                                #region Codian
                                if (mcu.etType == NS_MESSENGER.MCU.eType.CODIAN)
                                {
                                    NS_CODIAN.Codian codian = new NS_CODIAN.Codian(configParams);
                                    //ZD 103263 Start
                                    if (conf.iBJNConf == 0 || (conf.iBJNConf == 1 && conf.iBJNMeetingType == (int)NS_MESSENGER.BJNMeetingType.PersonalMeeting))
                                    {
                                        logger.Trace("Conf Name :" + conf.iDbNumName.ToString() + "-" + conf.sExternalName);
                                        ret3 = codian.TerminateConference(conf);
                                        if (!ret3)
                                        {
                                            this.errMsg = codian.errMsg;
                                        }
                                    }
                                    else if (conf.iBJNMeetingType == (int)NS_MESSENGER.BJNMeetingType.OneTimeMeeting)
                                    {
                                        NS_BlueJean.BlueJean BJN = new NS_BlueJean.BlueJean(configParams);
                                        ret1 = db.FetchAccessKey(ref mcu);
                                        if (!ret1)
                                        {
                                            logger.Trace("Error in Fetching Rooms.");
                                            return (false);
                                        }

                                        if (mcu.sBJNAPPkey != "" && mcu.sBJNAppsecret != "")
                                        {
                                            ret1 = db.CheckBJNToken(ref mcu);
                                            if (!ret1)
                                            {
                                                BJN = new NS_BlueJean.BlueJean(configParams);
                                                ret1 = BJN.UpdateAccessToken(ref mcu);
                                                if (!ret1) this.errMsg = BJN.errMsg;
                                            }
                                        }
                                        else
                                        {
                                            logger.Trace("BJN is not Configure properly.");
                                            return (false);
                                        }
                                        BJN.isRecurring = iRecurring;
                                        ret2 = BJN.TerminateConference(conf, mcu.sBJNAccessToken);
                                        if (!ret2) this.errMsg = BJN.errMsg;

                                        ret3 = codian.TerminateConference(conf);
                                        if (!ret3)
                                        {
                                            this.errMsg = codian.errMsg;
                                        }
                                    }
                                    //ZD 103263 End
                                }
                                #endregion
                                else
                                {
                                    if (mcu.etType == NS_MESSENGER.MCU.eType.TANDBERG)
                                    {
                                        // Tandberg MCU
                                        NS_TANDBERG.Tandberg tandberg = new NS_TANDBERG.Tandberg(configParams);
                                        ret3 = tandberg.TerminateConference(conf);
                                        if (!ret3)
                                        {
                                            this.errMsg = tandberg.errMsg;
                                        }
                                    }
                                    else
                                    {
                                        if (mcu.etType == NS_MESSENGER.MCU.eType.RADVISION)
                                        {
                                            // Radvision MCU
                                            NS_RADVISION.Radvision rad = new NS_RADVISION.Radvision(configParams);
                                            ret3 = rad.TerminateConference(conf);
                                            if (!ret3)
                                            {
                                                this.errMsg = rad.errMsg;
                                            }
                                        }
                                        else if (mcu.etType == NS_MESSENGER.MCU.eType.CMA) //polycom CMA
                                        {
                                            // CMA bridge
                                            NS_POLYCOMCMA.PolycomCMA polycomCMA = new NS_POLYCOMCMA.PolycomCMA(configParams);
                                            ret3 = polycomCMA.TerminateConference(conf);
                                            if (!ret3) this.errMsg = polycomCMA.errMsg;
                                        }
                                        else if (mcu.etType == NS_MESSENGER.MCU.eType.Lifesize) //FB 2261
                                        {
                                            // lifesize bridge
                                            NS_LifeSize.LifeSize lifeSize = new NS_LifeSize.LifeSize(configParams);
                                            ret3 = lifeSize.TerminateConference(conf);
                                            if (!ret3) this.errMsg = lifeSize.errMsg;
                                        }
                                        //FB 2501 Call Monitoring Start
                                        else if (mcu.etType == NS_MESSENGER.MCU.eType.CISCOTP) //FB 2261
                                        {
                                            // CISCO MSE 8710 bridge
                                            NS_CiscoTPServer.CiscoTPServer CiscoTPServer = new NS_CiscoTPServer.CiscoTPServer(configParams);
                                            ret3 = CiscoTPServer.TerminateConference(conf);
                                            if (!ret3) this.errMsg = CiscoTPServer.errMsg;
                                        }
                                        //FB 2501 Call Monitoring End
                                        //FB 2556 Start
                                        else if (mcu.etType == NS_MESSENGER.MCU.eType.RADVISIONIVIEW)
                                        {
                                            // Radvision Iview bridge
                                            NS_RADVISION_IView.RadVisionIView RadIview = new NS_RADVISION_IView.RadVisionIView(configParams);
                                            RadIview.isRecurring = iRecurring;
                                            //ZD 100190
                                            if (conf.iStatus == 0 || conf.iStatus == 5)//while delete the ongoing conference //Status will be 9 if conference is future conf. So we will check 0 and 5.
                                            {
                                                ret3 = RadIview.TerminateConference(conf);
                                            }
                                            else
                                            {
                                                ret3 = RadIview.CancelConference(conf);//delete the scheduled conference
                                            }
                                            if (!ret3) this.errMsg = RadIview.errMsg;
                                        }
                                        //FB 2556 End
                                        //FB  2441 Starts
                                        else if (mcu.etType == NS_MESSENGER.MCU.eType.RPRM)
                                        {
                                            //ALLDEV-847 Start
                                            NS_POLYCOMRPRM.POLYCOMRPRM polycomRPRM = new NS_POLYCOMRPRM.POLYCOMRPRM(configParams);                                            
                                            if (conf.iBJNConf == 0 || (conf.iBJNConf == 1 && conf.iBJNMeetingType == (int)NS_MESSENGER.BJNMeetingType.PersonalMeeting))
                                            {
                                                logger.Trace("Conf Name :" + conf.iDbNumName.ToString() + "-" + conf.sExternalName);
                                                polycomRPRM.isRecurring = iRecurring;
                                                ret3 = polycomRPRM.TerminateConference(conf);
                                                if (!ret3)
                                                {
                                                    this.errMsg = polycomRPRM.errMsg;
                                                }
                                            }
                                            else if (conf.iBJNMeetingType == (int)NS_MESSENGER.BJNMeetingType.OneTimeMeeting)
                                            {
                                                NS_BlueJean.BlueJean BJN = new NS_BlueJean.BlueJean(configParams);
                                                ret1 = db.FetchAccessKey(ref mcu);
                                                if (!ret1)
                                                {
                                                    logger.Trace("Error in Fetching Rooms.");
                                                    return (false);
                                                }

                                                if (mcu.sBJNAPPkey != "" && mcu.sBJNAppsecret != "")
                                                {
                                                    ret1 = db.CheckBJNToken(ref mcu);
                                                    if (!ret1)
                                                    {
                                                        BJN = new NS_BlueJean.BlueJean(configParams);
                                                        ret1 = BJN.UpdateAccessToken(ref mcu);
                                                        if (!ret1) this.errMsg = BJN.errMsg;
                                                    }
                                                }
                                                else
                                                {
                                                    logger.Trace("BJN is not Configure properly.");
                                                    return (false);
                                                }
                                                BJN.isRecurring = iRecurring;
                                                ret2 = BJN.TerminateConference(conf, mcu.sBJNAccessToken);
                                                if (!ret2) this.errMsg = BJN.errMsg;

                                                polycomRPRM.isRecurring = iRecurring;
                                                ret3 = polycomRPRM.TerminateConference(conf);
                                                if (!ret3)
                                                {
                                                    this.errMsg = polycomRPRM.errMsg;
                                                }
                                            }
                                            //ALLDEV-847 End
                                        } //FB 2441 Ends
                                        //FB  2718 Starts
                                        else if (mcu.etType == NS_MESSENGER.MCU.eType.TMSScheduling)
                                        {
                                            NS_TMS.TMSScheduling TMS = new NS_TMS.TMSScheduling(configParams);
                                            TMS.isRecurring = iRecurring;
                                            ret3 = TMS.DeleteScheduledConference(conf); //ZD 100971
                                            if (!ret3) this.errMsg = TMS.errMsg;
                                        } //FB 2718 Ends
                                        else if (mcu.etType == NS_MESSENGER.MCU.eType.Pexip) //ZD 101217
                                        {
                                            // Pexip bridge
                                            NS_Pexip.pexip pexip = new NS_Pexip.pexip(configParams);
                                            //ZD 104114 Start
                                            //ret3 = pexip.DeleteConference(conf);
                                            //if (!ret3) this.errMsg = pexip.errMsg;
                                            if (conf.iBJNConf == 0 || (conf.iBJNConf == 1 && conf.iBJNMeetingType == (int)NS_MESSENGER.BJNMeetingType.PersonalMeeting))
                                            {
                                                logger.Trace("Conf Name :" + conf.iDbNumName.ToString() + "-" + conf.sExternalName);
                                                ret3 = pexip.DeleteConference(conf);
                                                if (!ret3)
                                                {
                                                    this.errMsg = pexip.errMsg;
                                                }
                                            }
                                            else if (conf.iBJNMeetingType == (int)NS_MESSENGER.BJNMeetingType.OneTimeMeeting)
                                            {
                                                NS_BlueJean.BlueJean BJN = new NS_BlueJean.BlueJean(configParams);
                                                ret1 = db.FetchAccessKey(ref mcu);
                                                if (!ret1)
                                                {
                                                    logger.Trace("Error in Fetching Rooms.");
                                                    return (false);
                                                }

                                                if (mcu.sBJNAPPkey != "" && mcu.sBJNAppsecret != "")
                                                {
                                                    ret1 = db.CheckBJNToken(ref mcu);
                                                    if (!ret1)
                                                    {
                                                        BJN = new NS_BlueJean.BlueJean(configParams);
                                                        ret1 = BJN.UpdateAccessToken(ref mcu);
                                                        if (!ret1) this.errMsg = BJN.errMsg;
                                                    }
                                                }
                                                else
                                                {
                                                    logger.Trace("BJN is not Configure properly.");
                                                    return (false);
                                                }
                                                BJN.isRecurring = iRecurring;
                                                ret2 = BJN.TerminateConference(conf, mcu.sBJNAccessToken);
                                                if (!ret2) this.errMsg = BJN.errMsg;

                                                ret3 = pexip.DeleteConference(conf);
                                                if (!ret3)
                                                {
                                                    this.errMsg = pexip.errMsg;
                                                }
                                            }
                                            //ZD 104114 End
                                        }
                                        else if (mcu.etType == NS_MESSENGER.MCU.eType.BLUEJEANS) //ZD 104021
                                        {
                                            if (conf.iBJNMeetingType == (int)NS_MESSENGER.BJNMeetingType.OneTimeMeeting)
                                            {
                                                NS_BlueJean.BlueJean BJN = new NS_BlueJean.BlueJean(configParams);
                                                ret1 = db.FetchAccessKey(ref mcu);
                                                if (!ret1)
                                                {
                                                    logger.Trace("Error in Fetching Rooms.");
                                                    return (false);
                                                }

                                                if (mcu.sBJNAPPkey != "" && mcu.sBJNAppsecret != "")
                                                {
                                                    ret1 = db.CheckBJNToken(ref mcu);
                                                    if (!ret1)
                                                    {
                                                        BJN = new NS_BlueJean.BlueJean(configParams);
                                                        ret1 = BJN.UpdateAccessToken(ref mcu);
                                                        if (!ret1) this.errMsg = BJN.errMsg;
                                                    }
                                                }
                                                else
                                                {
                                                    logger.Trace("BJN is not Configure properly.");
                                                    return (false);
                                                }
                                                BJN.isRecurring = iRecurring;
                                                ret2 = BJN.TerminateConference(conf, mcu.sBJNAccessToken);
                                                if (!ret2) this.errMsg = BJN.errMsg;
                                            }
                                        }
                                    }
                                }
                            }
                        #endregion

                            if (!ret3)
                            {
                                logger.Trace("Terminate conference operation failed.");
                                //FB 2569 Start
                                alert = new NS_MESSENGER.Alert();
                                alert.confID = conf.iDbID;
                                alert.instanceID = conf.iInstanceID;
                                alert.message = this.errMsg;
                                alert.typeID = 7; //Conference Terminate
                                bool retp2p = db.InsertConfAlert(alert);
                                if (!retp2p)
                                    logger.Trace("Terminate Conference alert insert failed.");
                                //FB 2569 End
                                return false;
                            }
                        }
                    }
                    //ZD 100522 Start
                    else
                    {
                        NS_OPERATIONS.ConfMonitor cfmonitor = new ConfMonitor(configParams);
                        if (!cfmonitor.TerminateConfVMR(conf))
                            return false;
                    }
                    //ZD 100522 End
                }
				return true;
			}
			catch (Exception e)
			{
				logger.Exception(100,e.Message);
				return (false);
			}
        }

        internal bool TerminateConference(int confId, int instanceId)
        {
            NS_MESSENGER.Conference conf = null;
            NS_DATABASE.Database db = null;
            List<int> mcuIdList = null;
            NS_MESSENGER.MCU mcu = null;
            int mcuId = 0;
            bool ret1 = false;
            bool ret = false;
            bool ret3 = true;
            NS_POLYCOMRPRM.POLYCOMRPRM polycomRPRM = null;
            try
            {
                logger.Trace("Entering the Terminate Conference function...");

                #region Fetch conf
                // Fetch the conf details - unique id, users,guests,rooms.
                conf = new NS_MESSENGER.Conference();
                db = new NS_DATABASE.Database(configParams);
                conf.iDbID = confId;
                conf.iInstanceID = instanceId;
                conf.iAccessType = 0; //FB 2556
                ret = db.FetchConf(ref conf);
                if (!ret)
                {
                    logger.Exception(200, "Database fetch operation failed.");
                    return (false);
                }
                logger.Trace("Conf Name :" + conf.iDbNumName.ToString() + "-" + conf.sExternalName);
                #endregion

               

                
                mcuIdList = new List<int>();
                bool ret2 = db.FetchConfSychronousMcu(conf, ref mcuIdList);
                if (!ret2)
                {
                    logger.Exception(200, "Database fetch operation failed.");
                    return (false);
                }

                #region Connect to all the bridges and terminate the conference, one mcu at a time.
                
                logger.Trace("Number of mcu's on which conf is running :" + mcuIdList.Count.ToString());
                for (int k = 0; k < mcuIdList.Count; k++)
                {
                    mcuId = mcuIdList[k];

                    //Fetch the mcu info					
                    mcu = new NS_MESSENGER.MCU();
                    ret1 = db.FetchMcu(mcuId, ref mcu);
                    if (!ret1)
                    {
                        logger.Trace("Error fetching mcu details.");
                        continue;
                    }

                    // Assign the conf bridge to the "mcu".
                    
                    conf.cMcu = mcu;
                    
                    if (mcu.etType == NS_MESSENGER.MCU.eType.RPRM)
                    {                  
                
                        polycomRPRM = new NS_POLYCOMRPRM.POLYCOMRPRM(configParams);
                        ret3 = polycomRPRM.TerminateConference(conf);
                        if (!ret3) this.errMsg = polycomRPRM.errMsg;
                    }
                                
                            
                }
                #endregion

                if (!ret3)
                {
                    logger.Trace("Terminate conference operation failed. :" + errMsg);
                }

                ret3 = db.TerminateConference(confId, instanceId);

                if (!ret3)
                {
                    logger.Trace("Terminate conference operation failed. local Update" );
                    return false;
                }


                return true;
            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
                return (false);
            }
        }
        #endregion

        #region ExtendConfEndTime
        /// <summary>
		/// Extend an ongoing conference end time. 
		/// </summary>	
		internal bool ExtendConfEndTime(string inXml,ref string outXml,ref string msg)
		{
			try
			{
				logger.Trace ("Entering the function...");				
				// Load the string into xml parser and retrieve the tag values.
				XmlDocument xd = new XmlDocument();
				xd.LoadXml(inXml);
							
				string login = xd.SelectSingleNode("//login/userID").InnerXml.Trim();

				string strConfId = xd.SelectSingleNode("//login/confInfo/confID").InnerXml.Trim();
				int confId = 0 ,instanceId = 0 ;
				bool ret = ExtractConfInstanceIds(strConfId,ref confId,ref instanceId);
				if (!ret)
				{
					logger.Trace ("Invalid conf/instance id.");
					return false;
				}

				int retryNum = Int32.Parse(xd.SelectSingleNode("//login/confInfo/retry").InnerXml.Trim()); // retry		
				if (retryNum == 1) 
				{
					//TODO : what is this for  ?
					// retry = true;
				}
				
				int extendTime = Int32.Parse(xd.SelectSingleNode("//login/confInfo/extendEndTime").InnerXml.Trim()); // extend conf end time (in mins)

				// Fetch the conf details - unique id, users,guests,rooms.
				NS_MESSENGER.Conference conf = new NS_MESSENGER.Conference();
				NS_DATABASE.Database db = new NS_DATABASE.Database(configParams);
				conf.iDbID = confId;
				conf.iInstanceID = instanceId;
                ret = db.FetchConfInfo(ref conf); 
				if (!ret) 
				{
					logger.Exception(200,"Database fetch operation failed.");
					return (false);
				}

                //ZD 100522 Starts
                if (conf.iIsVMR == 2 && conf.iBJNConf == 0) //ZD 103263
                    return true;
                //ZD 100522 Ends
                ret = db.GetandSetMCUStartandEnd(ref conf); //ZD 100085


                #region Commented for FB 2584
                /* 
                #region Fetch the endpoints
                ret = false;
                ret = db.FetchRooms(conf.iDbID, conf.iInstanceID, ref conf.qParties, 0);
                if (!ret)
                {
                    logger.Exception(100, "Error in fetching rooms from db.");
                    return false;
                }
                ret = false;
                ret = db.FetchUsers(conf.iDbID, conf.iInstanceID, ref conf.qParties, 0);
                if (!ret)
                {
                    logger.Exception(100, "Error in fetching users from db.");
                    return false;
                }
                ret = false;
                ret = db.FetchGuests(conf.iDbID, conf.iInstanceID, ref conf.qParties, 0);
                if (!ret)
                {
                    logger.Exception(100, "Error in fetching guests from db.");
                    return false;
                }
                ret = false;
                Queue cascadeLinks = new Queue();
                ret = db.FetchCascadeLinks(conf.iDbID, conf.iInstanceID, ref cascadeLinks, 0);
                if (!ret)
                {
                    logger.Exception(100, "Error in fetching cascade links from db.");
                    return false;
                }
                #endregion

				// Fetch all the bridge ids on which this conf is running.
				System.Collections.IEnumerator partyEnumerator;				
				partyEnumerator = conf.qParties.GetEnumerator();
				int partyCount = conf.qParties.Count;
			
				Queue mcuIdList = new Queue();

				for (int i = 0 ; i < partyCount ; i++)
				{
					// go to next party in the queue
					partyEnumerator.MoveNext();
							
					// get the bridge which the party is assigned on.
					NS_MESSENGER.Party party = new NS_MESSENGER.Party();
					party = (NS_MESSENGER.Party) partyEnumerator.Current;
					int	mcuId = party.cMcu.iDbId;
						
					// check if this mcu id is already thr in the queue or not
					System.Collections.IEnumerator mcuIdListEnumerator = mcuIdList.GetEnumerator();
					int mcuCount = mcuIdList.Count;
					bool mcuIdExisting = false;
					for (int j= 0 ; j < mcuCount; j++)
					{
						mcuIdListEnumerator.MoveNext();

						//NS_MESSENGER.MCU mcu = new NS_MESSENGER.MCU();
						if ( mcuId == ((int) mcuIdListEnumerator.Current))
						{
							// mcu id already existing in the list. 
		    				mcuIdExisting = true;
							break;
						}						
					}
						
					if (!mcuIdExisting)
					{
						mcuIdList.Enqueue (mcuId);
					}						
				}
                */
                #endregion
                // connect to all the bridges and extend the conf end time.

                //FB 2710 Start
                if (conf.etType == NS_MESSENGER.Conference.eType.POINT_TO_POINT)
                {
                    logger.Trace("Point-to-Point call Extend time");
                    return true;
                }
                //FB 2710 End

                //FB 2584 Start
                List<int> mcuIdList = new List<int>();
                bool ret2 = db.FetchConfMcu(conf, ref mcuIdList);
                if (!ret2)
                {
                    logger.Exception(200, "Database fetch operation failed.");
                    return (false);
                }
                //FB 2584 End

				int mcuIdListCount = mcuIdList.Count;
				for (int k = 0; k < mcuIdListCount ; k++)
				{
                    int mcuId = mcuIdList[k];//FB 2584
					
					//Fetch the mcu info					
					NS_MESSENGER.MCU mcu = new NS_MESSENGER.MCU();
					bool ret1 = db.FetchMcu(mcuId,ref mcu);
					if (!ret1) 
					{
						logger.Trace("Error fetching mcu details.");
						continue;
					}
					
					//Load the conf with mcu details
					conf.cMcu = mcu;

					bool ret3 = false;
                    if (mcu.etType == NS_MESSENGER.MCU.eType.ACCORDv6 || mcu.etType == NS_MESSENGER.MCU.eType.ACCORDv7 || mcu.etType == NS_MESSENGER.MCU.eType.RMX)
					{
						//ZD 103263 Start
                        if (conf.iBJNConf == 1 && conf.iBJNMeetingType == (int)NS_MESSENGER.BJNMeetingType.OneTimeMeeting)
                        {
                            NS_BlueJean.BlueJean BJN = new NS_BlueJean.BlueJean(configParams);
                            ret1 = db.FetchAccessKey(ref mcu);
                            if (!ret1)
                            {
                                logger.Trace("Error in Fetching Rooms.");
                                return (false);
                            }

                            if (mcu.sBJNAPPkey != "" && mcu.sBJNAppsecret != "")
                            {
                                ret1 = db.CheckBJNToken(ref mcu);
                                if (!ret1)
                                {
                                    BJN = new NS_BlueJean.BlueJean(configParams);
                                    ret1 = BJN.UpdateAccessToken(ref mcu);
                                    if (!ret1) this.errMsg = BJN.errMsg;
                                }
                            }
                            else
                            {
                                logger.Trace("BJN is not Configure properly.");
                                return (false);
                            }

                            ret2 = BJN.ExtendConfEndTime(conf,extendTime, mcu.sBJNAccessToken);
                            if (!ret2) this.errMsg = BJN.errMsg;

                        }
						//ZD 103263 End
						//NS_POLYCOM.Polycom accord = new NS_POLYCOM.Polycom(configParams);
						NS_MESSENGER.Party dummyParty = new NS_MESSENGER.Party();
						dummyParty.cMcu = conf.cMcu;
                        ret3 = polycom.OngoingConfOps("ExtendEndTime", conf, dummyParty, ref msg, false, 0, extendTime, false, ref dummyParty.etStatus, false, false); //FB 2553-RMX
						if (!ret3)
						{
							this.errMsg = polycom.errMsg;
                            msg = polycom.errMsg;
						}
					}
					else
					{
                        if (mcu.etType == NS_MESSENGER.MCU.eType.CODIAN)
                        {
							//ZD 103263 Start
                            if (conf.iBJNConf == 1 && conf.iBJNMeetingType == (int)NS_MESSENGER.BJNMeetingType.OneTimeMeeting)
                            {
                                NS_BlueJean.BlueJean BJN = new NS_BlueJean.BlueJean(configParams);
                                ret1 = db.FetchAccessKey(ref mcu);
                                if (!ret1)
                                {
                                    logger.Trace("Error in Fetching Rooms.");
                                    return (false);
                                }

                                if (mcu.sBJNAPPkey != "" && mcu.sBJNAppsecret != "")
                                {
                                    ret1 = db.CheckBJNToken(ref mcu);
                                    if (!ret1)
                                    {
                                        BJN = new NS_BlueJean.BlueJean(configParams);
                                        ret1 = BJN.UpdateAccessToken(ref mcu);
                                        if (!ret1) this.errMsg = BJN.errMsg;
                                    }
                                }
                                else
                                {
                                    logger.Trace("BJN is not Configure properly.");
                                    return (false);
                                }

                                ret2 = BJN.ExtendConfEndTime(conf, extendTime, mcu.sBJNAccessToken);
                                if (!ret2) this.errMsg = BJN.errMsg;

                            }
							//ZD 103263 End
                            NS_CODIAN.Codian codian = new NS_CODIAN.Codian(configParams);
                            ret3 = codian.ExtendConfEndTime(conf, extendTime, ref msg);
                            if (!ret3)
                            {
                                this.errMsg = codian.errMsg;
                                msg = codian.errMsg;
                            }
                        }
                        else
                        {
                            if (mcu.etType == NS_MESSENGER.MCU.eType.TANDBERG)
                            {
                                NS_TANDBERG.Tandberg tandberg = new NS_TANDBERG.Tandberg(configParams);
                                ret3 = tandberg.ExtendConfEndTime(conf, extendTime);
                                if (!ret3)
                                {
                                    this.errMsg = tandberg.errMsg;                                    
                                }
                            }
                            else if (mcu.etType == NS_MESSENGER.MCU.eType.CMA)
                            {
                                // CMA bridge
                                NS_POLYCOMCMA.PolycomCMA polycomCMA = new NS_POLYCOMCMA.PolycomCMA(configParams);
                                ret3 = polycomCMA.ExtendConference(conf, extendTime);
                                if (!ret3) this.errMsg = polycomCMA.errMsg;
                            }
                            // FB 2441  Starts
                            else if (mcu.etType == NS_MESSENGER.MCU.eType.RPRM)
                            {
                                // RPRM bridge
                                NS_POLYCOMRPRM.POLYCOMRPRM polycomRPRM = new NS_POLYCOMRPRM.POLYCOMRPRM(configParams);
                                ret3 = polycomRPRM.ExtendConference(conf, extendTime);
                                if (!ret3) this.errMsg = polycomRPRM.errMsg;
                            }
                            //FB 2441 Ends
                            else if (mcu.etType == NS_MESSENGER.MCU.eType.Lifesize) //FB 2261
                            {
                                // lifesize bridge
                                NS_LifeSize.LifeSize lifeSize = new NS_LifeSize.LifeSize(configParams);
                                ret3 = lifeSize.ExtendConference(conf, extendTime);
                                if (!ret3) this.errMsg = lifeSize.errMsg;
                            }
                            //FB 2501 Call Monitoring Start
                            else if (mcu.etType == NS_MESSENGER.MCU.eType.CISCOTP) //FB 2261
                            {
                                // CISCO MSE 8710 bridge
                                NS_CiscoTPServer.CiscoTPServer CiscoTPServer = new NS_CiscoTPServer.CiscoTPServer(configParams);
                                ret3 = CiscoTPServer.ExtendConfEndTime(conf, extendTime);
                                if (!ret3) this.errMsg = CiscoTPServer.errMsg;
                            }
                            //FB 2501 Call Monitoring End
                            // FB 2556 Starts
                            else if (mcu.etType == NS_MESSENGER.MCU.eType.RADVISIONIVIEW) 
                            {
                                // Radvision IView
                                NS_RADVISION_IView.RadVisionIView RadIview = new NS_RADVISION_IView.RadVisionIView(configParams);
                                ret3 = RadIview.ExtendConfEndTime(conf, extendTime);
                                if (!ret3) this.errMsg = RadIview.errMsg;
                            }
                            // FB 2556 Ends
                            // ZD 101217 Starts
                            else if (mcu.etType == NS_MESSENGER.MCU.eType.Pexip)
                            {
                                //ZD 104114 Start
                                if (conf.iBJNConf == 1 && conf.iBJNMeetingType == (int)NS_MESSENGER.BJNMeetingType.OneTimeMeeting)
                                {
                                    NS_BlueJean.BlueJean BJN = new NS_BlueJean.BlueJean(configParams);
                                    ret1 = db.FetchAccessKey(ref mcu);
                                    if (!ret1)
                                    {
                                        logger.Trace("Error in Fetching Rooms.");
                                        return (false);
                                    }

                                    if (mcu.sBJNAPPkey != "" && mcu.sBJNAppsecret != "")
                                    {
                                        ret1 = db.CheckBJNToken(ref mcu);
                                        if (!ret1)
                                        {
                                            BJN = new NS_BlueJean.BlueJean(configParams);
                                            ret1 = BJN.UpdateAccessToken(ref mcu);
                                            if (!ret1) this.errMsg = BJN.errMsg;
                                        }
                                    }
                                    else
                                    {
                                        logger.Trace("BJN is not Configure properly.");
                                        return (false);
                                    }

                                    ret2 = BJN.ExtendConfEndTime(conf, extendTime, mcu.sBJNAccessToken);
                                    if (!ret2) this.errMsg = BJN.errMsg;

                                }
                                //ZD 104114 End
                                ret3 = true;
                            }
                            // ZD 101217 Ends
                        }
					}
					if (!ret3) 
					{
						logger.Trace("Extend conf end time operation failed.");
                        //FB 2569 Start
                        alert = new NS_MESSENGER.Alert();
                        alert.confID = conf.iDbID;
                        alert.instanceID = conf.iInstanceID;
                        alert.message = this.errMsg;
                        alert.typeID = 6; //Conference Extendtime
                        bool retp2p = db.InsertConfAlert(alert);
                        if (!retp2p)
                            logger.Trace("Extendtime alert insert failed.");
                        //FB 2569 End
						return false;
					}
				}
				return true;
			}
			catch (Exception e)
			{
				logger.Exception(100,e.Message);
				return (false);
			}
        }
        #endregion

        #region TerminateEndpoint
        /// <summary>
		/// Terminate an endpoint within an ongoing conference.
		/// </summary>
		/// <param name="inXml"></param>
		/// <param name="outXml"></param>
		/// <param name="msg"></param>
		/// <returns></returns>
		internal bool  TerminateEndpoint(string inXml,ref string outXml,ref string msg)
		{
            try
            {
                logger.Trace("Entering the function...");
                // Load the string into xml parser and retrieve the tag values.
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(inXml);

                string login = xd.SelectSingleNode("//login/userID").InnerXml.Trim();

                string strConfId = xd.SelectSingleNode("//login/confID").InnerXml.Trim();
                int confId = 0, instanceId = 0;
                bool ret = ExtractConfInstanceIds(strConfId, ref confId, ref instanceId);
                if (!ret)
                {
                    logger.Trace("Invalid conf/instance id.");
                    return false;
                }

                int endpointId = Int32.Parse(xd.SelectSingleNode("//login/endpointID").InnerXml.Trim()); // database id

                int terminalType = Int32.Parse(xd.SelectSingleNode("//login/terminalType").InnerXml.Trim()); // 1 = user , 2 = room, 3 = guest								 
                logger.Trace("Terminaltype = " + terminalType.ToString());
                //ALLDEV-814
                if (xd.SelectSingleNode("//login/EptProfileID") != null)
                    int.TryParse(xd.SelectSingleNode("//login/EptProfileID").InnerXml.Trim(), out eptProfileID);

                // Fetch the conf unique name since thats the unique identifier of the conf.
                NS_MESSENGER.Conference conf = new NS_MESSENGER.Conference();
                NS_DATABASE.Database db = new NS_DATABASE.Database(configParams);
                conf.iDbID = confId;
                conf.iInstanceID = instanceId;
                ret = db.FetchConf(ref conf);
                if (!ret)
                {
                    logger.Exception(200, "Database fetch operation failed.");
                    return (false);
                }

                // Retreive the bridge info for that endpoint.
                NS_MESSENGER.Party party = new NS_MESSENGER.Party();
                switch (terminalType)
                {
                    case 1:
                        {
                            // user
                            db.eptProfileID = eptProfileID; //ALLDEV-814
                            bool ret2 = db.FetchUser(confId, instanceId, endpointId, ref party);
                            break;
                        }
                    case 2:
                        {
                            // room
                            logger.Trace("In room...");
                            bool ret2 = db.FetchRoom(confId, instanceId, endpointId, ref party);
                            break;
                        }
                    case 3:
                        {
                            // guest
                            bool ret2 = db.FetchGuest(confId, instanceId, endpointId, ref party);
                            break;
                        }
                }

                //FB 2578 Start

                bool ret3 = false;

                List<NS_MESSENGER.Party> parties = TelepresenceEndpoint(party); //FB 2584

                for (int i = 0; i < parties.Count; i++)
                {

                    party = parties[i]; //FB 2578
                    // connect to the bridge and perform the operation.
                    ret3 = false;
                    if (party.cMcu.etType == NS_MESSENGER.MCU.eType.ACCORDv6 || party.cMcu.etType == NS_MESSENGER.MCU.eType.ACCORDv7 || party.cMcu.etType == NS_MESSENGER.MCU.eType.RMX)
                    {
                        //NS_POLYCOM.Polycom accord = new NS_POLYCOM.Polycom(configParams);
                        ret3 = polycom.OngoingConfOps("TerminateEndpoint", conf, party, ref msg, true, 0, 0, false, ref party.etStatus, false, false);//FB 2553-RMX
                        if (!ret3)
                        {
                            this.errMsg = polycom.errMsg;
                            msg = polycom.errMsg;
                        }
                    }
                    else
                    {
                        if (party.cMcu.etType == NS_MESSENGER.MCU.eType.CODIAN)
                        {
                            NS_CODIAN.Codian codian = new NS_CODIAN.Codian(configParams);
                            ret3 = codian.TerminateEndpoint(conf, party);
                            if (!ret3)
                            {
                                this.errMsg = codian.errMsg;
                            }
                        }
                        //FB 2441 Starts
                        else if (party.cMcu.etType == NS_MESSENGER.MCU.eType.RPRM)
                        {
                            return true;
                        }
                        //FB 2441 Ends
                        //FB 2501 Call Monitoring Start
                        else if (party.cMcu.etType == NS_MESSENGER.MCU.eType.CISCOTP)
                        {
                            NS_CiscoTPServer.CiscoTPServer CiscoTPServer = new NS_CiscoTPServer.CiscoTPServer(configParams);
                            ret3 = CiscoTPServer.TerminateEndpoint(conf, party);
                            if (!ret3)
                            {
                                this.errMsg = CiscoTPServer.errMsg;
                            }
                        }
                        //FB 2501 Call Monitoring End
                        else if (party.cMcu.etType == NS_MESSENGER.MCU.eType.Pexip) //ZD 101217 //ZD 101603
                        {
                            return true;
                        }
                        //FB 2556 Starts
                        else if (party.cMcu.etType == NS_MESSENGER.MCU.eType.RADVISIONIVIEW)
                        {
                            NS_RADVISION_IView.RadVisionIView RadIview = new NS_RADVISION_IView.RadVisionIView(configParams);
                            ret3 = RadIview.TerminateEndpoint(conf, party);
                            if (!ret3)
                            {
                                this.errMsg = RadIview.errMsg;
                            }

                        }
                        //FB 2556 Ends
                        else
                        {
                            // Tandberg MCU
                            NS_TANDBERG.Tandberg tandberg = new NS_TANDBERG.Tandberg(configParams);
                            ret3 = tandberg.TerminateEndpoint(conf, party);
                            if (!ret3)
                            {
                                this.errMsg = tandberg.errMsg;
                            }
                        }
                    }

                    
                }
                if (!ret3)
                {
                    logger.Trace("Terminate operation failed for endpoint.");
                    //FB 2569 Start
                    alert = new NS_MESSENGER.Alert();
                    alert.confID = conf.iDbID;
                    alert.instanceID = conf.iInstanceID;
                    alert.message = this.errMsg;
                    alert.typeID = 4; //Terminate Endpoint
                    bool retp2p = db.InsertConfAlert(alert);
                    if (!retp2p)
                        logger.Trace("Terminate Endpoint alert insert failed.");
                    //FB 2569 End
                    return false;
                }
                //FB 2578 End
                return true;
            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
                return (false);
            }
        }
        #endregion

        #region MuteEndpoint
        /// <summary>
		/// Mute an ongoing endpoint.
		/// </summary>
		/// <param name="inXml"></param>
		/// <param name="outXml"></param>
		/// <param name="msg"></param>
		/// <returns></returns>
		internal bool  MuteEndpoint(string inXml,ref string outXml,ref string msg)
		{
			try
			{
				logger.Trace ("Entering the function...");				
				// Load the string into xml parser and retrieve the tag values.
				XmlDocument xd = new XmlDocument();
				xd.LoadXml(inXml);
							
				string login = xd.SelectSingleNode("//login/userID").InnerXml.Trim();

				string strConfId = xd.SelectSingleNode("//login/confID").InnerXml.Trim();
				int confId = 0 ,instanceId = 0 ;
				bool ret = ExtractConfInstanceIds(strConfId,ref confId,ref instanceId);
				if (!ret)
				{
					logger.Trace ("Invalid conf/instance id.");
					return false;
				}

                int endpointId = Int32.Parse(xd.SelectSingleNode("//login/endpointID").InnerXml.Trim()); // database id

				int terminalType = Int32.Parse(xd.SelectSingleNode("//login/terminalType").InnerXml.Trim()); // 1 = user , 2 = room, 3 = guest , 4 = cascade				
				logger.Trace ("Terminaltype = " + terminalType.ToString());

				int iMute = Int32.Parse(xd.SelectSingleNode("//login/mute").InnerXml.Trim()); // 0 = off, 1 = on
				bool mute = false;
				if (iMute ==1) 
				{
					mute = true;
				}
                
                //FB 2530 Start

                string strmuteAll = xd.SelectSingleNode("//login/muteAll").InnerText.Trim(); // 1 - Party Mute  2 - Party All Mute
                int muteAll = 0;
                Int32.TryParse(strmuteAll, out muteAll);

                //FB 2530 End
                //ALLDEV-814
                if (xd.SelectSingleNode("//login/EptProfileID") != null)
                    int.TryParse(xd.SelectSingleNode("//login/EptProfileID").InnerXml.Trim(), out eptProfileID);

                // Fetch the conf unique name since thats the unique identifier of the conf.
				NS_MESSENGER.Conference conf = new NS_MESSENGER.Conference();
				NS_DATABASE.Database db = new NS_DATABASE.Database(configParams);
				conf.iDbID = confId; conf.iInstanceID = instanceId;
				ret = db.FetchConf(ref conf);
				if (!ret) 
				{
					logger.Exception(200,"Database fetch operation failed.");
					return (false);
				}

                //FB 2530 Start

                #region Mute Party
                if (muteAll == (int)eMuteType.PARTY_MUTE) //FB 2530
                {
                    // Retreive the bridge info for that endpoint.
                    NS_MESSENGER.Party party = new NS_MESSENGER.Party();
                    switch (terminalType)
                    {
                        case 1:
                            {
                                // user
                                db.eptProfileID = eptProfileID; //ALLDEV-814
                                bool ret2 = db.FetchUser(confId, instanceId, endpointId, ref party);
                                break;
                            }
                        case 2:
                            {
                                // room
                                logger.Trace("In room...");
                                bool ret2 = db.FetchRoom(confId, instanceId, endpointId, ref party);
                                break;
                            }
                        case 3:
                            {
                                // guest
                                bool ret2 = db.FetchGuest(confId, instanceId, endpointId, ref party);
                                break;
                            }
                        case 4:
                            {
                                // cascade
                                bool ret2 = db.FetchCascadeLink(confId, instanceId, endpointId, ref party);
                                break;
                            }
                    }

                    // connect to the bridge and perform the operation.
                    bool ret3 = false;

                    //FB 2584 Start

                    List<NS_MESSENGER.Party> parties = TelepresenceEndpoint(party);

                    for (int i = 0; i < parties.Count; i++)
                    {
                        if (party.cMcu.etType == NS_MESSENGER.MCU.eType.ACCORDv6 || party.cMcu.etType == NS_MESSENGER.MCU.eType.ACCORDv7 || party.cMcu.etType == NS_MESSENGER.MCU.eType.RMX)
                        {
                            //NS_POLYCOM.Polycom accord = new NS_POLYCOM.Polycom(configParams);
                            ret3 = polycom.OngoingConfOps("MuteEndpoint", conf, party, ref msg, mute, 0, 0, false, ref party.etStatus, false, false); //FB 2553-RMX
                            if (!ret3)
                            {
                                this.errMsg = polycom.errMsg;
                                msg = polycom.errMsg;
                            }
                        }
                        else
                        {
                            if (party.cMcu.etType == NS_MESSENGER.MCU.eType.CODIAN)
                            {
                                NS_CODIAN.Codian codian = new NS_CODIAN.Codian(configParams);
                                ret3 = codian.MuteEndpoint(conf, party, mute);
                                if (!ret3)
                                {
                                    this.errMsg = codian.errMsg;
                                }
                            }
                            //FB 2501 Call Monitoring Start
                            else if (party.cMcu.etType == NS_MESSENGER.MCU.eType.CISCOTP)
                            {
                                NS_CiscoTPServer.CiscoTPServer CiscoTPServer = new NS_CiscoTPServer.CiscoTPServer(configParams);
                                ret3 = CiscoTPServer.MuteAudioReceiveEndpoint(conf, party, mute);
                                if (!ret3)
                                {
                                    this.errMsg = CiscoTPServer.errMsg;
                                }
                            }
                            //FB 2501 Call Monitoring End
                            else
                            {
                                if (party.cMcu.etType == NS_MESSENGER.MCU.eType.TANDBERG)
                                {
                                    NS_TANDBERG.Tandberg tandberg = new NS_TANDBERG.Tandberg(configParams);
                                    ret3 = tandberg.MuteEndpoint(conf, party, mute);
                                    if (!ret3)
                                    {
                                        this.errMsg = tandberg.errMsg;
                                    }
                                }
								//FB 2441 Starts
                                if (party.cMcu.etType == NS_MESSENGER.MCU.eType.RPRM)
                                {
                                    NS_POLYCOMRPRM.POLYCOMRPRM polycomRPRM = new NS_POLYCOMRPRM.POLYCOMRPRM(configParams);
                                    ret3 = polycomRPRM.MuteEndpoint(conf, party, mute, (int)AudioVideoParty.Audio);
                                    if (!ret3)
                                    {
                                        this.errMsg = polycomRPRM.errMsg;
                                    }
                                }
								//FB 2441 Ends
                                //FB 2556 Starts
                                if (party.cMcu.etType == NS_MESSENGER.MCU.eType.RADVISIONIVIEW)
                                {
                                    NS_RADVISION_IView.RadVisionIView RadIview = new NS_RADVISION_IView.RadVisionIView(configParams);
                                    ret3 = RadIview.MuteEndpoint(conf, party, mute);
                                    if (!ret3)
                                    {
                                        this.errMsg = RadIview.errMsg;
                                    }
                                }
                                //FB 2556 Ends
                                else if (party.cMcu.etType == NS_MESSENGER.MCU.eType.RADVISION) // ZD 101157
                                {
                                    NS_RADVISION.Radvision rad = new NS_RADVISION.Radvision(configParams);
                                    ret3 = rad.MuteEndpoint(conf, party, mute);
                                    if (!ret3)
                                    {
                                        this.errMsg = rad.errMsg;
                                    }
                                }
                                else if (party.cMcu.etType == NS_MESSENGER.MCU.eType.Pexip) // ZD 101217
                                {
                                    NS_Pexip.pexip Pexip = new NS_Pexip.pexip(configParams);
                                    ret3 = Pexip.MuteEndpoint(conf, party, mute);
                                    if (!ret3)
                                    {
                                        this.errMsg = Pexip.errMsg;
                                    }
                                }
                            }

                        }
                    }
                    //FB 2584 End
                    if (!ret3)
                    {
                        logger.Trace("Mute operation failed for endpoint.");
                        //FB 2569 Start
                        alert = new NS_MESSENGER.Alert();
                        alert.confID = conf.iDbID;
                        alert.instanceID = conf.iInstanceID;
                        alert.message = this.errMsg;
                        alert.typeID = 9; //Endpoint mute/unmute
                        bool retp2p = db.InsertConfAlert(alert);
                        if (!retp2p)
                            logger.Trace("Endpoint mute/unmute alert insert failed.");
                        //FB 2569 End
                        return false;
                    }
                }
                #endregion

                #region Mute all Party
                else
                {
                    #region Fetch the endpoints
                    ret = false;
                    ret = db.FetchRooms(conf.iDbID, conf.iInstanceID, ref conf.qParties, 0);
                    if (!ret)
                    {
                        logger.Exception(100, "Error in fetching rooms from db.");
                        return false;
                    }
                    ret = false;
                    ret = db.FetchUsers(conf.iDbID, conf.iInstanceID, ref conf.qParties, 0);
                    if (!ret)
                    {
                        logger.Exception(100, "Error in fetching users from db.");
                        return false;
                    }
                    ret = false;
                    ret = db.FetchGuests(conf.iDbID, conf.iInstanceID, ref conf.qParties, 0);
                    if (!ret)
                    {
                        logger.Exception(100, "Error in fetching guests from db.");
                        return false;
                    }
                    ret = false;
                    Queue cascadeLinks = new Queue();
                    ret = db.FetchCascadeLinks(conf.iDbID, conf.iInstanceID, ref cascadeLinks, 0);
                    if (!ret)
                    {
                        logger.Exception(100, "Error in fetching cascade links from db.");
                        return false;
                    }
                    #endregion

                    logger.Trace(" Entering Party Mute All ...");

                    // Fetch all the bridge ids on which this conf is running.
                    System.Collections.IEnumerator partyEnumerator;
                    partyEnumerator = conf.qParties.GetEnumerator();
                    int partyCount = conf.qParties.Count;
                    logger.Trace("Party Count = " + partyCount.ToString());

                    Queue mcuIdList = new Queue();
                    bool ret3 = false;
                    for (int i = 0; i < partyCount; i++)
                    {
                        ret3 = partyEnumerator.MoveNext();
                        if (!ret3) break;

                        NS_MESSENGER.Party party = new NS_MESSENGER.Party();
                        party = (NS_MESSENGER.Party)partyEnumerator.Current;

                        logger.Trace("Party Address : " + party.sAddress.ToString());

                        if (party.cMcu.etType == NS_MESSENGER.MCU.eType.ACCORDv6 || party.cMcu.etType == NS_MESSENGER.MCU.eType.ACCORDv7 || party.cMcu.etType == NS_MESSENGER.MCU.eType.RMX)
                        {
                            ret3 = polycom.OngoingConfOps("MuteEndpoint", conf, party, ref msg, mute, 0, 0, false, ref party.etStatus, false, false);//FB 2553-RMX
                            if (!ret3)
                            {
                                this.errMsg = polycom.errMsg;
                                msg = polycom.errMsg;
                            }
                        }
                        else
                        {
                            if (party.cMcu.etType == NS_MESSENGER.MCU.eType.CODIAN)
                            {
                                NS_CODIAN.Codian codian = new NS_CODIAN.Codian(configParams);
                                ret3 = codian.MuteEndpoint(conf, party, mute);
                                if (!ret3)
                                {
                                    this.errMsg = codian.errMsg;
                                }
                            }
                            //FB 2501 Call Monitoring Start
                            else if (party.cMcu.etType == NS_MESSENGER.MCU.eType.CISCOTP)
                            {
                                NS_CiscoTPServer.CiscoTPServer CiscoTPServer = new NS_CiscoTPServer.CiscoTPServer(configParams);
                                ret3 = CiscoTPServer.MuteAudioReceiveEndpoint(conf, party, mute);
                                if (!ret3)
                                {
                                    this.errMsg = CiscoTPServer.errMsg;
                                }
                            }
                            //FB 2501 Call Monitoring End
                            //FB 2556
                            else if (party.cMcu.etType == NS_MESSENGER.MCU.eType.RADVISIONIVIEW)
                            {
                                NS_RADVISION_IView.RadVisionIView RadIview = new NS_RADVISION_IView.RadVisionIView(configParams);
                                ret3 = RadIview.MuteAllEndpoint(conf, mute);
                                if (!ret3)
                                {
                                    this.errMsg = RadIview.errMsg;
                                    return (false);
                                }
                                return (true);
                            }
                            //FB 2556
                            //FB 2441 Starts
                            if (party.cMcu.etType == NS_MESSENGER.MCU.eType.RPRM)
                            {
                                NS_POLYCOMRPRM.POLYCOMRPRM polycomRPRM = new NS_POLYCOMRPRM.POLYCOMRPRM(configParams);
                                ret3 = polycomRPRM.MuteEndpoint(conf, party, mute, (int)AudioVideoParty.Audio);
                                if (!ret3)
                                {
                                    this.errMsg = polycomRPRM.errMsg;
                                }
                            }
                            //FB 2441 Ends
                            else if (party.cMcu.etType == NS_MESSENGER.MCU.eType.Pexip) // ZD 101217
                            {
                                NS_Pexip.pexip Pexip = new NS_Pexip.pexip(configParams);
                                ret3 = Pexip.MuteEndpoint(conf, party, mute);
                                if (!ret3)
                                {
                                    this.errMsg = Pexip.errMsg;
                                }
                            }
                            else
                            {
                                if (party.cMcu.etType == NS_MESSENGER.MCU.eType.TANDBERG)
                                {
                                    NS_TANDBERG.Tandberg tandberg = new NS_TANDBERG.Tandberg(configParams);
                                    ret3 = tandberg.MuteEndpoint(conf, party, mute);
                                    if (!ret3)
                                    {
                                        this.errMsg = tandberg.errMsg;
                                    }
                                }
                                else
                                {
                                    NS_RADVISION.Radvision rad = new NS_RADVISION.Radvision(configParams);
                                    ret3 = rad.MuteEndpoint(conf, party, mute);
                                    if (!ret3)
                                    {
                                        this.errMsg = rad.errMsg;
                                    }
                                }

                            }
                            if (!ret3)
                            {
                                logger.Trace("Mute All operation failed.");
                                //FB 2569 Start
                                alert = new NS_MESSENGER.Alert();
                                alert.confID = conf.iDbID;
                                alert.instanceID = conf.iInstanceID;
                                alert.message = this.errMsg;
                                alert.typeID = 9; //Endpoint mute/unmute
                                bool retp2p = db.InsertConfAlert(alert);
                                if (!retp2p)
                                    logger.Trace("Endpoint mute/unmute alert insert failed.");
                                //FB 2569 End
                                //return false; commented for FB 2501 
                            }
                        }
                        logger.Trace("Leaving Mute All operation.");
                    }
                }
                #endregion

                //FB 2350 End

                return (true);
			}
			catch (Exception e)
			{
				logger.Exception(100,e.Message);
				return (false);
			}
        }
        #endregion

        //FB 2441 Starts
        #region MuteUnMuteParties (muteAllExcept & unMuteAll)
        /// <summary>
        /// MuteUnMuteParties (muteAllExcept & unMuteAll)
        /// </summary>
        /// <param name="inXml"></param>
        /// <param name="outXml"></param>
        /// <param name="msg"></param>
        /// <returns></returns>
        internal bool MuteUnMuteParties(string inXml, ref string outXml, ref string msg)
        {
            try
            {
                logger.Trace("Entering the MuteAllParties function...");
                List<NS_MESSENGER.Party> partyList = new List<NS_MESSENGER.Party>();
                NS_MESSENGER.Party party = new NS_MESSENGER.Party();
                int endpointId = 0, terminalType = 0;
                XmlDocument xd = new XmlDocument();
                bool bRet = false;
                List<int> mcuid = null;
                xd.LoadXml(inXml);

                string login = xd.SelectSingleNode("//login/userID").InnerXml.Trim();

                string strConfId = xd.SelectSingleNode("//login/confID").InnerXml.Trim();
                int confId = 0, instanceId = 0;
                bool ret = ExtractConfInstanceIds(strConfId, ref confId, ref instanceId);
                if (!ret)
                {
                    logger.Trace("Invalid conf/instance id.");
                    return false;
                }

                int iMute = Int32.Parse(xd.SelectSingleNode("//login/mode").InnerXml.Trim()); //0-UnMuteAllParties 1-MuteAllPartiesExcept
                bool mute = false;
                if (iMute == 1)
                {
                    mute = true;
                }

                NS_MESSENGER.Conference conf = new NS_MESSENGER.Conference();
                NS_DATABASE.Database db = new NS_DATABASE.Database(configParams);
                conf.iDbID = confId; conf.iInstanceID = instanceId;
                ret = db.FetchConf(ref conf);
                if (!ret)
                {
                    logger.Exception(200, "Database fetch operation failed.");
                    return (false);
                }
                ret = db.FetchConfbridID(conf,ref mcuid);
                if (!ret)
                {
                    logger.Exception(200, "Database fetch operation failed.");
                    return (false);
                }

                if (mute)
                {
                    XmlNodeList nodeList = xd.SelectNodes("//login/endpoints/endpoint");

                    foreach (XmlNode node in nodeList)
                    {
                        endpointId = 0; terminalType = 0;
                        party = new NS_MESSENGER.Party();

                        if (node.SelectSingleNode("endpointId") != null)
                            Int32.TryParse(node.SelectSingleNode("endpointId").InnerText, out endpointId);

                        if (node.SelectSingleNode("terminalType") != null)
                            Int32.TryParse(node.SelectSingleNode("terminalType").InnerText, out terminalType);  // 1 = user , 2 = room, 3 = guest , 4 = cascade				

                        logger.Trace("Enpoint = " + endpointId);
                        logger.Trace("Terminaltype = " + terminalType);

                        switch (terminalType)
                        {
                            case 1:
                                {
                                    bRet = db.FetchUser(confId, instanceId, endpointId, ref party);
                                    break;
                                }
                            case 2:
                                {
                                    logger.Trace("In room...");
                                    bRet = db.FetchRoom(confId, instanceId, endpointId, ref party);
                                    break;
                                }
                            case 3:
                                {
                                    bRet = db.FetchGuest(confId, instanceId, endpointId, ref party);
                                    break;
                                }
                           
                        }

                        partyList.Add(party);
                    }
                }
                for (int i = 0; i < mcuid.Count; i++)
                {
                    ret = false;
                    ret = db.FetchMcu(mcuid[i], ref conf.cMcu);
                    if (!ret)
                    {
                        logger.Exception(200, "Database fetch operation failed.");
                        return (false);
                    }
                    else
                    {
                        if (conf.cMcu.etType == NS_MESSENGER.MCU.eType.RPRM)
                        {
                            NS_POLYCOMRPRM.POLYCOMRPRM polycomRPRM = new NS_POLYCOMRPRM.POLYCOMRPRM(configParams);
                            bRet = polycomRPRM.MuteUnMuteParties(conf, partyList, mute);//Mode 0-UnMuteAllParties; Mode 1-MuteAllPartiesExcept
                            if (!bRet)
                            {
                                this.errMsg = polycomRPRM.errMsg;
                            }
                        }
                    }
                }
                if (!bRet)
                {
                    logger.Trace("MuteUnMuteParties command is failed.");
                    return false;
                }
                if (bRet)
                {
                    db.UpdatePartyMuteStatus(conf, partyList, iMute);//iMute 0-UnMuteAllParties; iMute 1-MuteAllPartiesExcept
                }

                return (true);
            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
                return (false);
            }
        }
        #endregion

        //FB 2249 start
        #region ModifyEndpoint
        /// <summary>
        /// ModifyEndpoint an ongoing endpoint.
        /// </summary>
        /// <param name="inXml"></param>
        /// <param name="outXml"></param>
        /// <param name="msg"></param>
        /// <returns></returns>
        internal bool ModifyEndpoint(string inXml, ref string outXml, ref string msg)
        {
            try
            {
                NS_MESSENGER.Conference conf = new NS_MESSENGER.Conference();               
                NS_MESSENGER.Party party = new NS_MESSENGER.Party();
                NS_DATABASE.Database db = new NS_DATABASE.Database(configParams); //FB 2569
                if (FetchTerminal(ref inXml, ref conf, ref party))
                {
                    // connect to the bridge and perform the operation.
                    bool ret3 = false;

                    //FB 2584 Start

                    List<NS_MESSENGER.Party> parties = TelepresenceEndpoint(party);

                    for (int i = 0; i < parties.Count; i++)
                    {
                        //if (party.cMcu.etType == NS_MESSENGER.MCU.eType.ACCORDv6 || party.cMcu.etType == NS_MESSENGER.MCU.eType.ACCORDv7 || party.cMcu.etType == NS_MESSENGER.MCU.eType.RMX)
                        //{
                        //    //NS_POLYCOM.Polycom accord = new NS_POLYCOM.Polycom(configParams);
                        //    ret3 = polycom.OngoingConfOps("ModifyEndpoint", conf, party, ref msg, mute, 0, 0, false, ref party.etStatus);
                        //    if (!ret3)
                        //    {
                        //        this.errMsg = polycom.errMsg;
                        //        msg = polycom.errMsg;
                        //    }
                        //}
                        //else
                        //{
                        if (party.cMcu.etType == NS_MESSENGER.MCU.eType.CODIAN)
                        {
                            NS_CODIAN.Codian codian = new NS_CODIAN.Codian(configParams);
                            ret3 = codian.ModifyEndpoint(conf, party);
                            if (!ret3)
                            {
                                this.errMsg = codian.errMsg;
                            }
                        }
                        //FB 2501 Call Monitoring Start
                        else if (party.cMcu.etType == NS_MESSENGER.MCU.eType.CISCOTP)
                        {
                            NS_CiscoTPServer.CiscoTPServer CiscoTPServer = new NS_CiscoTPServer.CiscoTPServer(configParams);
                            ret3 = CiscoTPServer.ModifyEndpoint(conf, party);
                            if (!ret3)
                            {
                                this.errMsg = CiscoTPServer.errMsg;
                            }
                        }
                        //FB 2501 Call Monitoring End
                        //else
                        //{
                        //    if (party.cMcu.etType == NS_MESSENGER.MCU.eType.TANDBERG)
                        //    {
                        //        NS_TANDBERG.Tandberg tandberg = new NS_TANDBERG.Tandberg(configParams);
                        //        ret3 = tandberg.MuteEndpoint(conf, party, mute);
                        //        if (!ret3)
                        //        {
                        //            this.errMsg = tandberg.errMsg;
                        //        }
                        //    }
                        //    else
                        //    {
                        //        NS_RADVISION.Radvision rad = new NS_RADVISION.Radvision(configParams);
                        //        ret3 = rad.MuteEndpoint(conf, party, mute);
                        //        if (!ret3)
                        //        {
                        //            this.errMsg = rad.errMsg;
                        //        }
                        //    }
                        //}

                        //}
                    }
                    //FB 2584 End
                    if (!ret3)
                    {
                        logger.Trace("Edit operation failed for endpoint.");
                        //FB 2569 Start
                        alert = new NS_MESSENGER.Alert();
                        alert.confID = conf.iDbID;
                        alert.instanceID = conf.iInstanceID;
                        alert.message = this.errMsg;
                        alert.typeID = 3; //modify party
                        bool retp2p = db.InsertConfAlert(alert);
                        if (!retp2p)
                            logger.Trace("modify party alert insert failed.");
                        //FB 2569 End
                        return false;
                    }
                }
                else
                {
                    logger.Trace("Edit operation failed for endpoint.");
                    return false;
                }


                return (true);
            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
                return (false);
            }
        }
        #endregion

        #region FetchTerminal ( Conference's Party)
        /// <summary>
        /// FetchTerminal
        /// </summary>
        /// <param name="inXml"></param>
        /// <param name="conf"></param>
        /// <param name="party"></param>
        /// <returns></returns>
        private bool FetchTerminal(ref string inXml, ref NS_MESSENGER.Conference conf, ref NS_MESSENGER.Party party)
        {
            bool ret2 = true;
            try
            {
                string login = "11", strConfId="";
                int endpointId = 0,terminalType = 0;

                logger.Trace("Entering the function...");
                // Load the string into xml parser and retrieve the tag values.
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(inXml);

                if(xd.SelectSingleNode("//login/userID") != null)
                  if (xd.SelectSingleNode("//login/userID").ToString().Trim() != "")
                   login = xd.SelectSingleNode("//login/userID").InnerXml.Trim();

                if (xd.SelectSingleNode("//login/confID") != null)
                    if (xd.SelectSingleNode("//login/confID").ToString().Trim() != "")
                     strConfId = xd.SelectSingleNode("//login/confID").InnerXml.Trim();


                int confId = 0, instanceId = 0;
                bool ret = ExtractConfInstanceIds(strConfId, ref confId, ref instanceId);
                if (!ret)
                {
                    logger.Trace("Invalid conf/instance id.");
                    return false;
                }
                logger.Trace("strConfId = " + strConfId.ToString());
                if (xd.SelectSingleNode("//login/endpointID") != null)
                    if (xd.SelectSingleNode("//login/endpointID").ToString().Trim() != "")
                         Int32.TryParse(xd.SelectSingleNode("//login/endpointID").InnerXml.Trim(),out endpointId); // database id
                logger.Trace("endpointId = " + endpointId.ToString());

                if (xd.SelectSingleNode("//login/terminalType") != null)
                     if (xd.SelectSingleNode("//login/terminalType").ToString().Trim() != "")
                         Int32.TryParse(xd.SelectSingleNode("//login/terminalType").InnerXml.Trim(), out terminalType); // 1 = user , 2 = room, 3 = guest , 4 = cascade				

                logger.Trace("Terminaltype = " + terminalType.ToString());

                // Fetch the conf unique name since thats the unique identifier of the conf.
                NS_DATABASE.Database db = new NS_DATABASE.Database(configParams);

                conf.iDbID = confId; conf.iInstanceID = instanceId;
                ret = db.FetchConf(ref conf);
                if (!ret)
                {
                    logger.Exception(200, "Database fetch operation failed.");
                    return (false);
                }

                // Retreive the bridge info for that endpoint.
                switch (terminalType)
                {
                    case 1:
                        {
                            // user
                            ret2 = db.FetchUser(confId, instanceId, endpointId, ref party);
                            break;
                        }
                    case 2:
                        {
                            // room
                            logger.Trace("In room...");
                            ret2 = db.FetchRoom(confId, instanceId, endpointId, ref party);
                            break;
                        }
                    case 3:
                        {
                            // guest
                            ret2 = db.FetchGuest(confId, instanceId, endpointId, ref party);
                            break;
                        }
                    case 4:
                        {
                            // cascade
                            ret2 = db.FetchCascadeLink(confId, instanceId, endpointId, ref party);
                            break;
                        }
                }
            }
            catch (Exception ex)
            {
                logger.Exception(100, ex.Message);
                return (false);
            }

            return ret2;
        }
        #endregion

        //FB 2249 end

        //FB 3073 Starts
        #region ChangeDisplayLayout
        /// <summary>
		/// Change display layout of an ongoing endpoint on bridge.
		/// </summary>
		/// <param name="inXml"></param>
		/// <param name="outXml"></param>
		/// <param name="msg"></param>
		/// <returns></returns>
        internal bool ChangeDisplayLayout(string inXml, ref string outXml, ref string msg)
        {
            try
            {
                logger.Trace("Entering the ChangeDisplayLayout function...");
                List<int> mcuid = null;
                bool ret3 = false;

                XmlDocument xd = new XmlDocument();
                xd.LoadXml(inXml);

                int login = 0;
                if (xd.SelectSingleNode("//login/userID") != null)
                    int.TryParse(xd.SelectSingleNode("//login/userID").InnerText.Trim(), out login);

                string strConfId = "";
                if (xd.SelectSingleNode("//login/confID") != null)
                    strConfId = xd.SelectSingleNode("//login/confID").InnerXml.Trim();

                int confId = 0, instanceId = 0;
                bool ret = ExtractConfInstanceIds(strConfId, ref confId, ref instanceId);
                if (!ret)
                {
                    logger.Trace("Invalid conf/instance id.");
                    return false;
                }

                int endpointId = 0;
                if (xd.SelectSingleNode("//login/endpointID") != null)
                    int.TryParse(xd.SelectSingleNode("//login/endpointID").InnerText.Trim(), out endpointId);
                logger.Trace("EndpointId = " + endpointId.ToString());

                int terminalType = 0;
                if (xd.SelectSingleNode("//login/terminalType") != null)
                    int.TryParse(xd.SelectSingleNode("//login/terminalType").InnerXml.Trim(), out terminalType); // 1 = user , 2 = room, 3 = guest				 
                logger.Trace("Terminaltype = " + terminalType.ToString());

                int displayLayout = 0;
                if (xd.SelectSingleNode("//login/displayLayout") != null)
                    int.TryParse(xd.SelectSingleNode("//login/displayLayout").InnerXml.Trim(), out displayLayout); // display layout code;

                //FB 2530 Start
                int displayLayoutAll = 0;
                if (xd.SelectSingleNode("//login/displayLayoutAll") != null)
                    int.TryParse(xd.SelectSingleNode("//login/displayLayoutAll").InnerXml.Trim(), out displayLayoutAll); // 1 = ConferenceandAllPartiesLayout , 2 = ParticipantLayout
                //FB 2530 End

                // Fetch the conf unique name since thats the unique identifier of the conf.
                NS_DATABASE.Database db = new NS_DATABASE.Database(configParams);
                NS_MESSENGER.Conference conf = new NS_MESSENGER.Conference();
                conf.iDbID = confId;
                conf.iInstanceID = instanceId;
                ret = db.FetchConf(ref conf);
                if (!ret)
                {
                    logger.Exception(200, "Database fetch operation failed.");
                    return (false);
                }

                #region Fetch the endpoints
                ret = false;
                ret = db.FetchRooms(conf.iDbID, conf.iInstanceID, ref conf.qParties, 0);
                if (!ret)
                {
                    logger.Exception(100, "Error in fetching rooms from db.");
                    return false;
                }
                ret = false;
                ret = db.FetchUsers(conf.iDbID, conf.iInstanceID, ref conf.qParties, 0);
                if (!ret)
                {
                    logger.Exception(100, "Error in fetching users from db.");
                    return false;
                }
                ret = false;
                ret = db.FetchGuests(conf.iDbID, conf.iInstanceID, ref conf.qParties, 0);
                if (!ret)
                {
                    logger.Exception(100, "Error in fetching guests from db.");
                    return false;
                }
                ret = false;
                Queue cascadeLinks = new Queue();
                ret = db.FetchCascadeLinks(conf.iDbID, conf.iInstanceID, ref cascadeLinks, 0);
                if (!ret)
                {
                    logger.Exception(100, "Error in fetching cascade links from db.");
                    return false;
                }
                #endregion

                #region Conference and All Parties Layout
                if (displayLayoutAll == (int)eLayoutType.CONF_AllPARTY_LAYOUT) //FB 2350
                {
                    // Conference and All Parties display layout..
                    logger.Trace("Changing the Conference and All Parties display layout...");

                    // connect to all the bridges and change the layout of the conference, one mcu at a time.
                    NS_MESSENGER.MCU mcu = new NS_MESSENGER.MCU();
                    ret = db.FetchConfbridID(conf, ref mcuid);
                    int mcuId = 0;

                    #region ConferenceLayout
                    for (int k = 0; k < mcuid.Count; k++)
                    {
                        mcuId = mcuid[k];

                        mcu = new NS_MESSENGER.MCU();
                        ret3 = db.FetchMcu(mcuId, ref mcu);
                        if (!ret3)
                        {
                            logger.Trace("Error fetching mcu details.");
                            continue;
                        }

                        ret3 = false;
                        if (mcu.etType == NS_MESSENGER.MCU.eType.ACCORDv6 || mcu.etType == NS_MESSENGER.MCU.eType.ACCORDv7 || mcu.etType == NS_MESSENGER.MCU.eType.RMX)
                        {
                            NS_MESSENGER.Party dummyParty = new NS_MESSENGER.Party();
                            conf.cMcu = mcu; dummyParty.cMcu = mcu;
                            logger.Trace("Entering Accord sub-system...");
                            ret3 = polycom.OngoingConfOps("ChangeConferenceDisplayLayout", conf, dummyParty, ref msg, false, displayLayout, 0, false, ref dummyParty.etStatus, false, false); //FB 2553-RMX
                            if (!ret3)
                            {
                                msg = polycom.errMsg;
                                this.errMsg = polycom.errMsg;
                            }
                        }
                        // FB 2441 Starts
                        else if (mcu.etType == NS_MESSENGER.MCU.eType.RPRM) //FB 2553
                        {
                            NS_POLYCOMRPRM.POLYCOMRPRM polycomRPRM = new NS_POLYCOMRPRM.POLYCOMRPRM(configParams);
                            conf.cMcu = mcu;
                            logger.Trace("Entering Polcom RPRM...");
                            ret3 = polycomRPRM.OngoingConfOps("ChangeConferenceDisplayLayout", conf, displayLayout);
                            if (!ret3)
                            {
                                msg = polycomRPRM.errMsg;
                                this.errMsg = polycomRPRM.errMsg;
                            }
                        }
                        // FB 2441 Ends
                        else
                        {
                            if (mcu.etType == NS_MESSENGER.MCU.eType.CODIAN)
                            {
                                NS_CODIAN.Codian codian = new NS_CODIAN.Codian(configParams);
                                NS_MESSENGER.Party dummyParty = new NS_MESSENGER.Party();
                                conf.cMcu = mcu; dummyParty.cMcu = mcu;
                                logger.Trace("Entering Codian sub-system...");
                                ret3 = codian.ChangeConfDisplayLayout(conf, dummyParty, displayLayout);
                                if (!ret3)
                                {
                                    this.errMsg = codian.errMsg;
                                }
                            }
                            else if (mcu.etType == NS_MESSENGER.MCU.eType.TANDBERG) //FB 2553
                            {
                                NS_TANDBERG.Tandberg tandberg = new NS_TANDBERG.Tandberg(configParams);
                                ret3 = tandberg.ChangeConfDisplayLayout(conf, displayLayout);
                                if (!ret3)
                                {
                                    this.errMsg = tandberg.errMsg;
                                }
                            }
                        }
                        if (!ret3)
                        {
                            logger.Trace("Change conf display layout operation failed.");
                            logger.Trace("Msg = " + this.errMsg);
                            //FB 2569 Start
                            alert = new NS_MESSENGER.Alert();
                            alert.confID = conf.iDbID;
                            alert.instanceID = conf.iInstanceID;
                            alert.message = this.errMsg;
                            alert.typeID = 5; //conference Layout
                            bool retp2p = db.InsertConfAlert(alert);
                            if (!retp2p)
                                logger.Trace("conference Layout alert insert failed.");
                            //FB 2569 End
                            //return false; //FB 3073
                        }
                    }
                    #endregion

                    #region AllPartiesLayout

                    System.Collections.IEnumerator partiesEnumerator;
                    partiesEnumerator = conf.qParties.GetEnumerator();
                    int partyCount = conf.qParties.Count;

                    for (int i = 0; i < partyCount; i++)
                    {
                        ret3 = false;
                        ret3 = partiesEnumerator.MoveNext();
                        if (!ret3) continue;

                        NS_MESSENGER.Party party = new NS_MESSENGER.Party();
                        party = (NS_MESSENGER.Party)partiesEnumerator.Current;
                        if (party.cMcu.etType == NS_MESSENGER.MCU.eType.ACCORDv6 || party.cMcu.etType == NS_MESSENGER.MCU.eType.ACCORDv7 || party.cMcu.etType == NS_MESSENGER.MCU.eType.RMX)
                        {

                            logger.Trace("Entering Accord sub-system...");
                            ret3 = polycom.OngoingConfOps("ChangePartyDisplayLayout", conf, party, ref msg, true, displayLayout, 0, false, ref party.etStatus, false, false); //FB 2553-RMX
                            if (!ret3)
                            {
                                this.errMsg = polycom.errMsg;
                            }
                        }
                        else
                        {
                            if (party.cMcu.etType == NS_MESSENGER.MCU.eType.CODIAN)
                            {
                                NS_CODIAN.Codian codian = new NS_CODIAN.Codian(configParams);
                                logger.Trace("Entering Codian sub-system...");
                                ret3 = codian.ChangePartyDisplayLayout(conf, party, displayLayout);
                                if (!ret3)
                                {
                                    this.errMsg = codian.errMsg;
                                }
                            }
                            else if (party.cMcu.etType == NS_MESSENGER.MCU.eType.TANDBERG) //ZD 102531
                            {
                                NS_TANDBERG.Tandberg tandberg = new NS_TANDBERG.Tandberg(configParams);
                                ret3 = tandberg.ChangePartyDisplayLayout(conf, party, displayLayout);
                                if (!ret3)
                                {
                                    this.errMsg = tandberg.errMsg;
                                }
                            }
                        }
                        if (!ret3)
                        {
                            logger.Trace("Change display layout operation failed for endpoint.");
                            //FB 2569 Start
                            alert = new NS_MESSENGER.Alert();
                            alert.confID = conf.iDbID;
                            alert.instanceID = conf.iInstanceID;
                            alert.message = this.errMsg;
                            alert.typeID = 8; //Endpoint Layout
                            bool retp2p = db.InsertConfAlert(alert);
                            if (!retp2p)
                                logger.Trace("Endpoint Layout alert insert failed.");
                            //FB 2569 End
                            //return false; commented for FB 2501 
                        }
                    }
                    #endregion
                }
                #endregion

                #region Party Layout
                else if (displayLayoutAll == (int)eLayoutType.PARTY_LAYOUT)//FB 2350
                {
                    // Display layout for only one participant needs to be changed.

                    NS_MESSENGER.Party party = new NS_MESSENGER.Party();
                    switch (terminalType)
                    {
                        case 1:
                            {
                                // user
                                bool ret2 = db.FetchUser(confId, instanceId, endpointId, ref party);
                                break;
                            }
                        case 2:
                            {
                                // room
                                logger.Trace("In room...");
                                bool ret2 = db.FetchRoom(confId, instanceId, endpointId, ref party);
                                break;
                            }
                        case 3:
                            {
                                // guest
                                bool ret2 = db.FetchGuest(confId, instanceId, endpointId, ref party);
                                break;
                            }
                    }
                    //FB 2584 Start
                    ret3 = false;
                    List<NS_MESSENGER.Party> parties = TelepresenceEndpoint(party);

                    for (int i = 0; i < parties.Count; i++)
                    {
                        if (party.cMcu.etType == NS_MESSENGER.MCU.eType.ACCORDv6 || party.cMcu.etType == NS_MESSENGER.MCU.eType.ACCORDv7 || party.cMcu.etType == NS_MESSENGER.MCU.eType.RMX)
                        {

                            logger.Trace("Entering Accord sub-system...");
                            ret3 = polycom.OngoingConfOps("ChangePartyDisplayLayout", conf, party, ref msg, true, displayLayout, 0, false, ref party.etStatus, false, false); //FB 2553-RMX
                            if (!ret3)
                            {
                                this.errMsg = polycom.errMsg;
                            }
                        }
                        else
                        {
                            if (party.cMcu.etType == NS_MESSENGER.MCU.eType.CODIAN)
                            {
                                NS_CODIAN.Codian codian = new NS_CODIAN.Codian(configParams);
                                logger.Trace("Entering Codian sub-system...");
                                ret3 = codian.ChangePartyDisplayLayout(conf, party, displayLayout);
                                if (!ret3)
                                {
                                    this.errMsg = codian.errMsg;
                                }
                            }
                            else
                            {
                                NS_TANDBERG.Tandberg tandberg = new NS_TANDBERG.Tandberg(configParams);
                                ret3 = tandberg.ChangePartyDisplayLayout(conf, party, displayLayout);
                                if (!ret3)
                                {
                                    this.errMsg = tandberg.errMsg;
                                }
                            }
                        }
                    }
                    //FB 2584 End
                    if (!ret3)
                    {
                        logger.Trace("Change display layout operation failed for endpoint.");
                        //FB 2569 Start
                        alert = new NS_MESSENGER.Alert();
                        alert.confID = conf.iDbID;
                        alert.instanceID = conf.iInstanceID;
                        alert.message = this.errMsg;
                        alert.typeID = 8; //Endpoint Layout
                        bool retp2p = db.InsertConfAlert(alert);
                        if (!retp2p)
                            logger.Trace("Endpoint Layout alert insert failed.");
                        //FB 2569 End
                        return false;
                    }
                }
                #endregion

                //Commented for FB 3073
                #region Partyall Layout

                /*
                //FB 2350 Start
                else if (displayLayoutAll == (int)eLayoutType.PARTYALL_LAYOUT)
                {
                    logger.Trace("Changing the display layout All code...");

                    // Fetch all the bridge ids on which this conf is running.
                    System.Collections.IEnumerator partyEnumerator;
                    partyEnumerator = conf.qParties.GetEnumerator();
                    int partyCount = conf.qParties.Count;
                    logger.Trace("Party Count = " + partyCount.ToString());
                   
                    Queue mcuIdList = new Queue();
                    
                    bool ret3 = false;
                    for (int i = 0; i < partyCount; i++)
                    {
                        partyEnumerator.MoveNext();
                        //if (!ret3) break;

                        NS_MESSENGER.Party party = new NS_MESSENGER.Party();
                        party = (NS_MESSENGER.Party)partyEnumerator.Current;
                        if (party.cMcu.etType == NS_MESSENGER.MCU.eType.ACCORDv6 || party.cMcu.etType == NS_MESSENGER.MCU.eType.ACCORDv7 || party.cMcu.etType == NS_MESSENGER.MCU.eType.RMX)
                        {

                            logger.Trace("Entering Accord sub-system...");
                            ret3 = polycom.OngoingConfOps("ChangePartyDisplayLayout", conf, party, ref msg, true, displayLayout, 0, false, ref party.etStatus, false, false); //FB 2553-RMX
                            if (!ret3)
                            {
                                this.errMsg = polycom.errMsg;
                            }
                        }
                        else
                        {
                            if (party.cMcu.etType == NS_MESSENGER.MCU.eType.CODIAN)
                            {
                                NS_CODIAN.Codian codian = new NS_CODIAN.Codian(configParams);
                                logger.Trace("Entering Codian sub-system...");
                                ret3 = codian.ChangePartyDisplayLayout(conf, party, displayLayout);
                                if (!ret3)
                                {
                                    this.errMsg = codian.errMsg;
                                }
                            }
                            else
                            {
                                // Tandberg MCU
                                NS_TANDBERG.Tandberg tandberg = new NS_TANDBERG.Tandberg(configParams);
                                ret3 = tandberg.ChangePartyDisplayLayout(conf, party, displayLayout);
                                if (!ret3)
                                {
                                    this.errMsg = tandberg.errMsg;
                                }
                            }
                        }
                        if (!ret3)
                        {
                            logger.Trace("Change display layout operation failed for endpoint.");
                            //FB 2569 Start
                            alert = new NS_MESSENGER.Alert();
                            alert.confID = conf.iDbID;
                            alert.instanceID = conf.iInstanceID;
                            alert.message = this.errMsg;
                            alert.typeID = 8; //Endpoint Layout
                            bool retp2p = db.InsertConfAlert(alert);
                            if (!retp2p)
                                logger.Trace("Endpoint Layout alert insert failed.");
                            //FB 2569 End
                            //return false; commented for FB 2501 
                        }
                    }
                }
                //FB 2350 End*/
                #endregion
                
                return (true);
            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
                return (false);
            }
        }
        #endregion
        //FB 3073 End

        #region ExtractConfInstanceIds
        /// <summary>
		/// Common method use to extract the conf id and instance id.
		/// </summary>
		/// <param name="inputStr"></param>
		/// <param name="confId"></param>
		/// <param name="instanceId"></param>
		/// <returns></returns>
		internal bool ExtractConfInstanceIds(string inputStr,ref int confId,ref int instanceId)
		{
			try
			{
				// Extract the conf & instance id from the input string . 
				// Input string is in format of "1,2" where confid = 1 & instanceid = 2.
				int delimitter = inputStr.IndexOf(",");
				if (delimitter < 0)
				{
					// there is no instance id.
					confId = Int32.Parse(inputStr.Trim()); 
					instanceId = 1;
                    iRecurring = true; //FB 2441
				}
				else
				{
					// there is an instance id. 
					confId = Int32.Parse(inputStr.Substring(0,delimitter).Trim());
					instanceId = Int32.Parse(inputStr.Substring(delimitter+1).Trim());
				}
				logger.Trace ("ConfID = " + confId.ToString() + ",InstanceId = " + instanceId.ToString());

				return true;
			}
			catch (Exception e)
			{
				logger.Exception (100,e.Message);
				return false;
			}
        }
        #endregion

        #region TestSMTPConnection
        /// <summary>
		/// Test the smtp mail server connectivity.
		/// </summary>
		/// <param name="inXml"></param>
		/// <param name="outXml"></param>
		/// <returns></returns>
		internal bool TestSMTPConnection (string inXml,ref string outXml)
		{
			try
			{
                cryptography.Crypto crypto = null; //FB 3054
                string encryptpass = "";//FB 3054
				logger.Trace ("Entering the Test SMTP Connection method...");				
				
				// Load the string into xml parser and retrieve the tag values.
				XmlDocument xd = new XmlDocument();
				xd.LoadXml(inXml);				
				
				NS_MESSENGER.Smtp smtpServer = new NS_MESSENGER.Smtp();
				smtpServer.Login = xd.SelectSingleNode("//login/AccountLogin").InnerXml.Trim();
                //FB 3054 Starts
                crypto = new Crypto();
                encryptpass = xd.SelectSingleNode("//login/AccountPwd").InnerXml.Trim();
                if (!string.IsNullOrEmpty(encryptpass))
                    encryptpass = crypto.decrypt(encryptpass);
                smtpServer.Password = encryptpass;
                //smtpServer.Password = xd.SelectSingleNode("//login/AccountPwd").InnerXml.Trim();
                //FB 3054 Ends
				smtpServer.ServerAddress = xd.SelectSingleNode("//login/ServerAddress").InnerXml.Trim();
				smtpServer.PortNumber = Int32.Parse(xd.SelectSingleNode("//login/ServerPort").InnerXml.Trim());
				smtpServer.CompanyMailAddress = xd.SelectSingleNode("//login/CompanyEmail").InnerXml.Trim();
				smtpServer.DisplayName = xd.SelectSingleNode("//login/DisplayName").InnerXml.Trim();
                string siteURL = xd.SelectSingleNode("//login/SiteURL").InnerXml.Trim();
				//smtpServer.ConnectionTimeOut = Int32.Parse(xd.SelectSingleNode("//login/ConnectionTimeOut").InnerXml.Trim());
				//smtpServer.FooterMessage = xd.SelectSingleNode("//login/FooterMessage").InnerXml.Trim();

				// Create the test email
				NS_MESSENGER.Email email = new NS_MESSENGER.Email();
				email.From = smtpServer.CompanyMailAddress;
				email.To = smtpServer.CompanyMailAddress;
				//email.Subject = "[VRM Alert] Test email"; //FB 1830
                email.Body = siteURL;
                //email.Body += " Site URL: " + siteURL; //FB 1830
				
				// Send test email via the mail server
				NS_EMAIL.Email sendEmail = new NS_EMAIL.Email(configParams);
				bool ret = sendEmail.SendTestEmail(smtpServer,email);
				if (!ret) 
				{
					this.errMsg = sendEmail.errMsg;
					logger.Trace ("SMTP connection failed.");
					return false;
				}

				return true;
			}
			catch (Exception e)
			{
				logger.Exception(100,e.Message);
				return (false);
			}
        }
        #endregion

        //FB 1758 Starts
        #region TestCompanyMail
        /// <summary>
        /// Test the Comapany mail format.
        /// </summary>
        /// <param name="inXml"></param>
        /// <param name="outXml"></param>
        /// <returns></returns>
        internal bool TestCompanyMail(string inXml)
        {
            int testOrgId = 11; //Default company id
            try
            {
                logger.Trace("Entering the Test company mail method...");

                // Load the string into xml parser and retrieve the tag values.
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(inXml);

                Int32.TryParse(xd.SelectSingleNode("//TestOrgEmail/organizationID").InnerXml.Trim(), out testOrgId);
                string footermsg = xd.SelectSingleNode("//TestOrgEmail/FooterMessage").InnerXml.Trim();

                // Create the test email
                NS_MESSENGER.Email email = new NS_MESSENGER.Email();

                email.orgID = testOrgId;
                email.From = xd.SelectSingleNode("//TestOrgEmail/userid").InnerXml.Trim();
                email.To = xd.SelectSingleNode("//TestOrgEmail/testemailid").InnerXml.Trim();
                
                // Codes removed for FB 1830

                // Send test email via the mail server
                NS_EMAIL.Email sendEmail = new NS_EMAIL.Email(configParams);
                bool ret = sendEmail.SendTestCompanyMail(email, footermsg);
                if (!ret)
                {
                    this.errMsg = sendEmail.errMsg;
                    logger.Trace("Test company mail failed.");
                    return false;
                }

                return true;
            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
                return (false);
            }
        }
        #endregion
        //FB 1758 ends

        #region TestLDAPConnection
        /// <summary>
		/// Test the LDAP server connectivity.
		/// </summary>
		/// <param name="inXml"></param>
		/// <param name="outXml"></param>
		/// <returns></returns>
		internal bool TestLDAPConnection (string inXml,ref string outXml)
		{
			try
			{
                cryptography.Crypto crypto = null; //FB 3054
                string encryptpass = "";//FB 3054
				logger.Trace ("Entering the Test LDAP Connection method...");				
				
				// Load the string into xml parser and retrieve the tag values.
				XmlDocument xd = new XmlDocument();
				xd.LoadXml(inXml);
											
				NS_MESSENGER.LdapSettings ldapSettings = new NS_MESSENGER.LdapSettings();
				
                // extract the LDAP server info from the InXML
				ldapSettings.serverLogin = xd.SelectSingleNode("//login/AccountLogin").InnerXml.Trim();
				
                //FB 3054 Starts
                //ldapSettings.serverPassword = xd.SelectSingleNode("//login/AccountPwd").InnerXml.Trim();
                crypto = new Crypto();
                encryptpass = xd.SelectSingleNode("//login/AccountPwd").InnerXml.Trim();
                if (!string.IsNullOrEmpty(encryptpass))
                    encryptpass = crypto.decrypt(encryptpass);
                
                ldapSettings.serverPassword = encryptpass;
                //FB 3054 Ends
				ldapSettings.serverAddress = xd.SelectSingleNode("//login/ServerAddress").InnerXml.Trim();
				ldapSettings.connPort = Int32.Parse(xd.SelectSingleNode("//login/Port").InnerXml.Trim());                
                ldapSettings.sessionTimeout = Int32.Parse(xd.SelectSingleNode("//login/ConnectionTimeout").InnerXml.Trim());
                ldapSettings.loginKey = xd.SelectSingleNode("//login/LoginKey").InnerXml.Trim();
                ldapSettings.searchFilter = xd.SelectSingleNode("//login/SearchFilter").InnerXml.Trim();
                ldapSettings.domainPrefix = xd.SelectSingleNode("//login/LDAPPrefix").InnerXml.Trim();
                int.TryParse(xd.SelectSingleNode("//login/AuthenticationType").InnerXml.Trim(), out ldapSettings.AuthenticationType); //FB 2993 LDAP              
                // Replace "&amp;" with "&" as ldap search filters need that. 
                ldapSettings.searchFilter = logger.RevertBackTheInvalidChars(ldapSettings.searchFilter);

				// Test LDAP connection
				NS_LDAP.LDAP ldap = new NS_LDAP.LDAP(this.configParams);
				bool ret = ldap.AuthenticateUser(ldapSettings);
				if (!ret) 
				{
					this.errMsg = ldap.errMsg;					
					return false;
				}

				return true;
			}
			catch (Exception e)
			{
				this.errMsg = e.Message;
				logger.Exception(100,e.Message);
				return (false);
			}
        }
        #endregion

        #region TestExchangeConnection
        /// <summary>
		/// Test the Exchange server connectivity.
		/// </summary>
		/// <param name="inXml"></param>
		/// <param name="outXml"></param>
		/// <returns></returns>
		internal bool TestExchangeConnection (string inXml,ref string outXml)
		{
			try
			{
                cryptography.Crypto crypto = null; //FB 3054
                string encryptpass = "";//FB 3054
				logger.Trace ("Entering the Exchange LDAP Connection method...");				
				
				// Load the string into xml parser and retrieve the tag values.
				XmlDocument xd = new XmlDocument();
				xd.LoadXml(inXml);
							
				
				NS_MESSENGER.LdapSettings ldapSettings = new NS_MESSENGER.LdapSettings();
				ldapSettings.serverLogin = xd.SelectSingleNode("//login/AccountLogin").InnerXml.Trim();
				//FB 3054 Starts
                //ldapSettings.serverPassword = xd.SelectSingleNode("//login/AccountPwd").InnerXml.Trim();
                crypto = new Crypto();
                encryptpass = xd.SelectSingleNode("//login/AccountPwd").InnerXml.Trim();
                if (!string.IsNullOrEmpty(encryptpass))
                    encryptpass = crypto.decrypt(encryptpass);
                ldapSettings.serverPassword = encryptpass;
                //FB 3054 Ends
				ldapSettings.serverAddress = xd.SelectSingleNode("//login/ServerURL").InnerXml.Trim();
				//ldapSettings.connPort = Int32.Parse(xd.SelectSingleNode("//login/Port").InnerXml.Trim());
				
				// test ldap connection
				NS_LDAP.LDAP ldap = new NS_LDAP.LDAP(this.configParams);
				bool ret = ldap.AuthenticateUser(ldapSettings);
				if (!ret) 
				{
					this.errMsg = ldap.errMsg;
					logger.Trace ("LDAP connection failed.");
					return false;
				}

				return true;
			}
			catch (Exception e)
			{
				logger.Exception(100,e.Message);
				return (false);
			}
        }
        #endregion

        #region GetConferenceAlerts
        /// <summary>
		/// Fetch all alerts associated with a particular conference.
		/// </summary>
		/// <param name="inXml"></param>
		/// <param name="outXml"></param>
		/// <param name="msg"></param>
		/// <returns></returns>
		internal bool GetConferenceAlerts (string inXml,ref string outXml,ref string msg)
		{
			try
			{
				logger.Trace ("Entering the GetConferenceAlerts method...");				
				
				// Load the string into xml parser and retrieve the tag values.
				XmlDocument xd = new XmlDocument();
				xd.LoadXml(inXml);
							
				// Instantiate the conf object
				NS_MESSENGER.Conference conf = new NS_MESSENGER.Conference();

				string strConfId = xd.SelectSingleNode("//GetConferenceAlerts/ConferenceID").InnerXml.Trim();
				bool ret = false;
				if (strConfId.IndexOf(",") > 0)
				{
					// Just in case ASP sends the confid in this format - 10,1 (confid,instanceid)
					int confId = 0 ,instanceId = 0 ;
					ret = ExtractConfInstanceIds(strConfId,ref confId,ref instanceId);
					if (!ret)
					{
						logger.Trace ("Invalid conf/instance id.");
						return false;
					}
					conf.iDbID = confId;
					conf.iInstanceID = instanceId;
				}
				else
				{
                    conf.iDbID = Int32.Parse(xd.SelectSingleNode("//GetConferenceAlerts/ConferenceID").InnerXml.Trim());
                    conf.iInstanceID = Int32.Parse(xd.SelectSingleNode("//GetConferenceAlerts/InstanceID").InnerXml.Trim()); //FB 2569
				}

                //FB 2569 Start
                int P2Palert = 0;
                if (xd.SelectSingleNode("//GetConferenceAlerts/P2PEvents") != null)
                    Int32.TryParse(xd.SelectSingleNode("//GetConferenceAlerts/P2PEvents").InnerXml.Trim(), out P2Palert);

                NS_DATABASE.Database db = new NS_DATABASE.Database(configParams);
                ret = false; Queue alertQ = new Queue();
                if (P2Palert == 0)
                    ret = db.FetchConfAlerts(conf, ref alertQ);
                else
                    ret = db.FetchP2PConfAlerts(conf, ref alertQ);
                //FB 2569 End
				if (!ret) 
				{
					logger.Trace("Error fetching conference alerts.");
					return false;
				}
				
				outXml = "<Alerts>";
				while (alertQ.Count >0)
				{
					NS_MESSENGER.Alert alert = new NS_MESSENGER.Alert();
					alert = (NS_MESSENGER.Alert)alertQ.Dequeue();
				
					outXml+= "<Alert>";
					outXml+= "<AlertID>" + alert.typeID.ToString() + "</AlertID>";
					outXml+= "<Message>" + alert.message + "</Message>";
					//outXml+= "<Timestamp>" + alert.timestamp.ToLocalTime().ToString("t") + "</Timestamp>";
                    outXml += "<Timestamp>" + alert.timestamp.ToLocalTime().ToString("MM/dd/yyyy hh:mm tt") + "</Timestamp>"; //FB 2569
					outXml+= "</Alert>";
				}
				outXml += "</Alerts>";

				return true;
			}
			catch (Exception e)
			{
				logger.Exception(100,e.Message);
				msg = e.Message;
				return (false);
			}
        }
        #endregion

        #region LogEvent
        /// <summary>
		/// Log an event in Windows Event Logs (Not used so far)
		/// </summary>
		/// <param name="message"></param>
		/// <returns></returns>
		internal bool LogEvent(string message)
		{
			try 
			{
				// log a event in the event viewer 
				NS_LOGGER.EvtLog evtLog = new NS_LOGGER.EvtLog(configParams);
				string sSource = "myVRM";
				string sLog = "Application";				
				bool ret = evtLog.Log(sSource,sLog,message);
				if (!ret)
				{
					this.errMsg = evtLog.errMsg;
					return false;
				}
				return true;
			}
			catch (Exception e)
			{
				this.errMsg = e.Message;
				return false;
			}
        }
        #endregion

        #region ForceConfDelete
        /// <summary>
		/// Force conference deletion/termination on mcu.
		/// </summary>
		/// <param name="inXml"></param>
		/// <param name="outXml"></param>
		/// <returns></returns>
		internal bool ForceConfDelete(string inXml,ref string outXml)
		{
			try
			{

				logger.Trace ("Entering the ForceConfDelete method...");				
				
				// Load the string into xml parser and retrieve the tag values.
				XmlDocument xd = new XmlDocument();
				xd.LoadXml(inXml);									

				// parse the inxml 
				// get the userID
				string userID = xd.SelectSingleNode("//ForceConfDelete/userID").InnerXml.Trim();
				logger.Trace ("UserID = " + userID);

				// confID, instanceID
				string strConfId = xd.SelectSingleNode("//ForceConfDelete/confID").InnerXml.Trim();
				bool ret = false;
				int confId = 0 ,instanceId = 0 ;
				if (strConfId.IndexOf(",") > 0)
				{
					// Just in case ASP sends the confid in this format - 10,1 (confid,instanceid)					
					ret = ExtractConfInstanceIds(strConfId,ref confId,ref instanceId);
					if (!ret)
					{
						logger.Trace ("Invalid conf/instance id.");
						return false;
					}				 
				}
				else
				{
					confId = Int32.Parse(strConfId);
					instanceId =1;
				}

				logger.Trace ("Confid = " + confId.ToString() + ", InstanceID = " + instanceId.ToString());


				// force terminate the conf 				
				NS_DATABASE.Database db = new NS_DATABASE.Database(configParams);
				ret = db.TerminateConference(confId,instanceId);
				if (!ret)
				{
					logger.Trace ("Error terminating conference.");
					return false;
				}
				
/*				// insert the changes in the audit table
				// get the max transaction id 
				ret = false; int transID = 0;
				ret = db.FetchNextTransactionID(confId,instanceId,ref transID);
				if (!ret)
				{
					logger.Trace ("Error fetching next transaction ID.");
					return false;
				}

				// save the history
				Queue diffList = new Queue();
				NS_PERSISTENCE.DiffChanges diff = new NS_PERSISTENCE.DiffChanges();
				diff.confID = confId;
				diff.instanceID = instanceId;
				diff.userID = Int32.Parse(userID);					
				diff.message = "Conference terminated by user.";
				diffList.Enqueue(diff);
				
				// use this transid for all diffs in this transactions
				while (diffList.Count > 0)
				{
					NS_PERSISTENCE.DiffChanges diff1 = new NS_PERSISTENCE.DiffChanges();
					diff1 = (NS_PERSISTENCE.DiffChanges) diffList.Dequeue(); 
					ret = false; 
					ret = db.InsertConfDiffRecord(diff1,transID);
					if (!ret)
					{
						// just log error
						logger.Trace ("Error inserting diff.");
					}
				}
				// commit the diffs 							
				ret = db.CommitDiffs(confId,instanceId,transID);
				if (!ret)
				{
					logger.Trace ("Error committing diffs.");
					return false;
				}
*/				
				outXml = "<success>1</success>";
				return true;

			}
			catch (Exception e)
			{
				logger.Exception(100,e.Message);
				return false;
			}
        }
        #endregion

        #region AuthLdapUser
        internal bool AuthLdapUser(string inXml,ref string outXml)
		{
			try
			{

				logger.Trace ("Entering the Auth ldap user method...");				
				
				// Load the string into xml parser and retrieve the tag values.
				XmlDocument xd = new XmlDocument();
				xd.LoadXml(inXml);									

				// parse the inxml 
				// get the user name and password
				string userName = xd.SelectSingleNode("//login/userName").InnerXml.Trim();
				logger.Trace ("User Name: " + userName);
				string userPassword = xd.SelectSingleNode("//login/userPassword").InnerXml.Trim();
				logger.Trace ("User Password: " + userPassword);

				if (userName != "admin")
				{
					NS_MESSENGER.LdapSettings ldapSettings = new NS_MESSENGER.LdapSettings();
					NS_DATABASE.Database db = new NS_DATABASE.Database(this.configParams);
					bool ret = db.FetchLdapSettings(ref ldapSettings);
					if (!ret) 
					{
						this.errMsg = "Error fetching ldap settings from database.";
						return false;
					}

					// Replace the server login/pwd with the info of the user who is trying to authenticate
					ldapSettings.serverLogin = userName;
					ldapSettings.serverPassword = userPassword;

					NS_LDAP.LDAP ldap = new NS_LDAP.LDAP(this.configParams); ret = false;
					ret = ldap.AuthenticateUser(ldapSettings);
					if (!ret)
					{
						this.errMsg = ldap.errMsg;
						return false;
					}
				}
				else
				{
					logger.Trace ("LDAP authentication skipped as this is the default admin login.");
				}

				outXml = "<success>1</success>";
				return true;
				
			}
			catch (Exception e)
			{
				logger.Exception(100,e.Message);
				return false;
			}
        }
        #endregion

        #region SyncWithLdap
        /// <summary>
		/// Synchronize with ldap server
		/// </summary>
		/// <returns></returns>
		internal bool SyncWithLdap()
		{
			try
			{
				logger.Trace ("Sync with ldap server...");
			
				NS_MESSENGER.LdapSettings ldapSettings = new NS_MESSENGER.LdapSettings();
				NS_DATABASE.Database db = new NS_DATABASE.Database(this.configParams);
				bool ret = db.FetchLdapSettings(ref ldapSettings);
				if (!ret)
				{
					logger.Trace("Error fetching ldap settings from database.");
					return false;
				}
						
				// fetch users from ldap server
				NS_LDAP.LDAP ldapOps = new NS_LDAP.LDAP(this.configParams);
				// maneesh pujari 08/04/2008 FB 637.
                // SyncWithLDAP requires a parameter , if the call is synchronous then pass true
                // otherwise pass false.
				ret = ldapOps.SyncWithLDAP(true);
				if (!ret) 
				{
					logger.Trace (" Error in starting LDAP Sync.");
					return false;			
				}
				
				logger.Trace ("Starting LDAP Sync Status Check");
			    // Code for LDAP Sync Status CHECK


                // End Code for LDAP Status CHECK

				/*// compare and load users in ldap holding area
				ret = false;
				int iUserSkipCount = 0;
				ret = CompareAndLoadUsersInLdapHoldingArea(ref iUserSkipCount,ldapUserList);
				if (!ret) 
				{
					logger.Trace (" Error in comparing and loading users in ldap holding area.");
					return false;			
				}

                // update in ldap table the sync time
                // maneesh pujari 07/29/2008 FB 594.
                db.UpdateLdapSyncTime();
                */
				return true;			
			}
			catch (Exception e)
			{
				this.errMsg = e.Message;
				logger.Exception(100,e.Message);
				return false;
			}
        }
        #endregion

        #region BulkLoadUsers
        /// <summary>
		/// load all ldap users from the a text file dump.
		/// </summary>
		/// <param name="csvFilePath"></param>
		/// <returns></returns>
		internal bool BulkLoadUsers(string inXml)
		{
			try
			{
				logger.Trace ("Entering the BulkLoadUsers method...");				
				
				// Load the string into xml parser and retrieve the tag values.
				XmlDocument xd = new XmlDocument();
				xd.LoadXml(inXml);									

				// parse the inxml 
				// get the user list				
				Queue newUserList = new Queue();
				XmlNodeList nodelist = xd.SelectNodes("//BulkLoadUsers/User");
				int iUserSkipCount = 0;
				
				//cycle through each user one-by-one
				foreach(XmlNode node in nodelist)
				{	
					try
					{
						logger.Trace("in node...");
					
						// parse the record
						NS_MESSENGER.LdapUser user = new NS_MESSENGER.LdapUser();

						// user details
						user.email = node.SelectSingleNode("Email").InnerText.Trim();
						user.firstName = node.SelectSingleNode("FirstName").InnerText.Trim();
						user.lastName = node.SelectSingleNode("LastName").InnerText.Trim();
						user.login = node.SelectSingleNode("Login").InnerText.Trim();
						user.telephone = node.SelectSingleNode("Telephone").InnerText.Trim();
				
						// user validation checks
						if (user.email.IndexOf("@") < 0 || user.login.Length < 3)
						{
							// email is invalid. skip this record.
							iUserSkipCount++;
							continue;
						}						
						logger.Trace ("User : " + user.email + "," + user.firstName + "," + user.lastName + "," + user.login + "," + user.telephone);

						// save the user in queue
						newUserList.Enqueue(user);
					}
					catch (Exception)
					{
						logger.Trace ("User record corrupted.Skipping...");

					}
				}
				
				logger.Trace ("Total valid user records in the csv file = " + newUserList.Count.ToString());
				
				// compare ldap users with existing users and add only the new users in db 				
				bool ret = this.CompareAndLoadUsersInLdapHoldingArea(ref iUserSkipCount,newUserList);
				if (!ret) 
				{
					logger.Trace (" Error in comparing and loading users in ldap holding area.");
					return false;			
				}
				return true;
			}
			catch (Exception e)
			{
				logger.Exception (100,e.Message);
				return false;
			}
        }
        #endregion

        #region CompareAndLoadUsersInLdapHoldingArea
        /// <summary>
		/// Compare user logins and load only unique users in ldap holding area
		/// </summary>
		/// <param name="newUserList"></param>
		/// <returns></returns>
		private bool CompareAndLoadUsersInLdapHoldingArea(ref int iUserSkipCount, Queue newUserList)
		{
			try 
			{
				// check to see if the users are already there in the database.
				// if user records already exists then skip adding the user. 				
				// get all the user email list from the active,inactive,ldap tables				
				Queue uniqueNewUserList = new Queue();
				NS_DATABASE.Database db = new NS_DATABASE.Database(this.configParams);

				// get the existing user list - EMAILS
				Queue existingUserList_Email = new Queue();
				bool ret = db.FetchAllUserEmails(ref existingUserList_Email);
				if (!ret)
				{
					logger.Trace ("Error fetching emails.");
					return false;
				}

				// get the existing user list - Logins
				Queue existingUserList_Login = new Queue(); ret=false;
				ret = db.FetchAllUserLogins(ref existingUserList_Login);
				if (!ret)
				{
					logger.Trace ("Error fetching emails.");
					return false;
				}
				
				while (newUserList.Count > 0)
				{
					NS_MESSENGER.LdapUser newUser = new NS_MESSENGER.LdapUser();
					newUser = (NS_MESSENGER.LdapUser) newUserList.Dequeue();						
					
					// check if this email is unique or not
					System.Collections.IEnumerator existingUserList_Email_Enumerator = existingUserList_Email.GetEnumerator();
					bool userEmailExists = false;
					int existingUserList_Email_Count = existingUserList_Email.Count;									
					for (int j= 0 ; j < existingUserList_Email_Count; j++)
					{
						// go to the next item in the list
						bool ret7 = existingUserList_Email_Enumerator.MoveNext();
						if (!ret7) break;
						
						NS_MESSENGER.LdapUser existingUser_Email =(NS_MESSENGER.LdapUser)existingUserList_Email_Enumerator.Current;
						if (newUser.email == existingUser_Email.email)
						{
							// user exists. 
							userEmailExists = true;
							iUserSkipCount++; 
							break;
						}
					}

					if (!userEmailExists)
					{
						// check if this email is unique or not
						System.Collections.IEnumerator existingUserList_Login_Enumerator = existingUserList_Login.GetEnumerator();
						bool userLoginExists = false;
						int existingUserList_Login_Count = existingUserList_Login.Count;									
						for (int j= 0 ; j < existingUserList_Login_Count; j++)
						{
							// go to the next item in the list
							bool ret7 = existingUserList_Login_Enumerator.MoveNext();
							if (!ret7) break;
						
							NS_MESSENGER.LdapUser existingUser_Login =(NS_MESSENGER.LdapUser)existingUserList_Login_Enumerator.Current;
							if (newUser.login == existingUser_Login.login)
							{
								// user exists. 
								userLoginExists = true;
								iUserSkipCount++; 
								break;
							}
						}
						
						if (!userLoginExists)
						{
							// found a unique user containing a unique email & login
							uniqueNewUserList.Enqueue(newUser);
						}
					}
				}
					
				logger.Trace ("Total unique user records  = " + uniqueNewUserList.Count.ToString());

				// load the users in the AD holding area
				while (uniqueNewUserList.Count > 0)
				{
					NS_MESSENGER.LdapUser ldapUser = new NS_MESSENGER.LdapUser();
					ldapUser = (NS_MESSENGER.LdapUser)uniqueNewUserList.Dequeue();

					ret = false;
					ret = db.InsertLdapUser(ldapUser);
					if (!ret)
					{
						iUserSkipCount++; 
						logger.Trace ("Error inserting ldap user");
						continue;
					}				
				}

				return true;
			}
			catch (Exception e)
			{
				logger.Exception (100,e.Message);
				return false;
			}

        }
        #endregion

        #region GetUserHistory
        internal bool GetUserHistory(string inXml)
        {
            try
            {
                logger.Trace("Entering the GetUserHistory method...");

                // Load the string into xml parser and retrieve the tag values.
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(inXml);

                // parse the inxml                                 
                string firstName = xd.SelectSingleNode("//GetUserHistory/FirstName").InnerXml.Trim();
                string lastName = xd.SelectSingleNode("//GetUserHistory/LastName").InnerXml.Trim();
                string email = xd.SelectSingleNode("//GetUserHistory/Email").InnerXml.Trim();
                DateTime fromDate = DateTime.Parse(xd.SelectSingleNode("//GetUserHistory/DateFrom").InnerXml.Trim());
                DateTime toDate = DateTime.Parse(xd.SelectSingleNode("//GetUserHistory/DateTo").InnerXml.Trim());

                // find the userid matching the email.
                NS_MESSENGER.Party party = new NS_MESSENGER.Party();
                NS_DATABASE.Database db = new NS_DATABASE.Database(this.configParams);
                bool ret = db.FetchUser(ref party);

                // find all the login audit records for the user.
                Queue loginDateList = new Queue(); ret = false;
                ret = db.FetchUserLoginHistory(party, fromDate,toDate, ref loginDateList);

                // build the outxml
                string outXML = "<GetUserHistory>";
                System.Collections.IEnumerator loginDateEnumerator = loginDateList.GetEnumerator();
                int loginDateCount = loginDateList.Count;
                for (int j = 0; j < loginDateCount; j++)
                {
                    outXML += "<User>";
                    outXML += "<FirstName>"+ firstName +"</FirstName>";
                    outXML += "<LastName>"+ lastName +"</LastName>";
                    outXML += "<Email>"+ email +"</Email>";

                    // go to the next item in the list
                    bool ret1 = loginDateEnumerator.MoveNext();
                    if (!ret1) break;

                    DateTime loginDate = (DateTime)loginDateEnumerator.Current;
                    outXML += "<LoginDateTime>"+loginDate +"</LoginDateTime>";
                    outXML += "</User>";
                }
                outXML += "</GetUserHistory>";

                return true;
            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
                return false;
            }
        }
        #endregion

        //FB 2501 Call Monitoring Command Changes Starts

        #region ConnectDisconnectEndpoint
        internal bool ConnectDisconnectEndpoint(string inXml, ref string outXml, ref string msg)
        {
            try
            {
                logger.Trace("Entering the function...");
                // Load the string into xml parser and retrieve the tag values.
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(inXml);
				//FB 2989 Starts
                string login = "";
                if (xd.SelectSingleNode("//login/userID") != null)
                    login = xd.SelectSingleNode("//login/userID").InnerXml.Trim();

                string strConfId = "";
                if (xd.SelectSingleNode("//login/confID") != null)
                    strConfId = xd.SelectSingleNode("//login/confID").InnerXml.Trim();

                int confId = 0, instanceId = 0;
                bool ret = ExtractConfInstanceIds(strConfId, ref confId, ref instanceId);
                if (!ret)
                {
                    logger.Trace("Invalid conf/instance id.");
                    return false;
                }

                int endpointId = 0;
                if (xd.SelectSingleNode("//login/endpointID") != null)
                    int.TryParse(xd.SelectSingleNode("//login/endpointID").InnerXml.Trim(), out endpointId); // database id

                int terminalType = 0;
                if (xd.SelectSingleNode("//login/terminalType") != null)
                    int.TryParse(xd.SelectSingleNode("//login/terminalType").InnerXml.Trim(), out terminalType); // 1 = user , 2 = room, 3 = guest , 4 = cascade				
                logger.Trace("Terminaltype = " + terminalType.ToString());

                int iConnectDisconnect = 0;
                if (xd.SelectSingleNode("//login/connectOrDisconnect") != null)
                    int.TryParse(xd.SelectSingleNode("//login/connectOrDisconnect").InnerXml.Trim(), out iConnectDisconnect); // 0 = disconnect, 1 = connect
                logger.Trace("connectOrDisconnect = " + terminalType.ToString());
                //FB 2989 End

                //ALLDEV-814
                if (xd.SelectSingleNode("//login/EptProfileID") != null)
                    int.TryParse(xd.SelectSingleNode("//login/EptProfileID").InnerXml.Trim(), out eptProfileID);

                bool connectDisconnect = false;
                if (iConnectDisconnect == 1)
                {
                    connectDisconnect = true;
                }

                //FB 2501 Call Monitoring Start

                int iConnectDisconnectAll = 0;
                if(xd.SelectSingleNode("//login/connectOrDisconnectAll") != null)
                    int.TryParse(xd.SelectSingleNode("//login/connectOrDisconnectAll").InnerXml.Trim(), out iConnectDisconnectAll);

                //FB 2501 Call Monitoring End

                // Fetch the conf unique name since thats the unique identifier of the conf.
                NS_MESSENGER.Conference conf = new NS_MESSENGER.Conference();
                NS_DATABASE.Database db = new NS_DATABASE.Database(configParams);
                conf.iDbID = confId; conf.iInstanceID = instanceId;
                ret = db.FetchConf(ref conf);
                if (!ret)
                {
                    logger.Exception(200, "Database fetch operation failed.");
                    return (false);
                }

                #region PartyConnectDisconnect

                if (iConnectDisconnectAll == (int)eConnectType.PARTY_CONNECTDISCONNECT)
                {

                    // Retreive the bridge info for that endpoint.
                    NS_MESSENGER.Party party = new NS_MESSENGER.Party();
                    switch (terminalType)
                    {
                        case 1:
                            {
                                // user
                                db.eptProfileID = eptProfileID; //ALLDEV-814
                                bool ret2 = db.FetchUser(confId, instanceId, endpointId, ref party);
                                break;
                            }
                        case 2:
                            {
                                // room
                                logger.Trace("In room...");
                                bool ret2 = db.FetchRoom(confId, instanceId, endpointId, ref party);
                                break;
                            }
                        case 3:
                            {
                                // guest
                                bool ret2 = db.FetchGuest(confId, instanceId, endpointId, ref party);
                                break;
                            }
                        case 4:
                            {
                                // cascade
                                bool ret2 = db.FetchCascadeLink(confId, instanceId, endpointId, ref party);
                                break;
                            }
                    }

                    bool ret3 = false;
                    if (conf.etType == NS_MESSENGER.Conference.eType.POINT_TO_POINT)
                    {
                        // fetch other endpoints in conference
                        #region Fetch the endpoints
                        ret = false;
                        ret = db.FetchRooms(conf.iDbID, conf.iInstanceID, ref conf.qParties, 0);
                        if (!ret)
                        {
                            logger.Exception(100, "Error in fetching rooms from db.");
                            return false;
                        }
                        ret = false;
                        ret = db.FetchUsers(conf.iDbID, conf.iInstanceID, ref conf.qParties, 0);
                        if (!ret)
                        {
                            logger.Exception(100, "Error in fetching users from db.");
                            return false;
                        }
                        ret = false;
                        ret = db.FetchGuests(conf.iDbID, conf.iInstanceID, ref conf.qParties, 0);
                        if (!ret)
                        {
                            logger.Exception(100, "Error in fetching guests from db.");
                            return false;
                        }
                        ret = false;
                        Queue cascadeLinks = new Queue();
                        ret = db.FetchCascadeLinks(conf.iDbID, conf.iInstanceID, ref cascadeLinks, 0);
                        if (!ret)
                        {
                            logger.Exception(100, "Error in fetching cascade links from db.");
                            return false;
                        }
                        #endregion

                        //ZD 101363 Start
                        #region Check Caller connection type

                        bool ret5 = false;
                        System.Collections.IEnumerator partyEnumerator;
                        partyEnumerator = conf.qParties.GetEnumerator();
                        int partyCount = conf.qParties.Count;
                        for (int i = 0; i < partyCount; i++)
                        {
                            // src party 
                            NS_MESSENGER.Party party1 = new NS_MESSENGER.Party();
                            // go to next party in the queue
                            partyEnumerator.MoveNext();
                            party1 = (NS_MESSENGER.Party)partyEnumerator.Current;
                            if (party1.etCallerCallee == NS_MESSENGER.Party.eCallerCalleeType.CALLER)
                            {
                                if (party1.iSSHSupport == 1)
                                {
                                    // connect call on ssh 
                                    NS_SSH.SSH SSH = new NS_SSH.SSH(this.configParams);
                                    ret5 = SSH.ConnectDisconnectCall(conf, party1, connectDisconnect);
                                    if (!ret5)
                                    {
                                        // conf setup failed
                                        this.errMsg = "Error: " + SSH.errMsg;
                                        return false;
                                    }
                                }
                                else
                                {
                                    // connect call on telnet 
                                    NS_TELNET.Telnet telnet = new NS_TELNET.Telnet(this.configParams);
                                    ret3 = telnet.ConnectDisconnectCall(conf, party1, connectDisconnect);
                                    if (!ret5)
                                    {
                                        // conf setup failed
                                        this.errMsg = "Error: " + telnet.errMsg;
                                        return false;
                                    }
                                }
                            }
                        }

                        #endregion                          
                        //ZD 101363 End

                        //FB 2390 Start
                        if (connectDisconnect == true && party.ip2pCallid > 1)
                        {
                            db.UpdateCallId(conf.iDbID, conf.iInstanceID, party.ip2pCallid);
                        }
                        //FB 2390 End
                        //ZD 100655 Start
                        if (ret3)
                        {
                            if (connectDisconnect == true)
                            {
                                party.etStatus = NS_MESSENGER.Party.eOngoingStatus.FULL_CONNECT;
                            }
                            else
                            {
                                party.etStatus = NS_MESSENGER.Party.eOngoingStatus.DIS_CONNECT;
                            }

                            db.UpdateP2PConferenceEndpointStatus(confId, instanceId, party.etStatus);
                        }
                       
                        //ZD 100655 End
                    }
                    else
                    {
                        //FB 2578 Start

                        List<NS_MESSENGER.Party> parties = TelepresenceEndpoint(party); //FB 2584

                        for (int i = 0; i < parties.Count; i++)
                        {

                            party = parties[i]; //FB 2578
                            // connect to the bridge and perform the operation.
                            ret3 = false;
                            if (party.etStatus == NS_MESSENGER.Party.eOngoingStatus.FULL_CONNECT && connectDisconnect == true) // FB 2501 Dec6
                            {

                                logger.Trace("Party already connected to MCU");
                                return true;
                            }
                            else
                            {
                                // connect to the bridge and perform the operation.               
                                if (party.cMcu.etType == NS_MESSENGER.MCU.eType.ACCORDv6 || party.cMcu.etType == NS_MESSENGER.MCU.eType.ACCORDv7 || party.cMcu.etType == NS_MESSENGER.MCU.eType.RMX)
                                {
                                    NS_POLYCOM.Polycom polycom = new NS_POLYCOM.Polycom(configParams);
                                    ret3 = polycom.OngoingConfOps("ConnectDisconnectEndpoint", conf, party, ref msg, false, 0, 0, connectDisconnect, ref party.etStatus, false, false); //FB 2553-RMX
                                    if (ret3) //FB 2989
                                        ret3 = polycom.OngoingConfOps("GetTerminalStatus", conf, party, ref msg, false, 0, 0, false, ref party.etStatus, false, false);
                                    else
                                    {
                                        this.errMsg = polycom.errMsg;
                                        msg = polycom.errMsg;
                                    }
                                }
                                else
                                {
                                    if (party.cMcu.etType == NS_MESSENGER.MCU.eType.CODIAN)
                                    {
                                        NS_CODIAN.Codian codian = new NS_CODIAN.Codian(configParams);
                                        ret3 = codian.ConnectDisconnectEndpoint(conf, party, connectDisconnect);
                                        if (!ret3)
                                        {
                                            this.errMsg = codian.errMsg;
                                        }
                                    }
                                    //FB 2501 Call Monitoring Start
                                    else if (party.cMcu.etType == NS_MESSENGER.MCU.eType.CISCOTP)
                                    {
                                        NS_CiscoTPServer.CiscoTPServer CiscoTPServer = new NS_CiscoTPServer.CiscoTPServer(configParams);
                                        ret3 = CiscoTPServer.ConnectDisconnectEndpoint(conf, party, connectDisconnect);
                                        if (!ret3)
                                        {
                                            this.errMsg = CiscoTPServer.errMsg;
                                        }
                                    }
                                    //FB 2501 Call Monitoring End   
                                    else if (party.cMcu.etType == NS_MESSENGER.MCU.eType.RPRM)
                                    {
                                        NS_POLYCOMRPRM.POLYCOMRPRM polycomRPRM = new NS_POLYCOMRPRM.POLYCOMRPRM(configParams);
                                        ret3 = polycomRPRM.ConnectDisconnectEndpoint(conf, party, connectDisconnect);
                                        if (!ret3)
                                        {
                                            this.errMsg = polycomRPRM.errMsg;
                                        }
                                        
                                    }
                                    //FB 2556 Starts
                                    else if (party.cMcu.etType == NS_MESSENGER.MCU.eType.RADVISIONIVIEW)
                                    {
                                        NS_RADVISION_IView.RadVisionIView radiview = new NS_RADVISION_IView.RadVisionIView(configParams);
                                        ret3 = radiview.ConnectDisconnectEndpoint(conf, party, connectDisconnect);
                                        if (!ret3)
                                        {
                                            this.errMsg = radiview.errMsg;
                                        }
                                    }
                                    //FB 2556 Ends
                                    //ZD 101217 Starts
                                    else if (party.cMcu.etType == NS_MESSENGER.MCU.eType.Pexip)
                                    {
                                        NS_Pexip.pexip pexip = new NS_Pexip.pexip(configParams);
                                        ret3 = pexip.ConnectDisconnectEndpoint(conf, party, connectDisconnect);
                                        if (!ret3)
                                        {
                                            this.errMsg = pexip.errMsg;
                                        }
                                    }
                                    //ZD 101217 Ends
                                    else
                                    {
                                        this.errMsg = "Operation not supported on Tandberg MCU.";
                                        return false;
                                    }

                                }
                            }
                            //FB 2989 Starts
                            if (ret3 && party.cMcu.etType == NS_MESSENGER.MCU.eType.RMX)
                            {
                                if (party.etType == NS_MESSENGER.Party.eType.USER || party.etType == NS_MESSENGER.Party.eType.GUEST)
                                {
                                    db.UpdateConferenceUserStatus(conf.iDbID, conf.iInstanceID, party.iEndpointId, party.etStatus);
                                }
                                else
                                {
                                    if (party.etType == NS_MESSENGER.Party.eType.ROOM)
                                    {
                                        db.UpdateConferenceRoomStatus(conf.iDbID, conf.iInstanceID, party.iEndpointId, party.etStatus);
                                    }
                                    else
                                    {
                                        db.UpdateConferenceCascadeStatus(conf.iDbID, conf.iInstanceID, party.iEndpointId, party.etStatus);
                                    }
                                }
                            }
                            //FB 2989 End
                        }
                        //FB 2578 End
                    }

                    if (!ret3)
                    {
                        logger.Trace("ConnectDisconnect operation failed for endpoint.");

                        //FB 2569 Start
                        alert = new NS_MESSENGER.Alert();
                        alert.confID = conf.iDbID;
                        alert.instanceID = conf.iInstanceID;
                        alert.message = this.errMsg;
                        alert.typeID = 10; //Endpoint redial
                        bool retp2p = db.InsertConfAlert(alert);
                        if (!retp2p)
                            logger.Trace("Endpoint ConnectDisconnect alert insert failed.");
                        //FB 2569 End

                        return false;
                    }
                }
                #endregion

                #region PartyConnectDisconnectAll

                else if (iConnectDisconnectAll == (int)eConnectType.PARTYALL_CONNECTDISCONNECT)
                {
                    #region Fetch the endpoints
                    ret = false;
                    ret = db.FetchRooms(conf.iDbID, conf.iInstanceID, ref conf.qParties, 0);
                    if (!ret)
                    {
                        logger.Exception(100, "Error in fetching rooms from db.");
                        return false;
                    }
                    ret = false;
                    ret = db.FetchUsers(conf.iDbID, conf.iInstanceID, ref conf.qParties, 0);
                    if (!ret)
                    {
                        logger.Exception(100, "Error in fetching users from db.");
                        return false;
                    }
                    ret = false;
                    ret = db.FetchGuests(conf.iDbID, conf.iInstanceID, ref conf.qParties, 0);
                    if (!ret)
                    {
                        logger.Exception(100, "Error in fetching guests from db.");
                        return false;
                    }
                    ret = false;
                    Queue cascadeLinks = new Queue();
                    ret = db.FetchCascadeLinks(conf.iDbID, conf.iInstanceID, ref cascadeLinks, 0);
                    if (!ret)
                    {
                        logger.Exception(100, "Error in fetching cascade links from db.");
                        return false;
                    }
                    #endregion

                    logger.Trace(" Entering Party ConnectDisconnect All ...");

                    // Fetch all the bridge ids on which this conf is running.
                    System.Collections.IEnumerator partyEnumerator;
                    partyEnumerator = conf.qParties.GetEnumerator();
                    int partyCount = conf.qParties.Count;
                    logger.Trace("Party Count = " + partyCount.ToString());

                    Queue mcuIdList = new Queue();
                    bool ret3 = false;
                    for (int i = 0; i < partyCount; i++)
                    {
                        ret3 = partyEnumerator.MoveNext();
                        if (!ret3) break;

                        NS_MESSENGER.Party party = new NS_MESSENGER.Party();
                        party = (NS_MESSENGER.Party)partyEnumerator.Current;

                        logger.Trace("Party Address : " + party.sAddress.ToString());
                        if (party.etStatus == NS_MESSENGER.Party.eOngoingStatus.FULL_CONNECT && connectDisconnect == true) // FB 2501 Dec6
                        {

                            logger.Trace("Party already connected to MCU");
                        }
                        else
                        {
                            if (party.cMcu.etType == NS_MESSENGER.MCU.eType.ACCORDv6 || party.cMcu.etType == NS_MESSENGER.MCU.eType.ACCORDv7 || party.cMcu.etType == NS_MESSENGER.MCU.eType.RMX)
                            {
                                NS_POLYCOM.Polycom polycom = new NS_POLYCOM.Polycom(configParams);
                                ret3 = polycom.OngoingConfOps("ConnectDisconnectEndpoint", conf, party, ref msg, false, 0, 0, connectDisconnect, ref party.etStatus, false, false); //FB 2553-RMX
                                if (ret3) //FB 2989
                                    ret3 = polycom.OngoingConfOps("GetTerminalStatus", conf, party, ref msg, false, 0, 0, false, ref party.etStatus, false, false);
                                else
                                {
                                    this.errMsg = polycom.errMsg;
                                    msg = polycom.errMsg;
                                }
                            }
                            else
                            {
                                if (party.cMcu.etType == NS_MESSENGER.MCU.eType.CODIAN)
                                {
                                    NS_CODIAN.Codian codian = new NS_CODIAN.Codian(configParams);
                                    ret3 = codian.ConnectDisconnectEndpoint(conf, party, connectDisconnect);
                                    if (!ret3)
                                    {
                                        this.errMsg = codian.errMsg;
                                    }
                                }
                                //FB 2501 Call Monitoring Start
                                else if (party.cMcu.etType == NS_MESSENGER.MCU.eType.CISCOTP)
                                {
                                    NS_CiscoTPServer.CiscoTPServer CiscoTPServer = new NS_CiscoTPServer.CiscoTPServer(configParams);
                                    ret3 = CiscoTPServer.ConnectDisconnectEndpoint(conf, party, connectDisconnect);
                                    if (!ret3)
                                    {
                                        this.errMsg = CiscoTPServer.errMsg;
                                    }
                                }
                                //FB 2501 Call Monitoring End   
                                //ZD 101217 Starts
                                else if (party.cMcu.etType == NS_MESSENGER.MCU.eType.Pexip)
                                {
                                    NS_Pexip.pexip pexip = new NS_Pexip.pexip(configParams);
                                    ret3 = pexip.ConnectDisconnectEndpoint(conf, party, connectDisconnect);
                                    if (!ret3)
                                    {
                                        this.errMsg = pexip.errMsg;
                                    }
                                }
                                //ZD 101217 Ends
                                //FB 2556 Starts //ZD 102359 Starts

                                else if (party.cMcu.etType == NS_MESSENGER.MCU.eType.RPRM)
                                {
                                    NS_POLYCOMRPRM.POLYCOMRPRM polycomRPRM = new NS_POLYCOMRPRM.POLYCOMRPRM(configParams);
                                    logger.Trace("sName" + party.sName);
                                    logger.Trace("sAddress" + party.sAddress);
                                    ret3 = polycomRPRM.ConnectDisconnectEndpoint(conf, party, connectDisconnect);
                                    if (!ret3)
                                    {
                                        this.errMsg = polycomRPRM.errMsg;
                                    }

                                }
								//ZD 102359 Ends
                                else if (party.cMcu.etType == NS_MESSENGER.MCU.eType.RADVISIONIVIEW)
                                {
                                    NS_RADVISION_IView.RadVisionIView radiview = new NS_RADVISION_IView.RadVisionIView(configParams);
                                    ret3 = radiview.ConnectDisconnectEndpoint(conf, party, connectDisconnect);
                                    if (!ret3)
                                    {
                                        this.errMsg = radiview.errMsg;
                                    }
                                }
                                //FB 2556 Ends
                                else
                                {
                                    this.errMsg = "Operation not supported on Tandberg MCU.";
                                    return false;
                                }

                            }
                        }
                        if (!ret3)
                        {
                            logger.Trace("ConnectDisconnect operation failed for endpoint.");
                            //FB 2569 Start
                            alert = new NS_MESSENGER.Alert();
                            alert.confID = conf.iDbID;
                            alert.instanceID = conf.iInstanceID;
                            alert.message = this.errMsg;
                            alert.typeID = 10; //Endpoint redial
                            bool retp2p = db.InsertConfAlert(alert);
                            if (!retp2p)
                                logger.Trace("Endpoint ConnectDisconnect alert insert failed.");
                            //FB 2569 End
                            //return false; commented for FB 2501 
                        } //FB 2989 Starts
                        else if (ret3 && party.cMcu.etType == NS_MESSENGER.MCU.eType.RMX)
                        {
                            if (conf.etType == NS_MESSENGER.Conference.eType.POINT_TO_POINT)
                            {
                                db.UpdateP2PConferenceEndpointStatus(confId, instanceId, party.etStatus);
                            }
                            else
                            {
                                if (party.etType == NS_MESSENGER.Party.eType.USER || party.etType == NS_MESSENGER.Party.eType.GUEST)
                                {
                                    db.UpdateConferenceUserStatus(conf.iDbID, conf.iInstanceID, party.iEndpointId, party.etStatus);
                                }
                                else
                                {
                                    if (party.etType == NS_MESSENGER.Party.eType.ROOM)
                                    {
                                        db.UpdateConferenceRoomStatus(conf.iDbID, conf.iInstanceID, party.iEndpointId, party.etStatus);
                                    }
                                    else
                                    {
                                        db.UpdateConferenceCascadeStatus(conf.iDbID, conf.iInstanceID, party.iEndpointId, party.etStatus);
                                    }
                                }
                            }
                        }
                        //FB 2989 End
                    }
                }

                #endregion

                return (true);
            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
                return (false);
            }
        }
        #endregion
        
        //FB 2501 Call Monitoring Command Changes End



        #region SendMessageToEndpoint
        internal bool SendMessageToEndpoint(string inXml, ref string outXml, ref string msg)
        {
            try
            {
                logger.Trace("Entering the SendMessageToEndpoint function...");
                // Load the string into xml parser and retrieve the tag values.
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(inXml);

                string login = xd.SelectSingleNode("//login/userID").InnerXml.Trim();

                string strConfId = xd.SelectSingleNode("//login/confID").InnerXml.Trim();
                int confId = 0, instanceId = 0;
                bool ret = ExtractConfInstanceIds(strConfId, ref confId, ref instanceId);
                if (!ret)
                {
                    logger.Trace("Invalid conf/instance id.");
                    return false;
                }

                int endpointId = Int32.Parse(xd.SelectSingleNode("//login/endpointID").InnerXml.Trim()); // database id

                int terminalType = Int32.Parse(xd.SelectSingleNode("//login/terminalType").InnerXml.Trim()); // 1 = user , 2 = room, 3 = guest , 4 = cascade				
                logger.Trace("Terminaltype = " + terminalType.ToString());

                string messageText = xd.SelectSingleNode("//login/messageText").InnerXml.Trim(); // message to be sent to endpoint
                logger.Trace("Message Text = " + messageText);
                //ALLDEV-814
                if (xd.SelectSingleNode("//login/EptProfileID") != null)
                    int.TryParse(xd.SelectSingleNode("//login/EptProfileID").InnerXml.Trim(), out eptProfileID);
                //FB 3059 Start
                int SendMsg = 0;
                if (xd.SelectSingleNode("//login/SendMsg") != null)
                    Int32.TryParse(xd.SelectSingleNode("//login/SendMsg").InnerXml.Trim(), out SendMsg);
                //FB 3059 End

                //FB 2501 Call Monitoring Start

                int direction = 0, duration = 0;

                Int32.TryParse(xd.SelectSingleNode("//login/direction").InnerXml.Trim(), out direction);

                Int32.TryParse(xd.SelectSingleNode("//login/duration").InnerXml.Trim(), out duration);

                //FB 2501 Call Monitoring End


                // Fetch the conf unique name since thats the unique identifier of the conf.
                NS_MESSENGER.Conference conf = new NS_MESSENGER.Conference();
                NS_DATABASE.Database db = new NS_DATABASE.Database(configParams);
                conf.iDbID = confId; conf.iInstanceID = instanceId;
                ret = db.FetchConf(ref conf);
                if (!ret)
                {
                    logger.Exception(200, "Database fetch operation failed.");
                    return (false);
                }

                // Retreive the bridge info for that endpoint.
                NS_MESSENGER.Party party = new NS_MESSENGER.Party();
                switch (terminalType)
                {
                    case 1:
                        {
                            // user
                            db.eptProfileID = eptProfileID; //ALLDEV-814
                            bool ret2 = db.FetchUser(confId, instanceId, endpointId, ref party);
                            break;
                        }
                    case 2:
                        {
                            // room
                            logger.Trace("In room...");
                            bool ret2 = db.FetchRoom(confId, instanceId, endpointId, ref party);
                            break;
                        }
                    case 3:
                        {
                            // guest
                            bool ret2 = db.FetchGuest(confId, instanceId, endpointId, ref party);
                            break;
                        }
                    case 4:
                        {
                            // cascade
                            bool ret2 = db.FetchCascadeLink(confId, instanceId, endpointId, ref party);
                            break;
                        }
                }

                bool ret3 = false;
                if (conf.etType == NS_MESSENGER.Conference.eType.POINT_TO_POINT)
                {
                    // fetch other endpoints in conference
                    #region Fetch the endpoints
                    ret = false;
                    ret = db.FetchRooms(conf.iDbID, conf.iInstanceID, ref conf.qParties, 0);
                    if (!ret)
                    {
                        logger.Exception(100, "Error in fetching rooms from db.");
                        return false;
                    }
                    ret = false;
                    ret = db.FetchUsers(conf.iDbID, conf.iInstanceID, ref conf.qParties, 0);
                    if (!ret)
                    {
                        logger.Exception(100, "Error in fetching users from db.");
                        return false;
                    }
                    ret = false;
                    ret = db.FetchGuests(conf.iDbID, conf.iInstanceID, ref conf.qParties, 0);
                    if (!ret)
                    {
                        logger.Exception(100, "Error in fetching guests from db.");
                        return false;
                    }
                    ret = false;
                    Queue cascadeLinks = new Queue();
                    ret = db.FetchCascadeLinks(conf.iDbID, conf.iInstanceID, ref cascadeLinks, 0);
                    if (!ret)
                    {
                        logger.Exception(100, "Error in fetching cascade links from db.");
                        return false;
                    }
                    #endregion

                    System.Collections.IEnumerator partyEnumerator;
                    partyEnumerator = conf.qParties.GetEnumerator();
                    int partyCount = conf.qParties.Count;
                    for (int i = 0; i < partyCount; i++)
                    {
                        // go to next party in the queue
                        partyEnumerator.MoveNext();
                        
                        NS_MESSENGER.Party party1 = new NS_MESSENGER.Party();
                        party1 = (NS_MESSENGER.Party)partyEnumerator.Current;
                        if (party1.iDbId == (endpointId + 10000)) // Magic Number = 10000 . Needed so that it doesnot clash with user-id's.
                        {
                            //ZD 101363 Start
                            if (party1.etCallerCallee == NS_MESSENGER.Party.eCallerCalleeType.CALLER)
                            {
                                if (party1.iSSHSupport == 1)
                                {
                                    NS_SSH.SSH SSH = new NS_SSH.SSH(this.configParams);
                                    ret3 = SSH.SendMessageTextToParty(party1, messageText, conf); 
                                    break;
                                }
                                else
                                {
                                    // connect call on telnet 
                                    NS_TELNET.Telnet telnet = new NS_TELNET.Telnet(this.configParams);
                                    ret3 = telnet.SendMessageTextToParty(party1, messageText, conf); //FB 2501 P2P Call Monitoring
                                    break;
                                }
                            }
                            //ZD 101363 End
                        }
                    }
                    
                }
                else
                {
                    //FB 2584 Start

                    List<NS_MESSENGER.Party> parties = TelepresenceEndpoint(party);

                    for (int i = 0; i < parties.Count; i++)
                    {
                        //FB 2486 Start
                        if (party.cMcu.etType == NS_MESSENGER.MCU.eType.RMX) //&& party.bIsMessageOverlay
                        {
                            NS_POLYCOM.Polycom polycom = new NS_POLYCOM.Polycom(configParams);
                            ret3 = polycom.AddMessageToParty(conf, party, messageText, SendMsg); //FB 3059
                            if (!ret3)
                            {
                                this.errMsg = polycom.errMsg;
                                msg = polycom.errMsg;
                            }
                        }
                        //FB 2501 Call Monitoring Start
                        else if (party.cMcu.etType == NS_MESSENGER.MCU.eType.CISCOTP)
                        {
                            NS_CiscoTPServer.CiscoTPServer CiscoTPServer = new NS_CiscoTPServer.CiscoTPServer(configParams);
                            ret3 = CiscoTPServer.SendConferenceEndpointmessage(conf, party, messageText, direction, duration);
                            if (!ret3)
                            {
                                this.errMsg = CiscoTPServer.errMsg;
                            }
                        }
                        else if (party.cMcu.etType == NS_MESSENGER.MCU.eType.CODIAN)
                        {
                            NS_CODIAN.Codian codian = new NS_CODIAN.Codian(configParams);
                            logger.Trace("Entering Codian sub-system...");
                            ret3 = codian.ParticipantMessage(conf, party, messageText, direction, duration);
                            if (!ret3)
                            {
                                this.errMsg = codian.errMsg;
                            }
                        }
                        //FB 2501 Call Monitoring End   
                        //FB 2556 start
                        else if (party.cMcu.etType == NS_MESSENGER.MCU.eType.RADVISIONIVIEW)
                        {
                            NS_RADVISION_IView.RadVisionIView RadIview = new NS_RADVISION_IView.RadVisionIView(configParams);
                            ret3 = RadIview.ParticipantMessage(conf, party, messageText, duration);
                            if (!ret3)
                            {
                                this.errMsg = RadIview.errMsg;
                            }
                        }
                        //FB 2556 End

                        else
                        {
                            this.errMsg = "Operation not supported on MCU.";
                            return false;
                        }

                        //FB 2486 End
                    }
                    //FB 2584 End
                }
                if (!ret3)
                {
                    logger.Trace("Send message text operation failed for endpoint.");
                    //FB 2569 Start
                    alert = new NS_MESSENGER.Alert();
                    alert.confID = conf.iDbID;
                    alert.instanceID = conf.iInstanceID;
                    alert.message = this.errMsg;
                    alert.typeID = 13; //Endpoint message
                    bool retp2p = db.InsertConfAlert(alert);
                    if (!retp2p)
                        logger.Trace("Endpoint message alert insert failed.");
                    //FB 2569 End
                    return false;
                }

                return (true);
            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
                return (false);
            }
        }
        #endregion

        #region SendMessageToConference
        internal bool SendMessageToConference(string inXml, ref string outXml, ref string msg)
        {
            try
            {
                //ZD 102998 Starts
                bool ret = false, ret1 = false, ret3 = false;
                List<int> mcuIdList = new List<int>();
                int confId = 0, instanceId = 0;
                //ZD 102998 Ends
                logger.Trace("Entering the SendMessageToConference function...");
                // Load the string into xml parser and retrieve the tag values.
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(inXml);

                string login = xd.SelectSingleNode("//login/userID").InnerXml.Trim();

                string strConfId = xd.SelectSingleNode("//login/confID").InnerXml.Trim();
                
                ret = ExtractConfInstanceIds(strConfId, ref confId, ref instanceId);
                if (!ret)
                {
                    logger.Trace("Invalid conf/instance id.");
                    return false;
                }

                //int endpointId = Int32.Parse(xd.SelectSingleNode("//login/endpointID").InnerXml.Trim()); // database id

                //int terminalType = Int32.Parse(xd.SelectSingleNode("//login/terminalType").InnerXml.Trim()); // 1 = user , 2 = room, 3 = guest , 4 = cascade				
                //logger.Trace("Terminaltype = " + terminalType.ToString());
                string messageText = "";
                if (xd.SelectSingleNode("//login/messageText") != null)
                    messageText = xd.SelectSingleNode("//login/messageText").InnerXml.Trim(); // message to be sent to endpoint
                logger.Trace("Message Text = " + messageText);

                //FB 2501 Call Monitoring Start

                int direction = 0, duration = 0, mcuId = 0, sendmsg = 0;//FB 3059

                if (xd.SelectSingleNode("//login/direction") != null)
                    Int32.TryParse(xd.SelectSingleNode("//login/direction").InnerXml.Trim(), out direction);

                if (xd.SelectSingleNode("//login/duration") != null)
                    Int32.TryParse(xd.SelectSingleNode("//login/duration").InnerXml.Trim(), out duration);

                if (xd.SelectSingleNode("//login/mcuId") != null)
                    Int32.TryParse(xd.SelectSingleNode("//login/mcuId").InnerXml.Trim(), out mcuId);

                //FB 2501 Call Monitoring End

                if (xd.SelectSingleNode("//login/SendMsg") != null) //FB 3059
                    Int32.TryParse(xd.SelectSingleNode("//login/SendMsg").InnerXml.Trim(), out sendmsg);


                // Fetch the conf unique name since thats the unique identifier of the conf.
                NS_MESSENGER.Conference conf = new NS_MESSENGER.Conference();
                NS_DATABASE.Database db = new NS_DATABASE.Database(configParams);
                conf.iDbID = confId; conf.iInstanceID = instanceId;
                ret = db.FetchConf(ref conf);
                if (!ret)
                {
                    logger.Exception(200, "Database fetch operation failed.");
                    return (false);
                }
                logger.Trace("mcuId : " + mcuId.ToString());
                //ZD 102998 Starts
                if (mcuId > 0)//FB 2560
                    mcuIdList.Add(mcuId);
                else
                {
                    bool ret2 = db.FetchConfMcu(conf, ref mcuIdList);
                    if (!ret2)
                    {
                        logger.Exception(200, "Database fetch operation failed.");
                        return (false);
                    }
                }
                //ZD 102998 Ends
                for (int k = 0; k < mcuIdList.Count; k++)
                {
                    mcuId = mcuIdList[k];
                    ret1 = db.FetchMcu(mcuId, ref conf.cMcu);
                    if (!ret1)
                    {
                        logger.Trace("Error fetching mcu details.");
                        return (false);
                    }
                    ret3 = false;
                    if (conf.etType == NS_MESSENGER.Conference.eType.POINT_TO_POINT)
                    {
                        // fetch other endpoints in conference
                        #region Fetch the endpoints
                        ret = false;
                        ret = db.FetchRooms(conf.iDbID, conf.iInstanceID, ref conf.qParties, 0);
                        if (!ret)
                        {
                            logger.Exception(100, "Error in fetching rooms from db.");
                            return false;
                        }
                        ret = false;
                        ret = db.FetchUsers(conf.iDbID, conf.iInstanceID, ref conf.qParties, 0);
                        if (!ret)
                        {
                            logger.Exception(100, "Error in fetching users from db.");
                            return false;
                        }
                        ret = false;
                        ret = db.FetchGuests(conf.iDbID, conf.iInstanceID, ref conf.qParties, 0);
                        if (!ret)
                        {
                            logger.Exception(100, "Error in fetching guests from db.");
                            return false;
                        }
                        ret = false;
                        Queue cascadeLinks = new Queue();
                        ret = db.FetchCascadeLinks(conf.iDbID, conf.iInstanceID, ref cascadeLinks, 0);
                        if (!ret)
                        {
                            logger.Exception(100, "Error in fetching cascade links from db.");
                            return false;
                        }
                        #endregion

                        System.Collections.IEnumerator partyEnumerator;
                        partyEnumerator = conf.qParties.GetEnumerator();
                        int partyCount = conf.qParties.Count;
                        for (int i = 0; i < partyCount; i++)
                        {
                            // go to next party in the queue
                            partyEnumerator.MoveNext();

                            NS_MESSENGER.Party party1 = new NS_MESSENGER.Party();
                            party1 = (NS_MESSENGER.Party)partyEnumerator.Current;

                            //ZD 101363 Start
                            if (party1.etCallerCallee == NS_MESSENGER.Party.eCallerCalleeType.CALLER)
                            {
                                if (party1.iSSHSupport == 1)
                                {
                                    NS_SSH.SSH SSH = new NS_SSH.SSH(this.configParams);
                                    ret3 = SSH.SendMessageTextToParty(party1, messageText, conf);
                                }
                                else
                                {
                                    // connect call on telnet 
                                    NS_TELNET.Telnet telnet = new NS_TELNET.Telnet(this.configParams);
                                    ret3 = telnet.SendMessageTextToParty(party1, messageText, conf); //FB 2501 P2P Call Monitoring     
                                }
                            }
                            //ZD 101363 End
                        }
                    }
                    else
                    {
                        //FB 2501 Call Monitoring Start
                        if (conf.cMcu.etType == NS_MESSENGER.MCU.eType.CISCOTP)
                        {
                            NS_CiscoTPServer.CiscoTPServer CiscoTPServer = new NS_CiscoTPServer.CiscoTPServer(configParams);
                            ret3 = CiscoTPServer.SendConferencemessage(conf, messageText, direction, duration);
                            if (!ret3)
                                this.errMsg = CiscoTPServer.errMsg;
                        }
                        //FB 2501 Call Monitoring End  
                        // connect to the bridge and perform the operation.               
                        //FB 2560 Start
                        else if (conf.cMcu.etType == NS_MESSENGER.MCU.eType.ACCORDv6 || conf.cMcu.etType == NS_MESSENGER.MCU.eType.ACCORDv7 || conf.cMcu.etType == NS_MESSENGER.MCU.eType.RMX)
                        {
                            ret3 = SendMessage(conf, messageText, 0, 0, sendmsg); //FB 3059
                            if (!ret3)
                                this.errMsg = "Message ont sent";
                        }
                        //FB 2560 End
                        //FB 3059 Start
                        else if (conf.cMcu.etType == NS_MESSENGER.MCU.eType.CODIAN)
                        {
                            ret3 = SendMessage(conf, messageText, direction, duration, sendmsg);
                            if (!ret3)
                                this.errMsg = "Message ont sent";
                        }
                        //FB 3059 End
                        //// FB 2441 Starts // ZD 102998  - Commented as this DMA API limitation
                        //else if (conf.cMcu.etType == NS_MESSENGER.MCU.eType.RPRM)
                        //{
                        //    NS_POLYCOMRPRM.POLYCOMRPRM polycomRPRM = new NS_POLYCOMRPRM.POLYCOMRPRM(configParams);
                        //    ret3 = polycomRPRM.ConferenceMessage(conf, messageText);
                        //    if (!ret3)
                        //        this.errMsg = polycomRPRM.errMsg;
                        //}
                        ////FB 2441 Ends
                        // FB 2556 Starts
                        else if (conf.cMcu.etType == NS_MESSENGER.MCU.eType.RADVISIONIVIEW)
                        {
                            NS_RADVISION_IView.RadVisionIView RadIview = new NS_RADVISION_IView.RadVisionIView(configParams);
                            ret3 = RadIview.ConferenceMessage(conf, messageText, duration);
                            if (!ret3)
                                this.errMsg = RadIview.errMsg;
                        }
                        //FB 2556 Ends
                        else
                        {
                            this.errMsg = "Operation not supported on Selected MCU.";
                            return false;
                        }
                    }
                }
                if (!ret3)
                {
                    logger.Trace("Send message text operation failed for endpoint.");
                    //FB 2569 Start
                    alert = new NS_MESSENGER.Alert();
                    alert.confID = conf.iDbID;
                    alert.instanceID = conf.iInstanceID;
                    alert.message = this.errMsg;
                    alert.typeID = 12; //Conference message
                    bool retp2p = db.InsertConfAlert(alert);
                    if (!retp2p)
                        logger.Trace("Conference message alert insert failed.");
                    //FB 2569 End
                    return false;
                }

                return (true);
            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
                return (false);
            }
        }
        #endregion

        //FB 2560 Start

        #region SendMessageto RMX & Codian
        internal bool SendMessage(NS_MESSENGER.Conference conf, string Message, int direction, int duration, int sendmsg) //FB 3059
        {
            try
            {
                bool ret = false;
                NS_DATABASE.Database db = new NS_DATABASE.Database(configParams);

                #region Fetch the endpoints
                ret = false;
                ret = db.FetchRooms(conf.iDbID, conf.iInstanceID, ref conf.qParties, 0);
                if (!ret)
                {
                    logger.Exception(100, "Error in fetching rooms from db.");
                    return false;
                }
                ret = false;
                ret = db.FetchUsers(conf.iDbID, conf.iInstanceID, ref conf.qParties, 0);
                if (!ret)
                {
                    logger.Exception(100, "Error in fetching users from db.");
                    return false;
                }
                ret = false;
                ret = db.FetchGuests(conf.iDbID, conf.iInstanceID, ref conf.qParties, 0);
                if (!ret)
                {
                    logger.Exception(100, "Error in fetching guests from db.");
                    return false;
                }
                ret = false;
                Queue cascadeLinks = new Queue();
                ret = db.FetchCascadeLinks(conf.iDbID, conf.iInstanceID, ref cascadeLinks, 0);
                if (!ret)
                {
                    logger.Exception(100, "Error in fetching cascade links from db.");
                    return false;
                }
                #endregion

                logger.Trace(" Entering Party Message Overlay ...");

                // Fetch all the bridge ids on which this conf is running.
                System.Collections.IEnumerator partyEnumerator;
                partyEnumerator = conf.qParties.GetEnumerator();
                int partyCount = conf.qParties.Count;
                logger.Trace("Party Count = " + partyCount.ToString());

                Queue mcuIdList = new Queue();
                bool ret3 = false;
                for (int i = 0; i < partyCount; i++)
                {
                    ret3 = partyEnumerator.MoveNext();
                    if (!ret3) break;

                    NS_MESSENGER.Party party = new NS_MESSENGER.Party();
                    party = (NS_MESSENGER.Party)partyEnumerator.Current;

                    logger.Trace("Party Address : " + party.sAddress.ToString());

                    if (party.cMcu.etType == NS_MESSENGER.MCU.eType.RMX)
                    {
                        if (party.bIsMessageOverlay || sendmsg == 1) //FB 3059
                        {
                            logger.Trace("ENTERING OVERLAY");
                            NS_POLYCOM.Polycom polycom = new NS_POLYCOM.Polycom(configParams);
                            ret3 = polycom.AddMessageToParty(conf, party, Message, sendmsg); //FB 3059
                            if (!ret3)
                            {
                                this.errMsg = polycom.errMsg;
                                logger.Trace("MEssage Overlay error : " + this.errMsg);
                            }
                        }
                    }
                    //FB 3059 Start
                    else if (party.cMcu.etType == NS_MESSENGER.MCU.eType.CODIAN)
                    {
                        NS_CODIAN.Codian codian = new NS_CODIAN.Codian(configParams);
                        ret3 = codian.ParticipantMessage(conf, party, Message, direction, duration);
                        if (!ret3)
                        {
                            this.errMsg = codian.errMsg;
                        }
                    }
                    //FB 3059 End
                }
                logger.Trace("Leaving Party Message Overlay operation.");
                return true;
            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
                return (false);
            }
        }

        #endregion

        //FB 2560 End


        #region Commented
        /*
        internal bool GetP2PConfStatus(string inXml, ref string outXml, ref string msg)
        {
            try
            {
                logger.Trace("Entering the GetP2PConfStatus function...");
                // Load the string into xml parser and retrieve the tag values.
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(inXml);

                string login = xd.SelectSingleNode("//login/userID").InnerXml.Trim();

                string strConfId = xd.SelectSingleNode("//login/confID").InnerXml.Trim();
                int confId = 0, instanceId = 0;
                bool ret = ExtractConfInstanceIds(strConfId, ref confId, ref instanceId);
                if (!ret)
                {
                    logger.Trace("Invalid conf/instance id.");
                    return false;
                }

                //int endpointId = Int32.Parse(xd.SelectSingleNode("//login/endpointID").InnerXml.Trim()); // database id

                //int terminalType = Int32.Parse(xd.SelectSingleNode("//login/terminalType").InnerXml.Trim()); // 1 = user , 2 = room, 3 = guest , 4 = cascade				
                //logger.Trace("Terminaltype = " + terminalType.ToString());

                
                // Fetch the conf unique name since thats the unique identifier of the conf.
                NS_MESSENGER.Conference conf = new NS_MESSENGER.Conference();
                NS_DATABASE.Database db = new NS_DATABASE.Database(configParams);
                conf.iDbID = confId; conf.iInstanceID = instanceId;
                ret = db.FetchConf(ref conf);
                if (!ret)
                {
                    logger.Exception(200, "Database fetch operation failed.");
                    return (false);
                }

                // Retreive the bridge info for that endpoint.
                NS_MESSENGER.Party party = new NS_MESSENGER.Party();
                switch (terminalType)
                {
                    case 1:
                        {
                            // user
                            bool ret2 = db.FetchUser(confId, instanceId, endpointId, ref party);
                            break;
                        }
                    case 2:
                        {
                            // room
                            logger.Trace("In room...");
                            bool ret2 = db.FetchRoom(confId, instanceId, endpointId, ref party);
                            break;
                        }
                    case 3:
                        {
                            // guest
                            bool ret2 = db.FetchGuest(confId, instanceId, endpointId, ref party);
                            break;
                        }
                    case 4:
                        {
                            // cascade
                            bool ret2 = db.FetchCascadeLink(confId, instanceId, endpointId, ref party);
                            break;
                        }
                }

                bool ret3 = false;
                if (conf.etType == NS_MESSENGER.Conference.eType.POINT_TO_POINT)
                {
                    // fetch other endpoints in conference
                    #region Fetch the endpoints
                    ret = false;
                    ret = db.FetchRooms(conf.iDbID, conf.iInstanceID, ref conf.qParties, 0);
                    if (!ret)
                    {
                        logger.Exception(100, "Error in fetching rooms from db.");
                        return false;
                    }
                    ret = false;
                    ret = db.FetchUsers(conf.iDbID, conf.iInstanceID, ref conf.qParties, 0);
                    if (!ret)
                    {
                        logger.Exception(100, "Error in fetching users from db.");
                        return false;
                    }
                    ret = false;
                    ret = db.FetchGuests(conf.iDbID, conf.iInstanceID, ref conf.qParties, 0);
                    if (!ret)
                    {
                        logger.Exception(100, "Error in fetching guests from db.");
                        return false;
                    }
                    ret = false;
                    Queue cascadeLinks = new Queue();
                    ret = db.FetchCascadeLinks(conf.iDbID, conf.iInstanceID, ref cascadeLinks, 0);
                    if (!ret)
                    {
                        logger.Exception(100, "Error in fetching cascade links from db.");
                        return false;
                    }
                    #endregion                   

                    // connect call on telnet 
                    NS_TELNET.Telnet telnet = new NS_TELNET.Telnet(this.configParams);
                    NS_MESSENGER.Party.eOngoingStatus onlineStatus;
                    //ret3 = telnet.isCallerConnected(ref conf );
                    msg = "<GetP2PConfStatus>";
                    msg += "<confID>" + strConfId  + "</confID>";
                    msg +="<endpointID/>";
                    if (ret3)
                    {
                        onlineStatus = NS_MESSENGER.Party.eOngoingStatus.FULL_CONNECT;
                        msg += "<connectionStatus>1</connectionStatus>";//<!--0=disconnect,2=connecting,1=connected, -1=N/A-->
                    }
                    else
                    {
                        onlineStatus = NS_MESSENGER.Party.eOngoingStatus.DIS_CONNECT;
                        msg += "<connectionStatus>0</connectionStatus>";//<!--0=disconnect,2=connecting,1=connected, -1=N/A-->                        
                    }
                    msg += "</GetP2PConfStatus>";

                    // update the terminal status in the db                     
                    db.UpdateConferenceUserStatus(confId, instanceId, -1, onlineStatus);
                    db.UpdateConferenceRoomStatus(confId, instanceId, -1, onlineStatus);
                    db.UpdateConferenceCascadeStatus(confId, instanceId, -1, onlineStatus);
                    
                    // update conf last run date time
                    db.UpdateP2PConferenceLastRunDateTime(confId, instanceId);

                }
                else
                {
                    this.errMsg = "Wrong conference type.";
                    return false;
                }

                return (true);
            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
                return (false);
            }
        }
*/
        #endregion

        #region GetTerminalStatus
        internal bool GetTerminalStatus(string inXml, ref string outXml, ref string msg)
        {
            bool isP2PCall = false;// FB 2710
            try
            {
                logger.Trace("Entering the function...");
                // Load the string into xml parser and retrieve the tag values.
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(inXml);

                string login = xd.SelectSingleNode("//GetTerminalStatus/login/userID").InnerXml.Trim();

                string strConfId = xd.SelectSingleNode("//GetTerminalStatus/login/confID").InnerXml.Trim();
                int confId = 0, instanceId = 0;
                bool ret = ExtractConfInstanceIds(strConfId, ref confId, ref instanceId);
                if (!ret)
                {
                    logger.Trace("Invalid conf/instance id.");
                    return false;
                }

                int endpointId = Int32.Parse(xd.SelectSingleNode("//GetTerminalStatus/login/endpointID").InnerXml.Trim()); // database id

                int terminalType = Int32.Parse(xd.SelectSingleNode("//GetTerminalStatus/login/terminalType").InnerXml.Trim()); // 1 = user , 2 = room, 3 = guest , 4 = cascade				
                logger.Trace("Terminaltype = " + terminalType.ToString());

                // Fetch the conf unique name since thats the unique identifier of the conf.
                NS_MESSENGER.Conference conf = new NS_MESSENGER.Conference();
                NS_DATABASE.Database db = new NS_DATABASE.Database(configParams);
                conf.iDbID = confId; conf.iInstanceID = instanceId;
                ret = db.FetchConf(ref conf);
                ret = db.GetandSetMCUStartandEnd(ref conf); //ZD 100085
                if (!ret)
                {
                    logger.Exception(200, "Database fetch operation failed.");
                    return (false);
                }

                // Retreive the bridge info for that endpoint.
                NS_MESSENGER.Party party = new NS_MESSENGER.Party();
                switch (terminalType)
                {
                    case 1:
                        {
                            // user
                            bool ret2 = db.FetchUserDetail(confId, instanceId, endpointId, ref party);//FB 1552
                            break;
                        }
                    case 2:
                        {
                            // room
                            logger.Trace("In room...");
                            bool ret2 = db.FetchRoomDetail(confId, instanceId, endpointId, ref party);//FB 1552
                            break;
                        }
                    case 3:
                        {
                            // guest
                            //bool ret2 = db.FetchGuest(confId, instanceId, endpointId, ref party);
                            bool ret2 = db.FetchGuestDetail(confId, instanceId, endpointId, ref party);//FB 1552
                            break;
                        }
                    case 4:
                        {
                            // cascade
                            bool ret2 = db.FetchCascadeLink(confId, instanceId, endpointId, ref party);
                            break;
                        }
                }

                bool ret5 = false;
                #region Check & connect if P2P call
                if (conf.etType == NS_MESSENGER.Conference.eType.POINT_TO_POINT)
                {


                    #region Fetch the endpoints
                    ret = false;
                    ret = db.FetchRooms(conf.iDbID, conf.iInstanceID, ref conf.qParties, 0);
                    if (!ret)
                    {
                        logger.Exception(100, "Error in fetching rooms from db.");
                        return false;
                    }
                    ret = false;
                    ret = db.FetchUsers(conf.iDbID, conf.iInstanceID, ref conf.qParties, 0);
                    if (!ret)
                    {
                        logger.Exception(100, "Error in fetching users from db.");
                        return false;
                    }
                    ret = false;
                    ret = db.FetchGuests(conf.iDbID, conf.iInstanceID, ref conf.qParties, 0);
                    if (!ret)
                    {
                        logger.Exception(100, "Error in fetching guests from db.");
                        return false;
                    }
                    ret = false;

                    #endregion
                    isP2PCall = true; //FB 2710
                    System.Collections.IEnumerator partyEnumerator;
                    partyEnumerator = conf.qParties.GetEnumerator();
                    int partyCount = conf.qParties.Count;
                    NS_MESSENGER.Party tmpEndpoint = new NS_MESSENGER.Party();
                    NS_MESSENGER.Party secondaryEndpoint = new NS_MESSENGER.Party();
                    for (int i = 0; i < partyCount; i++)
                    {

                        // go to next party in the queue
                        partyEnumerator.MoveNext();
                        tmpEndpoint = (NS_MESSENGER.Party)partyEnumerator.Current;
                        //tmpEndpoint.sLogin = "admin"; //GP Issue FB 2434
                        // compare tmp to primary. if match, copy the contents of tmp => primary
                        if (party.iDbId == tmpEndpoint.iDbId)
                        {
                            if (party.etCallerCallee == NS_MESSENGER.Party.eCallerCalleeType.CALLEE) //FB 2710
                                return true;

                            party.etModelType = tmpEndpoint.etModelType;
                            //party.iDbId = otherEndpoint.iDbId;
                            party.sAddress = tmpEndpoint.sAddress;
                            party.sLogin = tmpEndpoint.sLogin;
                            party.sPwd = tmpEndpoint.sPwd;
                            party.etProtocol = tmpEndpoint.etProtocol;
                            party.etModelType = tmpEndpoint.etModelType;
                            party.iAPIPortNo = tmpEndpoint.iAPIPortNo;
                            party.iSSHSupport = tmpEndpoint.iSSHSupport;//ZD 101363
                        }
                        else
                        {
                            // copy the contents of tmp => Secondary                            
                            secondaryEndpoint.etModelType = tmpEndpoint.etModelType;
                            //party.iDbId = otherEndpoint.iDbId;
                            secondaryEndpoint.sAddress = tmpEndpoint.sAddress;
                            secondaryEndpoint.sLogin = tmpEndpoint.sLogin;
                            secondaryEndpoint.sPwd = tmpEndpoint.sPwd;
                            secondaryEndpoint.etProtocol = tmpEndpoint.etProtocol;
                            secondaryEndpoint.etModelType = tmpEndpoint.etModelType;
                            secondaryEndpoint.iAPIPortNo = tmpEndpoint.iAPIPortNo;
                            secondaryEndpoint.iSSHSupport = tmpEndpoint.iSSHSupport;//ZD 101363
                        }

                    }
                    //ZD 101363 Start
                    if (party.etCallerCallee == NS_MESSENGER.Party.eCallerCalleeType.CALLER)
                    {
                        if (party.iSSHSupport == 1)
                        {
                            // connect call on ssh 
                            NS_SSH.SSH SSH = new NS_SSH.SSH(this.configParams);
                            ret5 = SSH.isEndpointConnected(ref party, secondaryEndpoint, conf);
                        }
                        else
                        {
                            // connect call on telnet 
                            NS_TELNET.Telnet telnet = new NS_TELNET.Telnet(this.configParams);
                            //bool ret5 = telnet.ConnectDisconnectCall(conf, null, false);

                            ret5 = telnet.isEndpointConnected(ref party, secondaryEndpoint, conf); //FB 2501 P2P Call Monitoring
                        }
                    }
                    //ZD 101363 End
                }
                #endregion
                else
                {
                    //bool ret3 = false; //Yorktel Fix
                    ret5 = false; //Yorktel Fix
                    // connect to the bridge and perform the operation.
                    //FB 2400 code changes start...
                    List<NS_MESSENGER.Party> parties = new List<NS_MESSENGER.Party>();

                    if (party.MulticodecAddress == null || party.MulticodecAddress == "")
                        party.isTelePresence = false;

                    if (party.isTelePresence)
                    {
                        String partyName = "";
                        NS_MESSENGER.Party partyMulti = null;
                        String[] MultiAdds = party.MulticodecAddress.Split('�');
                        for (int a = 0; a < MultiAdds.Length; a++)
                        {
                            partyMulti = new NS_MESSENGER.Party();
                            db.SetPartyProperties(ref partyMulti, ref party);
                            partyMulti.iDbId = party.iDbId + a;

                            if (a > 0)
                                partyMulti.iDbId += 5000;

                            partyName = party.sName;
                            if (MultiAdds[a].Trim() != "")
                            {
                                partyMulti.sAddress = MultiAdds[a].Trim();
                                partyName += a + 1; //FB 2493

                                partyMulti.sName = partyName;
                            }
                            parties.Add(partyMulti);
                        }
                    }
                    else
                        parties.Add(party);

                    for (int i = 0; i < parties.Count; i++)
                    {
                        NS_MESSENGER.Party participant = parties[i];

                        if (participant.cMcu.etType == NS_MESSENGER.MCU.eType.ACCORDv6 || participant.cMcu.etType == NS_MESSENGER.MCU.eType.ACCORDv7 || participant.cMcu.etType == NS_MESSENGER.MCU.eType.RMX)
                        {
                            NS_POLYCOM.Polycom polycom = new NS_POLYCOM.Polycom(configParams);
                            ret5 = polycom.OngoingConfOps("GetTerminalStatus", conf, participant, ref msg, false, 0, 0, false, ref participant.etStatus, false, false); //Yorktel Fix //FB 2553-RMX
                            if (!ret5) //Yorktel Fix
                            {
                                this.errMsg = polycom.errMsg;
                                msg = polycom.errMsg;
                            }
                        }
                        else
                        {
                            if (participant.cMcu.etType == NS_MESSENGER.MCU.eType.CODIAN)
                            {
                                NS_CODIAN.Codian codian = new NS_CODIAN.Codian(configParams);
                                ret5 = codian.GetTerminalStatus(conf, ref participant); //Yorktel Fix //FB 2501 Call Moniotring
                                if (!ret5) //Yorktel Fix
                                {
                                    this.errMsg = codian.errMsg;
                                }
                            }
                            else
                            {
                                //FB 1552 - start
                                if (participant.cMcu.etType == NS_MESSENGER.MCU.eType.TANDBERG)
                                {
                                    NS_TANDBERG.Tandberg tandberg = new NS_TANDBERG.Tandberg(configParams);
                                    ret5 = tandberg.GetEndpointStatus(conf, ref participant);
                                    if (!ret5)
                                    {
                                        errMsg = tandberg.errMsg;
                                    }
                                }
                                else if (participant.cMcu.etType == NS_MESSENGER.MCU.eType.RADVISION)
                                {
                                    NS_RADVISION.Radvision radVision = new NS_RADVISION.Radvision(configParams);
                                    ret5 = radVision.GetEndpointStatus(conf, ref participant);
                                    if (!ret5)
                                    {
                                        errMsg = radVision.errMsg;
                                    }
                                }
                                //FB 2441 Starts
                                else if (participant.cMcu.etType == NS_MESSENGER.MCU.eType.RPRM)
                                {
                                    ret5 = false;
                                    ret5 = db.FetchMcu(participant.cMcu.iDbId, ref conf.cMcu);
                                    if (!ret5)
                                        return false;
                                    NS_POLYCOMRPRM.POLYCOMRPRM polycomRPRM = new NS_POLYCOMRPRM.POLYCOMRPRM(configParams);

                                    //ZD 100685 Starts
                                    if (conf.iStatus == 6 && string.IsNullOrEmpty(conf.sGUID))
                                    {
                                        ret5 = false;
                                        ret5 = polycomRPRM.GetDMAConferenceDetails(conf);
                                    }
                                    //ZD 10685 Ends
                                    ret5 = false;
                                    ret5 = polycomRPRM.GetEndpointStatus(conf, ref participant); // ZD 101157
                                    if (!ret5)
                                    {
                                        ret5 = true;// FOr RPRM if no reply then party is disconnected // ZD 101157
                                        errMsg = polycomRPRM.errMsg;
                                        participant.etStatus = NS_MESSENGER.Party.eOngoingStatus.DIS_CONNECT;
                                    }
                                    else //ZD 102515 START
                                    {
                                        alert = new NS_MESSENGER.Alert();
                                        alert.confID = conf.iDbID;
                                        alert.instanceID = conf.iInstanceID;
                                        alert.message = this.errMsg;
                                        bool retp2p = db.DeleteRPRMConfAlert(alert);
                                        if (!retp2p)
                                            logger.Trace("Endpoint ConnectDisconnect Sucsessfull alert.");
                                        return true;
                                    }//ZD 102515 END
                                }
                                //FB 2441 Ends
                                //FB 2501 Call Monitoring Start
                                else if (participant.cMcu.etType == NS_MESSENGER.MCU.eType.CISCOTP)
                                {
                                    NS_CiscoTPServer.CiscoTPServer CiscoTPServer = new NS_CiscoTPServer.CiscoTPServer(configParams);
                                    NS_MESSENGER.Smtp smtpServerInfo = new NS_MESSENGER.Smtp();
                                    logger.Trace("Fetching smtp server info...");
                                    ret5 = db.FetchSmtpServerInfo(ref smtpServerInfo);
                                    if (!ret5)
                                    {
                                        logger.Trace("Error retreiving SMTP server info from db.");
                                        return false;
                                    }
                                    if (smtpServerInfo.ServerAddress.Length < 1)
                                    {
                                        logger.Exception(100, "Mail server not valid");
                                        return false;
                                    }
                                    ret5 = CiscoTPServer.GetEndpointStatus(conf, smtpServerInfo, ref participant);
                                    if (!ret5)
                                    {
                                        errMsg = CiscoTPServer.errMsg;
                                    }
                                }
                                //FB 2501 Call Monitoring End
                                //FB 2556 Starts
                                else if (participant.cMcu.etType == NS_MESSENGER.MCU.eType.RADVISIONIVIEW)
                                {
                                    NS_RADVISION_IView.RadVisionIView radiview = new NS_RADVISION_IView.RadVisionIView(configParams);
                                    ret5 = radiview.GetEndpointStatus(conf, ref participant);
                                    if (!ret5)
                                        errMsg = radiview.errMsg;
                                }
                                //FB 2556 Ends
                                //FB 2718 Starts
                                else if (participant.cMcu.etType == NS_MESSENGER.MCU.eType.TMSScheduling)
                                {
                                    NS_TMS.TMSScheduling TMS = new NS_TMS.TMSScheduling(configParams);
                                    conf.cMcu = participant.cMcu; //ZD Latest Issue
                                    ret5 = TMS.GetEndpointStatus(conf, ref participant);
                                    if (!ret5)
                                        logger.Trace("Error in GetEndpointStatus");
                                }
                                //FB 2718 Ends
                                //ZD 101522 Starts
                                else if (participant.cMcu.etType == NS_MESSENGER.MCU.eType.Pexip)
                                {
                                    NS_Pexip.pexip pexip = new NS_Pexip.pexip(configParams);
                                    ret5 = pexip.GetEndpointStatus(conf, ref participant);
                                    if (!ret5)
                                        logger.Trace("Error in GetEndpointStatus");
                                }
                                //ZD 101522 Ends
                                else
                                {
                                    this.errMsg = "Operation not supported on MCU.";
                                    return false;
                                }
                                //FB 1552 - end
                            }
                        }
                        if (!ret5) //Yorktel Fix
                        {
                            logger.Trace("GetTerminalStatus operation failed for endpoint.");
                            return false;
                        }

                        /* Party's Multi Address status logic
                        1.All connected -  2
                        2.All Disconnected - 0
                        3.Any one status diff with other -  1 (connecting) */

                        if (i == 0)
                            party.etStatus = participant.etStatus;
                        else if (i > 0 && party.etStatus != participant.etStatus)
                            party.etStatus = NS_MESSENGER.Party.eOngoingStatus.PARTIAL_CONNECT;
                    }

                    //FB 2400 end
                }

                // update the terminal status in the db   
                if (!ret5) return false; // don't update the db unless a response is received. 

                //To stop double update we need to find long term solution.
                if (party.cMcu.etType != NS_MESSENGER.MCU.eType.CISCOTP && party.cMcu.etType != NS_MESSENGER.MCU.eType.RPRM && !isP2PCall) //FB 2710 //ALLBUGS-49
                {

                    if (party.etType == NS_MESSENGER.Party.eType.USER || party.etType == NS_MESSENGER.Party.eType.GUEST)
                    {
                        db.UpdateConferenceUserStatus(confId, instanceId, endpointId, party.etStatus);
                    }
                    else
                    {
                        if (party.etType == NS_MESSENGER.Party.eType.ROOM)
                        {
                            db.UpdateConferenceRoomStatus(confId, instanceId, endpointId, party.etStatus);
                        }
                        else
                        {
                            //ZD 100768, We are supporting only 2 MCU cascade as of now so a general update is correct logic 
                            //db.UpdateConferenceCascadeStatus(confId, instanceId, endpointId, party.etStatus)
                            db.UpdateConferenceCascadeStatus(confId, instanceId, -1, party.etStatus);
                        }
                    }
                }

                if (isP2PCall)// FB 2710
                    db.UpdateP2PConferenceEndpointStatus(confId, instanceId,party.etStatus);


                // Construct the return xml
                msg = "<GetTerminalStatus>";
                msg += "<confID>" + strConfId + "</confID>";
                msg += "<endpointID>" + endpointId.ToString() + "</endpointID>";

                if (party.etStatus == NS_MESSENGER.Party.eOngoingStatus.DIS_CONNECT)
                {
                    msg += "<connectionStatus>0</connectionStatus>"; //<!--0=disconnect,1=connecting,2=connected -->
                    msg += "<remoteEndpoint></remoteEndpoint>";
                }
                else
                {
                    if (party.etStatus == NS_MESSENGER.Party.eOngoingStatus.PARTIAL_CONNECT)
                    {
                        msg += "<connectionStatus>1</connectionStatus>"; //Connecting
                        msg += "<remoteEndpoint></remoteEndpoint>";
                    }
                    else
                    {
                        if (party.etStatus == NS_MESSENGER.Party.eOngoingStatus.ONLINE)
                        {
                            msg += "<connectionStatus>3</connectionStatus>"; // value??? ... Online - 3
                            msg += "<remoteEndpoint></remoteEndpoint>";
                        }
                        else if (party.etStatus == NS_MESSENGER.Party.eOngoingStatus.FULL_CONNECT)
                        {
                            msg += "<connectionStatus>2</connectionStatus>"; //Connected
                            msg += "<remoteEndpoint></remoteEndpoint>";
                        }
                        else
                        {
                            msg += "<connectionStatus>-1</connectionStatus>"; //Unreachable
                            msg += "<remoteEndpoint></remoteEndpoint>";
                        }
                    }
                }
                
                msg += "</GetTerminalStatus>";

                logger.Trace("GetTerminalStatus outxml = " + msg);

                return (true);
            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
                return (false);
            }
        }
        #endregion

        //FB 2448
        #region FetchConfMCUDetails
        /// <summary>
        /// Get the conference bridge Details
        /// </summary>
        internal bool FetchConfMCUDetails(string inXml)
        {
            try
            {
                string login = "", strConfId = "";
                int confId = 0, instanceId = 0;
                int Timeout = 7500; //ZD 100369_MCU
                bool ret = false;
                List<NS_MESSENGER.MCU> mcus = null;
                NS_MESSENGER.MCU mcu = null;
                XmlDocument xd = new XmlDocument();
                NS_DATABASE.Database db = new NS_DATABASE.Database(configParams);
                logger.Trace("Entering the Getting MCU Details method...");
                
                xd.LoadXml(inXml);
                login = xd.SelectSingleNode("//GetConfMCUDetails/UserID").InnerXml.Trim();
                strConfId = xd.SelectSingleNode("//GetConfMCUDetails/ConfID").InnerXml.Trim();

                ret = ExtractConfInstanceIds(strConfId, ref confId, ref instanceId);
                if (!ret)
                {
                    logger.Trace("Invalid conf/instance id.");
                    return false;
                }

                db.FetchConfMcus(confId, instanceId, ref mcus);
                for (int m = 0; m < mcus.Count; m++)
                {
                    mcu = mcus[m];
                    switch (mcu.sMcuType)
                    {
                        case 0:
                        case 1:
                        case 2:
                        case 3:
                            {
                                // Its a Polycom MCU.
                                NS_POLYCOM.Polycom accord = new NS_POLYCOM.Polycom(configParams);
                                ret = accord.FetchMCUDetails(mcu);
                                if (!ret)
                                    this.errMsg = accord.errMsg;
                                
                                break;
                            }
                        case 4:
                        case 5:
                        case 6:
                            {
                                // Its a Codian MCU
                                NS_CODIAN.Codian codian = new NS_CODIAN.Codian(configParams);
                                ret = codian.FetchMCUDetails(mcu, Timeout);//ZD 100369_MCU
                                if (!ret)
                                    this.errMsg = codian.errMsg;
                                
                                break;
                            }
                        case 7:
                            {
                                // Its a Tandberg MCU
                                NS_TANDBERG.Tandberg tandberg = new NS_TANDBERG.Tandberg(configParams);
                                mcu.dblSoftwareVer = 0.0;
                                ret = false;
                                ret = tandberg.TestMCUConnection(mcu);
                                if (!ret)
                                    this.errMsg = tandberg.errMsg;
                                
                                break;
                            }
                        //ZD 103787- It is for Polycom RMX MCU
                        case 17:
                        case 18:
                        case 19:
                        case 8:
                            {
                                // Its a Polycom RMX.
                                NS_POLYCOM.Polycom rmx = new NS_POLYCOM.Polycom(configParams);
                                mcu.etType = NS_MESSENGER.MCU.eType.RMX;
                                ret = rmx.FetchMCUDetails(mcu);
                                if (!ret)
                                    this.errMsg = rmx.errMsg;
                                
                                break;
                            }
                        case 9:
                            {
                                // Radvision Scopia.
                                NS_RADVISION.Radvision rad = new NS_RADVISION.Radvision(configParams);
                                mcu.etType = NS_MESSENGER.MCU.eType.RADVISION;
                                ret = rad.TestMCUConnection(mcu);
                                if (!ret)
                                    this.errMsg = rad.errMsg;
                                
                                break;
                            }
                        /***FB polycom CMA FB 2441**/
                        case 10:
                            {
                                //// Its a polycom CMA MCU
                                //mcu.sIp = xd.SelectSingleNode("//setBridge/bridge/bridgeDetails/controlPortIPAddress").InnerXml.Trim();
                                //mcu.etType = NS_MESSENGER.MCU.eType.Lifesize;
                                //mcu.dblSoftwareVer = 0.0;

                                //// Check connection to bridge. 				
                                //NS_POLYCOMCMA.PolycomCMA polycomCMA = new NS_POLYCOMCMA.PolycomCMA(configParams);
                                //ret = false;
                                //ret = polycomCMA.TestMCUConnection(mcu);
                                //if (!ret)
                                //{
                                //    this.errMsg = tandberg.errMsg;
                                //}
                                break;
                            }
                        /***FB 2261**/
                        case 11:
                            {
                                // Its a lifesize MCU
                                NS_LifeSize.LifeSize lifeSize = new NS_LifeSize.LifeSize(configParams);
                                mcu.etType = NS_MESSENGER.MCU.eType.Lifesize;
                                mcu.dblSoftwareVer = 0.0;
                                ret = false;
                                ret = lifeSize.TestMCUConnection(mcu);
                                if (!ret)
                                    this.errMsg = lifeSize.errMsg;
                                
                                break;
                            }
                        //FB 2501 Call Monitoring Start
                        case 12:
                            {   
                                // Its a Codian MSE 8710 MCU
                                NS_CiscoTPServer.CiscoTPServer CiscoTPServer = new NS_CiscoTPServer.CiscoTPServer(configParams);
                                ret = CiscoTPServer.FetchMCUDetails(mcu, Timeout); //ZD 100369_MCU
                                if (!ret)
                                    this.errMsg = CiscoTPServer.errMsg;

                                break;
                            }
                        //FB 2501 Call Monitoring End
                        //FB 2441 Starts
                        case 13:
                            {
                                Timeout = 30000; //ZD 
                                NS_POLYCOMRPRM.POLYCOMRPRM polycomRPRM = new NS_POLYCOMRPRM.POLYCOMRPRM(configParams);
                                ret = polycomRPRM.FetchMCUDetails(mcu, Timeout); //ZD 100369_MCU
                                if (!ret)
                                    this.errMsg = polycomRPRM.errMsg;

                                break;
                            }
                        //FB 2441 Ends
                        // FB 2556 Starts-DoubtT
                        case 14:
                            {
                                // Its a RadvisionIView MCU
                                NS_RADVISION_IView.RadVisionIView radiview = new NS_RADVISION_IView.RadVisionIView(configParams);
                                //ret = radiview.FetchMCUDetails(mcu);
                                if (!ret)
                                    this.errMsg = radiview.errMsg;

                                break;
                            }
                        // FB 2556 Ends
                        //ZD 101522 Start
                        case 16:
                            {
                                // Its a Pexip MCU
                                NS_Pexip.pexip pexip = new NS_Pexip.pexip(configParams);
                                if (!ret)
                                    this.errMsg = pexip.errMsg;
                                break;
                            }
                        //ZD 101522 end
                        //ZD 104021 Start
                        case 20:
                            {
                                NS_BlueJean.BlueJean BJN = new NS_BlueJean.BlueJean(configParams);
                                if (!ret)
                                    this.errMsg = BJN.errMsg;
                                break;
                            }
                        //ZD 104021 end
                        default:
                            {
                                this.errMsg = "Unknown MCU type.";
                                return false;
                            }
                    }
                }
                if (!ret)
                {
                    logger.Trace("Getting MCU Details operation failed.");
                    logger.Trace("Msg = " + this.errMsg);
                    return false;
                }
                return true;
            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
                return (false);
            }
        }
        #endregion

        //FB 2501 Call Monitoring Start
        // Codian 8510
        #region ParticipantFECC
        /// <summary>
        /// Change display camera control of an ongoing endpoint on bridge.
        /// </summary>
        /// <param name="inXml"></param>
        /// <param name="outXml"></param>
        /// <param name="msg"></param>
        /// <returns></returns>
        internal bool ParticipantFECC(string inXml, ref string outXml, ref string msg)
        {
            try
            {
                logger.Trace("Entering the function...");
                // Load the string into xml parser and retrieve the tag values.
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(inXml);
                string login = "";
                string strConfId = "";
                string strEndpointId = "";

                if (xd.SelectSingleNode("//login/userID") != null)
                    login = xd.SelectSingleNode("//login/userID").InnerText.Trim();

                
                if (xd.SelectSingleNode("//login/confID") != null)
                    strConfId = xd.SelectSingleNode("//login/confID").InnerText.Trim(); ;
                int confId = 0, instanceId = 0;
                bool ret = ExtractConfInstanceIds(strConfId, ref confId, ref instanceId);
                if (!ret)
                {
                    logger.Trace("Invalid conf/instance id.");
                    return false;
                }

                // endpoint id
                if (xd.SelectSingleNode("//login/endpointID") != null)
                    strEndpointId = xd.SelectSingleNode("//login/endpointID").InnerText.Trim();
                int endpointId = 0;
                if (strEndpointId.Length > 0)
                {
                    endpointId = Int32.Parse(strEndpointId);
                }
                logger.Trace("EndpointId = " + endpointId.ToString());

                string strTerminalType = xd.SelectSingleNode("//login/terminalType").InnerText.Trim(); // 1 = user , 2 = room, 3 = guest				 
                int terminalType = 0;
                if (strTerminalType.Length > 0)
                {
                    terminalType = Int32.Parse(strTerminalType);
                }

                logger.Trace("Terminaltype = " + terminalType.ToString());

                int direction = Int32.Parse(xd.SelectSingleNode("//login/Direction").InnerXml.Trim()); 

                // Fetch the conf unique name since thats the unique identifier of the conf.
                NS_DATABASE.Database db = new NS_DATABASE.Database(configParams);
                NS_MESSENGER.Conference conf = new NS_MESSENGER.Conference();
                conf.iDbID = confId;
                conf.iInstanceID = instanceId;
                ret = db.FetchConf(ref conf);
                if (!ret)
                {
                    logger.Exception(200, "Database fetch operation failed.");
                    return (false);
                }

                // Retreive the bridge info for that endpoint.
                NS_MESSENGER.Party party = new NS_MESSENGER.Party();
                switch (terminalType)
                {
                    case 1:
                        {
                            // user
                            bool ret2 = db.FetchUser(confId, instanceId, endpointId, ref party);
                            break;
                        }
                    case 2:
                        {
                            // room
                            logger.Trace("In room...");
                            bool ret2 = db.FetchRoom(confId, instanceId, endpointId, ref party);
                            break;
                        }
                    case 3:
                        {
                            // guest
                            bool ret2 = db.FetchGuest(confId, instanceId, endpointId, ref party);
                            break;
                        }
                }

                // connect to the bridge and perform the operation.
                bool ret3 = false;

                //FB 2584 Start

                List<NS_MESSENGER.Party> parties = TelepresenceEndpoint(party);

                for (int i = 0; i < parties.Count; i++)
                {
                    if (party.cMcu.etType == NS_MESSENGER.MCU.eType.CODIAN)
                    {
                        NS_CODIAN.Codian codian = new NS_CODIAN.Codian(configParams);
                        logger.Trace("Entering Codian sub-system...");
                        ret3 = codian.ParticipantFECC(conf, party, direction);
                        if (!ret3)
                        {
                            this.errMsg = codian.errMsg;
                        }
                    }//FB 2556
                    else if (party.cMcu.etType == NS_MESSENGER.MCU.eType.RADVISIONIVIEW)
                    {
                        NS_RADVISION_IView.RadVisionIView RadIview = new NS_RADVISION_IView.RadVisionIView(configParams);
                        logger.Trace("Entering Participant FECC sub-system...");
                        ret3 = RadIview.ParticipantFECC(conf, party, direction);
                        if (!ret3)
                        {
                            this.errMsg = RadIview.errMsg;
                        }
                    }
                }

                //FB 2584 End
                if (!ret3)
                {
                    logger.Trace("Change Participant FECC Direction operation failed for endpoint.");
                    //FB 2569 Start
                    alert = new NS_MESSENGER.Alert();
                    alert.confID = conf.iDbID;
                    alert.instanceID = conf.iInstanceID;
                    alert.message = this.errMsg;
                    alert.typeID = 14; //Endpoint fecc
                    bool retp2p = db.InsertConfAlert(alert);
                    if (!retp2p)
                        logger.Trace("Endpoint fecc alert insert failed.");
                    //FB 2569 End
                    return false;
                }


                return (true);
            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
                return (false);
            }
        }
        #endregion
        // Codian 8510
        #region ParticipantAudioGainMode
        /// <summary>
        /// Participant AudioGain Mode of an ongoing endpoint on bridge.
        /// </summary>
        /// <param name="inXml"></param>
        /// <param name="outXml"></param>
        /// <param name="msg"></param>
        /// <returns></returns>
        internal bool ParticipantAudioGainMode(string inXml, ref string outXml, ref string msg)
        {
            try
            {
                logger.Trace("Entering the function...");
                // Load the string into xml parser and retrieve the tag values.
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(inXml);

                string login = xd.SelectSingleNode("//login/userID").InnerXml.Trim(); ;

                string strConfId = xd.SelectSingleNode("//login/confID").InnerXml.Trim(); ;
                int confId = 0, instanceId = 0;
                bool ret = ExtractConfInstanceIds(strConfId, ref confId, ref instanceId);
                if (!ret)
                {
                    logger.Trace("Invalid conf/instance id.");
                    return false;
                }

                // endpoint id 
                string strEndpointId = xd.SelectSingleNode("//login/endpointID").InnerXml.Trim();
                int endpointId = 0;
                if (strEndpointId.Length > 0)
                {
                    endpointId = Int32.Parse(strEndpointId);
                }
                logger.Trace("EndpointId = " + endpointId.ToString());

                string strTerminalType = xd.SelectSingleNode("//login/terminalType").InnerXml.Trim(); // 1 = user , 2 = room, 3 = guest				 
                int terminalType = 0;
                if (strTerminalType.Length > 0)
                {
                    terminalType = Int32.Parse(strTerminalType);
                }

                logger.Trace("Terminaltype = " + terminalType.ToString());

                int Mode = Int32.Parse(xd.SelectSingleNode("//login/AudioMode").InnerXml.Trim());

                int AudioGain = Int32.Parse(xd.SelectSingleNode("//login/AudioGain").InnerXml.Trim());

                // Fetch the conf unique name since thats the unique identifier of the conf.
                NS_DATABASE.Database db = new NS_DATABASE.Database(configParams);
                NS_MESSENGER.Conference conf = new NS_MESSENGER.Conference();
                conf.iDbID = confId;
                conf.iInstanceID = instanceId;
                ret = db.FetchConf(ref conf);
                if (!ret)
                {
                    logger.Exception(200, "Database fetch operation failed.");
                    return (false);
                }

                // Retreive the bridge info for that endpoint.
                NS_MESSENGER.Party party = new NS_MESSENGER.Party();
                switch (terminalType)
                {
                    case 1:
                        {
                            // user
                            bool ret2 = db.FetchUser(confId, instanceId, endpointId, ref party);
                            break;
                        }
                    case 2:
                        {
                            // room
                            logger.Trace("In room...");
                            bool ret2 = db.FetchRoom(confId, instanceId, endpointId, ref party);
                            break;
                        }
                    case 3:
                        {
                            // guest
                            bool ret2 = db.FetchGuest(confId, instanceId, endpointId, ref party);
                            break;
                        }
                }

                // connect to the bridge and perform the operation.
                bool ret3 = false;

                //FB 2584 Start

                List<NS_MESSENGER.Party> parties = TelepresenceEndpoint(party);

                for (int i = 0; i < parties.Count; i++)
                {
                    if (party.cMcu.etType == NS_MESSENGER.MCU.eType.ACCORDv6 || party.cMcu.etType == NS_MESSENGER.MCU.eType.ACCORDv7 || party.cMcu.etType == NS_MESSENGER.MCU.eType.RMX)
                    {

                        //logger.Trace("Entering Accord sub-system...");
                        //ret3 = polycom.OngoingConfOps("ParticipantFECC", conf, party, ref msg, true, displayLayout, 0, false, ref party.etStatus);
                        //if (!ret3)
                        //{
                        //    this.errMsg = polycom.errMsg;
                        //}
                    }
                    else
                    {
                        if (party.cMcu.etType == NS_MESSENGER.MCU.eType.CODIAN)
                        {
                            NS_CODIAN.Codian codian = new NS_CODIAN.Codian(configParams);
                            logger.Trace("Entering Codian sub-system...");
                            ret3 = codian.AudioGain(conf, party, Mode, AudioGain);
                            if (!ret3)
                            {
                                this.errMsg = codian.errMsg;
                            }
                        }
                        else
                        {
                            // Tandberg MCU
                            //NS_TANDBERG.Tandberg tandberg = new NS_TANDBERG.Tandberg(configParams);
                            //ret3 = tandberg.ParticipantFECC(conf, party, displayLayout);
                            //if (!ret3)
                            //{
                            //    this.errMsg = tandberg.errMsg;
                            //}
                        }
                    }
                }
                //FB 2584 End
                if (!ret3)
                {
                    logger.Trace("Change Participant FECC Direction operation failed for endpoint.");
                    return false;
                }


                return (true);
            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
                return (false);
            }
        }
        #endregion

        #region MuteTransmitEndpoint
        /// <summary>
        /// MuteTransmitEndpoint
        /// </summary>
        /// <param name="inXml"></param>
        /// <param name="outXml"></param>
        /// <param name="msg"></param>
        /// <returns></returns>
        internal bool MuteTransmitEndpoint(string inXml, ref string outXml, ref string msg)
        {
            try
            {
                logger.Trace("Entering the function...");
                // Load the string into xml parser and retrieve the tag values.
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(inXml);

                string login = xd.SelectSingleNode("//login/userID").InnerXml.Trim();

                string strConfId = xd.SelectSingleNode("//login/confID").InnerXml.Trim();
                int confId = 0, instanceId = 0;
                bool ret = ExtractConfInstanceIds(strConfId, ref confId, ref instanceId);
                if (!ret)
                {
                    logger.Trace("Invalid conf/instance id.");
                    return false;
                }

                int endpointId = Int32.Parse(xd.SelectSingleNode("//login/endpointID").InnerXml.Trim()); // database id

                int terminalType = Int32.Parse(xd.SelectSingleNode("//login/terminalType").InnerXml.Trim()); // 1 = user , 2 = room, 3 = guest , 4 = cascade				
                logger.Trace("Terminaltype = " + terminalType.ToString());

                int iMute = Int32.Parse(xd.SelectSingleNode("//login/mute").InnerXml.Trim()); // 0 = off, 1 = on
                bool mute = false;
                if (iMute == 1)
                {
                    mute = true;
                }

                //FB 2530 Start

                string strmuteAll = xd.SelectSingleNode("//login/muteAll").InnerText.Trim(); // 1 - Party Mute  2 - Party All Mute
                int muteAll = 0;
                Int32.TryParse(strmuteAll, out muteAll);

                //FB 2530 End

                // Fetch the conf unique name since thats the unique identifier of the conf.
                NS_MESSENGER.Conference conf = new NS_MESSENGER.Conference();
                NS_DATABASE.Database db = new NS_DATABASE.Database(configParams);
                conf.iDbID = confId; conf.iInstanceID = instanceId;
                ret = db.FetchConf(ref conf);
                if (!ret)
                {
                    logger.Exception(200, "Database fetch operation failed.");
                    return (false);
                }

                //FB 2530 Start

                #region Mute Party
                if (muteAll == (int)eMuteType.PARTY_MUTE) //FB 2530
                {
                    // Retreive the bridge info for that endpoint.
                    NS_MESSENGER.Party party = new NS_MESSENGER.Party();
                    switch (terminalType)
                    {
                        case 1:
                            {
                                // user
                                bool ret2 = db.FetchUser(confId, instanceId, endpointId, ref party);
                                break;
                            }
                        case 2:
                            {
                                // room
                                logger.Trace("In room...");
                                bool ret2 = db.FetchRoom(confId, instanceId, endpointId, ref party);
                                break;
                            }
                        case 3:
                            {
                                // guest
                                bool ret2 = db.FetchGuest(confId, instanceId, endpointId, ref party);
                                break;
                            }
                        case 4:
                            {
                                // cascade
                                bool ret2 = db.FetchCascadeLink(confId, instanceId, endpointId, ref party);
                                break;
                            }
                    }

                    // connect to the bridge and perform the operation.
                    bool ret3 = false;

                    //FB 2584 Start

                    List<NS_MESSENGER.Party> parties = TelepresenceEndpoint(party);

                    for (int i = 0; i < parties.Count; i++)
                    {
                        if (party.cMcu.etType == NS_MESSENGER.MCU.eType.ACCORDv6 || party.cMcu.etType == NS_MESSENGER.MCU.eType.ACCORDv7 || party.cMcu.etType == NS_MESSENGER.MCU.eType.RMX)
                        {
                            //FB 2553-RMX
                            //NS_POLYCOM.Polycom accord = new NS_POLYCOM.Polycom(configParams);
                            ret3 = polycom.OngoingConfOps("MuteTransmitTerminal", conf, party, ref msg, mute, 0, 0, false, ref party.etStatus, false, false);
                            if (!ret3)
                            {
                                this.errMsg = polycom.errMsg;
                                msg = polycom.errMsg;
                            }
                        }
                        else
                        {
                            if (party.cMcu.etType == NS_MESSENGER.MCU.eType.CODIAN)
                            {
                                NS_CODIAN.Codian codian = new NS_CODIAN.Codian(configParams);
                                ret3 = codian.MuteTransmitEndpoint(conf, party, mute);
                                if (!ret3)
                                {
                                    this.errMsg = codian.errMsg;
                                }
                            }
                            //FB 2501 Call Monitoring Start
                            else if (party.cMcu.etType == NS_MESSENGER.MCU.eType.CISCOTP)
                            {
                                NS_CiscoTPServer.CiscoTPServer CiscoTPServer = new NS_CiscoTPServer.CiscoTPServer(configParams);
                                ret3 = CiscoTPServer.MuteAudioTransmitEndpoint(conf, party, mute);
                                if (!ret3)
                                {
                                    this.errMsg = CiscoTPServer.errMsg;
                                }
                            }
                            //FB 2501 Call Monitoring End
                            else
                            {
                                if (party.cMcu.etType == NS_MESSENGER.MCU.eType.TANDBERG)
                                {
                                    //NS_TANDBERG.Tandberg tandberg = new NS_TANDBERG.Tandberg(configParams);
                                    //ret3 = tandberg.MuteEndpoint(conf, party, mute);
                                    //if (!ret3)
                                    //{
                                    //    this.errMsg = tandberg.errMsg;
                                    //}
                                }
                                else
                                {
                                    //NS_RADVISION.Radvision rad = new NS_RADVISION.Radvision(configParams);
                                    //ret3 = rad.MuteEndpoint(conf, party, mute);
                                    //if (!ret3)
                                    //{
                                    //    this.errMsg = rad.errMsg;
                                    //}
                                }
                            }
                        }

                    }
                    //FB 2584 End
                    if (!ret3)
                    {
                        logger.Trace("Mute operation failed for endpoint.");
                        //FB 2569 Start
                        alert = new NS_MESSENGER.Alert();
                        alert.confID = conf.iDbID;
                        alert.instanceID = conf.iInstanceID;
                        alert.message = this.errMsg;
                        alert.typeID = 9; //Endpoint mute/unmute
                        bool retp2p = db.InsertConfAlert(alert);
                        if (!retp2p)
                            logger.Trace("Endpoint mute/unmute alert insert failed.");
                        //FB 2569 End
                        return false;
                    }
                }
                #endregion

                #region Mute all Party
                else
                {
                    #region Fetch the endpoints
                    ret = false;
                    ret = db.FetchRooms(conf.iDbID, conf.iInstanceID, ref conf.qParties, 0);
                    if (!ret)
                    {
                        logger.Exception(100, "Error in fetching rooms from db.");
                        return false;
                    }
                    ret = false;
                    ret = db.FetchUsers(conf.iDbID, conf.iInstanceID, ref conf.qParties, 0);
                    if (!ret)
                    {
                        logger.Exception(100, "Error in fetching users from db.");
                        return false;
                    }
                    ret = false;
                    ret = db.FetchGuests(conf.iDbID, conf.iInstanceID, ref conf.qParties, 0);
                    if (!ret)
                    {
                        logger.Exception(100, "Error in fetching guests from db.");
                        return false;
                    }
                    ret = false;
                    Queue cascadeLinks = new Queue();
                    ret = db.FetchCascadeLinks(conf.iDbID, conf.iInstanceID, ref cascadeLinks, 0);
                    if (!ret)
                    {
                        logger.Exception(100, "Error in fetching cascade links from db.");
                        return false;
                    }
                    #endregion

                    logger.Trace(" Entering Party Mute All ...");

                    // Fetch all the bridge ids on which this conf is running.
                    System.Collections.IEnumerator partyEnumerator;
                    partyEnumerator = conf.qParties.GetEnumerator();
                    int partyCount = conf.qParties.Count;
                    logger.Trace("Party Count = " + partyCount.ToString());

                    Queue mcuIdList = new Queue();
                    bool ret3 = false;
                    for (int i = 0; i < partyCount; i++)
                    {
                        ret3 = partyEnumerator.MoveNext();
                        if (!ret3) break;

                        NS_MESSENGER.Party party = new NS_MESSENGER.Party();
                        party = (NS_MESSENGER.Party)partyEnumerator.Current;

                        logger.Trace("Party Address : " + party.sAddress.ToString());

                        if (party.cMcu.etType == NS_MESSENGER.MCU.eType.ACCORDv6 || party.cMcu.etType == NS_MESSENGER.MCU.eType.ACCORDv7 || party.cMcu.etType == NS_MESSENGER.MCU.eType.RMX)
                        {
                            //FB 2553-RMX
                            //ret3 = polycom.OngoingConfOps("MuteEndpoint", conf, party, ref msg, mute, 0, 0, false, ref party.etStatus);
                            ret3 = polycom.OngoingConfOps("MuteTransmitTerminal", conf, party, ref msg, false, 0, 0, false, ref party.etStatus, mute, false);
                            if (!ret3)
                            {
                                this.errMsg = polycom.errMsg;
                                msg = polycom.errMsg;
                            }
                        }
                        else
                        {
                            if (party.cMcu.etType == NS_MESSENGER.MCU.eType.CODIAN)
                            {
                                NS_CODIAN.Codian codian = new NS_CODIAN.Codian(configParams);
                                ret3 = codian.MuteTransmitEndpoint(conf, party, mute);
                                if (!ret3)
                                {
                                    this.errMsg = codian.errMsg;
                                }
                            }
                            //FB 2501 Call Monitoring Start
                            else if (party.cMcu.etType == NS_MESSENGER.MCU.eType.CISCOTP)
                            {
                                NS_CiscoTPServer.CiscoTPServer CiscoTPServer = new NS_CiscoTPServer.CiscoTPServer(configParams);
                                ret3 = CiscoTPServer.MuteAudioTransmitEndpoint(conf, party, mute);
                                if (!ret3)
                                {
                                    this.errMsg = CiscoTPServer.errMsg;
                                }
                            }
                            //FB 2501 Call Monitoring End
                            else
                            {
                                if (party.cMcu.etType == NS_MESSENGER.MCU.eType.TANDBERG)
                                {
                                    //NS_TANDBERG.Tandberg tandberg = new NS_TANDBERG.Tandberg(configParams);
                                    //ret3 = tandberg.MuteEndpoint(conf, party, mute);
                                    //if (!ret3)
                                    //{
                                    //    this.errMsg = tandberg.errMsg;
                                    //}
                                }
                                else
                                {
                                    //NS_RADVISION.Radvision rad = new NS_RADVISION.Radvision(configParams);
                                    //ret3 = rad.MuteEndpoint(conf, party, mute);
                                    //if (!ret3)
                                    //{
                                    //    this.errMsg = rad.errMsg;
                                    //}
                                }

                            }
                            if (!ret3)
                            {
                                logger.Trace("Mute All operation failed.");
                                //FB 2569 Start
                                alert = new NS_MESSENGER.Alert();
                                alert.confID = conf.iDbID;
                                alert.instanceID = conf.iInstanceID;
                                alert.message = this.errMsg;
                                alert.typeID = 9; //Endpoint mute/unmute
                                bool retp2p = db.InsertConfAlert(alert);
                                if (!retp2p)
                                    logger.Trace("Endpoint mute/unmute alert insert failed.");
                                //FB 2569 End
                                //return false; commented for FB 2501 
                            }
                        }
                        logger.Trace("Leaving Mute All operation.");
                    }
                }
                #endregion

                //FB 2350 End

                return (true);
            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
                return (false);
            }
        }
        #endregion

        //Codian 8710
        #region MuteTransmitVideoEndpoint
        /// <summary>
        /// MuteTransmitVideoEndpoint
        /// </summary>
        /// <param name="inXml"></param>
        /// <param name="outXml"></param>
        /// <param name="msg"></param>
        /// <returns></returns>
        internal bool MuteTransmitVideoEndpoint(string inXml, ref string outXml, ref string msg)
        {
            try
            {
                logger.Trace("Entering the function...");
                // Load the string into xml parser and retrieve the tag values.
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(inXml);

                string login = xd.SelectSingleNode("//login/userID").InnerXml.Trim();

                string strConfId = xd.SelectSingleNode("//login/confID").InnerXml.Trim();
                int confId = 0, instanceId = 0;
                bool ret = ExtractConfInstanceIds(strConfId, ref confId, ref instanceId);
                if (!ret)
                {
                    logger.Trace("Invalid conf/instance id.");
                    return false;
                }

                int endpointId = Int32.Parse(xd.SelectSingleNode("//login/endpointID").InnerXml.Trim()); // database id

                int terminalType = Int32.Parse(xd.SelectSingleNode("//login/terminalType").InnerXml.Trim()); // 1 = user , 2 = room, 3 = guest , 4 = cascade				
                logger.Trace("Terminaltype = " + terminalType.ToString());

                int iMute = Int32.Parse(xd.SelectSingleNode("//login/mute").InnerXml.Trim()); // 0 = off, 1 = on
                bool mute = false;
                if (iMute == 1)
                {
                    mute = true;
                }

                //FB 2530 Start

                string strmuteAll = xd.SelectSingleNode("//login/muteAll").InnerText.Trim(); // 1 - Party Mute  2 - Party All Mute
                int muteAll = 0;
                Int32.TryParse(strmuteAll, out muteAll);

                //FB 2530 End

                // Fetch the conf unique name since thats the unique identifier of the conf.
                NS_MESSENGER.Conference conf = new NS_MESSENGER.Conference();
                NS_DATABASE.Database db = new NS_DATABASE.Database(configParams);
                conf.iDbID = confId; conf.iInstanceID = instanceId;
                ret = db.FetchConf(ref conf);
                if (!ret)
                {
                    logger.Exception(200, "Database fetch operation failed.");
                    return (false);
                }

                //FB 2530 Start

                #region Mute Party
                if (muteAll == (int)eMuteType.PARTY_MUTE) //FB 2530
                {
                    // Retreive the bridge info for that endpoint.
                    NS_MESSENGER.Party party = new NS_MESSENGER.Party();
                    switch (terminalType)
                    {
                        case 1:
                            {
                                // user
                                bool ret2 = db.FetchUser(confId, instanceId, endpointId, ref party);
                                break;
                            }
                        case 2:
                            {
                                // room
                                logger.Trace("In room...");
                                bool ret2 = db.FetchRoom(confId, instanceId, endpointId, ref party);
                                break;
                            }
                        case 3:
                            {
                                // guest
                                bool ret2 = db.FetchGuest(confId, instanceId, endpointId, ref party);
                                break;
                            }
                        case 4:
                            {
                                // cascade
                                bool ret2 = db.FetchCascadeLink(confId, instanceId, endpointId, ref party);
                                break;
                            }
                    }

                    // connect to the bridge and perform the operation.
                    bool ret3 = false;

                    //FB 2584 Start

                    List<NS_MESSENGER.Party> parties = TelepresenceEndpoint(party);

                    for (int i = 0; i < parties.Count; i++)
                    {
                        if (party.cMcu.etType == NS_MESSENGER.MCU.eType.ACCORDv6 || party.cMcu.etType == NS_MESSENGER.MCU.eType.ACCORDv7 || party.cMcu.etType == NS_MESSENGER.MCU.eType.RMX)
                        {
                            ////NS_POLYCOM.Polycom accord = new NS_POLYCOM.Polycom(configParams);
                            //ret3 = polycom.OngoingConfOps("MuteEndpoint", conf, party, ref msg, mute, 0, 0, false, ref party.etStatus);
                            //if (!ret3)
                            //{
                            //    this.errMsg = polycom.errMsg;
                            //    msg = polycom.errMsg;
                            //}
                        }
                        else
                        {
                            if (party.cMcu.etType == NS_MESSENGER.MCU.eType.CODIAN)
                            {
                                //NS_CODIAN.Codian codian = new NS_CODIAN.Codian(configParams);
                                //ret3 = codian.MuteTransmitEndpoint(conf, party, mute);
                                //if (!ret3)
                                //{
                                //    this.errMsg = codian.errMsg;
                                //}
                            }
                            //FB 2501 Call Monitoring Start
                            else if (party.cMcu.etType == NS_MESSENGER.MCU.eType.CISCOTP)
                            {
                                NS_CiscoTPServer.CiscoTPServer CiscoTPServer = new NS_CiscoTPServer.CiscoTPServer(configParams);
                                ret3 = CiscoTPServer.MuteVideoTransmitEndpoint(conf, party, mute);
                                if (!ret3)
                                {
                                    this.errMsg = CiscoTPServer.errMsg;
                                }
                            }
                            //FB 2501 Call Monitoring End
                            else
                            {
                                if (party.cMcu.etType == NS_MESSENGER.MCU.eType.TANDBERG)
                                {
                                    //NS_TANDBERG.Tandberg tandberg = new NS_TANDBERG.Tandberg(configParams);
                                    //ret3 = tandberg.MuteEndpoint(conf, party, mute);
                                    //if (!ret3)
                                    //{
                                    //    this.errMsg = tandberg.errMsg;
                                    //}
                                }
                                else
                                //FB 2441 Starts
                                if (party.cMcu.etType == NS_MESSENGER.MCU.eType.RPRM)
                                {
                                    NS_POLYCOMRPRM.POLYCOMRPRM polycomRPRM = new NS_POLYCOMRPRM.POLYCOMRPRM(configParams);
                                    ret3 = polycomRPRM.MuteEndpoint(conf, party, mute, (int)AudioVideoParty.Video);
                                    if (!ret3)
                                    {
                                        this.errMsg = polycomRPRM.errMsg;
                                    }
                                }
                                //FB 2441 Ends
                                else
                                {
                                    //NS_RADVISION.Radvision rad = new NS_RADVISION.Radvision(configParams);
                                    //ret3 = rad.MuteEndpoint(conf, party, mute);
                                    //if (!ret3)
                                    //{
                                    //    this.errMsg = rad.errMsg;
                                    //}
                                }
                            }

                        }
                    }
                    //FB 2584 End
                    if (!ret3)
                    {
                        logger.Trace("Mute operation failed for endpoint.");
                        //FB 2569 Start
                        alert = new NS_MESSENGER.Alert();
                        alert.confID = conf.iDbID;
                        alert.instanceID = conf.iInstanceID;
                        alert.message = this.errMsg;
                        alert.typeID = 9; //Endpoint mute/unmute
                        bool retp2p = db.InsertConfAlert(alert);
                        if (!retp2p)
                            logger.Trace("Endpoint mute/unmute alert insert failed.");
                        //FB 2569 End
                        return false;
                    }
                }
                #endregion

                #region Mute all Party
                else
                {
                    #region Fetch the endpoints
                    ret = false;
                    ret = db.FetchRooms(conf.iDbID, conf.iInstanceID, ref conf.qParties, 0);
                    if (!ret)
                    {
                        logger.Exception(100, "Error in fetching rooms from db.");
                        return false;
                    }
                    ret = false;
                    ret = db.FetchUsers(conf.iDbID, conf.iInstanceID, ref conf.qParties, 0);
                    if (!ret)
                    {
                        logger.Exception(100, "Error in fetching users from db.");
                        return false;
                    }
                    ret = false;
                    ret = db.FetchGuests(conf.iDbID, conf.iInstanceID, ref conf.qParties, 0);
                    if (!ret)
                    {
                        logger.Exception(100, "Error in fetching guests from db.");
                        return false;
                    }
                    ret = false;
                    Queue cascadeLinks = new Queue();
                    ret = db.FetchCascadeLinks(conf.iDbID, conf.iInstanceID, ref cascadeLinks, 0);
                    if (!ret)
                    {
                        logger.Exception(100, "Error in fetching cascade links from db.");
                        return false;
                    }
                    #endregion

                    logger.Trace(" Entering Party Mute All ...");

                    // Fetch all the bridge ids on which this conf is running.
                    System.Collections.IEnumerator partyEnumerator;
                    partyEnumerator = conf.qParties.GetEnumerator();
                    int partyCount = conf.qParties.Count;
                    logger.Trace("Party Count = " + partyCount.ToString());

                    Queue mcuIdList = new Queue();
                    bool ret3 = false;
                    for (int i = 0; i < partyCount; i++)
                    {
                        ret3 = partyEnumerator.MoveNext();
                        if (!ret3) break;

                        NS_MESSENGER.Party party = new NS_MESSENGER.Party();
                        party = (NS_MESSENGER.Party)partyEnumerator.Current;

                        logger.Trace("Party Address : " + party.sAddress.ToString());

                        if (party.cMcu.etType == NS_MESSENGER.MCU.eType.ACCORDv6 || party.cMcu.etType == NS_MESSENGER.MCU.eType.ACCORDv7 || party.cMcu.etType == NS_MESSENGER.MCU.eType.RMX)
                        {
                            //ret3 = polycom.OngoingConfOps("MuteEndpoint", conf, party, ref msg, mute, 0, 0, false, ref party.etStatus);
                            //if (!ret3)
                            //{
                            //    this.errMsg = polycom.errMsg;
                            //    msg = polycom.errMsg;
                            //}
                        }
                        else
                        {
                            //FB 2501 Call Monitoring Start
                            if (party.cMcu.etType == NS_MESSENGER.MCU.eType.CISCOTP)
                            {
                                NS_CiscoTPServer.CiscoTPServer CiscoTPServer = new NS_CiscoTPServer.CiscoTPServer(configParams);
                                ret3 = CiscoTPServer.MuteVideoTransmitEndpoint(conf, party, mute);
                                if (!ret3)
                                {
                                    this.errMsg = CiscoTPServer.errMsg;
                                }
                            }
                            //FB 2501 Call Monitoring End
                            else
                            {
                                if (party.cMcu.etType == NS_MESSENGER.MCU.eType.TANDBERG)
                                {
                                    //NS_TANDBERG.Tandberg tandberg = new NS_TANDBERG.Tandberg(configParams);
                                    //ret3 = tandberg.MuteEndpoint(conf, party, mute);
                                    //if (!ret3)
                                    //{
                                    //    this.errMsg = tandberg.errMsg;
                                    //}
                                }
                                else if (party.cMcu.etType == NS_MESSENGER.MCU.eType.RPRM)//ZD 103198
                                {
                                    NS_POLYCOMRPRM.POLYCOMRPRM polycomRPRM = new NS_POLYCOMRPRM.POLYCOMRPRM(configParams);
                                    ret3 = polycomRPRM.MuteEndpoint(conf, party, mute, (int)AudioVideoParty.Video);
                                    if (!ret3)
                                    {
                                        this.errMsg = polycomRPRM.errMsg;
                                    }
                                }
                                else
                                {
                                    //NS_RADVISION.Radvision rad = new NS_RADVISION.Radvision(configParams);
                                    //ret3 = rad.MuteEndpoint(conf, party, mute);
                                    //if (!ret3)
                                    //{
                                    //    this.errMsg = rad.errMsg;
                                    //}
                                }

                            }
                            if (!ret3)
                            {
                                logger.Trace("Mute All operation failed.");
                                //FB 2569 Start
                                alert = new NS_MESSENGER.Alert();
                                alert.confID = conf.iDbID;
                                alert.instanceID = conf.iInstanceID;
                                alert.message = this.errMsg;
                                alert.typeID = 9; //Endpoint mute/unmute
                                bool retp2p = db.InsertConfAlert(alert);
                                if (!retp2p)
                                    logger.Trace("Endpoint mute/unmute alert insert failed.");
                                //FB 2569 End
                                //return false; commented for FB 2501 
                            }
                        }
                        logger.Trace("Leaving Mute All operation.");
                    }
                }
                #endregion

                //FB 2350 End

                return (true);
            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
                return (false);
            }
        }
        #endregion
        //Codian 8710
        #region MuteReceiveVideoEndpoint
        /// <summary>
        /// MuteReceiveVideoEndpoint
        /// </summary>
        /// <param name="inXml"></param>
        /// <param name="outXml"></param>
        /// <param name="msg"></param>
        /// <returns></returns>
        internal bool MuteReceiveVideoEndpoint(string inXml, ref string outXml, ref string msg)
        {
            try
            {
                logger.Trace("Entering the function...");
                // Load the string into xml parser and retrieve the tag values.
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(inXml);

                string login = xd.SelectSingleNode("//login/userID").InnerXml.Trim();

                string strConfId = xd.SelectSingleNode("//login/confID").InnerXml.Trim();
                int confId = 0, instanceId = 0;
                bool ret = ExtractConfInstanceIds(strConfId, ref confId, ref instanceId);
                if (!ret)
                {
                    logger.Trace("Invalid conf/instance id.");
                    return false;
                }

                int endpointId = Int32.Parse(xd.SelectSingleNode("//login/endpointID").InnerXml.Trim()); // database id

                int terminalType = Int32.Parse(xd.SelectSingleNode("//login/terminalType").InnerXml.Trim()); // 1 = user , 2 = room, 3 = guest , 4 = cascade				
                logger.Trace("Terminaltype = " + terminalType.ToString());

                int iMute = Int32.Parse(xd.SelectSingleNode("//login/mute").InnerXml.Trim()); // 0 = off, 1 = on
                bool mute = false;
                if (iMute == 1)
                {
                    mute = true;
                }

                //FB 2530 Start

                string strmuteAll = xd.SelectSingleNode("//login/muteAll").InnerText.Trim(); // 1 - Party Mute  2 - Party All Mute
                int muteAll = 0;
                Int32.TryParse(strmuteAll, out muteAll);

                //FB 2530 End

                // Fetch the conf unique name since thats the unique identifier of the conf.
                NS_MESSENGER.Conference conf = new NS_MESSENGER.Conference();
                NS_DATABASE.Database db = new NS_DATABASE.Database(configParams);
                conf.iDbID = confId; conf.iInstanceID = instanceId;
                ret = db.FetchConf(ref conf);
                if (!ret)
                {
                    logger.Exception(200, "Database fetch operation failed.");
                    return (false);
                }

                //FB 2530 Start

                #region Mute Party
                if (muteAll == (int)eMuteType.PARTY_MUTE) //FB 2530
                {
                    // Retreive the bridge info for that endpoint.
                    NS_MESSENGER.Party party = new NS_MESSENGER.Party();
                    switch (terminalType)
                    {
                        case 1:
                            {
                                // user
                                bool ret2 = db.FetchUser(confId, instanceId, endpointId, ref party);
                                break;
                            }
                        case 2:
                            {
                                // room
                                logger.Trace("In room...");
                                bool ret2 = db.FetchRoom(confId, instanceId, endpointId, ref party);
                                break;
                            }
                        case 3:
                            {
                                // guest
                                bool ret2 = db.FetchGuest(confId, instanceId, endpointId, ref party);
                                break;
                            }
                        case 4:
                            {
                                // cascade
                                bool ret2 = db.FetchCascadeLink(confId, instanceId, endpointId, ref party);
                                break;
                            }
                    }

                    // connect to the bridge and perform the operation.
                    bool ret3 = false;
                    //FB 2584 Start

                    List<NS_MESSENGER.Party> parties = TelepresenceEndpoint(party);

                    for (int i = 0; i < parties.Count; i++)
                    {
                        if (party.cMcu.etType == NS_MESSENGER.MCU.eType.ACCORDv6 || party.cMcu.etType == NS_MESSENGER.MCU.eType.ACCORDv7 || party.cMcu.etType == NS_MESSENGER.MCU.eType.RMX)
                        {
                            //FB 2553-RMX
                            //NS_POLYCOM.Polycom accord = new NS_POLYCOM.Polycom(configParams);
                            ret3 = polycom.OngoingConfOps("MuteVideoEndpoint", conf, party, ref msg, false, 0, 0, false, ref party.etStatus, mute, false); //FB 2553
                            if (!ret3)
                            {
                                this.errMsg = polycom.errMsg;
                                msg = polycom.errMsg;
                            }
                        }
                        else
                        {
                            if (party.cMcu.etType == NS_MESSENGER.MCU.eType.CODIAN)//FB 2553
                            {
                                NS_CODIAN.Codian codian = new NS_CODIAN.Codian(configParams);
                                ret3 = codian.MuteVideoReceiverEndpoint(conf, party, mute);
                                if (!ret3)
                                {
                                    this.errMsg = codian.errMsg;
                                }
                            }
                            //FB 2501 Call Monitoring Start
                            else if (party.cMcu.etType == NS_MESSENGER.MCU.eType.CISCOTP)
                            {
                                NS_CiscoTPServer.CiscoTPServer CiscoTPServer = new NS_CiscoTPServer.CiscoTPServer(configParams);
                                ret3 = CiscoTPServer.MuteVideoReceiveEndpoint(conf, party, mute);
                                if (!ret3)
                                {
                                    this.errMsg = CiscoTPServer.errMsg;
                                }
                            }
                            //FB 2501 Call Monitoring End
                            else
                            {
                                if (party.cMcu.etType == NS_MESSENGER.MCU.eType.TANDBERG)
                                {
                                    //NS_TANDBERG.Tandberg tandberg = new NS_TANDBERG.Tandberg(configParams);
                                    //ret3 = tandberg.MuteEndpoint(conf, party, mute);
                                    //if (!ret3)
                                    //{
                                    //    this.errMsg = tandberg.errMsg;
                                    //}
                                }
                                //FB 2556 Start
                                else if (party.cMcu.etType == NS_MESSENGER.MCU.eType.RADVISIONIVIEW)
                                {
                                    NS_RADVISION_IView.RadVisionIView RadIview = new NS_RADVISION_IView.RadVisionIView(configParams);
                                    ret3 = RadIview.MuteVideoEndpoint(conf, party, mute);
                                    if (!ret3)
                                    {
                                        this.errMsg = RadIview.errMsg;
                                    }
                                }
                                //FB 2556 End
                            }

                        }
                    }
                    //FB 2584 End
                    if (!ret3)
                    {
                        logger.Trace("Mute operation failed for endpoint.");
                        //FB 2569 Start
                        alert = new NS_MESSENGER.Alert();
                        alert.confID = conf.iDbID;
                        alert.instanceID = conf.iInstanceID;
                        alert.message = this.errMsg;
                        alert.typeID = 9; //Endpoint mute/unmute
                        bool retp2p = db.InsertConfAlert(alert);
                        if (!retp2p)
                            logger.Trace("Endpoint mute/unmute alert insert failed.");
                        //FB 2569 End
                        return false;
                    }
                }
                #endregion

                #region Mute all Party
                else
                {
                    #region Fetch the endpoints
                    ret = false;
                    ret = db.FetchRooms(conf.iDbID, conf.iInstanceID, ref conf.qParties, 0);
                    if (!ret)
                    {
                        logger.Exception(100, "Error in fetching rooms from db.");
                        return false;
                    }
                    ret = false;
                    ret = db.FetchUsers(conf.iDbID, conf.iInstanceID, ref conf.qParties, 0);
                    if (!ret)
                    {
                        logger.Exception(100, "Error in fetching users from db.");
                        return false;
                    }
                    ret = false;
                    ret = db.FetchGuests(conf.iDbID, conf.iInstanceID, ref conf.qParties, 0);
                    if (!ret)
                    {
                        logger.Exception(100, "Error in fetching guests from db.");
                        return false;
                    }
                    ret = false;
                    Queue cascadeLinks = new Queue();
                    ret = db.FetchCascadeLinks(conf.iDbID, conf.iInstanceID, ref cascadeLinks, 0);
                    if (!ret)
                    {
                        logger.Exception(100, "Error in fetching cascade links from db.");
                        return false;
                    }
                    #endregion

                    logger.Trace(" Entering Party Mute All ...");

                    // Fetch all the bridge ids on which this conf is running.
                    System.Collections.IEnumerator partyEnumerator;
                    partyEnumerator = conf.qParties.GetEnumerator();
                    int partyCount = conf.qParties.Count;
                    logger.Trace("Party Count = " + partyCount.ToString());

                    Queue mcuIdList = new Queue();
                    bool ret3 = false;
                    for (int i = 0; i < partyCount; i++)
                    {
                        ret3 = partyEnumerator.MoveNext();
                        if (!ret3) break;

                        NS_MESSENGER.Party party = new NS_MESSENGER.Party();
                        party = (NS_MESSENGER.Party)partyEnumerator.Current;

                        logger.Trace("Party Address : " + party.sAddress.ToString());

                        if (party.cMcu.etType == NS_MESSENGER.MCU.eType.ACCORDv6 || party.cMcu.etType == NS_MESSENGER.MCU.eType.ACCORDv7 || party.cMcu.etType == NS_MESSENGER.MCU.eType.RMX)
                        {
                            //FB 2553-RMX
                            //ret3 = polycom.OngoingConfOps("MuteEndpoint", conf, party, ref msg, mute, 0, 0, false, ref party.etStatus);
                            ret3 = polycom.OngoingConfOps("MuteVideoEndpoint", conf, party, ref msg, false, 0, 0, false, ref party.etStatus, mute, false);//FB 2553 RMX
                            if (!ret3)
                            {
                                this.errMsg = polycom.errMsg;
                                msg = polycom.errMsg;
                            }
                        }
                        else
                        {
                            if (party.cMcu.etType == NS_MESSENGER.MCU.eType.CODIAN)
                            {
                                NS_CODIAN.Codian codian = new NS_CODIAN.Codian(configParams);
                                ret3 = codian.MuteVideoReceiverEndpoint(conf, party, mute);
                                if (!ret3)
                                {
                                    this.errMsg = codian.errMsg;
                                }
                            }
                            //FB 2501 Call Monitoring Start
                            else if (party.cMcu.etType == NS_MESSENGER.MCU.eType.CISCOTP)
                            {
                                NS_CiscoTPServer.CiscoTPServer CiscoTPServer = new NS_CiscoTPServer.CiscoTPServer(configParams);
                                ret3 = CiscoTPServer.MuteVideoReceiveEndpoint(conf, party, mute);
                                if (!ret3)
                                {
                                    this.errMsg = CiscoTPServer.errMsg;
                                }
                            }
                            //FB 2501 Call Monitoring End
                            else
                            {
                                if (party.cMcu.etType == NS_MESSENGER.MCU.eType.TANDBERG)
                                {
                                    //NS_TANDBERG.Tandberg tandberg = new NS_TANDBERG.Tandberg(configParams);
                                    //ret3 = tandberg.MuteEndpoint(conf, party, mute);
                                    //if (!ret3)
                                    //{
                                    //    this.errMsg = tandberg.errMsg;
                                    //}
                                }
                                //FB 2556 Start
                                else if (party.cMcu.etType == NS_MESSENGER.MCU.eType.RADVISIONIVIEW)
                                {
                                    NS_RADVISION_IView.RadVisionIView RadIview = new NS_RADVISION_IView.RadVisionIView(configParams);
                                    ret3 = RadIview.MuteVideoEndpoint(conf, party, mute);
                                    if (!ret3)
                                    {
                                        this.errMsg = RadIview.errMsg;
                                    }
                                }
                                //FB 2556 End

                            }
                            if (!ret3)
                            {
                                logger.Trace("Mute All operation failed.");
                                //FB 2569 Start
                                alert = new NS_MESSENGER.Alert();
                                alert.confID = conf.iDbID;
                                alert.instanceID = conf.iInstanceID;
                                alert.message = this.errMsg;
                                alert.typeID = 9; //Endpoint mute/unmute
                                bool retp2p = db.InsertConfAlert(alert);
                                if (!retp2p)
                                    logger.Trace("Endpoint mute/unmute alert insert failed.");
                                //FB 2569 End
                                //return false;commented for FB 2501 
                            }
                        }
                        logger.Trace("Leaving Mute All operation.");
                    }
                }
                #endregion

                //FB 2350 End

                return (true);
            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
                return (false);
            }
        }
        #endregion
        //Codian 8710
        #region LockUnlockConference
        /// <summary>
        /// Mute an ongoing endpoint.
        /// </summary>
        /// <param name="inXml"></param>
        /// <param name="outXml"></param>
        /// <param name="msg"></param>
        /// <returns></returns>
        internal bool LockUnlockConference(string inXml, ref string outXml, ref string msg)
        {
            try
            {
                logger.Trace("Entering the function...");
                // Load the string into xml parser and retrieve the tag values.
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(inXml);

                string login = xd.SelectSingleNode("//login/userID").InnerXml.Trim();

                string strConfId = xd.SelectSingleNode("//login/confID").InnerXml.Trim();
                int confId = 0, instanceId = 0;
                bool ret = ExtractConfInstanceIds(strConfId, ref confId, ref instanceId);
                if (!ret)
                {
                    logger.Trace("Invalid conf/instance id.");
                    return false;
                }
				//FB 2501 Call Monitoring Start
                int duration = 0;
                if(xd.SelectSingleNode("//login/duration") != null)
                    Int32.TryParse(xd.SelectSingleNode("//login/duration").InnerXml.Trim(), out duration);

                int lockorunlock = 0;
                if (xd.SelectSingleNode("//login/lockorunlock") != null)
                    Int32.TryParse(xd.SelectSingleNode("//login/lockorunlock").InnerXml.Trim(), out lockorunlock); // 0 = off, 1 = on

                int mcuId = 0;
                if (xd.SelectSingleNode("//login/mcuId") != null)
                    Int32.TryParse(xd.SelectSingleNode("//login/mcuId").InnerXml.Trim(), out mcuId);
                //FB 2501 Call Monitoring End
                // Fetch the conf unique name since thats the unique identifier of the conf.
                NS_MESSENGER.Conference conf = new NS_MESSENGER.Conference();
                NS_DATABASE.Database db = new NS_DATABASE.Database(configParams);
                conf.iDbID = confId; conf.iInstanceID = instanceId;
                ret = db.FetchConf(ref conf);
                if (!ret)
                {
                    logger.Exception(200, "Database fetch operation failed.");
                    return (false);
                }


                if (mcuId >= 0)
                {
                    bool ret1 = db.FetchMcu(mcuId, ref conf.cMcu);
                    if (!ret1)
                    {
                        logger.Trace("Error fetching mcu details.");
                        return (false);
                    }
                }
                // connect to the bridge and perform the operation.
                bool ret3 = false;
                if (conf.cMcu.etType == NS_MESSENGER.MCU.eType.ACCORDv6 || conf.cMcu.etType == NS_MESSENGER.MCU.eType.ACCORDv7 || conf.cMcu.etType == NS_MESSENGER.MCU.eType.RMX)
                {
                    //NS_POLYCOM.Polycom accord = new NS_POLYCOM.Polycom(configParams);
                    //ret3 = polycom.OngoingConfOps("MuteTransmitEndpoint", conf, party, ref msg, mute, 0, 0, false, ref party.etStatus);
                    //if (!ret3)
                    //{
                    //    this.errMsg = polycom.errMsg;
                    //    msg = polycom.errMsg;
                    //}
                }
                else
                {
                    if (conf.cMcu.etType == NS_MESSENGER.MCU.eType.CODIAN)
                    {
                        NS_CODIAN.Codian codian = new NS_CODIAN.Codian(configParams);
                        ret3 = codian.LockUnlockConference(conf, 0, lockorunlock);
                        if (!ret3)
                        {
                            this.errMsg = codian.errMsg;
                        }
                    }
                    else if (conf.cMcu.etType == NS_MESSENGER.MCU.eType.CISCOTP)
                    {
                        NS_CiscoTPServer.CiscoTPServer CiscoTPServer = new NS_CiscoTPServer.CiscoTPServer(configParams);
                        ret3 = CiscoTPServer.LockUnlockConference(conf, duration, lockorunlock);
                        if (!ret3)
                        {
                            this.errMsg = CiscoTPServer.errMsg;
                        }
                    }
                    //FB 2441 Starts
                    else if (conf.cMcu.etType == NS_MESSENGER.MCU.eType.RPRM)
                    {
                        NS_POLYCOMRPRM.POLYCOMRPRM polycomRPRM = new NS_POLYCOMRPRM.POLYCOMRPRM(configParams);
                        ret = false;
                        ret3 = polycomRPRM.LockUnlockConference(conf, lockorunlock);
                        if (!ret3)
                        {
                            this.errMsg = polycomRPRM.errMsg;
                        }
                    }
                    //FB 2441 Ends
                    //ALLDEV-710 Starts
                    else if (conf.cMcu.etType == NS_MESSENGER.MCU.eType.Pexip)
                    {
                        NS_Pexip.pexip pexip = new NS_Pexip.pexip(configParams);
                        ret3 = pexip.LockUnlockConference(conf, lockorunlock);
                        if (!ret3)
                            this.errMsg = pexip.errMsg;
                    }
                    //ALLDEV-710 Ends
                    else
                    {
                        if (conf.cMcu.etType == NS_MESSENGER.MCU.eType.TANDBERG)
                        {
                            //NS_TANDBERG.Tandberg tandberg = new NS_TANDBERG.Tandberg(configParams);
                            //ret3 = tandberg.MuteTransmitEndpoint(conf, party, mute);
                            //if (!ret3)
                            //{
                            //    this.errMsg = tandberg.errMsg;
                            //}
                        }
                        //FB 2556 Start
                        else if (conf.cMcu.etType == NS_MESSENGER.MCU.eType.RADVISIONIVIEW)
                        {
                            NS_RADVISION_IView.RadVisionIView RadIview = new NS_RADVISION_IView.RadVisionIView(configParams);
                            ret3 = RadIview.LockUnlockConference(conf, lockorunlock);
                            if (!ret3)
                            {
                                this.errMsg = RadIview.errMsg;
                            }
                        }
                        //FB 2556 End
                    }
                }
                if (!ret3)
                {
                    logger.Trace("conference lock/unlock operation failed.");
                    //FB 2569 Start
                    alert = new NS_MESSENGER.Alert();
                    alert.confID = conf.iDbID;
                    alert.instanceID = conf.iInstanceID;
                    alert.message = this.errMsg;
                    alert.typeID = 11; //conference lock/unlock
                    bool retp2p = db.InsertConfAlert(alert);
                    if (!retp2p)
                        logger.Trace("conference lock/unlock alert insert failed.");
                    //FB 2569 End
                    return false;
                }

                return (true);
            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
                return (false);
            }
        }
        #endregion


        //FB 2441 Starts
        #region ConferenceRecording
        /// <summary>
        /// 
        /// </summary>
        /// <param name="inXml"></param>
        /// <param name="outXml"></param>
        /// <param name="msg"></param>
        /// <returns></returns>
        internal bool ConferenceRecording(string inXml, ref string outXml, ref string msg)
        {
            try
            {
                logger.Trace("Entering the function...");
                // Load the string into xml parser and retrieve the tag values.
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(inXml);

                string login = xd.SelectSingleNode("//login/userID").InnerXml.Trim();

                string strConfId = xd.SelectSingleNode("//login/confID").InnerXml.Trim();
                int confId = 0, instanceId = 0;
                bool ret = ExtractConfInstanceIds(strConfId, ref confId, ref instanceId);
                if (!ret)
                {
                    logger.Trace("Invalid conf/instance id.");
                    return false;
                }
                
                int Record = 0;
                if (xd.SelectSingleNode("//login/confrecord") != null)
                    Int32.TryParse(xd.SelectSingleNode("//login/confrecord").InnerXml.Trim(), out Record); // 0 = off, 1 = on

                int mcuId = 0;
                if (xd.SelectSingleNode("//login/mcuId") != null)
                    Int32.TryParse(xd.SelectSingleNode("//login/mcuId").InnerXml.Trim(), out mcuId);
                //FB 2501 Call Monitoring End
                // Fetch the conf unique name since thats the unique identifier of the conf.
                NS_MESSENGER.Conference conf = new NS_MESSENGER.Conference();
                NS_DATABASE.Database db = new NS_DATABASE.Database(configParams);
                conf.iDbID = confId; conf.iInstanceID = instanceId;
                ret = db.FetchConf(ref conf);
                if (!ret)
                {
                    logger.Exception(200, "Database fetch operation failed.");
                    return (false);
                }


                if (mcuId >= 0)
                {
                    bool ret1 = db.FetchMcu(mcuId, ref conf.cMcu);
                    if (!ret1)
                    {
                        logger.Trace("Error fetching mcu details.");
                        return (false);
                    }
                }
                if (conf.cMcu.etType == NS_MESSENGER.MCU.eType.RPRM)
                {
                    NS_POLYCOMRPRM.POLYCOMRPRM polycomRPRM = new NS_POLYCOMRPRM.POLYCOMRPRM(configParams);
                    ret = false;
                    ret = polycomRPRM.StartStopRecord(conf,Record);
                    if (!ret)
                    {
                        this.errMsg = polycomRPRM.errMsg;
                        return (false);
                    }
                }
                return (true);
            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
                return (false);
            }
        }
        #endregion
		// FB  2441 II Starts
        #region TerminateSyncConference
        /// <summary>
        /// TerminateSyncConference
        /// </summary>
        /// <param name="inXml"></param>
        /// <param name="outXml"></param>
        /// <param name="msg"></param>
        /// <returns></returns>
        internal bool TerminateSyncConference(string inXml, ref string outXml, ref string msg)
        {
            try
            {
                bool ret = false;
                NS_DATABASE.Database db = new NS_DATABASE.Database(configParams);
                List<NS_MESSENGER.Conference> confList = new List<NS_MESSENGER.Conference>();
                NS_MESSENGER.MCU mcu=new NS_MESSENGER.MCU();
                NS_MESSENGER.Conference Templateconf = new NS_MESSENGER.Conference();
                List<int> mcuid = null; //ZD 104214              
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(inXml);
                int iRPRM = 0;

                if (xd.SelectSingleNode("//login/FromService") != null)
                    int.TryParse(xd.SelectSingleNode("//login/FromService").InnerText.Trim(), out iRPRM);

                string strConfId = "";
                if (xd.SelectSingleNode("//login/conferenceID") != null)
                    strConfId = xd.SelectSingleNode("//login/conferenceID").InnerXml.Trim();

                int confId = 0, instanceId = 0;
                ret = ExtractConfInstanceIds(strConfId, ref confId, ref instanceId);
                if (!ret)
                {
                    logger.Trace("Invalid conf/instance id.");
                    return false;
                }

                Templateconf.iDbID = confId;
                Templateconf.iInstanceID = instanceId;

                ret = false;
                ret = db.FetchConf(ref Templateconf);
                if (!ret)
                {
                    logger.Exception(200, "Database fetch operation failed.");
                    return (false);
                }
                confList.Add(Templateconf);

                //ZD 104214 Starts
                ret = false;
                mcuid = new List<int>();
                ret = db.FetchConfbridID(Templateconf, ref mcuid);
                if (!ret)
                {
                    logger.Exception(200, "Database fetch operation failed.");
                    return (false);
                }

                if (mcuid.Count > 0)
                {
                    ret = false;
                    ret = db.FetchMcu(mcuid[0], ref Templateconf.cMcu);
                    if (!ret)
                    {
                        logger.Exception(200, "Database fetch operation failed.");
                        return (false);
                    }

                }
                //ZD 104214 Ends
                ret = false;
                ret = db.FetchSyncAdjustments(ref confList, Templateconf.cMcu.etType); //ZD 100221
                if (!ret)
                {
                    logger.Exception(200, "Database fetch operation failed.");
                    return (false);
                }
                for (int i = 0; i < confList.Count; i++)
                {
                    ret = false;
                    ret = db.FetchMcu(confList[i].cMcu.iDbId,ref mcu);
                    if (!ret)
                    {
                        logger.Exception(200, "Database fetch operation failed.");
                        return (false);
                    }
                    confList[i].cMcu = mcu;
                    if (confList[i].cMcu.etType == NS_MESSENGER.MCU.eType.RPRM)
                    {
                        NS_POLYCOMRPRM.POLYCOMRPRM polycomRPRM = new NS_POLYCOMRPRM.POLYCOMRPRM(configParams);
                        ret = false;
                        ret = polycomRPRM.DeleteConfChangeMCU(confList[i]);
                        if (!ret)
                        {
                            this.errMsg = polycomRPRM.errMsg;
                            return (false);
                        }
                        else
                        {
                            //ZD 100221 Starts
                            if (string.IsNullOrEmpty(confList[i].sWebEXMeetingKey))
                            {
                                ret = false;
                                ret = db.UpdateSyncStatus(confList[i]);
                                if (!ret)
                                {
                                    logger.Trace("Error in Delete Sync data");
                                }
                            }
                            //ZD 100221 Ends
                        }

                    }
                    // FB 2556 Starts
                    else
                        if (confList[i].cMcu.etType == NS_MESSENGER.MCU.eType.RADVISIONIVIEW)
                        {
                            NS_RADVISION_IView.RadVisionIView radiview = new NS_RADVISION_IView.RadVisionIView(configParams);
                            ret = false;
                            ret = radiview.TerminateConference(confList[i]);
                            if (!ret)
                            {
                                this.errMsg = radiview.errMsg;
                                return (false);
                            }
                            else
                            {
                                //ZD 100221 Starts
                                if (string.IsNullOrEmpty(confList[i].sWebEXMeetingKey))
                                {
                                    ret = false;
                                    ret = db.UpdateSyncStatus(confList[i]);
                                    if (!ret)
                                    {
                                        logger.Trace("Error in Delete Sync data");
                                    }
                                }
                                //ZD 100221 Ends
                            }

                        }
                    // FB 2556 Ends
                    //FB 2718 Starts
                        else
                            if (confList[i].cMcu.etType == NS_MESSENGER.MCU.eType.TMSScheduling)
                            {
                                NS_TMS.TMSScheduling CiscoTMS = new NS_TMS.TMSScheduling(configParams);
                                ret = false;
                                ret = CiscoTMS.DeleteScheduledConference(confList[i]);
                                if (!ret)
                                {
                                    this.errMsg = CiscoTMS.errMsg;
                                    return (false);
                                }
                                else
                                {
                                    //ZD 100221 Starts
                                    if (string.IsNullOrEmpty(confList[i].sWebEXMeetingKey))
                                    {
                                        ret = false;
                                        ret = db.UpdateSyncStatus(confList[i]);
                                        if (!ret)
                                        {
                                            logger.Trace("Error in Delete Sync data");
                                        }
                                    }
                                    //ZD 100221 Ends
                                }
                            }
                    //FB 2718 Ends

                }

               


                return true;
            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
                return (false);
            }
        }
        #endregion
        //FB 2441 II Ends
		//FB 2441 Starts

        #region UpdateParticipantStatus
        /// <summary>
        /// UpdateParticipantStatus
       /// </summary>
       /// <param name="inXml"></param>
       /// <returns></returns>
        internal bool UpdateParticipantStatus(string inXml)
        {
            //XmlDocument xd = null;
            //NS_DATABASE.Database db = null;
            //bool ret1 = false;
            //try
            //{
            //    logger.Trace("Entering the Call Detail Records method...");
            //    xd = new XmlDocument();
            //    xd.LoadXml(inXml);
            //    // load the xml doc
            //    XmlDocument oDOM = new XmlDocument();
            //    oDOM.LoadXml(inXml);
            //    String RxAudioPacketsReceived = "", RxAudioPacketErrors = "", RxAudioPacketsMissing = ""; //audioRx
            //    String TxAudioPacketsSent = ""; //audioTx
            //    String RxVideoPacketsReceived = "", RxVideoPacketErrors = "", RxVideoPacketsMissing = "";//videoRx
            //    String TxVideopacketsSent = "";//videoTx

            //    //member node list 
            //    XmlNodeList oNodeList = oDOM.SelectNodes("//member");

            //    //going thru the "member" array list 
            //    foreach (XmlNode oNode in oNodeList)
            //    {
            //        NS_MESSENGER.Party party = new NS_MESSENGER.Party();

            //        if (oNode.SelectSingleNode("name").InnerText == "participantGUID")
            //        {
            //            party.sGUID = oNode.SelectSingleNode("value/string").InnerText.Trim();
            //        }

            //        if (party.sGUID != "")
            //        {
            //            ret1 = db.FetchPartyTerminalTypeUsingGUID(party);
            //            if (!ret1)
            //            {
            //                logger.Trace("Error in Fetching MCU details.");
            //                return false;
            //            }
            //            if (oNode.SelectSingleNode("name").InnerText == "audioRx")
            //            {
            //                XmlNodeList oInnerNodeList = oNode.SelectNodes("value/array/data/value");

            //                foreach (XmlNode oInnerNode in oInnerNodeList)
            //                {
            //                    if (oInnerNode.SelectSingleNode("struct/member[name = \"packetsReceived\"]/value/int") != null) //Doubt in member - Gowniyan
            //                        RxAudioPacketsReceived = oInnerNode.SelectSingleNode("struct/member[name = \"packetsReceived\"]/value/int").InnerText.Trim();
            //                    logger.Trace("RxAudioPacketsReceived:" + RxAudioPacketsReceived);
            //                    party.sRxAudioPacketsReceived = RxAudioPacketsReceived;

            //                    if (oInnerNode.SelectSingleNode("struct/member[name = \"packetErrors\"]/value/int") != null)
            //                        RxAudioPacketErrors = oInnerNode.SelectSingleNode("struct/member[name = \"packetErrors\"]/value/int").InnerText.Trim();
            //                    logger.Trace("RxAudioPacketErrors:" + RxAudioPacketErrors);
            //                    party.sRxAudioPacketErrors = RxAudioPacketErrors;

            //                    if (oInnerNode.SelectSingleNode("struct/member[name = \"packetsMissing\"]/value/int") != null)
            //                        RxAudioPacketsMissing = oInnerNode.SelectSingleNode("struct/member[name = \"packetsMissing\"]/value/int").InnerText.Trim();
            //                    logger.Trace("RxAudioPacketsMissing:" + RxAudioPacketsMissing);
            //                    party.sRxAudioPacketsMissing = RxAudioPacketsMissing;
            //                }
            //            }

            //            if (oNode.SelectSingleNode("name").InnerText == "audioTx")
            //            {
            //                //get to each individual participant
            //                XmlNodeList oInnerNodeList = oNode.SelectNodes("value/array/data/value");

            //                foreach (XmlNode oInnerNode in oInnerNodeList)
            //                {
            //                    if (oInnerNode.SelectSingleNode("struct/member[name = \"packetsSent\"]/value/int") != null)
            //                        TxAudioPacketsSent = oInnerNode.SelectSingleNode("struct/member[name = \"packetsSent\"]/value/int").InnerText.Trim();
            //                    logger.Trace("TxAudioPacketsSent:" + TxAudioPacketsSent);
            //                }
            //            }

            //            if (oNode.SelectSingleNode("name").InnerText == "videoRx")
            //            {
            //                XmlNodeList oInnerNodeList = oNode.SelectNodes("value/array/data/value");

            //                foreach (XmlNode oInnerNode in oInnerNodeList)
            //                {

            //                    if (oInnerNode.SelectSingleNode("struct/member[name = \"packetsReceived\"]/value/int") != null)
            //                        RxVideoPacketsReceived = oInnerNode.SelectSingleNode("struct/member[name = \"packetsReceived\"]/value/int").InnerText.Trim();
            //                    logger.Trace("RxVideoPacketsReceived:" + RxVideoPacketsReceived);
            //                    party.sRxVideoPacketsReceived = RxVideoPacketsReceived;

            //                    if (oInnerNode.SelectSingleNode("struct/member[name = \"packetErrors\"]/value/int") != null)
            //                        RxVideoPacketErrors = oInnerNode.SelectSingleNode("struct/member[name = \"packetErrors\"]/value/int").InnerText.Trim();
            //                    logger.Trace("RxVideoPacketErrors:" + RxVideoPacketErrors);
            //                    party.sRxVideoPacketErrors = RxVideoPacketErrors;

            //                    if (oInnerNode.SelectSingleNode("struct/member[name = \"packetsMissing\"]/value/int") != null)
            //                        RxVideoPacketsMissing = oInnerNode.SelectSingleNode("struct/member[name = \"packetsMissing\"]/value/int").InnerText.Trim();
            //                    logger.Trace("RxVideoPacketsMissing:" + RxVideoPacketsMissing);
            //                    party.sRxVideoPacketsMissing = RxVideoPacketsMissing;

            //                }
            //            }

            //            if (oNode.SelectSingleNode("name").InnerText == "videoTx")
            //            {
            //                //get to each individual participant
            //                XmlNodeList oInnerNodeList = oNode.SelectNodes("value/array/data/value");

            //                foreach (XmlNode oInnerNode in oInnerNodeList)
            //                {
            //                    if (oInnerNode.SelectSingleNode("struct/member[name = \"packetsSent\"]/value/int") != null)
            //                        TxVideopacketsSent = oInnerNode.SelectSingleNode("struct/member[name = \"packetsSent\"]/value/int").InnerText.Trim();
            //                    logger.Trace("TxVideopacketsSent:" + TxVideopacketsSent);
            //                }
            //            }
            //            ret1 = db.UpdateparticipantStatus(party);
            //            if (!ret1)
            //            {
            //                logger.Trace("Error in Fetching MCU details.");
            //                return false;
            //            }
            //        }
            //    }
               
            //    return true;
            //}
            //catch (Exception e)
            //{
            //    logger.Exception(100, e.Message);
            //    return (false);
            //}
            return true;
        }
        #endregion

        //FB 2501 Dec6 Start

        //ZD 100369_MCU Start

        #region FetchMCUInfo
        /// <summary>
        /// Test the bridge connectivity
        /// </summary>
        internal bool FetchMCUInfo(string inXml, ref string outXml)
        {
            XmlDocument xd = null;
            NS_DATABASE.Database db = null;
            List<NS_MESSENGER.MCU> cMcuList = null;
            NS_MESSENGER.MCU cMcu = null;
            bool ret1 = false;
            int Timeout = 7500;
            try
            {
                logger.Trace("Entering the Fetch MCU Info method...");
                xd = new XmlDocument();
                xd.LoadXml(inXml);
                db = new NS_DATABASE.Database(configParams);
                cMcu = new NS_MESSENGER.MCU();

                ret1 = db.FetchAllMcu(ref cMcuList);
                if (!ret1)
                {
                    logger.Trace("Error in Fetching MCU details.");
                    return false;
                }

                for (int bCnt = 0; bCnt < cMcuList.Count; bCnt++)
                {
                    cMcu = cMcuList[bCnt];
                    ret1 = false;
                    switch (cMcu.etType)
                    {
                        case NS_MESSENGER.MCU.eType.CODIAN:
                            {
                                NS_CODIAN.Codian Codian = new NS_CODIAN.Codian(configParams);
                                ret1 = Codian.FetchMCUDetails(cMcu, Timeout);
                                if (!ret1) this.errMsg = Codian.errMsg;
                                Codian = null;
                                break;
                            }
                        case NS_MESSENGER.MCU.eType.CISCOTP:
                            {
                                NS_CiscoTPServer.CiscoTPServer CiscoTPServer = new NS_CiscoTPServer.CiscoTPServer(configParams);
                                ret1 = CiscoTPServer.FetchMCUDetails(cMcu, Timeout);
                                if (!ret1) this.errMsg = CiscoTPServer.errMsg;
                                CiscoTPServer = null;
                                break;
                            }
                        case NS_MESSENGER.MCU.eType.ACCORDv6:
                        case NS_MESSENGER.MCU.eType.ACCORDv7:
                        case NS_MESSENGER.MCU.eType.RMX:
                            {
                                NS_POLYCOM.Polycom accord = new NS_POLYCOM.Polycom(configParams);
                                ret1 = accord.FetchMCUStatus(cMcu, Timeout);
                                if (!ret1) this.errMsg = accord.errMsg;
                                accord = null;
                                break;
                            }
                        case NS_MESSENGER.MCU.eType.TANDBERG:
                            {
                                NS_TANDBERG.Tandberg tandberg = new NS_TANDBERG.Tandberg(configParams);
                                ret1 = false;
                                ret1 = tandberg.FetchMCUDetails(cMcu, Timeout);
                                if (!ret1) this.errMsg = tandberg.errMsg;
                                tandberg = null;
                                break;
                            }
                        case NS_MESSENGER.MCU.eType.RADVISION:
                            {
                                NS_RADVISION.Radvision rad = new NS_RADVISION.Radvision(configParams);
                                ret1 = rad.FetchMCUDetails(cMcu, Timeout);
                                if (!ret1) this.errMsg = rad.errMsg;
                                rad = null;
                                break;
                            }
                        case NS_MESSENGER.MCU.eType.Lifesize:
                            {
                                NS_LifeSize.LifeSize lifeSize = new NS_LifeSize.LifeSize(configParams);
                                ret1 = lifeSize.FetchMCUDetails(cMcu, Timeout);
                                if (!ret1) this.errMsg = lifeSize.errMsg;
                                lifeSize = null;
                                break;
                            }
                        case NS_MESSENGER.MCU.eType.RPRM:
                            {
                                NS_POLYCOMRPRM.POLYCOMRPRM PolycomRPRM = new NS_POLYCOMRPRM.POLYCOMRPRM(configParams);
                                ret1 = PolycomRPRM.FetchMCUDetails(cMcu, Timeout);
                                if (!ret1) this.errMsg = PolycomRPRM.errMsg;
                                PolycomRPRM = null;
                                break;
                            }
                        case NS_MESSENGER.MCU.eType.RADVISIONIVIEW:
                            {
                                NS_RADVISION_IView.RadVisionIView RadIview = new NS_RADVISION_IView.RadVisionIView(configParams);
                                ret1 = RadIview.FetchMCUDetails(cMcu, Timeout);
                                if (!ret1) this.errMsg = RadIview.errMsg;
                                RadIview = null;
                                break;
                            }
                        case NS_MESSENGER.MCU.eType.TMSScheduling:
                            {
                                NS_TMS.TMSScheduling TMS = new NS_TMS.TMSScheduling(configParams);
                                ret1 = TMS.FetchMCUDetails(cMcu, Timeout);
                                if (!ret1) this.errMsg = TMS.errMsg;
                                TMS = null;
                                break;
                            }
                        case NS_MESSENGER.MCU.eType.CMA:
                            {
                                //NS_POLYCOMCMA.PolycomCMA polycomCMA = new NS_POLYCOMCMA.PolycomCMA(configParams);
                                //ret3 = polycomCMA.testm(conf);
                                //if (!ret3) this.errMsg = polycomCMA.errMsg;
                                break;
                            }
                        default:
                            {
                                this.errMsg = "Unknown MCU type.";
                                break;
                            }
                    }
                    if (!ret1)
                    {
                        logger.Trace("Fetching MCU Command failed.");
                        logger.Trace("Msg = " + this.errMsg);
                    }
                }
                return true;
            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
                return (false);
            }
        }
        #endregion 

        //ZD 100369_MCU End

        //FB 2501 Dec6 End

        //FB 2591
        #region FetchMCUProfiles
        /// <summary>
        /// Test the bridge connectivity
        /// </summary>
        internal bool FetchMCUProfiles(string inXml, ref string outXml)
        {
            XmlDocument xd = null;
            NS_DATABASE.Database db = null;
            NS_MESSENGER.MCU cMcu = null;
            bool ret1 = false;
            try
            {
                logger.Trace("Entering the Fetch MCU Info method...");
                xd = new XmlDocument();
                xd.LoadXml(inXml);
                db = new NS_DATABASE.Database(configParams);
                cMcu = new NS_MESSENGER.MCU();

                int login = 0, MCUId = 0;

                if (xd.SelectSingleNode("//GetMCUProfiles/userID") != null)
                    Int32.TryParse(xd.SelectSingleNode("//GetMCUProfiles/userID").InnerText.Trim(), out login);


                if (xd.SelectSingleNode("//GetMCUProfiles/MCUId") != null)
                    Int32.TryParse(xd.SelectSingleNode("//GetMCUProfiles/MCUId").InnerText.Trim(), out MCUId);
                
                ret1 = db.FetchMcu(MCUId, ref cMcu);
                if (!ret1)
                {
                    logger.Trace("Error in Fetching MCU details.");
                    return false;
                }

                if (cMcu.etType == NS_MESSENGER.MCU.eType.RMX)
                {
                    NS_POLYCOM.Polycom RMX = new NS_POLYCOM.Polycom(configParams);
                    ret1 = RMX.FetchMCUProfileDetails(cMcu);
                    if (!ret1) this.errMsg = RMX.errMsg;
                    RMX = null;
                }
                //FB 2441 Starts
                else
                if (cMcu.etType == NS_MESSENGER.MCU.eType.RPRM)
                {
                    NS_POLYCOMRPRM.POLYCOMRPRM polycomRPRM = new NS_POLYCOMRPRM.POLYCOMRPRM(configParams);
                    ret1 = polycomRPRM.FetchMCUProfileDetails(cMcu);
                    if (!ret1) this.errMsg = polycomRPRM.errMsg;
                    polycomRPRM = null;
                }
                //FB 2441 Ends
                if (!ret1)
                {
                    logger.Trace("Fetching MCU Command failed.");
                    logger.Trace("Msg = " + this.errMsg);
                    return false;
                }

                return true;
            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
                return (false);
            }
        }
        #endregion 
        //FB 2501 Call Monitoring End

        //ZD 104211 Start
        #region FetchMCUPoolOrder
        /// <summary>
        /// FetchMCUPoolOrder
        /// </summary>
        /// <param name="inXml"></param>
        /// <param name="OutXML"></param>
        /// <returns></returns>
        internal bool FetchMCUPoolOrders(string inXml, ref string OutXML)
        {
            XmlDocument xd = null;
            NS_DATABASE.Database db = null;
            NS_MESSENGER.MCU cMcu = null;
            bool ret1 = false;
            try
            {
                logger.Trace("Entering the Fetch MCU Info method...");
                xd = new XmlDocument();
                xd.LoadXml(inXml);
                db = new NS_DATABASE.Database(configParams);
                cMcu = new NS_MESSENGER.MCU();

                int login = 0, MCUId = 0;

                if (xd.SelectSingleNode("//GetMCUPoolOrders/userID") != null)
                    Int32.TryParse(xd.SelectSingleNode("//GetMCUPoolOrders/userID").InnerText.Trim(), out login);


                if (xd.SelectSingleNode("//GetMCUPoolOrders/MCUId") != null)
                    Int32.TryParse(xd.SelectSingleNode("//GetMCUPoolOrders/MCUId").InnerText.Trim(), out MCUId);

                ret1 = db.FetchMcu(MCUId, ref cMcu);
                if (!ret1)
                {
                    logger.Trace("Error in Fetching MCU details.");
                    return false;
                }

                if (cMcu.etType == NS_MESSENGER.MCU.eType.RPRM)
                {
                    NS_POLYCOMRPRM.POLYCOMRPRM polycomRPRM = new NS_POLYCOMRPRM.POLYCOMRPRM(configParams);
                    ret1 = polycomRPRM.FetchMCUPoolorderDetails(cMcu);
                    if (!ret1) this.errMsg = polycomRPRM.errMsg;
                    polycomRPRM = null;
                }
                if (!ret1)
                {
                    logger.Trace("Fetching MCU Command failed.");
                    logger.Trace("Msg = " + this.errMsg);
                    return false;
                }

                return true;
            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
                return (false);
            }
        }
        //ZD 104211 End
        #endregion

        //FB 2584 Start
        #region TelepresenceEndpoint
        /// <summary>
        /// TelepresenceEndpoint
        /// </summary>
        /// <param name="party"></param>
        /// <returns></returns>
        private List<NS_MESSENGER.Party> TelepresenceEndpoint(NS_MESSENGER.Party party)
        {
            List<NS_MESSENGER.Party> parties = new List<NS_MESSENGER.Party>();

            NS_DATABASE.Database db = new NS_DATABASE.Database(configParams);

            if (party.MulticodecAddress == null || party.MulticodecAddress == "")
                party.isTelePresence = false;

            if (party.isTelePresence && party.cMcu.etType != NS_MESSENGER.MCU.eType.CISCOTP)
            {
                String partyName = "";
                NS_MESSENGER.Party partyMulti = null;
                String[] MultiAdds = party.MulticodecAddress.Split('�');
                for (int a = 0; a < MultiAdds.Length; a++)
                {
                    partyMulti = new NS_MESSENGER.Party();
                    db.SetPartyProperties(ref partyMulti, ref party);
                    partyMulti.iDbId = party.iDbId + a;

                    if (a > 0)
                        partyMulti.iDbId += 5000;

                    partyName = party.sName;
                    if (MultiAdds[a].Trim() != "")
                    {
                        partyMulti.sAddress = MultiAdds[a].Trim();
                        partyName += a + 1;

                        partyMulti.sName = partyName;
                    }
                    parties.Add(partyMulti);
                }
            }
            else
                parties.Add(party);

            return parties;
        }
        #endregion
        //FB 2584 End

        //FB 2595 Start
        #region SetSwitchingtoConf
        /// <summary>
        /// SetSwitchingtoConf
        /// </summary>
        /// <param name="inXml"></param>
        /// <param name="outXml"></param>
        /// <returns></returns>
        internal bool SetSwitchingtoConf(string inXml, ref string outXml)
        {
            String hadrwareAdminEmail = "";
            bool error = false;
            try
            {
                logger.Trace("Entering Switching function...");

                XmlDocument xd = new XmlDocument();
                xd.LoadXml(inXml);

                string strConfId = xd.SelectSingleNode("//Conference/confID").InnerXml.Trim();
                int confId = 0, instanceId = 0, NetworkState = 0, CallRequestTimeout = 0; //FB 2993
                bool ret = ExtractConfInstanceIds(strConfId, ref confId, ref instanceId);
                if (!ret)
                {
                    logger.Trace("Invalid conf/instance id.");
                    return false;
                }

                Int32.TryParse(xd.SelectSingleNode("//Conference/NetworkState").InnerXml.Trim(), out NetworkState); // database id

                Int32.TryParse(xd.SelectSingleNode("//Conference/CallRequestTimeout").InnerXml.Trim(), out CallRequestTimeout); // FB 2993

                NS_MESSENGER.Conference conf = new NS_MESSENGER.Conference();
                NS_DATABASE.Database db = new NS_DATABASE.Database(configParams);
                NS_Tcp.Tcp TP = new NS_Tcp.Tcp(configParams);
                NS_MESSENGER.AdminSettings site = new NS_MESSENGER.AdminSettings();

                conf.iDbID = confId; conf.iInstanceID = instanceId;
                ret = db.FetchConf(ref conf);
                if (!ret)
                {
                    logger.Exception(200, "Database fetch operation failed.");
                    return (false);
                }

                db.FetchHardwareAdminEmail(conf.iOrgID.ToString(), ref hadrwareAdminEmail);

                #region Fetch the Room endpoints
                ret = false;
                ret = db.FetchRooms(conf.iDbID, conf.iInstanceID, ref conf.qParties, 0); //Done
                if (!ret)
                {
                    logger.Exception(100, "Error in fetching rooms from db.");
                    return false;
                }
                #endregion

                // Fetch all the bridge ids on which this conf is running.
                System.Collections.IEnumerator partyEnumerator;
                partyEnumerator = conf.qParties.GetEnumerator();
                int partyCount = conf.qParties.Count;
                logger.Trace("Room Count = " + partyCount.ToString());

                bool ret3 = false;
                for (int i = 0; i < partyCount; i++)
                {
                    ret3 = partyEnumerator.MoveNext();
                    if (!ret3) break;

                    NS_MESSENGER.Party party = new NS_MESSENGER.Party();
                    party = (NS_MESSENGER.Party)partyEnumerator.Current;

                    logger.Trace("Room Address : " + party.sAddress.ToString());

                    ret = TP.SecureCheck(conf, party, NetworkState, ref outXml, hadrwareAdminEmail, ref error, CallRequestTimeout);//FB 2993
                    if (!ret)
                    {
                        this.errMsg = TP.errMsg;
                    }
                }
                if (!error)
                {
                    ret = false;
                    ret = db.UpdateConferenceSwitching(conf.iDbID, conf.iInstanceID, 1);
                    if (!ret)
                        logger.Trace("Update Conference Switching failed for confid : " + conf.iDbID.ToString());
                }
                return (true);
            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
                return (false);
            }
        }
        #endregion
        //FB 2595 End

        //FB 2709 Start
        #region CreateUserOnMcu
        /// <summary>
        /// CreateUserOnMcu
        /// </summary>
        /// <param name="inXml"></param>
        /// <param name="outXml"></param>
        /// <returns></returns>
        internal bool CreateUserOnMcu(string inXml, ref string outXml)
        {
            NS_DATABASE.Database db = new NS_DATABASE.Database(configParams);
            NS_MESSENGER.MCU mcu = new NS_MESSENGER.MCU();
            int UserID = 0, mcuId = 0, count = 0, Existingcallscount = 0;
            string userlogin = "", Login = "";
            bool userexist = false;
            try
            {
                logger.Trace("Entering CreateUserOnMcu function...");

                XmlDocument xd = new XmlDocument();
                xd.LoadXml(inXml);

                if (xd.SelectSingleNode("//CreateUser/UserID") != null)
                    Int32.TryParse(xd.SelectSingleNode("//CreateUser/UserID").InnerXml.Trim(), out UserID);

                if (xd.SelectSingleNode("//CreateUser/mcuId") != null)
                    Int32.TryParse(xd.SelectSingleNode("//CreateUser/mcuId").InnerXml.Trim(), out mcuId);

                if (xd.SelectSingleNode("//CreateUser/Existingcallscount") != null)
                    Int32.TryParse(xd.SelectSingleNode("//CreateUser/Existingcallscount").InnerXml.Trim(), out Existingcallscount);

                if (mcuId > 0)
                {
                    bool ret1 = db.FetchMcu(mcuId, ref mcu);
                    if (!ret1)
                    {
                        logger.Trace("Error fetching mcu details.");
                        return (false);
                    }
                    //FB 2441 II
                    if (mcu.sRPRMLogin != "")
                    {
                        int Existcount=0;
                        ret1 = db.CheckRPRMLogin(mcuId, mcu.sRPRMLogin, ref Existcount);
                        if (!ret1)
                        {
                            logger.Trace("Error fetching RPRM details.");
                            return (false);
                        }
                        count = mcu.ilogincount;
                        userlogin = mcu.sRPRMLogin;
                        Existingcallscount = Existcount;
                    }
                    //FB 2441 II
                    else
                    {
                        logger.Trace("Invalid Login Name.");
                        return (false);
                    }
                }
                else
                {
                    logger.Trace("No MCU ID Found");
                    return (false);
                }
                if (mcu.etType == NS_MESSENGER.MCU.eType.RPRM)
                {
                    NS_POLYCOMRPRM.POLYCOMRPRM polycomRPRM = new NS_POLYCOMRPRM.POLYCOMRPRM(configParams);
                    bool ret = false;
                    for (int i = Existingcallscount; i < count; i++)
                    {
                        Login = "";
                        Login = userlogin + "_" + (i + 1);
                        ret = polycomRPRM.CreateUser(mcu, Login, ref userexist);
                        if (!ret)
                        {
                            if (userexist)
                                count = count + 1;
                            this.errMsg = polycomRPRM.errMsg;
                        }
                        else
                        {
                            db.InsertRPRMUserList(mcuId, mcu.sMcuType, Login, mcu.sPwd, mcu.sDMADomain);
                            if (!ret)
                            {
                                logger.Trace("Failed To insert USer");
                                this.errMsg = polycomRPRM.errMsg;
                            }
                        }
                    }
                }
                return (true);
            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
                return (false);
            }
        }
        #endregion
		//FB 2556 Starts
        #region FetchExternalMCUService
        /// <summary>
        /// FetchExternalMCUService
        /// </summary>
        /// <param name="inXml"></param>
        /// <param name="outXml"></param>
        /// <param name="msg"></param>
        /// <returns></returns>
        internal bool FetchExternalMCUService(string inXml, ref string outXml, ref string msg)
        {
            try
            {
                logger.Trace("Entering the function...");
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(inXml);
                bool ret = false;
                int loginUser = 0;
                if (xd.SelectSingleNode("//GetExtMCUService/userID") != null)
                    int.TryParse(xd.SelectSingleNode("//GetExtMCUService/userID").InnerXml.Trim(), out loginUser);

                int bridgeTypeId = 0;
                if (xd.SelectSingleNode("//GetExtMCUService/BridgeTypeId") != null)
                    Int32.TryParse(xd.SelectSingleNode("//GetExtMCUService/BridgeTypeId").InnerXml.Trim(), out bridgeTypeId);

                int mcuId = 0;
                if (xd.SelectSingleNode("//GetExtMCUService/BridgeId") != null)
                    Int32.TryParse(xd.SelectSingleNode("//GetExtMCUService/BridgeId").InnerXml.Trim(), out mcuId);

                NS_MESSENGER.MCU MCU = new NS_MESSENGER.MCU();
                NS_DATABASE.Database db = new NS_DATABASE.Database(configParams);

                if (mcuId >= 0)
                {
                    bool ret1 = db.FetchMcu(mcuId, ref MCU);
                    if (!ret1)
                    {
                        logger.Trace("Error fetching mcu details.");
                        return (false);
                    }
                }
                if (MCU.etType == NS_MESSENGER.MCU.eType.RADVISIONIVIEW)
                {
                    NS_RADVISION_IView.RadVisionIView RadIview = new NS_RADVISION_IView.RadVisionIView(configParams);
                    logger.Trace("Entering Radvision IView sub-system...");
                    ret = RadIview.GetMeetingService(MCU);
                    this.errMsg = RadIview.errMsg;
                }
                return (true);
            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
                return (false);
            }
        }
        #endregion

        //ZD 100890 Start

        #region CreateUpdateUserOnRAD
        /// <summary>
        /// CreateUpdateUserOnRAD
        /// </summary>
        /// <param name="inXml"></param>
        /// <param name="outXml"></param>
        /// <param name="msg"></param>
        /// <returns></returns>
        internal bool CreateUpdateUserOnRAD(string inXml, ref string outXml, ref string msg)
        {
            bool ret = false;
            string OldStaticId = "";
            int userID = 0, mcuId = 0, orgID = 0;
            NS_MESSENGER.MCU MCU = new NS_MESSENGER.MCU();
            NS_MESSENGER.Party Party = new NS_MESSENGER.Party();
            NS_DATABASE.Database db = new NS_DATABASE.Database(configParams);
            try
            {
                logger.Trace("Entering the function Create/Update On MCU...");

                XmlDocument xd = new XmlDocument();
                xd.LoadXml(inXml);

                if (xd.SelectSingleNode("//SetUser/userID") != null)
                    Int32.TryParse(xd.SelectSingleNode("//SetUser/userID").InnerXml.Trim(), out userID);

                if (xd.SelectSingleNode("//SetUser/organizationID") != null)
                    Int32.TryParse(xd.SelectSingleNode("//SetUser/organizationID").InnerXml.Trim(), out orgID);

                if (xd.SelectSingleNode("//SetUser/OldStaticId") != null)
                    OldStaticId = xd.SelectSingleNode("//SetUser/OldStaticId").InnerXml.Trim();

                ret = db.FetchCloudMcu(mcuId, ref MCU, orgID);
                if (!ret)
                {
                    logger.Trace("Error fetching mcu details.");
                    return (false);
                }

                ret = db.FetchPartyInfo(userID, ref Party);
                if (!ret)
                {
                    logger.Trace("Error fetching Party details.");
                    return (false);
                }

                if (MCU.etType == NS_MESSENGER.MCU.eType.RADVISIONIVIEW)
                {
                    NS_RADVISION_IView.RadVisionIView RadIview = new NS_RADVISION_IView.RadVisionIView(configParams);
                    logger.Trace("Entering Radvision IView sub-system...");
                    if (Party.siViewUserID == "")
                        ret = RadIview.CreateUser(MCU, Party);
                    else
                        ret = RadIview.UpdateUser(MCU, Party, OldStaticId);

                    this.errMsg = RadIview.errMsg;
                }
                if (!ret)
                {
                    logger.Trace("Create/Update User On MCU Command failed.");
                    logger.Trace("Msg = " + this.errMsg);
                    return false;
                }
                return (true);
            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
                return (false);
            }
        }
        #endregion

        #region DeleteUseronRadvision
        /// <summary>
        /// Delete user on radvision 
        /// ZD 100755 30/1/2014 inncrewin 
        /// </summary>
        /// <param name="inXml"></param>
        /// <param name="outXml"></param>
        /// <param name="msg"></param>
        /// <returns></returns>
        internal bool DeleteUserOnRAD(string inXml, ref string outXml, ref string msg)
        {
            bool ret = false;
            int mcuId = 0, orgID = 0;
            NS_MESSENGER.MCU MCU = new NS_MESSENGER.MCU();
            NS_DATABASE.Database db = new NS_DATABASE.Database(configParams);
            NS_MESSENGER.Party Party = new NS_MESSENGER.Party();
            try
            {
                logger.Trace("Entering the function DeleteUser...");
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(inXml);


                if (xd.SelectSingleNode("//DeleteUser/UserExternalID") != null)
                    Party.siViewUserID = xd.SelectSingleNode("//DeleteUser/UserExternalID").InnerXml;

                if (xd.SelectSingleNode("//DeleteUser/OrganizationId") != null)
                    int.TryParse(xd.SelectSingleNode("//DeleteUser/OrganizationId").InnerXml.Trim(), out orgID);

                if (string.IsNullOrEmpty(Party.siViewUserID))
                    return true;

                ret = db.FetchCloudMcu(mcuId, ref MCU, orgID);
                if (!ret)
                {
                    logger.Trace("Error fetching mcu details.");
                    return (false);
                }

                if (MCU.etType == NS_MESSENGER.MCU.eType.RADVISIONIVIEW)
                {
                    NS_RADVISION_IView.RadVisionIView RadIview = new NS_RADVISION_IView.RadVisionIView(configParams);
                    logger.Trace("Entering Radvision IView sub-system...");
                    ret = RadIview.DeleteUser(MCU, Party);
                    this.errMsg = RadIview.errMsg;
                }
                if (!ret)
                {
                    logger.Trace("Fetching MCU Command failed.");
                    logger.Trace("Msg = " + this.errMsg);
                    return false;
                }
                return (true);
            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
                return (false);
            }
        }
        #endregion

        //ZD 100890 End

        #region FetchExternalMCUSilo
        /// <summary>
        /// FetchExternalMCUSilo
        /// </summary>
        /// <param name="inXml"></param>
        /// <param name="outXml"></param>
        /// <param name="msg"></param>
        /// <returns></returns>
        internal bool FetchExternalMCUSilo(string inXml, ref string outXml, ref string msg)
        {
            try
            {
                logger.Trace("Entering the function...");
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(inXml);
                bool ret = false;
                int loginUser = 0;
                if (xd.SelectSingleNode("//GetExtMCUSilo/userID") != null)
                    int.TryParse(xd.SelectSingleNode("//GetExtMCUSilo/userID").InnerXml.Trim(), out loginUser);

                int bridgeTypeId = 0;
                if (xd.SelectSingleNode("//GetExtMCUSilo/BridgeTypeId") != null)
                    Int32.TryParse(xd.SelectSingleNode("//GetExtMCUSilo/BridgeTypeId").InnerXml.Trim(), out bridgeTypeId);

                int mcuId = 0;
                if (xd.SelectSingleNode("//GetExtMCUSilo/BridgeId") != null)
                    Int32.TryParse(xd.SelectSingleNode("//GetExtMCUSilo/BridgeId").InnerXml.Trim(), out mcuId);

                NS_MESSENGER.MCU MCU = new NS_MESSENGER.MCU();
                NS_DATABASE.Database db = new NS_DATABASE.Database(configParams);
                
                if (mcuId >= 0)
                {
                    ret = db.FetchMcu(mcuId, ref MCU);
                    if (!ret)
                    {
                        logger.Trace("Error fetching mcu details.");
                        return (false);
                    }
                }
                if (MCU.etType == NS_MESSENGER.MCU.eType.RADVISIONIVIEW)
                {
                    NS_RADVISION_IView.RadVisionIView RadIview = new NS_RADVISION_IView.RadVisionIView(configParams);
                    logger.Trace("Entering Radvision IView sub-system...");
                    ret = RadIview.GetSiloDetails(MCU);
                    this.errMsg = RadIview.errMsg;
                }
                return (true);
            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
                return (false);
            }
        }
        #endregion
        //FB 2556 Ends
        //FB 2709 End

        //FB 2553-RMX Starts
        #region SetLeaderParty
        /// <summary>
        /// SetLeaderParty
        /// </summary>
        /// <param name="inXml"></param>
        /// <param name="outXml"></param>
        /// <param name="msg"></param>
        /// <returns></returns>
        internal bool SetLeaderParty(string inXml, ref string outXml, ref string msg)
        {
            try
            {
                logger.Trace("Entering the function SetLeaderParty ...");
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(inXml);

                string login = "11";
                if (xd.SelectSingleNode("//login/userID") != null)
                    login = xd.SelectSingleNode("//login/userID").InnerXml.Trim();

                string strConfId = "";
                if (xd.SelectSingleNode("//login/confID") != null)
                    strConfId = xd.SelectSingleNode("//login/confID").InnerXml.Trim();

                int confId = 0, instanceId = 0;
                bool ret = ExtractConfInstanceIds(strConfId, ref confId, ref instanceId);
                if (!ret)
                {
                    logger.Trace("Invalid conf/instance id.");
                    return false;
                }

                int endpointId = 0;
                if (xd.SelectSingleNode("//login/endpointID") != null)
                    Int32.TryParse(xd.SelectSingleNode("//login/endpointID").InnerXml.Trim(), out endpointId); // database id

                int leaderParty = 0;
                if (xd.SelectSingleNode("//login/leaderParty") != null)
                    Int32.TryParse(xd.SelectSingleNode("//login/leaderParty").InnerXml.Trim(), out leaderParty);

                bool isLeaderParty = true;
                if (leaderParty == 0)
                    isLeaderParty = false;

                int terminalType = 0;
                if (xd.SelectSingleNode("//login/terminalType") != null)
                    Int32.TryParse(xd.SelectSingleNode("//login/terminalType").InnerXml.Trim(), out terminalType); // 1 = user , 2 = room, 3 = guest	

                logger.Trace("Terminaltype = " + terminalType.ToString());

                NS_MESSENGER.Conference conf = new NS_MESSENGER.Conference();
                NS_DATABASE.Database db = new NS_DATABASE.Database(configParams);
                conf.iDbID = confId;
                conf.iInstanceID = instanceId;
                ret = db.FetchConf(ref conf);
                if (!ret)
                {
                    logger.Exception(200, "Database fetch operation failed.");
                    return (false);
                }

                NS_MESSENGER.Party party = new NS_MESSENGER.Party();
                switch (terminalType)
                {
                    case 1:
                        {
                            // user
                            bool ret2 = db.FetchUser(confId, instanceId, endpointId, ref party);
                            break;
                        }
                    case 2:
                        {
                            // room
                            logger.Trace("In room...");
                            bool ret2 = db.FetchRoom(confId, instanceId, endpointId, ref party);
                            break;
                        }
                    case 3:
                        {
                            // guest
                            bool ret2 = db.FetchGuest(confId, instanceId, endpointId, ref party);
                            break;
                        }
                }

                // connect to the bridge and perform the operation.
                bool ret3 = false;
                if (party.cMcu.etType == NS_MESSENGER.MCU.eType.ACCORDv6 || party.cMcu.etType == NS_MESSENGER.MCU.eType.ACCORDv7 || party.cMcu.etType == NS_MESSENGER.MCU.eType.RMX)
                {
                    //NS_POLYCOM.Polycom accord = new NS_POLYCOM.Polycom(configParams);
                    ret3 = polycom.OngoingConfOps("SetLeaderParty", conf, party, ref msg, true, 0, 0, false, ref party.etStatus, false, isLeaderParty);
                    if (!ret3)
                    {
                        this.errMsg = polycom.errMsg;
                        msg = polycom.errMsg;
                    }
                }
                else
                {
                    //if (party.cMcu.etType == NS_MESSENGER.MCU.eType.CODIAN)
                    //{
                    //    NS_CODIAN.Codian codian = new NS_CODIAN.Codian(configParams);
                    //    ret3 = codian.TerminateEndpoint(conf, party);
                    //    if (!ret3)
                    //    {
                    //        this.errMsg = codian.errMsg;
                    //    }
                    //}
                    ////FB 2501 Call Monitoring Start
                    //else if (party.cMcu.etType == NS_MESSENGER.MCU.eType.CISCOTP)
                    //{
                    //    NS_CiscoTPServer.CiscoTPServer CiscoTPServer = new NS_CiscoTPServer.CiscoTPServer(configParams);
                    //    ret3 = CiscoTPServer.TerminateEndpoint(conf, party);
                    //    if (!ret3)
                    //    {
                    //        this.errMsg = CiscoTPServer.errMsg;
                    //    }
                    //}
                    ////FB 2501 Call Monitoring End
                    //else
                    //{
                    //    // Tandberg MCU
                    //    NS_TANDBERG.Tandberg tandberg = new NS_TANDBERG.Tandberg(configParams);
                    //    ret3 = tandberg.TerminateEndpoint(conf, party);
                    //    if (!ret3)
                    //    {
                    //        this.errMsg = tandberg.errMsg;
                    //    }
                    //}
                }
                if (!ret3)
                {
                    logger.Trace("SetLeaderParty operation failed for endpoint.");
                    return false;
                }

                return true;
            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
                return (false);
            }
        }
        #endregion

        #region SetLectureMode
        /// <summary>
        /// SetLectureMode
        /// </summary>
        /// <param name="inXml"></param>
        /// <param name="outXml"></param>
        /// <param name="msg"></param>
        /// <returns></returns>
        internal bool SetLectureMode(string inXml, ref string outXml, ref string msg)
        {
            try
            {
                logger.Trace("Entering the function SetLectureMode ...");
                // Load the string into xml parser and retrieve the tag values.
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(inXml);

                string login = "11";
                if (xd.SelectSingleNode("//login/userID") != null)
                    login = xd.SelectSingleNode("//login/userID").InnerXml.Trim();

                string strConfId = "";
                if (xd.SelectSingleNode("//login/confID") != null)
                    strConfId = xd.SelectSingleNode("//login/confID").InnerXml.Trim();

                int confId = 0, instanceId = 0;
                bool ret = ExtractConfInstanceIds(strConfId, ref confId, ref instanceId);
                if (!ret)
                {
                    logger.Trace("Invalid conf/instance id.");
                    return false;
                }

                int endpointId = 0;
                if (xd.SelectSingleNode("//login/endpointID") != null)
                    Int32.TryParse(xd.SelectSingleNode("//login/endpointID").InnerXml.Trim(), out endpointId); // database id


                int isLectureParty = 0;
                if (xd.SelectSingleNode("//login/isLecturer") != null)
                    Int32.TryParse(xd.SelectSingleNode("//login/isLecturer").InnerXml.Trim(), out isLectureParty);

                int terminalType = Int32.Parse(xd.SelectSingleNode("//login/terminalType").InnerXml.Trim()); // 1 = user , 2 = room, 3 = guest								 
                logger.Trace("Terminaltype = " + terminalType.ToString());

                // Fetch the conf unique name since thats the unique identifier of the conf.
                NS_MESSENGER.Conference conf = new NS_MESSENGER.Conference();
                NS_DATABASE.Database db = new NS_DATABASE.Database(configParams);
                conf.iDbID = confId;
                conf.iInstanceID = instanceId;
                ret = db.FetchConf(ref conf);
                if (!ret)
                {
                    logger.Exception(200, "Database fetch operation failed.");
                    return (false);
                }

                if (isLectureParty == 0)
                {
                    conf.bLectureMode = false;
                    logger.Trace("Lecture Mode : Disabled ");
                }
                else
                {
                    conf.bLectureMode = true;
                    logger.Trace("Lecture Mode : Enabled ");
                }

                // Retreive the bridge info for that endpoint.
                NS_MESSENGER.Party party = new NS_MESSENGER.Party();
                switch (terminalType)
                {
                    case 1:
                        {
                            // user
                            bool ret2 = db.FetchUser(confId, instanceId, endpointId, ref party);
                            break;
                        }
                    case 2:
                        {
                            // room
                            logger.Trace("In room...");
                            bool ret2 = db.FetchRoom(confId, instanceId, endpointId, ref party);
                            break;
                        }
                    case 3:
                        {
                            // guest
                            bool ret2 = db.FetchGuest(confId, instanceId, endpointId, ref party);
                            break;
                        }
                }

                // connect to the bridge and perform the operation.
                bool ret3 = false;
                if (party.cMcu.etType == NS_MESSENGER.MCU.eType.ACCORDv6 || party.cMcu.etType == NS_MESSENGER.MCU.eType.ACCORDv7 || party.cMcu.etType == NS_MESSENGER.MCU.eType.RMX)
                {
                    //NS_POLYCOM.Polycom accord = new NS_POLYCOM.Polycom(configParams);
                    ret3 = polycom.OngoingConfOps("SetLectureMode", conf, party, ref msg, true, 0, 0, false, ref party.etStatus, false, false);
                    if (!ret3)
                    {
                        this.errMsg = polycom.errMsg;
                        msg = polycom.errMsg;
                    }
                }
                else
                {
                    //if (party.cMcu.etType == NS_MESSENGER.MCU.eType.CODIAN)
                    //{
                    //    NS_CODIAN.Codian codian = new NS_CODIAN.Codian(configParams);
                    //    ret3 = codian.TerminateEndpoint(conf, party);
                    //    if (!ret3)
                    //    {
                    //        this.errMsg = codian.errMsg;
                    //    }
                    //}
                    ////FB 2501 Call Monitoring Start
                    //else if (party.cMcu.etType == NS_MESSENGER.MCU.eType.CISCOTP)
                    //{
                    //    NS_CiscoTPServer.CiscoTPServer CiscoTPServer = new NS_CiscoTPServer.CiscoTPServer(configParams);
                    //    ret3 = CiscoTPServer.TerminateEndpoint(conf, party);
                    //    if (!ret3)
                    //    {
                    //        this.errMsg = CiscoTPServer.errMsg;
                    //    }
                    //}
                    ////FB 2501 Call Monitoring End
                    //else
                    //{
                    //    // Tandberg MCU
                    //    NS_TANDBERG.Tandberg tandberg = new NS_TANDBERG.Tandberg(configParams);
                    //    ret3 = tandberg.TerminateEndpoint(conf, party);
                    //    if (!ret3)
                    //    {
                    //        this.errMsg = tandberg.errMsg;
                    //    }
                    //}
                }
                if (!ret3)
                {
                    logger.Trace("SetLectureMode operation failed for endpoint.");
                    return false;
                }

                return true;
            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
                return (false);
            }
        }
        #endregion
        //FB 2553-RMX Ends

		//ZD 100518 Starts
        #region CheckConferenceCalls
        /// <summary>
        /// CheckConferenceCalls
        /// </summary>
        /// <param name="inXml"></param>
        /// <param name="outXml"></param>
        /// <param name="msg"></param>
        /// <returns></returns>
        internal bool CheckConferenceCalls(string inXml, ref string outXml, ref string msg)
        {
            try
            {
                int EnableCloudInstallation = 0;
                XmlDocument xd = new XmlDocument();
                NS_DATABASE.Database db = new NS_DATABASE.Database(configParams);
                bool ret = false;
                string StaticID = "";//ZD 100890
                int editConfId = 0, orgId = 0, mcuId = 0, TZId = 0, MeetingId = 0;//ZD 100890
                DateTime StartDateTime = new DateTime(), EndDateTime = new DateTime();
                db.FetchSysSettingDetails(ref EnableCloudInstallation);

                if (EnableCloudInstallation == 1)
                {
                    logger.Trace("Entering the function...");
                    
                    xd.LoadXml(inXml);

                    if (xd.SelectSingleNode("//CheckConferenceCalls/StartDateTime") != null)
                        DateTime.TryParse(xd.SelectSingleNode("//CheckConferenceCalls/StartDateTime").InnerXml.Trim(), out StartDateTime);

                    if (xd.SelectSingleNode("//CheckConferenceCalls/EndDateTime") != null)
                        DateTime.TryParse(xd.SelectSingleNode("//CheckConferenceCalls/EndDateTime").InnerXml.Trim(), out EndDateTime);

                    if (xd.SelectSingleNode("//CheckConferenceCalls/editConfId") != null)
                        Int32.TryParse(xd.SelectSingleNode("//CheckConferenceCalls/editConfId").InnerXml.Trim(), out editConfId);

                    if (xd.SelectSingleNode("//CheckConferenceCalls/BridgeId") != null)
                        Int32.TryParse(xd.SelectSingleNode("//CheckConferenceCalls/BridgeId").InnerXml.Trim(), out mcuId);//Doubt

                    if (xd.SelectSingleNode("//CheckConferenceCalls/orgId") != null)
                        Int32.TryParse(xd.SelectSingleNode("//CheckConferenceCalls/orgId").InnerXml.Trim(), out orgId);

                    if (xd.SelectSingleNode("//CheckConferenceCalls/timeZone") != null)
                        Int32.TryParse(xd.SelectSingleNode("//CheckConferenceCalls/timeZone").InnerXml.Trim(), out TZId);

                    //ZD 100890
                    if (xd.SelectSingleNode("//CheckConferenceCalls/MeetingId") != null)
                        Int32.TryParse(xd.SelectSingleNode("//CheckConferenceCalls/MeetingId").InnerXml.Trim(), out MeetingId);

                    if (xd.SelectSingleNode("//CheckConferenceCalls/StaticID") != null)
                        StaticID = xd.SelectSingleNode("//CheckConferenceCalls/StaticID").InnerXml.Trim();
                    //ZD 100890

                    NS_MESSENGER.MCU MCU = new NS_MESSENGER.MCU();
                    db = new NS_DATABASE.Database(configParams);

                    if (mcuId >= 0)
                    {
                        bool ret1 = db.FetchCloudMcu(mcuId, ref MCU, orgId);
                        if (!ret1)
                        {
                            logger.Trace("Error fetching mcu details.");
                            return (false);
                        }
                    }
                    if (MCU.etType == NS_MESSENGER.MCU.eType.RADVISIONIVIEW)
                    {
                        NS_RADVISION_IView.RadVisionIView RadIview = new NS_RADVISION_IView.RadVisionIView(configParams);
                        logger.Trace("Entering Radvision IView sub-system...");
                        ret = RadIview.CheckConferenceCalls(StartDateTime, EndDateTime, editConfId, orgId, MCU, TZId, MeetingId, StaticID);//ZD 100890
                        this.errMsg = RadIview.errMsg;
                        if (!ret)
                        {
                            logger.Trace("Concurrent Meeting limit reached.Please contact Admin");
                            return (false);
                        }
                    }
                }
                return (true);
            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
                return (false);
            }
        }
        #endregion
		//ZD 100518 Ends
		 //ZD 100221 Starts
        #region CreateWebEXMeeting
        /// <summary>
        /// CreateWebEXMeeting
        /// </summary>
        /// <param name="inXml"></param>
        /// <param name="outXml"></param>
        /// <param name="msg"></param>
        /// <returns></returns>
        public bool ScheduleWebEXMeeting(string _inXml, ref string _outXml, ref string _msg)
        {

            int _Confid = 0, _instanceid = 0;
            bool _ret = false;
            string _WebEXPassword = "", _WebEXSiteID = "", _WebEXPartnerID = "",  _ConfID = "";
            string _WebExURL = "", _WebEXEmail = "", _WebEXUserName = "", _WebexMeetingPwd = "", _WETEnabled = "0"; //ZD 100513
            

            XmlDocument _xdoc = null;
            NS_MESSENGER.Conference _Conf = null;
            NS_DATABASE.Database _db = null;
           
           
            try
            {
                _xdoc = new XmlDocument();
                _Conf = new NS_MESSENGER.Conference();
                _db = new NS_DATABASE.Database(configParams);

                _xdoc.LoadXml(_inXml);

                if (_xdoc.SelectSingleNode("//conferences/conference/confID") != null)
                    _ConfID = _xdoc.SelectSingleNode("//conferences/conference/confID").InnerText.Trim();

                _ret = false;
                _ret = ExtractConfInstanceIds(_ConfID, ref _Confid, ref _instanceid);
                if (!_ret)
                {
                    logger.Trace("Invalid conf/instance id.");
                    return false;
                }

                if (_xdoc.SelectSingleNode("//conferences/conference/WebEXIntegration/WebEXUserName") != null)
                    _WebEXUserName = _xdoc.SelectSingleNode("//conferences/conference/WebEXIntegration/WebEXUserName").InnerText.Trim();

                if (_xdoc.SelectSingleNode("//conferences/conference/WebEXIntegration/WebEXPassword") != null)
                    _WebEXPassword = _xdoc.SelectSingleNode("//conferences/conference/WebEXIntegration/WebEXPassword").InnerText.Trim();

                if (_xdoc.SelectSingleNode("//conferences/conference/WebEXIntegration/WebEXSiteID") != null)
                    _WebEXSiteID = _xdoc.SelectSingleNode("//conferences/conference/WebEXIntegration/WebEXSiteID").InnerText.Trim();

                if (_xdoc.SelectSingleNode("//conferences/conference/WebEXIntegration/WebEXPartnerID") != null)
                    _WebEXPartnerID = _xdoc.SelectSingleNode("//conferences/conference/WebEXIntegration/WebEXPartnerID").InnerText.Trim();

                if (_xdoc.SelectSingleNode("//conferences/conference/WebEXIntegration/WebExURL") != null)
                    _WebExURL = _xdoc.SelectSingleNode("//conferences/conference/WebEXIntegration/WebExURL").InnerText.Trim();

                if (_xdoc.SelectSingleNode("//conferences/conference/WebEXIntegration/WebEXUserEmail") != null)
                    _WebEXEmail = _xdoc.SelectSingleNode("//conferences/conference/WebEXIntegration/WebEXUserEmail").InnerText.Trim();

                if (_xdoc.SelectSingleNode("//conferences/conference/WebEXIntegration/WebEXMeetingPassword") != null)
                    _WebexMeetingPwd = _xdoc.SelectSingleNode("//conferences/conference/WebEXIntegration/WebEXMeetingPassword").InnerText.Trim();

                if (_xdoc.SelectSingleNode("//conferences/conference/WebEXIntegration/IsWETEnabled") != null) //ZD100513
                    _WETEnabled = _xdoc.SelectSingleNode("//conferences/conference/WebEXIntegration/IsWETEnabled").InnerText.Trim();
                

                _Conf.iDbID = _Confid;
                _Conf.iInstanceID = _instanceid;
                _Conf.sWebEXPartnerID = _WebEXPartnerID;
                _Conf.sWebEXPassword = _WebEXPassword;
                _Conf.sWebEXSiteID = _WebEXSiteID;
                _Conf.sWebExURL = _WebExURL;
                _Conf.sWebEXUserName = _WebEXUserName;
                _Conf.sWebEXEmail = _WebEXEmail;
                _Conf.sWebEXMeetingpwd = _WebexMeetingPwd;

                if (_WETEnabled == "1") //ZD 100513
                    return true;

                _ret = false;
                _ret = _db.FetchUsers(_Conf.iDbID, _Conf.iInstanceID, ref _Conf.qParties, -1);
                if (!_ret)
                {
                    logger.Exception(100, "Error in fetching users from db.");
                    return false;
                }

                _ret = false;
                _ret = _db.FetchGuests(_Conf.iDbID, _Conf.iInstanceID, ref _Conf.qParties, -1);
                if (!_ret)
                {
                    logger.Exception(100, "Error in fetching guests from db.");
                    return false;
                }
               

                if (iRecurring)
                {
                    _Conf.iRecurring = 1;
                    _ret = false;
                    _ret = _db.FetchRecurrence(_Conf.iDbID, ref _Conf.Recurrence);
                    if (!_ret)
                    {
                        logger.Exception(100, "Error in fetching guests from db.");
                        return false;
                    }
                   

                }

                NS_CiscoWebex.CiscoWebex _webEX = new NS_CiscoWebex.CiscoWebex(configParams);
                _webEX._isRecurring = iRecurring;

                

               _ret = _webEX.ScheduleMeeting(_Conf);
               if (!_ret)
                {
                    _outXml = "";
                    return false;
                }

                return true;
            }
            catch (Exception ex)
            {

                return false;
            }
        }
        #endregion

        #region DeleteWebEXMeeting

        public bool DeleteWebEXMeeting(string _inXml, ref string _outXml, ref string _msg)
        {
            XmlDocument _xdoc = null;
            NS_MESSENGER.Conference _Conf = null;

            int _Confid = 0, _instanceid = 0;
            string _WebEXPassword = "", _WebEXSiteID = "", _WebEXPartnerID = "", _ConfID = "";
            string _WebExURL = "", _WebEXEmail = "", _WebEXUserName = "", _WebexMeetingPwd = "",_FromCmd="";

            bool _ret = false;
            try
            {
                _xdoc = new XmlDocument();
                _Conf = new NS_MESSENGER.Conference();

                _xdoc.LoadXml(_inXml);

                if (_xdoc.SelectSingleNode("//conferences/conference/confID") != null)
                    _ConfID = _xdoc.SelectSingleNode("//conferences/conference/confID").InnerText.Trim();

                _ret = false;
                _ret = ExtractConfInstanceIds(_ConfID, ref _Confid, ref _instanceid);
                if (!_ret)
                {
                    logger.Trace("Invalid conf/instance id.");
                    return false;
                }

                if (_xdoc.SelectSingleNode("//conferences/conference/WebEXIntegration/WebEXUserName") != null)
                    _WebEXUserName = _xdoc.SelectSingleNode("//conferences/conference/WebEXIntegration/WebEXUserName").InnerText.Trim();

                if (_xdoc.SelectSingleNode("//conferences/conference/WebEXIntegration/WebEXPassword") != null)
                    _WebEXPassword = _xdoc.SelectSingleNode("//conferences/conference/WebEXIntegration/WebEXPassword").InnerText.Trim();

                if (_xdoc.SelectSingleNode("//conferences/conference/WebEXIntegration/WebEXSiteID") != null)
                    _WebEXSiteID = _xdoc.SelectSingleNode("//conferences/conference/WebEXIntegration/WebEXSiteID").InnerText.Trim();

                if (_xdoc.SelectSingleNode("//conferences/conference/WebEXIntegration/WebEXPartnerID") != null)
                    _WebEXPartnerID = _xdoc.SelectSingleNode("//conferences/conference/WebEXIntegration/WebEXPartnerID").InnerText.Trim();

                if (_xdoc.SelectSingleNode("//conferences/conference/WebEXIntegration/WebExURL") != null)
                    _WebExURL = _xdoc.SelectSingleNode("//conferences/conference/WebEXIntegration/WebExURL").InnerText.Trim();

                if (_xdoc.SelectSingleNode("//conferences/conference/WebEXIntegration/WebEXEmail") != null)
                    _WebEXEmail = _xdoc.SelectSingleNode("//conferences/conference/WebEXIntegration/WebEXEmail").InnerText.Trim();

                if (_xdoc.SelectSingleNode("//conferences/conference/WebEXIntegration/WebEXMeetingPassword") != null)
                    _WebexMeetingPwd = _xdoc.SelectSingleNode("//conferences/conference/WebEXIntegration/WebEXMeetingPassword").InnerText.Trim();

                if (_xdoc.SelectSingleNode("//conferences/conference/WebEXIntegration/FromCommand") != null)
                    _FromCmd = _xdoc.SelectSingleNode("//conferences/conference/WebEXIntegration/FromCommand").InnerText.Trim();

                _Conf.iDbID = _Confid;
                _Conf.iInstanceID = _instanceid;
                _Conf.sWebEXPartnerID = _WebEXPartnerID;
                _Conf.sWebEXPassword = _WebEXPassword;
                _Conf.sWebEXSiteID = _WebEXSiteID;
                _Conf.sWebExURL = _WebExURL;
                _Conf.sWebEXUserName = _WebEXUserName;
                _Conf.sWebEXEmail = _WebEXEmail;
                _Conf.sWebEXMeetingpwd = _WebexMeetingPwd;

                NS_CiscoWebex.CiscoWebex _webEX = new NS_CiscoWebex.CiscoWebex(configParams);
                if (_FromCmd == "ForceConfDelete")
                     _ret = _webEX.DeleteWebEXMeeting(_Conf,2); //if force delete or teminate ongoing call use the status as 2 
                else if (_FromCmd == "PendingConference")
                    _ret = _webEX.DeleteWebEXMeeting(_Conf, 3); //if Auto approve
                else
                    _ret = _webEX.DeleteWebEXMeeting(_Conf, 1);// for normal or recurrence use status as 1
                if (!_ret)
                {
                    _outXml = "";
                    return false;
                }


                return true;
            }
            catch (Exception ex)
            {

                return false;
            }
        }
        #endregion
        //ZD 100221 Ends
		//ZD 100152 Starts
        #region SaveGoolgeUID
        /// <summary>
        /// SaveGoolgeUID
        /// </summary>
        /// <param name="inxml"></param>
        /// <param name="outxml"></param>
        /// <param name="msg"></param>
        /// <returns></returns>
        internal bool SaveGoolgeUID(string inxml, ref string outxml, ref string msg)
        {
            XmlDocument _xdoc = new XmlDocument();
            bool _ret = false;
            int _confId = 0, _instanceId = 0;
            NS_DATABASE.Database _db = null;
            string _UpdatedDateTime = "";
            try
            {
                _xdoc.LoadXml(inxml);

                string _confid = "";
                if (_xdoc.SelectSingleNode("GoogleData/confid") != null)
                    _confid = _xdoc.SelectSingleNode("GoogleData/confid").InnerText;

                NS_OPERATIONS.Operations _ops = new Operations(this.configParams);
                _ret = _ops.ExtractConfInstanceIds(_confid, ref _confId, ref _instanceId);

                string _GoogleGuid = "";
                if (_xdoc.SelectSingleNode("GoogleData/GoogleID") != null)
                    _GoogleGuid = _xdoc.SelectSingleNode("GoogleData/GoogleID").InnerText;

                int _GoogleSequence = 0;
                if (_xdoc.SelectSingleNode("GoogleData/GoogleConfSeq") != null)
                    int.TryParse(_xdoc.SelectSingleNode("GoogleData/GoogleConfSeq").InnerText,out _GoogleSequence);

                if (_xdoc.SelectSingleNode("GoogleData/GoogleConfUpdated") != null)
                    _UpdatedDateTime=_xdoc.SelectSingleNode("GoogleData/GoogleConfUpdated").InnerText;

                _db = new NS_DATABASE.Database(configParams);
                _ret = false;
                _ret = _db.UpdateConferenceGoogleid(_confId, _instanceId, _GoogleGuid, _GoogleSequence, _UpdatedDateTime);
                if (!_ret)
                    return false;

                return true;
            }
            catch (Exception ex)
            {
                logger.Trace("SaveGoolgeUID" + ex.Message);
                return false;
            }
        }
        #endregion

        #region GetGoogleConference
        /// <summary>
        /// SaveGoolgeUID
        /// </summary>
        /// <param name="inxml"></param>
        /// <param name="outxml"></param>
        /// <param name="msg"></param>
        /// <returns></returns>
        internal bool GetGoogleConference(string inxml, ref string outxml, ref string msg)
        {
            XmlDocument _xdoc = new XmlDocument();
            bool _ret = false;
            NS_DATABASE.Database _db = null;
            DataSet ds = null;
            DateTime Lastrundate = DateTime.UtcNow; //ALLDEV-856
            try
            {
                _xdoc.LoadXml(inxml);

                string _GoogleGuid = "";
                if (_xdoc.SelectSingleNode("GetGoogleConference/GoogleGUID") != null)
                    _GoogleGuid = _xdoc.SelectSingleNode("GetGoogleConference/GoogleGUID").InnerText;

                string _userID = "";
                if (_xdoc.SelectSingleNode("GetGoogleConference/userid") != null)
                    _userID = _xdoc.SelectSingleNode("GetGoogleConference/userid").InnerText;

                if (_xdoc.SelectSingleNode("GetGoogleConference/LastrunDatetime") != null) //ALLDEV-856
                    DateTime.TryParse(_xdoc.SelectSingleNode("GetGoogleConference/LastrunDatetime").InnerText, out Lastrundate);

                _db = new NS_DATABASE.Database(configParams);
                _ret = false; ds = new DataSet();
                _ret = _db.GetGoogleConference(_GoogleGuid, _userID, Lastrundate, ref ds);
                if (!_ret)
                {
                    outxml = "";
                    return false;
                }
                else
                {
                    outxml = "<GetGoogleConference>";
                    if (ds != null)
                    {
                        for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                        {
                            if (ds.Tables[0].Rows.Count > 1)
                                outxml += "<GoogleConference>";

                            outxml += "<confid>" + ds.Tables[0].Rows[i]["confid"] + "</confid>";
                            outxml += "<InstanceID>" + ds.Tables[0].Rows[i]["instanceid"] + "</InstanceID>";
                            outxml += "<Recurring>" + ds.Tables[0].Rows[i]["recur"] + "</Recurring>";
                            outxml += "<GoogleConfLastUpdated>" + ds.Tables[0].Rows[i]["GoogleConfLastUpdated"] + "</GoogleConfLastUpdated>";
                            outxml += "<GoogleGuid>" + ds.Tables[0].Rows[i]["GoogleGuid"] + "</GoogleGuid>"; //ALLDEV-856
                            outxml += "<orgId>" + ds.Tables[0].Rows[i]["orgId"] + "</orgId>"; //ALLDEV-856

                            if (ds.Tables[0].Rows.Count > 1)
                                outxml += "</GoogleConference>";

                        }
                    }
                    outxml += "</GetGoogleConference>";
                }

                return true;
            }
            catch (Exception ex)
            {
                logger.Trace("SaveGoolgeUID" + ex.Message);
                return false;
            }
        }
        #endregion

        internal bool CheckGooglePoll(string inxml, ref string outxml, ref string msg)
        {
            XmlDocument _xdoc = new XmlDocument();
            bool _ret = false;
            NS_DATABASE.Database _db = null;
            DataSet ds = null;
            int userid = 0, pollstatus=0;

            try
            {
                _db = new NS_DATABASE.Database(configParams);
                _xdoc.LoadXml(inxml);
                
                if(_xdoc.SelectSingleNode("CheckGooglePoll/userid")!=null)
                    int.TryParse((_xdoc.SelectSingleNode("CheckGooglePoll/userid").InnerText),out userid);

                _ret=_db.CheckGooglePoll(userid,ref pollstatus);
                if (!_ret)
                {
                    logger.Trace("CheckGooglePoll failed");
                    return false;
                }
                outxml = "<GetCheckGooglePoll><PollStatus>"+pollstatus+"</PollStatus></GetCheckGooglePoll>";
               
                return true;
            }
            catch ( Exception ex)
            {
                logger.Trace("CheckGooglePoll" + ex.Message);
                return false;
            }
        }

        internal bool DeleteCheckGooglePoll(string inxml, ref string outxml, ref string msg)
        {
            XmlDocument _xdoc = new XmlDocument();
            bool _ret = false;
            NS_DATABASE.Database _db = null;
            DataSet ds = null;
            int userid = 0;

            try
            {
                _db = new NS_DATABASE.Database(configParams);
                _xdoc.LoadXml(inxml);
               
                if (_xdoc.SelectSingleNode("DeleteCheckGooglePoll/userid") != null)
                    int.TryParse((_xdoc.SelectSingleNode("DeleteCheckGooglePoll/userid").InnerText), out userid);

                _ret = _db.DeleteCheckGooglePoll(userid);
                if (!_ret)
                {
                    logger.Trace("DeleteCheckGooglePoll failed");
                    return false;
                }
                outxml = "<GetDeleteCheckGooglePoll>Success</GetDeleteCheckGooglePoll>";

                return true;
            }
            catch (Exception ex)
            {
                logger.Trace("CheckGooglePoll" + ex.Message);
                return false;
            }
        }

         
        //ZD 100152 Ends

        //ZD 100522 Start
        internal bool TerminateVMRConference(string inXml, ref string outXml, ref string msg)
        {
            NS_DATABASE.Database db = null;
            try
            {
                logger.Trace("Entering the Terminate Conference function...");

                if (db == null)
                    db = new NS_DATABASE.Database(configParams);

                #region parse inXml
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(inXml);

                string login = "";
                if (xd.SelectSingleNode("//login/userID") != null)
                    login = xd.SelectSingleNode("//login/userID").InnerXml.Trim();

                int roomID = 0;
                if (xd.SelectSingleNode("//login/roomID") != null)
                    int.TryParse(xd.SelectSingleNode("//login/roomID").InnerXml.Trim(), out roomID);

                string strConfId = "";
                bool ret = db.FetchVMRRoom(roomID, ref strConfId);
                if (string.IsNullOrWhiteSpace(strConfId))
                    return true;

                int confId = 0, instanceId = 0;
                ret = ExtractConfInstanceIds(strConfId, ref confId, ref instanceId);
                if (!ret)
                {
                    logger.Trace("Invalid conf/instance id.");
                    return false;
                }
                #endregion

                #region Fetch conf
                // Fetch the conf details - unique id, users,guests,rooms.
                NS_MESSENGER.Conference conf = new NS_MESSENGER.Conference();
                conf.iDbID = confId;
                conf.iInstanceID = instanceId;
                ret = db.FetchConf(ref conf);
                if (!ret)
                {
                    logger.Exception(200, "Database fetch operation failed.");
                    return (false);
                }
                logger.Trace("Conf Name :" + conf.iDbNumName.ToString() + "-" + conf.sExternalName);
                #endregion

                #region Fetch MCU
                List<int> mcuIdList = new List<int>();
                bool ret2 = db.FetchConfMcu(conf, ref mcuIdList);
                if (!ret2)
                {
                    logger.Exception(200, "Database fetch operation failed.");
                    return (false);
                }
                #endregion

                #region Connect to all the bridges and terminate the conference, one mcu at a time.

                logger.Trace("Number of mcu's on which conf is running :" + mcuIdList.Count.ToString());
                for (int k = 0; k < mcuIdList.Count; k++)
                {
                    int mcuId = mcuIdList[k];

                    //Fetch the mcu info					
                    NS_MESSENGER.MCU mcu = new NS_MESSENGER.MCU();
                    bool ret1 = db.FetchMcu(mcuId, ref mcu);
                    if (!ret1)
                    {
                        logger.Trace("Error fetching mcu details.");
                        continue;
                    }

                    // Assign the conf bridge to the "mcu".
                    bool ret3 = false;
                    conf.cMcu = mcu;
                    if (mcu.etType == NS_MESSENGER.MCU.eType.ACCORDv6 || mcu.etType == NS_MESSENGER.MCU.eType.ACCORDv7 || mcu.etType == NS_MESSENGER.MCU.eType.RMX)
                    {
                        NS_MESSENGER.Party dummyParty = new NS_MESSENGER.Party();
                        dummyParty.cMcu = conf.cMcu;
                        ret3 = polycom.OngoingConfOps("TerminateConference", conf, dummyParty, ref msg, true, 0, 0, false, ref dummyParty.etStatus, false, false); 
                        if (!ret3)
                        {
                            this.errMsg = polycom.errMsg;
                            msg = polycom.errMsg;
                        }
                    }
                    else if (mcu.etType == NS_MESSENGER.MCU.eType.CODIAN)
                    {
                        NS_CODIAN.Codian codian = new NS_CODIAN.Codian(configParams);
                        ret3 = codian.TerminateConference(conf);
                        if (!ret3) this.errMsg = codian.errMsg;
                    }
                    else if (mcu.etType == NS_MESSENGER.MCU.eType.CISCOTP)
                    {
                        NS_CiscoTPServer.CiscoTPServer CiscoTPServer = new NS_CiscoTPServer.CiscoTPServer(configParams);
                        ret3 = CiscoTPServer.TerminateConference(conf);
                        if (!ret3) this.errMsg = CiscoTPServer.errMsg;
                    }
                    if (!ret3)
                    {
                        logger.Trace("Terminate conference operation failed.");
                        alert = new NS_MESSENGER.Alert();
                        alert.confID = conf.iDbID;
                        alert.instanceID = conf.iInstanceID;
                        alert.message = this.errMsg;
                        alert.typeID = 7; 
                        bool retp2p = db.InsertConfAlert(alert);
                        if (!retp2p)
                            logger.Trace("Terminate Conference alert insert failed.");
                        
                        return false;
                    }
                }
                #endregion
                //ZD 101348 Starts
                #region Update Conference Status and Duration
                double TimeRangeinMin = DateTime.UtcNow.Subtract(conf.dtStartDateTimeInUTC).TotalMinutes;
                int confDuration = (int)Math.Round(TimeRangeinMin);
                db.UpdateConferenceDuration(conf.iDbID, conf.iInstanceID, confDuration);
                //ZD 101348 End

                conf.etStatus = NS_MESSENGER.Conference.eStatus.DELETED;
                ret = db.UpdateConferenceStatus(conf.iDbID, conf.iInstanceID, conf.etStatus, 1);
                if (!ret)
                {
                    logger.Exception(200, "Database fetch operation failed.");
                    return (false);
                }
                #endregion

                return true;
            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
                return (false);
            }
        }
		//ZD 101522 Starts
        #region FetchSystemLocation
        /// <summary>
        /// FetchSystemLocation
        /// </summary>
        /// <param name="inXml"></param>
        /// <param name="outXml"></param>
        /// <param name="msg"></param>
        /// <returns></returns>
        internal bool FetchSystemLocation(string inXml, ref string outXml, ref string msg)
        {
            try
            {
                logger.Trace("Entering the function...");
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(inXml);
                bool ret = false;
                int loginUser = 0;
                if (xd.SelectSingleNode("//GetSystemLocation/userID") != null)
                    int.TryParse(xd.SelectSingleNode("//GetSystemLocation/userID").InnerXml.Trim(), out loginUser);

                int bridgeTypeId = 0;
                if (xd.SelectSingleNode("//GetSystemLocation/BridgeTypeId") != null)
                    Int32.TryParse(xd.SelectSingleNode("//GetSystemLocation/BridgeTypeId").InnerXml.Trim(), out bridgeTypeId);

                int mcuId = 0;
                if (xd.SelectSingleNode("//GetSystemLocation/BridgeId") != null)
                    Int32.TryParse(xd.SelectSingleNode("//GetSystemLocation/BridgeId").InnerXml.Trim(), out mcuId);

                NS_MESSENGER.MCU MCU = new NS_MESSENGER.MCU();
                NS_DATABASE.Database db = new NS_DATABASE.Database(configParams);

                if (mcuId >= 0)
                {
                    ret = db.FetchMcu(mcuId, ref MCU);
                    if (!ret)
                    {
                        logger.Trace("Error fetching mcu details.");
                        return (false);
                    }
                }
                if (MCU.etType == NS_MESSENGER.MCU.eType.Pexip)
                {
                    NS_Pexip.pexip pexip = new NS_Pexip.pexip(configParams);
                    logger.Trace("Entering Pexip sub-system...");
                    ret = pexip.FetchSystemLocation(MCU);
                    this.errMsg = pexip.errMsg;
                }
                return (true);
            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
                return (false);
            }
        }
        #endregion
        //ZD 100522 End

        //ZD 101527
        #region SyncRoom
        /// <summary>
        /// SyncRoom
        /// </summary>
        internal bool SyncRoom(string inXml, ref string outXml,ref string msg)
        {
            XmlDocument xd = null;
            NS_DATABASE.Database db = null;
            NS_MESSENGER.MCU cMcu = null;
            bool ret1 = false;
            List<int> MCUId = new List<int>();
            try
            {
                logger.Trace("Entering the Fetch MCU Info method...");
                xd = new XmlDocument();
                xd.LoadXml(inXml);
                db = new NS_DATABASE.Database(configParams);
                cMcu = new NS_MESSENGER.MCU();

                ret1 = db.FetchRPRMMcu(ref MCUId, 0);
                if (!ret1)
                {
                    logger.Trace("Error in Fetching MCU details.");
                    return false;
                }

                for (int m = 0; m < MCUId.Count; m++)
                {
                    ret1 = db.FetchMcu(MCUId[m], ref cMcu);
                    if (!ret1)
                    {
                        logger.Trace("Error in Fetching MCU details.");
                        return false;
                    }

                    if (cMcu.etType == NS_MESSENGER.MCU.eType.RPRM)
                    {
                        NS_POLYCOMRPRM.POLYCOMRPRM polycomRPRM = new NS_POLYCOMRPRM.POLYCOMRPRM(configParams);
                        ret1 = polycomRPRM.FetchEndpoints(cMcu);
                        if (!ret1) this.errMsg = polycomRPRM.errMsg;
                        polycomRPRM = null;
                    }

                    if (!ret1)
                    {
                        logger.Trace("SyncRoom failed.");
                        logger.Trace("Msg = " + this.errMsg);
                        return false;
                    }
                }

                return true;
            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
                return (false);
            }
        }
        #endregion 

        #region ManualSyncRoom
        /// <summary>
        /// SyncRoom
        /// </summary>
        internal bool ManualSyncRoom(string inXml, ref string outXml, ref string msg)
        {
            XmlDocument xd = null;
            NS_DATABASE.Database db = null;
            NS_MESSENGER.MCU cMcu = null;
            bool ret1 = false;
            List<int> MCUId = new List<int>();
            int OrgID = 0;
            try
            {
                logger.Trace("Entering the Fetch MCU Info method...");
                xd = new XmlDocument();
                xd.LoadXml(inXml);
                db = new NS_DATABASE.Database(configParams);
                cMcu = new NS_MESSENGER.MCU();
                int.TryParse(xd.SelectSingleNode("//login/organizationID").InnerXml.Trim(), out OrgID);

                ret1 = db.FetchRPRMMcu(ref MCUId, OrgID);
                if (!ret1)
                {
                    logger.Trace("Error in Fetching MCU details.");
                    return false;
                }

                if (MCUId.Count == 0)
                {
                    this.errMsg = "No RPRM MCUs to Poll Rooms/Endpoints. ";
                    return false;
                }

                for (int m = 0; m < MCUId.Count; m++)
                {
                    ret1 = db.FetchMcu(MCUId[m], ref cMcu);
                    if (!ret1)
                    {
                        logger.Trace("Error in Fetching MCU details.");
                        return false;
                    }

                    if (cMcu.etType == NS_MESSENGER.MCU.eType.RPRM)
                    {
                        NS_POLYCOMRPRM.POLYCOMRPRM polycomRPRM = new NS_POLYCOMRPRM.POLYCOMRPRM(configParams);
                        ret1 = polycomRPRM.FetchEndpoints(cMcu);
                        if (!ret1) this.errMsg = polycomRPRM.errMsg;
                        polycomRPRM = null;
                    }

                    if (!ret1)
                    {
                        logger.Trace("SyncRoom failed.");
                        logger.Trace("Msg = " + this.errMsg);
                        return false;
                    }
                }

                return true;
            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
                return (false);
            }
        }
        #endregion 
        //ZD 101527 Ends

        //ZD 103263 Start
        #region GetRoomBJNDetails
        /// <summary>
        /// GetRoomBJNDetails
        /// </summary>
        /// <param name="inXml"></param>
        /// <param name="outXml"></param>
        /// <param name="msg"></param>
        /// <returns></returns>
        internal bool GetRoomBJNDetails(string inXml, ref string outXml, ref string msg)
        {
            XmlDocument xd = null;
            NS_DATABASE.Database db = null;
            bool ret1 = false;
            int RoomID = 0;
            string email = "";//, password = "";
            NS_BlueJean.BlueJean BJN = null;
            NS_MESSENGER.BJNRoomDetails BJNDetail = null;
            NS_MESSENGER.MCU mcu = null;
            try
            {
                logger.Trace("Entering the GetUserDetails method...");
                xd = new XmlDocument();
                xd.LoadXml(inXml);
                db = new NS_DATABASE.Database(configParams);
                BJN = new NS_BlueJean.BlueJean(configParams);
                BJNDetail = new NS_MESSENGER.BJNRoomDetails();
                mcu = new NS_MESSENGER.MCU();

                if (xd.SelectSingleNode("//GetRoomDetails/RoomID") != null)
                    int.TryParse(xd.SelectSingleNode("//GetRoomDetails/RoomID").InnerXml.Trim(), out RoomID);

                if (xd.SelectSingleNode("//GetRoomDetails/organizationID") != null)
                    int.TryParse(xd.SelectSingleNode("//GetRoomDetails/organizationID").InnerXml.Trim(), out mcu.iOrgId);

                if (xd.SelectSingleNode("//GetRoomDetails/RoomAdminEmail") != null)
                    email = xd.SelectSingleNode("//GetRoomDetails/RoomAdminEmail").InnerXml.Trim();

                //if (xd.SelectSingleNode("//GetUserDetails/Password") != null)
                //    password = xd.SelectSingleNode("//GetUserDetails/Password").InnerXml.Trim();

                ret1 = db.FetchAccessKey(ref mcu);
                if (!ret1)
                {
                    logger.Trace("Error in Fetching Rooms.");
                    return (false);
                }

                if (mcu.sBJNAPPkey != "" && mcu.sBJNAppsecret != "")
                {
                    ret1 = db.CheckBJNToken(ref mcu);
                    if (!ret1)
                    {
                        BJN = new NS_BlueJean.BlueJean(configParams);
                        ret1 = BJN.UpdateAccessToken(ref mcu);
                        if (!ret1) this.errMsg = BJN.errMsg;
                    }
                }
                else
                {
                    logger.Trace("BJN is not Configure properly.");
                    return (false);
                }

                ret1 = BJN.GetUserId(RoomID, email, mcu, 0);//0-Room , 1-User
                if (!ret1) this.errMsg = BJN.errMsg;
                
                ret1 = db.FetchBJNRoom(RoomID, ref BJNDetail);
                if (!ret1)
                {
                    logger.Trace("Error in Fetching BJN Rooms.");
                    return (false);
                }

                ret1 = BJN.Create_updateUserRoom(mcu.sBJNAccessToken, BJNDetail);
                if (!ret1)
                { 
                    this.errMsg = BJN.errMsg;
                    return false;
                }

                return true;
            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
                return (false);
            }
        }
        #endregion 

        #region GetUserBJNDetails
        /// <summary>
        /// GetUserBJNDetails
        /// </summary>
        /// <param name="inXml"></param>
        /// <param name="outXml"></param>
        /// <param name="msg"></param>
        /// <returns></returns>
        public bool GetUserBJNDetails(string inXml, ref string outXml, ref string msg)
        {
            XmlDocument xd = null;
            NS_DATABASE.Database db = null;
            bool ret1 = false;
            int UserID = 0;
            string email = "";//, password = "";
            NS_BlueJean.BlueJean BJN = null;
            NS_MESSENGER.BJNRoomDetails BJNDetail = null;
            NS_MESSENGER.MCU mcu = null;
            try
            {
                logger.Trace("Entering the GetUserDetails method...");
                xd = new XmlDocument();
                xd.LoadXml(inXml);
                db = new NS_DATABASE.Database(configParams);
                BJN = new NS_BlueJean.BlueJean(configParams);
                BJNDetail = new NS_MESSENGER.BJNRoomDetails();
                mcu = new NS_MESSENGER.MCU();

                if (xd.SelectSingleNode("//GetUserDetails/UserID") != null)
                    int.TryParse(xd.SelectSingleNode("//GetUserDetails/UserID").InnerXml.Trim(), out UserID);

                if (xd.SelectSingleNode("//GetUserDetails/organizationID") != null)
                    int.TryParse(xd.SelectSingleNode("//GetUserDetails/organizationID").InnerXml.Trim(), out mcu.iOrgId);

                if (xd.SelectSingleNode("//GetUserDetails/BJNUserEmail") != null)
                    email = xd.SelectSingleNode("//GetUserDetails/BJNUserEmail").InnerXml.Trim();

                //if (xd.SelectSingleNode("//GetUserDetails/Password") != null)
                //    password = xd.SelectSingleNode("//GetUserDetails/Password").InnerXml.Trim();

                //ZD 104225  Starts
                ret1 = db.UpdateBJNUserDetails(UserID, string.Empty, string.Empty, string.Empty,string.Empty,string.Empty);
                if (!ret1)
                {
                    logger.Trace("Error in Resetting BJN Tokens.");
                    return false;
                }
                //ZD 104225 Ends

                ret1 = db.FetchAccessKey(ref mcu);
                if (!ret1)
                {
                    logger.Trace("Error in Fetching Rooms.");
                    return (false);
                }

                if (mcu.sBJNAPPkey != "" && mcu.sBJNAppsecret != "")
                {
                    ret1 = db.CheckBJNToken(ref mcu);
                    if (!ret1)
                    {
                        BJN = new NS_BlueJean.BlueJean(configParams);
                        ret1 = BJN.UpdateAccessToken(ref mcu);
                        if (!ret1) this.errMsg = BJN.errMsg;
                    }
                }
                else
                {
                    logger.Trace("BJN is not Configure properly.");
                    return (false);
                }
              
                ret1 = false;
                ret1 = BJN.GetUserId(UserID, email, mcu, 1);//0-Room , 1-User
                if (!ret1) this.errMsg = BJN.errMsg;

                return true;
            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
                return (false);
            }
        }
        #endregion 

        //ZD 103263 End

        //ALLDEV-782 Starts
        #region PushStaticVMR
        /// <summary>
        /// PushStaticVMR
        /// </summary>
        /// <param name="inXml"></param>
        /// <param name="outXml"></param>
        /// <param name="msg"></param>
        /// <returns></returns>
        public bool PushStaticVMR(string inXml, ref string outXml, ref string msg)
        {
            XmlDocument xd = null;
            NS_DATABASE.Database db = null;
            bool ret1 = false;
            int MCUID = 0, OrgID = 0, userID = 0, PexipOperation = 0;
            NS_Pexip.pexip PexipMCU = null;
            NS_MESSENGER.MCU mcu = null;
            NS_MESSENGER.ActiveUser userDetails = null;
            string NewVMRAlias = "", OldVMRALias = "";
            try
            {
                logger.Trace("Entering the PushStaticVMR method...");

                xd = new XmlDocument();
                xd.LoadXml(inXml);

                db = new NS_DATABASE.Database(configParams);
                PexipMCU = new NS_Pexip.pexip(configParams);
                userDetails = new NS_MESSENGER.ActiveUser();
                mcu = new NS_MESSENGER.MCU();

                if (xd.SelectSingleNode("//Conference/UserID") != null)
                    int.TryParse(xd.SelectSingleNode("//Conference/UserID").InnerXml.Trim(), out userID);

                if (xd.SelectSingleNode("//Conference/organizationID") != null)
                    int.TryParse(xd.SelectSingleNode("//Conference/organizationID").InnerXml.Trim(), out OrgID);

                if (xd.SelectSingleNode("//Conference/AliasUpdateDelete") != null)
                    int.TryParse(xd.SelectSingleNode("//Conference/AliasUpdateDelete").InnerXml.Trim(), out PexipOperation);

                if (xd.SelectSingleNode("//Conference/OldVMRAlias") != null)
                    OldVMRALias = xd.SelectSingleNode("//Conference/OldVMRAlias").InnerXml.Trim();

                if (xd.SelectSingleNode("//Conference/NewVMRAlias") != null)
                    NewVMRAlias = xd.SelectSingleNode("//Conference/NewVMRAlias").InnerXml.Trim();

                bool ret = false;
                ret = db.FetchPexipMCU(OrgID, ref MCUID);
                if (!ret)
                {
                    logger.Exception(100, "Error in fetching Pexip mcu info from db.");
                    return false;
                }

                ret = false;
                ret = db.FetchMcu(MCUID, ref mcu);
                if (!ret)
                {
                    logger.Exception(100, "Error in fetching mcu info from db.");
                    return false;
                }

                ret = false;
                ret = db.FetchActiveUserDetails(ref userDetails, userID);
                if (!ret)
                {
                    logger.Exception(100, "Error in fetching userDetails info from db.");
                    return false;
                }

                ret = false;
                if (PexipOperation == 0) //Create New user VMR Alias
                    ret = PexipMCU.StaticVMR(userDetails, mcu);
                else if (PexipOperation == 1) //Update the User VMR  Alias
                    ret = PexipMCU.UpdateUserAlias(userDetails, OldVMRALias, mcu);
                else if (PexipOperation == 2) //Delete the User VMR
                    ret = PexipMCU.DeleteUserVMR(userDetails.PexipALiasURI, mcu);

                if (!ret1) this.errMsg = PexipMCU.errMsg;

                return true;
            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
                return (false);
            }
        }
        #endregion 
        //ALLDEV-782 Ends

#if CODE_COMMENTED_MAY_USE_IN_FUTURE
        internal bool RunMcuResourceAllocationReport(string inXml, ref string outXml)
        {
            try
            {
                logger.Trace("Entering the function...");
                outXml = "<GenerateMcuResourceAllocationReport>";
                // Load the string into xml parser and retrieve the tag values.
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(inXml);
                
                XmlNode node;
                logger.Trace("Parsing inxml...");
                int userID = Int32.Parse(xd.SelectSingleNode("//GenerateMcuResourceAllocationReport/UserID").InnerXml.Trim());
                DateTime fromDate = DateTime.Parse(xd.SelectSingleNode("//GenerateMcuResourceAllocationReport/DateFrom").InnerXml.Trim());
                DateTime toDate = DateTime.Parse(xd.SelectSingleNode("//GenerateMcuResourceAllocationReport/DateTo").InnerXml.Trim());
                
                Queue overBookedConfList = new Queue();
                
                // fetch all confs falling in that range             
                logger.Trace("Fetching all confs in the date range...");
                Queue confList = new Queue();
                NS_DATABASE.Database db = new NS_DATABASE.Database(this.configParams);
                bool ret = db.FetchConferenceList(0,fromDate, toDate, ref confList, false);
                if (!ret)
                {
                    logger.Trace("Error fetching conf list.");
                    this.errMsg = db.errMsg;
                    return false;
                }

                // check conf count
                if (confList.Count < 1)
                {
                    this.errMsg = "No active conferences found in the date range.";
                    return false;
                }

                logger.Trace("---Total confs found in the date range : " + confList.Count.ToString());

                // backup of the confList 
                Queue confListBackup = new Queue(confList);

                // cycle thru each conf 
                while (confList.Count > 0)
                {
                    // initialization
                    bool insuffucientAudioPorts = false;
                    bool insufficientVideoPorts = false;

                    // get the conf
                    NS_MESSENGER.Conference conf = new NS_MESSENGER.Conference();
                    conf = (NS_MESSENGER.Conference)confList.Dequeue();

                    // get the audio and video users within this conf.
                    int audioUsersWithinConf = 0;
                    int videoUsersWithinConf = 0;
                    ret = db.FetchAudioVideoUserListInConference(conf,ref audioUsersWithinConf,ref videoUsersWithinConf);
                    if (!ret)
                    {
                        logger.Trace ("Error fetching audio/video calls in conf.");
                        continue;
                    }

                    if (audioUsersWithinConf < 1 && videoUsersWithinConf < 1)
                    {
                        logger.Trace("No audio & video users in this conf. Hence check is skipped.");
                        continue;
                    }

                    // find all other confs falling in the above conf's duration range
                    Queue confsWithinTimeRangeList = new Queue();
                    Queue confUserUidList = new Queue();
                    Queue confRoomUidList = new Queue();

                    // get the unique bridge list out for this conf
                    logger.Trace("Fetching mcu list...");
                    Queue bridgeList = new Queue();
                    ret = false;
                    ret = db.FetchAllMcu(ref bridgeList);
                    if (!ret)
                    {
                        logger.Trace("Invalid bridge list.");
                        return false;
                    }

                    // fetch conf list - exclude the above conf
                    logger.Trace("Fetching all confs in the time range excluding the master conf...");
                    ret = false;
                    ret = db.FetchConferenceList(conf.iDbNumName, conf.dtStartDateTimeInUTC, conf.dtStartDateTimeInUTC.AddMinutes(conf.iDuration), ref confsWithinTimeRangeList, true);
                    if (!ret)
                    {
                        logger.Trace("No confs fall within the time range.");
                        continue;
                    }

                    logger.Trace("---No. of confs falling in the same time range - " + confsWithinTimeRangeList.Count.ToString());
 
                    // confs within time range list
                    while (confsWithinTimeRangeList.Count > 0)
                    {
                        // get the conf within the time range
                        NS_MESSENGER.Conference conf1 = new NS_MESSENGER.Conference();
                        conf1 = (NS_MESSENGER.Conference)confsWithinTimeRangeList.Dequeue();
                        logger.Trace("Conf Name : " + conf.sExternalName);

                        // find the conf_user_d uid's                  
                        bool ret1 = db.FetchConfUserUIDs(conf1,ref confUserUidList);
                        if (!ret1)
                        {
                           logger.Trace("Error fetching conf user UID's.");
                        }

                        // find the conf_room_d uid                        
                        ret1 = false;
                        ret1 = db.FetchConfRoomUIDs(conf1, ref confRoomUidList);
                        if (!ret)
                        {
                            logger.Trace("Error fetching conf room UID's.");
                        }
                    }

                    while (bridgeList.Count > 0)
                    {
                        NS_MESSENGER.MCU mcu = new NS_MESSENGER.MCU();
                        mcu = (NS_MESSENGER.MCU)bridgeList.Dequeue();

        #region fetch total users in confs (within the time range) excl the master conf
                        int audioUsersWithinRangeCount = 0;
                        int videoUsersWithinRangeCount = 0;                                                
                        if (confUserUidList.Count > 0)
                        {

                            // exclude that conf and find the total external attendee audio count                
                            bool ret3 = db.FetchPartyListCount(false, false, mcu.iDbId, confUserUidList, ref audioUsersWithinRangeCount);
                            if (!ret3)
                            {
                                logger.Trace("Error - Rooms within range count.");
                            }

                            // exclude that conf and find the total external attendee video count    
                            ret3 = false;
                            ret3 = db.FetchPartyListCount(true, false, mcu.iDbId, confUserUidList, ref videoUsersWithinRangeCount);
                            if (!ret3)
                            {
                                logger.Trace("Error - Rooms within range count.");
                            }
                        }
        #endregion

        #region fetch total users in confs (within the time range) excl the master conf
                        int audioRoomsWithinRangeCount = 0;
                        int videoRoomsWithinRangeCount = 0;
                        if (confRoomUidList.Count > 0)
                        {
                            // exclude that conf and find the total room audio count
                            bool ret3 = db.FetchPartyListCount(false, true, mcu.iDbId, confRoomUidList, ref audioRoomsWithinRangeCount);
                            if (!ret3)
                            {
                                logger.Trace("Error - Rooms within range count.");
                            }
                            // exclude that conf and find the total room video count                            
                            ret3 = false;
                            ret3 = db.FetchPartyListCount(true, true, mcu.iDbId, confRoomUidList, ref videoRoomsWithinRangeCount);
                            if (!ret3)
                            {
                                logger.Trace("Error - Rooms within range count.");
                            }
                        }
        #endregion

                        // compare the actual audio count with max audio count
                        if ((audioUsersWithinConf + audioUsersWithinRangeCount + audioRoomsWithinRangeCount) > mcu.iMaxConcurrentAudioCalls)
                        {
                            insuffucientAudioPorts = true;
                        }

                        // compare the actual video count with max video count
                        if ((videoUsersWithinConf + videoUsersWithinRangeCount + videoRoomsWithinRangeCount ) > mcu.iMaxConcurrentVideoCalls)
                        {
                            insufficientVideoPorts = true;
                        }
                    }
                    // if count exceeds in either add to "overbooking" queue 
                    if (insuffucientAudioPorts == true || insufficientVideoPorts == true)
                    {
                        outXml += "<Conference>";
                        outXml += "<ID>" + conf.iDbID.ToString() + "," + conf.iInstanceID.ToString() + "</ID>";
                        outXml += "<UniqueID>" + conf.iDbNumName.ToString() + "</UniqueID>";
                        outXml += "<Name>" + conf.sExternalName + "</Name>";
                        outXml += "<DateTime>" + conf.dtStartDateTime + "</DateTime>";
                        outXml += "<Timezone>" + conf.sName_StartDateTimeInLocalTZ + "</Timezone>";
                        outXml += "<Duration>" + conf.iDuration.ToString() + "</Duration>";
                        if (insuffucientAudioPorts)
                            outXml += "<InsufficientAudioPorts>Yes</InsufficientAudioPorts>";
                        else
                            outXml += "<InsufficientAudioPorts>No</InsufficientAudioPorts>";
                        if (insufficientVideoPorts)
                            outXml += "<InsufficientVideoPorts>Yes</InsufficientVideoPorts>";
                        else
                            outXml += "<InsufficientVideoPorts>No</InsufficientVideoPorts>";
                        outXml += "</Conference>";
                    }
                }
                outXml += "</GenerateMcuResourceAllocationReport>";

                return true;
            }
            catch (Exception e)
            {                
                logger.Exception(100, e.Message);
                this.errMsg = e.Message;
                return false;
            }
        }

		internal bool SetConference_SaveDiffs (string inXml,ref string outXml)
		{			
			try
			{
				logger.Trace ("Entering the SetConference_SaveDiffs method...");				
				
				// Load the string into xml parser and retrieve the tag values.
				XmlDocument xd = new XmlDocument();
				xd.LoadXml(inXml);				
				
				//XmlNode node;

				// parse the inxml and store the conf info in a persistence object
				// instantiate the conf object
				NS_PERSISTENCE.Conference newConf = new NS_PERSISTENCE.Conference();
				
				// userid 
				newConf.iOwnerID = Int32.Parse(xd.SelectSingleNode("//conference/userInfo/userID").InnerXml.Trim());				

				// confID, instanceID
				string strConfId = xd.SelectSingleNode("//conference/confInfo/confID").InnerXml.Trim();				
				logger.Trace ("StrConfID = " + strConfId);
				bool ret = false;
				if (strConfId.IndexOf(",") > 0)
				{
					// Just in case ASP sends the confid in this format - 10,1 (confid,instanceid)
					int confId = 0 ,instanceId = 0 ;
					ret = ExtractConfInstanceIds(strConfId,ref confId,ref instanceId);
					if (!ret)
					{
						logger.Trace ("Invalid conf/instance id.");
						return false;
					}
					newConf.iConfID = confId;
					newConf.iInstanceID = instanceId;
				}
				else
				{
					if(strConfId == "new")
					{
						// no updates reqd as confid/instance id have not been created yet
						outXml = "<transactionID>new</transactionID>";
						return true; 
					}					
					else
					{
						//newConf.isRecurring = true;
						newConf.iConfID = Int32.Parse(strConfId);
						newConf.iInstanceID =1;
					}
				}

				logger.Trace ("New conf -  confid = " + newConf.iConfID.ToString() + ",instanceid = " + newConf.iInstanceID.ToString());

        #region conf details
				// get the external name 
				newConf.sConfName = xd.SelectSingleNode("//conference/confInfo/confName").InnerXml.Trim();
				logger.Trace ("Name : " + newConf.sConfName);

				// get the description
				newConf.sDescription = xd.SelectSingleNode("//conference/confInfo/description").InnerXml.Trim();
				logger.Trace ("Description : " + newConf.sDescription);

				// get the password
				newConf.sPassword = xd.SelectSingleNode("//conference/confInfo/confPassword").InnerXml.Trim();
				logger.Trace ("Password : " + newConf.sPassword);

				// get the type
				newConf.iConfType = Int32.Parse(xd.SelectSingleNode("//conference/confInfo/createBy").InnerXml.Trim());
				logger.Trace ("ConfType : " + newConf.iConfType.ToString());

				// public
				int iPublic = Int32.Parse(xd.SelectSingleNode("//conference/confInfo/publicConf").InnerXml.Trim());
				if (iPublic ==1)
					newConf.isPublic = true;

        #endregion

        #region recurring check
				int recurring = Int32.Parse(xd.SelectSingleNode("//conference/confInfo/recurring").InnerXml.Trim());
				if (recurring == 0)
				{
					// conf is not recurring
					
					// start date/time
					DateTime startDate = DateTime.Parse(xd.SelectSingleNode("//conference/confInfo/startDate").InnerXml.Trim());					
					logger.Trace ("Start Date = " + startDate.ToString());
					int hour = Int32.Parse(xd.SelectSingleNode("//conference/confInfo/startHour").InnerXml.Trim());
					logger.Trace ("Hour = " + hour.ToString());
					int min = Int32.Parse(xd.SelectSingleNode("//conference/confInfo/startMin").InnerXml.Trim());
					logger.Trace ("Min = " + min.ToString());
					string ampm = xd.SelectSingleNode("//conference/confInfo/startSet").InnerXml.Trim().ToUpper();
					if (ampm == "PM")
					{
						hour = hour + 12;
					}
					logger.Trace ("AMPM = " + ampm);

					// complete date = startdate + starttime
					newConf.dtStartDateTime = new DateTime(startDate.Year,startDate.Month,startDate.Day,hour,min,00);
					logger.Trace ("Conf DateTime : " + newConf.dtStartDateTime.ToString());					

					//timezone
					newConf.iTimezoneID = Int32.Parse(xd.SelectSingleNode("//conference/confInfo/timeZone").InnerXml.Trim());

					// duration
					newConf.iDurationMin = Int32.Parse(xd.SelectSingleNode("//conference/confInfo/durationMin").InnerXml.Trim());

				}
				else
				{
					// conf is recurring 
					// start date/time
					DateTime startDate = DateTime.Parse(xd.SelectSingleNode("//conference/confInfo/recurrenceRange/startDate").InnerXml.Trim());					
					logger.Trace ("Start Date = " + startDate.ToString());

					// start time
					int hour = Int32.Parse(xd.SelectSingleNode("//conference/confInfo/appointmentTime/startHour").InnerXml.Trim());
					int min = Int32.Parse(xd.SelectSingleNode("//conference/confInfo/appointmentTime/startMin").InnerXml.Trim());
					string ampm = xd.SelectSingleNode("//conference/confInfo/appointmentTime/startSet").InnerXml.Trim().ToUpper();
					if (ampm == "PM")
					{
						hour = hour + 12;
					}

					// complete date = startdate + starttime
					newConf.dtStartDateTime = new DateTime(startDate.Year,startDate.Month,startDate.Day,hour,min,00);
					logger.Trace ("Conf DateTime : " + newConf.dtStartDateTime.ToString());					

					//timezone
					newConf.iTimezoneID = Int32.Parse(xd.SelectSingleNode("//conference/confInfo/appointmentTime/timeZone").InnerXml.Trim());					

					// duration
					newConf.iDurationMin = Int32.Parse(xd.SelectSingleNode("//conference/confInfo/appointmentTime/durationMin").InnerXml.Trim());
					logger.Trace ("Duration = " + newConf.iDurationMin.ToString());

					// TODO: recurring pattern - daily,weekly,monthly,yearly
					newConf.isRecurring = true;
					newConf.iRecur_RecurType = Int32.Parse(xd.SelectSingleNode("//conference/confInfo/recurrencePattern/recurType").InnerXml.Trim());
					/*					newConf.iRecur_DailyType = Int32.Parse(xd.SelectSingleNode("//conference/confInfo/recurrencePattern/dailyType").InnerXml.Trim());
										switch (newConf.iRecur_DailyType)
										{
											case 1 :
											{
												newConf.iRecur_DayGap = Int32.Parse(xd.SelectSingleNode("//conference/confInfo/recurrencePattern/dailyGap").InnerXml.Trim());
												break;
											}
											case 2 :
											{
												newConf.iRecur_DayGap = Int32.Parse(xd.SelectSingleNode("//conference/confInfo/recurrencePattern/dailyGap").InnerXml.Trim());
												break;
											}
											case 3 :
											{
												newConf.iRecur_DayGap = Int32.Parse(xd.SelectSingleNode("//conference/confInfo/recurrencePattern/dailyGap").InnerXml.Trim());
												break;
											}
											case 4 :
											{
												newConf.iRecur_DayGap = Int32.Parse(xd.SelectSingleNode("//conference/confInfo/recurrencePattern/dailyGap").InnerXml.Trim());
												break;
											}
										}
					
										newConf.iRecur_EndType = Int32.Parse(xd.SelectSingleNode("//conference/confInfo/recurrenceRange/endType").InnerXml.Trim());
										switch (newConf.iRecur_EndType)
										{
											case 1 :
											{
												newConf.iRecur_Occurrence = Int32.Parse(xd.SelectSingleNode("//conference/confInfo/recurrencePattern/occurrence").InnerXml.Trim());
												break;
											}
											case 2 :
											{
												newConf.iRecur_DayGap = Int32.Parse(xd.SelectSingleNode("//conference/confInfo/recurrencePattern/dailyGap").InnerXml.Trim());
												break;
											}
											case 3 :
											{
												newConf.iRecur_DayGap = Int32.Parse(xd.SelectSingleNode("//conference/confInfo/recurrencePattern/dailyGap").InnerXml.Trim());
												break;
											}
										}
					*/					
				}
        #endregion

        #region rooms
				NS_DATABASE.Database db  =  new NS_DATABASE.Database(this.configParams);
				try
				{
					XmlNodeList nodeList = xd.SelectNodes("//conference/confInfo/locationList/selected/level1ID");
					foreach (XmlNode node1 in nodeList)
					{
						NS_PERSISTENCE.Room room = new NS_PERSISTENCE.Room();
						room.roomID = Int32.Parse(node1.InnerText.Trim());
						db.FetchRoomName(room.roomID,ref room.name);
						logger.Trace ("Room ID = " + room.roomID.ToString());
						newConf.roomList.Enqueue(room);
					}
				}
				catch (Exception e)
				{
					logger.Trace("Error in fetching room data from xml. Msg = " + e.Message);
				}

        #endregion

				// get the conf info from db
				NS_PERSISTENCE.Conference currentConf = new NS_PERSISTENCE.Conference();				
				ret = false; currentConf.iConfID = newConf.iConfID;  currentConf.iInstanceID = newConf.iInstanceID ;
				ret = db.FetchConf(ref currentConf);
				if (!ret)
				{
					logger.Trace("Invalid conf data.");
					return false;
				}

				// compare the new & db conf objects
				Queue diffList = new Queue(); ret = false;
				ret = CompareConfs(currentConf,newConf,ref diffList);
				if (!ret)
				{
					logger.Trace("Problem doing diffs.");
					return false;
				}

				// insert the changes in the audit table
				// get the max transaction id 
				ret = false; int transID = 0;
				ret = db.FetchNextTransactionID(newConf.iConfID,newConf.iInstanceID,ref transID);
				if (!ret)
				{
					logger.Trace ("Error fetching next transaction ID.");
					return false;
				}
				
				// use this transid for all diffs in this transactions
				while (diffList.Count > 0)
				{
					NS_PERSISTENCE.DiffChanges diff = new NS_PERSISTENCE.DiffChanges();
					diff = (NS_PERSISTENCE.DiffChanges) diffList.Dequeue(); 
					ret = false; 
					ret = db.InsertConfDiffRecord(diff,transID);
					if (!ret)
					{
						// just log error
						logger.Trace ("Error inserting diff.");
					}
				}
				
				//return back the transactionID
				outXml = "<transactionID>" + transID.ToString() + "</transactionID>";

				return true;
			}
			catch (Exception e)
			{
				logger.Exception(100,e.Message);
				return false;
			}
		}

		private bool CompareConfs(NS_PERSISTENCE.Conference currentConf,NS_PERSISTENCE.Conference newConf,ref Queue diffList)
		{			
			try
			{
				logger.Trace ("Comparing conf data...");
				
        #region compare conf name 
				if (currentConf.sConfName != newConf.sConfName)
				{
					NS_PERSISTENCE.DiffChanges diff = new NS_PERSISTENCE.DiffChanges();
					diff.confID = currentConf.iConfID;
					diff.instanceID = currentConf.iInstanceID;
					diff.userID = currentConf.iOwnerID;					
					diff.message = "Name changed from ''" + currentConf.sConfName + "'' to ''" + newConf.sConfName + "''";
					diffList.Enqueue(diff);
				}
        #endregion

        #region compare the description 
				if (currentConf.sDescription.Trim() != newConf.sDescription.Trim())
				{
					NS_PERSISTENCE.DiffChanges diff = new NS_PERSISTENCE.DiffChanges();
					diff.confID = currentConf.iConfID;
					diff.instanceID = currentConf.iInstanceID;
					diff.userID = currentConf.iOwnerID;
					diff.message = "Description changed from ''" + currentConf.sDescription + "'' to ''" + newConf.sDescription + "''";
					diffList.Enqueue(diff);
				}
        #endregion

        #region compare the pwd 
				if (currentConf.sPassword.Trim() != newConf.sPassword.Trim())
				{
					NS_PERSISTENCE.DiffChanges diff = new NS_PERSISTENCE.DiffChanges();
					diff.confID = currentConf.iConfID;
					diff.instanceID = currentConf.iInstanceID;
					diff.userID = currentConf.iOwnerID;					
					diff.message = "Password has been modified.";
					diffList.Enqueue(diff);
				}
        #endregion
				
        #region compare the conf type 
				if (currentConf.iConfType != newConf.iConfType)
				{
					NS_PERSISTENCE.DiffChanges diff = new NS_PERSISTENCE.DiffChanges();
					diff.confID = currentConf.iConfID;
					diff.instanceID = currentConf.iInstanceID;
					diff.userID = currentConf.iOwnerID;					
					diff.message = "Conference type has been changed.";
					diffList.Enqueue(diff);
				}
        #endregion

        #region compare the public 
				if (currentConf.isPublic != newConf.isPublic)
				{
					NS_PERSISTENCE.DiffChanges diff = new NS_PERSISTENCE.DiffChanges();
					diff.confID = currentConf.iConfID;
					diff.instanceID = currentConf.iInstanceID;
					diff.userID = currentConf.iOwnerID;	
					if (newConf.isPublic)
					{
						diff.message = "Conference marked as Public.";
					}
					else
					{
						diff.message = "Conference marked as Private.";
					}
					diffList.Enqueue(diff);
				}
        #endregion

        #region compare start date 			
				bool hasDateChanged = false;
				if (currentConf.dtStartDateTime.Year == newConf.dtStartDateTime.Year)
				{
					if (currentConf.dtStartDateTime.Month == newConf.dtStartDateTime.Month)
					{
						if (currentConf.dtStartDateTime.Day == newConf.dtStartDateTime.Day)
						{
							if (currentConf.dtStartDateTime.Hour == newConf.dtStartDateTime.Hour)
							{
								if (currentConf.dtStartDateTime.Minute != newConf.dtStartDateTime.Minute)
								{
									hasDateChanged = true;
								}
							}
							else
							{
								hasDateChanged = true;
							}
						}
						else
						{
							hasDateChanged = true;
						}
					}
					else
					{
						hasDateChanged = true;
					}
				}
				else
				{
					hasDateChanged = true;
				}

				if (hasDateChanged)
				{
					NS_PERSISTENCE.DiffChanges diff = new NS_PERSISTENCE.DiffChanges();
					diff.confID = currentConf.iConfID;
					diff.instanceID = currentConf.iInstanceID;
					diff.userID = currentConf.iOwnerID;					
					diff.message = "Date/Time changed from ''" + currentConf.dtStartDateTime.ToString() + "'' to ''" + newConf.dtStartDateTime.ToString() + "''";
					diffList.Enqueue(diff);
				}
        #endregion

        #region compare duration
				if (currentConf.iDurationMin != newConf.iDurationMin)
				{
					NS_PERSISTENCE.DiffChanges diff = new NS_PERSISTENCE.DiffChanges();
					diff.confID = currentConf.iConfID;
					diff.instanceID = currentConf.iInstanceID;
					diff.userID = currentConf.iOwnerID;					
					diff.message = "Duration changed from ''" + currentConf.iDurationMin.ToString() + " mins'' to ''" + newConf.iDurationMin.ToString() + " mins''";
					diffList.Enqueue(diff);
				}
        #endregion

        #region compare timezone
				if (currentConf.iTimezoneID != newConf.iTimezoneID)
				{
					NS_PERSISTENCE.DiffChanges diff = new NS_PERSISTENCE.DiffChanges();
					diff.confID = currentConf.iConfID;
					diff.instanceID = currentConf.iInstanceID;
					diff.userID = currentConf.iOwnerID;					
					diff.message = "Timezone changed.";
					diffList.Enqueue(diff);
				}
        #endregion
								
        #region compare recurring
				if (currentConf.isRecurring != newConf.isRecurring)
				{
					NS_PERSISTENCE.DiffChanges diff = new NS_PERSISTENCE.DiffChanges();
					diff.confID = currentConf.iConfID;
					diff.instanceID = currentConf.iInstanceID;
					diff.userID = currentConf.iOwnerID;	
					if (newConf.isRecurring)				
					{
						diff.message = "Recurring enabled.";
					}
					else
					{
						diff.message = "Recurring disabled.";
					}
					diffList.Enqueue(diff);
				}
        #endregion

        #region if recurring, compare change in recurring pattern
				if (currentConf.isRecurring  && newConf.isRecurring)
				{
					logger.Trace ("Conf is still recurring.");
					if (currentConf.iRecur_RecurType != newConf.iRecur_RecurType)
					{
						NS_PERSISTENCE.DiffChanges diff = new NS_PERSISTENCE.DiffChanges();
						diff.confID = currentConf.iConfID;
						diff.instanceID = currentConf.iInstanceID;
						diff.userID = currentConf.iOwnerID;	
						switch (newConf.iRecur_RecurType)
						{
							case 1: 
							{
								diff.message = "Recurring pattern changed to ''Daily''.";								
								break;
							}
							case 2: 
							{
								diff.message = "Recurring pattern changed to ''Weekly''.";
								break;
							}
							case 3: 
							{
								diff.message = "Recurring pattern changed to ''Monthly''.";
								break;
							}
							case 4: 
							{
								diff.message = "Recurring pattern changed to ''Yearly''.";
								break;
							}
							default:
							{
								diff.message = "Recurring pattern changed to ''Custom''.";
								break;
							}
						}					
						diffList.Enqueue(diff);
					}					
				}
        #endregion
				
        #region TODO : if recurring, compare recurring pattern details
        #endregion
				
				NS_DATABASE.Database db = new NS_DATABASE.Database(this.configParams);
				
        #region compare the rooms
				bool isRoomListDifferent = false;

				// get the current rooms in a list 
				/*				NS_PERSISTENCE.Room[] currentRoomList = new NS_PERSISTENCE.Room[500]; // 500= magic number
								int currentCount = 0;
								while (currentConf.roomList.Count > 0)	
								{
									currentRoomList[currentCount] = (NS_PERSISTENCE.Room) currentConf.roomList.Dequeue();
									bool ret = db.FetchRoomName(currentRoomList[currentCount].roomID,ref currentRoomList[currentCount].name);	
									logger.Trace ("Room ID = " + currentRoomList[currentCount].roomID.ToString());
									currentCount++;
								}
                
								currentRoomList = (NS_PERSISTENCE.Room[])currentConf.roomList.ToArray();
								for (int i = 0 ; i < 500; i++)
								{
									logger.Trace ("Room ID = " + currentRoomList[i].roomID.ToString());
								}
				*/
				// backup of both current and new room lists
				Queue currentConfBackup = new Queue(currentConf.roomList);
				Queue newConfBackup = new Queue(newConf.roomList);
				
				// compare the room lists
				if (newConf.roomList.Count == currentConf.roomList.Count)
				{
					// dive in for addtl checks
					//cycle thru new rooms 
					while (newConf.roomList.Count > 0)	
					{
						NS_PERSISTENCE.Room newRoom = new NS_PERSISTENCE.Room();
						newRoom = (NS_PERSISTENCE.Room) newConf.roomList.Dequeue();
						logger.Trace( "New Room ID = " + newRoom.roomID.ToString());
						Queue currentConfRoomList = new Queue(currentConf.roomList);

						// down and dirty comparison
						bool bRoomAlreadyExists = false;
						while (currentConfRoomList.Count > 0)
						{
							NS_PERSISTENCE.Room cur = new NS_PERSISTENCE.Room();
							cur = (NS_PERSISTENCE.Room) currentConfRoomList.Dequeue();
							logger.Trace( "Current Room ID = " + cur.roomID.ToString());

							if (newRoom.roomID == cur.roomID)
							{
								bRoomAlreadyExists = true;
								break;
							}
						}
						
						if (!bRoomAlreadyExists)
						{
							// new room
							isRoomListDifferent = true;
							break;
						}
					}
				}
				else
				{
					isRoomListDifferent = true;
				}
				
				if(isRoomListDifferent)
				{
					// rooms have been added/modified
					char[] ch = {','};
					string currentRoomNames = "";
					while (currentConfBackup.Count>0)
					{
						NS_PERSISTENCE.Room room = (NS_PERSISTENCE.Room) currentConfBackup.Dequeue();
						currentRoomNames += room.name + ",";
					}
					currentRoomNames = currentRoomNames.TrimEnd(ch);

					string newRoomNames = "";
					while (newConfBackup.Count>0)
					{
						NS_PERSISTENCE.Room room = (NS_PERSISTENCE.Room) newConfBackup.Dequeue();
						newRoomNames += room.name + ",";
					}
					newRoomNames = newRoomNames.TrimEnd(ch);

					NS_PERSISTENCE.DiffChanges diff = new NS_PERSISTENCE.DiffChanges();					
					diff.confID = currentConf.iConfID;
					diff.instanceID = currentConf.iInstanceID;
					diff.userID = currentConf.iOwnerID;					
					diff.message = "Rooms changed from ''" + currentRoomNames + "'' to ''" + newRoomNames + "''";
					diffList.Enqueue(diff);					
				}				
        #endregion

        #region TODO : compare the participants
        #endregion

				return true;
			}
			catch (Exception e)
			{
				logger.Exception (100,e.Message);
				return false;
			}
		}

		internal bool SetConference_CommitDiffs(string inXml,ref string outXml)
		{
			try
			{

				logger.Trace ("Entering the SetConference_CommitDiffs method...");				
				
				// Load the string into xml parser and retrieve the tag values.
				XmlDocument xd = new XmlDocument();
				xd.LoadXml(inXml);
						
				// parse the inxml and store the conf info in a persistence object
				// Instantiate the conf object
				NS_PERSISTENCE.Conference newConf = new NS_PERSISTENCE.Conference();
				
				// userID
				newConf.iOwnerID = Int32.Parse(xd.SelectSingleNode("//committDiffs/userID").InnerXml.Trim());
				
				// confID, instanceID
				string strConfId = xd.SelectSingleNode("//committDiffs/confInfo/confID").InnerXml.Trim();
				bool ret = false;
				if (strConfId.IndexOf(",") > 0)
				{
					// Just in case ASP sends the confid in this format - 10,1 (confid,instanceid)
					int confId = 0 ,instanceId = 0 ;
					ret = ExtractConfInstanceIds(strConfId,ref confId,ref instanceId);
					if (!ret)
					{
						logger.Trace ("Invalid conf/instance id.");
						return false;
					}
					newConf.iConfID = confId;
					newConf.iInstanceID = instanceId;
				}
				else
				{
					if(strConfId == "new")
						return false; // no updates reqd
					else
					{
						newConf.isRecurring = true;
						newConf.iConfID = Int32.Parse(strConfId);
						newConf.iInstanceID =1;
					}
				}
				logger.Trace ("Confid = " + newConf.iConfID.ToString() + ", InstanceID = " + newConf.iInstanceID.ToString());

				// get the transactionID
				string transID = xd.SelectSingleNode("//committDiffs/confInfo/transactionID").InnerXml.Trim();
				logger.Trace ("TransactionID = " + transID);
				NS_DATABASE.Database db = new NS_DATABASE.Database(configParams);

				/*				int confInstanceCount = 1;
								// if recurring, fetch all the instance ids.
								if (newConf.isRecurring)
								{
									// fetch all instanceids
									ret = false;
									ret = db.FetchConfInstances(ref confInstanceCount);
									if (!ret)
									{
										logger.Trace ("Error fetching instance count.");
										return false;							
									}				
				
									// cycle thru each instance id 				
									for (int i =0;i < confInstanceCount;i++)
									{
										if (transID == "new")
										{
											// Check to see if its a new conference
											// If yes, then record this with an entry in the diff table. 
											NS_PERSISTENCE.DiffChanges diff = new NS_PERSISTENCE.DiffChanges();
											diff.confID = newConf.iConfID;
											diff.instanceID = newConf.iInstanceID;
											diff.userID = newConf.iOwnerID;					
											diff.message = "Initial conference setup.";
											transID = "0"; // first transaction
											ret = db.InsertConfDiffRecord(diff,Int32.Parse(transID));
											if (!ret)
											{
												// just log error
												logger.Trace ("Error inserting diff.");
											}
										}
				
										// commit the diffs 								
										ret = db.CommitDiffs(newConf.iConfID,newConf.iInstanceID,Int32.Parse(transID));
										if (!ret)
										{
											logger.Trace ("Error committing diffs.");
											return false;
										}
									}
								}
								else
								{
				*/					if (transID == "new")
									{
										// Check to see if its a new conference
										// If yes, then record this with an entry in the diff table. 
										NS_PERSISTENCE.DiffChanges diff = new NS_PERSISTENCE.DiffChanges();
										diff.confID = newConf.iConfID;
										diff.instanceID = newConf.iInstanceID;
										diff.userID = newConf.iOwnerID;					
										diff.message = "Initial conference setup.";
										transID = "0"; // first transaction
										ret = db.InsertConfDiffRecord(diff,Int32.Parse(transID));
										if (!ret)
										{
											// just log error
											logger.Trace ("Error inserting diff.");
										}
									}
				
				// commit the diffs 								
				ret = db.CommitDiffs(newConf.iConfID,newConf.iInstanceID,Int32.Parse(transID));
				if (!ret)
				{
					logger.Trace ("Error committing diffs.");
					return false;
				}
				//				}
				outXml = "<success>1</success>";
				return true;
			}
			catch (Exception e)
			{
				logger.Exception(100,e.Message);
				return false;
			}
		}



		public bool SearchConfsByHistory(string inXml,ref string outXml)
		{
			try
			{
				logger.Trace ("Entering the SearchConfsByHistory function...");				
				
				// Load the string into xml parser and retrieve the tag values.
				XmlDocument xd = new XmlDocument();
				xd.LoadXml(inXml);
							
				XmlNode node;

				// confid , instanceid
				node = xd.SelectSingleNode("//login/confSearch/confDate/type");
				int dateSearchParam = Int32.Parse(node.InnerXml.Trim());
				DateTime from,to;
				if (dateSearchParam == 5)
				{
					from = DateTime.Parse(xd.SelectSingleNode("//login/confSearch/confDate/type/from").InnerXml.Trim());
					to = DateTime.Parse(xd.SelectSingleNode("//login/confSearch/confDate/type/to").InnerXml.Trim());
				}
				

				// search for confs using the above params
				NS_DATABASE.Database db = new NS_DATABASE.Database(configParams);
				bool ret = false; Queue confList = new Queue();
				ret = false;//db.SearchConfsByHistory(ref confList);
				if (!ret)
				{
					logger.Trace("Error retreiving diff list.");
					return false;
				}

				// cycle thru the confs
				while (confList.Count > 0)
				{
					NS_PERSISTENCE.Conference conf = new NS_PERSISTENCE.Conference();
					//conf = (NS_PERSISTENCE.Conference) conf
				}

				
				return true;
			}
			catch (Exception e)
			{
				logger.Exception(100,e.Message);
				return false;
			}
		}


		internal bool FetchConfHistory(string inXml,ref string outXml)
		{
			try
			{
				logger.Trace ("Entering the FetchConfHistory function...");				
				
				// Load the string into xml parser and retrieve the tag values.
				XmlDocument xd = new XmlDocument();
				xd.LoadXml(inXml);
						
				//userid 
				int userID = Int32.Parse(xd.SelectSingleNode("//login/userID").InnerXml.Trim());

				// confid , instanceid
				string strConfId = xd.SelectSingleNode("//login/confid").InnerXml.Trim();
				int confId = 0 ,instanceId = 0 ;
				bool ret = ExtractConfInstanceIds(strConfId,ref confId,ref instanceId);
				if (!ret)
				{
					logger.Trace ("Invalid conf/instance id.");
					return false;
				}

				// fetch conf history
				NS_DATABASE.Database db = new NS_DATABASE.Database(configParams);
				ret = false; Queue diffList = new Queue();
				ret = db.FetchConfHistory(confId,instanceId,ref diffList);
				if (!ret)
				{
					logger.Trace("Error retreiving diff list.");
					return false;
				}

				// create the outxml
				outXml = "<ConferenceHistory>";
				while (diffList.Count >0)
				{
					NS_PERSISTENCE.DiffChanges diff = new NS_PERSISTENCE.DiffChanges();
					diff = (NS_PERSISTENCE.DiffChanges) diffList.Dequeue();

					ret = db.ChangeGMTTimeToUserPreferredTimeZone(userID,ref diff.timestamp);
					string tz = "GMT";
					ret = db.FetchUserTimeZone(userID,ref tz);
					outXml += "<item>";
					outXml += "<dateModified>" + diff.timestamp.ToString() + "</dateModified>";
					outXml += "<timeZone>"+ tz +"</timeZone>";  
					outXml += "<description>" + diff.message + "</description>";
					outXml += "<userEmail>" + diff.userEmail + "</userEmail>";
					outXml += "<userName>" + diff.userName + "</userName>";
					outXml += "</item>";
				}
				outXml += "</ConferenceHistory>";
				return true;
			}
			catch (Exception e)
			{
				logger.Exception(100,e.Message);
				return false;
			}
		}
		
		internal bool ReportsModule(string reportCmd,string inXml,ref string outXml)
		{
			// Report module caller. Just for v18. 		

			try
			{			
				VRMReports.Reports reports = new VRMReports.Reports();
				outXml = reports.Operations(this.configParams.reportsModuleConfigPath,reportCmd,inXml);
				return true;
			}			
			catch (Exception e)
			{
				logger.Exception(100,e.Message);
				return false;
			}		

#endif
    }


/// <summary>
/// All conference setup operations on the bridges.
/// </summary>
		
class ConfSetup
{
	/// <summary>
	/// Public class properties
	/// </summary>
	internal string errMsg = null;
	
	/// <summary>
	/// Private class properties
	/// </summary>
	private NS_LOGGER.Log logger;
	//private NS_POLYCOM.Polycom accord;
	private NS_MESSENGER.ConfigParams configParams;
    internal bool iRecurring = false; //FB 2441
	
	/// <summary>
	/// Constructor
	/// </summary>
	/// <param name="config"></param>
	internal ConfSetup(NS_MESSENGER.ConfigParams config)
	{
		// constructor 
		configParams = new NS_MESSENGER.ConfigParams();
		configParams = config;
		logger = new NS_LOGGER.Log(configParams);
		//accord = new NS_POLYCOM.Polycom(configParams);
	}
	
	/// <summary>
	/// Checks to see if the conference is cascaded or not. If yes, conf is broken into multiple confs with each associated to a bridge.
	/// </summary>
	/// <param name="conf"></param>
	/// <param name="confq"></param>
	/// <param name="cascadeLinks"></param>
	/// <returns></returns>
	private bool CascadeLogic (NS_MESSENGER.Conference conf , ref Queue confq,Queue cascadeLinkList)
	{
        List<NS_MESSENGER.MCU.eType> mcuType = new List<NS_MESSENGER.MCU.eType>();// ZD 100768
        try
		{
			NS_DATABASE.Database db = new NS_DATABASE.Database(configParams);

			logger.Trace("Before cascade array initalization");

			// use this array to map bridge id values.
			// magic number = 100
			bool[] mcuIdList = new bool[100];
			for (int i = 1 ; i <= 99 ; i++)
			{
				//array initialization
				mcuIdList[i] = false;						
			}

			logger.Trace("After cascade array initalization");

            
			// cycle thru each of the cascade links and find the unique mcu's 
            Queue qCascadeLinkListBackup = new Queue(cascadeLinkList);
            while (qCascadeLinkListBackup.Count > 0)
			{
				NS_MESSENGER.Party cascadeLink = new NS_MESSENGER.Party();
                cascadeLink = (NS_MESSENGER.Party)qCascadeLinkListBackup.Dequeue();
				if (mcuIdList[cascadeLink.cMcu.iDbId] == false)
				{
					// new bridge found and added to the list
					mcuIdList[cascadeLink.cMcu.iDbId]= true;	
					logger.Trace("Cascade Link bridge id  = " + cascadeLink.iDbId.ToString());
					
                    // fetch the mcu info
					NS_MESSENGER.MCU mcu = new NS_MESSENGER.MCU();
					bool ret = db.FetchMcu(cascadeLink.cMcu.iDbId,ref mcu);
					if (!ret) continue;

                    ret = db.FetchBridgeDetails(cascadeLink.cMcu.iDbId, ref conf); //FB 2636
                    if (!ret) continue;

                    if (!mcuType.Contains(mcu.etType))//ZD 100768
                        mcuType.Add(mcu.etType);

					// copy the conf & mcu info 
					NS_MESSENGER.Conference partialConf = new NS_MESSENGER.Conference();					
					conf.CopyTo(ref partialConf);
					partialConf.cMcu.iDbId = mcu.iDbId;	
					partialConf.cMcu.sIp = mcu.sIp;	
					partialConf.cMcu.sLogin = mcu.sLogin;	
					partialConf.cMcu.sName = mcu.sName;	
					partialConf.cMcu.sPwd = mcu.sPwd;	
					partialConf.cMcu.sStation = mcu.sStation;	
					partialConf.cMcu.etType = mcu.etType;
                    partialConf.cMcu.iHttpPort = mcu.iHttpPort;
                    partialConf.cMcu.iTimezoneID = mcu.iTimezoneID;
                    partialConf.cMcu.iOrgId = mcu.iOrgId;//ZD 104021
                    //FB 1642 - DTMF Commented for FB 2655
                    //partialConf.cMcu.postLPin = mcu.postLPin;
                    //partialConf.cMcu.preLPin = mcu.preLPin;
                    //partialConf.cMcu.preConfCode = mcu.preConfCode;
                    partialConf.cMcu.iEnableIVR = mcu.iEnableIVR;    //FB 1766 - IVR Settings
                    partialConf.cMcu.sIVRServiceName = mcu.sIVRServiceName;  //FB 1766 - IVR Settings
                    partialConf.cMcu.iEnableRecording = mcu.iEnableRecording; //FB 1907
                    partialConf.cMcu.iLPR = mcu.iLPR; //FB 1907
                    //FB 2003 start
                    partialConf.cMcu.iISDNAudioPrefix = mcu.iISDNAudioPrefix;
                    partialConf.cMcu.iISDNVideoPrefix = mcu.iISDNVideoPrefix;
                    partialConf.cMcu.sisdnGateway = mcu.sisdnGateway;
                    //FB 2003 end
                    //FB 2839 Start
                    if(mcu.etType == NS_MESSENGER.MCU.eType.RMX)
                        partialConf.cMcu.ConfServiceID = conf.iConfRMXServiceID; 
                    else
                        partialConf.cMcu.ConfServiceID = mcu.ConfServiceID; //FB 2016
                    //FB 2839 End
					// add the conf to the confq
                    partialConf.cMcu.sBridgeExtNo = mcu.sBridgeExtNo;// ZD 100768
					confq.Enqueue(partialConf);
				}													
			}

            // add the participants and cascade links to the cascaded confs (partialconf)
            Queue qConfListBackup = new Queue(confq); //backup conf            
            Queue qConfListWithParticipants = new Queue(); //new
            while (qConfListBackup.Count > 0)
            {
                // get the conf
                NS_MESSENGER.Conference fullConf = new NS_MESSENGER.Conference();
                fullConf = (NS_MESSENGER.Conference)qConfListBackup.Dequeue();

                // add the corresponding participants                   
                Queue qPartyListBackup = new Queue(conf.qParties);                   
                while (qPartyListBackup.Count > 0)
                {
                    // party 
                    NS_MESSENGER.Party party = new NS_MESSENGER.Party();
                    party = (NS_MESSENGER.Party)qPartyListBackup.Dequeue();

                    // compare the bridge id's
                    if (fullConf.cMcu.iDbId == party.cMcu.iDbId)
                    {
                        // mcu id's match. so add this party to this "partialconf"
                        fullConf.qParties.Enqueue(party);                   
                    }
                }
                // add the corresponding participants                   
                Queue qCascadeLinkListBackup2 = new Queue(cascadeLinkList);
                while (qCascadeLinkListBackup2.Count > 0)
                {
                    // party 
                    NS_MESSENGER.Party cascadeLinkParty = new NS_MESSENGER.Party();
                    cascadeLinkParty = (NS_MESSENGER.Party)qCascadeLinkListBackup2.Dequeue();

                    // compare the bridge id's
                    if (fullConf.cMcu.iDbId == cascadeLinkParty.cMcu.iDbId)
                    {
                        if (cascadeLinkParty.etConnType == NS_MESSENGER.Party.eConnectionType.DIAL_OUT && mcuType.Count == 1) //ZD 100768
                            fullConf.bIsCasecadeofsametype = true;
                        // mcu id's match. so add this party to this "partialconf"
                        fullConf.qParties.Enqueue(cascadeLinkParty);
                    }
                }
            }
			return true;
		}
		catch (Exception e)
		{
			logger.Exception(100,e.Message);
			return false;
		}
	}

	/// <summary>
	/// Checks to see if a conf is non cascaded i.e. to be setup on a single bridge.
	/// </summary>
	/// <param name="conf"></param>
	/// <param name="confq"></param>
	/// <param name="partyq"></param>
	/// <returns></returns>
	private bool NonCascadeLogic (ref NS_MESSENGER.Conference conf, ref Queue confq,Queue partyq)
	{
		try
		{
			// get the bridge id of the first party in the queue
			// assumption is that all other parties will have the same bridgeid.
			// if not, then data in db is inconsistent.
			Queue tmpPartyq = new Queue(partyq);
            NS_DATABASE.Database db = new NS_DATABASE.Database(configParams);//FB 2659
            NS_MESSENGER.MCU mcu = new NS_MESSENGER.MCU();
            int emptyConfPush = 0; // ZD 104151
			if (tmpPartyq.Count>0)
			{
				
				NS_MESSENGER.Party firstParty = new NS_MESSENGER.Party();
				firstParty =(NS_MESSENGER.Party) tmpPartyq.Dequeue();				
				logger.Trace("Party : " + firstParty.sName + "," + firstParty.sAddress);
				bool ret = db.FetchMcu(firstParty.cMcu.iDbId,ref mcu);
				if (!ret) 
				{
					logger.Exception (100,"Error in fetching mcu info from db.");
					return false;
				}
				

                db.SetDefaultLO(conf.iOrgID, ref mcu, partyq);//FB 2335

                conf.cMcu = mcu;
                confq.Enqueue(conf);

				return true;
			}
			else
				// no participants in conf or error.
			{
				this.errMsg = "Incomplete conference data. No participant or room in the conference.";
				logger.Trace("No participant or room in the conference.");
                //FB 2659 - Starts
                int EnableCloudInstallation = 0;
                bool ret = false;
                db.FetchSysSettingDetails(ref EnableCloudInstallation);

                if (EnableCloudInstallation == 1)
                {
                    ret = db.FetchCloudMcu(0, ref mcu, conf.iOrgID);
                    if (!ret)
                    {
                        logger.Exception(100, "Error in fetching mcu info from db.");
                        return false;
                    }
                    conf.cMcu = mcu;
                    confq.Enqueue(conf);
                    return true;
                }
                // ZD 104151 Start
                else 
                {
                    db.FetchEmptyConfOrgDetails(ref emptyConfPush, ref conf.iOrgID);
                    if (emptyConfPush > 0)
                    {
                        ret = db.FetchMcu(emptyConfPush, ref mcu);
                        if (!ret)
                        {
                            logger.Exception(100, "Error in fetching mcu info from db.");
                            return false;
                        }
                        conf.cMcu = mcu;
                        confq.Enqueue(conf);
                        return true;
 
                    }
 
                }
                // ZD 104151 End
                //FB 2659 - End
                return false;	
			}
		}
		catch (Exception e)
		{
			logger.Exception(100,e.Message);
			return false;
		}
	}

	/// <summary>
	/// Scheduled conferences are setup on the bridge
	/// </summary>
	/// <param name="conf"></param>
	/// <returns></returns>
	private bool SetupOnMcu (ref NS_MESSENGER.Conference conf)
	{
        string outxml = "";//ZD 100522
		try
		{
			NS_DATABASE.Database db = new NS_DATABASE.Database(configParams);

			Queue partyq = new Queue();

			#region Fetch the conf info
			bool ret = db.FetchConf(ref conf);
			if (!ret) 
			{
				this.errMsg = db.errMsg;
						
				try
				{
					NS_MESSENGER.Alert alert = new NS_MESSENGER.Alert();
					alert.confID = conf.iDbID;
					alert.instanceID = conf.iInstanceID;
					alert.message = this.errMsg;
					alert.typeID = 1; // conf setup failed.
					ret = false;
					ret = db.InsertConfAlert(alert);						
					if (!ret) logger.Trace("Conference alert insert failed.");
				}
				catch (Exception e)
				{
					logger.Exception(100,e.Message);
				}

				logger.Exception (100,"Error in fetching conf info from db.");
				return false;
			}
			#endregion

			#region Fetch the endpoints
			ret = false;
			ret = db.FetchRooms(conf.iDbID,conf.iInstanceID,ref partyq,0);
			if (!ret) 
			{
				logger.Exception (100,"Error in fetching rooms from db.");
				return false;
			}
			ret = false;
			ret = db.FetchUsers(conf.iDbID,conf.iInstanceID,ref partyq,0);
			if (!ret) 
			{
				logger.Exception (100,"Error in fetching users from db.");
				return false;
			}
			ret = false;
			ret = db.FetchGuests(conf.iDbID,conf.iInstanceID,ref partyq,0);
			if (!ret) 
			{
				logger.Exception (100,"Error in fetching guests from db.");
				return false;
			}
			ret = false;
			Queue cascadeLinks = new Queue();
			ret = db.FetchCascadeLinks(conf.iDbID,conf.iInstanceID,ref cascadeLinks,0);
			if (!ret) 
			{
				logger.Exception (100,"Error in fetching cascade links from db.");
				return false;
			}			
			#endregion

			#region Cascade Check
			// backup of cascade links 
			Queue cascadeLinksBackup = new Queue(cascadeLinks);
					
			// check if conf is cascaded
			Queue confq = new Queue();						
			if (cascadeLinks.Count > 0) 
			{			
				logger.Trace("In cascade");	
				bool ret1 = CascadeLogic(conf,ref confq,cascadeLinks);
				if (!ret1)
				{
					logger.Exception (100,"Error in fetching the bridge for a cascade conf. ConfID = "+ conf.iDbID.ToString() + ",InstanceID = " + conf.iInstanceID.ToString());
					return false;
				}
			}
			else
			{	
				logger.Trace("In non-cascade ");
				bool ret1 = NonCascadeLogic(ref conf,ref confq,partyq);
				if (!ret1)
				{
					logger.Exception (100,"Error in fetching the bridge for a Non-Cascade conf. ConfID = "+ conf.iDbID.ToString() + ",InstanceID = " + conf.iInstanceID.ToString());
					return false;
				}
			}
			#endregion

			#region Setup only the confs on the bridge without any participants			
			while (confq.Count >0)
			{
				NS_MESSENGER.Conference partialConf = new NS_MESSENGER.Conference();
				partialConf = (NS_MESSENGER.Conference)confq.Dequeue();
					
				bool ret2 = false;
					
				// Polycom Accord Bridge 
                if (partialConf.cMcu.etType == NS_MESSENGER.MCU.eType.ACCORDv6 || partialConf.cMcu.etType == NS_MESSENGER.MCU.eType.ACCORDv7 || partialConf.cMcu.etType == NS_MESSENGER.MCU.eType.RMX)
                {
                    // VA_SPECIFIC_SWITCH
                    if (this.configParams.client.ToUpper() == "VAOHIO")
                    {
                        // THIS SWITCH IS ONLY FOR VA OHIO.
                        // switch conf.m_videosession to videoswitching for Chillicothe,Cincinnati,VISN10 bridge
                        string bridgeName = partialConf.cMcu.sName.Trim();
                        if (bridgeName.StartsWith("VISN")) //bridgeName.StartsWith("Cincin") ||bridgeName.StartsWith("Chilli")
                        {
                            partialConf.etVideoSession = NS_MESSENGER.Conference.eVideoSession.SWITCHING;
                        }
                    }

                    // Adjusting the conf time (in GMT) to mcu time (local time)
                    //db.ConvertGmtToLocalTime(partialConf.cMcu.m_timezoneId,ref partialConf.m_startDateTime);							
                    NS_POLYCOM.Polycom accord = new NS_POLYCOM.Polycom(configParams);
                    ret2 = accord.SetConference(partialConf, ref outxml); //ZD 100522
                    this.errMsg = accord.errMsg;
                }
                else
                {
                    // Codian Bridge.
                    if (partialConf.cMcu.etType == NS_MESSENGER.MCU.eType.CODIAN)
                    {
                        NS_CODIAN.Codian codian = new NS_CODIAN.Codian(configParams);
                        logger.Trace("Entering codian sub-system...");
                        ret2 = codian.SetConference(partialConf);
                        this.errMsg = codian.errMsg;
                    }
                    else
                    {
                        if (partialConf.cMcu.etType == NS_MESSENGER.MCU.eType.TANDBERG)
                        {
                            // Tandberg Bridge
                            NS_TANDBERG.Tandberg tandberg = new NS_TANDBERG.Tandberg(configParams);
                            logger.Trace("Entering tandberg sub-system...");
                            ret2 = tandberg.SetConference(partialConf);
                            this.errMsg = tandberg.errMsg;
                        }
                        //FB 2501 Call Monitoring Start
                        else if (partialConf.cMcu.etType == NS_MESSENGER.MCU.eType.CISCOTP)
                        {
                            // CISCO MSE 8710 bridge
                            NS_CiscoTPServer.CiscoTPServer ciscoTP = new NS_CiscoTPServer.CiscoTPServer(configParams);
                            NS_MESSENGER.Party party = new NS_MESSENGER.Party();
                            ret2 = ciscoTP.SetConference(ref partialConf, ref party);
                            if (!ret2)
                                this.errMsg = ciscoTP.errMsg;
                            //else
                            //    db.UpdateConference(conf.iDbID, conf.iInstanceID, conf.sGUID, party);
                        }
                        //FB 2501 Call Monitoring End
                        else if (partialConf.cMcu.etType == NS_MESSENGER.MCU.eType.RADVISION) //FB 2556
                        {
                            // Radvision Bridge
                            NS_RADVISION.Radvision radvision= new NS_RADVISION.Radvision(configParams);
                            logger.Trace("Entering Radvision sub-system...");
                            ret2 = radvision.SetConference(partialConf);
                            this.errMsg = radvision.errMsg;
                        }
                        //FB 2556 Starts
                        else if (partialConf.cMcu.etType == NS_MESSENGER.MCU.eType.RADVISIONIVIEW)
                        {
                            // Radvision Iview Bridge
                            NS_RADVISION_IView.RadVisionIView RadIview = new NS_RADVISION_IView.RadVisionIView(configParams);
                            logger.Trace("Entering Radvision IView sub-system...");
                            ret2 = RadIview.SetConference(partialConf);
                            this.errMsg = RadIview.errMsg;
                        }
                        //FB 2556 Ends
                    }
                }
				if (ret2) 
				{	
					// conf setup was successful.
					// update the conf.status flag to "Ongoing"
                    conf.etStatus = NS_MESSENGER.Conference.eStatus.ONGOING;
					db.UpdateConferenceStatus(conf.iDbID,conf.iInstanceID,conf.etStatus);							
				}	
				else
				{
					try
					{
						NS_MESSENGER.Alert alert = new NS_MESSENGER.Alert();
						alert.confID = partialConf.iDbID;
						alert.instanceID = partialConf.iInstanceID;
						alert.message = this.errMsg;
						alert.typeID = 1; 
						ret = false;
						ret = db.InsertConfAlert(alert);						
						if (!ret) logger.Trace("Conference alert insert failed.");
					}
					catch (Exception e)
					{
						logger.Exception(100,e.Message);
					}

					logger.Exception (100,"Conference setup failed on the bridge. Confid = " + partialConf.iDbID.ToString() + ",InstanceId = " + partialConf.iInstanceID.ToString());							
				}
			}
			#endregion

			#region Add the participants to the bridge's one-by-one
			while (partyq.Count >0)
			{
				// fetch the party from the queue
				NS_MESSENGER.Party party = new NS_MESSENGER.Party();
				party = (NS_MESSENGER.Party) partyq.Dequeue();

				//audio or video party
				//if (conf.m_type == NS_MESSENGER.Conference.eType.AUDIO)
				//{
				//	party.etCallType = NS_MESSENGER.Party.etCallType.AUDIO;
				//}
				//else
				//{
				//	party.etCallType = NS_MESSENGER.Party.etCallType.VIDEO;
				//}					

				// add the party to mcu
				ret = false;
                if (party.cMcu.etType == NS_MESSENGER.MCU.eType.ACCORDv6 || party.cMcu.etType == NS_MESSENGER.MCU.eType.ACCORDv7 || party.cMcu.etType == NS_MESSENGER.MCU.eType.RMX)
                {
                    NS_POLYCOM.Polycom accord = new NS_POLYCOM.Polycom(configParams);
                    ret = accord.AddPartyToMcu(conf,party);
                    if (!ret) this.errMsg = accord.errMsg;
                }
                else
                {
                    if (party.cMcu.etType == NS_MESSENGER.MCU.eType.CODIAN)
                    {
                        NS_CODIAN.Codian codian = new NS_CODIAN.Codian(configParams);
                        ret = codian.AddPartyToMcu(conf,party);
                        if (!ret) this.errMsg = codian.errMsg;
                    }
                    //FB 2501 Call Monitoring Start
                    else if (party.cMcu.etType == NS_MESSENGER.MCU.eType.CISCOTP)
                    {
                        NS_CiscoTPServer.CiscoTPServer CiscoTPServer = new NS_CiscoTPServer.CiscoTPServer(configParams);
                        ret = CiscoTPServer.AddParticipantToMcu(conf, party);
                        if (!ret) this.errMsg = CiscoTPServer.errMsg;
                    }
                    //FB 2501 Call Monitoring End
                    //FB 2441 Starts
                    else if (party.cMcu.etType == NS_MESSENGER.MCU.eType.RPRM)
                    {
                        NS_POLYCOMRPRM.POLYCOMRPRM PolycomRPRM = new NS_POLYCOMRPRM.POLYCOMRPRM(configParams);
                        ret = PolycomRPRM.AddPartyToMcu(conf, party);
                        if (!ret) this.errMsg = PolycomRPRM.errMsg;
                    }
                    //FB 2441 Ends
                    //FB 2556 Starts
                    else if (party.cMcu.etType == NS_MESSENGER.MCU.eType.RADVISIONIVIEW)
                    {
                        // Radvision Iview Bridge
                        NS_RADVISION_IView.RadVisionIView RadIview = new NS_RADVISION_IView.RadVisionIView(configParams);
                        logger.Trace("Entering Radvision IView sub-system...");
                        ret = RadIview.AddPartyToMcu(conf, party);
                        this.errMsg = RadIview.errMsg;
                    }
                    //FB 2556 Ends
                    else
                    {
                        NS_TANDBERG.Tandberg tandberg = new NS_TANDBERG.Tandberg(configParams);
                        ret = tandberg.AddPartyToMcu(conf,party,0);
                        if (!ret)  this.errMsg = tandberg.errMsg;
                    }
                }
				if (!ret) 
				{
					if (this.errMsg != null)
						logger.Exception(100,"Name = " + party.sName + " ,Status = " + this.errMsg);
							
					logger.Exception(100,"Error adding party . Party = " + party.sName);
					try
					{
						// alert
						NS_MESSENGER.Alert alert = new NS_MESSENGER.Alert();
						alert.confID = conf.iDbID;
						alert.instanceID = conf.iInstanceID;
						alert.message = this.errMsg;
						alert.typeID = 2; //add party
						ret = false;
						ret = db.InsertConfAlert(alert);						
						if (!ret) logger.Trace("Conference alert insert failed.");
					}
					catch (Exception e)
					{
						logger.Exception(100,e.Message);
					}
						
					// switch the party flag to 2 so that it can be added the next time.
					db.UpdateOngoingPartyStatus(party.etType,party.iDbId,conf.iDbID,conf.iInstanceID,2);
					continue;
				}	

			}
			#endregion

			#region Add the cascade links to the bridge's one-by-one
			while (cascadeLinksBackup.Count >0)
			{

				// fetch the cascade link from the queue
				NS_MESSENGER.Party cascadeLink = new NS_MESSENGER.Party();
				cascadeLink = (NS_MESSENGER.Party) cascadeLinksBackup.Dequeue();
				
				//audio or video party
				//if (conf.m_type == NS_MESSENGER.Conference.eType.AUDIO)
				//{
				//	cascadeLink.etCallType = NS_MESSENGER.Party.etCallType.AUDIO;
				//}
				//else
				//{
				//	cascadeLink.etCallType = NS_MESSENGER.Party.etCallType.VIDEO;
				//}
						
				// skip adding SLAVE cascade links
				if (cascadeLink.etCascadeRole == NS_MESSENGER.Party.eCascadeRole.SLAVE)
				{
					// switch the party flag to 2 so that it can be added the next time.
					db.UpdateOngoingPartyStatus(cascadeLink.etType,cascadeLink.iDbId,conf.iDbID,conf.iInstanceID,2);
					continue;
				}

				string errMcuMsg = null;
                if (cascadeLink.cMcu.etType == NS_MESSENGER.MCU.eType.ACCORDv6 || cascadeLink.cMcu.etType == NS_MESSENGER.MCU.eType.ACCORDv7 || cascadeLink.cMcu.etType == NS_MESSENGER.MCU.eType.RMX)
                {
                    // Accord
                    ret = false;
                    NS_POLYCOM.Polycom accord = new NS_POLYCOM.Polycom(configParams);
                    ret = accord.AddPartyToMcu(conf,cascadeLink);
                    if (!ret) this.errMsg = accord.errMsg;
                }
                else
                {
                    if (cascadeLink.cMcu.etType == NS_MESSENGER.MCU.eType.CODIAN)
                    {
                        // Codian
                        ret = false; NS_CODIAN.Codian codian = new NS_CODIAN.Codian(configParams);
                        ret = codian.AddPartyToMcu(conf,cascadeLink);
                        if (!ret) this.errMsg = codian.errMsg;
                    }
                    //FB 2501 Call Monitoring Start
                    else if (cascadeLink.cMcu.etType == NS_MESSENGER.MCU.eType.CISCOTP)
                    {
                        NS_CiscoTPServer.CiscoTPServer CiscoTPServer = new NS_CiscoTPServer.CiscoTPServer(configParams);
                        ret = CiscoTPServer.AddParticipantToMcu(conf, cascadeLink);
                        if (!ret) this.errMsg = CiscoTPServer.errMsg;
                    }
                    //FB 2501 Call Monitoring End
                    //FB 2556 Starts
                    else if (cascadeLink.cMcu.etType == NS_MESSENGER.MCU.eType.RADVISIONIVIEW)
                    {
                        // Radvision Iview Bridge
                        NS_RADVISION_IView.RadVisionIView RadIview = new NS_RADVISION_IView.RadVisionIView(configParams);
                        logger.Trace("Entering Radvision IView sub-system...");
                        ret = RadIview.AddPartyToMcu(conf, cascadeLink);
                        this.errMsg = RadIview.errMsg;
                    }
                    //FB 2556 Ends
                    else 
                    {
                        // Tandberg
                        ret = false; NS_TANDBERG.Tandberg tandberg = new NS_TANDBERG.Tandberg(configParams);
                        ret = tandberg.AddPartyToMcu(conf,cascadeLink,0);
                        if (!ret) this.errMsg = tandberg.errMsg;
                    }
                }
				if (!ret) 
				{
					if (errMcuMsg != null)
						logger.Exception(100,"Name = " + cascadeLink.sName + " ,Status = " + errMcuMsg);

					logger.Exception(100,"Error adding party . Party = " + cascadeLink.sName);
					try
					{
						// alert
						NS_MESSENGER.Alert alert = new NS_MESSENGER.Alert();
						alert.confID = conf.iDbID;
						alert.instanceID = conf.iInstanceID;
						alert.message = this.errMsg;
						alert.typeID = 2; //add party
						ret = false;
						ret = db.InsertConfAlert(alert);						
						if (!ret) logger.Trace("Conference alert insert failed.");
					}
					catch (Exception e)
					{
						logger.Exception (100,e.Message);
					}

					// switch the party flag to 2 so that it can be added the next time.
					db.UpdateOngoingPartyStatus(cascadeLink.etType,cascadeLink.iDbId,conf.iDbID,conf.iInstanceID,2);

					continue;
				}	
			}
			#endregion

			return true;
		}
		catch (Exception e)
		{
			logger.Exception(100,e.Message);
			return false;
		}
	}// end - SetupOnMcu()




	/// <summary>
	/// Controller method to setup all immediate conferences on the bridge.
	/// </summary>
	/// <returns></returns>
	internal bool ConfsSetupOnMcu()
	{
		try 
		{
			// Fetch immediate confs
			NS_DATABASE.Database db = new NS_DATABASE.Database(configParams);
			Queue confq = new Queue();
			logger.Trace ("Fetching Immediate Confs...");
			bool ret = db.FetchImmediateConferences(ref confq,0);
			if (!ret) 
			{					
				logger.Exception(100,"Error fetching confs from db.");
				return false;
			}
			
			// Fetch retry confs
			ret = false;
			logger.Trace ("Fetching Retry Confs...");
			ret = db.FetchRetryConferences(ref confq);		
			if (!ret) 
			{
				logger.Exception(100,"Error retrying confs on db.");
				return false;
			}

			// Setup conferences on the mcu one-by-one
			while (confq.Count > 0) 
			{
				NS_MESSENGER.Conference confObj = new NS_MESSENGER.Conference();
				confObj = (NS_MESSENGER.Conference)confq.Dequeue();
					
				// Setup a single conference on the mcu.
				//bool ret1 = SetupOnMcu(ref confObj); CHANGED
                bool ret1 = PushConfToMcu(confObj, false);
				if (!ret1) 
				{

					logger.Exception(100,"Conference setup skipped.");
					//db.UpdateConfStatusAsRetry(confObj);
					continue;
				}
			}

			// Add party to mcu
			logger.Trace ("Entering the Add Party sub-system...");
			ret = false;
			ret = AddPartyOnMcu();
			if (!ret)
			{
				logger.Trace("Add party ops failed.");
			}

			// Edit party on mcu
			logger.Trace ("Entering the Edit Party sub-system...");
			ret = false;
			ret = EditPartyOnMcu();
			if (!ret)
			{
				logger.Trace("Add party ops failed.");
			}

			return true;			
		}
		catch (Exception e)
		{
			logger.Exception(100,e.Message);
			return false;
		}
	}


	/// <summary>
	/// Add an endpoint in an ongoing conference on the bridge.
	/// </summary>
	/// <returns></returns>
	internal bool AddPartyOnMcu()
	{
		try
		{
			// initialisation
			NS_DATABASE.Database db = new NS_DATABASE.Database(configParams);
			Queue confq = new Queue();

			// fetch immediate confs 
			logger.Trace("Fetch immediate confs...");
			bool ret = db.FetchImmediateConferences(ref confq,5);
			if (!ret) 
			{
				logger.Exception(100,"Error fetching confs from db.");
				//return false;
			}

			// fetch the ongoing confs 
			ret = false;
			logger.Trace("Fetch ongoing confs...");
			ret = db.FetchOngoingConfs(ref confq,5);
			if (!ret) 
			{
				logger.Exception(100,"Error retreiving ongoing confs.");
				return false;
			}		
			
			// fetch the new participants for these confs
			while (confq.Count > 0)
			{
				NS_MESSENGER.Conference conf = new NS_MESSENGER.Conference();
				conf = (NS_MESSENGER.Conference) confq.Dequeue();	

				//fetch the conf info i.e. conf.sDbName
				ret = false;
				logger.Trace("Fetch conf details....");
				ret = db.FetchConf (ref conf);
				if (!ret) 
				{
					logger.Exception(100,"Error retreiving conf info.");
					return false;
				}

				// Clearing out all the participants.
				conf.qParties.Clear();

				// fetch the new participants
				ret = false;
				logger.Trace("Fetch ongoing participants....");
				ret = db.FetchOngoingParticipants(ref conf,2); 
				if (!ret) 
				{
					logger.Exception(100,"Error retreiving new participants.");
					return false;
				}

				// fetch the new cascadelinks
				ret = false;
				logger.Trace("Fetch ongoing cascade links....");
				ret = db.FetchOngoingCascadeLinks(ref conf,2); 
				if (!ret) 
				{
					logger.Exception(100,"Error retreiving new cascade links.");
					return false;
				}


				// the conf contains only those parties which needed to be added.
				ret = false;
				while (conf.qParties.Count > 0)
				{
					// fetch a party from the queue
					NS_MESSENGER.Party party = new NS_MESSENGER.Party();
					party = (NS_MESSENGER.Party) conf.qParties.Dequeue();

					//audio or video party
					if (conf.etMediaType == NS_MESSENGER.Conference.eMediaType.AUDIO)
					{
						party.etCallType = NS_MESSENGER.Party.eCallType.AUDIO;
					}
					else
					{
						party.etCallType = NS_MESSENGER.Party.eCallType.VIDEO;
					}

                    

					// add the party to mcu
					ret = false;string errMcuMsg = null ;
                    if (party.cMcu.etType == NS_MESSENGER.MCU.eType.ACCORDv6 || party.cMcu.etType == NS_MESSENGER.MCU.eType.ACCORDv7 || party.cMcu.etType == NS_MESSENGER.MCU.eType.RMX)
                    {
                        // Polycom 
                        logger.Trace("Entering Polycom sub-system....");
                        NS_POLYCOM.Polycom polycom = new NS_POLYCOM.Polycom(configParams);
                        ret = polycom.AddPartyToMcu(conf,party);
                        if (!ret) this.errMsg = polycom.errMsg;
                    }
                    else
                    {
                        if (party.cMcu.etType == NS_MESSENGER.MCU.eType.CODIAN)
                        {
                            // Codian
                            logger.Trace("Entering Codian sub-system....");
                            NS_CODIAN.Codian codian = new NS_CODIAN.Codian(configParams);
                            ret = codian.AddPartyToMcu(conf,party);
                            if (!ret) this.errMsg = codian.errMsg;
                        }
                        //FB 2501 Call Monitoring Start
                        else if (party.cMcu.etType == NS_MESSENGER.MCU.eType.CISCOTP)
                        {
                            NS_CiscoTPServer.CiscoTPServer CiscoTPServer = new NS_CiscoTPServer.CiscoTPServer(configParams);
                            ret = CiscoTPServer.AddParticipantToMcu(conf, party);
                            if (!ret) this.errMsg = CiscoTPServer.errMsg;
                        }
                        //FB 2501 Call Monitoring End
                        //FB 2556 Starts
                        else if (party.cMcu.etType == NS_MESSENGER.MCU.eType.RADVISIONIVIEW)
                        {
                            // Radvision Iview Bridge
                            NS_RADVISION_IView.RadVisionIView RadIview = new NS_RADVISION_IView.RadVisionIView(configParams);
                            logger.Trace("Entering Radvision IView sub-system...");
                            ret = RadIview.AddPartyToMcu(conf, party);
                            this.errMsg = RadIview.errMsg;
                        }
                        //FB 2556 Ends
                        //ZD 101217 Start
                        else if (party.cMcu.etType == NS_MESSENGER.MCU.eType.Pexip)
                        {
                            // Pexip Bridge
                            NS_Pexip.pexip pexip = new NS_Pexip.pexip(configParams);
                            logger.Trace("Entering pexip sub-system...");
                            ret = pexip.ConnectDisconnectEndpoint(conf, party, true);
                            this.errMsg = pexip.errMsg;
                        }
                        //ZD 101217 End
                        else 
                        {
                            if (party.cMcu.etType == NS_MESSENGER.MCU.eType.TANDBERG)
                            {
                                // Tandberg
                                logger.Trace("Entering Tandberg sub-system....");
                                NS_TANDBERG.Tandberg tandberg = new NS_TANDBERG.Tandberg(configParams);
                                ret = tandberg.AddPartyToMcu(conf, party, 0);
                                if (!ret) this.errMsg = tandberg.errMsg;
                            }
                        }
                    }
					if (!ret) 
					{
						// party couldn't be added 
						if (errMcuMsg != null)
							logger.Exception(100,"Name = " + party.sName + ",Status =" + errMcuMsg);
						
						// alert
						NS_MESSENGER.Alert alert = new NS_MESSENGER.Alert();
						alert.confID = conf.iDbID;
						alert.instanceID = conf.iInstanceID;
						alert.message = this.errMsg;
						alert.typeID = 2; //add party
						ret = false;
						ret = db.InsertConfAlert(alert);						
						if (!ret) logger.Trace("Conference alert insert failed.");
						
						logger.Exception(100,"Error adding party . Party = " + party.sName);
						//continue;
					}	
					else 
					{
						// party added successfully 
						// update the party status in the db from "New" to "Unchanged" 
						db.UpdateOngoingPartyStatus(party.etType,party.iDbId,conf.iDbID,conf.iInstanceID,0);
					}
				}	//cycling thru parties
			} // cycling thru confs

			return true;
		}
		catch (Exception e)
		{
			logger.Exception(100,e.Message);
			return false;
		}
	}



	/// <summary>
	/// Edit an endpoint in an ongoing conference on the bridge
	/// </summary>
	/// <returns></returns>
	private bool EditPartyOnMcu()
	{
		try 
		{
			// initialisation
			NS_DATABASE.Database db = new NS_DATABASE.Database(configParams);
			Queue confq = new Queue();

			// fetch the ongoing confs 
			bool ret = db.FetchOngoingConfs(ref confq,5);
			if (!ret) 
			{
				logger.Exception(100,"Error retreiving ongoing confs.");
				return false;
			}		
			// fetch the new participants for these confs
			while (confq.Count > 0)
			{
				NS_MESSENGER.Conference conf = new NS_MESSENGER.Conference();
				conf = (NS_MESSENGER.Conference) confq.Dequeue();	

				//fetch the conf info i.e. conf.sDbName
				ret = false;
				ret = db.FetchConf (ref conf);
				if (!ret) 
				{
					logger.Exception(100,"Error retreiving conf info.");
					return false;
				}

				// Clearing out all the participants.
				conf.qParties.Clear();

				// fetch the edited participants
				ret = false;
				ret = db.FetchOngoingParticipants(ref conf,1);
				if (!ret) 
				{
					logger.Exception(100,"Error retreiving edited participants.");
					return false;
				}
				
				// fetch the edited cascade links
				ret = false;
				ret = db.FetchOngoingCascadeLinks(ref conf,1);
				if (!ret) 
				{
					logger.Exception(100,"Error retreiving edited cascade links.");
					return false;
				}
				

				// the conf contains only those parties which needed to be edited.
				ret = false;
				while (conf.qParties.Count > 0)
				{
					// fetch a party from the queue
					NS_MESSENGER.Party party = new NS_MESSENGER.Party();
					party = (NS_MESSENGER.Party) conf.qParties.Dequeue();

					// step 1:  delete the party from mcu
					ret = false;

                    if (party.cMcu.etType == NS_MESSENGER.MCU.eType.ACCORDv6 || party.cMcu.etType == NS_MESSENGER.MCU.eType.ACCORDv7 || party.cMcu.etType == NS_MESSENGER.MCU.eType.RMX)
                    {
                        // Accord
                        NS_POLYCOM.Polycom accord = new NS_POLYCOM.Polycom(configParams);
                        ret = accord.DeletePartyFromMcu(party, conf.sDbName);
                        if (!ret) this.errMsg = accord.errMsg;
                    }
                    else
                    {
                        if (party.cMcu.etType == NS_MESSENGER.MCU.eType.CODIAN)
                        {
                            // Codian
                            NS_CODIAN.Codian codian = new NS_CODIAN.Codian(configParams); 
                            ret = codian.TerminateEndpoint(conf, party);
                            if (!ret) this.errMsg = codian.errMsg;
                        }
                        //FB 2501 Call Monitoring Start
                        else if (party.cMcu.etType == NS_MESSENGER.MCU.eType.CISCOTP)
                        {
                            NS_CiscoTPServer.CiscoTPServer CiscoTPServer = new NS_CiscoTPServer.CiscoTPServer(configParams);
                            ret = CiscoTPServer.TerminateEndpoint(conf, party);
                            if (!ret) this.errMsg = CiscoTPServer.errMsg;
                        }
                        //FB 2501 Call Monitoring End
                        //ZD 101217 Start
                        else if (party.cMcu.etType == NS_MESSENGER.MCU.eType.Pexip)
                        {
                            // Pexip Bridge
                            NS_Pexip.pexip pexip = new NS_Pexip.pexip(configParams);
                            logger.Trace("Entering pexip sub-system...");
                            ret = pexip.ConnectDisconnectEndpoint(conf, party, false);
                            this.errMsg = pexip.errMsg;
                        }
                        //ZD 101217 End
                        //FB 2556 Starts
                        else if (party.cMcu.etType == NS_MESSENGER.MCU.eType.RADVISIONIVIEW)
                        {
                            // Radvision Iview Bridge
                            NS_RADVISION_IView.RadVisionIView RadIview = new NS_RADVISION_IView.RadVisionIView(configParams);
                            logger.Trace("Entering Radvision IView sub-system...");
                            ret = RadIview.TerminateEndpoint(conf, party);
                            this.errMsg = RadIview.errMsg;
                        }
                        //FB 2556 Ends
                        else
                        {
                            // Tandberg
                            NS_TANDBERG.Tandberg tandberg = new NS_TANDBERG.Tandberg(configParams);
                            ret = tandberg.TerminateEndpoint(conf, party);
                            if (!ret) this.errMsg = tandberg.errMsg;
                        }
                    }
					if (!ret) 
					{
						// keep on continuing if party couldn't be deleted.
						logger.Exception(100,"Error deleting party . Party = " + party.sName);
					}
					
					// step 2 : add the party to mcu 
					ret = false;string errMcuMsg = null;
                    if (party.cMcu.etType == NS_MESSENGER.MCU.eType.ACCORDv6 || party.cMcu.etType == NS_MESSENGER.MCU.eType.ACCORDv7 || party.cMcu.etType == NS_MESSENGER.MCU.eType.RMX)
                    {
                        NS_POLYCOM.Polycom accord = new NS_POLYCOM.Polycom(configParams);
                        ret = accord.AddPartyToMcu(conf,party);
                        if (!ret) this.errMsg = accord.errMsg;
                    }
                    else
                    {
                        if (party.cMcu.etType == NS_MESSENGER.MCU.eType.CODIAN)
                        {
                            NS_CODIAN.Codian codian = new NS_CODIAN.Codian(configParams);
                            ret = codian.AddPartyToMcu(conf,party);
                            if (!ret) this.errMsg = codian.errMsg;
                        }
                        //FB 2501 Call Monitoring Start
                        else if (party.cMcu.etType == NS_MESSENGER.MCU.eType.CISCOTP)
                        {
                            NS_CiscoTPServer.CiscoTPServer CiscoTPServer = new NS_CiscoTPServer.CiscoTPServer(configParams);
                            ret = CiscoTPServer.AddParticipantToMcu(conf, party);
                            if (!ret) this.errMsg = CiscoTPServer.errMsg;
                        }
                        //FB 2501 Call Monitoring End
                        //FB 2556 Starts
                        else if (party.cMcu.etType == NS_MESSENGER.MCU.eType.RADVISIONIVIEW)
                        {
                            // Radvision Iview Bridge
                            NS_RADVISION_IView.RadVisionIView RadIview = new NS_RADVISION_IView.RadVisionIView(configParams);
                            logger.Trace("Entering Radvision IView sub-system...");
                            ret = RadIview.AddPartyToMcu(conf, party);
                            this.errMsg = RadIview.errMsg;
                        }
                        //FB 2556 Ends
                        //ZD 101217 Start
                        else if (party.cMcu.etType == NS_MESSENGER.MCU.eType.Pexip)
                        {
                            // Pexip Bridge
                            NS_Pexip.pexip pexip = new NS_Pexip.pexip(configParams);
                            logger.Trace("Entering pexip sub-system...");
                            ret = pexip.ConnectDisconnectEndpoint(conf, party, true);
                            this.errMsg = pexip.errMsg;
                        }
                        //ZD 101217 End
                        else
                        {
                            NS_TANDBERG.Tandberg tandberg = new NS_TANDBERG.Tandberg(configParams);
                            ret = tandberg.AddPartyToMcu(conf,party,0);
                            if (!ret) this.errMsg = tandberg.errMsg;
                        }
                    }
					if (!ret) 
					{
						// party couldn't be added
						if (errMcuMsg != null)
							logger.Exception(100,"Name = " + party.sName + ",Status = " + errMcuMsg);

						// alert
						NS_MESSENGER.Alert alert = new NS_MESSENGER.Alert();
						alert.confID = conf.iDbID;
						alert.instanceID = conf.iInstanceID;
						alert.message = this.errMsg;
						alert.typeID = 3; //edit party
						ret = false;
						ret = db.InsertConfAlert(alert);						
						if (!ret) logger.Trace("Conference alert insert failed.");
						
						logger.Exception(100,"Error adding party . Party = " + party.sName);
						//continue;
					}
					else
					{					
						// party added successfully 
						// update the party status in the db from "Edit" to "Unchanged" 
						db.UpdateOngoingPartyStatus(party.etType,party.iDbId,conf.iDbID,conf.iInstanceID,0);			
					}

				} // cycling thru parties
			} // cycling thru confs
		
			return true;
		}
		catch (Exception e) 
		{
			logger.Exception(100,e.Message);
			return false;
		}
	}


	/// <summary>
	/// Send a reservation to the bridge. Mainly used to push future conferences.
	/// </summary>
	/// <param name="inXml"></param>
	/// <returns></returns>		
	internal bool SetConferenceOnMcu(string inXml)
	{
		//FB 2717 Starts
        NS_DATABASE.Database db = null;
        int isEnableVidyo = -1, isCloudConference = 0;
        int orgID = 11;
		//FB 2717 End
		try
		{
			// Load the string into xml parser and retrieve the tag values.
			XmlDocument xd = new XmlDocument();
			xd.LoadXml(inXml);									

			#region parse the inxml 
			// confID, instanceID
			string strConfId = xd.SelectSingleNode("//Conference/confID").InnerXml.Trim();
			bool ret = false;
			int confId = 0 ,instanceId = 0 ;
            //FB 2441 starts
            //if (strConfId.IndexOf(",") > 0)
            //{
				// Just in case ASP sends the confid in this format - 10,1 (confid,instanceid)					
				NS_OPERATIONS.Operations ops = new Operations(this.configParams);
				ret = ops.ExtractConfInstanceIds(strConfId,ref confId,ref instanceId);
                iRecurring = ops.iRecurring; //FB 2441
				if (!ret)
				{
					logger.Trace ("Invalid conf/instance id.");
					return false;
				}				 
            //}
            //else
            //{
            //    confId = Int32.Parse(strConfId);
            //    instanceId =1;
            //}
                //FB 2441 ends
			logger.Trace ("Confid = " + confId.ToString() + ", InstanceID = " + instanceId.ToString());
			#endregion

			//FB 2717 Starts
            if(db == null)
                db = new NS_DATABASE.Database(configParams);

            db.FetchVidyoStatus(ref isEnableVidyo, ref isCloudConference, confId, instanceId, ref orgID);

            if (isEnableVidyo > 0 && isCloudConference > 0)
            {
                ret = PushConfToVidyo(confId, instanceId, orgID);

            } //FB 2717 End
            else
            {
                //fetch the conf details
                NS_MESSENGER.Conference conf = new NS_MESSENGER.Conference();
                Queue partyq = new Queue();

                //FB 2441 Starts
                int FromService = 0;
                if (xd.SelectSingleNode("//Conference/FromService") != null)
                    int.TryParse(xd.SelectSingleNode("//Conference/FromService").InnerXml.Trim(), out FromService);
                conf.iAccessType = FromService; //FB 2556
                //FB 2441 Ends

                //ZD 100522 Start
                int roomid = 0;
                if (xd.SelectSingleNode("//Conference/RoomID") != null)
                    int.TryParse(xd.SelectSingleNode("//Conference/RoomID").InnerXml.Trim(), out roomid);
                conf.iRoomVMRID = roomid;
                //ZD 100522 End

                conf.iDbID = confId;
                conf.iInstanceID = instanceId;
                ret = false;
                ret = PushConfToMcu(conf, false);
            }

            // update the conf last run date time
            //NS_DATABASE.Database db = new NS_DATABASE.Database(this.configParams);
            db.UpdateConferenceLastRunDateTime(confId,instanceId);

            if (!ret)
            {
                return false;
            }

			return true;
		}
		catch (Exception e)
		{
			this.errMsg = e.Message;
			logger.Exception(100,e.Message);
			return false;
		}							
	}

    internal bool SetConferenceOnRPRM(string inXml)
    {
        //FB 2717 Starts
        NS_DATABASE.Database db = null;
        int isEnableVidyo = -1, isCloudConference = 0;
        int orgID = 11;
        //FB 2717 End
        try
        {
            // Load the string into xml parser and retrieve the tag values.
            XmlDocument xd = new XmlDocument();
            xd.LoadXml(inXml);

            #region parse the inxml
            // confID, instanceID
            string strConfId = xd.SelectSingleNode("//Conference/confID").InnerXml.Trim();
            bool ret = false;
            int confId = 0, instanceId = 0;
            //FB 2441 starts
            //if (strConfId.IndexOf(",") > 0)
            //{
            // Just in case ASP sends the confid in this format - 10,1 (confid,instanceid)					
            NS_OPERATIONS.Operations ops = new Operations(this.configParams);
            ret = ops.ExtractConfInstanceIds(strConfId, ref confId, ref instanceId);
            iRecurring = ops.iRecurring; //FB 2441
            if (!ret)
            {
                logger.Trace("Invalid conf/instance id.");
                return false;
            }
            //}
            //else
            //{
            //    confId = Int32.Parse(strConfId);
            //    instanceId =1;
            //}
            //FB 2441 ends
            logger.Trace("Confid = " + confId.ToString() + ", InstanceID = " + instanceId.ToString());
            #endregion

            //FB 2717 Starts
            if (db == null)
                db = new NS_DATABASE.Database(configParams);

            db.FetchVidyoStatus(ref isEnableVidyo, ref isCloudConference, confId, instanceId, ref orgID);

            if (isEnableVidyo > 0 && isCloudConference > 0)
            {
                ret = PushConfToVidyo(confId, instanceId, orgID);

            } //FB 2717 End
            else
            {
                //fetch the conf details
                NS_MESSENGER.Conference conf = new NS_MESSENGER.Conference();
                Queue partyq = new Queue();

                //FB 2441 Starts
                int FromService = 0;
                if (xd.SelectSingleNode("//Conference/FromService") != null)
                    int.TryParse(xd.SelectSingleNode("//Conference/FromService").InnerXml.Trim(), out FromService);
                conf.iAccessType = FromService; //FB 2556
                //FB 2441 Ends

                //ZD 100522 Start
                int roomid = 0;
                if (xd.SelectSingleNode("//Conference/RoomID") != null)
                    int.TryParse(xd.SelectSingleNode("//Conference/RoomID").InnerXml.Trim(), out roomid);
                conf.iRoomVMRID = roomid;
                //ZD 100522 End

                conf.iDbID = confId;
                conf.iInstanceID = instanceId;
                ret = false;
                ret = PushConfToRPRM(conf, false);
            }

            // update the conf last run date time
            //NS_DATABASE.Database db = new NS_DATABASE.Database(this.configParams);
            db.UpdateConferenceLastRunDateTime(confId, instanceId);

            if (!ret)
            {
                return false;
            }

            return true;
        }
        catch (Exception e)
        {
            this.errMsg = e.Message;
            logger.Exception(100, e.Message);
            return false;
        }
    }

    #region Add Endpoint Independently
    /// <summary>
    /// FB 2261
    /// Add endpoint to lifesize as there are no facility to find the endpoint status independently.
    /// </summary>
    /// <param name="inXml"></param>
    /// <returns></returns>		
    internal bool AddConferenceEndpoint(string inXml)
    {
        NS_DATABASE.Database db = null;
        XmlDocument xd = null;
        string strConfId, endpointId = "";
        bool ret = false;
        int confId = 0, instanceId = 0, epId = 0;
        NS_OPERATIONS.Operations ops = null;
        NS_MESSENGER.Conference conf = null;
        Queue partyq = null;
        NS_MESSENGER.Party guest = null;
        NS_LifeSize.LifeSize lfSize = null;
        XmlNode node = null;//RPRM Add party
        string type = "U"; // ZD 101157
        bool ConnectDisconnect = true;//ZD 104134
        
        try
        {
            // Load the string into xml parser and retrieve the tag values.
            xd = new XmlDocument();
            xd.LoadXml(inXml);

            #region parse the inxml
            // confID, instanceID
            strConfId = xd.SelectSingleNode("//AddConferenceEndpoint/ConfID").InnerXml.Trim();
            endpointId = xd.SelectSingleNode("//AddConferenceEndpoint/EndpointID").InnerXml.Trim();

            node = xd.SelectSingleNode("//AddConferenceEndpoint/Type");
            if (node != null)
                type = node.InnerXml.Trim();

            if (endpointId.Trim() != "")
            {

                if (strConfId.IndexOf(",") > 0)
                {
                    // Just in case ASP sends the confid in this format - 10,1 (confid,instanceid)					
                    ops = new Operations(this.configParams);
                    ret = ops.ExtractConfInstanceIds(strConfId, ref confId, ref instanceId);
                    if (!ret)
                    {
                        logger.Trace("Invalid conf/instance id.");
                        return false;
                    }
                }
                else
                {
                    confId = Int32.Parse(strConfId);
                    instanceId = 1;
                }

                epId = Int32.Parse(endpointId);

                logger.Trace("Confid = " + confId.ToString() + ", InstanceID = " + instanceId.ToString());
            #endregion

                //fetch the conf details
                conf = new NS_MESSENGER.Conference();
                conf.iDbID = confId;
                conf.iInstanceID = instanceId;
                ret = false;
                db = new NS_DATABASE.Database(this.configParams);
                ret = db.FetchConf(ref conf);
                if (!ret)
                    return false;

                db = new NS_DATABASE.Database(this.configParams);
                if (type == "U") // ZD 101157 Need to add in future //ZD 101496
                    ret = db.FetchGuestDetail(confId, instanceId, epId, ref guest);
                else if (type == "R") // ZD 101157 
                    ret = db.FetchRoom(confId, instanceId, epId, ref guest); // ZD 101157

                if (!ret)
                    return false;

                conf.cMcu = guest.cMcu;

                if (guest.etConnType == NS_MESSENGER.Party.eConnectionType.DIAL_IN)//ZD 104134
                {
                    logger.Trace("Skip adding the dial-in endpoint - " + guest.sName);
                    return true;
                }

                if (conf.cMcu.etType == NS_MESSENGER.MCU.eType.Lifesize)
                {
                    lfSize = new NS_LifeSize.LifeSize(this.configParams);
                    ret = lfSize.AddPartyToMcu(conf, guest);
                    if (!ret)
                    {
                        this.errMsg = lfSize.errMsg;
                        return false;
                    }
                }
                else if (conf.cMcu.etType == NS_MESSENGER.MCU.eType.CISCOTP)
                {
                    NS_CiscoTPServer.CiscoTPServer ciscoTP = new NS_CiscoTPServer.CiscoTPServer(configParams);
                    ret = ciscoTP.AddParticipantToMcu(conf, guest);
                    if (!ret)
                        this.errMsg = ciscoTP.errMsg;
                    //else
                    //    db.UpdateConference(conf.iDbID, conf.iInstanceID, conf.sGUID, guest);
                }
                else if (conf.cMcu.etType == NS_MESSENGER.MCU.eType.CODIAN)
                {
                    NS_CODIAN.Codian Codian = new NS_CODIAN.Codian(configParams);
                    ret = Codian.AddPartyToMcu(conf, guest);
                    if (!ret)
                        this.errMsg = Codian.errMsg;
                }
                //FB 2441 II Starts
                else if (conf.cMcu.etType == NS_MESSENGER.MCU.eType.RPRM)
                {
                    NS_POLYCOMRPRM.POLYCOMRPRM polycomRPRM = new NS_POLYCOMRPRM.POLYCOMRPRM(configParams);
                    ret = polycomRPRM.AddPartyToMcu(conf, guest);
                    if (!ret)
                        this.errMsg = polycomRPRM.errMsg;
                }
                //FB 2441 II Ends
				//FB 2556 Starts
                else if (conf.cMcu.etType == NS_MESSENGER.MCU.eType.RADVISIONIVIEW)
                {
                    conf.qParties.Enqueue(guest);
                    NS_RADVISION_IView.RadVisionIView RadIview = new NS_RADVISION_IView.RadVisionIView(configParams);
                    ret = RadIview.AddPartyToMcu(conf, guest);
                    if (!ret) this.errMsg = RadIview.errMsg;
                }
                //FB 2556 Ends
                else if (conf.cMcu.etType == NS_MESSENGER.MCU.eType.RMX)//FB 2553-RMX
                {
                    NS_POLYCOM.Polycom RMX = new NS_POLYCOM.Polycom(configParams);
                    ret = RMX.AddPartyToMcu(conf, guest);
                    if (!ret)
                        this.errMsg = RMX.errMsg;
                }
                //ZD 104134 Start
                else if (conf.cMcu.etType == NS_MESSENGER.MCU.eType.Pexip)
                {
                    if (guest.etConnType != NS_MESSENGER.Party.eConnectionType.DIAL_IN) //ZD 104579
                    {
                        NS_Pexip.pexip pexip = new NS_Pexip.pexip(configParams);
                        ret = pexip.ConnectDisconnectEndpoint(conf, guest, ConnectDisconnect);
                        if (!ret)
                            this.errMsg = pexip.errMsg;
                    }
                }
                //ZD 104134 End
                //FB 2569 Start
                if (!ret)
                {
                    NS_MESSENGER.Alert alert = new NS_MESSENGER.Alert();
                    alert.confID = conf.iDbID;
                    alert.instanceID = conf.iInstanceID;
                    alert.message = this.errMsg;
                    alert.typeID = 2; //add party
                    bool retp2p = db.InsertConfAlert(alert);
                    if (!retp2p)
                        logger.Trace("add party alert insert failed.");
                }
                //FB 2569 End
            }
            return true;
        }
        catch (Exception e)
        {
            this.errMsg = e.Message;
            logger.Exception(100, e.Message);
            return false;
        }
    }
    #endregion

    private bool PushConfToMcu(NS_MESSENGER.Conference conf, bool isPushConf)
    {
        string OutXML = ""; //ZD 100522
        try
        {            
            #region Fetch the conf info
            NS_DATABASE.Database db = new NS_DATABASE.Database(this.configParams);
            bool ret = db.FetchConf(ref conf);
            //ZD 101816 Starts
            List<int> confmcuList = new List<int>();
            if (conf.iConfType == 2 || conf.iConfType == 6)
            {
                db.FetchConfMcu(conf, ref confmcuList);
                if (confmcuList.Count <= 0)
                {
					//ZD 103944 Starts
                    NS_MESSENGER.Alert alert = new NS_MESSENGER.Alert();
                    alert.confID = conf.iDbID;
                    alert.instanceID = conf.iInstanceID;
                    alert.message = "Conference is not launched due to MCU is in Maintenance OR Disabled state";
                    alert.typeID = 1;
                    ret = false;
                    ret = db.InsertConfAlert(alert);
                    if (!ret) logger.Trace("Conference alert insert failed.");
                    logger.Trace("Error in feching MCU for the CONFERENCE ID = '" + conf.iDbID.ToString() + "' and INSTANCE ID = '" + conf.iInstanceID.ToString() + "' due to MCU is in Maintenance OR Disabled state");//ZD 103944
					//ZD 103944 Ends
                    return false;
                }
            }
            //ZD 101816 End

            bool ret2 = db.GetandSetMCUStartandEnd(ref conf);//FB 2440
            if (!ret || !ret2) //ZD 100036
            {
                this.errMsg = db.errMsg;

                try
                {
                    NS_MESSENGER.Alert alert = new NS_MESSENGER.Alert();
                    alert.confID = conf.iDbID;
                    alert.instanceID = conf.iInstanceID;
                    alert.message = this.errMsg;
                    alert.typeID = 1; // conf setup failed.
                    ret = false;
                    ret = db.InsertConfAlert(alert);
                    if (!ret) logger.Trace("Conference alert insert failed.");
                }
                catch (Exception e)
                {
                    logger.Exception(100, e.Message);
                }

                logger.Exception(100, "Error in fetching conf info from db.");
                return false;
            }
            #endregion                  

            if (conf.iPermanent == 0) //ZD 100522
            {
                #region Fetch the endpoints
                ret = false;
                ret = db.FetchRooms(conf.iDbID, conf.iInstanceID, ref conf.qParties, 0);
                if (!ret)
                {
                    logger.Exception(100, "Error in fetching rooms from db.");
                    return false;
                }
                ret = false;
                ret = db.FetchUsers(conf.iDbID, conf.iInstanceID, ref conf.qParties, 0);
                if (!ret)
                {
                    logger.Exception(100, "Error in fetching users from db.");
                    return false;
                }
                ret = false;
                ret = db.FetchGuests(conf.iDbID, conf.iInstanceID, ref conf.qParties, 0);
                if (!ret)
                {
                    logger.Exception(100, "Error in fetching guests from db.");
                    return false;
                }
                ret = false;
                Queue cascadeLinks = new Queue();
                ret = db.FetchCascadeLinks(conf.iDbID, conf.iInstanceID, ref cascadeLinks, 0);
                if (!ret)
                {
                    logger.Exception(100, "Error in fetching cascade links from db.");
                    return false;
                }
                if (conf.etType != NS_MESSENGER.Conference.eType.POINT_TO_POINT && conf.iIsVMR > 0 && conf.iIsVMR != 2) //FB 2447 //ZD 100522
                {
                    NS_MESSENGER.Party refParty = new NS_MESSENGER.Party();

                    if (cascadeLinks.Count > 0)
                        refParty = (NS_MESSENGER.Party)cascadeLinks.Peek();

                    ret = false;
                    ret = db.FetchVMRParty(ref conf, ref conf.qParties, refParty);
                    if (!ret)
                    {
                        logger.Exception(100, "Error in fetching VMR from db.");
                    }

                }
                //ZD 103263 Start
                if (conf.etType != NS_MESSENGER.Conference.eType.POINT_TO_POINT && conf.iBJNConf == 1 && conf.iIsVMR == 0)
                {
                    ret = false;
                    ret = db.FetchBJNEndpoint(conf, ref conf.qParties, confmcuList);
                    if (!ret)
                    {
                        logger.Exception(100, "Error in fetching VMR from db.");
                    }
                }
                //ZD 103263 End
                #endregion

                #region Check if P2P call
                if (conf.etType == NS_MESSENGER.Conference.eType.POINT_TO_POINT)
                {
                    //ZD 101363 Start
                    #region Check Caller connection type

                    bool ret5 = false;
                    System.Collections.IEnumerator partyEnumerator1;
                    partyEnumerator1 = conf.qParties.GetEnumerator();
                    int partyCount = conf.qParties.Count;
                    for (int i = 0; i < partyCount; i++)
                    {
                        // src party 
                        NS_MESSENGER.Party party = new NS_MESSENGER.Party();
                        // go to next party in the queue
                        partyEnumerator1.MoveNext();
                        party = (NS_MESSENGER.Party)partyEnumerator1.Current;
                        if (party.etCallerCallee == NS_MESSENGER.Party.eCallerCalleeType.CALLER)
                        {
                            logger.Trace("port :" + party.iAPIPortNo);
                            if (party.iSSHSupport == 1)
                            {
                                // connect call on ssh 
                                NS_SSH.SSH SSH = new NS_SSH.SSH(this.configParams);
                                ret5 = SSH.ConnectDisconnectCall(conf, null, true);
                            }
                            else
                            {
                                // connect call on telnet 
                                NS_TELNET.Telnet telnet = new NS_TELNET.Telnet(this.configParams);
                                ret5 = telnet.ConnectDisconnectCall(conf, null, true);
                            }
                        }
                    }

                    #endregion
                    //ZD 101363 End

                    if (ret5)
                    {
                        // conf setup was successful.
                        // update the conf.status flag to "Ongoing"
                        conf.etStatus = NS_MESSENGER.Conference.eStatus.ONGOING;
                        db.UpdateConferenceStatus(conf.iDbID, conf.iInstanceID, conf.etStatus);
                        //FB 2390 Start
                        if (conf.qParties.Count > 0)
                        {
                            System.Collections.IEnumerator partyEnumerator;
                            partyEnumerator = conf.qParties.GetEnumerator();
                            NS_MESSENGER.Party party = new NS_MESSENGER.Party();
                            // go to next party in the queue
                            partyEnumerator.MoveNext();
                            party = (NS_MESSENGER.Party)partyEnumerator.Current;
                            if (party.ip2pCallid > 1)
                            {
                                db.UpdateCallId(conf.iDbID, conf.iInstanceID, party.ip2pCallid);
                            }
                        }
                        //FB 2390 End
                        return true;
                    }
                    else
                    {
                        // conf setup failed
                        this.errMsg = "Error: " + this.errMsg;
                        try
                        {
                            NS_MESSENGER.Alert alert = new NS_MESSENGER.Alert();
                            alert.confID = conf.iDbID;
                            alert.instanceID = conf.iInstanceID;
                            alert.message = this.errMsg;
                            alert.typeID = 1;
                            ret = false;
                            ret = db.InsertConfAlert(alert);
                            if (!ret) logger.Trace("Conference alert insert failed.");
                        }
                        catch (Exception e)
                        {
                            logger.Exception(100, e.Message);
                        }
                        logger.Exception(100, "Conference setup failed for a p2p telnet call. Confid = " + conf.iDbID.ToString() + ",InstanceId = " + conf.iInstanceID.ToString());
                        return false;
                    }
                }
                #endregion

                #region Cascade Check

                // backup of cascade links 
                Queue cascadeLinksBackup = new Queue(cascadeLinks);

                // check if conf is cascaded
                Queue confq = new Queue();
                if (cascadeLinks.Count > 0)
                {
                    logger.Trace("In cascade");
                    bool ret1 = CascadeLogic(conf, ref confq, cascadeLinks);
                    if (!ret1)
                    {
                        logger.Exception(100, "Error in fetching the bridge for a cascade conf. ConfID = " + conf.iDbID.ToString() + ",InstanceID = " + conf.iInstanceID.ToString());
                        return false;
                    }
                }
                else
                {
                    logger.Trace("In non-cascade ");
                    bool ret1 = NonCascadeLogic(ref conf, ref confq, conf.qParties);
                    if (!ret1)
                    {
                        logger.Exception(100, "Error in fetching the bridge for a Non-Cascade conf. ConfID = " + conf.iDbID.ToString() + ",InstanceID = " + conf.iInstanceID.ToString());
                        return false;
                    }
                }
                #endregion

                #region Setup the confs on the bridge with all participants
                while (confq.Count > 0)
                {
                    NS_MESSENGER.Conference partialConf = new NS_MESSENGER.Conference();
                    partialConf = (NS_MESSENGER.Conference)confq.Dequeue();

                    // check if any party is set as a lecturer
                    if (partialConf.bLectureMode)
                    {
                        System.Collections.IEnumerator partyEnumerator;
                        partyEnumerator = partialConf.qParties.GetEnumerator();
                        int partyCount = partialConf.qParties.Count;
                        for (int i = 0; i < partyCount; i++)
                        {
                            // go to next party in the queue
                            partyEnumerator.MoveNext();

                            // check if party is lecturer. if yes, save the lecturer name in conf object.
                            NS_MESSENGER.Party party = new NS_MESSENGER.Party();
                            party = (NS_MESSENGER.Party)partyEnumerator.Current;
                            if (party.bIsLecturer)
                            {
                                partialConf.sLecturer = party.sName;
                                break;
                            }
                        }
                    }
                    ret2 = false;//ZD 100036

                    // Polycom Bridge 
                    #region Polycom
                    if (partialConf.cMcu.etType == NS_MESSENGER.MCU.eType.ACCORDv6 || partialConf.cMcu.etType == NS_MESSENGER.MCU.eType.ACCORDv7 || partialConf.cMcu.etType == NS_MESSENGER.MCU.eType.RMX)
                    {
                        //FB 1766 - Code Start
                        #region ####### IVR-SETTINGS ########
                        if (partialConf.cMcu.iEnableIVR == 1)
                        {
                            partialConf.etMessageServiceType = NS_MESSENGER.Conference.eMsgServiceType.IVR;
                            partialConf.sMessageServiceName = partialConf.cMcu.sIVRServiceName;

                            if (partialConf.cMcu.etType == NS_MESSENGER.MCU.eType.ACCORDv7) //FB 1740 - Code modified during 1766
                                partialConf.bEntryQueueAccess = true; //Modified during FB 1907 as entryqueue property of conf is being set instead of partialconf which is sent to calling methods
                            //conf.bEntryQueueAccess = true;
                        }
                        #endregion
                        //FB 1766 - Code End
                        #region ###### CLIENT-SPECIFIC-ADJUSTMENTS #####
                        // ###### CLIENT-SPECIFIC-CODE-BEGIN #####
                        // Client: VAOHIO 
                        if (this.configParams.client.ToUpper() == "VAOHIO")
                        {
                            NS_OPERATIONS.ClientCustomizations client = new ClientCustomizations(this.configParams);
                            client.VAOHIO_SwitchConferenceToVideoSwitching(ref partialConf);
                        }
                        // Client: PSU
                        if (this.configParams.client.ToUpper() == "PSU")
                        {
                            NS_OPERATIONS.ClientCustomizations client = new ClientCustomizations(this.configParams);
                            client.PSU_SetMcuVideoConferenceDefaults(ref partialConf);
                        }
                        //####### CLIENT-SPECIFIC-CODE-END ##### 
                        #endregion

                        // Adjusting the conf time (in GMT) to mcu time (local time)
                        //db.ConvertGmtToLocalTime(partialConf.cMcu.m_timezoneId,ref partialConf.m_startDateTime);							
                        NS_POLYCOM.Polycom polycom = new NS_POLYCOM.Polycom(configParams);
                        //ZD 100522_S1 Start
                        if (partialConf.iIsVMR == 0 && partialConf.iBJNConf == 0 && partialConf.sBJNUniqueid == "")//ZD 103263
                            ret2 = polycom.SetConference(partialConf, ref OutXML);
                        //ZD 103263 Start
                        else if (partialConf.iBJNConf == 1 || partialConf.sBJNUniqueid != "") 
                        {
                            if (partialConf.iPushtoExternal >= 1 && partialConf.iAccessType == (int)NS_MESSENGER.AccessMode.FromService && partialConf.iCascadeBJN == 1)
                            {
                                ret2 = polycom.SetConference(partialConf, ref OutXML);
                            }
                            else if (conf.iBJNMeetingType == (int)NS_MESSENGER.BJNMeetingType.OneTimeMeeting)
                            {
                                NS_BlueJean.BlueJean BJN = new NS_BlueJean.BlueJean(configParams);
                                bool ret1 = db.FetchAccessKey(ref partialConf.cMcu);
                                if (!ret1)
                                {
                                    logger.Trace("Error in Fetching Rooms.");
                                    return (false);
                                }

                                if (partialConf.cMcu.sBJNAPPkey != "" && partialConf.cMcu.sBJNAppsecret != "")
                                {
                                    ret1 = db.CheckBJNToken(ref partialConf.cMcu);
                                    if (!ret1)
                                    {
                                        BJN = new NS_BlueJean.BlueJean(configParams);
                                        ret1 = BJN.UpdateAccessToken(ref partialConf.cMcu);
                                        if (!ret1) this.errMsg = BJN.errMsg;
                                    }
                                }
                                else
                                {
                                    logger.Trace("BJN is not Configure properly.");
                                    return (false);
                                }
                                BJN.isRecurring = iRecurring;
                                ret2 = BJN.SetConference(partialConf, partialConf.cMcu.sBJNAccessToken);
                                if (!ret2) this.errMsg = BJN.errMsg;
                            }
                        }
                        else if (partialConf.iIsVMR == 2)
                        {
                            ret2 = polycom.SetConference(partialConf, ref OutXML);
                            ret2 = polycom.UpdateConfPassword(conf, ref OutXML); ////104003
                        }
                        //ZD 103263 End
                        //ZD 100522_S1 End
                        if (!ret2) this.errMsg = polycom.errMsg;
                    }
                    #endregion
                    else
                    {
                        // Codian Bridge.
                        #region Codian
                        if (partialConf.cMcu.etType == NS_MESSENGER.MCU.eType.CODIAN)
                        {
                            //this.errMsg = "Codian MCU is not currently supported.";
                            //return false;
                            // Adjusting the conf time (in GMT) to mcu time (local time)
                            db.ConvertTimeAcrossTimeZone(partialConf.dtStartDateTimeInUTC, partialConf.cMcu.iTimezoneID, ref partialConf.dtStartDateTimeInLocalTZ);
                            NS_CODIAN.Codian codian = new NS_CODIAN.Codian(configParams);
                            logger.Trace("Entering codian sub-system...");
                            //ZD 100522_S1 Start
                            if (partialConf.iIsVMR == 0 && partialConf.iBJNConf == 0 && partialConf.sBJNUniqueid == "")//ZD 103263
                                ret2 = codian.SetConference(partialConf);
                            //ZD 103263 Start
                            else if (partialConf.iBJNConf == 1 || partialConf.sBJNUniqueid != "") 
                            {
                                if (partialConf.iPushtoExternal >= 1 && partialConf.iAccessType == (int)NS_MESSENGER.AccessMode.FromService && partialConf.iCascadeBJN == 1)
                                {
                                    ret2 = codian.SetConference(partialConf);
                                }
                                else if (conf.iBJNMeetingType == (int)NS_MESSENGER.BJNMeetingType.OneTimeMeeting)
                                {
                                    NS_BlueJean.BlueJean BJN = new NS_BlueJean.BlueJean(configParams);
                                    bool ret1 = db.FetchAccessKey(ref partialConf.cMcu);
                                    if (!ret1)
                                    {
                                        logger.Trace("Error in Fetching Rooms.");
                                        return (false);
                                    }

                                    if (partialConf.cMcu.sBJNAPPkey != "" && partialConf.cMcu.sBJNAppsecret != "")
                                    {
                                        ret1 = db.CheckBJNToken(ref partialConf.cMcu);
                                        if (!ret1)
                                        {
                                            BJN = new NS_BlueJean.BlueJean(configParams);
                                            ret1 = BJN.UpdateAccessToken(ref partialConf.cMcu);
                                            if (!ret1) this.errMsg = BJN.errMsg;
                                        }
                                    }
                                    else
                                    {
                                        logger.Trace("BJN is not Configure properly.");
                                        return (false);
                                    }
                                    BJN.isRecurring = iRecurring;
                                    ret2 = BJN.SetConference(partialConf, partialConf.cMcu.sBJNAccessToken);
                                    if (!ret2) this.errMsg = BJN.errMsg;
                                }
                            }
                            else if (partialConf.iIsVMR == 2)
                            {
                                partialConf.sDbName = partialConf.sPermanentconfName;
                                ret2 = codian.UpdateVMRConference(partialConf, ref OutXML);
                            }
                            //ZD 103263 End
                            //ZD 100522_S1 End
                            if (!ret2) this.errMsg = codian.errMsg;
                        }
                        #endregion
                        else
                        {
                            if (partialConf.cMcu.etType == NS_MESSENGER.MCU.eType.TANDBERG)
                            {
                                // Tandberg bridge
                                NS_TANDBERG.Tandberg tandberg = new NS_TANDBERG.Tandberg(configParams);
                                ret2 = tandberg.SetConference(partialConf);
                                if (!ret2) this.errMsg = tandberg.errMsg;
                            }
                            else if (partialConf.cMcu.etType == NS_MESSENGER.MCU.eType.CMA)
                            {
                                // CMA bridge
                                NS_POLYCOMCMA.PolycomCMA polycomCMA = new NS_POLYCOMCMA.PolycomCMA(configParams);
                                ret2 = polycomCMA.SetConference(partialConf);
                                if (!ret2) this.errMsg = polycomCMA.errMsg;
                            }
                            //FB 2441 Starts
                            else if (partialConf.cMcu.etType == NS_MESSENGER.MCU.eType.RPRM)
                            {
								//ALLDEV-847 - Start
                                NS_POLYCOMRPRM.POLYCOMRPRM polycomRPRM = new NS_POLYCOMRPRM.POLYCOMRPRM(configParams);
                                                          
                                if (partialConf.iAccessType == (int)NS_MESSENGER.AccessMode.FromService) //FB 2556
                                { 
                                    ret2 = polycomRPRM.GetDMAConferenceDetails(partialConf);
                                    if (partialConf.iBJNConf == 1 || partialConf.sBJNUniqueid != "")
                                    {
                                        NS_MESSENGER.Party party = new NS_MESSENGER.Party();
                                        if (partialConf.etStatus.ToString() == NS_MESSENGER.Conference.eStatus.ONGOING.ToString())
                                            polycomRPRM.AddPartyToBlueJean(partialConf, party);
                                        if (!ret) this.errMsg = polycomRPRM.errMsg;
                                    }
                                }
                                else
                                {
                                    if (partialConf.iBJNConf == 1 || partialConf.sBJNUniqueid != "")
                                    {
                                        if (conf.iBJNMeetingType == (int)NS_MESSENGER.BJNMeetingType.OneTimeMeeting)
                                        {
                                            NS_BlueJean.BlueJean BJN = new NS_BlueJean.BlueJean(configParams);
                                            bool ret1 = db.FetchAccessKey(ref partialConf.cMcu);
                                            if (!ret1)
                                            {
                                                logger.Trace("Error in Fetching Rooms.");
                                                return (false);
                                            }

                                            if (partialConf.cMcu.sBJNAPPkey != "" && partialConf.cMcu.sBJNAppsecret != "")
                                            {
                                                ret1 = db.CheckBJNToken(ref partialConf.cMcu);
                                                if (!ret1)
                                                {
                                                    BJN = new NS_BlueJean.BlueJean(configParams);
                                                    ret1 = BJN.UpdateAccessToken(ref partialConf.cMcu);
                                                    if (!ret1) this.errMsg = BJN.errMsg;
                                                }
                                            }
                                            else
                                            {
                                                logger.Trace("BJN is not Configure properly.");
                                                return (false);
                                            }
                                            BJN.isRecurring = iRecurring;
                                            ret2 = BJN.SetConference(partialConf, partialConf.cMcu.sBJNAccessToken);
                                            if (!ret2) this.errMsg = BJN.errMsg;
                                        }
                                    }
                                    else
                                    {

                                        polycomRPRM.isRecurring = iRecurring;
                                        if (iRecurring)
                                            ret2 = polycomRPRM.SetConferenceRecurrence(partialConf);
                                        else
                                            ret2 = polycomRPRM.SetConference(partialConf);
                                        if (ret2) return true;
                                    }
									//ALLDEV-847 - End
                                }
                            }
                            //FB 2441 Ends
                            else if (partialConf.cMcu.etType == NS_MESSENGER.MCU.eType.Lifesize) //FB 2261
                            {
                                // lifesize bridge
                                NS_LifeSize.LifeSize lifeSize = new NS_LifeSize.LifeSize(configParams);
                                ret2 = lifeSize.SetConference(partialConf);
                                if (!ret2) this.errMsg = lifeSize.errMsg;
                            }
                            //FB 2501 Call Monitoring Start
                            else if (partialConf.cMcu.etType == NS_MESSENGER.MCU.eType.CISCOTP)
                            {
                                // CISCO MSE 8710 bridge
                                NS_CiscoTPServer.CiscoTPServer ciscoTP = new NS_CiscoTPServer.CiscoTPServer(configParams);
                                NS_MESSENGER.Party party = new NS_MESSENGER.Party();
                                //ZD 100522_S1 Start
                                if (partialConf.iIsVMR == 0)
                                    ret2 = ciscoTP.SetConference(ref partialConf, ref party);
                                else
                                {
                                    partialConf.sDbName = partialConf.sPermanentconfName;
                                    ret2 = ciscoTP.UpdateVMRConference(partialConf, ref party, ref OutXML);
                                }
                                //ZD 100522_S1 End
                                if (!ret2)
                                    this.errMsg = ciscoTP.errMsg;
                                //else
                                //    db.UpdateConference(conf.iDbID, conf.iInstanceID, conf.sGUID,party);
                            }
                            //FB 2501 Call Monitoring End
                            //FB 2556
                            else if (partialConf.cMcu.etType == NS_MESSENGER.MCU.eType.RADVISION)
                            {
                                // Radvision Bridge
                                NS_RADVISION.Radvision radvision = new NS_RADVISION.Radvision(configParams);
                                logger.Trace("Entering Radvision sub-system...");
                                ret2 = radvision.SetConference(partialConf);
                                this.errMsg = radvision.errMsg;
                            }
                            //FB 2556 Starts
                            //FB 2718 Start
                            else if (partialConf.cMcu.etType == NS_MESSENGER.MCU.eType.TMSScheduling)
                            {

                                NS_TMS.TMSScheduling ciscoTMS = new NS_TMS.TMSScheduling(configParams);

                                ciscoTMS.isRecurring = iRecurring;
                                if (partialConf.iAccessType == (int)NS_MESSENGER.AccessMode.FromService)
                                    ret2 = ciscoTMS.GetConferenceDetails(partialConf);
                                else
                                    ret2 = ciscoTMS.SetorUpdateConference(partialConf);
                                if (!ret2)
                                    this.errMsg = ciscoTMS.errMsg;
                                else return true;
                            }
                            //FB 2718 End  //ZD 100167 START
                            else if (partialConf.cMcu.etType == NS_MESSENGER.MCU.eType.RADVISIONIVIEW)
                            {
                                //ZD 100167
                                if (conf.iMeetingId == 1) //Dynamic
                                {
                                    //ZD 100190
                                    if (partialConf.iPushtoExternal >= 1 && partialConf.iAccessType == (int)NS_MESSENGER.AccessMode.FromService)
                                        ret2 = true;
                                    else
                                    {
                                        // Radvision Iview Bridge
                                        NS_RADVISION_IView.RadVisionIView RadIview = new NS_RADVISION_IView.RadVisionIView(configParams);
                                        logger.Trace("Entering Radvision IView sub-system...");
                                        RadIview.isRecurring = iRecurring;
                                        ret2 = RadIview.CreateDeleteUpdate(partialConf);
                                        this.errMsg = RadIview.errMsg;
                                        if (ret2) return true;
                                    }
                                }
                                else
                                    ret2 = true;
                            }
                            //FB 100167 Ends
                            //ZD 101217 Start
                            else if (partialConf.cMcu.etType == NS_MESSENGER.MCU.eType.Pexip)
                            {
                                // Pexip Bridge
                                NS_Pexip.pexip pexip = new NS_Pexip.pexip(configParams);
                                //ZD 104114 Start
                                if (partialConf.iIsVMR == 0 && partialConf.iBJNConf == 0 && partialConf.sBJNUniqueid == "" && (partialConf.iPexipConf == 1 && partialConf.iPexipCOnfTYpe == 2)) //ALLDEV-782
                                    ret2 = pexip.SetConference(partialConf);
                                //ALLDEV-862 Start
                                else if (partialConf.iIsLyncConf == 1 && partialConf.iAccessType == (int)NS_MESSENGER.AccessMode.FromService && partialConf.iEnableS4BEWS == 1)
                                    ret2 = pexip.SetLyncPexipConference(partialConf);
                                //ALLDEV-862 End
                                else if (partialConf.iBJNConf == 1 || partialConf.sBJNUniqueid != "")
                                {
                                    if (partialConf.iPushtoExternal >= 1 && partialConf.iAccessType == (int)NS_MESSENGER.AccessMode.FromService && partialConf.iCascadeBJN == 1)
                                    {
                                        ret2 = pexip.SetConference(partialConf);
                                    }
                                    else if (conf.iBJNMeetingType == (int)NS_MESSENGER.BJNMeetingType.OneTimeMeeting)
                                    {
                                        NS_BlueJean.BlueJean BJN = new NS_BlueJean.BlueJean(configParams);
                                        bool ret1 = db.FetchAccessKey(ref partialConf.cMcu);
                                        if (!ret1)
                                        {
                                            logger.Trace("Error in Fetching Rooms.");
                                            return (false);
                                        }

                                        if (partialConf.cMcu.sBJNAPPkey != "" && partialConf.cMcu.sBJNAppsecret != "")
                                        {
                                            ret1 = db.CheckBJNToken(ref partialConf.cMcu);
                                            if (!ret1)
                                            {
                                                BJN = new NS_BlueJean.BlueJean(configParams);
                                                ret1 = BJN.UpdateAccessToken(ref partialConf.cMcu);
                                                if (!ret1) this.errMsg = BJN.errMsg;
                                            }
                                        }
                                        else
                                        {
                                            logger.Trace("BJN is not Configure properly.");
                                            return (false);
                                        }
                                        BJN.isRecurring = iRecurring;
                                        ret2 = BJN.SetConference(partialConf, partialConf.cMcu.sBJNAccessToken);
                                        if (!ret2) this.errMsg = BJN.errMsg;
                                    }
                                }//ZD 104114 End
                                else if (partialConf.iPexipConf == 1 || partialConf.iPexipCOnfTYpe == 1)  //ALLDEV-782 Starts
                                {
                                    ret2 = false;
                                    NS_MESSENGER.ActiveUser userDetails = new NS_MESSENGER.ActiveUser();

                                    ret = db.FetchActiveUserDetails(ref userDetails, partialConf.iHostUserID);
                                    if (!ret)
                                    {
                                        logger.Exception(100, "Error in fetching userDetails info from db.");
                                        return false;
                                    }

                                    ret2 = false;
                                    ret2 = pexip.UpdateVMRConference(partialConf, userDetails.PexipVMRAlias, userDetails.PexipALiasURI);
                                } //ALLDEV-782 Ends
                            }
                            //ZD 101217 End
                            //ZD 104021 Start
                            else if (partialConf.cMcu.etType == NS_MESSENGER.MCU.eType.BLUEJEANS)
                            {
                                if (partialConf.iBJNMeetingType == (int)NS_MESSENGER.BJNMeetingType.OneTimeMeeting)
                                {
                                    NS_BlueJean.BlueJean BJN = new NS_BlueJean.BlueJean(configParams);
                                    bool ret1 = db.FetchAccessKey(ref partialConf.cMcu);
                                    if (!ret1)
                                    {
                                        logger.Trace("Error in Fetching Rooms.");
                                        return (false);
                                    }

                                    if (partialConf.cMcu.sBJNAPPkey != "" && partialConf.cMcu.sBJNAppsecret != "")
                                    {
                                        ret1 = db.CheckBJNToken(ref partialConf.cMcu);
                                        if (!ret1)
                                        {
                                            BJN = new NS_BlueJean.BlueJean(configParams);
                                            ret1 = BJN.UpdateAccessToken(ref partialConf.cMcu);
                                            if (!ret1) this.errMsg = BJN.errMsg;
                                        }
                                    }
                                    else
                                    {
                                        logger.Trace("BJN is not Configure properly.");
                                        return (false);
                                    }
                                    BJN.isRecurring = iRecurring;
                                    ret2 = BJN.SetConference(partialConf, partialConf.cMcu.sBJNAccessToken);
                                    if (!ret2) this.errMsg = BJN.errMsg;
                                }
                            }
                            //ZD 104021 End
                        }
                    }
                    //Dynamic //ZD 100036 Starts //Commented for 103729
                    if (partialConf.cMcu.etType == NS_MESSENGER.MCU.eType.RADVISIONIVIEW) //Dynamic 
                    {
                        return true;
                    }
                    //ZD 100036 End
                    if (ret2)
                    {
                        // conf setup was successful.
                        // update the conf.status flag to "Ongoing".

                        //ZD 100085 ZD 103263 Start
                        if (partialConf.cMcu.etType != NS_MESSENGER.MCU.eType.RPRM) 
                        {
                            if (conf.iBJNConf == 1 && conf.iBJNMeetingType == (int)NS_MESSENGER.BJNMeetingType.OneTimeMeeting)
                            {
                                if (partialConf.iPushtoExternal >= 1 && partialConf.iAccessType == (int)NS_MESSENGER.AccessMode.FromService)
                                {
                                    if (conf.dtStartDateTimeInUTC > DateTime.UtcNow)
                                    {
                                        conf.etStatus = NS_MESSENGER.Conference.eStatus.OnMCU;
                                    }
                                    else
                                        conf.etStatus = NS_MESSENGER.Conference.eStatus.ONGOING;
                                }
                            }
                            else
                            {
                                if (conf.dtStartDateTimeInUTC > DateTime.UtcNow)
                                {
                                    conf.etStatus = NS_MESSENGER.Conference.eStatus.OnMCU;
                                }
                                else
                                    conf.etStatus = NS_MESSENGER.Conference.eStatus.ONGOING;
                            }
                            db.UpdateConferenceStatus(conf.iDbID, conf.iInstanceID, conf.etStatus);
                        }
                        //ZD 103263 End
                    }
                    else
                    {
                        // conf setup failed
                        this.errMsg = "MCU Error: " + this.errMsg;
                        try
                        {
                            NS_MESSENGER.Alert alert = new NS_MESSENGER.Alert();
                            alert.confID = partialConf.iDbID;
                            alert.instanceID = partialConf.iInstanceID;
                            alert.message = this.errMsg;
                            alert.typeID = 1;
                            ret = false;
                            ret = db.InsertConfAlert(alert);
                            if (!ret) logger.Trace("Conference alert insert failed.");
                        }
                        catch (Exception e)
                        {
                            logger.Exception(100, e.Message);
                        }
                        logger.Exception(100, "Conference setup failed on the bridge. Confid = " + partialConf.iDbID.ToString() + ",InstanceId = " + partialConf.iInstanceID.ToString());
                        return false;
                    }
                }
                #endregion

                #region Update conference status
                // conf has been pushed. it hasn't been launched automatically.
                /*
                 * if (isPushConf)
                {              
                    ret = false;
                    conf.etStatus = NS_MESSENGER.Conference.eStatus.OnMCU;
                    ret = db.UpdateConferenceStatus(conf.iDbID, conf.iInstanceID, conf.etStatus); //status = 6 = OnMcu
                    if (!ret)
                    {
                        logger.Trace("Error updating conf status to OnMcu.");
                        return false;
                    }

                }
                 */
                #endregion
            }
            //ZD 100522 Start
            else
            {
                if (!PushVMRConfToMcu(conf, isPushConf))
                    return false;
            }
            //ZD 100522 End
            return true;
        }
        catch (Exception e)
        {
            this.errMsg = e.Message;
            logger.Exception(100, e.Message);
            return false;
        }
    }
    //ZD 102600
    private bool PushConfToRPRM(NS_MESSENGER.Conference conf, bool isPushConf)
    {
        string OutXML = ""; //ZD 100522
        int isRPRMConf = 1;
        try
        {            
            #region Fetch the conf info
            NS_DATABASE.Database db = new NS_DATABASE.Database(this.configParams);
            bool ret = db.FetchConf(ref conf);
            //ZD 101816 Starts
            List<int> confmcuList = new List<int>();
            if (conf.iConfType == 2 || conf.iConfType == 6)
            {
                db.FetchConfMcu(conf, ref confmcuList);
                if (confmcuList.Count <= 0)
                {
                    logger.Exception(100, "Error in fetching the bridge details from table. ConfID = " + conf.iDbID + ",InstanceID = " + conf.iInstanceID);
                    return false;
                }
            }
            //ZD 101816 End

            bool ret2 = db.GetandSetMCUStartandEnd(ref conf);//FB 2440
            if (!ret || !ret2) //ZD 100036
            {
                this.errMsg = db.errMsg;

                try
                {
                    NS_MESSENGER.Alert alert = new NS_MESSENGER.Alert();
                    alert.confID = conf.iDbID;
                    alert.instanceID = conf.iInstanceID;
                    alert.message = this.errMsg;
                    alert.typeID = 1; // conf setup failed.
                    ret = false;
                    ret = db.InsertConfAlert(alert);
                    if (!ret) logger.Trace("Conference alert insert failed.");
                }
                catch (Exception e)
                {
                    logger.Exception(100, e.Message);
                }

                logger.Exception(100, "Error in fetching conf info from db.");
                return false;
            }
            #endregion                  

            if (conf.iPermanent == 0) //ZD 100522
            {
                #region Fetch the endpoints
                ret = false;
                ret = db.FetchRooms(conf.iDbID, conf.iInstanceID, ref conf.qParties, 0);
                if (!ret)
                {
                    logger.Exception(100, "Error in fetching rooms from db.");
                    return false;
                }
                ret = false;
                ret = db.FetchUsers(conf.iDbID, conf.iInstanceID, ref conf.qParties, 0);
                if (!ret)
                {
                    logger.Exception(100, "Error in fetching users from db.");
                    return false;
                }
                ret = false;
                ret = db.FetchGuests(conf.iDbID, conf.iInstanceID, ref conf.qParties, 0);
                if (!ret)
                {
                    logger.Exception(100, "Error in fetching guests from db.");
                    return false;
                }
                ret = false;
                Queue cascadeLinks = new Queue();
                ret = db.FetchCascadeLinks(conf.iDbID, conf.iInstanceID, ref cascadeLinks, 0);
                if (!ret)
                {
                    logger.Exception(100, "Error in fetching cascade links from db.");
                    return false;
                }
                if (conf.etType != NS_MESSENGER.Conference.eType.POINT_TO_POINT && conf.iIsVMR > 0 && conf.iIsVMR != 2) //FB 2447 //ZD 100522
                {
                    NS_MESSENGER.Party refParty = new NS_MESSENGER.Party();

                    if (cascadeLinks.Count > 0)
                        refParty = (NS_MESSENGER.Party)cascadeLinks.Peek();

                    ret = false;
                    ret = db.FetchVMRParty(ref conf, ref conf.qParties, refParty);
                    if (!ret)
                    {
                        logger.Exception(100, "Error in fetching VMR from db.");
                    }

                }
                #endregion

                

                #region Cascade Check

                // backup of cascade links 
                Queue cascadeLinksBackup = new Queue(cascadeLinks);

                // check if conf is cascaded
                Queue confq = new Queue();
                if (cascadeLinks.Count > 0)
                {
                    logger.Trace("In cascade");
                    bool ret1 = CascadeLogic(conf, ref confq, cascadeLinks);
                    if (!ret1)
                    {
                        logger.Exception(100, "Error in fetching the bridge for a cascade conf. ConfID = " + conf.iDbID.ToString() + ",InstanceID = " + conf.iInstanceID.ToString());
                        return false;
                    }
                }
                else
                {
                    logger.Trace("In non-cascade ");
                    bool ret1 = NonCascadeLogic(ref conf, ref confq, conf.qParties);
                    if (!ret1)
                    {
                        logger.Exception(100, "Error in fetching the bridge for a Non-Cascade conf. ConfID = " + conf.iDbID.ToString() + ",InstanceID = " + conf.iInstanceID.ToString());
                        return false;
                    }
                }
                #endregion

                #region Setup the confs on the bridge with all participants
                while (confq.Count > 0)
                {
                    NS_MESSENGER.Conference partialConf = new NS_MESSENGER.Conference();
                    partialConf = (NS_MESSENGER.Conference)confq.Dequeue();

                    // check if any party is set as a lecturer
                    if (partialConf.bLectureMode)
                    {
                        System.Collections.IEnumerator partyEnumerator;
                        partyEnumerator = partialConf.qParties.GetEnumerator();
                        int partyCount = partialConf.qParties.Count;
                        for (int i = 0; i < partyCount; i++)
                        {
                            // go to next party in the queue
                            partyEnumerator.MoveNext();

                            // check if party is lecturer. if yes, save the lecturer name in conf object.
                            NS_MESSENGER.Party party = new NS_MESSENGER.Party();
                            party = (NS_MESSENGER.Party)partyEnumerator.Current;
                            if (party.bIsLecturer)
                            {
                                partialConf.sLecturer = party.sName;
                                break;
                            }
                        }
                    }
                    ret2 = false;//ZD 100036

                    
                    if (partialConf.cMcu.etType == NS_MESSENGER.MCU.eType.RPRM)
                    {
                        // RPRM bridge //ALLDEV-847 - Start
                        NS_POLYCOMRPRM.POLYCOMRPRM polycomRPRM = new NS_POLYCOMRPRM.POLYCOMRPRM(configParams);
                        if (partialConf.iAccessType == (int)NS_MESSENGER.AccessMode.FromService) //FB 2556
                        {
                            ret2 = polycomRPRM.GetDMAConferenceDetails(partialConf);
                            if (partialConf.iBJNConf == 1 || partialConf.sBJNUniqueid != "")
                            {
                                NS_MESSENGER.Party party = new NS_MESSENGER.Party();
                                if (partialConf.etStatus.ToString() == NS_MESSENGER.Conference.eStatus.ONGOING.ToString())
                                    polycomRPRM.AddPartyToBlueJean(partialConf, party);
                                if (!ret) this.errMsg = polycomRPRM.errMsg;
                            }
                        }
                        else
                        {
                            polycomRPRM.isRecurring = iRecurring;
                            if (iRecurring)
                                ret2 = polycomRPRM.SetConferenceRecurrence(partialConf);
                            else
                                ret2 = polycomRPRM.SetConference(partialConf);
                            /*
                            if (partialConf.iBJNConf == 1 || partialConf.sBJNUniqueid != "")
                            {
                                if (conf.iBJNMeetingType == (int)NS_MESSENGER.BJNMeetingType.OneTimeMeeting)
                                {
                                    NS_BlueJean.BlueJean BJN = new NS_BlueJean.BlueJean(configParams);
                                    bool ret1 = db.FetchAccessKey(ref partialConf.cMcu);
                                    if (!ret1)
                                    {
                                        logger.Trace("Error in Fetching Rooms.");
                                        return (false);
                                    }

                                    if (partialConf.cMcu.sBJNAPPkey != "" && partialConf.cMcu.sBJNAppsecret != "")
                                    {
                                        ret1 = db.CheckBJNToken(ref partialConf.cMcu);
                                        if (!ret1)
                                        {
                                            BJN = new NS_BlueJean.BlueJean(configParams);
                                            ret1 = BJN.UpdateAccessToken(ref partialConf.cMcu);
                                            if (!ret1) this.errMsg = BJN.errMsg;
                                        }
                                    }
                                    else
                                    {
                                        logger.Trace("BJN is not Configure properly.");
                                        return (false);
                                    }
                                    BJN.isRecurring = iRecurring;
                                    ret2 = BJN.SetConference(partialConf, partialConf.cMcu.sBJNAccessToken);
                                    if (!ret2) this.errMsg = BJN.errMsg;
                                }
                            }
                             */
                        }
                            
                    }
                    //ALLDEV-847 END

                    isRPRMConf = 3;

                    //ZD 100036 End
                    if (!ret2)
                    {
                        // conf setup failed
                        this.errMsg = "MCU Error: " + this.errMsg;
                        try
                        {
                            NS_MESSENGER.Alert alert = new NS_MESSENGER.Alert();
                            alert.confID = partialConf.iDbID;
                            alert.instanceID = partialConf.iInstanceID;
                            alert.message = this.errMsg;
                            alert.typeID = 1;
                            ret = false;
                            ret = db.InsertConfAlert(alert);
                            if (!ret) logger.Trace("Conference alert insert failed.");
                        }
                        catch (Exception e)
                        {
                            logger.Exception(100, e.Message);
                        }
                        logger.Exception(100, "Conference setup failed on the bridge. Confid = " + partialConf.iDbID.ToString() + ",InstanceId = " + partialConf.iInstanceID.ToString());
                        
                    }
                }
                #endregion

                #region Update conference status

                db.UpdateRPRMConferenceStatus(conf.iDbID, conf.iInstanceID, conf.etStatus, isRPRMConf, iRecurring); //ZD 104496

                #endregion
            }
            //ZD 100522 Start
            else
            {
                if (!PushVMRConfToMcu(conf, isPushConf))
                    return false;
            }
            //ZD 100522 End
            return true;
        }
        catch (Exception e)
        {
            this.errMsg = e.Message;
            logger.Exception(100, e.Message);
            return false;
        }
    }
	//FB 2717 Starts
    private bool PushConfToVidyo(int confID, int instanceID, int orgID)
    {
        NS_Vidyo.Vidyo vidyoFuncton = null;
        bool bRet = false;
        bool bInnerRet = false;
        String outXML = "";
        NS_MESSENGER.Alert alert = null;
        NS_DATABASE.Database db = null;
        try
        {
            vidyoFuncton = new NS_Vidyo.Vidyo(configParams);
            bRet = vidyoFuncton.SetConfernece(confID, instanceID, orgID, ref outXML);
            if (!bRet)
            {
                this.errMsg = vidyoFuncton.errMsg;
                return false;
            }
            if (db == null)
                db = new NS_DATABASE.Database(configParams);

            if (bRet)
                db.UpdateConferenceStatus(confID, instanceID, NS_MESSENGER.Conference.eStatus.ONGOING);
            else
            {
                // conf setup failed
                this.errMsg = "MCU Error: " + this.errMsg;
                try
                {
                    alert = new NS_MESSENGER.Alert();
                    alert.confID = confID;
                    alert.instanceID = instanceID;
                    alert.message = this.errMsg;
                    alert.typeID = 1;
                    bInnerRet = false;
                    bInnerRet = db.InsertConfAlert(alert);
                    if (!bInnerRet) logger.Trace("Conference alert insert failed.");
                }
                catch (Exception e)
                {
                    logger.Exception(100, e.Message);
                }
                logger.Exception(100, "Conference setup failed on the bridge. Confid = " + confID.ToString() + ",InstanceId = " + instanceID.ToString());
                return false;
            }



            return true;
        }
        catch (Exception e)
        {
            this.errMsg = e.Message;
            logger.Exception(100, e.Message);
            return false;
        }
    }
	//FB 2717 End

    //ZD 100522 Start
    public bool PushVMRConfToMcu(NS_MESSENGER.Conference conf, bool isPushConf)
    {
        string OutXML = "", VMRConfUID = "", PermanentconfName = "", InternalNumber = "", ExternalNumber = "", confid = "";
        try
        {
            NS_DATABASE.Database db = new NS_DATABASE.Database(this.configParams);
            bool ret2 = false;
          
            #region Fetch MCU
            List<int> mcuIdList = new List<int>();
            ret2 = db.FetchConfMcu(conf, ref mcuIdList);
            if (!ret2)
            {
                logger.Exception(200, "Database fetch operation failed.");
                return (false);
            }
            #endregion

            #region Connect to all the bridges and create the conference, one mcu at a time.

            logger.Trace("Number of mcu's on which conf is running :" + mcuIdList.Count.ToString());
            for (int k = 0; k < mcuIdList.Count; k++)
            {
                int mcuId = mcuIdList[k];

                //Fetch the mcu info					
                NS_MESSENGER.MCU mcu = new NS_MESSENGER.MCU();
                bool ret1 = db.FetchMcu(mcuId, ref mcu);
                if (!ret1)
                {
                    logger.Trace("Error fetching mcu details.");
                    continue;
                }

                // Assign the conf bridge to the "mcu".
                conf.cMcu = mcu;

                if (conf.cMcu.etType == NS_MESSENGER.MCU.eType.CODIAN)
                {
                    db.ConvertTimeAcrossTimeZone(conf.dtStartDateTimeInUTC, conf.cMcu.iTimezoneID, ref conf.dtStartDateTimeInLocalTZ);
                    NS_CODIAN.Codian codian = new NS_CODIAN.Codian(configParams);
                    logger.Trace("Entering codian sub-system...");

                    if (string.IsNullOrEmpty(conf.sGUID))
                        ret2 = codian.SetVMRConference(conf, ref OutXML);
                    else
                        ret2 = codian.UpdateVMRConference(conf, ref OutXML);

                    if (!ret2)
                    {
                        this.errMsg = codian.errMsg;
                        return false;
                    }
                }
                else if (conf.cMcu.etType == NS_MESSENGER.MCU.eType.CISCOTP)
                {
                    NS_CiscoTPServer.CiscoTPServer ciscoTP = new NS_CiscoTPServer.CiscoTPServer(configParams);
                    NS_MESSENGER.Party party = new NS_MESSENGER.Party();
                    logger.Trace("Entering Cisco sub-system...");

                    if (string.IsNullOrEmpty(conf.sGUID))
                        ret2 = ciscoTP.SetVMRConference(conf, ref party, ref OutXML);
                    else
                        ret2 = ciscoTP.UpdateVMRConference(conf, ref party, ref OutXML);

                    if (!ret2)
                    {
                        this.errMsg = ciscoTP.errMsg;
                        return false;
                    }
                }
                else if (conf.cMcu.etType == NS_MESSENGER.MCU.eType.ACCORDv6 || conf.cMcu.etType == NS_MESSENGER.MCU.eType.ACCORDv7 || conf.cMcu.etType == NS_MESSENGER.MCU.eType.RMX)
                {

                    NS_POLYCOM.Polycom polycom = new NS_POLYCOM.Polycom(configParams);

                    if (string.IsNullOrEmpty(conf.sGUID))
                        ret2 = polycom.SetConference(conf, ref OutXML);
                    else
                        ret2 = polycom.UpdateConfPassword(conf, ref OutXML); ////104003
                        

                    if (!ret2)
                    {
                        this.errMsg = polycom.errMsg;
                        return false;
                    }
                }

                if (ret2)
                {
                    if (conf.iRoomVMRID > 0 && OutXML != "")
                    {
                        XmlDocument xmlout = new XmlDocument();
                        xmlout.LoadXml(OutXML);

                        VMRConfUID = xmlout.SelectSingleNode("//Conference/ConfUID").InnerText;
                        PermanentconfName = xmlout.SelectSingleNode("//Conference/PermanentconfName").InnerText;


                        if (string.IsNullOrEmpty(conf.sDialinNumber))
                            conf.sDialinNumber = conf.iDbNumName.ToString();

                        confid = conf.iDbID + "," + conf.iInstanceID;

                        if (!string.IsNullOrEmpty(mcu.sBridgeExtNo))
                            InternalNumber = mcu.sBridgeExtNo + "" + conf.sDialinNumber;
                        else
                            InternalNumber = mcu.sIp + "##" + conf.sDialinNumber;

                        if (!string.IsNullOrEmpty(mcu.sBridgeDomain))
                            ExternalNumber = conf.sDialinNumber + "@" + mcu.sBridgeDomain;
                        else
                            ExternalNumber = mcu.sIp + "##" + conf.sDialinNumber;

                        ret2 = db.UpdateVMRRoomDetails(conf.iRoomVMRID, VMRConfUID, PermanentconfName, conf.sDialinNumber, confid, InternalNumber, ExternalNumber);
                        if (!ret2)
                        {
                            logger.Trace("Error in updating Room VMR.");
                            return false;
                        }
                    }
                }
            }
            #endregion

            return true;
        }
        catch (Exception e)
        {
            this.errMsg = e.Message;
            logger.Exception(100, e.Message);
            return false;
        }
    }
    //ZD 100522 End
#if NOT_IN_USE
	
	/// <summary>
	/// Delete an endpoint in an ongoing conference on the bridge.
	/// </summary>
	/// <returns></returns>
	private bool DeletePartyOnMcu()
	{
		try 
		{
			// initialisation
			NS_DATABASE.Database db = new NS_DATABASE.Database(configParams);
			Queue confq = new Queue();

			// fetch the ongoing confs 
			bool ret = db.FetchOngoingConfs(ref confq);
			if (!ret) 
			{
				logger.Exception(100,"Error retreiving ongoing confs.");
				return false;
			}		
			
			// fetch the deleted participants for these confs
			while (confq.Count > 0)
			{
				NS_MESSENGER.Conference conf = new NS_MESSENGER.Conference();
				conf = (NS_MESSENGER.Conference) confq.Dequeue();	

				//fetch the conf info i.e. conf.sDbName
				ret = false;
				ret = db.FetchConf (ref conf);
				if (!ret) 
				{
					logger.Exception(100,"Error retreiving conf info.");
					return false;
				}

				// fetch the deleted participants
				ret = false;
				ret = db.FetchOngoingParticipants(ref conf,3);
				if (!ret) 
				{
					logger.Exception(100,"Error retreiving new participants.");
					return false;
				}

				// fetch the deleted cascade links
				ret = false;
				ret = db.FetchOngoingCascadeLinks(ref conf,3);
				if (!ret) 
				{
					logger.Exception(100,"Error retreiving deleted cascade links.");
					return false;
				}

				// the conf contains only those parties which needed to be deleted.
					
				ret = false;
				while (conf.qParties.Count > 0)
				{
					// fetch a party from the queue
					NS_MESSENGER.Party party = new NS_MESSENGER.Party();
					party = (NS_MESSENGER.Party) conf.qParties.Dequeue();

					// delete the party from mcu
					ret = false;
						
					if (party.cMcu.etType == NS_MESSENGER.MCU.eType.ACCORDv6 || party.cMcu.etType == NS_MESSENGER.MCU.eType.ACCORDv7)
					{
						//NS_POLYCOM.Polycom accord = new NS_POLYCOM.Polycom(configParams);
						ret = accord.DeletePartyFromMcu(party,conf.sDbName);
					}
					if (party.cMcu.etType == NS_MESSENGER.MCU.eType.CODIAN)
					{
						NS_CODIAN.Codian codian = new NS_CODIAN.Codian(configParams);
						//ret = codian.DeletePartyFromMcu(party, conf);
					}
						
					if (!ret) 
					{
						logger.Exception(100,"Error deleting party . Party = " + party.sName);
						continue;
					}

					// party deleted successfully 
					// delete the party status in the db from "Delete" to "Terminated" 
					db.UpdateOngoingPartyStatus(party.etType,party.iDbId,conf.iDbID,conf.iInstanceID,4);
			
				}		
			}
			return true;
		}
		catch (Exception e)
		{
			logger.Exception(100,e.Message);
			return false;
		}

	}

#endif 
}

class ConfMonitor 
{
	private NS_LOGGER.Log logger;
	private NS_POLYCOM.Polycom accord;
	internal string errMsg = null;
	private NS_MESSENGER.ConfigParams configParams;

	internal ConfMonitor (NS_MESSENGER.ConfigParams config)
	{
		configParams = new NS_MESSENGER.ConfigParams();
		configParams = config;
		logger = new NS_LOGGER.Log(configParams);
		accord = new NS_POLYCOM.Polycom(configParams);
	}

	internal bool MonitorConfsOnMcu()
	{
		try
		{
			// fetch all the bridges
			NS_DATABASE.Database db = new NS_DATABASE.Database(configParams);
			Queue mcuq = new Queue();
			bool ret = db.FetchAllMcu(ref mcuq);
			if (!ret)
			{
				logger.Trace ("Error fetching all mcu info.");
				return false;
			}

			// connect to each bridge and fetch the conf listing
			Queue mcuConfq = new Queue();
			while (mcuq.Count > 0)
			{
				// connect to one bridge at a time
				NS_MESSENGER.MCU mcu = new NS_MESSENGER.MCU();
				mcu = (NS_MESSENGER.MCU) mcuq.Dequeue();
				logger.Trace ("MCU silo = " + mcu.sName);
					
				// get all the confs on one bridge
				bool ret1 = false;

                if (mcu.etType == NS_MESSENGER.MCU.eType.ACCORDv6 || mcu.etType == NS_MESSENGER.MCU.eType.ACCORDv7 || mcu.etType == NS_MESSENGER.MCU.eType.RMX)
                {
                    //NS_POLYCOM.Polycom accord = new NS_POLYCOM.Polycom(configParams);
                    ret1 = accord.FetchAllOngoingConfsStatus(mcu, ref mcuConfq);
                }
                else
                {
                    if (mcu.etType == NS_MESSENGER.MCU.eType.CODIAN)
                    {
                        NS_CODIAN.Codian codian = new NS_CODIAN.Codian(configParams);
                        ret1 = codian.FetchAllOngoingConfsStatus(mcu, ref mcuConfq);
                    }
                    else
                    {
                        NS_TANDBERG.Tandberg tandberg = new NS_TANDBERG.Tandberg(configParams);
                        ret1 = tandberg.FetchAllOngoingConfsStatus(mcu, ref mcuConfq);
                    }
                }					
				if (!ret1) 
				{
					logger.Trace ("Error fetching latest conf list status from mcu. mcu = " + mcu.sName);
				}
			}

			logger.Trace("Total confs found on mcu's : " + mcuConfq.Count.ToString());

			// Get the list of ongoing confs
			Queue ongoingConfq = new Queue(); ret = false;
			ret = db.FetchOngoingConfs(ref ongoingConfq,5);
			if (!ret)
			{
				logger.Trace ("Error in fetching ongoing conf list.");
			}
					
			//  200 = magic number
			object[] ongoingConfArray = new object[200];
			ongoingConfArray = ongoingConfq.ToArray();

			// Update the db with individual conf status.
			while (mcuConfq.Count>0)
			{
				// fetch conf from the queue
				NS_MESSENGER.Conference conf = new NS_MESSENGER.Conference();
				conf = (NS_MESSENGER.Conference)mcuConfq.Dequeue();
				
				// using the name of the conf on the bridge 
				// fetch the confid,instanceid 
				ret = false;
				ret = db.FetchConf(ref conf);
				if (!ret) continue;

				// Compare if this is an ongoing conf in vrm . 
				// If not, discard it and move to next one.
				bool isOngoingConf = false;
				for (int i = 0;i < ongoingConfArray.Length;i++)
				{
					NS_MESSENGER.Conference ongoingConf = new NS_MESSENGER.Conference();
					ongoingConf = (NS_MESSENGER.Conference)ongoingConfArray[i];
					if (conf.iDbID ==  ongoingConf.iDbID && conf.iInstanceID == ongoingConf.iInstanceID)
					{
						isOngoingConf = true;
						break;
					}
				}
				if (!isOngoingConf)
				{
					logger.Trace ("Not a ongoing conf . Name : " + conf.sMcuName);
					continue;
				}

				//change the conf status to "Ongoing"
				ret = false;
                conf.etStatus = NS_MESSENGER.Conference.eStatus.ONGOING;
				ret = db.UpdateConferenceStatus(conf.iDbID,conf.iInstanceID,conf.etStatus);
				if (!ret) continue;

				// update the conf parties 
				while (conf.qParties.Count > 0)
				{
					// one party at a time
					NS_MESSENGER.Party party = new NS_MESSENGER.Party();
					party = (NS_MESSENGER.Party) conf.qParties.Dequeue();

					#region fetch the db-equivalent party id.otherwise skip updating this party.
					// check if party is a USER
					ret = false;
					ret = db.FetchUser(ref party); 
					if (ret)
					{
						party.etType = NS_MESSENGER.Party.eType.USER;
					}
					else
					{
						// check if party is GUEST
						bool ret1 = db.FetchGuest(ref party);
						if (ret1)
						{
							party.etType = NS_MESSENGER.Party.eType.GUEST;
						}
						else
						{
							// check if party is a ROOM
							logger.Trace("Before room check.");
							bool ret2 = db.FetchRoom(ref party);
								
							logger.Trace ("After room check.");
							if (ret2) 
							{
								party.etType = NS_MESSENGER.Party.eType.ROOM;
							}
							else
							{
								bool ret3 = db.FetchCascadeLink(ref party,conf.iDbID,conf.iInstanceID); 
								if (ret3)	
								{
									party.etType = NS_MESSENGER.Party.eType.CASCADE_LINK;
								}
								else 
								{
									logger.Exception (100,"Mcu Party equivalent not found in db. Party Name = " + party.sMcuName);
									continue;
								}
							}
						}
					}
					#endregion

					// update the party status
					ret = false;
					ret =  db.UpdatePartyWithMCUMonitorData(party,conf.iDbID,conf.iInstanceID);
					if (!ret) 
					{
				    logger.Exception (100,"Error updating the party status . Party = " + party.sMcuName);
					}
				}
			}		
			return true;	
		}
		catch (Exception e)
		{
			logger.Exception (100,e.Message);
			return false;
		}
	}

  internal bool TerminateP2PConfs()
	{
		try
		{
            logger.Trace("Entering TerminateP2PConfs...");
            // fetch all the p2p confs which have ended in past 1 min
            NS_DATABASE.Database db = new NS_DATABASE.Database(configParams);
            Queue confq = new Queue();
            bool ret = db.FetchCompletedP2PConfs(ref confq);
            if (!ret)
            {
                logger.Trace("Error fetching all conf info.");
                return false;
            }
            while (confq.Count > 0)
            {              
                NS_MESSENGER.Conference conf = new NS_MESSENGER.Conference();
                conf = (NS_MESSENGER.Conference)confq.Dequeue();
                ret = false;
                ret = db.FetchConf(ref conf);
                if(!ret)
                {
                    logger.Trace ("Error fetching conf details.");
                    continue;
                }
                #region Fetch the endpoints
                ret = false;
                //ret = db.FetchAllRooms(conf.iDbID,conf.iInstanceID,ref conf.qParties);
                ret = db.FetchRooms(conf.iDbID, conf.iInstanceID, ref conf.qParties, 0);
                if (!ret)
                {
                    logger.Exception(100, "Error in fetching rooms from db.");
                    return false;
                }
                ret = false;
                //ret = db.FetchAllUsers(conf.iDbID,conf.iInstanceID,ref conf.qParties);
                ret = db.FetchUsers(conf.iDbID, conf.iInstanceID, ref conf.qParties, 0);
                if (!ret)
                {
                    logger.Exception(100, "Error in fetching users from db.");
                    return false;
                }
                #endregion

                NS_TELNET.Telnet telnet = new NS_TELNET.Telnet(this.configParams);
                ret = false;
                ret = telnet.ConnectDisconnectCall(conf, null, false);
                if (!ret)
                {
                    logger.Trace("Error disconnecting p2p conf.");                   
                }
            }
            return true;
        }      
		catch (Exception e)
		{
			logger.Exception (100,e.Message);
			return false;
		}
	}
  //Vidyo //FB 2599 Start
  internal bool TerminateCloudConfs()
  {
      NS_DATABASE.Database db = null;
      Queue confq = null;
      NS_MESSENGER.Conference conf = null;
      bool ret = false;
      NS_Vidyo.Vidyo vidyoFuncton = null;
      String outXML = "";
      try
      {
          logger.Trace("Entering Terminate Cloud Calls...");
          db = new NS_DATABASE.Database(configParams);
          confq = new Queue();
          ret = db.FetchCompletedCloudConfs(ref confq);
          if (!ret)
          {
              logger.Trace("Error fetching all conf info.");
              return false;
          }
          while (confq.Count > 0)
          {
              try
              {
                  conf = new NS_MESSENGER.Conference();
                  conf = (NS_MESSENGER.Conference)confq.Dequeue();

                  if (vidyoFuncton == null)
                      vidyoFuncton = new NS_Vidyo.Vidyo(configParams);

                  ret = false;
                  ret = vidyoFuncton.TerminateConfernece(conf.iDbID, conf.iInstanceID, conf.iOrgID, ref outXML);
                  if (!ret)
                  {
                      logger.Trace("Error fetching conf details.");
                      continue;
                  }

              }
              catch (Exception ex)
              {

                  logger.Trace("Error fetching all conf info." + ex.Message);
              }
          }
          return true;
      }
      catch (Exception e)
      {
          logger.Exception(100, e.Message);
          return false;
      }
  }
    //FB 2599 End
    //ZD 100522 Start
  internal bool TerminateVMRConfs()
  {
      try
      {
          logger.Trace("Entering TerminateVMRConfs...");
          // fetch all the VMR confs which have ended in past 1 min
          NS_DATABASE.Database db = new NS_DATABASE.Database(configParams);
          NS_OPERATIONS.ConfSetup cfsetup = new NS_OPERATIONS.ConfSetup(configParams);
          Queue confq = new Queue();
          bool ret = db.FetchCompletedVMRConfs(ref confq);
          if (!ret)
          {
              logger.Trace("Error fetching all conf info.");
              return false;
          }
          while (confq.Count > 0)
          {
              NS_MESSENGER.Conference conf = new NS_MESSENGER.Conference();
              NS_MESSENGER.Conference Vmrconf = new NS_MESSENGER.Conference();
              conf = (NS_MESSENGER.Conference)confq.Dequeue();
              ret = false;
              ret = db.FetchConf(ref conf);
              if (!ret)
              {
                  logger.Trace("Error fetching conf details.");
                  continue;
              }

              //ZD 101217 Start
              List<int> mcuIdList = new List<int>();
              bool ret2 = db.FetchConfMcu(conf, ref mcuIdList);
              if (!ret2)
              {
                  logger.Exception(200, "Database fetch operation failed.");
                  return (false);
              }

              #region Connect to all the bridges and terminate the conference, one mcu at a time.
              
              logger.Trace("Number of mcu's on which conf is running :" + mcuIdList.Count.ToString());
              for (int k = 0; k < mcuIdList.Count; k++)
              {
                  int mcuId = mcuIdList[k];

                  //Fetch the mcu info					
                  NS_MESSENGER.MCU mcu = new NS_MESSENGER.MCU();
                  bool ret1 = db.FetchMcu(mcuId, ref mcu);
                  if (!ret1)
                  {
                      logger.Trace("Error fetching mcu details.");
                      continue;
                  }

                  // Assign the conf bridge to the "mcu".
                  bool ret3 = false;
                  conf.cMcu = mcu;
                  if (mcu.etType == NS_MESSENGER.MCU.eType.Pexip)
                  {
                      #region Fetch the endpoints
                      ret = false;
                      //ret = db.FetchAllRooms(conf.iDbID,conf.iInstanceID,ref conf.qParties);
                      ret = db.FetchRooms(conf.iDbID, conf.iInstanceID, ref conf.qParties, 0);
                      if (!ret)
                      {
                          logger.Exception(100, "Error in fetching rooms from db.");
                          return false;
                      }
                      ret = false;
                      //ret = db.FetchAllUsers(conf.iDbID,conf.iInstanceID,ref conf.qParties);
                      ret = db.FetchUsers(conf.iDbID, conf.iInstanceID, ref conf.qParties, 0);
                      if (!ret)
                      {
                          logger.Exception(100, "Error in fetching users from db.");
                          return false;
                      }
                      ret = db.FetchGuests(conf.iDbID, conf.iInstanceID, ref conf.qParties, 0);
                      if (!ret)
                      {
                          logger.Exception(100, "Error in fetching guests from db.");
                          return false;
                      }
                      ret = false;
                      #endregion

                      NS_Pexip.pexip pexip = new NS_Pexip.pexip(configParams);
                      ret3 = pexip.DeleteConference(conf);
                      if (!ret3)
                      {
                          this.errMsg = pexip.errMsg;
                      }
                      else
                        db.UpdateConferenceStatus(conf.iDbID, conf.iInstanceID, NS_MESSENGER.Conference.eStatus.COMPLETED); //ZD 101522
                  }
                  else
                  {
                      if (!TerminateConfVMR(conf))
                      {
                          logger.Trace("Error in Terminating VMR conf.");
                          continue;
                      }
                  }
              }
              #endregion
              //ZD 101217 End

          }
          return true;
      }
      catch (Exception e)
      {
          logger.Exception(100, e.Message);
          return false;
      }
  }

internal bool TerminateConfVMR(NS_MESSENGER.Conference conf)
  {
      try
      {
          logger.Trace("Entering TerminateVMRConfs...");
          // fetch all the VMR confs which have ended in past 1 min
          NS_DATABASE.Database db = new NS_DATABASE.Database(configParams);
          NS_OPERATIONS.ConfSetup cfsetup = new NS_OPERATIONS.ConfSetup(configParams);
          bool connectDisconnect = false;
          string msg = ""; bool ret = false;
          NS_MESSENGER.Conference Vmrconf = new NS_MESSENGER.Conference();

          #region Fetch the endpoints
          ret = false;
          //ret = db.FetchAllRooms(conf.iDbID,conf.iInstanceID,ref conf.qParties);
          ret = db.FetchRooms(conf.iDbID, conf.iInstanceID, ref conf.qParties, 0);
          if (!ret)
          {
              logger.Exception(100, "Error in fetching rooms from db.");
              return false;
          }
          ret = false;
          //ret = db.FetchAllUsers(conf.iDbID,conf.iInstanceID,ref conf.qParties);
          ret = db.FetchUsers(conf.iDbID, conf.iInstanceID, ref conf.qParties, 0);
          if (!ret)
          {
              logger.Exception(100, "Error in fetching users from db.");
              return false;
          }
          //ZD 101567 Starts
          ret = false;
          ret = db.FetchGuests(conf.iDbID, conf.iInstanceID, ref conf.qParties, 0);
          if (!ret)
          {
              logger.Exception(100, "Error in fetching users from db.");
              return false;
          }
          //ZD 101567 Ends
          #endregion

          #region Disconnect All Endpoints
          logger.Trace(" Entering Party ConnectDisconnect All ...");

          // Fetch all the bridge ids on which this conf is running.
          System.Collections.IEnumerator partyEnumerator;
          partyEnumerator = conf.qParties.GetEnumerator();
          int partyCount = conf.qParties.Count;
          logger.Trace("Party Count = " + partyCount.ToString());

          Queue mcuIdList = new Queue();
          bool ret3 = false;
          for (int i = 0; i < partyCount; i++)
          {
              ret3 = partyEnumerator.MoveNext();
              if (!ret3) break;

              NS_MESSENGER.Party party = new NS_MESSENGER.Party();
              party = (NS_MESSENGER.Party)partyEnumerator.Current;

              logger.Trace("Party Address : " + party.sAddress.ToString());

              if (party.cMcu.etType == NS_MESSENGER.MCU.eType.ACCORDv6 || party.cMcu.etType == NS_MESSENGER.MCU.eType.ACCORDv7 || party.cMcu.etType == NS_MESSENGER.MCU.eType.RMX)
              {
                  NS_POLYCOM.Polycom polycom = new NS_POLYCOM.Polycom(configParams);
                  ret3 = polycom.OngoingConfOps("ConnectDisconnectEndpoint", conf, party, ref msg, false, 0, 0, connectDisconnect, ref party.etStatus, false, false);
                  if (ret3)
                      ret3 = polycom.OngoingConfOps("GetTerminalStatus", conf, party, ref msg, false, 0, 0, false, ref party.etStatus, false, false);
                  else
                  {
                      this.errMsg = polycom.errMsg;
                  }
                  //ZD 101371
                  ret3 = polycom.OngoingConfOps("TerminateEndpoint", conf, party, ref msg, true, 0, 0, false, ref party.etStatus, false, false);//FB 2553-RMX
                  if (!ret3)
                  {
                      this.errMsg = polycom.errMsg;
                      msg = polycom.errMsg;
                  }

              }
              else if (party.cMcu.etType == NS_MESSENGER.MCU.eType.CODIAN)
              {
                  NS_CODIAN.Codian codian = new NS_CODIAN.Codian(configParams);
                  conf.sDbName = conf.sPermanentconfName;
                  ret3 = codian.ConnectDisconnectEndpoint(conf, party, connectDisconnect);
                  if (!ret3)
                  {
                      this.errMsg = codian.errMsg;
                  }
                  //ZD 101371
                  ret3 = codian.TerminateEndpoint(conf, party);
                  if (!ret3)
                  {
                      this.errMsg = codian.errMsg;
                  }

              }
              else if (party.cMcu.etType == NS_MESSENGER.MCU.eType.CISCOTP)
              {
                  NS_CiscoTPServer.CiscoTPServer CiscoTPServer = new NS_CiscoTPServer.CiscoTPServer(configParams);
                  conf.sDbName = conf.sPermanentconfName;
                  ret3 = CiscoTPServer.ConnectDisconnectEndpoint(conf, party, connectDisconnect);
                  if (!ret3)
                  {
                      this.errMsg = CiscoTPServer.errMsg;
                  }
                  //ZD 101371
                  ret3 = CiscoTPServer.TerminateEndpoint(conf, party);
                  if (!ret3)
                  {
                      this.errMsg = CiscoTPServer.errMsg;
                  }

              }
              else
              {
                  this.errMsg = "Operation not supported on MCU.";
                  return false;
              }
          }
          #endregion

          #region Fetch VMR Conf
          Vmrconf = new NS_MESSENGER.Conference();
          Vmrconf.sGUID = conf.sGUID;
          Vmrconf.iIsVMR = conf.iIsVMR;
          ret = db.FetchConf(ref Vmrconf);
          if (!ret)
          {
              logger.Trace("Error fetching VMR conf details.");
              return false;
          }
          #endregion

          if (!cfsetup.PushVMRConfToMcu(Vmrconf, false))
          {
              logger.Trace("Error in updating VMR conference");
              return false;
          }
          return true;
      }
      catch (Exception e)
      {
          logger.Exception(100, e.Message);
          return false;
      }
  }
    //ZD 100522 End
}
	
class SendEmails
{
	private NS_LOGGER.Log logger ;
	internal string errMsg = null;
	private NS_MESSENGER.ConfigParams configParams;
	internal SendEmails (NS_MESSENGER.ConfigParams config)
	{
		configParams = new NS_MESSENGER.ConfigParams();
		configParams = config;
		logger = new NS_LOGGER.Log(configParams);
	}

	internal bool Send()
	{
		try
		{
			logger.Trace ("In the Email Sub-system...");
            logger.Trace("*** Entering Send Email Sub-system ***");
			NS_EMAIL.Email email = new NS_EMAIL.Email(configParams);
			bool ret = email.SendAllEmails();
			if (!ret)
			{
				return false;
			}
            // update LastRunTime status in db
            NS_DATABASE.Database db = new NS_DATABASE.Database(this.configParams);
            db.UpdateMailServerLastRunDateTime();

			return true;
		}
		catch (Exception e)
		{
			logger.Exception(100,e.Message);
			return false;
		}
	}
} 
     

/// <summary>
/// Misc tasks which need to be performed at the back-end.
/// </summary>
class MiscTasks	
{
	/// <summary>
	/// Public class attribute
	/// </summary>
	internal string errMsg = null;

	/// <summary>
	/// Private class attribute
	/// </summary>
	private NS_MESSENGER.ConfigParams configParams;
	private NS_LOGGER.Log logger;

	/// <summary>
	/// Constructor
	/// </summary>
	/// <param name="config"></param>
	internal MiscTasks (NS_MESSENGER.ConfigParams config)
	{	
		configParams = new NS_MESSENGER.ConfigParams();	
		configParams = config;
		logger = new NS_LOGGER.Log(configParams);
	}

	/// <summary>
	/// Delete pending conferences to release resources
	/// </summary>
	/// <returns></returns>
	internal bool DeletePendingConfs()
	{
		// zombie confs are pending confs which haven't been approved but are going to start in under 5 mins.
		// since their resources are locked these need to be released so that others can use it.
		logger.Trace ("Entering delete pending confs sub-system...");
		NS_DATABASE.Database db = new NS_DATABASE.Database(configParams);
		//bool ret = db.DeleteZombieConfs();
		//if (!ret) return false;
		return true;			
	}

	/// <summary>
	/// Delete unsent emails
	/// </summary>
	/// <returns></returns>	
	internal bool DeleteUnsentOldEmails()
	{
		// If emails are more than 7 days old delete them from the queue
		NS_DATABASE.Database db = new NS_DATABASE.Database(configParams);
		bool ret = db.DeleteUnsentOldEmails();
		if (!ret)return false;
		return true;
	}

	/// <summary>
	/// Shrink database transaction log file
	/// </summary>
	/// <returns></returns>
	internal bool ShrinkDbTransLogFile()
	{
		// Transaction log file increases over time and can space can run out on hard disk.				
		// Run this every day to keep the log file under 1 GB. 
		if ((DateTime.Now.Hour > 0 && DateTime.Now.Hour < 2) && (DateTime.Now.Minute > 0 && DateTime.Now.Minute < 3))
		{
			NS_DATABASE.Database db = new NS_DATABASE.Database(configParams);
			bool ret = db.ShrinkDbTransLogFile();
			if (!ret) return false;
		}
				
		return true;
	}
		
	/// <summary>
	/// Delete log records
	/// </summary>
	/// <returns></returns>
	internal bool DeleteLogRecords()
	{
		// TODO: Delete log records depending on log preference settings.				
		if ((DateTime.Now.Hour == 3) && (DateTime.Now.Minute > 0 && DateTime.Now.Minute < 5))
		{
			NS_DATABASE.Database db = new NS_DATABASE.Database(configParams);
			bool ret = db.DeleteLogRecords();
			if (!ret) return false;
		}					
		return true;
	}
			
	/// <summary>
	/// Reminders for conferences starting within 60 mins
	/// </summary>
	/// <returns></returns>
	internal bool ConferenceReminders()
	{
		// Fetch all confs starting in 1 hr.
		NS_DATABASE.Database db = new NS_DATABASE.Database(configParams);
		Queue confq = new Queue();
		bool ret = db.FetchReminderConfs(60,ref confq); // 60 mins = MAGIC NUMBER
				
		//Cycle thru each conf
		int confCount = confq.Count;
		for (int i = 0; i<confCount;i++)
		{
			NS_MESSENGER.Conference conf = new NS_MESSENGER.Conference();
			conf = (NS_MESSENGER.Conference) confq.Dequeue();

			// fetch conf details
			ret = false;
			ret = db.FetchConf(ref conf);
			if (!ret)
			{
				// skip this conf
				logger.Trace ("Error fetching conf info.");
				continue;
			}

			Queue emailQ = new Queue();

			//Fetch room admins
			ret = db.FetchRoomAdminsEmails(conf.iDbID,conf.iInstanceID,ref emailQ);
			if (!ret)
			{
				logger.Trace("Error fetching room admin emails.");
			}

			//Fetch participants
			ret = db.FetchParticipantsEmails(conf.iDbID,conf.iInstanceID,ref emailQ);
			if (!ret)
			{
				logger.Trace("Error fetching participants emails.");
			}

			//Create email reminder
			NS_MESSENGER.Email email = new NS_MESSENGER.Email();
			email.From = conf.sHostEmail;
			email.isHTML = true;
			email.Subject = "[VRM Reminder] " + conf.sExternalName;
			email.Body = "<html><body>This is an automated reminder. ";
			email.Body += "<BR> Conference Details: ";
			email.Body += "<BR> Name:" + conf.sExternalName;
			email.Body += "<BR> UniqueID:" + conf.iDbNumName.ToString();
			email.Body += "<BR> Description:" + conf.sDescription;
			//email.Body += "<BR> Locations:" + conf.;

			// Send email reminder to all
			while (emailQ.Count > 0)
			{
				// participants & room admins who are going to recv reminders
				email.To = (string)emailQ.Dequeue();
				ret = db.InsertEmailInQueue(email);
				if (!ret)
				{
					logger.Trace ("Error inserting email in queue.");
				}
			}
		}
		return true;
	}
	

	/// <summary>
	/// Pending work reminders to be generated.
	/// TODO: Complete this method
	/// </summary>
	/// <returns></returns>
	internal bool PendingWorkReminders()
	{
		try
		{
			//Fetch the pending work orders

			//Cycle thru each work order to create the email reminder 
			for (int i=0;i<0;i++)
			{

				//Create email reminder
/*				NS_MESSENGER.Email email = new NS_MESSENGER.Email();
				email.From = conf.sHostEmail;
				email.isHTML = true;
			
				email.Subject = "[VRM Reminder] " + conf.sExternalName;

				// work order details
				email.Body = "<html><body>This is an automated reminder for a pending work order.";
				email.Body += "<BR> Work Order Details:" ;
				email.Body += "<BR> Name: ";
				email.Body += "<BR> Type: ";
				email.Body += "<BR> Room: ";
				email.Body += "<BR> Due By: ";

				// conference details
				email.Body += "<BR><BR> Conference Details: ";
				email.Body += "<BR> Name:" + conf.sExternalName;
				email.Body += "<BR> UniqueID:" + conf.iDbNumName.ToString();
				email.Body += "<BR> Description:" + conf.sDescription;
				//email.Body += "<BR> Locations:" + conf.;

				// insert the email in the queue
				db.InsertEmailInQueue(email);
*/			}

			return true;
		}
		catch (Exception e)
		{
			logger.Exception(100,e.Message);
			return false;
		}
	}

	/// <summary>
	/// Delete Polycom MCU tokens to refresh it periodically every 2 mins.
	/// </summary>
	/// <returns></returns>
	internal bool DeletePolycomMcuTokens()
	{
		// delete the tokens every 3 hrs from 9 AM - 6 PM
		//if (this.configParams.client == "BTBOCES")
		//{
		if ((DateTime.Now.Hour == 8 && DateTime.Now.Minute < 4) || (DateTime.Now.Hour == 12 && DateTime.Now.Minute < 3) || (DateTime.Now.Hour == 18 && DateTime.Now.Minute < 5))
		{
			NS_DATABASE.Database db = new NS_DATABASE.Database(this.configParams);
			//db.DeletePolycomMcuTokens(); FB 2261
            db.DeleteMcuTokens();//FB 2261
		}
		//}				
		return true;
	}		

    internal bool AutoSyncLdap()
    {
        try
        {
            logger.Trace("Auto-sync ldap...");
            NS_DATABASE.Database db = new NS_DATABASE.Database(this.configParams);
            NS_MESSENGER.LdapSettings ldap = new NS_MESSENGER.LdapSettings();
            bool ret = db.FetchLdapSettings(ref ldap);
            if (!ret)
            {
                logger.Trace("Error fetching ldap settings.");
                return false;
            }

            if (ldap.scheduleDays == null)
            {
                logger.Trace("No LDAP schedule day/time specified for sync.");
                return false;
            }

            bool isWithinTimeRange = false;
            bool allowSync = false;

            // Check LDAP sync conditions  
            // --- ldap sync shouldn't have happened on the same day
            // --- calculate if current time is within time range 
            //TimeSpan sameDay = DateTime.UtcNow.Subtract(ldap.syncTime);

            // Maneesh Pujari FB 594 Date: 07/29/2008
            // Sync time is the time when the last ldap sync completed, so just making sure 
            // that the sync is not happening more than once if last sync was successful.
            if (DateTime.UtcNow.DayOfYear != ldap.syncTime.DayOfYear)  // prev sync cycle happened on a different day
            {
                //logger.Trace("Last sync happened on a different day. Diff  in days = " + sameDay.TotalDays.ToString());
                // first convert the scheduled time to utc time for correct calculations.
                // Maneesh Pujari FB FB 594 Date: 07/29/2008
				// maneesh pujari 08/04/2008 FB 649.
                TimeSpan timeRangeInMins = ldap.scheduleTime.Subtract(DateTime.UtcNow);
                // The time difference should not be negative(scheduled time should not be past)
                if ((timeRangeInMins.TotalMinutes > 0) && (timeRangeInMins.TotalMinutes <= 5))
                {
                    logger.Trace("Within ldap schedule time range of +5 or -5 mins.");
                    isWithinTimeRange = true;
                }
            }            


            // Time crunch now !!!
            // TODO: Revise the below code and put in switch-case block  & also revisit the usage of boolean flags later to make it more efficient. 

            // compare the day of week & time
            if (ldap.scheduleDays.Contains("1") && DateTime.Now.DayOfWeek == DayOfWeek.Monday && isWithinTimeRange )
            {
                // Monday 
                allowSync = true;
            }

            if (ldap.scheduleDays.Contains("2") && DateTime.Now.DayOfWeek == DayOfWeek.Tuesday && isWithinTimeRange)
            {
                // Tuesday 
                allowSync = true;
            }
            if (ldap.scheduleDays.Contains("3") && DateTime.Now.DayOfWeek == DayOfWeek.Wednesday && isWithinTimeRange)
            {
                // Wednesday
                allowSync = true;
            }
            if (ldap.scheduleDays.Contains("4") && DateTime.Now.DayOfWeek == DayOfWeek.Thursday && isWithinTimeRange)
            {
                // Thursday
                allowSync = true;
            }
            if (ldap.scheduleDays.Contains("5") && DateTime.Now.DayOfWeek == DayOfWeek.Friday && isWithinTimeRange)
            {
                // Friday
                allowSync = true;
            }
            if (ldap.scheduleDays.Contains("6") && DateTime.Now.DayOfWeek == DayOfWeek.Thursday && isWithinTimeRange)
            {
                // Saturday
                allowSync = true;
            }
            if (ldap.scheduleDays.Contains("7") && DateTime.Now.DayOfWeek == DayOfWeek.Sunday && isWithinTimeRange)
            {
                // Sunday
                allowSync = true;
            }

            if (allowSync)
            {
                // Do ldap sync
                NS_OPERATIONS.Operations ops = new Operations(this.configParams);
                bool ret1 = ops.SyncWithLdap();
                if (!ret1)
                {
                    logger.Trace("Auto sync ldap failed.");
                    return false;
                }
                return true;
            }
            else
            {
                logger.Trace("Not yet sync time.");
            }            
        
            return true;
        }
		catch (Exception e)
		{
			logger.Exception(100,e.Message);
			return false;
		}
    }

    
#if NOT_IN_USE
    /// <summary>
	/// Run scheduled reports.
	/// </summary>
	/// <returns></returns>
	internal bool RunScheduledReports()
	{
		try
		{
			// run reports every day between 3 - 5 AM
			logger.Trace("Checking wether local system clock time is between 3-5 AM...");
			if (DateTime.Now.Hour >= 3 && DateTime.Now.Hour <= 4)
			{
				logger.Trace("Invoking scheduled reports sub-system in ASPIL...");
				CallAspxSite rep = new CallAspxSite(this.configParams);
				rep.RunScheduledReports();
			}
				
			return true;
		}
		catch (Exception e)
		{
			logger.Exception(100,e.Message);
			return false;
		}
	}
		/// <summary>
	/// Check polycom mcu firmware clock
	/// </summary>
	/// <returns></returns>
	internal bool CheckPolycomMCUFirmwareClock()
	{
		logger.Trace ("Entering Polycom MCU Firmware Clock Sub-system...");

		// Fetch all mcus with timezone info & offset
		NS_DATABASE.Database db = new NS_DATABASE.Database(this.configParams);
		Queue mcuq = new Queue();
		bool ret = db.FetchAllMcu(ref mcuq);
		if (!ret)
		{
			logger.Trace ("Error fetching mcu list");
			return false;
		}

		NS_POLYCOM.Polycom accord = new NS_POLYCOM.Polycom(this.configParams);

		// Cycle thru each mcu 
		while (mcuq.Count > 0)
		{
			NS_MESSENGER.MCU mcu = new NS_MESSENGER.MCU();
			mcu = (NS_MESSENGER.MCU) mcuq.Dequeue();

			// skip mcu's below firmware 7.x
			if (mcu.etType == NS_MESSENGER.MCU.eType.ACCORDv7)
			{						
				accord.CheckMcuTime(mcu);
			}
		}
		return true;
	}

#endif
}	

/// <summary>
/// Client customizations
/// </summary>
class ClientCustomizations
{
	/// <summary>
	/// Public class attribute
	/// </summary>
	internal string errMsg = null;

	/// <summary>
	/// Private class attributes
	/// </summary>
	private NS_MESSENGER.ConfigParams configParams;
	private NS_LOGGER.Log logger;

	/// <summary>
	/// Constructor
	/// </summary>
	/// <param name="config"></param>
	internal ClientCustomizations (NS_MESSENGER.ConfigParams config)
	{	
		configParams = new NS_MESSENGER.ConfigParams();	
		configParams = config;
		logger = new NS_LOGGER.Log(configParams);
	}

    #region Client: VAOHIO
    /// <summary>
    /// Switch "conf.etVideosession" to "videoswitching" for specific bridges due to difference in hardware configuration.
    /// </summary>
    /// <param name="conf"></param>
    /// <returns></returns>
    internal bool VAOHIO_SwitchConferenceToVideoSwitching(ref NS_MESSENGER.Conference conf)
    {
        try
        {                        
            string bridgeName = conf.cMcu.sName.Trim();
            if (bridgeName.StartsWith("VISN")) //bridgeName.StartsWith("Cincin") ||bridgeName.StartsWith("Chilli")
            {
                conf.etVideoSession = NS_MESSENGER.Conference.eVideoSession.SWITCHING;
            }

            return true;
        }
        catch (Exception e)
        {
            this.errMsg = e.Message;
            logger.Exception(100, e.Message);
            return false;
        }
    }
    #endregion

    #region Client: PSU
    /// <summary>
    /// MCU video conference defaults 
    /// </summary>
    /// <param name="conf"></param>
    /// <returns></returns>
    internal bool PSU_SetMcuVideoConferenceDefaults(ref NS_MESSENGER.Conference conf)
    {
        try
        {
            // Set entry/end/exit tones to enabled
            conf.bEndTimeNotification = true;
            conf.bEntryNotification = true;
            conf.bExitNotification = true;

            // Set border color to Blue
            conf.stLayoutBorderColor.iBlue = 255;
            conf.stLayoutBorderColor.iGreen = 0;
            conf.stLayoutBorderColor.iRed = 0;

            // Set speaker color to White 
            conf.stSpeakerNotation.iBlue = 255;
            conf.stSpeakerNotation.iGreen = 255;
            conf.stSpeakerNotation.iRed = 255;

            //FB 1766 - Commented IVR settings
            // Set MsgServiceType to "IVR"
            //conf.etMessageServiceType = NS_MESSENGER.Conference.eMsgServiceType.IVR;

            // Set MsgServiceName to "PSU"
            //conf.sMessageServiceName = "PSU";

            // Entry Queue Access 
            conf.bEntryQueueAccess = true;

            return true;
        }
        catch (Exception e)
        {
            this.errMsg = e.Message;
            logger.Exception(100, e.Message);
            return false;
        }
    }
    #endregion

#if COMMENT
    #region Client: HKLaw
	/// <summary>
	/// HKLAW custom report - Daily conference report for each room
	/// </summary>
	/// <returns></returns>
	internal void HKLAW_DailyConfReportPerRoom()
	{
		try 
		{
			logger.Trace ("************************* Daily reports for room....**************************");

			// find all room ids
			NS_DATABASE.Database db = new NS_DATABASE.Database(configParams);
			Queue roomList = new Queue();
			bool ret = db.FetchAllRooms(ref roomList);
			if (!ret) 
			{
				logger.Trace("Error retreiving room list");				
			}
 			
			// fetch daily room schedule 
			Queue roomListBkup = new Queue();
			roomListBkup = roomList;
			int roomCount = roomList.Count;
			for (int i = 0 ; i < roomCount ; i++)
			{
				NS_MESSENGER_CUSTOMIZATIONS.Room room = new NS_MESSENGER_CUSTOMIZATIONS.Room();
				room = (NS_MESSENGER_CUSTOMIZATIONS.Room) roomList.Dequeue();
              
								
				ret = false;
				ret = db.FetchDailyConfSchedulePerRoom(ref room);
				if (!ret)
				{
					logger.Trace("Problem in retreiving the room schedule");
				}
				
				roomListBkup.Enqueue (room);
			}

			// save data in file 
			roomList = roomListBkup;
			roomCount = roomList.Count;
			for (int j = 0 ; j < roomCount ; j++)
			{
				try
				{
					
					NS_MESSENGER_CUSTOMIZATIONS.Room room = new NS_MESSENGER_CUSTOMIZATIONS.Room();
					room = (NS_MESSENGER_CUSTOMIZATIONS.Room) roomList.Dequeue();
               	
					// compare the room-id and get the file path
					//string roomFilePath = room.filePath;
					string roomFilePath = "C:\\Program Files\\Expedite VCS\\Reports\\myVRM\\"+ room.topTierName + "\\Daily\\" + room.name + ".html";

					logger.Trace("Room ID : " + room.roomID);
					logger.Trace ("Daily Report By Room FilePath : " + roomFilePath);
					
					StreamWriter sw,sw1 ;
					// delete the existing file
					if (File.Exists (roomFilePath))
					{						
						logger.Trace ("Deleting existing report file...");
                        try
                        {
                            File.Delete(roomFilePath);
                        }
                        catch (Exception)
                        {
                        }
					}
			
					// create a new file 					
					sw = File.CreateText(roomFilePath); 
					sw.Flush();
					sw.Close();

					sw1 = File.AppendText(roomFilePath);
				
					
					// add the report header
					sw1.WriteLine("<html><head><title>myVRM Conference Reports</title></head><body>");
					sw1.WriteLine ("<p><strong>DAILY ROOM REPORT</strong></p>");
					sw1.WriteLine ("<p><strong>ROOM NAME: " + room.name + "</strong></p>");
					logger.Trace ("Room Name : " + room.name);
					sw1.WriteLine ("<strong>DATE: " + DateTime.Now.ToString("d") + "</strong>");
					logger.Trace ("Report Date : " + DateTime.Now.ToString("d"));
					sw1.WriteLine ("<p>**************************</p>");

					// start appending conf records in the file
					int confCount = room.confList.Count;
					if (confCount < 1)
					{
						sw1.WriteLine ("No conferences scheduled.");
					}
					else 
					{
						// add the title row
						//string title3= "Date,UniqueID, StartTime,EndTime,UniqueID,ConferenceName";
						for (int k = 0; k < confCount ; k++)
						{
							NS_MESSENGER_CUSTOMIZATIONS.Conference conf = new NS_MESSENGER_CUSTOMIZATIONS.Conference();
							conf = (NS_MESSENGER_CUSTOMIZATIONS.Conference) room.confList.Dequeue();		
						
							// participant list
							string participantList = null;
							int participantCount = conf.participantList.Count;
							for (j=0;j< participantCount;j++)
							{
								participantList += (string) conf.participantList.Dequeue(); 
					
								if (j <= participantCount-1)
								{
									participantList += ";";
								}
							}
				
							// location list
							string locationList = null;
							int locationCount = conf.locationList.Count;
							for (j=0;j< locationCount;j++)
							{
								locationList += (string) conf.locationList.Dequeue(); 
					
								if (j <= locationCount-1)
								{
									locationList += "<BR>";
								}
							}
	
							// catering item list
							string cateringList = null ;
							int cateringCount = conf.cateringOrders.Count;
							for (j=0;j< cateringCount;j++)
							{
								cateringList += (string) conf.cateringOrders.Dequeue(); 
					
								if (j <= cateringCount-1)
								{
									cateringList += "<BR>";
								}
							}
	
							// resource item list
							string resourceList = null ;
							int resourceCount = conf.resourceOrders.Count;
							for (j=0;j< resourceCount;j++)
							{
								resourceList += (string) conf.resourceOrders.Dequeue(); 
					
								if (j <= resourceCount-1)
								{
									resourceList += "<BR>";
								}
							}
	
							// writing to file					
							/*							sw1.WriteLine("<P><TABLE id=\"Table1\" cellSpacing=\"0\" cellPadding=\"0\" width=\"75%\" border=\"0\">");
															sw1.WriteLine ("<TR><TD><P><STRONG>Date</STRONG></P></TD><TD><P><STRONG>UniqueID</STRONG></P></TD><TD><P><STRONG>Conference Name</STRONG></P></TD><TD><P><STRONG>Start Time</STRONG></P></TD><TD><P><STRONG>End Time</STRONG></P></TD></TR>");
															sw1.WriteLine ("<TR><TD>"+ conf.startTime.ToString("d")+"</TD><TD>"+ conf.uniqueID.ToString().Trim() +"</TD><TD>"+ conf.name.Trim() +"</TD><TD>" + conf.startTime.ToString("t")  + "</TD><TD>"+ conf.endTime.ToString("t")+ "</TD></TR>");
															sw1.WriteLine ("</TABLE></P>");

															sw1.WriteLine("<P><TABLE id=\"Table1\" cellSpacing=\"0\" cellPadding=\"0\" width=\"75%\" border=\"0\">");
															sw1.WriteLine ("<TR><TD><Strong>Locations</Strong></TD><TD><Strong>Participants</Strong></TD></TR>");				 						
															sw1.WriteLine ("<TR><TD>"+ locationList +"</TD><TD>"+ participantList +"</TD></TR>");				 						
															sw1.WriteLine ("</TABLE></P>");

															sw1.WriteLine("<P><TABLE id=\"Table1\" cellSpacing=\"0\" cellPadding=\"0\" width=\"75%\" border=\"0\">");
															sw1.WriteLine ("<TR><TD><Strong>Description</Strong></TD></TR>");				 						
															sw1.WriteLine ("<TR><TD>"+ conf.description +"</TD></TR>");				 						
															sw1.WriteLine ("</TABLE></P>");

															sw1.WriteLine("<P><TABLE id=\"Table1\" cellSpacing=\"0\" cellPadding=\"0\" width=\"75%\" border=\"0\">");
															sw1.WriteLine ("<TR><TD><Strong>Catering Orders</Strong></TD><TD><Strong>Resource Orders</Strong></TD></TR>");				 						
															sw1.WriteLine ("<TR><TD>"+ cateringList +"</TD><TD>"+ resourceList +"</TD></TR>");				 						
															sw1.WriteLine ("</TABLE></P>");
								*/
							sw1.WriteLine("<P><TABLE id=\"Table1\" cellSpacing=\"0\" cellPadding=\"0\" width=\"75%\" border=\"0\">");
							//sw1.WriteLine ("<TR><TD><P><STRONG>Date</STRONG></P></TD><TD><P><STRONG>UniqueID</STRONG></P></TD><TD><P><STRONG>Conference Name</STRONG></P></TD><TD><P><STRONG>Start Time</STRONG></P></TD><TD><P><STRONG>End Time</STRONG></P></TD><TD><P><STRONG>Timezone</STRONG></P></TD></TR>");
							//sw1.WriteLine ("<TR><TD>"+ conf.startTime.ToString("d")+"</TD><TD>"+ conf.uniqueID.ToString().Trim() +"</TD><TD>"+ conf.name.Trim() +"</TD><TD>" + conf.startTime.ToString("t")  + "</TD><TD>"+ conf.endTime.ToString("t")+ "</TD><TD>Eastern Time</TD></TR>");
							sw1.WriteLine ("<TR><TD><P><STRONG>Conference Name</STRONG></P></TD><TD><P><STRONG>UniqueID</STRONG></P></TD></TR>");
							sw1.WriteLine ("<TR><TD>"+ conf.name.Trim()+"</TD><TD>"+ conf.uniqueID.ToString().Trim() +"</TD></TR>");
							sw1.WriteLine ("</TABLE></P>");

							sw1.WriteLine("<P><TABLE id=\"Table1\" cellSpacing=\"1\" cellPadding=\"0\" width=\"75%\" border=\"0\">");
							//sw1.WriteLine ("<TR><TD><Strong>Locations</Strong></TD><TD><Strong>Participants</Strong></TD></TR>");				 						
							//sw1.WriteLine ("<TR><TD>"+ locationList +"</TD><TD>"+ participantList +"</TD></TR>");				 						
							sw1.WriteLine ("<TR><TD><Strong>Locations</Strong></TD><TD><Strong>Date</Strong></TD><TD><Strong>Start</Strong></TD><TD><Strong>End</Strong></TD><TD><Strong>Timezone</Strong></TD></TR>");				 						
							sw1.WriteLine ("<TR><TD>"+ locationList +"</TD><TD>"+ conf.startTime.ToString("d") +"</TD><TD>" + conf.startTime.ToString("t") +"</TD><TD>"+ conf.endTime.ToString("t")+"</TD><TD>" + room.timezoneID_Name + "</TD></TR>");				 						
							sw1.WriteLine ("</TABLE></P>");

							sw1.WriteLine("<P><TABLE id=\"Table1\" cellSpacing=\"0\" cellPadding=\"0\" width=\"75%\" border=\"0\">");
							sw1.WriteLine ("<TR><TD><Strong>Description</Strong></TD></TR>");				 						
						
							// Changed as per Angela's request of 1/11/07 - add a line break in desc field for a comma.
							conf.description = conf.description.Replace(",","<BR>");
						
							sw1.WriteLine ("<TR><TD>"+ conf.description +"</TD></TR>");				 						
							sw1.WriteLine ("</TABLE></P>");

							sw1.WriteLine("<P><TABLE id=\"Table1\" cellSpacing=\"0\" cellPadding=\"0\" width=\"75%\" border=\"0\">");
							//sw1.WriteLine ("<TR><TD><Strong>Catering Orders</Strong></TD><TD><Strong>Resource Orders</Strong></TD></TR>");				 						
							//sw1.WriteLine ("<TR><TD>"+ cateringList +"</TD><TD>"+ resourceList +"</TD></TR>");				 						
							sw1.WriteLine ("<TR><TD><Strong>Participants</Strong></TD></TR>");
							sw1.WriteLine ("<TR><TD>"+ participantList +"</TD></TR>");				 						
							sw1.WriteLine ("<TR><TD><Strong>Catering Orders</Strong></TD></TR>");				 						
							sw1.WriteLine ("<TR><TD>"+ cateringList +"</TD></TR>");
							sw1.WriteLine ("<TR><TD><Strong>Resource Orders</Strong></TD></TR>");				 						
							sw1.WriteLine ("<TR><TD>"+ resourceList +"</TD></TR>");
							sw1.WriteLine ("</TABLE></P>");

							sw1.WriteLine ("------------------------------------------------------------------------------------------------------------");
						}
					}
				
					sw1.WriteLine ("</TABLE></P></body></html>");
					sw1.Flush();
					sw1.Close();
				}
				catch (Exception e)
				{
					logger.Trace (e.Message);
					continue;
				}
			} //for loop
		}
		catch (Exception e)
		{
            logger.Exception(100, "Exception generated : Message = " + e.Message + " , StackTrace = " + e.StackTrace);			
		}
	}


	/// <summary>
	/// HKLAW custom report - Weekly conference report for each tier2 location entity
	/// </summary>
	/// <returns></returns>
    internal void HKLAW_WeeklyConfReportByTier2()
	{
		try 
		{
			logger.Trace ("*************************** Weekly reports by tier....*************************");

			NS_DATABASE.Database db = new NS_DATABASE.Database(configParams);
			
			// get the tier-2 list
			Queue tier2List = new Queue();
			bool ret = db.FetchTier2List(ref tier2List);
			if (!ret)
			{
				logger.Trace("Error fetching tier-2 list");			
			}

			// go thru tier-2 list one-by-one
			int tier2ListCount = tier2List.Count;
			for (int tier2Id =0; tier2Id < tier2ListCount ; tier2Id++)
			{
				NS_MESSENGER_CUSTOMIZATIONS.Tier2 tier2 = new NS_MESSENGER_CUSTOMIZATIONS.Tier2();
				tier2 = (NS_MESSENGER_CUSTOMIZATIONS.Tier2) tier2List.Dequeue();

				// get all conf's data
				Queue confList = new Queue();
				ret = db.WeeklyConfsByTier(tier2.id,tier2.timezoneId, ref confList);
				if (!ret)
				{
					logger.Trace("Error in retreiving all confs");
					//return false;
				}
				
				// save data in file
				string tier2FilePath = tier2.filePath;

				// delete the existing file
				if (File.Exists(tier2FilePath))
				{	
					logger.Trace ("Deleting existing report file...");
                    try
                    {
                        File.Delete(tier2FilePath);
                    }
                    catch (Exception)
                    {
                    }
				}

				// create a new file 
				StreamWriter sw ;
				sw = File.CreateText(tier2FilePath); 
				sw.Flush();
				sw.Close();

				// add the report header
				StreamWriter sw1 ;
				sw1 = File.AppendText(tier2FilePath);

				// report header
				sw1.WriteLine("<html><head><title>myVRM Conference Reports</title></head><body>");
				sw1.WriteLine ("<p><strong>WEEKLY CONFERENCE REPORT</strong></p>");
				sw1.WriteLine ("<p><strong>TIER-2 NAME: " + tier2.name + "</strong></p>");
				sw1.WriteLine ("<p><strong>WEEK OF : " + DateTime.Now.ToString("d") + " - " + DateTime.Now.AddDays(7).ToString("d") + "</p></strong>");
				sw1.WriteLine ("<p>************************************************</p>");

				// append the conf records
				int confCount = confList.Count;
				if (confCount == 0)
				{
					sw1.WriteLine (" No conferences scheduled.");
				}
				else 
				{
					// add the title row
					for (int i= 0; i< confCount ; i++)
					{
						NS_MESSENGER_CUSTOMIZATIONS.Conference conf = new NS_MESSENGER_CUSTOMIZATIONS.Conference();
						conf = (NS_MESSENGER_CUSTOMIZATIONS.Conference) confList.Dequeue();
				
						// participant list
						string participantList = null;
						int participantCount = conf.participantList.Count;
						for (int j=0;j< participantCount;j++)
						{
							participantList += (string) conf.participantList.Dequeue(); 
					
							if (j <= participantCount-1)
							{
								participantList += ";";
							}
						}
				
						// location list
						string locationList = null;
						int locationCount = conf.locationList.Count;
						for (int j=0;j< locationCount;j++)
						{
							locationList += (string) conf.locationList.Dequeue(); 
					
							if (j <= locationCount-1)
							{
								locationList += "<BR>";
							}
						}
	
						// catering item list
						string cateringList = null ;
						int cateringCount = conf.cateringOrders.Count;
						for (int j=0;j< cateringCount;j++)
						{
							cateringList += (string) conf.cateringOrders.Dequeue(); 
					
							if (j <= cateringCount-1)
							{
								cateringList += "<BR>";
							}
						}
	
						// resource item list
						string resourceList = null ;
						int resourceCount = conf.resourceOrders.Count;
						for (int j=0;j< resourceCount;j++)
						{
							resourceList += (string) conf.resourceOrders.Dequeue(); 
					
							if (j <= resourceCount-1)
							{
								resourceList += "<BR>";
							}
						}

						//string confRecord = conf.startDate.ToString("d") + "," + conf.startTime.ToString("t") + "," + conf.endTime.ToString("t") + "," + conf.uniqueID.ToString() + "," + conf.name + "," + conf.description + ",";
						//confRecord += participantList + "," + locationList + "," + cateringList +"," + resourceList;

						// writing to file					
						/*							sw1.WriteLine("<P><TABLE id=\"Table1\" cellSpacing=\"0\" cellPadding=\"0\" width=\"75%\" border=\"0\">");
													sw1.WriteLine ("<TR><TD><P><STRONG>Date</STRONG></P></TD><TD><P><STRONG>UniqueID</STRONG></P></TD><TD><P><STRONG>Conference Name</STRONG></P></TD><TD><P><STRONG>Start Time</STRONG></P></TD><TD><P><STRONG>End Time</STRONG></P></TD><TD><P><STRONG>Timezone</STRONG></P></TD></TR>");
													sw1.WriteLine ("<TR><TD>"+ conf.startTime.ToString("d")+"</TD><TD>"+ conf.uniqueID.ToString().Trim() +"</TD><TD>"+ conf.name.Trim() +"</TD><TD>" + conf.startTime.ToString("t")  + "</TD><TD>"+ conf.endTime.ToString("t")+ "</TD><TD>Eastern Time</TD></TR>");
													sw1.WriteLine ("</TABLE></P>");

													sw1.WriteLine("<P><TABLE id=\"Table1\" cellSpacing=\"0\" cellPadding=\"0\" width=\"75%\" border=\"0\">");
													sw1.WriteLine ("<TR><TD><Strong>Locations</Strong></TD><TD><Strong>Participants</Strong></TD></TR>");				 						
													sw1.WriteLine ("<TR><TD>"+ locationList +"</TD><TD>"+ participantList +"</TD></TR>");				 						
													sw1.WriteLine ("</TABLE></P>");

													sw1.WriteLine("<P><TABLE id=\"Table1\" cellSpacing=\"0\" cellPadding=\"0\" width=\"75%\" border=\"0\">");
													sw1.WriteLine ("<TR><TD><Strong>Description</Strong></TD></TR>");				 						
													sw1.WriteLine ("<TR><TD>"+ conf.description +"</TD></TR>");				 						
													sw1.WriteLine ("</TABLE></P>");

													sw1.WriteLine("<P><TABLE id=\"Table1\" cellSpacing=\"0\" cellPadding=\"0\" width=\"75%\" border=\"0\">");
													sw1.WriteLine ("<TR><TD><Strong>Catering Orders</Strong></TD><TD><Strong>Resource Orders</Strong></TD></TR>");				 						
													sw1.WriteLine ("<TR><TD>"+ cateringList +"</TD><TD>"+ resourceList +"</TD></TR>");				 						
													sw1.WriteLine ("</TABLE></P>");
						*/
						sw1.WriteLine("<P><TABLE id=\"Table1\" cellSpacing=\"0\" cellPadding=\"0\" width=\"75%\" border=\"0\">");
						//sw1.WriteLine ("<TR><TD><P><STRONG>Date</STRONG></P></TD><TD><P><STRONG>UniqueID</STRONG></P></TD><TD><P><STRONG>Conference Name</STRONG></P></TD><TD><P><STRONG>Start Time</STRONG></P></TD><TD><P><STRONG>End Time</STRONG></P></TD><TD><P><STRONG>Timezone</STRONG></P></TD></TR>");
						//sw1.WriteLine ("<TR><TD>"+ conf.startTime.ToString("d")+"</TD><TD>"+ conf.uniqueID.ToString().Trim() +"</TD><TD>"+ conf.name.Trim() +"</TD><TD>" + conf.startTime.ToString("t")  + "</TD><TD>"+ conf.endTime.ToString("t")+ "</TD><TD>Eastern Time</TD></TR>");
						sw1.WriteLine ("<TR><TD><P><STRONG>Conference Name</STRONG></P></TD><TD><P><STRONG>UniqueID</STRONG></P></TD></TR>");
						sw1.WriteLine ("<TR><TD>"+ conf.name.Trim()+"</TD><TD>"+ conf.uniqueID.ToString().Trim() +"</TD></TR>");
						sw1.WriteLine ("</TABLE></P>");

						sw1.WriteLine("<P><TABLE id=\"Table1\" cellSpacing=\"1\" cellPadding=\"0\" width=\"75%\" border=\"0\">");
						//sw1.WriteLine ("<TR><TD><Strong>Locations</Strong></TD><TD><Strong>Participants</Strong></TD></TR>");				 						
						//sw1.WriteLine ("<TR><TD>"+ locationList +"</TD><TD>"+ participantList +"</TD></TR>");				 						
						sw1.WriteLine ("<TR><TD><Strong>Locations</Strong></TD><TD><Strong>Date</Strong></TD><TD><Strong>Start</Strong></TD><TD><Strong>End</Strong></TD><TD><Strong>Timezone</Strong></TD></TR>");				 						
						sw1.WriteLine ("<TR><TD>"+ locationList +"</TD><TD>"+ conf.startTime.ToString("d") +"</TD><TD>" + conf.startTime.ToString("t") +"</TD><TD>"+ conf.endTime.ToString("t")+"</TD><TD>"+ tier2.timezoneId_Name +"</TD></TR>");				 						
						sw1.WriteLine ("</TABLE></P>");

						sw1.WriteLine("<P><TABLE id=\"Table1\" cellSpacing=\"0\" cellPadding=\"0\" width=\"75%\" border=\"0\">");
						sw1.WriteLine ("<TR><TD><Strong>Description</Strong></TD></TR>");				 						
							
						// Changed as per Angela's request of 1/11/07 - add a line break in desc field for a comma.
						conf.description = conf.description.Replace(",","<BR>");
						
						sw1.WriteLine ("<TR><TD>"+ conf.description +"</TD></TR>");				 						
						sw1.WriteLine ("</TABLE></P>");

						sw1.WriteLine("<P><TABLE id=\"Table1\" cellSpacing=\"0\" cellPadding=\"0\" width=\"75%\" border=\"0\">");
						//sw1.WriteLine ("<TR><TD><Strong>Catering Orders</Strong></TD><TD><Strong>Resource Orders</Strong></TD></TR>");				 						
						//sw1.WriteLine ("<TR><TD>"+ cateringList +"</TD><TD>"+ resourceList +"</TD></TR>");				 						
						sw1.WriteLine ("<TR><TD><Strong>Participants</Strong></TD></TR>");
						sw1.WriteLine ("<TR><TD>"+ participantList +"</TD></TR>");				 						
						sw1.WriteLine ("<TR><TD><Strong>Catering Orders</Strong></TD></TR>");				 						
						sw1.WriteLine ("<TR><TD>"+ cateringList +"</TD></TR>");
						sw1.WriteLine ("<TR><TD><Strong>Resource Orders</Strong></TD></TR>");				 						
						sw1.WriteLine ("<TR><TD>"+ resourceList +"</TD></TR>");
						sw1.WriteLine ("</TABLE></P>");

						sw1.WriteLine ("------------------------------------------------------------------------------------------------------------");
										
					}
				}
				sw1.WriteLine ("</TABLE></P></body></html>");
				sw1.Flush();
				sw1.Close();
			}
		}
		catch (Exception e)
		{
            logger.Exception(100, "Exception generated : Message = " + e.Message + " , StackTrace = " + e.StackTrace);			
		}
	}
				

	/// <summary>
	/// HKLAW custom report - Daily conference report for each tier2 location entity
	/// </summary>
	/// <returns></returns>
    internal void HKLAW_DailyConfReportByTier2()
	{
		try 
		{
			// These reports have the same format as "WeeklyConfReportByTier2".
			logger.Trace ("**************** Daily reports by tier.... ***************");

			NS_DATABASE.Database db = new NS_DATABASE.Database(configParams);
			
			// get the tier-2 list
			Queue tier2List = new Queue();
			bool ret = db.FetchTier2List(ref tier2List);
			if (!ret)
			{
				logger.Trace("Error fetching tier-2 list");
				//return false;
			}

			// go thru tier-2 list one-by-one
			int tier2ListCount = tier2List.Count;
			for (int tier2Id =0; tier2Id < tier2ListCount ; tier2Id++)
			{
				NS_MESSENGER_CUSTOMIZATIONS.Tier2 tier2 = new NS_MESSENGER_CUSTOMIZATIONS.Tier2();
				tier2 = (NS_MESSENGER_CUSTOMIZATIONS.Tier2) tier2List.Dequeue();

				// get all conf's data
				Queue confList = new Queue();
				ret = db.DailyConfsByTier(tier2.id,tier2.timezoneId, ref confList);
				if (!ret)
				{
					logger.Trace("Error in retreiving all confs");
					//return false;
				}
				
				// save data in file
				string tier2FilePath = tier2.filePath.Replace("weeklyreport.html","dailytier2report.html");					
					
				// delete the existing file
				if (File.Exists(tier2FilePath))
				{	
					logger.Trace ("Deleting existing report file...");
                    try
                    {
                        File.Delete(tier2FilePath);
                    }
                    catch (Exception)
                    {
                    }
				}

				// create a new file 
				StreamWriter sw ;
				sw = File.CreateText(tier2FilePath); 
				sw.Flush();
				sw.Close();

				// add the report header
				StreamWriter sw1 ;
				sw1 = File.AppendText(tier2FilePath);

				// report header
				sw1.WriteLine("<html><head><title>myVRM Conference Reports</title></head><body>");
				sw1.WriteLine ("<p><strong>DAILY TIER-2 CONFERENCE REPORT</strong></p>");
				sw1.WriteLine ("<p><strong>TIER-2 NAME: " + tier2.name + "</strong></p>");
				sw1.WriteLine ("<p><strong>DATE : " + DateTime.Now.ToString("d") + "</p></strong>");
				sw1.WriteLine ("<p>************************************************</p>");

				// append the conf records
				int confCount = confList.Count;
				if (confCount == 0)
				{
					sw1.WriteLine (" No conferences scheduled.");
				}
				else 
				{
					// add the title row
					for (int i= 0; i< confCount ; i++)
					{
						NS_MESSENGER_CUSTOMIZATIONS.Conference conf = new NS_MESSENGER_CUSTOMIZATIONS.Conference();
						conf = (NS_MESSENGER_CUSTOMIZATIONS.Conference) confList.Dequeue();
				
						// participant list
						string participantList = null;
						int participantCount = conf.participantList.Count;
						for (int j=0;j< participantCount;j++)
						{
							participantList += (string) conf.participantList.Dequeue(); 
					
							if (j <= participantCount-1)
							{
								participantList += ";";
							}
						}
				
						// location list
						string locationList = null;
						int locationCount = conf.locationList.Count;
						for (int j=0;j< locationCount;j++)
						{
							locationList += (string) conf.locationList.Dequeue(); 
					
							if (j <= locationCount-1)
							{
								locationList += "<BR>";
							}
						}
	
						// catering item list
						string cateringList = null ;
						int cateringCount = conf.cateringOrders.Count;
						for (int j=0;j< cateringCount;j++)
						{
							cateringList += (string) conf.cateringOrders.Dequeue(); 
					
							if (j <= cateringCount-1)
							{
								cateringList += "<BR>";
							}
						}
	
						// resource item list
						string resourceList = null ;
						int resourceCount = conf.resourceOrders.Count;
						for (int j=0;j< resourceCount;j++)
						{
							resourceList += (string) conf.resourceOrders.Dequeue(); 
					
							if (j <= resourceCount-1)
							{
								resourceList += "<BR>";
							}
						}

						//string confRecord = conf.startDate.ToString("d") + "," + conf.startTime.ToString("t") + "," + conf.endTime.ToString("t") + "," + conf.uniqueID.ToString() + "," + conf.name + "," + conf.description + ",";
						//confRecord += participantList + "," + locationList + "," + cateringList +"," + resourceList;

						// writing to file					
						/*							sw1.WriteLine("<P><TABLE id=\"Table1\" cellSpacing=\"0\" cellPadding=\"0\" width=\"75%\" border=\"0\">");
														sw1.WriteLine ("<TR><TD><P><STRONG>Date</STRONG></P></TD><TD><P><STRONG>UniqueID</STRONG></P></TD><TD><P><STRONG>Conference Name</STRONG></P></TD><TD><P><STRONG>Start Time</STRONG></P></TD><TD><P><STRONG>End Time</STRONG></P></TD><TD><P><STRONG>Timezone</STRONG></P></TD></TR>");
														sw1.WriteLine ("<TR><TD>"+ conf.startTime.ToString("d")+"</TD><TD>"+ conf.uniqueID.ToString().Trim() +"</TD><TD>"+ conf.name.Trim() +"</TD><TD>" + conf.startTime.ToString("t")  + "</TD><TD>"+ conf.endTime.ToString("t")+ "</TD><TD>Eastern Time</TD></TR>");
														sw1.WriteLine ("</TABLE></P>");

														sw1.WriteLine("<P><TABLE id=\"Table1\" cellSpacing=\"0\" cellPadding=\"0\" width=\"75%\" border=\"0\">");
														sw1.WriteLine ("<TR><TD><Strong>Locations</Strong></TD><TD><Strong>Participants</Strong></TD></TR>");				 						
														sw1.WriteLine ("<TR><TD>"+ locationList +"</TD><TD>"+ participantList +"</TD></TR>");				 						
														sw1.WriteLine ("</TABLE></P>");

														sw1.WriteLine("<P><TABLE id=\"Table1\" cellSpacing=\"0\" cellPadding=\"0\" width=\"75%\" border=\"0\">");
														sw1.WriteLine ("<TR><TD><Strong>Description</Strong></TD></TR>");				 						
														sw1.WriteLine ("<TR><TD>"+ conf.description +"</TD></TR>");				 						
														sw1.WriteLine ("</TABLE></P>");

														sw1.WriteLine("<P><TABLE id=\"Table1\" cellSpacing=\"0\" cellPadding=\"0\" width=\"75%\" border=\"0\">");
														sw1.WriteLine ("<TR><TD><Strong>Catering Orders</Strong></TD><TD><Strong>Resource Orders</Strong></TD></TR>");				 						
														sw1.WriteLine ("<TR><TD>"+ cateringList +"</TD><TD>"+ resourceList +"</TD></TR>");				 						
														sw1.WriteLine ("</TABLE></P>");
							*/
						sw1.WriteLine("<P><TABLE id=\"Table1\" cellSpacing=\"0\" cellPadding=\"0\" width=\"75%\" border=\"0\">");
						//sw1.WriteLine ("<TR><TD><P><STRONG>Date</STRONG></P></TD><TD><P><STRONG>UniqueID</STRONG></P></TD><TD><P><STRONG>Conference Name</STRONG></P></TD><TD><P><STRONG>Start Time</STRONG></P></TD><TD><P><STRONG>End Time</STRONG></P></TD><TD><P><STRONG>Timezone</STRONG></P></TD></TR>");
						//sw1.WriteLine ("<TR><TD>"+ conf.startTime.ToString("d")+"</TD><TD>"+ conf.uniqueID.ToString().Trim() +"</TD><TD>"+ conf.name.Trim() +"</TD><TD>" + conf.startTime.ToString("t")  + "</TD><TD>"+ conf.endTime.ToString("t")+ "</TD><TD>Eastern Time</TD></TR>");
						sw1.WriteLine ("<TR><TD><P><STRONG>Conference Name</STRONG></P></TD><TD><P><STRONG>UniqueID</STRONG></P></TD></TR>");
						sw1.WriteLine ("<TR><TD>"+ conf.name.Trim()+"</TD><TD>"+ conf.uniqueID.ToString().Trim() +"</TD></TR>");
						sw1.WriteLine ("</TABLE></P>");

						sw1.WriteLine("<P><TABLE id=\"Table1\" cellSpacing=\"1\" cellPadding=\"0\" width=\"75%\" border=\"0\">");
						//sw1.WriteLine ("<TR><TD><Strong>Locations</Strong></TD><TD><Strong>Participants</Strong></TD></TR>");				 						
						//sw1.WriteLine ("<TR><TD>"+ locationList +"</TD><TD>"+ participantList +"</TD></TR>");				 						
						sw1.WriteLine ("<TR><TD><Strong>Locations</Strong></TD><TD><Strong>Date</Strong></TD><TD><Strong>Start</Strong></TD><TD><Strong>End</Strong></TD><TD><Strong>Timezone</Strong></TD></TR>");				 						
						sw1.WriteLine ("<TR><TD>"+ locationList +"</TD><TD>"+ conf.startTime.ToString("d") +"</TD><TD>" + conf.startTime.ToString("t") +"</TD><TD>"+ conf.endTime.ToString("t")+"</TD><TD>"+ tier2.timezoneId_Name +"</TD></TR>");				 						
						sw1.WriteLine ("</TABLE></P>");

						sw1.WriteLine("<P><TABLE id=\"Table1\" cellSpacing=\"0\" cellPadding=\"0\" width=\"75%\" border=\"0\">");
						sw1.WriteLine ("<TR><TD><Strong>Description</Strong></TD></TR>");				 						
							
						// Changed as per Angela's request of 1/11/07 - add a line break in desc field for a comma.
						conf.description = conf.description.Replace(",","<BR>");
						
						sw1.WriteLine ("<TR><TD>"+ conf.description +"</TD></TR>");				 						
						sw1.WriteLine ("</TABLE></P>");

						sw1.WriteLine("<P><TABLE id=\"Table1\" cellSpacing=\"0\" cellPadding=\"0\" width=\"75%\" border=\"0\">");
						//sw1.WriteLine ("<TR><TD><Strong>Catering Orders</Strong></TD><TD><Strong>Resource Orders</Strong></TD></TR>");				 						
						//sw1.WriteLine ("<TR><TD>"+ cateringList +"</TD><TD>"+ resourceList +"</TD></TR>");				 						
						sw1.WriteLine ("<TR><TD><Strong>Participants</Strong></TD></TR>");
						sw1.WriteLine ("<TR><TD>"+ participantList +"</TD></TR>");				 						
						sw1.WriteLine ("<TR><TD><Strong>Catering Orders</Strong></TD></TR>");				 						
						sw1.WriteLine ("<TR><TD>"+ cateringList +"</TD></TR>");
						sw1.WriteLine ("<TR><TD><Strong>Resource Orders</Strong></TD></TR>");				 						
						sw1.WriteLine ("<TR><TD>"+ resourceList +"</TD></TR>");
						sw1.WriteLine ("</TABLE></P>");

						sw1.WriteLine ("------------------------------------------------------------------------------------------------------------");
										
					}
				}
				sw1.WriteLine ("</TABLE></P></body></html>");
				sw1.Flush();
				sw1.Close();
			}
			
		}
		catch (Exception e)
		{			
            logger.Exception(100, "Exception generated : Message = " + e.Message + " , StackTrace = " + e.StackTrace);
		}
	}
		

	/// <summary>
	/// HKLAW custom report - Daily conference report for each tier2 (different view than prev one)
	/// </summary>
	/// <returns></returns>
    internal void HKLAW_DailyLocnReportByTier2()
	{
		try 
		{
			// These reports have the same format as "WeeklyConfReportByTier2".
			logger.Trace ("**************** Daily location reports by tier.... ***************");

			NS_DATABASE.Database db = new NS_DATABASE.Database(configParams);
			
			// get the tier-2 list
			Queue tier2List = new Queue();
			bool ret = db.FetchTier2List(ref tier2List);
			if (!ret)
			{
				logger.Trace("Error fetching tier-2 list");
				//return false;
			}

			// go thru tier-2 list one-by-one
			int tier2ListCount = tier2List.Count;
			for (int tier2Id =0; tier2Id < tier2ListCount ; tier2Id++)
			{
				NS_MESSENGER_CUSTOMIZATIONS.Tier2 tier2 = new NS_MESSENGER_CUSTOMIZATIONS.Tier2();
				tier2 = (NS_MESSENGER_CUSTOMIZATIONS.Tier2) tier2List.Dequeue();

				// get all conf's data
				Queue confList = new Queue();
				ret = db.DailyConfsByTier(tier2.id,tier2.timezoneId, ref confList);
				if (!ret)
				{
					logger.Trace("Error in retreiving all confs");
					//return false;
				}
				
				// save data in file
				string tier2FilePath = tier2.filePath.Replace("weeklyreport.html","dailyreport.html");					
					
				// delete the existing file
				if (File.Exists(tier2FilePath))
				{	
					logger.Trace ("Deleting existing report file...");
                    try
                    {
                        File.Delete(tier2FilePath);
                    }
                    catch (Exception)
                    {

                    }
				}

				// create a new file 
				StreamWriter sw ;
				sw = File.CreateText(tier2FilePath); 
				sw.Flush();
				sw.Close();

				// add the report header
				StreamWriter sw1 ;
				sw1 = File.AppendText(tier2FilePath);

				// report header
				sw1.WriteLine("<html><head><title>myVRM Conference Reports</title></head><body>");
				sw1.WriteLine ("<p><strong>DAILY TIER-2 CONFERENCE REPORT</strong></p>");
				sw1.WriteLine ("<p><strong>TIER-2 NAME: " + tier2.name + "</strong></p>");
				sw1.WriteLine ("<p><strong>DATE : " + DateTime.Now.ToString("d") + "</p></strong>");
				sw1.WriteLine ("<p>************************************************</p>");

				// append the conf records
				int confCount = confList.Count;
				if (confCount == 0)
				{
					sw1.WriteLine (" No conferences scheduled.");
				}
				else 
				{
					// add the title row
					sw1.WriteLine("<P><TABLE id=\"Table1\" cellSpacing=\"0\" cellPadding=\"0\" width=\"75%\" border=\"0\">");
					sw1.WriteLine ("<TR><TD><P><STRONG>Location</STRONG></P></TD><TD><P><STRONG>Start Time</STRONG></P></TD><TD><P><STRONG>End Time</STRONG></P></TD><TD><P><STRONG>Conference Name</STRONG></P></TD></TR>");
							
					for (int i= 0; i< confCount ; i++)
					{
						NS_MESSENGER_CUSTOMIZATIONS.Conference conf = new NS_MESSENGER_CUSTOMIZATIONS.Conference();
						conf = (NS_MESSENGER_CUSTOMIZATIONS.Conference) confList.Dequeue();
				
						// participant list
						string participantList = null;
						int participantCount = conf.participantList.Count;
						for (int j=0;j< participantCount;j++)
						{
							participantList += (string) conf.participantList.Dequeue(); 
					
							if (j <= participantCount-1)
							{
								participantList += ";";
							}
						}
				
						// location list
						string locationList = null;
						int locationCount = conf.locationList.Count;
						for (int j=0;j< locationCount;j++)
						{
							locationList += (string) conf.locationList.Dequeue(); 
					
							if (j <= locationCount-1)
							{
								locationList += "<BR>";
							}
						}
	
						// catering item list
						string cateringList = null ;
						int cateringCount = conf.cateringOrders.Count;
						for (int j=0;j< cateringCount;j++)
						{
							cateringList += (string) conf.cateringOrders.Dequeue(); 
					
							if (j <= cateringCount-1)
							{
								cateringList += "<BR>";
							}
						}
	
						// resource item list
						string resourceList = null ;
						int resourceCount = conf.resourceOrders.Count;
						for (int j=0;j< resourceCount;j++)
						{
							resourceList += (string) conf.resourceOrders.Dequeue(); 
					
							if (j <= resourceCount-1)
							{
								resourceList += "<BR>";
							}
						}

						//string confRecord = conf.startDate.ToString("d") + "," + conf.startTime.ToString("t") + "," + conf.endTime.ToString("t") + "," + conf.uniqueID.ToString() + "," + conf.name + "," + conf.description + ",";
						//confRecord += participantList + "," + locationList + "," + cateringList +"," + resourceList;

						// writing to file					
						/*							sw1.WriteLine("<P><TABLE id=\"Table1\" cellSpacing=\"0\" cellPadding=\"0\" width=\"75%\" border=\"0\">");
														sw1.WriteLine ("<TR><TD><P><STRONG>Date</STRONG></P></TD><TD><P><STRONG>UniqueID</STRONG></P></TD><TD><P><STRONG>Conference Name</STRONG></P></TD><TD><P><STRONG>Start Time</STRONG></P></TD><TD><P><STRONG>End Time</STRONG></P></TD><TD><P><STRONG>Timezone</STRONG></P></TD></TR>");
														sw1.WriteLine ("<TR><TD>"+ conf.startTime.ToString("d")+"</TD><TD>"+ conf.uniqueID.ToString().Trim() +"</TD><TD>"+ conf.name.Trim() +"</TD><TD>" + conf.startTime.ToString("t")  + "</TD><TD>"+ conf.endTime.ToString("t")+ "</TD><TD>Eastern Time</TD></TR>");
														sw1.WriteLine ("</TABLE></P>");

														sw1.WriteLine("<P><TABLE id=\"Table1\" cellSpacing=\"0\" cellPadding=\"0\" width=\"75%\" border=\"0\">");
														sw1.WriteLine ("<TR><TD><Strong>Locations</Strong></TD><TD><Strong>Participants</Strong></TD></TR>");				 						
														sw1.WriteLine ("<TR><TD>"+ locationList +"</TD><TD>"+ participantList +"</TD></TR>");				 						
														sw1.WriteLine ("</TABLE></P>");

														sw1.WriteLine("<P><TABLE id=\"Table1\" cellSpacing=\"0\" cellPadding=\"0\" width=\"75%\" border=\"0\">");
														sw1.WriteLine ("<TR><TD><Strong>Description</Strong></TD></TR>");				 						
														sw1.WriteLine ("<TR><TD>"+ conf.description +"</TD></TR>");				 						
														sw1.WriteLine ("</TABLE></P>");

														sw1.WriteLine("<P><TABLE id=\"Table1\" cellSpacing=\"0\" cellPadding=\"0\" width=\"75%\" border=\"0\">");
														sw1.WriteLine ("<TR><TD><Strong>Catering Orders</Strong></TD><TD><Strong>Resource Orders</Strong></TD></TR>");				 						
														sw1.WriteLine ("<TR><TD>"+ cateringList +"</TD><TD>"+ resourceList +"</TD></TR>");				 						
														sw1.WriteLine ("</TABLE></P>");
							
							sw1.WriteLine("<P><TABLE id=\"Table1\" cellSpacing=\"0\" cellPadding=\"0\" width=\"75%\" border=\"0\">");
							//sw1.WriteLine ("<TR><TD><P><STRONG>Date</STRONG></P></TD><TD><P><STRONG>UniqueID</STRONG></P></TD><TD><P><STRONG>Conference Name</STRONG></P></TD><TD><P><STRONG>Start Time</STRONG></P></TD><TD><P><STRONG>End Time</STRONG></P></TD><TD><P><STRONG>Timezone</STRONG></P></TD></TR>");
							//sw1.WriteLine ("<TR><TD>"+ conf.startTime.ToString("d")+"</TD><TD>"+ conf.uniqueID.ToString().Trim() +"</TD><TD>"+ conf.name.Trim() +"</TD><TD>" + conf.startTime.ToString("t")  + "</TD><TD>"+ conf.endTime.ToString("t")+ "</TD><TD>Eastern Time</TD></TR>");
							sw1.WriteLine ("<TR><TD><P><STRONG>Conference Name</STRONG></P></TD><TD><P><STRONG>UniqueID</STRONG></P></TD></TR>");
							sw1.WriteLine ("<TR><TD>"+ conf.name.Trim()+"</TD><TD>"+ conf.uniqueID.ToString().Trim() +"</TD></TR>");
							sw1.WriteLine ("</TABLE></P>");

							sw1.WriteLine("<P><TABLE id=\"Table1\" cellSpacing=\"1\" cellPadding=\"0\" width=\"75%\" border=\"0\">");
							//sw1.WriteLine ("<TR><TD><Strong>Locations</Strong></TD><TD><Strong>Participants</Strong></TD></TR>");				 						
							//sw1.WriteLine ("<TR><TD>"+ locationList +"</TD><TD>"+ participantList +"</TD></TR>");				 						
							sw1.WriteLine ("<TR><TD><Strong>Locations</Strong></TD><TD><Strong>Date</Strong></TD><TD><Strong>Start</Strong></TD><TD><Strong>End</Strong></TD><TD><Strong>Timezone</Strong></TD></TR>");				 						
							sw1.WriteLine ("<TR><TD>"+ locationList +"</TD><TD>"+ conf.startTime.ToString("d") +"</TD><TD>" + conf.startTime.ToString("t") +"</TD><TD>"+ conf.endTime.ToString("t")+"</TD><TD>"+ tier2.timezoneId_Name +"</TD></TR>");				 						
							sw1.WriteLine ("</TABLE></P>");

							sw1.WriteLine("<P><TABLE id=\"Table1\" cellSpacing=\"0\" cellPadding=\"0\" width=\"75%\" border=\"0\">");
							sw1.WriteLine ("<TR><TD><Strong>Description</Strong></TD></TR>");				 						
							
							// Changed as per Angela's request of 1/11/07 - add a line break in desc field for a comma.
							conf.description = conf.description.Replace(",","<BR>");
						
							sw1.WriteLine ("<TR><TD>"+ conf.description +"</TD></TR>");				 						
							sw1.WriteLine ("</TABLE></P>");

							sw1.WriteLine("<P><TABLE id=\"Table1\" cellSpacing=\"0\" cellPadding=\"0\" width=\"75%\" border=\"0\">");
							//sw1.WriteLine ("<TR><TD><Strong>Catering Orders</Strong></TD><TD><Strong>Resource Orders</Strong></TD></TR>");				 						
							//sw1.WriteLine ("<TR><TD>"+ cateringList +"</TD><TD>"+ resourceList +"</TD></TR>");				 						
							sw1.WriteLine ("<TR><TD><Strong>Participants</Strong></TD></TR>");
							sw1.WriteLine ("<TR><TD>"+ participantList +"</TD></TR>");				 						
							sw1.WriteLine ("<TR><TD><Strong>Catering Orders</Strong></TD></TR>");				 						
							sw1.WriteLine ("<TR><TD>"+ cateringList +"</TD></TR>");
							sw1.WriteLine ("<TR><TD><Strong>Resource Orders</Strong></TD></TR>");				 						
							sw1.WriteLine ("<TR><TD>"+ resourceList +"</TD></TR>");
							sw1.WriteLine ("</TABLE></P>");
							*/
							
						sw1.WriteLine ("<TR><TD>"+ locationList  + "</TD><TD>"+ conf.startTime.ToString("t")  + "</TD><TD>"+ conf.endTime.ToString("t")+ "</TD><TD>"+ conf.name.Trim() +"</TD></TR>");
						sw1.WriteLine ("<TR><TD>------------------------------------------</TD></TR>");
										
					}
				}
				sw1.WriteLine ("</TABLE></P></body></html>");
				sw1.Flush();
				sw1.Close();
			}
			
		}
		catch (Exception e)
		{
            logger.Exception(100, "Exception generated : Message = " + e.Message + " , StackTrace = " + e.StackTrace);
		}
	}

	/// <summary>
	/// HKLAW custom report - Two week conference report for each tier 2 location entity
	/// </summary>
	/// <returns></returns>
    internal void HKLAW_TwoWeekConfReportByTier2()
	{
		try 
		{
			logger.Trace ("******************** Two Week reports by tier.... ********************");

			NS_DATABASE.Database db = new NS_DATABASE.Database(configParams);
			
			// get the tier-2 list
			Queue tier2List = new Queue();
			bool ret = db.FetchTier2List(ref tier2List);
			if (!ret)
			{
				logger.Trace("Error fetching tier-2 list");
				//return false;
			}

			// go thru tier-2 list one-by-one
			int tier2ListCount = tier2List.Count;
			for (int tier2Id =0; tier2Id < tier2ListCount ; tier2Id++)
			{
				NS_MESSENGER_CUSTOMIZATIONS.Tier2 tier2 = new NS_MESSENGER_CUSTOMIZATIONS.Tier2();
				tier2 = (NS_MESSENGER_CUSTOMIZATIONS.Tier2) tier2List.Dequeue();

				// get all conf's data
				Queue confList = new Queue();
				ret = db.TwoWeekConfsByTier(tier2.id,tier2.timezoneId, ref confList);
				if (!ret)
				{
					logger.Trace("Error in retreiving all confs");
					//return false;
				}
				
				// save data in file
				string tier2FilePath = tier2.filePath.Replace("weeklyreport.html","twoweekreport.html");					
					
				// delete the existing file
				if (File.Exists(tier2FilePath))
				{	
					logger.Trace ("Deleting existing report file...");
                    try
                    {
                        File.Delete(tier2FilePath);
                    }
                    catch (Exception)
                    {
                    }
				}

				// create a new file 
				StreamWriter sw ;
				sw = File.CreateText(tier2FilePath); 
				sw.Flush();
				sw.Close();

				// add the report header
				StreamWriter sw1 ;
				sw1 = File.AppendText(tier2FilePath);

				// report header
				sw1.WriteLine("<html><head><title>myVRM Conference Reports</title></head><body>");
				sw1.WriteLine ("<p><strong>TWO WEEK CONFERENCE REPORT</strong></p>");
				sw1.WriteLine ("<p><strong>TIER-2 NAME: " + tier2.name + "</strong></p>");
				sw1.WriteLine ("<p><strong>WEEK OF : " + DateTime.Now.ToString("d") + " - " + DateTime.Now.AddDays(14).ToString("d") + "</p></strong>");
				sw1.WriteLine ("<p>************************************************</p>");

				// append the conf records
				int confCount = confList.Count;
				if (confCount == 0)
				{
					sw1.WriteLine (" No conferences scheduled.");
				}
				else 
				{
					// add the title row
					for (int i= 0; i< confCount ; i++)
					{
						NS_MESSENGER_CUSTOMIZATIONS.Conference conf = new NS_MESSENGER_CUSTOMIZATIONS.Conference();
						conf = (NS_MESSENGER_CUSTOMIZATIONS.Conference) confList.Dequeue();
				
						// participant list
						string participantList = null;
						int participantCount = conf.participantList.Count;
						for (int j=0;j< participantCount;j++)
						{
							participantList += (string) conf.participantList.Dequeue(); 
					
							if (j <= participantCount-1)
							{
								participantList += ";";
							}
						}
				
						// location list
						string locationList = null;
						int locationCount = conf.locationList.Count;
						for (int j=0;j< locationCount;j++)
						{
							locationList += (string) conf.locationList.Dequeue(); 
					
							if (j <= locationCount-1)
							{
								locationList += "<BR>";
							}
						}
	
						// catering item list
						string cateringList = null ;
						int cateringCount = conf.cateringOrders.Count;
						for (int j=0;j< cateringCount;j++)
						{
							cateringList += (string) conf.cateringOrders.Dequeue(); 
					
							if (j <= cateringCount-1)
							{
								cateringList += "<BR>";
							}
						}
	
						// resource item list
						string resourceList = null ;
						int resourceCount = conf.resourceOrders.Count;
						for (int j=0;j< resourceCount;j++)
						{
							resourceList += (string) conf.resourceOrders.Dequeue(); 
					
							if (j <= resourceCount-1)
							{
								resourceList += "<BR>";
							}
						}

						//string confRecord = conf.startDate.ToString("d") + "," + conf.startTime.ToString("t") + "," + conf.endTime.ToString("t") + "," + conf.uniqueID.ToString() + "," + conf.name + "," + conf.description + ",";
						//confRecord += participantList + "," + locationList + "," + cateringList +"," + resourceList;

						// writing to file					
						/*							sw1.WriteLine("<P><TABLE id=\"Table1\" cellSpacing=\"0\" cellPadding=\"0\" width=\"75%\" border=\"0\">");
														sw1.WriteLine ("<TR><TD><P><STRONG>Date</STRONG></P></TD><TD><P><STRONG>UniqueID</STRONG></P></TD><TD><P><STRONG>Conference Name</STRONG></P></TD><TD><P><STRONG>Start Time</STRONG></P></TD><TD><P><STRONG>End Time</STRONG></P></TD><TD><P><STRONG>Timezone</STRONG></P></TD></TR>");
														sw1.WriteLine ("<TR><TD>"+ conf.startTime.ToString("d")+"</TD><TD>"+ conf.uniqueID.ToString().Trim() +"</TD><TD>"+ conf.name.Trim() +"</TD><TD>" + conf.startTime.ToString("t")  + "</TD><TD>"+ conf.endTime.ToString("t")+ "</TD><TD>Eastern Time</TD></TR>");
														sw1.WriteLine ("</TABLE></P>");

														sw1.WriteLine("<P><TABLE id=\"Table1\" cellSpacing=\"0\" cellPadding=\"0\" width=\"75%\" border=\"0\">");
														sw1.WriteLine ("<TR><TD><Strong>Locations</Strong></TD><TD><Strong>Participants</Strong></TD></TR>");				 						
														sw1.WriteLine ("<TR><TD>"+ locationList +"</TD><TD>"+ participantList +"</TD></TR>");				 						
														sw1.WriteLine ("</TABLE></P>");

														sw1.WriteLine("<P><TABLE id=\"Table1\" cellSpacing=\"0\" cellPadding=\"0\" width=\"75%\" border=\"0\">");
														sw1.WriteLine ("<TR><TD><Strong>Description</Strong></TD></TR>");				 						
														sw1.WriteLine ("<TR><TD>"+ conf.description +"</TD></TR>");				 						
														sw1.WriteLine ("</TABLE></P>");

														sw1.WriteLine("<P><TABLE id=\"Table1\" cellSpacing=\"0\" cellPadding=\"0\" width=\"75%\" border=\"0\">");
														sw1.WriteLine ("<TR><TD><Strong>Catering Orders</Strong></TD><TD><Strong>Resource Orders</Strong></TD></TR>");				 						
														sw1.WriteLine ("<TR><TD>"+ cateringList +"</TD><TD>"+ resourceList +"</TD></TR>");				 						
														sw1.WriteLine ("</TABLE></P>");
							*/
						sw1.WriteLine("<P><TABLE id=\"Table1\" cellSpacing=\"0\" cellPadding=\"0\" width=\"75%\" border=\"0\">");
						//sw1.WriteLine ("<TR><TD><P><STRONG>Date</STRONG></P></TD><TD><P><STRONG>UniqueID</STRONG></P></TD><TD><P><STRONG>Conference Name</STRONG></P></TD><TD><P><STRONG>Start Time</STRONG></P></TD><TD><P><STRONG>End Time</STRONG></P></TD><TD><P><STRONG>Timezone</STRONG></P></TD></TR>");
						//sw1.WriteLine ("<TR><TD>"+ conf.startTime.ToString("d")+"</TD><TD>"+ conf.uniqueID.ToString().Trim() +"</TD><TD>"+ conf.name.Trim() +"</TD><TD>" + conf.startTime.ToString("t")  + "</TD><TD>"+ conf.endTime.ToString("t")+ "</TD><TD>Eastern Time</TD></TR>");
						sw1.WriteLine ("<TR><TD><P><STRONG>Conference Name</STRONG></P></TD><TD><P><STRONG>UniqueID</STRONG></P></TD></TR>");
						sw1.WriteLine ("<TR><TD>"+ conf.name.Trim()+"</TD><TD>"+ conf.uniqueID.ToString().Trim() +"</TD></TR>");
						sw1.WriteLine ("</TABLE></P>");

						sw1.WriteLine("<P><TABLE id=\"Table1\" cellSpacing=\"1\" cellPadding=\"0\" width=\"75%\" border=\"0\">");
						//sw1.WriteLine ("<TR><TD><Strong>Locations</Strong></TD><TD><Strong>Participants</Strong></TD></TR>");				 						
						//sw1.WriteLine ("<TR><TD>"+ locationList +"</TD><TD>"+ participantList +"</TD></TR>");				 						
						sw1.WriteLine ("<TR><TD><Strong>Locations</Strong></TD><TD><Strong>Date</Strong></TD><TD><Strong>Start</Strong></TD><TD><Strong>End</Strong></TD><TD><Strong>Timezone</Strong></TD></TR>");				 						
						sw1.WriteLine ("<TR><TD>"+ locationList +"</TD><TD>"+ conf.startTime.ToString("d") +"</TD><TD>" + conf.startTime.ToString("t") +"</TD><TD>"+ conf.endTime.ToString("t")+"</TD><TD>"+ tier2.timezoneId_Name +"</TD></TR>");				 						
						sw1.WriteLine ("</TABLE></P>");

						sw1.WriteLine("<P><TABLE id=\"Table1\" cellSpacing=\"0\" cellPadding=\"0\" width=\"75%\" border=\"0\">");
						sw1.WriteLine ("<TR><TD><Strong>Description</Strong></TD></TR>");				 						
							
						// Changed as per Angela's request of 1/11/07 - add a line break in desc field for a comma.
						conf.description = conf.description.Replace(",","<BR>");
						
						sw1.WriteLine ("<TR><TD>"+ conf.description +"</TD></TR>");				 						
						sw1.WriteLine ("</TABLE></P>");

						sw1.WriteLine("<P><TABLE id=\"Table1\" cellSpacing=\"0\" cellPadding=\"0\" width=\"75%\" border=\"0\">");
						//sw1.WriteLine ("<TR><TD><Strong>Catering Orders</Strong></TD><TD><Strong>Resource Orders</Strong></TD></TR>");				 						
						//sw1.WriteLine ("<TR><TD>"+ cateringList +"</TD><TD>"+ resourceList +"</TD></TR>");				 						
						sw1.WriteLine ("<TR><TD><Strong>Participants</Strong></TD></TR>");
						sw1.WriteLine ("<TR><TD>"+ participantList +"</TD></TR>");				 						
						sw1.WriteLine ("<TR><TD><Strong>Catering Orders</Strong></TD></TR>");				 						
						sw1.WriteLine ("<TR><TD>"+ cateringList +"</TD></TR>");
						sw1.WriteLine ("<TR><TD><Strong>Resource Orders</Strong></TD></TR>");				 						
						sw1.WriteLine ("<TR><TD>"+ resourceList +"</TD></TR>");
						sw1.WriteLine ("</TABLE></P>");

						sw1.WriteLine ("------------------------------------------------------------------------------------------------------------");
										
					}
				}
				sw1.WriteLine ("</TABLE></P></body></html>");
				sw1.Flush();
				sw1.Close();
			}
			
		}
		catch (Exception e)
		{
            logger.Exception(100, "Exception generated : Message = " + e.Message + " , StackTrace = " + e.StackTrace);
		}
	}
		

	/// <summary>
	/// HKLAW custom report - Weekly list of all conferences across all locations.
	/// </summary>
	/// <returns></returns>
    internal bool HKLAW_WeeklyAllConfs()
	{
		try 
		{
			logger.Trace ("************************** Weekly reports for all locns (system reports).... ***********************");

			NS_DATABASE.Database db = new NS_DATABASE.Database(configParams);
			Queue confList = new Queue();

			// get all conf's data
			bool ret = db.WeeklyAllConfs(ref confList);
			if (!ret)
			{
				logger.Trace("Error in retreiving all confs");
				return false;
			}
				
			// HACK - hardcoded path
			configParams.rootFilePath = "C:\\Program Files\\Expedite VCS\\Reports\\myVRM\\All\\WeeklyAllConfs.html";

			// save data in file
			string rootFilePath = configParams.rootFilePath;

    #region first report
			// delete the existing file
			if (File.Exists(rootFilePath))
			{	
				logger.Trace ("Deleting existing report file...");
                try
                {
                    File.Delete(rootFilePath);
                }
                catch (Exception)
                {
                }
			}

			logger.Trace(rootFilePath);

			// create a new file 
			StreamWriter sw ;
			sw = File.CreateText(rootFilePath); 
			sw.Flush();
			sw.Close();

			// add the report header
			StreamWriter sw1 ;
			sw1 = File.AppendText(rootFilePath);
				
			logger.Trace("before the header");

			// report header
			sw1.WriteLine("<html><head><title>myVRM Conference Reports</title></head><body>");
			sw1.WriteLine ("<p><strong>WEEKLY CONFERENCE REPORT (ALL LOCATIONS)</strong></p>");
			sw1.WriteLine ("<strong>WEEK OF : " + DateTime.Now.ToString("d") + " - " + DateTime.Now.AddDays(7).ToString("d") + "</strong>");
			sw1.WriteLine ("<p>************************************************</p>");

			logger.Trace("after the header");

			// append the conf records
			int confCount = confList.Count;
			if (confCount == 0)
			{
				sw1.WriteLine (" No conferences scheduled.");
			}
			else 
			{
				// add the title row
				//string title3= "Date,StartTime,EndTime,UniqueID,ConferenceName,Description,Participants,Locations,Catering Orders,Resource Orders";
				//sw1.WriteLine (title3);

				for (int i= 0; i< confCount ; i++)
				{
					NS_MESSENGER_CUSTOMIZATIONS.Conference conf = new NS_MESSENGER_CUSTOMIZATIONS.Conference();
					conf = (NS_MESSENGER_CUSTOMIZATIONS.Conference) confList.Dequeue();

					logger.Trace("before the video check");				
					// As per client's request of Dec 13,2006 a special filter has been added to the conf - "video". 
					// Only confs which have this filter would be added to the report. 
					if (conf.name ==null || conf.name.Length < 1 || conf.name.ToUpper().IndexOf("VIDEO") < 0)
					{
							
						// conf is not video
						continue;
					}
					logger.Trace("after the video check");

					char[] ch = {';'};

					// participant list
					string participantList = null;
					try
					{
						int participantCount = conf.participantList.Count;
						for (int j=0;j< participantCount;j++)
						{
							participantList += (string) conf.participantList.Dequeue(); 
						
							if (j <= participantCount-1)
							{
								participantList += ";";
							}
						}							
						participantList = participantList.TrimEnd(ch);
					}
					catch (Exception ex)
					{
						logger.Trace (ex.Message);
					}
					logger.Trace("after the party list");

					// location list
					string locationList = null;
					try
					{
						int locationCount = conf.locationList.Count;
						for (int j=0;j< locationCount;j++)
						{
							locationList += (string) conf.locationList.Dequeue(); 
						
							if (j <= locationCount-1)
							{
								locationList += "<BR>";
							}
						}
						locationList = locationList.TrimEnd(ch);
					}
					catch (Exception ex)
					{
						logger.Trace (ex.Message);
					}
					logger.Trace("after the locn list");

					// catering item list
					string cateringList = "" ;
					try
					{
						int cateringCount = conf.cateringOrders.Count;
						for (int j=0;j< cateringCount;j++)
						{
							cateringList += (string) conf.cateringOrders.Dequeue(); 
						
							if (j <= cateringCount-1)
							{
								cateringList += "<BR>";
							}
						}						
						cateringList = cateringList.TrimEnd(ch);
					}
					catch (Exception ex)
					{
						logger.Trace (ex.Message);
					}
					logger.Trace("after the catering list");
						
						
					// resource item list
					string resourceList = "" ;
					try
					{
						int resourceCount = conf.resourceOrders.Count;
						for (int j=0;j< resourceCount;j++)
						{
							resourceList += (string) conf.resourceOrders.Dequeue(); 
					
							if (j <= resourceCount-1)
							{
								resourceList += ";";
							}
						}						
						resourceList = resourceList.TrimEnd(ch);
					}
					catch (Exception ex)
					{
						logger.Trace (ex.Message);
					}
					logger.Trace("after the resource list");
					// writing to file					
					sw1.WriteLine("<P><TABLE id=\"Table1\" cellSpacing=\"0\" cellPadding=\"0\" width=\"75%\" border=\"0\">");
					//sw1.WriteLine ("<TR><TD><P><STRONG>Date</STRONG></P></TD><TD><P><STRONG>UniqueID</STRONG></P></TD><TD><P><STRONG>Conference Name</STRONG></P></TD><TD><P><STRONG>Start Time</STRONG></P></TD><TD><P><STRONG>End Time</STRONG></P></TD><TD><P><STRONG>Timezone</STRONG></P></TD></TR>");
					//sw1.WriteLine ("<TR><TD>"+ conf.startTime.ToString("d")+"</TD><TD>"+ conf.uniqueID.ToString().Trim() +"</TD><TD>"+ conf.name.Trim() +"</TD><TD>" + conf.startTime.ToString("t")  + "</TD><TD>"+ conf.endTime.ToString("t")+ "</TD><TD>Eastern Time</TD></TR>");
					sw1.WriteLine ("<TR><TD><P><STRONG>Conference Name</STRONG></P></TD><TD><P><STRONG>UniqueID</STRONG></P></TD></TR>");
					sw1.WriteLine ("<TR><TD>"+ conf.name.Trim()+"</TD><TD>"+ conf.uniqueID.ToString().Trim() +"</TD></TR>");
					sw1.WriteLine ("</TABLE></P>");
					logger.Trace("after the name");										
					sw1.WriteLine("<P><TABLE id=\"Table1\" cellSpacing=\"1\" cellPadding=\"0\" width=\"75%\" border=\"0\">");
					//sw1.WriteLine ("<TR><TD><Strong>Locations</Strong></TD><TD><Strong>Participants</Strong></TD></TR>");				 						
					//sw1.WriteLine ("<TR><TD>"+ locationList +"</TD><TD>"+ participantList +"</TD></TR>");				 						
					sw1.WriteLine ("<TR><TD><Strong>Locations</Strong></TD><TD><Strong>Date</Strong></TD><TD><Strong>Start</Strong></TD><TD><Strong>End</Strong></TD><TD><Strong>Timezone</Strong></TD></TR>");				 						
					sw1.WriteLine ("<TR><TD>"+ locationList +"</TD><TD>"+ conf.startTime.ToString("d") +"</TD><TD>" + conf.startTime.ToString("t") +"</TD><TD>"+ conf.endTime.ToString("t")+"</TD><TD>Eastern Time</TD></TR>");				 						
					sw1.WriteLine ("</TABLE></P>");
					logger.Trace("after the loc and res");										
					sw1.WriteLine("<P><TABLE id=\"Table1\" cellSpacing=\"0\" cellPadding=\"0\" width=\"75%\" border=\"0\">");
					sw1.WriteLine ("<TR><TD><Strong>Description</Strong></TD></TR>");				 						
						
					// Changed as per Angela's request of 1/11/07 - add a line break in desc field for a comma.
					conf.description = conf.description.Replace(",","<BR>");
						
					sw1.WriteLine ("<TR><TD>"+ conf.description +"</TD></TR>");				 						
					sw1.WriteLine ("</TABLE></P>");
					logger.Trace("after the desc");										
					sw1.WriteLine("<P><TABLE id=\"Table1\" cellSpacing=\"0\" cellPadding=\"0\" width=\"75%\" border=\"0\">");
					//sw1.WriteLine ("<TR><TD><Strong>Catering Orders</Strong></TD><TD><Strong>Resource Orders</Strong></TD></TR>");				 						
					//sw1.WriteLine ("<TR><TD>"+ cateringList +"</TD><TD>"+ resourceList +"</TD></TR>");				 						
					sw1.WriteLine ("<TR><TD><Strong>Participants</Strong></TD></TR>");
					sw1.WriteLine ("<TR><TD>"+ participantList +"</TD></TR>");				 						
					sw1.WriteLine ("<TR><TD><Strong>Catering Orders</Strong></TD></TR>");				 						
					sw1.WriteLine ("<TR><TD>"+ cateringList +"</TD></TR>");
					sw1.WriteLine ("<TR><TD><Strong>Resource Orders</Strong></TD></TR>");				 						
					sw1.WriteLine ("<TR><TD>"+ resourceList +"</TD></TR>");
					sw1.WriteLine ("</TABLE></P>");

					sw1.WriteLine ("------------------------------------------------------------------------------------------------------------");
					logger.Trace("after the file write");										
				}
			}
			sw1.WriteLine ("</TABLE></P></body></html>");
			sw1.Flush();
			sw1.Close();
            #endregion



			return true;
		}
		catch (Exception e)
		{
			logger.Exception (100,e.Message);
			return false;
		}
	}
		

	/// <summary>
	/// HKLAW custom report - Daily list of all conferences across all locations.
	/// </summary>
	/// <returns></returns>
    internal void HKLAW_DailyAllConfs()
	{
		try 
		{
			logger.Trace ("************************** Daily reports for all locns (system reports).... ***********************");

			NS_DATABASE.Database db = new NS_DATABASE.Database(configParams);
			Queue confList = new Queue();

			// get all conf's data
			bool ret = db.DailyAllConfs(ref confList);
			if (!ret)
			{
				logger.Trace("Error in retreiving all confs");
				//return false;
			}
				
			// HACK - hardcoded path
			configParams.rootFilePath = "C:\\Program Files\\Expedite VCS\\Reports\\myVRM\\All\\DailyAllConfs.html";

			// save data in file
			string rootFilePath = configParams.rootFilePath;
		
			// delete the existing file
			if (File.Exists(rootFilePath))
			{	
				logger.Trace ("Deleting existing report file...");
                try
                {
                    File.Delete(rootFilePath);
                }
                catch (Exception)
                {
                }
			}

			logger.Trace(rootFilePath);

			// create a new file 
			StreamWriter sw ;
			sw = File.CreateText(rootFilePath); 
			sw.Flush();
			sw.Close();

			// add the report header
			StreamWriter sw1 ;
			sw1 = File.AppendText(rootFilePath);
				
			logger.Trace("before the header");

			// report header
			sw1.WriteLine("<html><head><title>myVRM Conference Reports</title></head><body>");
			sw1.WriteLine ("<p><strong>DAILY CONFERENCE REPORT (ALL LOCATIONS)</strong></p>");
			sw1.WriteLine ("<strong>DATE : " + DateTime.Now.ToString("d") + "</strong>");
			sw1.WriteLine ("<p>************************************************</p>");

			logger.Trace("after the header");

			// append the conf records
			int confCount = confList.Count;
			if (confCount == 0)
			{
				sw1.WriteLine (" No conferences scheduled.");
			}
			else 
			{
				// add the title row
				sw1.WriteLine("<P><TABLE id=\"Table1\" cellSpacing=\"0\" cellPadding=\"0\" width=\"75%\" border=\"0\">");
				sw1.WriteLine ("<TR><TD><P><STRONG>Location</STRONG></P></TD><TD><P><STRONG>Start Time</STRONG></P></TD><TD><P><STRONG>End Time</STRONG></P></TD><TD><P><STRONG>Conference Name</STRONG></P></TD></TR>");

				for (int i= 0; i< confCount ; i++)
				{
					NS_MESSENGER_CUSTOMIZATIONS.Conference conf = new NS_MESSENGER_CUSTOMIZATIONS.Conference();
					conf = (NS_MESSENGER_CUSTOMIZATIONS.Conference) confList.Dequeue();

					logger.Trace("before the video check");				
					// As per client's request of Dec 13,2006 a special filter has been added to the conf - "video". 
					// Only confs which have this filter would be added to the report. 
					if (conf.name ==null || conf.name.Length < 1 || conf.name.ToUpper().IndexOf("VIDEO") < 0)
					{
							
						// conf is not video
						continue;
					}
					logger.Trace("after the video check");

					char[] ch = {';'};

					// participant list
					string participantList = null;
					try
					{
						int participantCount = conf.participantList.Count;
						for (int j=0;j< participantCount;j++)
						{
							participantList += (string) conf.participantList.Dequeue(); 
						
							if (j <= participantCount-1)
							{
								participantList += ";";
							}
						}							
						participantList = participantList.TrimEnd(ch);
					}
					catch (Exception ex)
					{
						logger.Trace (ex.Message);
					}
					logger.Trace("after the party list");

					// location list
					string locationList = null;
					try
					{
						int locationCount = conf.locationList.Count;
						for (int j=0;j< locationCount;j++)
						{
							locationList += (string) conf.locationList.Dequeue(); 
						
							if (j <= locationCount-1)
							{
								locationList += "<BR>";
							}
						}
						locationList = locationList.TrimEnd(ch);
					}
					catch (Exception ex)
					{
						logger.Trace (ex.Message);
					}
					logger.Trace("after the locn list");

					// catering item list
					string cateringList = "" ;
					try
					{
						int cateringCount = conf.cateringOrders.Count;
						for (int j=0;j< cateringCount;j++)
						{
							cateringList += (string) conf.cateringOrders.Dequeue(); 
						
							if (j <= cateringCount-1)
							{
								cateringList += "<BR>";
							}
						}						
						cateringList = cateringList.TrimEnd(ch);
					}
					catch (Exception ex)
					{
						logger.Trace (ex.Message);
					}
					logger.Trace("after the catering list");
						
						
					// resource item list
					string resourceList = "" ;
					try
					{
						int resourceCount = conf.resourceOrders.Count;
						for (int j=0;j< resourceCount;j++)
						{
							resourceList += (string) conf.resourceOrders.Dequeue(); 
					
							if (j <= resourceCount-1)
							{
								resourceList += ";";
							}
						}						
						resourceList = resourceList.TrimEnd(ch);
					}
					catch (Exception ex)
					{
						logger.Trace (ex.Message);
					}
					logger.Trace("after the resource list");
					// writing to file					
					//sw1.WriteLine("<P><TABLE id=\"Table1\" cellSpacing=\"0\" cellPadding=\"0\" width=\"75%\" border=\"0\">");
					//sw1.WriteLine ("<TR><TD><P><STRONG>Date</STRONG></P></TD><TD><P><STRONG>UniqueID</STRONG></P></TD><TD><P><STRONG>Conference Name</STRONG></P></TD><TD><P><STRONG>Start Time</STRONG></P></TD><TD><P><STRONG>End Time</STRONG></P></TD><TD><P><STRONG>Timezone</STRONG></P></TD></TR>");
					//sw1.WriteLine ("<TR><TD>"+ conf.startTime.ToString("d")+"</TD><TD>"+ conf.uniqueID.ToString().Trim() +"</TD><TD>"+ conf.name.Trim() +"</TD><TD>" + conf.startTime.ToString("t")  + "</TD><TD>"+ conf.endTime.ToString("t")+ "</TD><TD>Eastern Time</TD></TR>");
					/*						sw1.WriteLine ("<TR><TD><P><STRONG>Conference Name</STRONG></P></TD><TD><P><STRONG>UniqueID</STRONG></P></TD></TR>");
											sw1.WriteLine ("<TR><TD>"+ conf.name.Trim()+"</TD><TD>"+ conf.uniqueID.ToString().Trim() +"</TD></TR>");
											sw1.WriteLine ("</TABLE></P>");
											logger.Trace("after the name");										
											sw1.WriteLine("<P><TABLE id=\"Table1\" cellSpacing=\"1\" cellPadding=\"0\" width=\"75%\" border=\"0\">");
											//sw1.WriteLine ("<TR><TD><Strong>Locations</Strong></TD><TD><Strong>Participants</Strong></TD></TR>");				 						
											//sw1.WriteLine ("<TR><TD>"+ locationList +"</TD><TD>"+ participantList +"</TD></TR>");				 						
											sw1.WriteLine ("<TR><TD><Strong>Locations</Strong></TD><TD><Strong>Date</Strong></TD><TD><Strong>Start</Strong></TD><TD><Strong>End</Strong></TD><TD><Strong>Timezone</Strong></TD></TR>");				 						
											sw1.WriteLine ("<TR><TD>"+ locationList +"</TD><TD>"+ conf.startTime.ToString("d") +"</TD><TD>" + conf.startTime.ToString("t") +"</TD><TD>"+ conf.endTime.ToString("t")+"</TD><TD>Eastern Time</TD></TR>");				 						
											sw1.WriteLine ("</TABLE></P>");
											logger.Trace("after the loc and res");										
											sw1.WriteLine("<P><TABLE id=\"Table1\" cellSpacing=\"0\" cellPadding=\"0\" width=\"75%\" border=\"0\">");
											sw1.WriteLine ("<TR><TD><Strong>Description</Strong></TD></TR>");				 						
						
											// Changed as per Angela's request of 1/11/07 - add a line break in desc field for a comma.
											conf.description = conf.description.Replace(",","<BR>");
						
											sw1.WriteLine ("<TR><TD>"+ conf.description +"</TD></TR>");				 						
											sw1.WriteLine ("</TABLE></P>");
											logger.Trace("after the desc");										
											sw1.WriteLine("<P><TABLE id=\"Table1\" cellSpacing=\"0\" cellPadding=\"0\" width=\"75%\" border=\"0\">");
											//sw1.WriteLine ("<TR><TD><Strong>Catering Orders</Strong></TD><TD><Strong>Resource Orders</Strong></TD></TR>");				 						
											//sw1.WriteLine ("<TR><TD>"+ cateringList +"</TD><TD>"+ resourceList +"</TD></TR>");				 						
											sw1.WriteLine ("<TR><TD><Strong>Participants</Strong></TD></TR>");
											sw1.WriteLine ("<TR><TD>"+ participantList +"</TD></TR>");				 						
											sw1.WriteLine ("<TR><TD><Strong>Catering Orders</Strong></TD></TR>");				 						
											sw1.WriteLine ("<TR><TD>"+ cateringList +"</TD></TR>");
											sw1.WriteLine ("<TR><TD><Strong>Resource Orders</Strong></TD></TR>");				 						
											sw1.WriteLine ("<TR><TD>"+ resourceList +"</TD></TR>");
											sw1.WriteLine ("</TABLE></P>");
					*/
					sw1.WriteLine ("<TR><TD>"+ locationList +"</TD>" + "<TD>"+ conf.startTime.ToString("t") +"</TD>" +"<TD>"+ conf.endTime.ToString("t") +"</TD>" +"<TD>"+ conf.name.Trim() +"</TD></TR>");
					sw1.WriteLine ("<TR><TD>-----------------------------------</TD></TR>");
					logger.Trace("after the file write");										
				}
			}
			sw1.WriteLine ("</TABLE></P></body></html>");
			sw1.Flush();
			sw1.Close();
				

		}
		catch (Exception e)
		{
            logger.Exception(100, "Exception generated : Message = " + e.Message + " , StackTrace = " + e.StackTrace);
		}
	}
		
        #endregion

    
    #region Client: WASHU
    /// <summary>
    /// Conf reminders 
    /// </summary>
    /// <param name="conf"></param>
    /// <returns></returns>
    internal bool WASHU_SendConferenceReminders()
    {
        try
        {
            logger.Trace("Wash Univ - Send work order reminders for confs starting in 2 weeks...");

            // get confs from db which are starting within the next 24 hrs
            NS_DATABASE.Database db = new NS_DATABASE.Database(this.configParams);
            Queue confList = new Queue();
            bool ret = db.FetchConferenceList_WithNoWorkOrdersAndNoReminderAlert(ref confList);
            if (!ret)
            {
                logger.Trace("Error fetching conf list.");
                return false;
            }

            // send pending emails
            while (confList.Count > 0)
            {
                try
                {
                    NS_MESSENGER.Conference conf = new NS_MESSENGER.Conference();
                    conf = (NS_MESSENGER.Conference)confList.Dequeue();

                    // Get Tech Support Email
                    // FB 377-Maneesh Pujari Start Code
                    string strEmailFrom = "";
                    if (db.GetTechSupportEmail(ref strEmailFrom) ==  false)
                    {
                        logger.Trace("Tech Support email address not found");
                        return false;
                    }
                    // FB 377-Maneesh Pujari End Code

                    // create email 
                    NS_MESSENGER.Email email = new NS_MESSENGER.Email();
                    // FB 377-Maneesh Pujari Start Code
                    email.From = strEmailFrom;
                    // FB 377-Maneesh Pujari End Code
                    email.To = conf.sHostEmail;
                    email.Subject = "[VRM Reminder] No workorders associated with - " + conf.sExternalName;
                    email.Body = "This is a friendly reminder to let you know that no work orders have been setup with the below conference yet. Audio/Visual , Catering and Housekeeping work orders can be added to the conference.";
                    email.Body += "<BR>Conference Name: " + conf.sExternalName;
                    email.Body += "<BR>Unique ID: " + conf.iDbNumName.ToString();

                    // send email 
                    ret = false;
                    ret = db.InsertEmailInQueue(email);
                    if (!ret)
                    {
                        logger.Trace("Email couldn't be added to the queue.");
                    }

                    // update the conference alerts as the reminder email has been sent successfully.
                    NS_MESSENGER.Alert alert = new NS_MESSENGER.Alert();ret = false;
                    alert.confID = conf.iDbID;
                    alert.instanceID = conf.iInstanceID;
                    alert.message = "No work orders found for the conference. 2 week reminder email sent to conference host.";

                    // Crunch time right now so do in future !!! 
                    // TODO: Make all alert types enum instead of integer. Code will be more readable and flexible. 
                    alert.typeID = 10; // Magic Number . Alert type = 10 (for 2 week reminder). 
                                        
                    ret = db.InsertConfAlert(alert);
                    if (!ret)
                    {
                        logger.Trace("Alert couldn't be added to the conference alerts.");
                    }
                }
                catch (Exception)
                {
                    // log error and continue processing
                    logger.Trace("Error processing conf in the list.");
                }
            }
            return true;
        }
        catch (Exception e)
        {
            this.errMsg = e.Message;
            logger.Exception(100, e.Message);
            return false;
        }
    }
    #endregion
#endif
}

}

#if COMMENT
    /// <summary>
	/// Setup a point-to-point conference using Telnet (only for Polycom VSX endpoints).
	/// </summary>
	/// <returns></returns>
	internal bool P2PConfSetup()
	{
		try 
		{
			logger.Trace ("Entering P2P section...");

			// Fetch immediate confs dbId, dbInstanceid
			NS_DATABASE.Database db = new NS_DATABASE.Database(configParams);
			Queue confq = new Queue();
			bool ret = db.FetchP2PConferences(ref confq);
			if (!ret) 
			{
				logger.Exception(100,"Error fetching confs from db.");
				return false;
			}
			// Setup conferences one-by-one
			while (confq.Count > 0) 
			{
				NS_MESSENGER.Conference confObj = new NS_MESSENGER.Conference();
				confObj = (NS_MESSENGER.Conference)confq.Dequeue();
					
				// Fetch the rooms
				bool ret5 = db.FetchRooms(confObj.iDbID,confObj.iInstanceID,ref confObj.qParties,0);
				if (!ret5) 						
				{
					logger.Trace ("Error fetching rooms.");
					return false;
				}

				// Fetch the users
				bool ret6 = db.FetchUsers(confObj.iDbID,confObj.iInstanceID,ref confObj.qParties,0);
				if (!ret6) 
				{
					logger.Trace ("Error fetching users.");
					return false;
				}

				// Setup the p2p conference.
				NS_MESSENGER.Party srcParty = new NS_MESSENGER.Party();
				NS_MESSENGER.Party destParty = new NS_MESSENGER.Party();
				srcParty = (NS_MESSENGER.Party) confObj.qParties.Dequeue();
				destParty = (NS_MESSENGER.Party)  confObj.qParties.Dequeue();

				logger.Trace("Fetching the endpoint info...");
				NS_VSX.VSX vsx = new NS_VSX.VSX(this.configParams);
				bool isIP = true;
				if (srcParty.etProtocol == NS_MESSENGER.Party.eProtocol.ISDN)
				{
					isIP = false;
				}					
						
				logger.Trace("Dialing the endpoints...");
				bool ret7 = vsx.Dial(srcParty.sAddress,srcParty.sLogin,srcParty.sPwd,destParty.sAddress,"384",isIP);
				if (!ret7) 
				{
					logger.Exception(100,"Conference setup skipped.");
					continue;
				}
			}
			return true;
		}
		catch (Exception e)
		{
			logger.Exception(100,e.Message);
			return false;
		}			
	}
	class Conference
	{
		// attributes
		private NS_LOGGER.Log logger;
		private NS_MESSENGER.ConfigParams configParams;
		internal string errMsg = null;

		internal Conference(NS_MESSENGER.ConfigParams config)
		{
			configParams = config;
			logger = new NS_LOGGER.Log(configParams);
		}

		internal bool SetConference(string inXML,int mode, int videoMode)
		{
			
			try
			{
				logger.Trace ("Inside SetConference...");
				
#region Pre-processing checks

    #endregion

#region Parse input xml
				// Load the string into xml parser and retrieve the tag values.
				XmlDocument xd = new XmlDocument();
				xd.LoadXml(inXML);							

				// Instantiate the MCU object.
				MS_Conference conf = new MS_Conference();

				// start retreiving data from the inxml and load it in the conf messenger object.
				// conf name
				conf.sName = xd.SelectSingleNode("//conference/name").InnerXml.Trim();
				// conf owner
				conf.iOwnerID = Int32.Parse(xd.SelectSingleNode("//conference/userID").InnerXml.Trim());
				// conf type
				int confType = Int32.Parse(xd.SelectSingleNode("//conference/createBy").InnerXml.Trim());
				switch (confType)
				{
					// TODO: Add the conf types
					case 0: {
								conf.eType = MS_Conference.eConfType.AUDIO_VIDEO;
								break;
							}
					default:
							{
								conf.eType = MS_Conference.eConfType.NON_AUDIO_VIDEO;
								break;
							}
				}
				// password
				conf.sPassword = xd.SelectSingleNode("//conference/confPassword").InnerXml.Trim();
				// immediate
				int immediate = Int32.Parse(xd.SelectSingleNode("//conference/immediate").InnerXml.Trim());
				if (immediate ==0)
				{
					conf.isImmediate = true;
				}

				//recurring tags

				// deptID
				conf.iDeptID = Int32.Parse(xd.SelectSingleNode("//conference/deptID").InnerXml.Trim());
				// timezone
				conf.iTimezoneID = Int32.Parse(xd.SelectSingleNode("//conference/timeZone").InnerXml.Trim());
				// durationMin
				conf.iDurationInMin = Int32.Parse(xd.SelectSingleNode("//conference/durationMin").InnerXml.Trim());
				// description
				conf.sDescription = xd.SelectSingleNode("//conference/description").InnerXml.Trim();
				// public
				conf.sDescription = xd.SelectSingleNode("//conference/publicConf").InnerXml.Trim();
				// continous presence
				conf.iContinousPresence = Int32.Parse(xd.SelectSingleNode("//conference/continous").InnerXml.Trim());
				// video layout
				conf.iVideoLayout = Int32.Parse(xd.SelectSingleNode("//conference/videoLayout").InnerXml.Trim());
				// manual video layout
				conf.iManualVideoLayout = Int32.Parse(xd.SelectSingleNode("//conference/manualVideoLayout").InnerXml.Trim());
				// lecture mode
				conf.iLectureMode = Int32.Parse(xd.SelectSingleNode("//conference/lectureMode").InnerXml.Trim());
				if (conf.iLectureMode == 1)
				{
					// lecturer 
					conf.iLecturerUserID = Int32.Parse(xd.SelectSingleNode("//conference/lecturer").InnerXml.Trim());
				}
				// line rate
				conf.iLineRateID = Int32.Parse(xd.SelectSingleNode("//conference/lineRateID").InnerXml.Trim());

				// audio algorithm
				conf.iAudioAlgorithmID = Int32.Parse(xd.SelectSingleNode("//conference/audioAlgorithmID").InnerXml.Trim());

				//video protocol
				conf.iVideoProtocolID = Int32.Parse(xd.SelectSingleNode("//conference/videoProtocolID").InnerXml.Trim());
				
				string modifyAll = xd.SelectSingleNode("//conference/ModifyType").InnerXml.Trim();
				if (modifyAll == null)
				{
					modifyAll = "0";
				}

				// dynamic invite 
				string dynInvite = xd.SelectSingleNode("//conference/dynamicInvite").InnerXml.Trim();
				if (dynInvite == "1")
				{
					conf.isOpenForPublicRegistration =  true;
				}

				// User list

				// Room list 

    #endregion

#region Post-processing checks

				// CHECK: check for security key expiration			
				if (conf.eOperation == MS_Conference.eConfOperation.NEW)
				{	
					if (HasSecurityLicenseExpired())
					{
						// TODO: add call to db error 
						this.errMsg = " Invalid security key. ";
						return false;
					}
				}

				// CHECK: check if room split is valid. 
				// Only non-video confs are allowed. 
				if ( conf.eType != MS_Conference.eConfType.NON_AUDIO_VIDEO && conf.isRoomSplitEnabled == true)
				{	
					// TODO: add call to db error 410
					this.errMsg = " Split Room Feature is available only for non-video conferences.";
					return false;
				}

				// CHECK: check if p2p confs are allowed or not to be created. 			
				if ( conf.eType == MS_Conference.eConfType.POINT_TO_POINT && conf.eOperation == MS_Conference.eConfOperation.NEW)
				{	
					// TODO: Add sql to fetch system setting - p2p enabled or not
					// if not then do not permit this conf to go thru.
					// TODO: add call to db error 410
					this.errMsg = " Point to point conferences are not permitted.";
					return false;
				}

				// CHECK: check that rooms have endpoints (only for audio & video confs)			
				if ( conf.eType != MS_Conference.eConfType.NON_AUDIO_VIDEO && conf.eOperation != MS_Conference.eConfOperation.DELETE)
				{	
					this.errMsg = "Room does not have endpoints associated.";
					return false;
				}


			
		
    #endregion

				return true;
			}
			catch (Exception e)
			{
				logger.Exception(100,e.Message);
				return false;
			}
		}
		internal bool HasSecurityLicenseExpired()
		{
			//TODO: Add the sec license check code
			return true;
		}
	}
	class MS_Conference
	{
		internal enum eConfType {AUDIO_ONLY,AUDIO_VIDEO,NON_AUDIO_VIDEO,POINT_TO_POINT};
		internal enum eConfOperation {NEW,EDIT,DELETE};
		internal int iConfID,iInstanceID,iOwnerID,iDeptID,iTimezoneID,iDurationInMin;
		internal int iUniqueID;
		internal bool isPublic,isImmediate,isOpenForPublicRegistration,isRoomSplitEnabled;
		internal string sName,sDescription,sPassword;
		internal eConfType eType;
		internal eConfOperation eOperation;
		internal IList arrUserList;
		internal IList arrRoomList;

		//video terms
		internal int iContinousPresence,iVideoLayout,iManualVideoLayout,iVideoSession,iLineRateID;
		internal int iLectureMode,iLecturerUserID,iAudioAlgorithmID,iVideoProtocolID;

		internal MS_Conference()
		{
			iTimezoneID	= 26;			
			isPublic = isImmediate = false;			
			arrUserList = new ArrayList();
			arrRoomList = new ArrayList();
		}
	}
	
	class MS_User
	{
		
		internal string sFirstName,sLastName,sEmail;
		internal int iUserID;		
		internal MS_User()
		{
			
		}
	}
	
	class MS_Room
	{
		internal int iRoomID;		
		internal MS_Room()
		{
			
		}
	}

    class CallAspxSite
{
	/// <summary>
	/// private class attributes
	/// </summary>
	private NS_LOGGER.Log logger;
	private NS_MESSENGER.ConfigParams configParams;

	/// <summary>
	/// Constructor
	/// </summary>
	/// <param name="config"></param>
	internal CallAspxSite(NS_MESSENGER.ConfigParams config)
	{
		this.configParams = config;
		logger = new NS_LOGGER.Log(this.configParams);
	}

	public bool RunScheduledReports()
	{
		try
		{

			logger.Trace("Calling Scheduled Reports ASPX controller page...");
			ServicePointManager.CertificatePolicy = new AcceptAllCertificatePolicy();

			WebRequest req = WebRequest.Create(this.configParams.scheduledReportsAspxUrl);
			WebResponse result = req.GetResponse();
			Stream ReceiveStream = result.GetResponseStream();
			Encoding encode = System.Text.Encoding.GetEncoding("utf-8");
			StreamReader sr = new StreamReader( ReceiveStream, encode );						
			Char[] read = new Char[1024];			
			int count = sr.Read( read, 0, 1024 );						
			logger.Trace ("Response : ");
			while (count > 0) 
			{
				String str = new String(read, 0, count);
				logger.Trace (str);
				count = sr.Read(read, 0, 1024);
			}
					
			//GetSSLPage(configParams.siteUrl);
			/*						try
											{
												XMLHTTP xmlHttp = new XMLHTTP();
												xmlHttp.open("POST",configParams.siteUrl,false,null,null);
												xmlHttp.send("hello");							
												if (xmlHttp.status == 200)
												{
													receive= xmlHttp.responseText;
												}
												else 
												{
													receive = "Error - " + xmlHttp.status.ToString();
												}
											}
											catch (Exception e)
											{
												receive = e.Message;
											}
					*/
				

		return true;	
		}
		catch (Exception e)
		{
			logger.Exception(100,e.Message);
			return false;
		}
	}

	private bool GetSSLPage(string url)
	{
	
	
		try 
		{
			ServicePointManager.CertificatePolicy = new AcceptAllCertificatePolicy();

			WebRequest req = WebRequest.Create(url);
			WebResponse result = req.GetResponse();
			Stream ReceiveStream = result.GetResponseStream();
			Encoding encode = System.Text.Encoding.GetEncoding("utf-8");
			StreamReader sr = new StreamReader( ReceiveStream, encode );
			Console.WriteLine("\r\nResponse stream received");
			Char[] read = new Char[256];
			int count = sr.Read( read, 0, 256 );

			Console.WriteLine("HTML...\r\n");
			while (count > 0) 
			{
				String str = new String(read, 0, count);
				Console.Write(str);
				count = sr.Read(read, 0, 256);
			}
			Console.WriteLine("");
				
			return true;
		} 
		catch(Exception e) 
		{
			Console.WriteLine (e.Message);
			Console.WriteLine("\r\nThe request URI could not be found or was malformed");
			return false;
		}
	}

}
internal class AcceptAllCertificatePolicy : ICertificatePolicy
{
	public AcceptAllCertificatePolicy()
	{
	}

	public bool CheckValidationResult(ServicePoint sPoint,X509Certificate cert, WebRequest wRequest,int certProb)
	{
		// Always accept
		return true;
	}
}
#endif
