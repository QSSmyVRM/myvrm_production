//ZD 100147 Start
/* Copyright (C) 2015 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
//ZD 100147 ZD 100886 End
/* FILE : Codian.cs
 * DESCRIPTION : All Codian MCU api commands are stored in this file. 
 * AUTHOR : Kapil M
 */
namespace NS_CODIAN
{
	using System;
	using System.Xml;
	using System.Collections;
	using System.Runtime.InteropServices;
	using vbxmlrpc_Net;

	class Codian
	{
		private NS_LOGGER.Log logger;
		internal string errMsg = null;
		private int confEnumerateID = 0;
		private int participantEnumerateID = 0;
		private NS_MESSENGER.ConfigParams configParams ;
        private int indexCDR = 0; //FB 2797

		internal Codian(NS_MESSENGER.ConfigParams config)
		{
			configParams = new NS_MESSENGER.ConfigParams();
			configParams = config;
			logger = new NS_LOGGER.Log(configParams);
		}

		#region Terminal Control Methods

		private bool CreateStruct(string operation, NS_MESSENGER.Party party, NS_MESSENGER.Conference conf, ref vbxmlrpc_Net.XMLRPCStruct oStruct)
		{			
			operation = operation.Trim();
			logger.Trace ("Entering create struct for operation = " + operation);

			string bridgeIP = party.cMcu.sIp.Trim();
			logger.Trace ("Codian Bridge IP: " + bridgeIP);
			string bridgeLogin = party.cMcu.sLogin.Trim();
			logger.Trace ("Codian Bridge Login: " + bridgeLogin);
		    string bridgePwd = party.cMcu.sPwd.Trim();
			logger.Trace ("Codian Bridge Pwd: " + bridgePwd);
            vbxmlrpc_Net.XMLRPCArrayClass stateArray = null; //FB 2640
			
			// check if bridge params are not empty
			if (bridgeIP.Length < 1 || bridgeLogin.Length < 1 || bridgePwd.Length < 1) 
			{
				logger.Exception (100,"Bridge details are not correct.");
				return false;
			}
			
			// Struct object : Add the authentication parameters
			oStruct.AddString("authenticationUser", bridgeLogin);
			oStruct.AddString("authenticationPassword", bridgePwd);
			
			// Different method calls on the mcu.
			logger.Trace("Operation = " + operation);
			switch(operation)
			{
				case "ConferenceCreate":
                    {
                        #region ConferenceCreate
                        // Add the conference name 
					oStruct.AddString("conferenceName",conf.sDbName);								

					// Add the conf num name 

                    //FB 2636 Start
                    oStruct.AddBoolean("registerWithSIPRegistrar", conf.bE164);
                    oStruct.AddBoolean("registerWithGatekeeper", conf.bH323);

                    if (conf.bE164 || conf.bH323)
                    {
                        oStruct.AddString("numericId", conf.sDialinNumber.ToString());//FB 2659
                        logger.Trace("Conf iDialinNumber ID: " + conf.sDialinNumber.ToString());
                    }
                    else
                    {
                        oStruct.AddString("numericId", conf.iDbNumName.ToString());
                        logger.Trace("Conf Unique ID: " + conf.iDbNumName.ToString());
                    }
                    //FB 2636 End
                    

                    // Add the start time.
                    logger.Trace("StartTime: " + conf.dtStartDateTimeInLocalTZ.ToString("s"));                       
                    oStruct.AddDateTime("startTime", conf.dtStartDateTimeInLocalTZ);
                                        
					// Add the conference duration in "seconds"
                    logger.Trace("DurationSeconds ");
					oStruct.AddInteger("durationSeconds", (conf.iDuration *  60));
					
					// Add the numeric password 
					if (conf.iPwd > 0)
					{
						oStruct.AddString ("pin",conf.iPwd.ToString());
					}

					// Equate db display layout value to value expected by mcu.
                    logger.Trace("Layout ");
					//string codianLayoutValue = "layout1";
					//bool ret1 = EquateVideoLayout(conf.iVideoLayout,ref codianLayoutValue);
					//if(!ret1)
					//{
					//	logger.Trace ("Invalid Video Layout.");
					//	return false;
					//}
                    int customLayout = conf.iVideoLayout;
                    string familyLayoutId = "";
                    if (conf.iVideoLayout >= 100)
                    {
                        familyLayoutId = conf.iVideoLayout.ToString().Substring(2, 1);
                        int.TryParse(familyLayoutId, out customLayout);
                    }
                    if (customLayout <= 0)
                    {
                        oStruct.AddBoolean("customLayoutEnabled", false);
                        oStruct.AddBoolean("newParticipantsCustomLayout", false);
                    }
                    else
                    {
                        oStruct.AddBoolean("customLayoutEnabled", true);
                        oStruct.AddBoolean("newParticipantsCustomLayout", true);
                        oStruct.AddInteger("customLayout", customLayout);
                    }

					// Equate db line rate value to value expected by mcu.
                    logger.Trace("LineRate ");
					int codianLineRate = 384;
					bool ret2 = EquateLineRate(conf.stLineRate.etLineRate,ref codianLineRate);
					if(!ret2)
					{
						logger.Trace ("Invalid LineRate.");
						return false;
					}
                    logger.Trace("MaxBitRates ");
					oStruct.AddInteger("maxBitRateToMcu", codianLineRate);
					oStruct.AddInteger("maxBitRateFromMcu", codianLineRate);

					// Dual Stream Mode 
					if (conf.bH239)
					{
						oStruct.AddBoolean("h239Enabled", true);
					}

					// Max audio & video ports
					if (conf.iMaxAudioPorts > 0)
					{
						oStruct.AddInteger("reservedAudioPorts", conf.iMaxAudioPorts);					
	
					}
					if (conf.iMaxVideoPorts > 0)
					{						
						oStruct.AddInteger("reservedVideoPorts", conf.iMaxVideoPorts);
					}

                    //FB 2501 Call Monitoring Start
                    if (conf.bLectureMode)
                    {
                        oStruct.AddBoolean("automaticLectureModeEnabled", conf.bLectureMode);
                    }
                    //FB 2501 Call Monitoring End

                    // Reserved audio & video ports
                    int audioCount = 0; int videoCount = 0;
                    logger.Trace("Party count: " + conf.qParties.Count.ToString());                                                            
                    System.Collections.IEnumerator partyEnumerator;
                    partyEnumerator = conf.qParties.GetEnumerator();
                    int partyCount = conf.qParties.Count;                    
                    for (int i = 0; i < partyCount; i++)
                    {
                        // go to next party in the queue
                        partyEnumerator.MoveNext();

                        // party
                        NS_MESSENGER.Party party1 = new NS_MESSENGER.Party();
                        party1 = (NS_MESSENGER.Party)partyEnumerator.Current;

                        logger.Trace("Party call type: " + party1.etCallType.ToString());

                        if (party1.etCallType == NS_MESSENGER.Party.eCallType.AUDIO)
                        {
                            audioCount++;
                        }
                        else
                        {
                            if (party1.etCallType == NS_MESSENGER.Party.eCallType.VIDEO)
                            {
                                videoCount++;
                            }
                        }                        
                    }

                    logger.Trace("Reserved Audio & Video ports: Audio =" + audioCount.ToString() + " , Video= " + videoCount.ToString());
                    oStruct.AddInteger("reservedAudioPorts", audioCount);
                    oStruct.AddInteger("reservedVideoPorts", videoCount);
 
					break;
                        #endregion

                    }

                case "ConferenceModify":
                    {
                        #region ConferenceModify
                        // Add the conference name 
                    oStruct.AddString("conferenceName", conf.sDbName);
/*
                    // Add the conf num name 
                    logger.Trace("Conf Unique ID: " + conf.iDbNumName.ToString());
                    oStruct.AddString("numericId", conf.iDbNumName.ToString());

                    // FIRMWARE-SPECIFIC
                    // This param is now supported due to the Push-To-Conf feature availability, which supports push for future confs.
                    // Previously, all calls were launched in a "immediate launch" mode via the back-end service. 
                    // Add the start time.
                    oStruct.AddDateTime("startTime", conf.dtStartDateTimeInLocalTZ);

                    // Add the conference duration in "seconds"
                    oStruct.AddInteger("durationSeconds", (conf.iDuration * 60));

                    // Add the numeric password 
                    if (conf.iPwd > 0)
                    {
                        oStruct.AddInteger("pin", conf.iPwd);
                    }

                    // Equate db display layout value to value expected by mcu.
                    string codianLayoutValue = "layout1";
                    bool ret1 = EquateVideoLayout(conf.iVideoLayout, ref codianLayoutValue);
                    if (!ret1)
                    {
                        logger.Trace("Invalid Video Layout.");
                        return false;
                    }
                    oStruct.AddString("cpLayout", codianLayoutValue);

                    // Equate db line rate value to value expected by mcu.
                    int codianLineRate = 384;
                    bool ret2 = EquateLineRate(conf.stLineRate.etLineRate, ref codianLineRate);
                    if (!ret2)
                    {
                        logger.Trace("Invalid LineRate.");
                        return false;
                    }
                    oStruct.AddInteger("maxBitRateToMcu", codianLineRate);
                    oStruct.AddInteger("maxBitRateFromMcu", codianLineRate);

                    // Dual Stream Mode 
                    if (conf.bH239)
                    {
                        oStruct.AddBoolean("h239Enabled", true);
                    }
                    
                    // Max audio & video ports
                    if (conf.iMaxAudioPorts > 0)
                    {
                        oStruct.AddInteger("maximumAudioPorts", conf.iMaxAudioPorts);

                    }
                    if (conf.iMaxVideoPorts > 0)
                    {
                        oStruct.AddInteger("maximumVideoPorts", conf.iMaxVideoPorts);
                    }

*/
                    // Reserved audio & video ports
                    int audioCount = 0; int videoCount = 0;
                    logger.Trace("Party count: " + conf.qParties.Count.ToString());
                    System.Collections.IEnumerator partyEnumerator;
                    partyEnumerator = conf.qParties.GetEnumerator();
                    int partyCount = conf.qParties.Count;
                    for (int i = 0; i < partyCount; i++)
                    {
                        // go to next party in the queue
                        partyEnumerator.MoveNext();

                        // party
                        NS_MESSENGER.Party party1 = new NS_MESSENGER.Party();
                        party1 = (NS_MESSENGER.Party)partyEnumerator.Current;

                        logger.Trace("Party call type: " + party1.etCallType.ToString());

                        if (party1.etCallType == NS_MESSENGER.Party.eCallType.AUDIO)
                        {
                            audioCount++;
                        }
                        else
                        {
                            if (party1.etCallType == NS_MESSENGER.Party.eCallType.VIDEO)
                            {
                                videoCount++;
                            }
                        }
                    }

                    oStruct.AddInteger("reservedAudioPorts", audioCount);
                    oStruct.AddInteger("reservedVideoPorts", videoCount);
                  
                    break;
                        #endregion
                    }
				case "ExtendConferenceEndTime":
                    {
                        #region ExtendTime
                       // Add the conference name 
					    oStruct.AddString("conferenceName",conf.sDbName);								

					    // Add the conference duration in "seconds"
					    oStruct.AddInteger("durationSeconds", (conf.iDuration *  60));
                        #endregion

                        break;
				    }
                case "ModifyEndpoint": //FB 2249
                    {
                        #region ModifyEndpoint
                        logger.Trace("Trace1" + party.sAddress + conf.sDbName + party.sName);

                        oStruct.AddString("conferenceName", conf.sDbName.Trim());
                        oStruct.AddString("participantName", party.sName.Trim());
                        oStruct.AddString("operationScope", "configuredState");
                        oStruct.AddString("address", party.sAddress.Trim());
                        #endregion
                        break;
                    }
				case "AddParty":
                    {
                        #region Add Party
                        // Add a participant to existing conf on mcu. 
                        // The conf could be scheduled or ongoing.
                        //Add conf & party info to the RPC struct
					oStruct.AddString("conferenceName", conf.sDbName.Trim());
                    oStruct.AddString("participantName", party.sName.Trim());
					// Check if party is a cascade link or not
				   	if(party.etType == NS_MESSENGER.Party.eType.CASCADE_LINK)
					{
						// party is a cascade link
						if (party.etCascadeRole == NS_MESSENGER.Party.eCascadeRole.MASTER)
						{
							logger.Trace ("Add Party - Master Cascade Link");								
							oStruct.AddString("address", party.sAddress.Trim());
							oStruct.AddBoolean("deferConnection",true);
							oStruct.AddString("cpLayout","layout1");							
						}
						else
						{
							logger.Trace ("Add Party - Slave Cascade Link");								
							oStruct.AddString("address", conf.iDbNumName.ToString().Trim());
							oStruct.AddString("gatewayAddress", party.sAddress.Trim());
						}
					}
					else
					{
                        logger.Trace("Address " + party.sAddress.Trim());
						// party is not a cascade link
                        string partyAddress = party.sAddress;
                        //ZD 103263 Start
                        if (partyAddress.Trim().Contains("B"))
                        {
                            int Gindex = partyAddress.IndexOf("B");
                            partyAddress = partyAddress.Substring(0, Gindex);
                        }
                        //ZD 103263 End
                        if (partyAddress.Trim().Contains("G"))
                        {
                            int Gindex = partyAddress.IndexOf("G");                            
                            partyAddress = partyAddress.Substring(0, Gindex);
                        }
                        if (partyAddress.Trim().Contains("D") && party.bAudioBridgeParty) //ZD 101655
                        {
                            int Dindex = partyAddress.IndexOf("D");
                            partyAddress = partyAddress.Substring(0, Dindex);

                            //Audio Dial-in prefix is added before the address
                            //FB 2655 DTMF Start
                            //partyAddress = configParams.audioDialNoPrefix + partyAddress; //Disney New Audio Add on Request
                            partyAddress = party.AudioDialInPrefix + partyAddress;
                            //FB 2655 DTMF End
                        }
                        else  //FB 2003 ISDN Prefix start
                        {
                            if (party.etProtocol == NS_MESSENGER.Party.eProtocol.ISDN)
                            {
                                string phonePrefix = "";
                                if (party.etCallType == NS_MESSENGER.Party.eCallType.AUDIO)
                                {
                                    phonePrefix = party.cMcu.iISDNAudioPrefix;
                                }
                                else
                                {
                                    phonePrefix = party.cMcu.iISDNVideoPrefix;
                                }
                                if (phonePrefix != "")
                                    partyAddress = phonePrefix + partyAddress;
                            }
                        }
                        //FB 2003 ISDN Prefix end

                        //FB 3058 Start
                        if (party.etProtocol == NS_MESSENGER.Party.eProtocol.SIP)
                        {
                            string protocol = "sip";
                            oStruct.AddString("participantProtocol", protocol.Trim());
                            oStruct.AddBoolean("useSIPRegistrar", true); //ZD 104605 
                        }
                        //FB 3058 End

						oStruct.AddString("address", partyAddress.Trim());

						if (party.etVideoEquipment == NS_MESSENGER.Party.eVideoEquipment.RECORDER)
						{
							oStruct.AddString("gatewayAddress", party.sGatewayAddress.Trim());	
						}
                        else if (party.etAddressType == NS_MESSENGER.Party.eAddressType.ISDN_PHONE_NUMBER && party.cMcu.sisdnGateway.Trim() != "")
                        {
                            oStruct.AddString("gatewayAddress", party.cMcu.sisdnGateway.Trim());
                        }

                        // check for gateway  
                        if (party.sAddress.Trim().Contains("G"))
                        {
                             // gateway exists
                            if (party.sAddress.Trim().Contains("D") && party.bAudioBridgeParty) //ZD 101655
                            {
                                // dtmf sequence also exists. Hence, gateway value needs to be extracted between G & D.
                                int Gindex = party.sAddress.IndexOf("G");
                                int Dindex = party.sAddress.IndexOf("D");
                                party.sGatewayAddress = party.sAddress.Substring(Gindex + 1, Dindex - Gindex-1);
                                oStruct.AddString("gatewayAddress", party.sGatewayAddress.Trim());
                            }
                            else
                            {
                                int Gindex = party.sAddress.IndexOf("G");                               
                                party.sGatewayAddress = party.sAddress.Substring(Gindex + 1);
                                oStruct.AddString("gatewayAddress", party.sGatewayAddress.Trim());
                            }
                        }
                        // check for dtmf sequence
                        if (party.sAddress.Trim().Contains("D") && party.bAudioBridgeParty) //FB 2655
                        {
                            // dtmf sequence exists                            
                            int Dindex = party.sAddress.IndexOf("D");
                            party.sDTMFSequence = party.sAddress.Substring(Dindex+1);

                            // FB case#1632
                            // GENERIC STRING: ,,,,,,,,CONFCODE,#,,,,,*,,,,,LEADERPIN,# ,,,,,1 (Codian uses a comma �,� for ,pauses)

                            //FB 1642 - DTMF
                            // add the pre-ConfCode DTMF codes
                            //party.sDTMFSequence = this.configParams.stDTMF_Codian.PreConfCode + party.sDTMFSequence;
                            party.sDTMFSequence = party.preConfCode + party.sDTMFSequence; //FB 2655

                            //FB 1642 - DTMF
                            // add the pre-LeaderPIN DTMF codes
                            //party.sDTMFSequence = this.configParams.stDTMF_Codian.PreConfCode + party.sDTMFSequence;
                            party.sDTMFSequence = party.sDTMFSequence.Replace("+", party.preLPin); //FB 2655

                            //FB 1642 - DTMF
                            // add the post-LeaderPIN DTMF codes
                            //party.sDTMFSequence = party.sDTMFSequence.Trim() + this.configParams.stDTMF_Codian.PostLeaderPin;
                            party.sDTMFSequence = party.sDTMFSequence.Trim() + party.postLPin; //FB 2655

                            oStruct.AddString("dtmfSequence", party.sDTMFSequence.Trim());
                        }
                        //ZD 103263 Start
                        else if (party.sAddress.Trim().Contains("B")) //Blue jean Endpoint
                        {
                            string meetingid = "", passcode = "";
                            if (party.sAddress.IndexOf('+') > 0)
                            {
                                meetingid = party.sAddress.Split('B')[1].Split('+')[0]; //Meeting ID
                                passcode = party.sAddress.Split('B')[1].Split('+')[1]; //Attendee passcode
                            }
                            oStruct.AddString("dtmfSequence",",,,,,##"+ meetingid + "#,,,,,,,,," + passcode + "#");
                        }
                        //ZD 103263 End
                        else if (party.etType == NS_MESSENGER.Party.eType.CASCADE_LINK && conf.iPwd > 0) //ZD 101403
                            oStruct.AddString("dtmfSequence", ",,,,," + conf.iPwd.ToString().ToString() + ",#");
					}
					// Equate db display layout value to value expected by mcu.
                    //ZD 101869 Start
                    string codianLayoutValue = "layout1";
                    if (party.cMcu.iVideoLayoutId >= 100)
                        codianLayoutValue = "family1";

                    //Doubt-Need discussion
                    int videoLayout = conf.iVideoLayout;
                    if (party.etType == NS_MESSENGER.Party.eType.CASCADE_LINK && party.etCascadeRole == NS_MESSENGER.Party.eCascadeRole.MASTER) //ZD 101403
                        videoLayout = 1; //Send a full screen video display to the callee MCU(Master)-DanD.
                    bool ret1 = EquateVideoLayout(videoLayout, ref codianLayoutValue);
                    //ZD 101869 End
					if(!ret1)
					{
						logger.Trace ("Invalid Video Layout.");
						return false;
					}
					oStruct.AddString("cpLayout", codianLayoutValue.Trim());

					// Equate db line rate value to value expected by mcu.
					int codianLineRate = 384;
					bool ret2 = EquateLineRate(party.stLineRate.etLineRate,ref codianLineRate);
					if(!ret2)
					{
						logger.Trace ("Invalid LineRate.");
						return false;
					}					

					oStruct.AddInteger("maxBitRateToMcu", codianLineRate);
					oStruct.AddInteger("maxBitRateFromMcu", codianLineRate);
                    oStruct.AddBoolean("audioTxMuted", party.bMute); //ZD 100628

					break;
                        #endregion

                    }

				case "TerminateParty":
                    {
                   
                    #region Terminate
                    string protocol = "";
                        // Terminate a party in an existing conf.
					oStruct.AddString("conferenceName", conf.sDbName); // conf name
                    //ZD 101056 Starts
                    if(!string.IsNullOrEmpty(party.sMcuName)) //Sending Party name as it in MCU
                        oStruct.AddString("participantName", party.sMcuName);
                    else
					    oStruct.AddString("participantName", party.sName); // party name

                    if (party.etProtocol == NS_MESSENGER.Party.eProtocol.SIP)
                    {
                        protocol = "sip";
                        oStruct.AddString("participantProtocol", protocol); // party Protocol
                    }
                    
                    if (party.etConnType == NS_MESSENGER.Party.eConnectionType.DIAL_IN)
                    {
                        oStruct.AddString("participantType", "ad_hoc");
                    }
                    
                    //ZD 101056 Ends
					break;
                        #endregion

                    }

                case "MuteParty":
                    {
                        #region Mute
                        // Mute an existing party. 
                        string Type = "", protocol = "";
                        logger.Trace("In the struct mute case...");
                        //FB 3058 Start
                        if (party.etType == NS_MESSENGER.Party.eType.VMR)
                            Type = "ad_hoc";
                        else
                            Type = "by_address";

                        oStruct.AddString("conferenceName", conf.sDbName); // conf name
                        //ZD 101056 Starts
                        if (!string.IsNullOrEmpty(party.sMcuName)) //Sending Party name as it in MCU
                            oStruct.AddString("participantName", party.sMcuName);
                        else
                            oStruct.AddString("participantName", party.sName); // party name
                        //ZD 101056 Ends

                        if (party.etProtocol == NS_MESSENGER.Party.eProtocol.SIP)
                        {
                            protocol = "sip";
                            oStruct.AddString("participantProtocol", protocol); // party Protocol
                        }
                        //ZD 101056 Starts
                        if (party.etConnType == NS_MESSENGER.Party.eConnectionType.DIAL_IN)
                        {
                            oStruct.AddString("participantType", "ad_hoc");
                        }
                        else
                           oStruct.AddString("participantType", Type);//Party type
                        //ZD 101056 Ends
                        //FB 3058 End
                        oStruct.AddBoolean("audioRxMuted", party.bMute); // mute
                        break;
                        #endregion

                    }
                case "ChangeDisplayLayoutOfParty":
                    {
                        #region diplay layout
                        string Type = "", protocol = "";
                        // Change display layout of an existing party. 
                        oStruct.AddString("conferenceName", conf.sDbName); // conf name

                        //ZD 101056 Starts
                        if (!string.IsNullOrEmpty(party.sMcuName)) //Sending Party name as it in MCU
                            oStruct.AddString("participantName", party.sMcuName);
                        else
                            oStruct.AddString("participantName", party.sName); // party name
                        //ZD 101056 Ends

                        //FB 3058 Start
                        if (party.etType == NS_MESSENGER.Party.eType.VMR)
                            Type = "ad_hoc";
                        else
                            Type = "by_address";

                        if (party.etProtocol == NS_MESSENGER.Party.eProtocol.SIP)
                        {
                            protocol = "sip";
                            oStruct.AddString("participantProtocol", protocol); // party Protocol
                        }
                        //ZD 101056 Starts
                        if (party.etConnType == NS_MESSENGER.Party.eConnectionType.DIAL_IN)
                        {
                            oStruct.AddString("participantType", "ad_hoc");
                        }
                        else
                            oStruct.AddString("participantType", Type);//Party type
                        //ZD 101056 Ends
                        //FB 3058 End
                        oStruct.AddString("cpLayout", party.sDisplayLayout); // new party layout					
                        break;
                        #endregion

                    }
                case "ChangeDisplayLayoutOfConf":
                    {
                        #region

                        // Change display layout of an existing conf. 
                    oStruct.AddString("conferenceName", conf.sDbName); // conf name
                    
                    // Equate db display layout value to value expected by mcu.
                    logger.Trace("Layout ");
                    //string codianLayoutValue = "layout1";
                    //bool ret1 = EquateVideoLayout(conf.iVideoLayout, ref codianLayoutValue);
                    //if (!ret1)
                    //{
                    //    logger.Trace("Invalid Video Layout.");
                    //    return false;
                    //}
                    //ZD 101869 Starts
                    int customLayout = conf.iVideoLayout;
                    string familyLayoutId = "";
                    if (conf.iVideoLayout >= 100)
                    {
                        familyLayoutId = conf.iVideoLayout.ToString().Substring(2, 1);
                        int.TryParse(familyLayoutId, out customLayout);
                    }
                    if (customLayout <= 0) //No need to send tags for Default Family Layout
                    {
                        oStruct.AddBoolean("customLayoutEnabled", false);
                        oStruct.AddBoolean("newParticipantsCustomLayout", false);
                    }
                    else
                    {
                        oStruct.AddBoolean("customLayoutEnabled", true);
                        oStruct.AddBoolean("newParticipantsCustomLayout", true);
                        oStruct.AddInteger("customLayout", customLayout);
                    }

                    //ZD 101869 Ends
                    break;
                        #endregion

                    }
				case "TerminateConference":
                    {
                        #region

                        // Terminate an ongoing conf.
					oStruct.AddString("conferenceName", conf.sDbName);
					break;
                        #endregion

                    }
                case "FetchMCUDetails": //FB 2448
				case "TestMCUConnection":
				{
					// Intentionally left blank.
					break;
				}
				
				case "FetchAllConfs":
                {
                    #region
                    if (this.confEnumerateID > 0)
						oStruct.AddString("enumerateID", this.confEnumerateID.ToString());
					break;
                    #endregion

                }
				case "FetchAllParticipants":
                {                    
                    if (this.participantEnumerateID > 0)
					{
						try 
						{
							oStruct.AddString("enumerateID", this.participantEnumerateID.ToString());
                            stateArray = new vbxmlrpc_Net.XMLRPCArrayClass(); //FB 2640
                            stateArray.AddString("currentState");
                            oStruct.AddArray("operationScope", stateArray);
						}
						catch (Exception e)
						{
							logger.Exception(100,e.Message);
							return false;
						}
					}
					break;                    
                }
                case "ConnectDisconnectParty":
                {
                  
                    oStruct.AddString("conferenceName", conf.sDbName); //conf name

                    //ZD 101056 Starts
                    if (!string.IsNullOrEmpty(party.sMcuName)) //Sending Party name as it in MCU
                        oStruct.AddString("participantName", party.sMcuName);
                    else
                        oStruct.AddString("participantName", party.sName); // party name
                    //ZD 101056 Ends

                    //ZD 100589 start
                    if (party.etProtocol == NS_MESSENGER.Party.eProtocol.SIP)
                    {
                        oStruct.AddString("participantProtocol","sip"); //party Protocol
                        oStruct.AddBoolean("useSIPRegistrar", true); //ZD 104605 
                    }
                    //ZD 100589 end
                    //ZD 101056 Starts
                    logger.Trace("party.etConnType :" + party.etConnType);
                    if (party.etConnType == NS_MESSENGER.Party.eConnectionType.DIAL_IN)
                    {
                        oStruct.AddString("participantType", "ad_hoc");
                    }
                    //ZD 101056 Ends
                    break;
                  

                }
                case "ConnectedParticipantList":
                {                                       
                    oStruct.AddString("enumerateFilter","connected"); // conf name
                    stateArray = new vbxmlrpc_Net.XMLRPCArrayClass(); //FB 2640
                    stateArray.AddString("currentState");
                    oStruct.AddArray("operationScope", stateArray);
                    if (this.participantEnumerateID > 0)
                    {
                        oStruct.AddString("enumerateID", this.participantEnumerateID.ToString()); // enumeration number to fetch the next page
                    }
                    break;
                    
                }
                case "DisconnectedParticipantList":
                {
                    oStruct.AddString("enumerateFilter", "disconnected"); // conf name
                    stateArray = new vbxmlrpc_Net.XMLRPCArrayClass(); //FB 2640
                    stateArray.AddString("currentState");
                    oStruct.AddArray("operationScope", stateArray);
                    if (this.participantEnumerateID > 0)
                    {
                        oStruct.AddString("enumerateID", this.participantEnumerateID.ToString()); // enumeration number to fetch the next page
                    }
                    break;

                }
                // FB 2501 Call Monitoring Start
                case "ConferenceStatus":
                {
                    #region
                    // Get an ongoing conf status.
                    oStruct.AddString("conferenceName", conf.sDbName);
                    break;
                    #endregion

                }
                case "ChangeParticipantFECC":
                {
                    #region ParticipantFECC

                    string Direction = "", protocol = "", Type = "";
                    bool ret2 = ParticipantFECCDirection(party.sFECCdirection, ref Direction);
                    if (!ret2)
                    {
                        logger.Trace("Invalid FECCDirection.");
                        return false;
                    }
                    
                    if (party.etType == NS_MESSENGER.Party.eType.VMR)
                        Type = "ad_hoc";
                    else
                        Type = "by_address";

                    oStruct.AddString("conferenceName", conf.sDbName); // conf name

                    //ZD 101056 Starts
                    if (!string.IsNullOrEmpty(party.sMcuName)) //Sending Party name as it in MCU
                        oStruct.AddString("participantName", party.sMcuName);
                    else
                        oStruct.AddString("participantName", party.sName); // party name
                    //ZD 101056 Ends

                    //FB 3058 Start
                    if (party.etProtocol == NS_MESSENGER.Party.eProtocol.SIP)
                    {
                        protocol = "sip";
                        oStruct.AddString("participantProtocol", protocol); // party Protocol
                    }
                    //FB 3058 End
                    //oStruct.AddString("autoAttendantUniqueID", "");
                    //ZD 101056 Starts
                    if (party.etConnType == NS_MESSENGER.Party.eConnectionType.DIAL_IN)
                    {
                        oStruct.AddString("participantType", "ad_hoc");
                    }
                    else
                        oStruct.AddString("participantType", Type);//Party type
                    //ZD 101056 Ends
                    oStruct.AddString("direction", Direction); // Party Camera direction			
                    break;

                    #endregion
                }
                case "SendParticipantMessage":
                {
                    #region SendParticipantMessage

                    string Direction = "", protocol = "", Type = "";
                    bool ret2 = MessageDirection(party.sMessagedirection, ref Direction);
                    if (!ret2)
                    {
                        logger.Trace("Invalid SendParticipantMessage.");
                        return false;
                    }
                    
                    if (party.etType == NS_MESSENGER.Party.eType.VMR)
                        Type = "ad_hoc";
                    else
                        Type = "by_address";

                    oStruct.AddString("conferenceName", conf.sDbName); // conf name

                    //ZD 101056 Starts
                    if (!string.IsNullOrEmpty(party.sMcuName)) //Sending Party name as it in MCU
                        oStruct.AddString("participantName", party.sMcuName);
                    else
                        oStruct.AddString("participantName", party.sName); // party name
                    //ZD 101056 Ends

                    //FB 3058 Start
                    if (party.etProtocol == NS_MESSENGER.Party.eProtocol.SIP)
                    {
                        protocol = "sip";
                        oStruct.AddString("participantProtocol", protocol); // party Protocol
                    }
                    //FB 3058 End
                    //oStruct.AddString("autoAttendantUniqueID", "");
                    //ZD 101056 Starts
                    if (party.etConnType == NS_MESSENGER.Party.eConnectionType.DIAL_IN)
                    {
                        oStruct.AddString("participantType", "ad_hoc");
                    }
                    else
                        oStruct.AddString("participantType", Type);//Party type
                    //ZD 101056 Ends
                    oStruct.AddString("message", party.sMessage);//Party type
                    oStruct.AddString("verticalPosition", Direction); // Optional			
                    oStruct.AddInteger("durationSeconds", party.iMessDuration);// Optional		
                    break;

                    #endregion
                }
                case "ConferenceMessage":
                {
                    #region ConferenceMessage

                    string Direction = "", protocol = "", Type = "";
                    bool ret2 = MessageDirection(party.sMessagedirection, ref Direction);
                    if (!ret2)
                    {
                        logger.Trace("Invalid ConferenceMessage.");
                        return false;
                    }
                    
                    if (party.etType == NS_MESSENGER.Party.eType.VMR)
                        Type = "ad_hoc";
                    else
                        Type = "by_address";

                    oStruct.AddString("conferenceName", conf.sDbName); // conf name

                    //ZD 101056 Starts
                    if (!string.IsNullOrEmpty(party.sMcuName)) //Sending Party name as it in MCU
                        oStruct.AddString("participantName", party.sMcuName);
                    else
                        oStruct.AddString("participantName", party.sName); // party name
                    //ZD 101056 Ends

                    //FB 3058 Start
                    if (party.etProtocol == NS_MESSENGER.Party.eProtocol.SIP)
                    {
                        protocol = "sip";
                        oStruct.AddString("participantProtocol", protocol); // party Protocol
                    }
                    //FB 3058 End
                    //oStruct.AddString("autoAttendantUniqueID", "");
                    //ZD 101056 Starts
                    if (party.etConnType == NS_MESSENGER.Party.eConnectionType.DIAL_IN)
                    {
                        oStruct.AddString("participantType", "ad_hoc");
                    }
                    else
                        oStruct.AddString("participantType", Type);//Party type
                    //ZD 101056 Ends
                    oStruct.AddString("message", party.sMessage);//Party type
                    oStruct.AddString("verticalPosition", Direction); // Optional			
                    oStruct.AddInteger("durationSeconds", party.iMessDuration);// Optional		
                    break;

                    #endregion
                }
                case "AudioGainMode":
                {
                    #region AudioGainMode

                    string Mode = "", protocol = "", Type = "";
                    bool ret2 = AudioGainMode(party.iAudioMode, ref Mode);
                    if (!ret2)
                    {
                        logger.Trace("Invalid FECCDirection.");
                        return false;
                    }
                    
                    if (party.etType == NS_MESSENGER.Party.eType.VMR)
                        Type = "ad_hoc";
                    else
                        Type = "by_address";

                    oStruct.AddString("conferenceName", conf.sDbName); // conf name
                    oStruct.AddString("participantName", party.sName); // party name	
                    //FB 3058 Start
                    if (party.etProtocol == NS_MESSENGER.Party.eProtocol.SIP)
                    {
                        protocol = "sip";
                        oStruct.AddString("participantProtocol", protocol); // party Protocol
                    }
                    //FB 3058 End
                    //oStruct.AddString("autoAttendantUniqueID", "");
                    //oStruct.AddString("operationScope","");
                    oStruct.AddString("participantType", Type);//Party type
                    oStruct.AddString("audioRxGainMode", Mode);//none, automatic, or fixed.		
                    if (Mode == "fixed")
                        oStruct.AddInteger("audioRxGainMillidB", party.iAudioGained);// If audio gain mode is fixed, this is the number of decibels of
                    //  gain applied, multiplied by 1000, and can be a negative value.	
                    break;

                    #endregion
                }
                case "MutePartyTransmit":
                {
                    #region Mute Transmit
                    // Mute an existing party. 
                    logger.Trace("In the struct mute case...");
                    string protocol = "", Type = "";

                    if (party.etType == NS_MESSENGER.Party.eType.VMR)
                        Type = "ad_hoc";
                    else
                        Type = "by_address";

                    oStruct.AddString("conferenceName", conf.sDbName); // conf name
                    //ZD 101056 Starts
                    if (!string.IsNullOrEmpty(party.sMcuName)) //Sending Party name as it in MCU
                        oStruct.AddString("participantName", party.sMcuName);
                    else
                        oStruct.AddString("participantName", party.sName); // party name
                    //ZD 101056 Ends
                    //FB 3058 Start
                    if (party.etProtocol == NS_MESSENGER.Party.eProtocol.SIP)
                    {
                        protocol = "sip";
                        oStruct.AddString("participantProtocol", protocol); // party Protocol
                    }
                    //FB 3058 End
                    //oStruct.AddString("autoAttendantUniqueID", "");
                    //oStruct.AddString("operationScope","");
                    //ZD 101056 Starts
                    if (party.etConnType == NS_MESSENGER.Party.eConnectionType.DIAL_IN)
                    {
                        oStruct.AddString("participantType", "ad_hoc");
                    }
                    else
                        oStruct.AddString("participantType", Type);//Party type
                    //ZD 101056 Ends
                    oStruct.AddBoolean("audioTxMuted", party.bMute); // mute
                    break;

                    #endregion
                }
                case "MutePartyVideoReceiver":
                {
                    #region Mute Transmit
                    // Mute an existing party. 
                    logger.Trace("In the struct mute case...");
                    string protocol = "", Type = "";

                    if (party.etType == NS_MESSENGER.Party.eType.VMR)
                        Type = "ad_hoc";
                    else
                        Type = "by_address";

                    oStruct.AddString("conferenceName", conf.sDbName); // conf name
                    //ZD 101056 Starts
                    if (!string.IsNullOrEmpty(party.sMcuName)) //Sending Party name as it in MCU
                        oStruct.AddString("participantName", party.sMcuName);
                    else
                        oStruct.AddString("participantName", party.sName); // party name
                    //ZD 101056 Ends
                    //FB 3058 Start
                    if (party.etProtocol == NS_MESSENGER.Party.eProtocol.SIP)
                    {
                        protocol = "sip";
                        oStruct.AddString("participantProtocol", protocol); // party Protocol
                    }
                    //FB 3058 End
                    //ZD 101056 Starts
                    if (party.etConnType == NS_MESSENGER.Party.eConnectionType.DIAL_IN)
                    {
                        oStruct.AddString("participantType", "ad_hoc");
                    }
                    else
                        oStruct.AddString("participantType", Type);//Party type
                    //ZD 101056 Ends
                    oStruct.AddBoolean("videoRxMuted", party.bMute); // mute
                    break;

                    #endregion
                }
                case "ParticipantStatus":
                {
                    #region ParticipantStatus
                    string protocol = "", Type = "";
                   
                    if (party.etType == NS_MESSENGER.Party.eType.VMR)
                        Type = "ad_hoc";
                    else
                        Type = "by_address";

                    oStruct.AddString("conferenceName", conf.sDbName); // conf name

                    //ZD 101056 Starts
                    if (!string.IsNullOrEmpty(party.sMcuName)) //Sending Party name as it in MCU
                        oStruct.AddString("participantName", party.sMcuName);
                    else
                        oStruct.AddString("participantName", party.sName); // party name
                    //ZD 101056 Ends

                    //FB 3058 Start
                    if (party.etProtocol == NS_MESSENGER.Party.eProtocol.SIP)
                    {
                        protocol = "sip";
                        oStruct.AddString("participantProtocol", protocol); // party Protocol
                    }
                    //FB 3058 End
                    //oStruct.AddString("autoAttendantUniqueID", "");
                    //oStruct.AddString("operationScope","");
                    //ZD 101056 Starts
                    if (party.etConnType == NS_MESSENGER.Party.eConnectionType.DIAL_IN)
                    {
                        oStruct.AddString("participantType", "ad_hoc");
                    }
                    else
                        oStruct.AddString("participantType", Type);//Party type
                    //ZD 101056 Ends
                    break;
                    #endregion

                }
                case "LockOrUnLockConference":
                {
                    #region LockOrUnLockConference
                    logger.Trace("locked = LockOrUnLockConference");

                    oStruct.AddString("conferenceName", conf.sDbName);

                    if (conf.ilockorunlock == 1)
                    {
                        oStruct.AddBoolean("locked", true);
                    }
                    else
                        oStruct.AddBoolean("locked", false);

                    logger.Trace("locked = " + conf.ilockorunlock.ToString());

                    break;

                    #endregion
                }
                case "ParticipantDiagnostics":
                {
                    #region ParticipantDiagnostics
                    string protocol = "", Type = "";
                    
                    if (party.etType == NS_MESSENGER.Party.eType.VMR)
                        Type = "ad_hoc";
                    else
                        Type = "by_address";

                    oStruct.AddString("conferenceName", conf.sDbName); // conf name

                    //ZD 101056 Starts
                    if (!string.IsNullOrEmpty(party.sMcuName)) //Sending Party name as it in MCU
                        oStruct.AddString("participantName", party.sMcuName);
                    else
                        oStruct.AddString("participantName", party.sName); // party name
                    //ZD 101056 Ends

                    //FB 3058 Start
                    if (party.etProtocol == NS_MESSENGER.Party.eProtocol.SIP)
                    {
                        protocol = "sip";
                        oStruct.AddString("participantProtocol", protocol); // party Protocol
                    }
                    //FB 3058 End
                    //ZD 101056 Starts
                    if (party.etConnType == NS_MESSENGER.Party.eConnectionType.DIAL_IN)
                    {
                        oStruct.AddString("participantType", "ad_hoc");
                    }
                    else
                        oStruct.AddString("participantType", Type);//Party type
                    //ZD 101056 Ends
                    break;
                    #endregion
                }
                // FB 2501 Call Monitoring End
                //FB 2797 Start
                case "CallDetailRecords":
                {
                    #region CallDetailRecords
                    oStruct.AddInteger("index", indexCDR);
                    #endregion
                }
                break;
                //FB 2797 End
                //ZD 100522 Start
                case "ConferenceVMRCreate":
                {
                    #region ConferenceVMRCreate
                 
                    oStruct.AddString("conferenceName", conf.sDbName);

                    if (conf.sDialinNumber != "")
                    {
                        oStruct.AddString("numericId", conf.sDialinNumber.ToString());//FB 2659
                        logger.Trace("Conf iDialinNumber ID: " + conf.sDialinNumber.ToString());
                    }
                    else
                    {
                        oStruct.AddString("numericId", conf.iDbNumName.ToString());
                        logger.Trace("Conf Unique ID: " + conf.iDbNumName.ToString());
                    }

                    // Add the numeric password 
                    if (conf.iPwd > 0)
                    {
                        oStruct.AddString("pin", conf.iPwd.ToString());
                    }

                    break;
                    #endregion

                }
                case "ConferenceVMRUpdate":
                {
                    #region ConferenceVMRUpdate

                    oStruct.AddString("conferenceName", conf.sDbName);

                    if (conf.sDialinNumber != "")
                    {
                        oStruct.AddString("numericId", conf.sDialinNumber.ToString());//FB 2659
                        logger.Trace("Conf iDialinNumber ID: " + conf.sDialinNumber.ToString());
                    }
                    else
                    {
                        oStruct.AddString("numericId", conf.iDbNumName.ToString());
                        logger.Trace("Conf Unique ID: " + conf.iDbNumName.ToString());
                    }

                    // Add the numeric password 
                    if (conf.iPwd > 0)
                    {
                        oStruct.AddString("pin", conf.iPwd.ToString());
                    }
                    else
                        oStruct.AddString("pin", "");

                    break;
                    #endregion
                    
                }
				default : 
				{
					// no match 
					return false;
				}
			}			
			return true;			
		}
		
		private bool EquateVideoLayout (int videoLayoutID, ref string codianLayoutValue) //ZD 101869
		{
			try
			{	
				if (videoLayoutID < 1) 
					videoLayoutID = 1;
                //ZD 101869 Start

                if (videoLayoutID >= 100)
                {
                    string familyLayoutId = videoLayoutID.ToString().Substring(2, 1);

                    if (familyLayoutId.Trim() == "0")
                        codianLayoutValue = "default";
                    else
                        codianLayoutValue = "family" + familyLayoutId.Trim();
                }
                else
                    codianLayoutValue = "layout" + videoLayoutID.ToString();
                //ZD 101869 End
				return true;
			}
			catch (Exception e)
			{
				logger.Exception(100,e.Message);
				return false;
			}
		
		}

		private bool EquateLineRate (NS_MESSENGER.LineRate.eLineRate lineRateValue, ref int codianLineRate)
		{
			try 
			{  
				switch(lineRateValue)
				{
					case NS_MESSENGER.LineRate.eLineRate.K64:
					{
						codianLineRate = 64;						
						break;
					}
                case NS_MESSENGER.LineRate.eLineRate.K128:
					{
						codianLineRate = 128;						
						break;
					}
                case NS_MESSENGER.LineRate.eLineRate.K192:
                    {
                        codianLineRate = 192;
                        break;
                    }
                case NS_MESSENGER.LineRate.eLineRate.K256:
					{
						codianLineRate = 256;												
						break;
					}
                case NS_MESSENGER.LineRate.eLineRate.K320:
                    {
                        codianLineRate = 320;
                        break;
                    }
                case NS_MESSENGER.LineRate.eLineRate.K384:
					{
						codianLineRate = 384;						
						break;
					}
                case NS_MESSENGER.LineRate.eLineRate.K512:
					{
						codianLineRate = 512;						
						break;
					}
                case NS_MESSENGER.LineRate.eLineRate.K768:
					{
						codianLineRate = 768;												
						break;
					}
                case NS_MESSENGER.LineRate.eLineRate.M1024:
                    {
                        codianLineRate = 1024;
                        break;
                    }
                case NS_MESSENGER.LineRate.eLineRate.M1152:
					{
						codianLineRate = 1024;						
						break;
					}
                case NS_MESSENGER.LineRate.eLineRate.M1250:
                    {
                        codianLineRate = 1250;
                        break;
                    }
                case NS_MESSENGER.LineRate.eLineRate.M1472:
					{
						codianLineRate = 1250;						
						break;
					}
                
                case NS_MESSENGER.LineRate.eLineRate.M1536:
					{
						codianLineRate = 1536;						
						break;
					}
                case NS_MESSENGER.LineRate.eLineRate.M1792:
                    {
                        codianLineRate = 1792;
                        break;
                    }
                case NS_MESSENGER.LineRate.eLineRate.M1920:
					{
						codianLineRate = 1792;						
						break;
					}
                case NS_MESSENGER.LineRate.eLineRate.M2048:
                    {
                        codianLineRate = 2048;
                        break;
                    }
                case NS_MESSENGER.LineRate.eLineRate.M2560:
                    {
                        codianLineRate = 2560;
                        break;
                    }
                case NS_MESSENGER.LineRate.eLineRate.M3072:
                    {
                        codianLineRate = 3072;
                        break;
                    }
                case NS_MESSENGER.LineRate.eLineRate.M3584:
                    {
                        codianLineRate = 3584;
                        break;
                    }
                case NS_MESSENGER.LineRate.eLineRate.M4096:
                    {
                        codianLineRate = 4096;
                        break;
                    }
				default:
					{
						// default is 384 kbps
						codianLineRate = 384;						
						break;
					}
				}				
				return true;
			}
			catch (Exception e)
			{
				logger.Exception(100,e.Message);
				return false;
			}
		}

		private bool SendCommand(NS_MESSENGER.MCU mcu, string strMethodName, vbxmlrpc_Net.XMLRPCStruct oStruct, ref string response)
		{
			try
			{	// Initialize the RPC structures. These would be used to communicate with mcu.
				vbxmlrpc_Net.XMLRPCRequest oRpc = new vbxmlrpc_Net.XMLRPCRequestClass();
				vbxmlrpc_Net.XMLRPCResponse oResponse = new vbxmlrpc_Net.XMLRPCResponseClass();
			
				// Assign the mcu details
				oRpc.HostName = mcu.sIp;
				oRpc.Username = mcu.sLogin;
				oRpc.Password = mcu.sPwd;
				oRpc.HostURI = "/RPC2";
                oRpc.HostPort = mcu.iHttpPort;//80;Code added for MCU port	
				oRpc.Params.AddStruct(oStruct);
				
				// Assign the method call 
				oRpc.MethodName = strMethodName;
				logger.Trace(oRpc.XMLToSend.ToString());
				
				// Send command to mcu.
				oResponse = oRpc.Submit();
                response = oResponse.XMLResponse.ToString();
				logger.Trace(oResponse.XMLResponse.ToString());
                
				// Check for mcu-returned faults
				if(oResponse.XMLResponse.IndexOf("<fault>", 0) > 0)
				{
					logger.Trace("SendXml = " + oRpc.XMLToSend.ToString());
					logger.Trace("ResponseXml = " + oResponse.XMLResponse.ToString());
					ProcessCodianFaultResponse(mcu.iDbId,strMethodName,oResponse.XMLResponse);
					return false;
				}
				else
				{
					// No faults. MCU returned some response.
					this.errMsg = oResponse.XMLResponse;					
				}				
				return true;
			}
			catch(Exception e)
			{				
				logger.Exception(100,e.Message);
				return false;
			}
		}

        //ZD 100369_MCU Start
        private bool SendMCUStatusCommand(NS_MESSENGER.MCU mcu, string strMethodName, vbxmlrpc_Net.XMLRPCStruct oStruct, ref string response, int Timeout)
        {
            try
            {	// Initialize the RPC structures. These would be used to communicate with mcu.
                vbxmlrpc_Net.XMLRPCRequest oRpc = new vbxmlrpc_Net.XMLRPCRequestClass();
                vbxmlrpc_Net.XMLRPCResponse oResponse = new vbxmlrpc_Net.XMLRPCResponseClass();

                // Assign the mcu details
                oRpc.HostName = mcu.sIp;
                oRpc.Username = mcu.sLogin;
                oRpc.Password = mcu.sPwd;
                oRpc.HostURI = "/RPC2";
                oRpc.HostPort = mcu.iHttpPort;//80;Code added for MCU port	
                oRpc.Params.AddStruct(oStruct);

                // Assign the method call 
                oRpc.MethodName = strMethodName;
                logger.Trace(oRpc.XMLToSend.ToString());

                // Send command to mcu.
                oResponse = oRpc.Submit();
                response = oResponse.XMLResponse.ToString();
                logger.Trace(oResponse.XMLResponse.ToString());

                // Check for mcu-returned faults
                if (oResponse.XMLResponse.IndexOf("<fault>", 0) > 0)
                {
                    logger.Trace("SendXml = " + oRpc.XMLToSend.ToString());
                    logger.Trace("ResponseXml = " + oResponse.XMLResponse.ToString());
                    ProcessCodianFaultResponse(mcu.iDbId, strMethodName, oResponse.XMLResponse);
                    return false;
                }
                else
                {
                    // No faults. MCU returned some response.
                    this.errMsg = oResponse.XMLResponse;
                }
                return true;
            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
                return false;
            }
        }
        //ZD 100369_MCU End

		private void ProcessCodianFaultResponse(int intMcuID, string strFunctionCall,string strFaultXML)
		{
			try
			{
				string strFaultCode = null,strFaultMsg = null;

				// Load the incoming xml in parser.
				XmlDocument xDoc = new XmlDocument();
				xDoc.LoadXml(strFaultXML);
				
				// Node list.
				XmlNodeList oNodeList = xDoc.SelectNodes("//member");
				
				// Start retreiving the fault codes.
				foreach(XmlNode oNode in oNodeList)
				{
					if(oNode.SelectSingleNode("name").InnerText == "faultCode")
					{
						strFaultCode = oNode.SelectSingleNode("value/int").InnerText.ToString();
					}
					else 
					{
						if(oNode.SelectSingleNode("name").InnerText == "faultString")
						{
							strFaultMsg = oNode.SelectSingleNode("value/string").InnerText.ToString();
						}
					}
				}

				// Fault message
				this.errMsg = "Operation failed. Message: " + strFaultMsg + "(Codian API error code - " + strFaultCode + ").";				
				logger.Trace (this.errMsg);;
			}
			catch(Exception e)
			{
				logger.Exception(100,e.Message);
			}			
		}

		internal bool MuteEndpoint (NS_MESSENGER.Conference conf, NS_MESSENGER.Party party,bool mute)
		{
			try 
			{
				// Create the RPC struct
				logger.Trace ("Entering MuteEndpoint method...Mute = " + mute.ToString());
				vbxmlrpc_Net.XMLRPCStruct oStruct = new vbxmlrpc_Net.XMLRPCStructClass();
				party.bMute = mute;
				bool ret = CreateStruct("MuteParty", party, conf, ref oStruct);
				if (!ret)
				{
					logger.Trace ("Struct problem.");
					return false;
				}
				
				// Send the command to the mcu
				logger.Trace ("Sending mute cmd to codian mcu...");
				ret = false;string strResponse = null;
				ret = SendCommand(party.cMcu,"participant.modify", oStruct,ref strResponse);				
				if (!ret)
				{	
					logger.Trace ("SendCommand failed");
					return false;
				}
				return true;
			}
			catch (Exception e)
			{
				logger.Exception (100,e.Message);
			}
			return true;
		}

        //FB 2249 start
        internal bool ModifyEndpoint(NS_MESSENGER.Conference conf, NS_MESSENGER.Party party)
        {
            try
            {
                // Create the RPC struct
                logger.Trace("Entering ModifyEndpoint method..");
                vbxmlrpc_Net.XMLRPCStruct oStruct = new vbxmlrpc_Net.XMLRPCStructClass();
                bool ret = CreateStruct("ModifyEndpoint", party, conf, ref oStruct);
                if (!ret)
                {
                    logger.Trace("Error in creating modify struct...");
                    return false;
                }

                // Send the command to the mcu
                logger.Trace("Sending ModifyEndpoint cmd to codian mcu...");
                ret = false; string strResponse = null;
                ret = SendCommand(party.cMcu, "participant.modify", oStruct, ref strResponse);
                if (!ret)
                {
                    logger.Trace("SendCommand failed for modify endpoint operation");
                    return false;
                }
                return true;
            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
            }
            return true;
        }
        //FB 2249 end

		internal bool TerminateEndpoint(NS_MESSENGER.Conference conf, NS_MESSENGER.Party party)
		{
			try 
			{
				// Create the RPC struct
				logger.Trace ("Entering TerminateEndpoint method...");
				vbxmlrpc_Net.XMLRPCStruct oStruct = new vbxmlrpc_Net.XMLRPCStructClass();			
				bool ret = CreateStruct("TerminateParty", party, conf, ref oStruct);
				if (!ret)
				{
					logger.Trace ("Struct problem.");
					return false;
				}
				
				// Send the command to the mcu
				logger.Trace ("Sending terminate party cmd to codian mcu...");
                ret = false; string strResponse = null;
				ret = SendCommand(party.cMcu,"participant.remove", oStruct,ref strResponse);				
				if (!ret)
				{	
					logger.Trace ("SendCommand failed");
					return false;
				}
				return true;
			}
			catch (Exception e)
			{
				logger.Exception (100,e.Message);
			}
			return true;
		}

		internal bool ChangeConfDisplayLayout(NS_MESSENGER.Conference conf, NS_MESSENGER.Party party,int displayLayout)
		{
            try 
			{
			// NOTE : This feature is only supported from Codian firmware version "2.6" or above.
            // Check codian firmware release version. 
            //if (conf.cMcu.etType == NS_MESSENGER.MCU.eType.CODIAN && conf.cMcu.dblSoftwareVer < 2.6)
            //{
            //    this.errMsg = "This operation is not supported on the Codian firmware release version 2.5 or below.";
            //    return false;
            //}

            // Create the RPC struct
            logger.Trace("Entering Change Conference Display Layout method...");
            vbxmlrpc_Net.XMLRPCStruct oStruct = new vbxmlrpc_Net.XMLRPCStructClass();
            //conf.sDisplayLayout = "layout" + displayLayout.ToString();
            NS_MESSENGER.Party dummyParty = new NS_MESSENGER.Party();
            dummyParty.cMcu = conf.cMcu;
            conf.iVideoLayout = displayLayout;

            bool ret = CreateStruct("ChangeDisplayLayoutOfConf", dummyParty, conf, ref oStruct);
            if (!ret)
            {
                logger.Trace("Struct problem.");
                return false;
            }

            // Send the command to the mcu
            logger.Trace("Sending change display layout cmd to codian mcu...");
            ret = false; string strResponse = null;
            ret = SendCommand(conf.cMcu, "conference.modify", oStruct,ref strResponse);
            if (!ret)
            {
                logger.Trace("SendCommand failed");
                return false;
            }

                return true;
			}
			catch (Exception e)
			{
				logger.Exception (100,e.Message);
                return false;
			}		            
		}

		internal bool ChangePartyDisplayLayout(NS_MESSENGER.Conference conf, NS_MESSENGER.Party party,int displayLayout)
		{
			try 
			{
				// Create the RPC struct
				logger.Trace ("Entering DisplayLayout method...");
				vbxmlrpc_Net.XMLRPCStruct oStruct = new vbxmlrpc_Net.XMLRPCStructClass();
                
                string codianLayoutValue = "layout1";
                bool ret1 = EquateVideoLayout(displayLayout, ref codianLayoutValue);
                party.sDisplayLayout = codianLayoutValue;
				bool ret = CreateStruct("ChangeDisplayLayoutOfParty", party, conf, ref oStruct);
				if (!ret)
				{
					logger.Trace ("Struct problem.");
					return false;
				}
				
				// Send the command to the mcu
				logger.Trace ("Sending change display layout cmd to codian mcu...");
                ret = false; string strResponse = null;
				ret = SendCommand(party.cMcu,"participant.modify", oStruct,ref strResponse);				
				if (!ret)
				{	
					logger.Trace ("SendCommand failed");
					return false;
				}
				return true;
			}
			catch (Exception e)
			{
				logger.Exception (100,e.Message);
				return (false);
			}
			
		}

		internal bool ExtendConfEndTime(NS_MESSENGER.Conference conf, int extendTime,ref string msg)
		{
			// NOTE : This feature is only supported from Codian firmware version "1.4" or above.
			// Check codian firmware release version. 
			//if (conf.cMcu.etType == NS_MESSENGER.MCU.eType.CODIAN && conf.cMcu.dblSoftwareVer < 1.4 ) 
			//{
			//	msg = "This operation is not supported on the Codian firmware release version 1.3 or below . Please contact myVRM Technical Support for assistance.";
			//	return false;				
			//}
			
			// Create the RPC struct
			logger.Trace ("Entering Extend Conference Time method...");
			vbxmlrpc_Net.XMLRPCStruct oStruct = new vbxmlrpc_Net.XMLRPCStructClass();			
			NS_MESSENGER.Party dummyParty = new NS_MESSENGER.Party();
			dummyParty.cMcu = conf.cMcu;
			conf.iDuration = conf.iDuration + extendTime;

			bool ret = CreateStruct("ExtendConferenceEndTime", dummyParty, conf, ref oStruct);
			if (!ret)
			{
				logger.Trace ("Struct problem.");
				return false;
			}
				
			// Send the command to the mcu
			logger.Trace ("Sending modify conference cmd to codian mcu...");
            ret = false; string strResponse = null;
			ret = SendCommand(conf.cMcu,"conference.modify", oStruct,ref strResponse);				
			if (!ret)
			{	
				logger.Trace ("SendCommand failed");
				return false;
			}			
			
			return true;
		}

		internal bool TerminateConference (NS_MESSENGER.Conference conf)
		{
			try 
			{
				// Create the RPC struct
				logger.Trace ("Entering Terminate Conference method...");
				vbxmlrpc_Net.XMLRPCStruct oStruct = new vbxmlrpc_Net.XMLRPCStructClass();			
				NS_MESSENGER.Party dummyParty = new NS_MESSENGER.Party();
				dummyParty.cMcu = conf.cMcu;
				bool ret = CreateStruct("TerminateConference", dummyParty, conf, ref oStruct);
				if (!ret)
				{
					logger.Trace ("Struct problem.");
					return false;
				}
				
				// Send the command to the mcu
				logger.Trace ("Sending terminate conference cmd to codian mcu...");
                ret = false; string strResponse = null;
				ret = SendCommand(conf.cMcu,"conference.destroy", oStruct,ref strResponse);				
				if (!ret)
				{	
					logger.Trace ("SendCommand failed");
					return false;
				}				
				return true;
			}
			catch (Exception e)
			{
				logger.Exception (100,e.Message);
				return (false);
			}
		}
		
		internal bool TestMCUConnection (NS_MESSENGER.MCU mcu)
		{
			try 
			{				
				logger.Trace ("Entering Test MCU Connection method...");
				
				// Create a empty RPC struct
				vbxmlrpc_Net.XMLRPCStruct oStruct = new vbxmlrpc_Net.XMLRPCStructClass();							
				NS_MESSENGER.Party dummyParty = new NS_MESSENGER.Party();
				NS_MESSENGER.Conference dummyConf = new NS_MESSENGER.Conference();
				dummyParty.cMcu = mcu;
				bool ret = CreateStruct("TestMCUConnection", dummyParty, dummyConf, ref oStruct);
				if (!ret)
				{
					logger.Trace ("Struct problem.");
					return false;
				}

				// Send the command to the mcu
				logger.Trace ("Sending system query cmd to codian mcu...");
                ret = false; string strResponse = null;
				ret = SendCommand(mcu,"device.query", oStruct,ref strResponse);				
				if (!ret)
				{	
					logger.Trace ("Connection to MCU failed. Please recheck the MCU information.");
					return false;
				}				
				return true;
			}
			catch (Exception e)
			{
				logger.Exception(100,e.Message);
				return false;
			}			
		}
		#endregion

		#region ConfMonitor Methods
		internal bool FetchAllOngoingConfsStatus(NS_MESSENGER.MCU mcu , ref Queue confq)
			{
				try
				{
					bool ret = false;

					// fetch all participants
					Queue partyq = new Queue();
					this.participantEnumerateID = 0;
					do
					{
						// instantiating new objects
						vbxmlrpc_Net.XMLRPCStruct oStruct = new vbxmlrpc_Net.XMLRPCStructClass();
						NS_MESSENGER.Party party = new NS_MESSENGER.Party();
						NS_MESSENGER.Conference conference = new NS_MESSENGER.Conference();
						party.cMcu = mcu;
						conference.cMcu = mcu;

						// create the xml-rpc object
						ret = false;
						ret= CreateStruct("FetchAllParticipants",party,conference, ref oStruct);					
						if (!ret)
						{
							logger.Trace ("Create Struct failed.");
							return false;
						}						
					
						// send the cmd to codian mcu
                        string strResponse = null; ret = false; 
						ret = SendCommand(mcu,"participant.enumerate", oStruct,ref strResponse);
						if(!ret)
						{
							logger.Trace ("Send command failed.");
							return false;
						}

						// process the response
						ret=false; 
						ret = ProcessXML_AllPartyList(strResponse, ref partyq);			
						if(!ret)
						{
							logger.Trace ("Failure in processing the conference list returned by Codian MCU");
							return false;
						}						
					}
					while (this.participantEnumerateID > 0); //keep repeating since thr are more confs to be retreived from the codian mcu.

					// array to store all conf names
					// magic number = 200 (used becos thats the max number that can be stored in codian history)
					int maxArrayCount = 200;
					NS_MESSENGER.Conference[] confList = new NS_MESSENGER.Conference[200];
					logger.Trace ("Before the conf name array initalization");
					for (int i = 1 ; i < maxArrayCount ; i++)
					{
						//array initialization
						confList[i] = new NS_MESSENGER.Conference();
						confList[i].sMcuName = "";						
					}

					logger.Trace ("After the conf name array initalization");

					// combine all participants in conf objects
					int lastElementIndexAddedInArray = 1;
					int partyCount = partyq.Count;
					logger.Trace ("Party Count = " + partyCount.ToString());
					for (int i=0;i<partyCount; i++)
					{
						logger.Trace ("Party#" + i.ToString());

						// get the party from the queue
						NS_MESSENGER.Party party = new NS_MESSENGER.Party();
						party = (NS_MESSENGER.Party) partyq.Dequeue();

						if (party.sConfNameOnMcu == null)
						{
							logger.Trace ("Invalid party. No conf name associated.");
							continue;
						}
						
						if (party.sConfNameOnMcu.Length < 3)
						{
							logger.Trace ("Invalid party. Conf name associated is too short.");
							continue;
						}

						logger.Trace ("Got the party info.");

						// compare the confs 
						bool isConfNameInList = false;
						for (int j=1;j < maxArrayCount ; j++)
						{							
							if (party.sConfNameOnMcu.Equals(confList[j].sMcuName))
							{
								logger.Trace ("Conf exists so skip adding it.");
								// conf exists in the array so skip adding it
								isConfNameInList = true;
								confList[j].qParties.Enqueue(party);
								break;
							}
						}

						// conf name is not in the list
						// so add the new conf and attach the participant to the conf list
						if (isConfNameInList == false)
						{
							logger.Trace ("New conf found. Added to list.");

							// add the conf name in list							
							confList[lastElementIndexAddedInArray].sMcuName = party.sConfNameOnMcu;
							confList[lastElementIndexAddedInArray].qParties.Enqueue(party);	
							lastElementIndexAddedInArray++;
						}						
					}
					
					// attach confs in queue
					for (int k = 1 ; k < lastElementIndexAddedInArray ; k++)
					{
						confq.Enqueue((NS_MESSENGER.Conference)confList[k]);
					}

					return true;
				}
				catch(Exception ex)
				{	
					logger.Exception(100,ex.Message);
					return false;
				}
			}
	
		private bool ProcessXML_AllPartyList(string strXML, ref Queue partyq)
		{
			try
			{
				// load the xml doc
				XmlDocument oDOM = new XmlDocument();
				oDOM.LoadXml(strXML);

				//member node list 
				XmlNodeList oNodeList = oDOM.SelectNodes("//member");
				bool isEnumerateIDPresent= false;

				//going thru the "member" array list 
				foreach(XmlNode oNode in oNodeList)
				{
					// check if there are more participants . to do that need to check if enumerateID > 0.					
					if(oNode.SelectSingleNode("name").InnerText == "enumerateID")
					{
						isEnumerateIDPresent = true;
						try 
						{
							this.participantEnumerateID = Int32.Parse (oNode.SelectSingleNode("value/string").InnerText.Trim());
						}
						catch (Exception)
						{
							logger.Trace ("Invalid enumerateID.");
							this.participantEnumerateID = 0;
						}
					}

					if (isEnumerateIDPresent == false)
					{
						this.participantEnumerateID = 0;
					}

					//get to the "participants" member and start to retrive the conf list
					if(oNode.SelectSingleNode("name").InnerText == "participants")
					{
						//get to each individual participant
						XmlNodeList oInnerNodeList = oNode.SelectNodes("value/array/data/value");

						foreach (XmlNode oInnerNode in oInnerNodeList)
						{
						
							try
							{
								// new party
								NS_MESSENGER.Party party = new NS_MESSENGER.Party();
							
								// party name
								party.sMcuName = oInnerNode.SelectSingleNode("struct/member[name = \"participantName\"]/value/string").InnerText.Trim();
								logger.Trace ("Party Name:" +  party.sMcuName);								

								// party type
								string type = oInnerNode.SelectSingleNode("struct/member[name = \"participantType\"]/value/string").InnerText.Trim();
								logger.Trace ("Party type:" +  type);

								// conf name
								party.sConfNameOnMcu = oInnerNode.SelectSingleNode("struct/member[name = \"conferenceName\"]/value/string").InnerText.Trim();
								logger.Trace ("Party conf name:" +  party.sConfNameOnMcu);
							
								// call state
								string callState = oInnerNode.SelectSingleNode("struct/member[name = \"currentState\"]/value/struct/member[name = \"callState\"]/value/string").InnerText.Trim();
								if (callState == "disconnected")
								{
									party.etStatus = NS_MESSENGER.Party.eOngoingStatus.DIS_CONNECT;
								}
								else
								{	
									if (callState == "connected") 
									{
										party.etStatus = NS_MESSENGER.Party.eOngoingStatus.FULL_CONNECT;
									}
									else
									{
										party.etStatus = NS_MESSENGER.Party.eOngoingStatus.PARTIAL_CONNECT;
									}
								}
								logger.Trace ("Party callstate:" + callState);
                                //FB 2501 Call Moniotring Start
                                string maxBitRateToMCU = oInnerNode.SelectSingleNode("struct/member[name = \"currentState\"]/value/struct/member[name = \"maxBitRateToMCU\"]/value/integer").InnerText.Trim();
                                string maxBitRateFromMCU = oInnerNode.SelectSingleNode("struct/member[name = \"currentState\"]/value/struct/member[name = \"maxBitRateFromMCU\"]/value/integer").InnerText.Trim();
                                bool lecture = false;
                                string lecturemode = oInnerNode.SelectSingleNode("struct/member[name = \"currentState\"]/value/struct/member[name = \"lecturer\"]/value/boolean").InnerText.Trim();
                                Boolean.TryParse(lecturemode, out lecture);
                                string borderWidth = oInnerNode.SelectSingleNode("struct/member[name = \"configuredState\"]/value/struct/member[name = \"borderWidth\"]/value/integer").InnerText.Trim();
                                //FB 2501 Call Moniotring End
								// add party to queue
								logger.Trace("Adding party to queue...");
								//logger.Trace ("Party conf name: " + party.sConfNameOnMcu);
								partyq.Enqueue(party);
							}
							catch (Exception e)
							{
								logger.Trace ("Invalid party call parameters. Error:" + e.Message);
							}
						}
					}
				}					
				return true;
			}
			catch(Exception ex)
			{	
				logger.Exception(100,ex.Message);
				return false;
			}			
		}

		#endregion

		private void LogCodianFaultResponse(string strName, string strFaultXML, string strFunctionCall)
			{
				try
				{
					string strFaultCode = "";
					string strFaultMsg = "";

					XmlDocument oDOM = new XmlDocument();
					oDOM.LoadXml(strFaultXML);

					XmlNodeList oNodeList = oDOM.SelectNodes("//member");

					foreach(XmlNode oNode in oNodeList)
					{
						if(oNode.SelectSingleNode("name").InnerText == "faultCode")
						{
							strFaultCode = oNode.SelectSingleNode("value/int").InnerText.ToString();
						}
						else if(oNode.SelectSingleNode("name").InnerText == "faultString")
						{
							strFaultMsg = oNode.SelectSingleNode("value/string").InnerText.ToString();
						}
					}

					string strLogMsg = "Codian function call failed. ";
					strLogMsg = strLogMsg + "Code: " + strFaultCode;
					strLogMsg = strLogMsg + ", Message: " + strFaultMsg;

					logger.Exception(100,"7," + strFunctionCall + strLogMsg);
					this.errMsg = strLogMsg;

				}
				catch(Exception ex)
				{
					logger.Exception(100,ex.Message);
				}
			

			}
		
		internal bool AddPartyToMcu(NS_MESSENGER.Conference conf,NS_MESSENGER.Party party)
		{
			try
			{
/*              // Prepare the xml-rpc object
                vbxmlrpc_Net.XMLRPCStruct oStruct = new vbxmlrpc_Net.XMLRPCStructClass();
                bool ret = CreateStruct("ConferenceModify", party, conf, ref oStruct);

                if (!ret)
                {
                    logger.Trace("Create Struct failed.");
                    return false;
                }

                // Send cmd to mcu.
                ret = false;
                ret = SendCommand(party.cMcu, "conference.modify", oStruct);

                if (!ret)
                {
                    logger.Trace("Conference modify failed on Codian Mcu.");
                    return false;
                }
*/
                // FB case#720: skip the dial-in endpoints
                if (party.etConnType == NS_MESSENGER.Party.eConnectionType.DIAL_IN)
                {
                    logger.Trace("Skip adding the dial-in endpoint - " + party.sName);
                    return true;
                }

				// Prepare the xml-rpc object
                vbxmlrpc_Net.XMLRPCStruct oStructAdd = new vbxmlrpc_Net.XMLRPCStructClass(); 
                bool ret = false;
				ret = CreateStruct("AddParty", party, conf, ref oStructAdd);

				if(!ret)
				{
					logger.Trace ("Create Struct failed.");
					return false;
				}
				
				// Send cmd to mcu.
                ret = false; string strResponse = null;
				ret = SendCommand(party.cMcu,"participant.add", oStructAdd,ref strResponse);					

				if (!ret)
				{
					logger.Trace ("Adding party to ongoing conference on Codian Mcu failed."); //FB 1552
					return false;
				}

				return true;

			}
			catch(Exception ex)
			{
				logger.Exception(100,ex.Message);
				return false;
			}					
		}

	
		// Creating an empty conference		
		internal bool SetConference(NS_MESSENGER.Conference conf)
		{
			try
			{
				// Initializing the xml-rpc struct object				
				vbxmlrpc_Net.XMLRPCStruct oStruct = new vbxmlrpc_Net.XMLRPCStructClass();							
				NS_MESSENGER.Party party = new NS_MESSENGER.Party();
				party.cMcu = conf.cMcu;
				logger.Trace("Conf Unique ID: " + conf.iDbNumName.ToString());                

				bool ret = CreateStruct("ConferenceCreate", party,conf, ref oStruct);				
				if(!ret)
				{	
					logger.Trace (" Create Struct failed...");
					return false;
				}
				
				// Sending the conference data over to the MCU
                ret = false; string strResponse = null;
				ret = SendCommand(conf.cMcu, "conference.create", oStruct,ref strResponse);					
				if (!ret)
				{
					logger.Trace ("Conference setup failed on Codian MCU.");
					return false;										
				}
				
                // add participants to conference
                ret = false; //string failedParty = null;
                logger.Trace("Total parties: " + conf.qParties.Count.ToString());

                while (conf.qParties.Count > 0)
                {
                    NS_MESSENGER.Party party1 = new NS_MESSENGER.Party();
                    party1 = (NS_MESSENGER.Party)conf.qParties.Dequeue();
                    
                    // Requested by Dan for Yorktel - Disney (Jan, 29 2010)
                    // check line rate of party with conference. if conference is lesser than party, then use conference line rate. Else, don't change anything.
                    if (conf.stLineRate.etLineRate < party1.stLineRate.etLineRate)
                    {
                        party1.stLineRate.etLineRate = conf.stLineRate.etLineRate;
                    }

                    bool ret1 = AddPartyToMcu(conf, party1);
                    if (!ret1)
                    {
                        logger.Exception(100, "Participant failed to be added : " + party1.sName); 
                    }

                }

                //ConferenceStatus(conf); //FB 2797

				// successful 
				return true;
			}
			catch(Exception ex)
			{
				logger.Exception(100,ex.Message);
				return false;
			}


        }
        //FB 2989 Starts
        #region ConnectDisconnectEndpoint
        /// <summary>
        /// ConnectDisconnectEndpoint
        /// </summary>
        /// <param name="conf"></param>
        /// <param name="party"></param>
        /// <param name="connectOrDisconnect"></param>
        /// <returns></returns>
        internal bool ConnectDisconnectEndpoint(NS_MESSENGER.Conference conf, NS_MESSENGER.Party party, bool connectOrDisconnect)
        {
            NS_DATABASE.Database db = new NS_DATABASE.Database(configParams); //FB 2989

            try
            {
                logger.Trace("Entering ConnectDisconnect Endpoint method...ConnectOrDisconnect = " + connectOrDisconnect.ToString());
                
                vbxmlrpc_Net.XMLRPCStruct oStruct = new vbxmlrpc_Net.XMLRPCStructClass();
                
                bool ret = CreateStruct("ConnectDisconnectParty", party, conf, ref oStruct);
                if (!ret)
                {
                    logger.Trace("Struct problem.");
                    return false;
                }

                logger.Trace("Sending connectOrDisconnect cmd to codian mcu...");
                
                ret = false; string strResponse = null;
                if (connectOrDisconnect)
                {
                    ret = SendCommand(party.cMcu, "participant.connect", oStruct, ref strResponse);
                }
                else
                {
                    ret = SendCommand(party.cMcu, "participant.disconnect", oStruct, ref strResponse);
                }

                ret = ParticipantStatus(conf, ref party);
                if (!ret)
                    party.etStatus = NS_MESSENGER.Party.eOngoingStatus.DIS_CONNECT;

                if (conf.etType == NS_MESSENGER.Conference.eType.POINT_TO_POINT)
                {
                    db.UpdateP2PConferenceEndpointStatus(conf.iDbID, conf.iInstanceID, party.etStatus);
                }
                else
                {
                    if (party.etType == NS_MESSENGER.Party.eType.USER || party.etType == NS_MESSENGER.Party.eType.GUEST)
                    {
                        db.UpdateConferenceUserStatus(conf.iDbID, conf.iInstanceID, party.iEndpointId, party.etStatus);
                    }
                    else
                    {
                        if (party.etType == NS_MESSENGER.Party.eType.ROOM)
                        {
                            db.UpdateConferenceRoomStatus(conf.iDbID, conf.iInstanceID, party.iEndpointId, party.etStatus);
                        }
                        else
                        {
                            db.UpdateConferenceCascadeStatus(conf.iDbID, conf.iInstanceID, party.iEndpointId, party.etStatus);
                        }
                    }
                }
                if (!ret)
                {
                    logger.Trace("SendCommand failed");
                    return false;
                }
                return true;
            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
            }
            return true;
        }
        #endregion
        //FB 2989 End
        internal bool GetEndpointStatus(NS_MESSENGER.Conference conf, ref NS_MESSENGER.Party party)
        {
            try
            {
                // Fetch all the connected participants
                //Queue partyq = new Queue();
                int pageNumber = 1;
                this.participantEnumerateID = 0;
                do
                {
                    // instantiating new objects
                    vbxmlrpc_Net.XMLRPCStruct oStruct = new vbxmlrpc_Net.XMLRPCStructClass();
                    NS_MESSENGER.Conference conference = new NS_MESSENGER.Conference();
                    conference.cMcu = party.cMcu;

                    // create the xml-rpc object
                    bool ret = false;
                    ret = CreateStruct("ConnectedParticipantList", party, conference, ref oStruct);
                    if (!ret)
                    {
                        logger.Trace("Create Struct failed.");
                        return false;
                    }

                    // send the cmd to codian mcu
                    string strResponse = null; ret = false;
                    ret = SendCommand(party.cMcu, "participant.enumerate", oStruct,ref strResponse);
                    if (!ret)
                    {
                        logger.Trace("Send command failed.");
                        return false;
                    }

                    // process the response
                    // check if the db party is matching with any of the mcu participants
                    ret = false; bool isConnected = false; bool foundParty = false;
                    ret = ProcessXML_FindConnectedParticipant(strResponse, ref party, ref isConnected, ref foundParty, conf.sDbName); //ZD 104511
                    if (!ret)
                    {
                        logger.Trace("Failure in processing the participant list returned by Codian MCU");
                        return false;
                    }
                    
                    logger.Trace("Codian Response Page Number # " + pageNumber);
                    pageNumber++;

                    if (foundParty)
                    {
                        // found the participant
                        logger.Trace("Endpoint party found. Exiting the codian paging mechanism...");
                        return true;
                    }
                    //ALLOPS-118 - Start
                    /*else //FB 1552 start
                    {
                        logger.Trace("Into add participant on ongoing.");
                        if (conf.stLineRate.etLineRate < party.stLineRate.etLineRate)
                        {
                            party.stLineRate.etLineRate = conf.stLineRate.etLineRate;
                        }
                        
                        bool ret1 = AddPartyToMcu(conf, party);
                        if (!ret1)
                        {
                            logger.Exception(100, "Participant failed to be added : " + party.sName);
                        }
                    } //FB 1552 end*/
                    //ALLOPS-118 - End
                    
                    if (!isConnected)                    
                    {
                        party.etStatus = NS_MESSENGER.Party.eOngoingStatus.DIS_CONNECT;
                    }
                    
                }
                while (this.participantEnumerateID > 0); //keep repeating since thr are more participants to be retreived from the codian mcu.

                return true;
            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
                return false;
            }
        }
        private bool ProcessXML_FindConnectedParticipant(string strXML, ref NS_MESSENGER.Party party, ref bool isConnected, ref bool foundParty, string Title) //ZD 104511
        {
            String snapshotMCUURL = "", Address = ""; //FB 2640
            string txAudioMute = "", rxAudioMute = "", rxVideoMute = "", activeSpeaker = "";//FB 2971 //ZD 100174
            try
            {
                //ZD 100113 Start
                if(party.cMcu.iURLAccess == 1)
                    snapshotMCUURL = "https://" + party.cMcu.sIp; //FB 2640
                else
                    snapshotMCUURL = "http://" + party.cMcu.sIp + ":" + party.cMcu.iHttpPort.ToString(); //FB 2640
                //ZD 100113 End
                logger.Trace("MCU URI :" + snapshotMCUURL);
                // load the xml doc
                XmlDocument oDOM = new XmlDocument();
                oDOM.LoadXml(strXML);

                //member node list 
                XmlNodeList oNodeList = oDOM.SelectNodes("//member");
                bool isEnumerateIDPresent = false;

                //going thru the "member" array list 
                foreach (XmlNode oNode in oNodeList)
                {
                    // check if there are more participants . to do that need to check if enumerateID > 0.					
                    if (oNode.SelectSingleNode("name").InnerText == "enumerateID")
                    {
                        isEnumerateIDPresent = true;
                        try
                        {
                            this.participantEnumerateID = Int32.Parse(oNode.SelectSingleNode("value/string").InnerText.Trim());
                        }
                        catch (Exception)
                        {
                            logger.Trace("Invalid enumerateID.");
                            this.participantEnumerateID = 0;
                        }
                    }

                    if (isEnumerateIDPresent == false)
                    {
                        this.participantEnumerateID = 0;
                    }

                    //get to the "participants" member and start to retrive the conf list
                    if (oNode.SelectSingleNode("name").InnerText == "participants")
                    {
                        //get to each individual participant
                        XmlNodeList oInnerNodeList = oNode.SelectNodes("value/array/data/value");
                        foreach (XmlNode oInnerNode in oInnerNodeList)
                        {
                            try
                            {
                                // party name
                                string partyMcuName = oInnerNode.SelectSingleNode("struct/member[name = \"participantName\"]/value/string").InnerText.Trim();
                                logger.Trace("Party Name:" + partyMcuName);

                                // conf name //ZD 104511
                                string ConfNameOnMcu = oInnerNode.SelectSingleNode("struct/member[name = \"conferenceName\"]/value/string").InnerText.Trim();
                                logger.Trace("Party conf name:" + ConfNameOnMcu);                              

                                //ZD 100614 Start
                                string participantProtocol = oInnerNode.SelectSingleNode("struct/member[name = \"participantProtocol\"]/value/string").InnerText.Trim();
                                logger.Trace("Party type:" + participantProtocol);

                                string partyaddress = oInnerNode.SelectSingleNode("struct/member[name = \"currentState\"]/value/struct/member[name = \"address\"]/value/string").InnerText.Trim();
                                logger.Trace("Party address:" + partyaddress);

                                string partyipAddress = oInnerNode.SelectSingleNode("struct/member[name = \"currentState\"]/value/struct/member[name = \"ipAddress\"]/value/string").InnerText.Trim();
                                logger.Trace("Party ipaddress:" + partyipAddress);//ZD 103973

                                if (participantProtocol.Trim() == "sip")
                                    partyaddress = partyaddress.Trim().Replace("sip:", "");

                                //ZD 103263 Start
                                if (party.sAddress.Contains("B"))
                                    Address = party.sAddress.Split('B')[0].ToString();
                                else
                                    Address = party.sAddress;
                                //ZD 103263 End 

                                //ZD 103633 Start
                                if (party.sAddress.Contains("D"))
                                    Address = party.sAddress.Split('D')[0].ToString();
                                else
                                    Address = party.sAddress;
                                //ZD 103633 End 

                                if ((Address.Trim() == partyaddress.Trim() || Address.Trim() == partyipAddress.Trim()) && (ConfNameOnMcu == Title)) //ZD 103263 //ZD 103973 //ZD 104511
                                {
                                    //ZD 100614 End      
                                    // party name matches                                    
                                    foundParty = true;
                                    logger.Trace("Endpoint party found !");

                                    // party bame
                                    party.sMcuName = partyMcuName;

                                    // party type
                                    string type = oInnerNode.SelectSingleNode("struct/member[name = \"participantType\"]/value/string").InnerText.Trim();
                                    logger.Trace("Party type:" + type);

                                    // conf name
                                    party.sConfNameOnMcu = ConfNameOnMcu;

                                    // call state
                                    string callState = oInnerNode.SelectSingleNode("struct/member[name = \"currentState\"]/value/struct/member[name = \"callState\"]/value/string").InnerText.Trim(); //FB 2640
                                    if (callState == "disconnected")
                                    {
                                        party.etStatus = NS_MESSENGER.Party.eOngoingStatus.DIS_CONNECT;
                                    }
                                    else
                                    {
                                        if (callState == "connected")
                                        {
                                            party.etStatus = NS_MESSENGER.Party.eOngoingStatus.FULL_CONNECT;
                                            isConnected = true;
                                        }
                                        else
                                        {
                                            party.etStatus = NS_MESSENGER.Party.eOngoingStatus.PARTIAL_CONNECT;
                                        }
                                    }
                                    logger.Trace("Party callstate:" + callState);
                                    // FB 2553 - Codian
									//FB 2640
                                    if (oInnerNode.SelectSingleNode("struct/member[name = \"currentState\"]/value/struct/member[name = \"previewURL\"]/value/string") != null)
                                        party.sStream = oInnerNode.SelectSingleNode("struct/member[name = \"currentState\"]/value/struct/member[name = \"previewURL\"]/value/string").InnerText.Trim();
                                    if (!String.IsNullOrEmpty(party.sStream))
                                        party.sStream = snapshotMCUURL + party.sStream;
                                    logger.Trace("Party PreviewURL:" + party.sStream);

                                    //FB 2971 Start
                                    if (oInnerNode.SelectSingleNode("struct/member[name = \"currentState\"]/value/struct/member[name = \"audioRxMuted\"]/value/boolean") != null)
                                        rxAudioMute = oInnerNode.SelectSingleNode("struct/member[name = \"currentState\"]/value/struct/member[name = \"audioRxMuted\"]/value/boolean").InnerText.Trim();
                                    logger.Trace("Participant rxAudioMute:" + txAudioMute);
                                    if (rxAudioMute == "1")
                                        party.bMute = true; //FB 3008
                                    else
                                        party.bMute = false; //FB 3008

                                    if (oInnerNode.SelectSingleNode("struct/member[name = \"currentState\"]/value/struct/member[name = \"audioTxMuted\"]/value/boolean") != null)
                                        txAudioMute = oInnerNode.SelectSingleNode("struct/member[name = \"currentState\"]/value/struct/member[name = \"audioTxMuted\"]/value/boolean").InnerText.Trim();
                                    logger.Trace("Participant txAudioMute:" + txAudioMute);
                                    if (txAudioMute == "1")
                                        party.bMuteRxaudio = true; //FB 3008
                                    else
                                        party.bMuteRxaudio = false; //FB 3008

                                    if (oInnerNode.SelectSingleNode("struct/member[name = \"currentState\"]/value/struct/member[name = \"videoRxMuted\"]/value/boolean") != null)
                                        rxVideoMute = oInnerNode.SelectSingleNode("struct/member[name = \"currentState\"]/value/struct/member[name = \"videoRxMuted\"]/value/boolean").InnerText.Trim();
                                    logger.Trace("Participant rxVideoMute:" + txAudioMute);
                                    if (rxVideoMute == "1")
                                        party.bMuteRxvideo = true;
                                    else
                                        party.bMuteRxvideo = false;
                                    
                                    logger.Trace("Party PreviewURL:" + party.sStream);

                                    //FB 2971 End

                                    //ZD 100174 Start

                                    if (oInnerNode.SelectSingleNode("struct/member[name = \"currentState\"]/value/struct/member[name = \"activeSpeaker\"]/value/boolean") != null)
                                        activeSpeaker = oInnerNode.SelectSingleNode("struct/member[name = \"currentState\"]/value/struct/member[name = \"activeSpeaker\"]/value/boolean").InnerText.Trim();
                                    logger.Trace("Participant activeSpeaker:" + activeSpeaker);
                                    if (activeSpeaker == "1")
                                        party.bactiveSpeaker = true;
                                    else
                                        party.bactiveSpeaker = false;

                                    logger.Trace("Party activeSpeaker:" + party.bactiveSpeaker);
                                    //ZD 100174 End

                                    return true;
                                }
                            }
                            catch (Exception e)
                            {
                                logger.Trace("Invalid party call parameters. Error:" + e.Message);
                            }
                        }
                    }
                }
                return true;
            }
            catch (Exception ex)
            {
                logger.Exception(100, ex.Message);
                return false;
            }

        }

        //FB 2448
        #region FetchMCUDetails
        /// <summary>
        /// FetchMCUDetails
        /// </summary>
        /// <param name="mcu"></param>
        /// <returns></returns>
        internal bool FetchMCUDetails(NS_MESSENGER.MCU mcu, int Timeout) // Changes done in command for FB 2501 Dec 10
        {
            try
            {
                logger.Trace("Entering FetchMCUDetails function...");

                // Create a empty RPC struct
                vbxmlrpc_Net.XMLRPCStruct oStruct = new vbxmlrpc_Net.XMLRPCStructClass();
                NS_MESSENGER.Party dummyParty = new NS_MESSENGER.Party();
                NS_MESSENGER.Conference dummyConf = new NS_MESSENGER.Conference();
                NS_DATABASE.Database db = new NS_DATABASE.Database(configParams);
                int portsVideoTotal = 0, portsVideoFree = 0, portsAudioTotal = 0, portsAudioFree = 0, status = 1;
                dummyParty.cMcu = mcu;
                bool ret = CreateStruct("FetchMCUDetails", dummyParty, dummyConf, ref oStruct);
                if (!ret)
                {
                    logger.Trace("Struct problem.");
                    db.UpdateMcuStatus(mcu, (int)NS_MESSENGER.MCU.PollState.FAIL); //ZD 100369_MCU
                    return false;
                }

                // Send the command to the mcu
                logger.Trace("Sending system query cmd to codian mcu...");
                ret = false; string strResponse = null;
                ret = SendMCUStatusCommand(mcu, "device.query", oStruct, ref strResponse, Timeout); //ZD 100369_MCU
                if (!ret)
                {
                    logger.Trace("Connection to MCU failed. Please recheck the MCU information.");
                    portsVideoTotal = 0; portsVideoFree = 0; portsAudioTotal = 0; portsAudioFree = 0;
                    status = 3;//MCU Status
                    db.UpdateMcuInfo(mcu.iDbId, portsVideoTotal, portsVideoFree, portsAudioTotal, portsAudioFree, status);
                    db.UpdateMcuStatus(mcu, (int)NS_MESSENGER.MCU.PollState.FAIL); //ZD 100369_MCU
                    return false;
                }

                if (strResponse.Trim() != "")
                {
                    db = new NS_DATABASE.Database(configParams);
                    XmlDocument oDOM = new XmlDocument();
                    XmlNodeList oNodeList = null;
                    DateTime currentTime = new DateTime();
                    String mcuCurrentTime = "";
                    oDOM.LoadXml(strResponse);

                    oNodeList = oDOM.SelectNodes("//member");
                    foreach (XmlNode oNode in oNodeList)//going thru the "member" array list 
                    {
                        if (oNode.SelectSingleNode("name").InnerText.Trim() == "currentTime")
                        {
                            mcuCurrentTime = oNode.SelectSingleNode("value/dateTime.iso8601").InnerText.Trim();
                            if (mcuCurrentTime != "")
                            {
                                if (mcuCurrentTime.IndexOf('-') < 0)
                                    mcuCurrentTime = mcuCurrentTime.Insert(4, "-").Insert(7, "-");

                                DateTime.TryParse(mcuCurrentTime.Trim(), out currentTime);
                                db.UpdateMcuCurrentTime(mcu.iDbId, currentTime);
                            }
                        }
                        if (oNode.SelectSingleNode("name").InnerText.Trim() == "totalVideoPorts")
                        {
                            Int32.TryParse(oNode.SelectSingleNode("value/int").InnerText.Trim(), out  portsVideoTotal);
                        }
                        if (oNode.SelectSingleNode("name").InnerText.Trim() == "totalAudioOnlyPorts")
                        {
                            Int32.TryParse(oNode.SelectSingleNode("value/int").InnerText.Trim(), out portsAudioTotal);
                        }
                    }
                }
                db.UpdateMcuInfo(mcu.iDbId, portsVideoTotal, portsVideoFree, portsAudioTotal, portsAudioFree, status);
                db.UpdateMcuStatus(mcu, (int)NS_MESSENGER.MCU.PollState.PASS); //ZD 100369_MCU
                return true;
            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
                return false;
            }
        }
        #endregion

        //FB 2501 Call Monitoring Start

        #region Call Monitoring Commands

        internal bool ConferenceStatus(NS_MESSENGER.Conference conf,NS_MESSENGER.Party party) //FB 2971
        {
            try
            {
                // Create the RPC struct
                logger.Trace("Entering Conference Status method...");
                vbxmlrpc_Net.XMLRPCStruct oStruct = new vbxmlrpc_Net.XMLRPCStructClass();
                NS_MESSENGER.Party dummyParty = new NS_MESSENGER.Party();
                dummyParty.cMcu = party.cMcu; //FB 2971
                int ConfActiveStatus = 0; //ZD 100085
                bool ret = CreateStruct("ConferenceStatus", dummyParty, conf, ref oStruct);
                if (!ret)
                {
                    logger.Trace("Struct problem.");
                    return false;
                }

                // Send the command to the mcu
                logger.Trace("Sending conference status cmd to codian mcu...");
                ret = false; string strResponse = null;
                ret = SendCommand(party.cMcu, "conference.status", oStruct, ref strResponse);
                if (!ret)
                {
                    logger.Trace("SendCommand failed");
                    return false;
                }
                if (strResponse.Trim() != "")
                {
                    NS_DATABASE.Database db = new NS_DATABASE.Database(configParams);
                    XmlDocument oDOM = new XmlDocument();
                    XmlNodeList oNodeList = null;
                    string uniqueId = "", locked = ""; //FB 2797
                    int confStatus = 0;
                    oDOM.LoadXml(strResponse);

                    oNodeList = oDOM.SelectNodes("//member");
                    foreach (XmlNode oNode in oNodeList)
                    {
                        //FB 2797 Start
                        if (oNode.SelectSingleNode("name").InnerText.Trim() == "uniqueId")
                            uniqueId = oNode.SelectSingleNode("value/int").InnerText.Trim();
                        logger.Trace("ConfId: " + conf.iDbID.ToString() + "uniqueId: " + uniqueId.ToString());

                        if (oNode.SelectSingleNode("name").InnerText.Trim() == "locked")
                            locked = oNode.SelectSingleNode("value/boolean").InnerText.Trim();
                        logger.Trace("Conf Locked: " + locked.ToString());

                        //ZD 100085 Starts
                        if (oNode.SelectSingleNode("name").InnerText.Trim() == "conferenceActive")
                        {
                            int.TryParse(oNode.SelectSingleNode("value/boolean").InnerText.Trim(), out ConfActiveStatus);
                            if (ConfActiveStatus == 1)
                                confStatus = 5;
                            else
                                confStatus = 6;
                        }
                        logger.Trace("Conf Active: " + confStatus.ToString());
                        //FB 2797
                    }

                    if (uniqueId != "" || locked != "")
                        db.UpdateConferenceUniqueid(conf.iDbID, conf.iInstanceID, uniqueId, locked, confStatus);
                    //ZD 100085 End

                }
                return true;
            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
                return (false);
            }
        }

        internal bool NetworkState(NS_MESSENGER.Conference conf)
        {
            try
            {
                // Create the RPC struct
                logger.Trace("Entering Network State method...");
                vbxmlrpc_Net.XMLRPCStruct oStruct = new vbxmlrpc_Net.XMLRPCStructClass();
                // Send the command to the mcu
                logger.Trace("Sending conference status cmd to codian mcu...");
                bool ret = false; string strResponse = null;
                ret = SendCommand(conf.cMcu, "device.network.query", oStruct, ref strResponse);
                if (!ret)
                {
                    logger.Trace("SendCommand failed");
                    return false;
                }
                if (strResponse.Trim() != "")
                {
                    NS_DATABASE.Database db = new NS_DATABASE.Database(configParams);
                    XmlDocument oDOM = new XmlDocument();
                    String Speed = "", packetsSent = "", packetsReceived = "", bytesSent = "", bytesReceived = "";
                    oDOM.LoadXml(strResponse);
                    if (oDOM.SelectSingleNode("name").InnerText == "portB")
                    {
                        XmlNodeList oInnerNodeList = oDOM.SelectNodes("value/struct/member[\"portB\"]");

                        foreach (XmlNode oInnerNode in oInnerNodeList)
                        {
                            Speed = oInnerNode.SelectSingleNode("value/integer").InnerText.Trim();
                            packetsSent = oInnerNode.SelectSingleNode("value/integer").InnerText.Trim();
                            packetsReceived = oInnerNode.SelectSingleNode("value/integer").InnerText.Trim();
                            bytesSent = oInnerNode.SelectSingleNode("value/integer").InnerText.Trim();
                            bytesReceived = oInnerNode.SelectSingleNode("value/integer").InnerText.Trim();
                            //Speed = oInnerNode.SelectSingleNode("struct/member[name = \"speed\"]/value/integer").InnerText.Trim();
                            //packetsSent = oInnerNode.SelectSingleNode("struct/member[name = \"packetsSent\"]/value/integer").InnerText.Trim();
                            //packetsReceived = oInnerNode.SelectSingleNode("struct/member[name = \"packetsReceived\"]/value/integer").InnerText.Trim();
                            //bytesSent = oInnerNode.SelectSingleNode("struct/member[name = \"bytesSent\"]/value/integer").InnerText.Trim();
                            //bytesReceived = oInnerNode.SelectSingleNode("struct/member[name = \"bytesReceived\"]/value/integer").InnerText.Trim();
                            //db.UpdateNetworkStatus(Speed,packetsSent,packetsReceived,bytesSent,bytesReceived);
                        }
                    }
                }
                return true;
            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
                return (false);
            }
        }

        internal bool ParticipantFECC(NS_MESSENGER.Conference conf, NS_MESSENGER.Party party, int direction)
        {
            try
            {
                // Create the RPC struct
                logger.Trace("Entering ParticipantFECC method...");
                vbxmlrpc_Net.XMLRPCStruct oStruct = new vbxmlrpc_Net.XMLRPCStructClass();
                party.sFECCdirection = direction.ToString();
                bool ret = CreateStruct("ChangeParticipantFECC", party, conf, ref oStruct);
                if (!ret)
                {
                    logger.Trace("Struct problem.");
                    return false;
                }

                // Send the command to the mcu
                logger.Trace("Sending change display layout cmd to codian mcu...");
                ret = false; string strResponse = null;
                ret = SendCommand(party.cMcu, "participant.fecc", oStruct, ref strResponse);
                if (!ret)
                {
                    logger.Trace("SendCommand failed");
                    return false;
                }
                return true;
            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
                return (false);
            }

        }

        internal bool ParticipantMessage(NS_MESSENGER.Conference conf, NS_MESSENGER.Party party, string Message, int direction, int duration)
        {
            try
            {
                // Create the RPC struct
                logger.Trace("Entering ParticipantMessage method...");
                vbxmlrpc_Net.XMLRPCStruct oStruct = new vbxmlrpc_Net.XMLRPCStructClass();
                party.sMessagedirection = direction.ToString();
                party.sMessage = Message;
                party.iMessDuration = duration;
                bool ret = CreateStruct("SendParticipantMessage", party, conf, ref oStruct);
                if (!ret)
                {
                    logger.Trace("Struct problem.");
                    return false;
                }

                // Send the command to the mcu
                logger.Trace("Sending change display layout cmd to codian mcu...");
                ret = false; string strResponse = null;
                ret = SendCommand(party.cMcu, "participant.message", oStruct, ref strResponse);
                if (!ret)
                {
                    logger.Trace("SendCommand failed");
                    return false;
                }
                return true;
            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
                return (false);
            }

        }

        //FB 2501 - Send Conference Message
        internal bool ConferenceMessage(NS_MESSENGER.Conference conf, NS_MESSENGER.Party party, string Message, int direction, int duration)
        {
            try
            {
                // Create the RPC struct
                logger.Trace("Entering ConferenceMessage method...");
                vbxmlrpc_Net.XMLRPCStruct oStruct = new vbxmlrpc_Net.XMLRPCStructClass();
                party.sMessagedirection = direction.ToString();
                party.sMessage = Message;
                party.iMessDuration = duration;
                party.cMcu = conf.cMcu;
                bool ret = CreateStruct("ConferenceMessage", party, conf, ref oStruct);
                if (!ret)
                {
                    logger.Trace("Struct problem.");
                    return false;
                }

                // Send the command to the mcu
                logger.Trace("Sending change display layout cmd to codian mcu...");
                ret = false; string strResponse = null;
                ret = SendCommand(party.cMcu, "conference.message", oStruct, ref strResponse);
                if (!ret)
                {
                    logger.Trace("SendCommand failed");
                    return false;
                }
                return true;
            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
                return (false);
            }

        }

        private bool ParticipantFECCDirection(string FECCType, ref string Direction)
        {
            try
            {
                switch (FECCType)
                {
                    case "1":
                        {
                            Direction = "up";
                            break;
                        }
                    case "2":
                        {
                            Direction = "down";
                            break;
                        }
                    case "3":
                        {
                            Direction = "left";
                            break;
                        }
                    case "4":
                        {
                            Direction = "right";
                            break;
                        }
                    case "5":
                        {
                            Direction = "zoomIn";
                            break;
                        }
                    case "6":
                        {
                            Direction = "zoomOut";
                            break;
                        }
                    case "7":
                        {
                            Direction = "focusIn";
                            break;
                        }
                    case "8":
                        {
                            Direction = "focusOut";
                            break;
                        }
                    default:
                        {
                            Direction = "";
                            break;
                        }
                }
                return true;
            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
                return false;
            }
        }

        private bool MessageDirection(string Type, ref string Direction)
        {
            try
            {
                switch (Type)
                {
                    case "1":
                        {
                            Direction = "top";
                            break;
                        }
                    case "2":
                        {
                            Direction = "middle";
                            break;
                        }
                    case "3":
                        {
                            Direction = "bottom";
                            break;
                        }
                    default:
                        {
                            Direction = "middle";
                            break;
                        }
                }
                return true;
            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
                return false;
            }
        }

        internal bool AudioGain(NS_MESSENGER.Conference conf, NS_MESSENGER.Party party, int Mode, int AudioGain)
        {
            try
            {
                // Create the RPC struct
                logger.Trace("Entering Audio Gain method...Mute = " + Mode.ToString());
                vbxmlrpc_Net.XMLRPCStruct oStruct = new vbxmlrpc_Net.XMLRPCStructClass();
                party.iAudioMode = Mode;
                party.iAudioGained = AudioGain;
                bool ret = CreateStruct("AudioGainMode", party, conf, ref oStruct);
                if (!ret)
                {
                    logger.Trace("Struct problem.");
                    return false;
                }

                // Send the command to the mcu
                logger.Trace("Sending mute cmd to codian mcu...");
                ret = false; string strResponse = null;
                ret = SendCommand(party.cMcu, "participant.modify", oStruct, ref strResponse);
                if (!ret)
                {
                    logger.Trace("SendCommand failed");
                    return false;
                }
                return true;
            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
            }
            return true;
        }

        private bool AudioGainMode(int AudioMode, ref string Mode)
        {
            try
            {
                string Type = AudioMode.ToString();
                switch (Type)
                {
                    case "1":
                        {
                            Mode = "none";
                            break;
                        }
                    case "2":
                        {
                            Mode = "automatic";
                            break;
                        }
                    case "3":
                        {
                            Mode = "fixed";
                            break;
                        }
                    default:
                        {
                            Mode = "none";
                            break;
                        }
                }
                return true;
            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
                return false;
            }
        }

        internal bool MuteTransmitEndpoint(NS_MESSENGER.Conference conf, NS_MESSENGER.Party party, bool mute)
        {
            try
            {
                // Create the RPC struct
                logger.Trace("Entering MuteEndpoint method...Mute = " + mute.ToString());
                vbxmlrpc_Net.XMLRPCStruct oStruct = new vbxmlrpc_Net.XMLRPCStructClass();
                party.bMute = mute;
                bool ret = CreateStruct("MutePartyTransmit", party, conf, ref oStruct);
                if (!ret)
                {
                    logger.Trace("Struct problem.");
                    return false;
                }

                // Send the command to the mcu
                logger.Trace("Sending mute cmd to codian mcu...");
                ret = false; string strResponse = null;
                ret = SendCommand(party.cMcu, "participant.modify", oStruct, ref strResponse);
                if (!ret)
                {
                    logger.Trace("SendCommand failed");
                    return false;
                }
                return true;
            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
            }
            return true;
        }

         internal bool MuteVideoReceiverEndpoint(NS_MESSENGER.Conference conf, NS_MESSENGER.Party party, bool mute)
        {
            try
            {
                // Create the RPC struct
                logger.Trace("Entering MuteEndpoint method...Mute = " + mute.ToString());
                vbxmlrpc_Net.XMLRPCStruct oStruct = new vbxmlrpc_Net.XMLRPCStructClass();
                party.bMute = mute;
                bool ret = CreateStruct("MutePartyVideoReceiver", party, conf, ref oStruct);
                if (!ret)
                {
                    logger.Trace("Struct problem.");
                    return false;
                }

                // Send the command to the mcu
                logger.Trace("Sending mute cmd to codian mcu...");
                ret = false; string strResponse = null;
                ret = SendCommand(party.cMcu, "participant.modify", oStruct, ref strResponse);
                if (!ret)
                {
                    logger.Trace("SendCommand failed");
                    return false;
                }
                return true;
            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
            }
            return true;
        }

         internal bool ParticipantStatus(NS_MESSENGER.Conference conf, ref NS_MESSENGER.Party party)
         {
             try
             {
                 vbxmlrpc_Net.XMLRPCStruct oStruct = new vbxmlrpc_Net.XMLRPCStructClass();

                 bool ret = false;
                 ret = CreateStruct("ParticipantStatus", party, conf, ref oStruct);
                 if (!ret)
                 {
                     logger.Trace("Create ParticipantStatus Struct failed.");
                     return false;
                 }

                 string strResponse = null; ret = false;
                 ret = SendCommand(party.cMcu, "participant.status", oStruct, ref strResponse);
                 if (!ret)
                 {
                     logger.Trace("Send command failed.");
                     return false;
                 }

                 ret = false; bool foundParty = false;
                 ret = ProcessXML_FindParticipantStatus(strResponse, ref party, ref foundParty);
                 if (!ret || !foundParty)
                 {
                     logger.Trace("Failure in processing the participant list returned by Codian MCU");
                     return false;
                 }
                 return true;
             }
             catch (Exception e)
             {
                 logger.Exception(100, e.Message);
                 return false;
             }
         }

        private bool ProcessXML_FindConnectedParticipantStatus(string strXML, ref NS_MESSENGER.Party party, ref bool isConnected, ref bool foundParty)
        {
            try
            {
                // load the xml doc
                XmlDocument oDOM = new XmlDocument();
                oDOM.LoadXml(strXML);

                //member node list 
                XmlNodeList oNodeList = oDOM.SelectNodes("//member");
                bool isEnumerateIDPresent = false;

                //going thru the "member" array list 
                foreach (XmlNode oNode in oNodeList)
                {
                    // check if there are more participants . to do that need to check if enumerateID > 0.					
                    if (oNode.SelectSingleNode("name").InnerText == "enumerateID")
                    {
                        isEnumerateIDPresent = true;
                        try
                        {
                            this.participantEnumerateID = Int32.Parse(oNode.SelectSingleNode("value/string").InnerText.Trim());
                        }
                        catch (Exception)
                        {
                            logger.Trace("Invalid enumerateID.");
                            this.participantEnumerateID = 0;
                        }
                    }

                    if (isEnumerateIDPresent == false)
                    {
                        this.participantEnumerateID = 0;
                    }

                    //get to the "participants" member and start to retrive the conf list
                    if (oNode.SelectSingleNode("name").InnerText == "participants")
                    {
                        //get to each individual participant
                        XmlNodeList oInnerNodeList = oNode.SelectNodes("value/array/data/value");
                        foreach (XmlNode oInnerNode in oInnerNodeList)
                        {
                            try
                            {
                                // party name
                                string partyMcuName = oInnerNode.SelectSingleNode("struct/member[name = \"participantName\"]/value/string").InnerText.Trim();
                                logger.Trace("Party Name:" + partyMcuName);

                                if (party.sName == partyMcuName)
                                {
                                    // party name matches                                    
                                    foundParty = true;
                                    logger.Trace("Endpoint party found !");

                                    // party bame
                                    party.sMcuName = partyMcuName;

                                    // party type
                                    string type = oInnerNode.SelectSingleNode("struct/member[name = \"participantType\"]/value/string").InnerText.Trim();
                                    logger.Trace("Party type:" + type);

                                    // conf name
                                    party.sConfNameOnMcu = oInnerNode.SelectSingleNode("struct/member[name = \"conferenceName\"]/value/string").InnerText.Trim();
                                    logger.Trace("Party conf name:" + party.sConfNameOnMcu);

                                    // call state
                                    string callState = oInnerNode.SelectSingleNode("struct/member[name = \"callState\"]/value/string").InnerText.Trim();
                                    if (callState == "disconnected")
                                    {
                                        party.etStatus = NS_MESSENGER.Party.eOngoingStatus.DIS_CONNECT;
                                    }
                                    else
                                    {
                                        if (callState == "connected")
                                        {
                                            party.etStatus = NS_MESSENGER.Party.eOngoingStatus.FULL_CONNECT;
                                            isConnected = true;
                                        }
                                        else
                                        {
                                            party.etStatus = NS_MESSENGER.Party.eOngoingStatus.PARTIAL_CONNECT;
                                        }
                                    }
                                    logger.Trace("Party callstate:" + callState);

                                    // FB 2553 - Codian
                                    party.sStream = oInnerNode.SelectSingleNode("struct/member[name = \"previewURL\"]/value/string").InnerText.Trim();
                                    logger.Trace("Party PreviewURL:" + party.sStream);

                                    return true;
                                }
                            }
                            catch (Exception e)
                            {
                                logger.Trace("Invalid party call parameters. Error:" + e.Message);
                            }
                        }
                    }
                }
                return true;
            }
            catch (Exception ex)
            {
                logger.Exception(100, ex.Message);
                return false;
            }

        }

        private bool ProcessXML_FindParticipantStatus(string strXML, ref NS_MESSENGER.Party party, ref bool foundParty)
        {
            string partnameonmcu = ""; //ZD 101056
            try
            {
                XmlDocument oDOM = new XmlDocument();
                oDOM.LoadXml(strXML);

                XmlNodeList oNodeList = oDOM.SelectNodes("//member");

                string partyMcuName = "", confMcuName = "", callState = "";
                XmlNode oInnerNode = null;

                for (int i = 0; i < oNodeList.Count; i++)
                {
                    oInnerNode = oNodeList[i];

                    if (oInnerNode.SelectSingleNode("//member[name = \"conferenceName\"]/value/string") != null)
                        confMcuName = oInnerNode.SelectSingleNode("//member[name = \"conferenceName\"]/value/string").InnerText.Trim();

                    if (oInnerNode.SelectSingleNode("//member[name = \"participantName\"]/value/string") != null)
                        partyMcuName = oInnerNode.SelectSingleNode("//member[name = \"participantName\"]/value/string").InnerText.Trim();

                    //ZD 101056 Starts
                    if (!string.IsNullOrEmpty(party.sMcuName))
                        partnameonmcu = party.sMcuName;
                    else
                        partnameonmcu = party.sName;


                    if (partnameonmcu == partyMcuName)
                    {
                     //ZD 101056 Ends
                        foundParty = true;
                        callState = "";
                        if (oInnerNode.SelectSingleNode("//member[name = \"callState\"]/value/string") != null)
                        {
                            callState = oInnerNode.SelectSingleNode("//member[name = \"callState\"]/value/string").InnerText.Trim();

                            if (callState.Trim().ToLower() == "disconnected")
                            {
                                party.etStatus = NS_MESSENGER.Party.eOngoingStatus.DIS_CONNECT;
                            }
                            else
                            {
                                if (callState.Trim().ToLower() == "connected")
                                {
                                    party.etStatus = NS_MESSENGER.Party.eOngoingStatus.FULL_CONNECT;
                                }
                                else
                                {
                                    party.etStatus = NS_MESSENGER.Party.eOngoingStatus.PARTIAL_CONNECT;
                                }
                            }
                            return true;
                        }
                    }
                }
                return true;
            }
            catch (Exception ex)
            {
                logger.Exception(100, ex.Message);
                return false;
            }

        }

        internal bool LockUnlockConference(NS_MESSENGER.Conference conf, int duration, int lockorunlock)
        {
            try
            {
                // Create the RPC struct
                logger.Trace("Entering LockUnlockConference method...LockOrUnlock = " + lockorunlock.ToString());
                vbxmlrpc_Net.XMLRPCStruct oStruct = new vbxmlrpc_Net.XMLRPCStructClass();
                NS_MESSENGER.Party dummyParty = new NS_MESSENGER.Party();
                dummyParty.cMcu = conf.cMcu;
                conf.ilockorunlock = lockorunlock;
                bool ret = CreateStruct("LockOrUnLockConference", dummyParty, conf, ref oStruct);
                if (!ret)
                {
                    logger.Trace("Struct problem.");
                    return false;
                }

                // Send the command to the mcu
                logger.Trace("Sending LockUnlockConference cmd to codian mcu...");
                ret = false; string strResponse = null;
                ret = SendCommand(conf.cMcu, "conference.modify", oStruct, ref strResponse);

                if (!ret)
                {
                    logger.Trace("SendCommand failed");
                    return false;
                }
                return true;
            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
            }
            return true;
        }

        internal bool GetTerminalStatus(NS_MESSENGER.Conference conf, ref NS_MESSENGER.Party party)
        {
            try
            {
                ConferenceStatus(conf,party); //FB 2971
                GetEndpointStatus(conf,ref party);
                ParticipantDiagnostics(conf, ref party);
                return true;
            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
                return false;
            }
        }

        #region ParticipantDiagnostics
        /// <summary>
        /// ParticipantDiagnostics
        /// </summary>
        /// <param name="conf"></param>
        /// <param name="party"></param>
        /// <returns></returns>
        private bool ParticipantDiagnostics(NS_MESSENGER.Conference conf, ref NS_MESSENGER.Party party)
        {
            try
            {
                // instantiating new objects
                vbxmlrpc_Net.XMLRPCStruct oStruct = new vbxmlrpc_Net.XMLRPCStructClass();
                conf.cMcu = party.cMcu;
                // create the xml-rpc object
                bool ret = false;
                ret = CreateStruct("ParticipantDiagnostics", party, conf, ref oStruct);
                if (!ret)
                {
                    logger.Trace("Create Struct failed.");
                    return false;
                }

                // send the cmd to Cisco 8710 MCU
                string strResponse = null; ret = false;
                ret = SendCommand(party.cMcu, "participant.diagnostics", oStruct, ref strResponse);
                if (!ret)
                {
                    logger.Trace("Send command failed.");
                    return false;
                }

                UpdateParticipantStatus(strResponse, party,conf.iDbID,conf.iInstanceID);//FB 2553 //ZD 101056
                return true;
            }
            catch (Exception ex)
            {
                logger.Exception(100, ex.Message);
                return false;
            }
        }

        internal bool UpdateParticipantStatus(string inXml, NS_MESSENGER.Party party,int confid, int instanceid )//FB 2553 //ZD 101056
        {
            XmlDocument xd = null;
            NS_DATABASE.Database db = new NS_DATABASE.Database(configParams);//FB 2584
            bool ret1 = false;
            try
            {
                logger.Trace("Entering the Update participant details...");                
                xd = new XmlDocument();
                xd.LoadXml(inXml);
                // load the xml doc
                XmlDocument oDOM = new XmlDocument();
                oDOM.LoadXml(inXml);
                String RxAudioPacketsReceived = "", RxAudioPacketsMissing = ""; 
                String RxVideoPacketsReceived = "", RxVideoPacketsMissing = "";
               
                //member node list 
                XmlNodeList oNodeList = oDOM.SelectNodes("//member");
                //FB 2553 Start
                foreach (XmlNode oNode in oNodeList)
                {
                    //ZD 100632 Start
                    if (oNode.SelectSingleNode("name").InnerText == "audioTxSent")
                    {
                        RxAudioPacketsReceived = oNode.SelectSingleNode("value/int").InnerText.Trim();
                        logger.Trace("TXaudioPacketSent:" + RxAudioPacketsReceived);
                        party.sTxAudioPacketsSent = RxAudioPacketsReceived;
                    }
                    //ZD 100632 End
                    if (oNode.SelectSingleNode("name").InnerText == "audioRxReceived")
                    {
                        RxAudioPacketsReceived = oNode.SelectSingleNode("value/int").InnerText.Trim();
                        logger.Trace("RxAudioPacketsReceived:" + RxAudioPacketsReceived);
                        party.sRxAudioPacketsReceived = RxAudioPacketsReceived;
                    }
                    if (oNode.SelectSingleNode("name").InnerText == "audioRxLost")
                    {
                        RxAudioPacketsMissing = oNode.SelectSingleNode("value/int").InnerText.Trim();
                        logger.Trace("RxAudioPacketsMissing:" + RxAudioPacketsMissing);
                        party.sRxAudioPacketsMissing = RxAudioPacketsMissing;
                    }
                    //ZD 100632 Start
                    if (oNode.SelectSingleNode("name").InnerText == "videoTxSent")
                    {
                        RxAudioPacketsReceived = oNode.SelectSingleNode("value/int").InnerText.Trim();
                        logger.Trace("TXvideoPacketSent:" + RxAudioPacketsReceived);
                        party.sTxVideoPacketsSent = RxAudioPacketsReceived;
                    }
                    //ZD 100632 End
                    if (oNode.SelectSingleNode("name").InnerText == "videoRxReceived")
                    {
                        RxVideoPacketsReceived = oNode.SelectSingleNode("value/int").InnerText.Trim();
                        logger.Trace("RxVideoPacketsReceived:" + RxVideoPacketsReceived);
                        party.sRxVideoPacketsReceived = RxVideoPacketsReceived;
                    }
                    if (oNode.SelectSingleNode("name").InnerText == "videoRxLost")
                    {
                        RxVideoPacketsMissing = oNode.SelectSingleNode("value/int").InnerText.Trim();
                        logger.Trace("RxVideoPacketsMissing:" + RxVideoPacketsMissing);
                        party.sRxVideoPacketsMissing = RxVideoPacketsMissing;
                    }
                }
                ret1 = db.UpdateparticipantStatus(party,confid,instanceid); //ZD 101056
                if (!ret1)
                {
                    logger.Trace("Error in Fetching MCU details.");
                    return false;
                }
                //FB 2553 End
                return true;
            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
                return (false);
            }
        }

        #endregion

        #endregion

        //FB 2501 Call Monitoring End

        //FB 2797 Start

        #region CallDetailRecords
        /// <summary>
        /// CallDetailRecords
        /// </summary>
        /// <param name="conf"></param>
        /// <param name="party"></param>
        /// <returns></returns>
        public bool CallDetailRecords(NS_MESSENGER.MCU MCU, int index, ref String strResponse)
        {
            vbxmlrpc_Net.XMLRPCStruct oStruct = new vbxmlrpc_Net.XMLRPCStructClass();
            NS_MESSENGER.Party dummyParty = new NS_MESSENGER.Party();
            NS_MESSENGER.Conference dummyConf = new NS_MESSENGER.Conference();
            bool ret = false;
            try
            {
                dummyParty.cMcu = MCU;
                indexCDR = index;
                logger.Trace("Create Struct for cdrlog.enumerate method");
                ret = CreateStruct("CallDetailRecords", dummyParty, dummyConf, ref oStruct);
                if (!ret)
                {
                    logger.Trace("Create Struct failed.");
                    return false;
                }
                ret = false;
                ret = SendCommand(dummyParty.cMcu, "cdrlog.enumerate", oStruct, ref strResponse);
                if (!ret)
                {
                    logger.Trace("Response Error in CDR command.");
                    return false;
                }

                return true;
            }
            catch (Exception ex)
            {
                logger.Exception(100, ex.Message);
                return false;
            }
        }
        #endregion

        //FB 2797 End

        //ZD 100522 Start

        #region SetVMRConference
        internal bool SetVMRConference(NS_MESSENGER.Conference conf, ref string outXml)
        {
            try
            {
                // Initializing the xml-rpc struct object				
                vbxmlrpc_Net.XMLRPCStruct oStruct = new vbxmlrpc_Net.XMLRPCStructClass();
                NS_DATABASE.Database db = new NS_DATABASE.Database(configParams);
                XmlDocument xDoc = null;
                NS_MESSENGER.Party party = new NS_MESSENGER.Party();
                party.cMcu = conf.cMcu;
                logger.Trace("Conf Unique ID: " + conf.iDbNumName.ToString());

                bool ret = CreateStruct("ConferenceVMRCreate", party, conf, ref oStruct);
                if (!ret)
                {
                    logger.Trace(" Create Struct failed...");
                    return false;
                }

                // Sending the conference data over to the MCU
                ret = false; string strResponse = null;
                ret = SendCommand(conf.cMcu, "conference.create", oStruct, ref strResponse);
                if (!ret)
                {
                    logger.Trace("Conference setup failed on Codian MCU.");
                    return false;
                }
                else
                {
                    oStruct = new vbxmlrpc_Net.XMLRPCStructClass();
                    ret = CreateStruct("ConferenceStatus", party, conf, ref oStruct);
                    if (!ret)
                    {
                        logger.Trace("Struct problem.");
                        return false;
                    }

                    // Send the command to the mcu
                    logger.Trace("Sending conference status cmd to codian mcu...");
                    ret = false; strResponse = null;
                    ret = SendCommand(party.cMcu, "conference.status", oStruct, ref strResponse);
                    if (!ret)
                    {
                        logger.Trace("SendCommand failed");
                        return false;
                    }
                    if (strResponse.Trim() != "")
                    {
                        db = new NS_DATABASE.Database(configParams);
                        XmlDocument oDOM = new XmlDocument();
                        XmlNodeList oNodeList = null;
                       
                        xDoc = new XmlDocument();
                        xDoc.LoadXml(strResponse);

                        oNodeList = xDoc.SelectNodes("//member");

                        foreach (XmlNode oNode in oNodeList)
                        {
                            if (oNode.SelectSingleNode("name").InnerText.Trim() == "uniqueId")
                            {
                                conf.sGUID = oNode.SelectSingleNode("value/int").InnerText.Trim();

                                logger.Trace("ConfId: " + conf.iDbID.ToString() + "uniqueId: " + conf.sGUID.ToString());

                                if (conf.dtStartDateTimeInUTC > DateTime.UtcNow)
                                    conf.etStatus = NS_MESSENGER.Conference.eStatus.OnMCU;
                                else
                                    conf.etStatus = NS_MESSENGER.Conference.eStatus.ONGOING;

                                db.UpdateConference(conf.iDbID, conf.iInstanceID, conf.sGUID, conf.etStatus, string.Empty);//ZD 101217

                                outXml = "<Conference><ConfUID>" + conf.sGUID + "</ConfUID><PermanentconfName>" + conf.sDbName + "</PermanentconfName></Conference>";//ZD 100522_S1
                            }

                        }
                    }
                }
                return true;
            }
            catch (Exception ex)
            {
                logger.Exception(100, ex.Message);
                return false;
            }


        }
        #endregion

        #region UpdateVMRConference
        internal bool UpdateVMRConference(NS_MESSENGER.Conference conf, ref string outXml)
        {
            try
            {
                // Initializing the xml-rpc struct object				
                vbxmlrpc_Net.XMLRPCStruct oStruct = new vbxmlrpc_Net.XMLRPCStructClass();
                NS_DATABASE.Database db = new NS_DATABASE.Database(configParams);
                XmlDocument xDoc = null;
                NS_MESSENGER.Party party = new NS_MESSENGER.Party();
                party.cMcu = conf.cMcu;
                logger.Trace("Conf Unique ID: " + conf.iDbNumName.ToString());

                bool ret = CreateStruct("ConferenceVMRUpdate", party, conf, ref oStruct);
                if (!ret)
                {
                    logger.Trace(" Create Struct failed...");
                    return false;
                }

                // Sending the conference data over to the MCU
                ret = false; string strResponse = null;
                ret = SendCommand(conf.cMcu, "conference.modify", oStruct, ref strResponse);
                if (!ret)
                {
                    logger.Trace("Conference setup failed on Codian MCU.");
                    return false;
                }
                else
                {
                    oStruct = new vbxmlrpc_Net.XMLRPCStructClass();
                    ret = CreateStruct("ConferenceStatus", party, conf, ref oStruct);
                    if (!ret)
                    {
                        logger.Trace("Struct problem.");
                        return false;
                    }

                    // Send the command to the mcu
                    logger.Trace("Sending conference status cmd to codian mcu...");
                    ret = false; strResponse = null;
                    ret = SendCommand(party.cMcu, "conference.status", oStruct, ref strResponse);
                    if (!ret)
                    {
                        logger.Trace("SendCommand failed");
                        return false;
                    }
                    if (strResponse.Trim() != "")
                    {
                        db = new NS_DATABASE.Database(configParams);
                        XmlDocument oDOM = new XmlDocument();
                        XmlNodeList oNodeList = null;

                        xDoc = new XmlDocument();
                        xDoc.LoadXml(strResponse);

                        oNodeList = xDoc.SelectNodes("//member");

                        foreach (XmlNode oNode in oNodeList)
                        {
                            if (oNode.SelectSingleNode("name").InnerText.Trim() == "uniqueId")
                            {
                                conf.sGUID = oNode.SelectSingleNode("value/int").InnerText.Trim();

                                logger.Trace("ConfId: " + conf.iDbID.ToString() + "uniqueId: " + conf.sGUID.ToString());

                                if (conf.dtStartDateTimeInUTC > DateTime.UtcNow)
                                    conf.etStatus = NS_MESSENGER.Conference.eStatus.OnMCU;
                                else
                                    conf.etStatus = NS_MESSENGER.Conference.eStatus.ONGOING;

                                db.UpdateConference(conf.iDbID, conf.iInstanceID, conf.sGUID, conf.etStatus, string.Empty);//ZD 101217

                                outXml = "<Conference><ConfUID>" + conf.sGUID + "</ConfUID><PermanentconfName>" + conf.sDbName + "</PermanentconfName></Conference>";//ZD 100522_S1
                            }

                        }
                    }
                }
                //ZD 100522_S1 Start
                logger.Trace("Total parties: " + conf.qParties.Count.ToString());

                while (conf.qParties.Count > 0)
                {
                    NS_MESSENGER.Party party1 = new NS_MESSENGER.Party();
                    party1 = (NS_MESSENGER.Party)conf.qParties.Dequeue();

                    // Requested by Dan for Yorktel - Disney (Jan, 29 2010)
                    // check line rate of party with conference. if conference is lesser than party, then use conference line rate. Else, don't change anything.
                    if (conf.stLineRate.etLineRate < party1.stLineRate.etLineRate)
                    {
                        party1.stLineRate.etLineRate = conf.stLineRate.etLineRate;
                    }

                    bool ret1 = AddPartyToMcu(conf, party1);
                    if (!ret1)
                    {
                        logger.Exception(100, "Participant failed to be added : " + party1.sName);
                    }

                }
                //ZD 100522_S1 End
                return true;
            }
            catch (Exception ex)
            {
                logger.Exception(100, ex.Message);
                return false;
            }


        }
        #endregion
        //ZD 100522 End
	}
}

#if COMMENT
	private bool ModifyParticipantLayout(NS_MESSENGER.Party party, NS_MESSENGER.Conference conf)
		{
			try
			{
				vbxmlrpc_Net.XMLRPCStruct oStruct = new vbxmlrpc_Net.XMLRPCStructClass();
				vbxmlrpc_Net.XMLRPCStruct oModStruct = new vbxmlrpc_Net.XMLRPCStructClass();

				bool ret = CreateStruct("ModifyParticipantLayout", party,conf, ref oStruct);
				if(ret)
				{
					logger.Trace ("Create struct failed.");	
					return false;
				}

				string strResponse = null; ret = false;
				ret = SendCommand(party.cMcu,"conference.query", oStruct,ref strResponse);
				if(!ret)
				{
					logger.Trace ("Send cmd failed.");
					return false;
				}
				else
				{
					XmlDocument oDOM = new XmlDocument();
					oDOM.LoadXml(strResponse);

					string strMethod = "participant.modify";

					//Member Node List
					XmlNodeList oNodeList = oDOM.SelectNodes("//member");

					//get to the "conferences" member and start to retrive the array
					foreach(XmlNode oNode in oNodeList)
					{
						
						if(oNode.SelectSingleNode("name").InnerText == "participants")
						{
							
							//get to each individual conference 
							XmlNodeList oInnerNodeList = oNode.SelectNodes("value/array/data/value");
							
							foreach (XmlNode oInnerNode in oInnerNodeList)
							{
								if(oInnerNode.InnerXml.IndexOf("<name>address</name>", 0) == -1)
								{
									NS_MESSENGER.Party singleParty = party;

									XmlNodeList oMemberNodeList = oInnerNode.SelectNodes("struct/member");

									foreach(XmlNode oMemberNode in oMemberNodeList)
									{									
										if(oMemberNode.SelectSingleNode("name").InnerText == "participantName")
										{
											singleParty.sName = oMemberNode.SelectSingleNode("value/string").InnerText;
											ret = CreateStruct(strMethod, singleParty, conf, ref oModStruct);

											if(ret)
											{
												ret =false;
												ret = SendCommand(party.cMcu,strMethod, oModStruct,ref strResponse);
											}

											if(strResponse == "")
											{
												return false;
											}

											break;
										}
									}
								}
								
							}

						}
					}

					

					return true;
				}
			}
			catch(Exception ex)
			{	
				logger.Exception(100,ex.Message);
				return false;
			}

		}


		private bool FetchAllParticipants(NS_MESSENGER.MCU mcu, ref Queue partyq)
			{
				try
				{
					this.participantEnumerateID = 0;
					do 
					{
						vbxmlrpc_Net.XMLRPCStruct oStruct = new vbxmlrpc_Net.XMLRPCStructClass();
						NS_MESSENGER.Conference conf = new NS_MESSENGER.Conference();
						NS_MESSENGER.Party tmpParty = new NS_MESSENGER.Party();
						tmpParty.cMcu = mcu;
						conf.cMcu = mcu;

						// create struct
						bool ret = CreateStruct("FetchAllParticipants", tmpParty,conf, ref oStruct);
						if(!ret)
						{
							logger.Trace ("Create Struct failed.");
							return false;
						}

						// send cmd to mcu
						ret = false;string strResponse = null; 
						ret = SendCommand(mcu,"participant.enumerate", oStruct,ref strResponse);
						if(!ret)
						{
							logger.Trace ("Send Cmd failed.");
							return false;
						}

						// process the response
						XmlDocument oDOM = new XmlDocument();
						oDOM.LoadXml(strResponse);

						//Member Node List
						XmlNodeList oNodeList = oDOM.SelectNodes("//member");

						//get to the "participants" member and start to retrive the array
						foreach(XmlNode oNode in oNodeList)
						{
							if(oNode.SelectSingleNode("name").InnerText == "participants")
							{
								//get to each individual conference 
								XmlNodeList oInnerNodeList = oNode.SelectNodes("value/array/data/value");

								foreach (XmlNode oInnerNode in oInnerNodeList)
								{
									NS_MESSENGER.Party party = new NS_MESSENGER.Party();

									XmlNodeList oMemberNodeList = oInnerNode.SelectNodes("struct/member");

									foreach(XmlNode oMemberNode in oMemberNodeList)
									{
										if(oMemberNode.SelectSingleNode("name").InnerText == "participantName")
										{
											party.sMcuName = oMemberNode.SelectSingleNode("value/string").InnerText;
											logger.Trace("#### Party Name : " + party.sMcuName);
										}
							
										if(oMemberNode.SelectSingleNode("name").InnerText == "address")
										{
											party.sMcuAddress = oMemberNode.SelectSingleNode("value/string").InnerText;
										}

										if(oMemberNode.SelectSingleNode("name").InnerText == "callState")
										{
											logger.Trace("#### Party Status : " + oMemberNode.SelectSingleNode("value/string").InnerText);

											switch(oMemberNode.SelectSingleNode("value/string").InnerText)
											{
												case "alerting":
													party.etStatus = NS_MESSENGER.Party.eOngoingStatus.PARTIAL_CONNECT;
													break;

												case "connected":
													party.etStatus = NS_MESSENGER.Party.eOngoingStatus.FULL_CONNECT;
													break;

												case "disconnected":
													party.etStatus = NS_MESSENGER.Party.eOngoingStatus.DIS_CONNECT;
													break;

												case "dormant":
													party.etStatus = NS_MESSENGER.Party.eOngoingStatus.DIS_CONNECT;
													break;
											}
										}
							
									}

									/*loggerger.Trace (party.sMcuName);
									loggerger.Trace (party.m_mcuAddress);
									loggerger.Trace (party.m_partyStatus.ToString());*/

									conf.qParties.Enqueue(party);
								}

							}
						}
						//count++;
					}
					while (this.participantEnumerateID > 0); // keep calling codian mcu till all participants are retreived.
					return true;
				}
				catch(Exception ex)
				{	
					logger.Exception(100,ex.Message);
					return false;
				}
			}


		private bool ProcessXML_OngoingConfList(string strXML, ref Queue confq)
			{
				try
				{
					// load the xml doc
					XmlDocument oDOM = new XmlDocument();
					oDOM.LoadXml(strXML);

					//member node list 
					XmlNodeList oNodeList = oDOM.SelectNodes("//member");
					bool isEnumerateIDPresent= false;

					//going thru the "member" array list 
					foreach(XmlNode oNode in oNodeList)
					{
						// check if there are more confs . to do that need to check if enumerateID > 0.
					
						if(oNode.SelectSingleNode("name").InnerText == "enumerateID")
						{
							isEnumerateIDPresent = true;
							try 
							{
								this.confEnumerateID = Int32.Parse (oNode.SelectSingleNode("value/string").InnerText.Trim());
							}
							catch (Exception)
							{
								logger.Trace ("Invalid enumerateID.");
								this.confEnumerateID = 0;
							}
						}

						if (isEnumerateIDPresent == false)
						{
							this.confEnumerateID = 0;
						}

						//get to the "conferences" member and start to retrive the conf list
						if(oNode.SelectSingleNode("name").InnerText == "conferences")
						{
							//get to each individual conference 
							XmlNodeList oInnerNodeList = oNode.SelectNodes("value/array/data/value");

							foreach (XmlNode oInnerNode in oInnerNodeList)
							{
								XmlNodeList oMemberNodeList = oInnerNode.SelectNodes("struct/member");

								// cycle thru each of the confs
								foreach(XmlNode oMemberNode in oMemberNodeList)
								{
									// find the unique id of the conf in the array list
									if(oMemberNode.SelectSingleNode("name").InnerText == "numericId")
									{
										try
										{
											NS_MESSENGER.Conference conf = new NS_MESSENGER.Conference();
											conf.iDbNumName = Int32.Parse(oMemberNode.SelectSingleNode("value/string").InnerText.Trim());
											confq.Enqueue(conf);
										}
										catch (Exception)
										{
											logger.Trace ("Found a non-VRM conference.");
											break;
										}
									}
							
									//if(oMemberNode.SelectSingleNode("name").InnerText == "numLeft")
									//{
									//	conf.stStatus.iTotalConnectedParties= Int32.Parse(oMemberNode.SelectSingleNode("value/int").InnerText);
									//}							
								}								
							}
						}
					}
					
					return true;
				}
				catch(Exception ex)
				{	
					logger.Exception(100,ex.Message);
					return false;
				}			
			}


		internal bool SetConference(NS_MESSENGER.Conference conf)
		{
			try
			{				
				string strConfName = conf.sDbName;
				// Create the RPC struct
				vbXMLRPC.XMLRPCStruct oStruct = new vbXMLRPC.XMLRPCStructClass();			
				bool ret = CreateStruct("CreateConference", conf, ref oStruct);
				if (!ret)
				{
					logger.Trace ("CreateStruct failed.");
					return false;
				}
				
				// Send command to MCU				
				string strResponse = null;
				bool ret1 = SendCommand(conf.cMcu,"conference.create", oStruct,ref strResponse);
				if (!ret1)
				{
					logger.Trace ("SendCommand failed.");
					return false;
				}
		
				return true;
			}
			catch(Exception e)
			{
				logger.Exception(100,e.Message);
				return false;
			}		
		}

		private bool QueryServerTime(NS_MESSENGER.Conference conf,ref string responseXml)
		{
			try
			{	
				// Create the RPC struct
				vbXMLRPC.XMLRPCStruct oStruct = new vbXMLRPC.XMLRPCStructClass();			
				bool ret = CreateStruct("FetchSystemInfo", conf, ref oStruct);
				if (!ret)
				{
					logger.Trace ("CreateStruct failed.");
					return false;
				}
				
				// Send command to MCU
				bool ret1 = SendCommand(conf.cMcu,"system.query", oStruct,ref responseXml);
				if (!ret1)
				{
					logger.Trace ("SendCommand failed.");
					return false;
				}				
				return true;				
			}
			catch(Exception e)
			{
				logger.Exception(100,e.Message);
				return false;
			}			
		}

		internal bool DeletePartyFromMcu(NS_MESSENGER.Party party, NS_MESSENGER.Conference conf)
		{
			try
			{	
				// Create the RPC struct
				vbXMLRPC.XMLRPCStruct oStruct = new vbXMLRPC.XMLRPCStructClass();
				bool ret = CreateStruct("TerminateParty", party, conf, ref oStruct);
				if (!ret)
				{
					logger.Trace ("Struct problem.");
					return false;
				}
				
				// Send the command to the mcu			
				string strResponse= null;
				bool ret1 = SendCommand(party.cMcu,"conference.participant.remove", oStruct,ref strResponse);				
				if (!ret1)
				{	
					logger.Trace ("SendCommand failed");
					return false;
				}
				return true;
			}
			catch(Exception ex)
			{
				logger.Exception(100,ex.Message);
				return false;
			}			
		}
		
		internal bool AddPartyToMcu(NS_MESSENGER.Party party, NS_MESSENGER.Conference conf)
		{
			try
			{		
				string strConfName = conf.sDbName;

				if((party.etType == NS_MESSENGER.Party.eType.CASCADE_LINK)&&(party.etCascadeRole == NS_MESSENGER.Party.eCascadeRole.MASTER))
				{	
					return true;
				}
				else
				{
					vbXMLRPC.XMLRPCStruct oStruct = new vbXMLRPC.XMLRPCStructClass();

					bool ret = CreateStruct("AddParty", party, conf, ref oStruct);
					if (!ret)
					{
						logger.Trace("CreateStruct failed");
						return false;
					}
					
					string strResponse = null;
					bool ret1 = SendCommand(party.cMcu,"conference.participant.add", oStruct,ref strResponse);
					if (!ret)
					{
						logger.Trace("SendCommand failed");
						return false;
					}					
					
					logger.Trace (party.etType.ToString());					

					if(cLocalMaster != null)
					{
						// copying the data from "party" to "conf" object 
						conf.cMcu.sIp = party.cMcu.sIp;
						conf.cMcu.sLogin = party.cMcu.sLogin;
						conf.cMcu.sPwd = party.cMcu.sPwd;
								
						// modify the participant layout
						logger.Trace ("Before Modify");
						ModifyParticipantLayout(party, conf);
					}
			
					return true;
				}
									
			}
			catch(Exception ex)
			{
				logger.Exception(100,ex.Message);
				return false;
			}					
		}

		private bool ModifyParticipantLayout(NS_MESSENGER.Party party, NS_MESSENGER.Conference conf)
		{
			try
			{
				string strMethod = "conference.query";
				vbXMLRPC.XMLRPCStruct oStruct = new vbXMLRPC.XMLRPCStructClass();
				vbXMLRPC.XMLRPCStruct oModStruct = new vbXMLRPC.XMLRPCStructClass();

				bool ret = CreateStruct(strMethod, conf, ref oStruct);
				if (!ret)
				{
					logger.Trace("CreateStruct failed.");
					return false;
				}
	
				string strResponse = null;
				bool ret1 = SendCommand(conf.cMcu,strMethod, oStruct,ref strResponse);
				if (!ret1)
				{
					logger.Trace("SendCommand failed.");
					return false;
				}
				
				XmlDocument oDOM = new XmlDocument();
				oDOM.LoadXml(strResponse);

				strMethod = "conference.participant.modify";

				//Member Node List
				XmlNodeList oNodeList = oDOM.SelectNodes("//member");

				//get to the "conferences" member and start to retrive the array
				foreach(XmlNode oNode in oNodeList)
				{					
					if(oNode.SelectSingleNode("name").InnerText == "participants")
					{						
						//get to each individual conference 
						XmlNodeList oInnerNodeList = oNode.SelectNodes("value/array/data/value");
						
						foreach (XmlNode oInnerNode in oInnerNodeList)
						{
							if(oInnerNode.InnerXml.IndexOf("<name>address</name>", 0) == -1)
							{
								NS_MESSENGER.Party singleParty = party;

								XmlNodeList oMemberNodeList = oInnerNode.SelectNodes("struct/member");

								foreach(XmlNode oMemberNode in oMemberNodeList)
								{									
									if(oMemberNode.SelectSingleNode("name").InnerText == "participantName")
									{
										singleParty.sName = oMemberNode.SelectSingleNode("value/string").InnerText;
										bool ret2 = CreateStruct(strMethod, singleParty, conf, ref oModStruct);
										if (!ret2)
										{
											logger.Trace ("CreateStruct failed");
											return false;
										}
										string strResponse1= null;
										bool ret3 = SendCommand(conf.cMcu,strMethod, oModStruct,ref strResponse1);
										if (!ret3)
										{
											logger.Trace ("SendCommand failed");
											return false;
										}
										break;
									}
								}
							}
							
						}

					}
				}
				
				  return true;				
			}
			catch(Exception e)
			{	
				logger.Exception(100,e.Message);
				return false;
			}

		}
		
		private bool CreateStruct(string operation, NS_MESSENGER.Conference conf, ref vbXMLRPC.XMLRPCStruct oStruct)
		{
			string sBridgeIP = conf.cMcu.sIp.Trim(); 
			string sUser = conf.cMcu.sLogin.Trim();
			string sPwd = conf.cMcu.sPwd.Trim();
			
			// check if all bridge params are not empty
			if (sBridgeIP.Length < 1 || sUser.Length < 1 || sPwd.Length < 1)
			{
				return false;
			}
			
			vbXMLRPC.XMLRPCRequest oRpc = new vbXMLRPC.XMLRPCRequestClass();
			vbXMLRPC.XMLRPCResponse oResponse = new vbXMLRPC.XMLRPCResponseClass();

			if(operation == "FetchConfInfo")
			{
				cLocalMaster.cMcu.sLogin = cLocalMaster.cMcu.sLogin.Trim();
				cLocalMaster.cMcu.sPwd = cLocalMaster.cMcu.sPwd.Trim();

				if(cLocalMaster.cMcu.sLogin.Length < 1)
				{
					cLocalMaster.cMcu.sLogin = sUser;
				}

				if(cLocalMaster.cMcu.sPwd.Length < 1)
				{
					cLocalMaster.cMcu.sPwd = sPwd;
				}			
				oStruct.AddString("authenticationUser", cLocalMaster.cMcu.sLogin);
				oStruct.AddString("authenticationPassword", cLocalMaster.cMcu.sPwd);
			}
			else
			{
				oStruct.AddString("authenticationUser", sUser);
				oStruct.AddString("authenticationPassword", sPwd);
			}

			switch (operation)
			{
				case "FetchSystemInfo":
				{
					break;
				}

				case "CreateConference":
				{
					string responseXml = null;
					bool ret = QueryServerTime(conf,ref responseXml);
					if(ret)
					{
						XmlDocument oDOM = new XmlDocument();
						oDOM.LoadXml(responseXml);

						string strCurrentTime = oDOM.SelectSingleNode("//dateTime.iso8601").InnerText;
						string year, month, date, hour, min, sec;				
		
						year = strCurrentTime.Substring(0,4);
						month = strCurrentTime.Substring(4,2);
						date = strCurrentTime.Substring(6,2);
						hour = strCurrentTime.Substring(9,2);
						min = strCurrentTime.Substring(12,2);
						sec = strCurrentTime.Substring(15,2);
			
						DateTime dtTime = new DateTime(Convert.ToInt32(year), Convert.ToInt32(month),Convert.ToInt32(date), Convert.ToInt32(hour),Convert.ToInt32(min), Convert.ToInt32(sec));
						DateTime dtEnd = new DateTime();
						dtTime = dtTime.AddSeconds(5);
						string strStart = dtTime.ToString();
						dtEnd = dtTime.AddMinutes(conf.iDuration);

						oStruct.AddString("conferenceName", conf.sDbName);
						oStruct.AddString("conferenceID", conf.sDbName);
						oStruct.AddDateTime("startTime", dtTime);
						oStruct.AddDateTime("endTime", dtEnd);					
					}
					else
					{
						return false;
					}
					break;
				}
				case "FetchConfInfo":
				{
					oStruct.AddString("conferenceName", conf.sDbName);
					break;
				}
			}
			return true;
		}
		
		*/

/*
		private bool GenerateConfName(NS_MESSENGER.Conference conf,ref string confName)
		{
			// concatenate the conf uniqueid with conf external name 
			// format : 1234-tech meeting
			// limit of total 30 chars.

			try 
			{
				confName = conf.iDbNumName.ToString()+"-"+conf.sExternalName;
				if (confName.Length > 30)
				{
					// truncate to 30 chars 
					confName = confName.Substring(0,30);
				}
				
				return true;
			}
			catch (Exception e)
			{
				logger.Exception (100,e.Message);
				return false;
			}
		}


		// Creating the RPC structure to send over to Codian MCU.


			internal bool TerminateConference(NS_MESSENGER.Conference conf)
			{
				try
				{
					// Create the xml-rpc object
					vbxmlrpc_Net.XMLRPCStruct oStruct = new vbxmlrpc_Net.XMLRPCStructClass();				
					NS_MESSENGER.Party party = new NS_MESSENGER.Party();
					party.cMcu = conf.cMcu;
					bool ret = CreateStruct("TerminateConference", party,conf, ref oStruct);					
					if(!ret)
					{
						logger.Trace ("Create Struct failed.");
					}

					string strResponse = null; ret = false;
					ret = SendCommand(conf.cMcu,"conference.destroy", oStruct,ref strResponse);				
					if(!ret)
					{
						logger.Trace ("Terminate Conference failed on Codian MCU.");
						return false;
					}

					return true;
				}
				catch(Exception ex)
				{	
					logger.Exception(100,ex.Message);
					return false;
				}
			

			}

	internal bool DeletePartyFromMcu(NS_MESSENGER.Party party, NS_MESSENGER.Conference conf)
		{
			try
			{

				vbxmlrpc_Net.XMLRPCStruct oStruct = new vbxmlrpc_Net.XMLRPCStructClass();
				bool ret = CreateStruct("TerminateParty", party, conf, ref oStruct);			
				if(!ret)
				{
					logger.Trace ("Create Struct failed.");
					return false;
				}

				string strResponse = null; ret = false;
				ret = SendCommand(party.cMcu,"participant.remove", oStruct,ref strResponse);				
				if (!ret)
				{
					logger.Trace ("Send cmd failed.");
					return false;
				}

				return true;
			}
			catch(Exception ex)
			{
				logger.Exception(100,ex.Message);
				return false;
			}			
		}
		
		
			private bool CreateStruct(string strMethodName, NS_MESSENGER.Conference conf, ref vbxmlrpc_Net.XMLRPCStruct oStruct)
			{
				if(strBridgeIP == "")
				{
					strBridgeIP = conf.cMcu.sIp;
					strUser = conf.cMcu.sLogin;
					strPwd = conf.cMcu.sPwd;
				}


				/*loggerger.Trace ("###" + conf.cMcu.m_dbID.ToString() + "###");
				loggerger.Trace ("###" + conf.cMcu.m_ip + "###");
				loggerger.Trace ("###" + conf.cMcu.m_login + "###");
				loggerger.Trace ("###" + conf.cMcu.m_name + "###");
				loggerger.Trace ("###" + conf.iDbID.ToString() + "###");
				loggerger.Trace ("###" + conf.sDbName + "###");
				loggerger.Trace ("###" + conf.m_mcuName + "###");*/

				vbxmlrpc_Net.XMLRPCRequest oRpc = new vbxmlrpc_Net.XMLRPCRequestClass();
				vbxmlrpc_Net.XMLRPCResponse oResponse = new vbxmlrpc_Net.XMLRPCResponseClass();

				oStruct.AddString("authenticationUser", strUser);
				oStruct.AddString("authenticationPassword", strPwd);

				switch (strMethodName)
				{
					case "conference.query":
						oStruct.AddString("conferenceName", conf.sMcuName);
					
						break;
					case "conference.destroy":
						oStruct.AddString("conferenceName", conf.sMcuName);
						break;

				}

				return true;
			}
		
private bool CreateStruct(string strMethodName, NS_MESSENGER.MCU mcu, ref vbxmlrpc_Net.XMLRPCStruct oStruct)
			{
				if(strBridgeIP == "")
				{
					strBridgeIP = mcu.sIp;
					strUser = mcu.sLogin;
					strPwd = mcu.sPwd;
				}

				logger.Trace(strBridgeIP + " p");
				logger.Trace(strUser + " q");
				logger.Trace(strPwd + " r");

				vbxmlrpc_Net.XMLRPCRequest oRpc = new vbxmlrpc_Net.XMLRPCRequestClass();
				vbxmlrpc_Net.XMLRPCResponse oResponse = new vbxmlrpc_Net.XMLRPCResponseClass();

				oStruct.AddString("authenticationUser", strUser);
				oStruct.AddString("authenticationPassword", strPwd);

				return true;
			}

	private string SendCommand(string strMethodName, vbxmlrpc_Net.XMLRPCStruct oStruct)
			{
				try
				{
					/*string strBridgeIP = mcu.m_ip;
					string strUser = mcu.m_login;
					string strPwd = mcu.m_pwd;*/
			
					vbxmlrpc_Net.XMLRPCRequest oRpc = new vbxmlrpc_Net.XMLRPCRequestClass();
					vbxmlrpc_Net.XMLRPCResponse oResponse = new vbxmlrpc_Net.XMLRPCResponseClass();

					oRpc.HostName = strBridgeIP;
					oRpc.Username = strUser;
					oRpc.Password = strPwd;
					oRpc.HostURI = "/RPC2";
					oRpc.HostPort = 80;
		
					oRpc.Params.AddStruct(oStruct);
					oRpc.MethodName = strMethodName;
					logger.Trace (oRpc.XMLToSend);
					oResponse = oRpc.Submit();
					logger.Trace (oResponse.XMLResponse);

					if(oResponse.XMLResponse.IndexOf("<fault>", 0) > 0)
					{
						logger.Trace (oRpc.XMLToSend);
						logger.Trace (oResponse.XMLResponse);
						LogCodianFaultResponse(strBridgeIP,oResponse.XMLResponse, strMethodName);
						return "";
					}
					else
					{
						return oResponse.XMLResponse;
					}
				
				}
				catch(Exception ex)
				{	
					logger.Exception(100,ex.Message);
					return "";
				}
			
			}


#endif 
