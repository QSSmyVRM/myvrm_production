﻿//ZD 100147 Start
/* Copyright (C) 2015 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
//ZD 100147 ZD 100886 End
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.Xml.Linq; //Fb 2593
using System.IO; // FB 2683
using LumenWorks.Framework.IO.Csv; // FB 2683


namespace NS_CallDetailRecords
{
    class CallDetailRecords
    {
        private NS_LOGGER.Log logger;
        private NS_MESSENGER.ConfigParams configParams;
        private NS_DATABASE.Database db;
        internal string errMsg = "";
        Dictionary<string, NS_MESSENGER.Conference> listedConference = null;

        internal CallDetailRecords(NS_MESSENGER.ConfigParams config)
        {
            configParams = new NS_MESSENGER.ConfigParams();
            configParams = config;
            logger = new NS_LOGGER.Log(configParams);
            db = new NS_DATABASE.Database(configParams);
        }

        public class CDR
        {
            public string conferenceGUID { get; set; }
            public string participantGUID { get; set; }
            public string eventType { get; set; }
            public string confName { get; set; }
            public string numericId { get; set; }
            public string uri { get; set; }

        }

        #region CDRforCisco8710
        /// <summary>
        /// CDRforCisco8710
        /// </summary>
        /// <param name="MCU"></param>
        /// <returns></returns>
        public bool CDRforCisco8710(NS_MESSENGER.MCU MCU)
        {
            XmlDocument xd = new XmlDocument();
            NS_MESSENGER.CDREvent cdrEvent = null;
            List<NS_MESSENGER.CDREvent> cdrEventLists = null;
            XmlNode oEventsNode = null;
            XmlNodeList oNodeList = null;
            int ndeCnt = 0; int eventsRemainin = 0;
            XmlNodeList oInnerNodeList = null;
            XmlNode oNode = null; ;
            XmlNode oInnerNode = null;
            NS_CiscoTPServer.CiscoTPServer tpServerCDR = null;
            string strResponse = "";
            KeyValuePair<string, NS_MESSENGER.Conference> foundConference = new KeyValuePair<string, NS_MESSENGER.Conference>();
            string dtPattern = "yyyyMMddTHH:mm:ss";
            DateTime dtCdrTime = DateTime.Now;
            List<string> GUIDlist = new List<string>();
            List<string> ConfGUIDlist = new List<string>();
            int CDRCount = 0; //ZD 101348

            try
            {
                logger.Trace("Entering inot CDRforCisco8710 Method");
                int index = 0, CDRIndex = 0;
                string strCurrentTime = "";

            GetCDR: tpServerCDR = new NS_CiscoTPServer.CiscoTPServer(configParams);

                if (index == 0)
                {
                    CDRIndex = db.FetchMaxCDRIndex(CDRIndex, "CDR_Cisco_D");
                    index = CDRIndex + 1;
                }
                logger.Trace("CDR Index: " + index);

                if (!tpServerCDR.CallDetailRecords(MCU, index, ref strResponse))
                    return false;

                xd.LoadXml(strResponse);
                oEventsNode = xd.SelectSingleNode("//member");

                if (oEventsNode != null)
                {
                    oNodeList = oEventsNode.SelectNodes("value/array/data/value");
                    for (ndeCnt = 0; ndeCnt < oNodeList.Count; ndeCnt++)
                    {
                        CDRCount++; //ZD 101348
                        cdrEvent = new NS_MESSENGER.CDREvent();
                        strCurrentTime = "";

                        oNode = oNodeList[ndeCnt];
                        if (oNode.SelectSingleNode("struct/member[name = \"index\"]/value/int") != null)
                            int.TryParse(oNode.SelectSingleNode("struct/member[name = \"index\"]/value/int").InnerText.ToString().Trim(), out cdrEvent.iIndex);

                        if (oNode.SelectSingleNode("struct/member[name = \"time\"]/value/dateTime.iso8601") != null)
                            strCurrentTime = oNode.SelectSingleNode("struct/member[name = \"time\"]/value/dateTime.iso8601").InnerText.ToString().Trim();

                        DateTime.TryParseExact(strCurrentTime, dtPattern, null, DateTimeStyles.None, out dtCdrTime);

                        if (dtCdrTime <= DateTime.MinValue)
                            dtCdrTime = DateTime.Now;

                        cdrEvent.dTime = db.ConverttoGMT(dtCdrTime, MCU.iTimezoneID); //FB 2593

                        logger.Trace("strCurrentTime:" + strCurrentTime);
                        logger.Trace("cdrEvent.dTime :" + cdrEvent.dTime);

                        if (oNode.SelectSingleNode("struct/member[name = \"type\"]/value/string") != null)
                            cdrEvent.sEventType = oNode.SelectSingleNode("struct/member[name = \"type\"]/value/string").InnerText.ToString().Trim();

                        if (oNode.SelectSingleNode("struct/member[name = \"" + cdrEvent.sEventType.Trim() + "\"]") != null)
                        {
                            oInnerNodeList = oNode.SelectNodes("struct/member/value/struct");
                            for (int innerNdeCnt = 0; innerNdeCnt < oInnerNodeList.Count; innerNdeCnt++)
                            {
                                oInnerNode = oInnerNodeList[innerNdeCnt];

                                if (oInnerNode.SelectSingleNode("member[name = \"conferenceGUID\"]/value/string") != null)
                                {
                                    cdrEvent.sConferenceGUID = oInnerNode.SelectSingleNode("member[name = \"conferenceGUID\"]/value/string").InnerText.ToString().Trim();
                                    cdrEvent.sMessage += " ConferenceGUID: " + cdrEvent.sConferenceGUID;
                                    if (!ConfGUIDlist.Contains(cdrEvent.sConferenceGUID))
                                        ConfGUIDlist.Add(cdrEvent.sConferenceGUID);
                                }
                                if (oInnerNode.SelectSingleNode("member[name = \"participantGUID\"]/value/string") != null)
                                {
                                    cdrEvent.sParticipantGUID = oInnerNode.SelectSingleNode("member[name = \"participantGUID\"]/value/string").InnerText.ToString().Trim();
                                    cdrEvent.sMessage += " ParticipantGUID: " + cdrEvent.sParticipantGUID;
                                }

                                if (oInnerNode.SelectSingleNode("member[name = \"callID\"]/value/string") != null)
                                {
                                    cdrEvent.sCallID = oInnerNode.SelectSingleNode("member[name = \"callID\"]/value/string").InnerText.ToString().Trim();
                                    cdrEvent.sMessage += " CallID: " + cdrEvent.sCallID;
                                }

                                cdrEvent.sScheduledTime = "0";
                                //1
                                if (cdrEvent.sEventType.Trim() == "conferenceStarted")
                                {
                                    if (oInnerNode.SelectSingleNode("member[name = \"name\"]/value/string") != null)
                                    {
                                        cdrEvent.sName = oInnerNode.SelectSingleNode("member[name = \"name\"]/value/string").InnerText.ToString().Trim();
                                        cdrEvent.sMessage += " Name: " + cdrEvent.sName;
                                    }
                                    if (oInnerNode.SelectSingleNode("member[name = \"numericId\"]/value/string") != null)
                                    {
                                        int.TryParse(oInnerNode.SelectSingleNode("member[name = \"numericId\"]/value/string").InnerText.ToString().Trim(), out cdrEvent.iNumericId);
                                        cdrEvent.sMessage += " NumericId: " + cdrEvent.iNumericId;
                                    }
                                    if (oInnerNode.SelectSingleNode("member[name = \"uris\"]/value/array/data/value/struct/member[name = \"uri\"]/value/string") != null)
                                    {
                                        cdrEvent.sUri = oInnerNode.SelectSingleNode("member[name = \"uris\"]/value/array/data/value/struct/member[name = \"uri\"]/value/string").InnerText.ToString().Trim();
                                        cdrEvent.sMessage += " URI: " + cdrEvent.sUri;
                                    }
                                    if (oInnerNode.SelectSingleNode("member[name = \"uris\"]/value/array/data/value/struct/member[name = \"pinProtected\"]/value/string") != null)
                                    {
                                        cdrEvent.sPinProtected = oInnerNode.SelectSingleNode("member[name = \"uris\"]/value/array/data/value/struct/member[name = \"pinProtected\"]/value/string").InnerText.ToString().Trim();
                                        cdrEvent.sMessage += " PinProtected: " + cdrEvent.sPinProtected;
                                    }
                                    if (oInnerNode.SelectSingleNode("member[name = \"scheduledTime\"]/value/dateTime.iso8601") != null)
                                    {
                                        cdrEvent.sScheduledTime = oInnerNode.SelectSingleNode("member[name = \"scheduledTime\"]/value/dateTime.iso8601").InnerText.ToString().Trim();
                                        cdrEvent.sMessage += " scheduledTime: " + cdrEvent.sScheduledTime;
                                    }
                                }
                                //2
                                if (cdrEvent.sEventType.Trim() == "conferenceInactive" || cdrEvent.sEventType.Trim() == "conferenceFinished")
                                {
                                    if (oInnerNode.SelectSingleNode("member[name = \"maxSimultaneousAudioOnlyParticipants\"]/value/int") != null)
                                    {
                                        int.TryParse(oInnerNode.SelectSingleNode("member[name = \"maxSimultaneousAudioOnlyParticipants\"]/value/int").InnerText.ToString().Trim(), out cdrEvent.iMaxSimultaneousAudioOnlyParticipants);
                                        cdrEvent.sMessage += " MaxSimultaneousAudioOnlyParticipants: " + cdrEvent.iMaxSimultaneousAudioOnlyParticipants;
                                    }
                                    if (oInnerNode.SelectSingleNode("member[name = \"maxSimultaneousAudioVideoParticipants\"]/value/int") != null)
                                    {
                                        int.TryParse(oInnerNode.SelectSingleNode("member[name = \"maxSimultaneousAudioVideoParticipants\"]/value/int").InnerText.ToString().Trim(), out cdrEvent.iMaxSimultaneousAudioVideoParticipants);
                                        cdrEvent.sMessage += " MaxSimultaneousAudioOnlyParticipants: " + cdrEvent.iMaxSimultaneousAudioVideoParticipants;
                                    }
                                    if (oInnerNode.SelectSingleNode("member[name = \"totalAudioVideoParticipants\"]/value/int") != null)
                                    {
                                        int.TryParse(oInnerNode.SelectSingleNode("member[name = \"totalAudioVideoParticipants\"]/value/int").InnerText.ToString().Trim(), out cdrEvent.iTotalAudioVideoParticipants);
                                        cdrEvent.sMessage += " TotalAudioVideoParticipants: " + cdrEvent.iTotalAudioVideoParticipants;
                                    }
                                    if (oInnerNode.SelectSingleNode("member[name = \"totalAudioOnlyParticipants\"]/value/int") != null)
                                    {
                                        int.TryParse(oInnerNode.SelectSingleNode("member[name = \"totalAudioOnlyParticipants\"]/value/int").InnerText.ToString().Trim(), out cdrEvent.iTotalAudioOnlyParticipants);
                                        cdrEvent.sMessage += " TotalAudioOnlyParticipants: " + cdrEvent.iTotalAudioOnlyParticipants;
                                    }
                                    if (oInnerNode.SelectSingleNode("member[name = \"sessionDuration\"]/value/int") != null)
                                    {
                                        int.TryParse(oInnerNode.SelectSingleNode("member[name = \"sessionDuration\"]/value/int").InnerText.ToString().Trim(), out cdrEvent.iSessionDuration);
                                        cdrEvent.sMessage += " Session Duration: " + cdrEvent.iSessionDuration;
                                    }
                                    if (oInnerNode.SelectSingleNode("member[name = \"duration\"]/value/int") != null)
                                    {
                                        int.TryParse(oInnerNode.SelectSingleNode("member[name = \"duration\"]/value/int").InnerText.ToString().Trim(), out cdrEvent.iDuration);
                                        cdrEvent.sMessage += " Duration: " + cdrEvent.iDuration;
                                    }
                                }
                                //3
                                if (cdrEvent.sEventType.Trim() == "participantConnected")
                                {
                                    if (oInnerNode.SelectSingleNode("member[name = \"callDirection\"]/value/string") != null)
                                    {
                                        cdrEvent.sCallDirection = oInnerNode.SelectSingleNode("member[name = \"callDirection\"]/value/string").InnerText.ToString().Trim();
                                        cdrEvent.sMessage += " CallDirection: " + cdrEvent.sCallDirection;
                                    }
                                    if (oInnerNode.SelectSingleNode("member[name = \"callProtocol\"]/value/string") != null)
                                    {
                                        cdrEvent.sCallProtocol = oInnerNode.SelectSingleNode("member[name = \"callProtocol\"]/value/string").InnerText.ToString().Trim();
                                        cdrEvent.sMessage += " CallProtocol: " + cdrEvent.sCallProtocol;
                                    }
                                    if (oInnerNode.SelectSingleNode("member[name = \"endpointIPAddress\"]/value/string") != null)
                                    {
                                        cdrEvent.sEndpointIPAddress = oInnerNode.SelectSingleNode("member[name = \"endpointIPAddress\"]/value/string").InnerText.ToString().Trim();
                                        cdrEvent.sMessage += " EndpointIPAddress: " + cdrEvent.sEndpointIPAddress;
                                    }
                                    if (oInnerNode.SelectSingleNode("member[name = \"endpointDisplayName\"]/value/string") != null)
                                    {
                                        cdrEvent.sEndpointDisplayName = oInnerNode.SelectSingleNode("member[name = \"endpointDisplayName\"]/value/string").InnerText.ToString().Trim();
                                        cdrEvent.sMessage += " Endpoint Display Name: " + cdrEvent.sEndpointDisplayName;
                                    }
                                    if (oInnerNode.SelectSingleNode("member[name = \"endpointURI\"]/value/string") != null)
                                    {
                                        cdrEvent.sEndpointURI = oInnerNode.SelectSingleNode("member[name = \"endpointURI\"]/value/string").InnerText.ToString().Trim();
                                        cdrEvent.sMessage += " EndpointURI: " + cdrEvent.sEndpointURI;
                                    }
                                    if (oInnerNode.SelectSingleNode("member[name = \"endpointConfiguredName\"]/value/string") != null)
                                    {
                                        cdrEvent.sEndpointConfiguredName = oInnerNode.SelectSingleNode("member[name = \"endpointConfiguredName\"]/value/string").InnerText.ToString().Trim();
                                        cdrEvent.sMessage += " Endpoint Configured Name: " + cdrEvent.sEndpointConfiguredName;
                                    }
                                }
                                //4
                                if (cdrEvent.sEventType.Trim() == "participantDisconnected")
                                {
                                    if (oInnerNode.SelectSingleNode("member[name = \"disconnectReason\"]/value/string") != null)
                                    {
                                        cdrEvent.sDisconnectReason = oInnerNode.SelectSingleNode("member[name = \"disconnectReason\"]/value/string").InnerText.ToString().Trim();
                                        cdrEvent.sDisconnectReason += " DisconnectReason: " + cdrEvent.sDisconnectReason;
                                    }
                                }
                                //5
                                if (cdrEvent.sEventType.Trim() == "participantLeft")
                                {
                                    if (oInnerNode.SelectSingleNode("member[name = \"timeInConference\"]/value/int") != null)
                                    {
                                        cdrEvent.sTimeInConference = oInnerNode.SelectSingleNode("member[name = \"timeInConference\"]/value/int").InnerText.ToString().Trim();
                                        cdrEvent.sMessage += " time In Conference: " + cdrEvent.sTimeInConference;
                                    }
                                }
                                //6
                                if (cdrEvent.sEventType.Trim() == "participantMediaSummary")
                                {
                                    if (oInnerNode.SelectSingleNode("member[name = \"width\"]/value/int") != null)
                                    {
                                        int.TryParse(oInnerNode.SelectSingleNode("member[name = \"width\"]/value/int").InnerText.ToString().Trim(), out cdrEvent.iWidth);
                                        cdrEvent.sMessage += " Width: " + cdrEvent.iWidth;
                                    }
                                    if (oInnerNode.SelectSingleNode("member[name = \"height\"]/value/int") != null)
                                    {
                                        int.TryParse(oInnerNode.SelectSingleNode("member[name = \"height\"]/value/int").InnerText.ToString().Trim(), out cdrEvent.iHeight);
                                        cdrEvent.sMessage += " Height" + cdrEvent.iHeight;
                                    }
                                    if (oInnerNode.SelectSingleNode("member[name = \"maxBandwidth\"]/value/int") != null)
                                    {
                                        int.TryParse(oInnerNode.SelectSingleNode("member[name = \"maxBandwidth\"]/value/int").InnerText.ToString().Trim(), out cdrEvent.iMaxBandwidth);
                                        cdrEvent.sMessage += " MaxBandwidth: " + cdrEvent.iMaxBandwidth;
                                    }
                                    if (oInnerNode.SelectSingleNode("member[name = \"bandwidth\"]/value/int") != null)
                                    {
                                        int.TryParse(oInnerNode.SelectSingleNode("member[name = \"bandwidth\"]/value/int").InnerText.ToString().Trim(), out cdrEvent.iBandwidth);
                                        cdrEvent.sMessage += " Bandwidth: " + cdrEvent.iBandwidth;
                                    }
                                    if (oInnerNode.SelectSingleNode("member[name = \"packetsReceived\"]/value/int") != null)
                                    {
                                        int.TryParse(oInnerNode.SelectSingleNode("member[name = \"packetsReceived\"]/value/int").InnerText.ToString().Trim(), out cdrEvent.iPacketsReceived);
                                        cdrEvent.sMessage += " PacketsReceived: " + cdrEvent.iPacketsReceived;
                                    }
                                    if (oInnerNode.SelectSingleNode("member[name = \"packetsLost\"]/value/int") != null)
                                    {
                                        int.TryParse(oInnerNode.SelectSingleNode("member[name = \"packetsLost\"]/value/int").InnerText.ToString().Trim(), out cdrEvent.iPacketsLost);
                                        cdrEvent.sMessage += " PacketsLost: " + cdrEvent.iPacketsLost;
                                    }
                                }
                                //FB 2593 End
                            }
                            if (listedConference == null)
                                listedConference = new Dictionary<string, NS_MESSENGER.Conference>();

                            foundConference = listedConference.Where(l => l.Key == cdrEvent.sConferenceGUID.Trim() || l.Key == cdrEvent.sParticipantGUID.Trim()).FirstOrDefault();
                            if (foundConference.Value != null)
                                cdrEvent.confDetails = foundConference.Value;
                            else
                            {
                                cdrEvent.confDetails = new NS_MESSENGER.Conference();
                                cdrEvent.confDetails.sGUID = cdrEvent.sConferenceGUID.Trim();
                                db.FetchConfforCallDetailRecord(ref cdrEvent.confDetails, cdrEvent.sParticipantGUID.Trim(), MCU);
                                if (cdrEvent.confDetails.iDbID > 0)
                                    listedConference.Add(cdrEvent.sParticipantGUID.Trim() == "" ? cdrEvent.sConferenceGUID : cdrEvent.sParticipantGUID.Trim(), cdrEvent.confDetails);
                            }

                            if (cdrEventLists == null)
                                cdrEventLists = new List<NS_MESSENGER.CDREvent>();

                            cdrEventLists.Add(cdrEvent);
                        }
                    }
                }

                oNodeList = xd.SelectNodes("//param/value/struct/member");
                eventsRemainin = 0;
                for (ndeCnt = 0; ndeCnt < oNodeList.Count; ndeCnt++)
                {
                    oNode = oNodeList[ndeCnt];

                    if (oNode.SelectSingleNode("//member[name = \"nextIndex\"]/value/int") != null)
                        int.TryParse(oNode.SelectSingleNode("//member[name = \"nextIndex\"]/value/int").InnerText, out index);
                    logger.Trace("nextIndex: " + index);

                    if (oNode.SelectSingleNode("//member[name = \"eventsRemaining\"]/value/boolean") != null)
                        int.TryParse(oNode.SelectSingleNode("//member[name = \"eventsRemaining\"]/value/boolean").InnerText, out eventsRemainin);
                }

                if (eventsRemainin > 0 && CDRCount <= 500) //ZD 101348
                    goto GetCDR;

                if (cdrEventLists != null)
                {
                    if (!ProcessandInsertCDR(ref cdrEventLists, ref MCU, ref listedConference, ref ConfGUIDlist))
                    {
                        logger.Exception(100, "Insert into CDR table Process is failed in Codian 8710.");
                    }
                }
                else
                {
                    logger.Trace("No Conference to add in CDR Tables in Codian 8710");
                }

                //ZD 101348
                if (eventsRemainin > 0)
                {
                    CDRCount = 0;
                    goto GetCDR;
                }

				//ZD 101348 - Commented as new table is created.
                //if (!ProcessandInsertVMRCDR(ref MCU))
                //{
                    //logger.Exception(100, "Insert process of VMR Conferences is failed in Codian 8710.");
                //}


            }
            catch (Exception ex)
            {
                logger.Trace("CDRforCisco8710 is failed: " + ex.Message);
                return false;
            }
            return true;
        }
        #endregion

        //FB 2797 Start

        #region CDRforCodianMSESeries
        /// <summary>
        /// CDRforCodianMSESeries
        /// </summary>
        /// <param name="MCU"></param>
        /// <returns></returns>
        public bool CDRforCodianMSESeries(NS_MESSENGER.MCU MCU)
        {
            XmlDocument xd = new XmlDocument();
            NS_MESSENGER.CDREvent cdrEvent = null;
            List<NS_MESSENGER.CDREvent> cdrEventLists = null;
            List<int> uniqueidlist = new List<int>();
            XmlNode oEventsNode = null;
            XmlNodeList oNodeList = null;
            int ndeCnt = 0; int eventsRemainin = 0;
            XmlNode oNode = null; ;
            NS_CODIAN.Codian tpServerCDR = null;
            string strResponse = "";
            KeyValuePair<string, NS_MESSENGER.Conference> foundConference = new KeyValuePair<string, NS_MESSENGER.Conference>();
            string dtPattern = "yyyyMMddTHH:mm:ss";
            DateTime dtCdrTime = DateTime.Now;
            List<string> ConfNumnamelist = new List<string>();
            int CDRCount = 0; //ZD 101348
            try
            {
                logger.Trace("Entering into CDRforCodian");
                int index = 0, CDRIndex = 0;
                string strCurrentTime = "";

            GetCDR: tpServerCDR = new NS_CODIAN.Codian(configParams);

                if (index == 0)
                {
                    CDRIndex = db.FetchMaxCDRIndex(CDRIndex, "CDR_Codian_D");
                    index = CDRIndex + 1;
                }
                logger.Trace("CDR Index: " + index);

                if (!tpServerCDR.CallDetailRecords(MCU, index, ref strResponse))
                    return false;

                xd.LoadXml(strResponse);
                oEventsNode = xd.SelectSingleNode("//member");

                if (oEventsNode != null)
                {
                    oNodeList = oEventsNode.SelectNodes("value/array/data/value");
                    for (ndeCnt = 0; ndeCnt < oNodeList.Count; ndeCnt++)
                    {
                        CDRCount++;//ZD 101348
                        cdrEvent = new NS_MESSENGER.CDREvent();
                        strCurrentTime = "";

                        oNode = oNodeList[ndeCnt];
                        if (oNode.SelectSingleNode("struct/member[name = \"index\"]/value/int") != null)
                            int.TryParse(oNode.SelectSingleNode("struct/member[name = \"index\"]/value/int").InnerText.ToString().Trim(), out cdrEvent.iIndex);

                        if (oNode.SelectSingleNode("struct/member[name = \"time\"]/value/dateTime.iso8601") != null)
                            strCurrentTime = oNode.SelectSingleNode("struct/member[name = \"time\"]/value/dateTime.iso8601").InnerText.ToString().Trim();

                        DateTime.TryParseExact(strCurrentTime, dtPattern, null, DateTimeStyles.None, out dtCdrTime);

                        if (dtCdrTime <= DateTime.MinValue)
                            dtCdrTime = DateTime.Now;

                        cdrEvent.dTime = dtCdrTime.ToUniversalTime();

                        logger.Trace("strCurrentTime:" + strCurrentTime);
                        logger.Trace("cdrEvent.dTime :" + cdrEvent.dTime);

                        if (oNode.SelectSingleNode("struct/member[name = \"type\"]/value/string") != null)
                            cdrEvent.sEventType = oNode.SelectSingleNode("struct/member[name = \"type\"]/value/string").InnerText.ToString().Trim();

                        //1
                        if (cdrEvent.sEventType == "scheduledConferenceStarted" || cdrEvent.sEventType == "participantJoined" || cdrEvent.sEventType == "participantLeft" || cdrEvent.sEventType == "conferenceFinished" || cdrEvent.sEventType == "ad-hocconferencestarted")
                        {
                            if (oNode.SelectSingleNode("struct/member[name = \"conference\"]/value/struct/member[name = \"uniqueId\"]/value/int") != null)
                            {
                                int.TryParse(oNode.SelectSingleNode("struct/member[name = \"conference\"]/value/struct/member[name = \"uniqueId\"]/value/int").InnerText.ToString().Trim(), out cdrEvent.iuniqueId);
                                cdrEvent.sMessage += " uniqueId: " + cdrEvent.iuniqueId;
                            }

                            if (oNode.SelectSingleNode("struct/member[name = \"conference\"]/value/struct/member[name = \"name\"]/value/string") != null)
                            {
                                cdrEvent.sName = oNode.SelectSingleNode("struct/member[name = \"conference\"]/value/struct/member[name = \"name\"]/value/string").InnerText.ToString().Trim();
                                cdrEvent.sMessage += " Name: " + cdrEvent.sName;

                                if (cdrEvent.sName.Contains("(") && cdrEvent.sName.Contains(")"))
                                {
                                    int n = cdrEvent.sName.Split('(').Count();
                                    string p = cdrEvent.sName.Split('(')[n - 1].Split(')')[0];
                                    int.TryParse(p, out cdrEvent.iConfnumname);
                                }
                                if (cdrEvent.iConfnumname > 0)
                                    if (!ConfNumnamelist.Contains(cdrEvent.iConfnumname.ToString()))
                                        ConfNumnamelist.Add(cdrEvent.iConfnumname.ToString());
                            }
                        }
                        //2
                        if (cdrEvent.sEventType == "scheduledConferenceStarted" || cdrEvent.sEventType == "ad-hocconferencestarted")
                        {
                            if (oNode.SelectSingleNode("struct/member[name = \"conferenceDetails\"]/value/struct/member[name = \"numericId\"]/value/string") != null)
                            {
                                int NumericId = 0;
                                int.TryParse(oNode.SelectSingleNode("struct/member[name = \"conferenceDetails\"]/value/struct/member[name = \"numericId\"]/value/string").InnerText.ToString().Trim(), out NumericId);
                                cdrEvent.iNumericId = NumericId;
                                cdrEvent.sMessage += " NumericId: " + cdrEvent.iNumericId;
                            }

                            if (oNode.SelectSingleNode("struct/member[name = \"conferenceDetails\"]/value/struct/member[name = \"hasPin\"]/value/string") != null)
                            {
                                cdrEvent.sPinProtected = oNode.SelectSingleNode("struct/member[name = \"conferenceDetails\"]/value/struct/member[name = \"hasPin\"]/value/string").InnerText.ToString().Trim();
                                cdrEvent.sMessage += " hasPin: " + cdrEvent.sPinProtected;
                            }

                            if (oNode.SelectSingleNode("struct/member[name = \"conferenceDetails\"]/value/struct/member[name = \"billingCode\"]/value/string") != null)
                            {
                                cdrEvent.sbillingCode = oNode.SelectSingleNode("struct/member[name = \"conferenceDetails\"]/value/struct/member[name = \"billingCode\"]/value/string").InnerText.ToString().Trim();
                                cdrEvent.sMessage += " billingCode: " + cdrEvent.sbillingCode;
                            }
                        }
                        //3
                        if (cdrEvent.sEventType == "scheduledConferenceStarted")
                        {
                            if (oNode.SelectSingleNode("struct/member[name = \"owner\"]/value/struct/member[name = \"name\"]/value/string") != null)
                            {
                                cdrEvent.sownername = oNode.SelectSingleNode("struct/member[name = \"owner\"]/value/struct/member[name = \"name\"]/value/string").InnerText.ToString().Trim();
                                cdrEvent.sMessage += " OwnerName: " + cdrEvent.sownername;
                            }

                            if (oNode.SelectSingleNode("struct/member[name = \"end\"]/value/struct/member[name = \"scheduleddate\"]/value/string") != null)
                            {
                                cdrEvent.sScheduleddate = oNode.SelectSingleNode("struct/member[name = \"end\"]/value/struct/member[name = \"scheduleddate\"]/value/string").InnerText.ToString().Trim();
                                cdrEvent.sMessage += " scheduleddate: " + cdrEvent.sScheduleddate;
                            }

                            if (oNode.SelectSingleNode("struct/member[name = \"end\"]/value/struct/member[name = \"scheduledTime\"]/value/string") != null)
                            {
                                cdrEvent.sScheduledTime = oNode.SelectSingleNode("struct/member[name = \"end\"]/value/struct/member[name = \"scheduledTime\"]/value/string").InnerText.ToString().Trim();
                                cdrEvent.sMessage += " scheduledTime: " + cdrEvent.sScheduledTime;
                            }

                            if (oNode.SelectSingleNode("struct/member[name = \"end\"]/value/struct/member[name = \"scheduleddurationinminutes\"]/value/string") != null)
                            {
                                cdrEvent.sScheduleddurationinminutes = oNode.SelectSingleNode("struct/member[name = \"end\"]/value/struct/member[name = \"scheduleddurationinminutes\"]/value/string").InnerText.ToString().Trim();
                                cdrEvent.sMessage += " scheduleddurationinminutes: " + cdrEvent.sScheduleddurationinminutes;
                            }
                        }
                        //4
                        if (cdrEvent.sEventType == "participantJoined" || cdrEvent.sEventType == "participantLeft")
                        {
                            if (oNode.SelectSingleNode("struct/member[name = \"participants\"]/value/struct/member[name = \"participantId\"]/value/int") != null)
                            {
                                int.TryParse(oNode.SelectSingleNode("struct/member[name = \"participants\"]/value/struct/member[name = \"participantId\"]/value/int").InnerText.ToString().Trim(), out cdrEvent.iparticipantId);
                                cdrEvent.sMessage += " participantId: " + cdrEvent.iparticipantId;
                            }
                        }
                        //5
                        if (cdrEvent.sEventType == "participantJoined")
                        {
                            if (oNode.SelectSingleNode("struct/member[name = \"call\"]/value/struct/member[name = \"direction\"]/value/string") != null)
                            {
                                cdrEvent.sDirection = oNode.SelectSingleNode("struct/member[name = \"call\"]/value/struct/member[name = \"direction\"]/value/string").InnerText.ToString().Trim();
                                cdrEvent.sMessage += " direction: " + cdrEvent.sDirection;
                            }
                        }
                        //6
                        if (cdrEvent.sEventType == "participantLeft")
                        {
                            if (oNode.SelectSingleNode("struct/member[name = \"endpointDetails\"]/value/struct/member[name = \"ipAddress\"]/value/string") != null)
                            {
                                cdrEvent.sEndpointIPAddress = oNode.SelectSingleNode("struct/member[name = \"endpointDetails\"]/value/struct/member[name = \"ipAddress\"]/value/string").InnerText.ToString().Trim();
                                cdrEvent.sMessage += " ipAddress: " + cdrEvent.sEndpointIPAddress;
                            }

                            if (oNode.SelectSingleNode("struct/member[name = \"endpointDetails\"]/value/struct/member[name = \"dn\"]/value/string") != null)
                            {
                                cdrEvent.sdn = oNode.SelectSingleNode("struct/member[name = \"endpointDetails\"]/value/struct/member[name = \"dn\"]/value/string").InnerText.ToString().Trim();
                                cdrEvent.sMessage += " dn: " + cdrEvent.sdn;
                            }

                            if (oNode.SelectSingleNode("struct/member[name = \"endpointDetails\"]/value/struct/member[name = \"h323Alias\"]/value/string") != null)
                            {
                                cdrEvent.sh323Alias = oNode.SelectSingleNode("struct/member[name = \"endpointDetails\"]/value/struct/member[name = \"h323Alias\"]/value/string").InnerText.ToString().Trim();
                                cdrEvent.sMessage += " h323Alias: " + cdrEvent.sh323Alias;
                            }

                            if (oNode.SelectSingleNode("struct/member[name = \"endpointDetails\"]/value/struct/member[name = \"configuredName\"]/value/string") != null)
                            {
                                cdrEvent.sEndpointConfiguredName = oNode.SelectSingleNode("struct/member[name = \"endpointDetails\"]/value/struct/member[name = \"configuredName\"]/value/string").InnerText.ToString().Trim();
                                cdrEvent.sMessage += " configuredName: " + cdrEvent.sEndpointConfiguredName;
                            }

                            if (oNode.SelectSingleNode("struct/member[name = \"call\"]/value/struct/member[name = \"timeInConference\"]/value/int") != null)
                            {
                                int.TryParse(oNode.SelectSingleNode("struct/member[name = \"call\"]/value/struct/member[name = \"timeInConference\"]/value/int").InnerText.ToString().Trim(), out cdrEvent.iTimeInConference);
                                cdrEvent.sMessage += " timeInConference: " + cdrEvent.iTimeInConference.ToString();
                            }

                            if (oNode.SelectSingleNode("struct/member[name = \"call\"]/value/struct/member[name = \"timeInConferenceInMinutes\"]/value/int") != null)
                            {
                                int.TryParse(oNode.SelectSingleNode("struct/member[name = \"call\"]/value/struct/member[name = \"timeInConferenceInMinutes\"]/value/int").InnerText.ToString().Trim(), out cdrEvent.itimeInConferenceInMinutes);
                                cdrEvent.sMessage += " timeInConferenceInMinutes: " + cdrEvent.itimeInConferenceInMinutes;
                            }


                            if (oNode.SelectSingleNode("struct/member[name = \"call\"]/value/struct/member[name = \"disconnectReason\"]/value/string") != null)
                            {
                                cdrEvent.sDisconnectReason = oNode.SelectSingleNode("struct/member[name = \"call\"]/value/struct/member[name = \"disconnectReason\"]/value/string").InnerText.ToString().Trim();
                                cdrEvent.sMessage += " disconnectReason: " + cdrEvent.sDisconnectReason;
                            }

                            if (oNode.SelectSingleNode("struct/member[name = \"call\"]/value/struct/member[name = \"direction\"]/value/string") != null)
                            {
                                cdrEvent.sEndpointDirection = oNode.SelectSingleNode("struct/member[name = \"call\"]/value/struct/member[name = \"direction\"]/value/string").InnerText.ToString().Trim();
                                cdrEvent.sMessage += " direction: " + cdrEvent.sEndpointDirection;
                            }

                            if (oNode.SelectSingleNode("struct/member[name = \"call\"]/value/struct/member[name = \"protocol\"]/value/string") != null)
                            {
                                cdrEvent.sCallProtocol = oNode.SelectSingleNode("struct/member[name = \"call\"]/value/struct/member[name = \"protocol\"]/value/string").InnerText.ToString().Trim();
                                cdrEvent.sMessage += " protocol: " + cdrEvent.sCallProtocol;
                            }

                            if (oNode.SelectSingleNode("struct/member[name = \"call\"]/value/struct/member[name = \"mediaEncryptionStatus\"]/value/string") != null)
                            {
                                cdrEvent.sMediaEncryptionStatus = oNode.SelectSingleNode("struct/member[name = \"call\"]/value/struct/member[name = \"mediaEncryptionStatus\"]/value/string").InnerText.ToString().Trim();
                                cdrEvent.sMessage += " mediaEncryptionStatus: " + cdrEvent.sMediaEncryptionStatus;
                            }

                            if (oNode.SelectSingleNode("struct/member[name = \"mediaFromEndpoint\"]/value/struct/member[name = \"resolution\"]/value/string") != null)
                            {
                                cdrEvent.sMediaFromResolution = oNode.SelectSingleNode("struct/member[name = \"mediaFromEndpoint\"]/value/struct/member[name = \"resolution\"]/value/string").InnerText.ToString().Trim();
                                cdrEvent.sMessage += " resolution: " + cdrEvent.sMediaFromResolution;
                            }

                            if (oNode.SelectSingleNode("struct/member[name = \"mediaFromEndpoint\"]/value/struct/member[name = \"videoCodec\"]/value/string") != null)
                            {
                                cdrEvent.sMediaFromVideoCodec = oNode.SelectSingleNode("struct/member[name = \"mediaFromEndpoint\"]/value/struct/member[name = \"videoCodec\"]/value/string").InnerText.ToString().Trim();
                                cdrEvent.sMessage += " videoCodec: " + cdrEvent.sMediaFromVideoCodec;
                            }

                            if (oNode.SelectSingleNode("struct/member[name = \"mediaFromEndpoint\"]/value/struct/member[name = \"audioCodec\"]/value/string") != null)
                            {
                                cdrEvent.sMediaFromaudioCodec = oNode.SelectSingleNode("struct/member[name = \"mediaFromEndpoint\"]/value/struct/member[name = \"audioCodec\"]/value/string").InnerText.ToString().Trim();
                                cdrEvent.sMessage += " audioCodec: " + cdrEvent.sMediaFromaudioCodec;
                            }
                            if (oNode.SelectSingleNode("struct/member[name = \"mediaFromEndpoint\"]/value/struct/member[name = \"bandwidth\"]/value/int") != null)
                            {
                                int.TryParse(oNode.SelectSingleNode("struct/member[name = \"mediaFromEndpoint\"]/value/struct/member[name = \"bandwidth\"]/value/int").InnerText.ToString().Trim(), out cdrEvent.iMediaFrombandwidth);
                                cdrEvent.sMessage += " bandwidth: " + cdrEvent.iMediaFrombandwidth;
                            }

                            if (oNode.SelectSingleNode("struct/member[name = \"mediaToEndpoint\"]/value/struct/member[name = \"resolution\"]/value/string") != null)
                            {
                                cdrEvent.sMediaToresolution = oNode.SelectSingleNode("struct/member[name = \"mediaToEndpoint\"]/value/struct/member[name = \"resolution\"]/value/string").InnerText.ToString().Trim();
                                cdrEvent.sMessage += " resolution: " + cdrEvent.sMediaToresolution;
                            }

                            if (oNode.SelectSingleNode("struct/member[name = \"mediaToEndpoint\"]/value/struct/member[name = \"videoCodec\"]/value/string") != null)
                            {
                                cdrEvent.sMediaTovideoCodec = oNode.SelectSingleNode("struct/member[name = \"mediaToEndpoint\"]/value/struct/member[name = \"videoCodec\"]/value/string").InnerText.ToString().Trim();
                                cdrEvent.sMessage += " videoCodec: " + cdrEvent.sMediaTovideoCodec;
                            }

                            if (oNode.SelectSingleNode("struct/member[name = \"mediaToEndpoint\"]/value/struct/member[name = \"audioCodec\"]/value/string") != null)
                            {
                                cdrEvent.sMediaToaudioCodec = oNode.SelectSingleNode("struct/member[name = \"mediaToEndpoint\"]/value/struct/member[name = \"audioCodec\"]/value/string").InnerText.ToString().Trim();
                                cdrEvent.sMessage += " audioCodec: " + cdrEvent.sMediaToaudioCodec;
                            }
                            if (oNode.SelectSingleNode("struct/member[name = \"mediaToEndpoint\"]/value/struct/member[name = \"bandwidth\"]/value/int") != null)
                            {
                                int.TryParse(oNode.SelectSingleNode("struct/member[name = \"mediaToEndpoint\"]/value/struct/member[name = \"bandwidth\"]/value/int").InnerText.ToString().Trim(), out cdrEvent.iMediaTobandwidth);
                                cdrEvent.sMessage += " bandwidth: " + cdrEvent.iMediaTobandwidth;
                            }
                        }
                        //7
                        if (cdrEvent.sEventType == "conferenceFinished")
                        {
                            if (oNode.SelectSingleNode("struct/member[name = \"limits\"]/value/struct/member[name = \"audioVideoParticipants\"]/value/int") != null)
                            {
                                int.TryParse(oNode.SelectSingleNode("struct/member[name = \"limits\"]/value/struct/member[name = \"audioVideoParticipants\"]/value/int").InnerText.ToString().Trim(), out cdrEvent.iAudioVideoParticipants);
                                cdrEvent.sMessage += " audioVideoParticipants: " + cdrEvent.iAudioVideoParticipants;
                            }

                            if (oNode.SelectSingleNode("struct/member[name = \"limits\"]/value/struct/member[name = \"audioOnlyParticipants\"]/value/int") != null)
                            {
                                int.TryParse(oNode.SelectSingleNode("struct/member[name = \"limits\"]/value/struct/member[name = \"audioOnlyParticipants\"]/value/int").InnerText.ToString().Trim(), out cdrEvent.iAudioOnlyParticipants);
                                cdrEvent.sMessage += " audioOnlyParticipants: " + cdrEvent.iAudioOnlyParticipants;
                            }

                            if (oNode.SelectSingleNode("struct/member[name = \"limits\"]/value/struct/member[name = \"streamingParticipantsAllowed\"]/value/int") != null)
                            {
                                int.TryParse(oNode.SelectSingleNode("struct/member[name = \"limits\"]/value/struct/member[name = \"streamingParticipantsAllowed\"]/value/int").InnerText.ToString().Trim(), out cdrEvent.istreamingParticipantsAllowed);
                                cdrEvent.sMessage += " streamingParticipantsAllowed: " + cdrEvent.istreamingParticipantsAllowed;
                            }

                            if (oNode.SelectSingleNode("struct/member[name = \"participants\"]/value/struct/member[name = \"maxSimultaneousAudioVideo\"]/value/int") != null)
                            {
                                int.TryParse(oNode.SelectSingleNode("struct/member[name = \"participants\"]/value/struct/member[name = \"maxSimultaneousAudioVideo\"]/value/int").InnerText.ToString().Trim(), out cdrEvent.iMaxSimultaneousAudioVideoParticipants);
                                cdrEvent.sMessage += " maxSimultaneousAudioVideo: " + cdrEvent.iMaxSimultaneousAudioVideoParticipants;
                            }

                            if (oNode.SelectSingleNode("struct/member[name = \"participants\"]/value/struct/member[name = \"maxSimultaneousAudioOnly\"]/value/int") != null)
                            {
                                int.TryParse(oNode.SelectSingleNode("struct/member[name = \"participants\"]/value/struct/member[name = \"maxSimultaneousAudioOnly\"]/value/int").InnerText.ToString().Trim(), out cdrEvent.iMaxSimultaneousAudioOnlyParticipants);
                                cdrEvent.sMessage += " maxSimultaneousAudioOnly: " + cdrEvent.iMaxSimultaneousAudioOnlyParticipants;
                            }

                            if (oNode.SelectSingleNode("struct/member[name = \"participants\"]/value/struct/member[name = \"maxSimultaneousStreaming\"]/value/int") != null)
                            {
                                int.TryParse(oNode.SelectSingleNode("struct/member[name = \"participants\"]/value/struct/member[name = \"maxSimultaneousStreaming\"]/value/int").InnerText.ToString().Trim(), out cdrEvent.imaxSimultaneousStreaming);
                                cdrEvent.sMessage += " maxSimultaneousStreaming: " + cdrEvent.imaxSimultaneousStreaming;
                            }

                            if (oNode.SelectSingleNode("struct/member[name = \"participants\"]/value/struct/member[name = \"totalAudioVideo\"]/value/int") != null)
                            {
                                int.TryParse(oNode.SelectSingleNode("struct/member[name = \"participants\"]/value/struct/member[name = \"totalAudioVideo\"]/value/int").InnerText.ToString().Trim(), out cdrEvent.iTotalAudioVideoParticipants);
                                cdrEvent.sMessage += " totalAudioVideo: " + cdrEvent.iTotalAudioVideoParticipants;
                            }

                            if (oNode.SelectSingleNode("struct/member[name = \"participants\"]/value/struct/member[name = \"totalAudioOnly\"]/value/int") != null)
                            {
                                int.TryParse(oNode.SelectSingleNode("struct/member[name = \"participants\"]/value/struct/member[name = \"totalAudioOnly\"]/value/int").InnerText.ToString().Trim(), out cdrEvent.iTotalAudioOnlyParticipants);
                                cdrEvent.sMessage += " totalAudioOnly: " + cdrEvent.iTotalAudioOnlyParticipants;
                            }

                            if (oNode.SelectSingleNode("struct/member[name = \"participants\"]/value/struct/member[name = \"totalStreaming\"]/value/int") != null)
                            {
                                int.TryParse(oNode.SelectSingleNode("struct/member[name = \"participants\"]/value/struct/member[name = \"totalStreaming\"]/value/int").InnerText.ToString().Trim(), out cdrEvent.iTotalstreamingParticipantsAllowed);
                                cdrEvent.sMessage += " totalStreaming: " + cdrEvent.iTotalstreamingParticipantsAllowed;
                            }

                            if (oNode.SelectSingleNode("struct/member[name = \"gatekeeper\"]/value/struct/member[name = \"registeredWithGatekeeper\"]/value/string") != null)
                            {
                                cdrEvent.sregisteredWithGatekeeper = oNode.SelectSingleNode("struct/member[name = \"gatekeeper\"]/value/struct/member[name = \"registeredWithGatekeeper\"]/value/string").InnerText.ToString().Trim();
                                cdrEvent.sMessage += " audioCodec: " + cdrEvent.sregisteredWithGatekeeper;
                            }

                            if (oNode.SelectSingleNode("struct/member[name = \"end\"]/value/struct/member[name = \"duration\"]/value/int") != null)
                            {
                                int.TryParse(oNode.SelectSingleNode("struct/member[name = \"end\"]/value/struct/member[name = \"duration\"]/value/int").InnerText.ToString().Trim(), out cdrEvent.iduration);
                                cdrEvent.sMessage += " duration: " + cdrEvent.iduration;
                            }

                            if (oNode.SelectSingleNode("struct/member[name = \"end\"]/value/struct/member[name = \"durationInMinutes\"]/value/int") != null)
                            {
                                int.TryParse(oNode.SelectSingleNode("struct/member[name = \"end\"]/value/struct/member[name = \"durationInMinutes\"]/value/int").InnerText.ToString().Trim(), out cdrEvent.idurationInMinutes);
                                cdrEvent.sMessage += " durationInMinutes: " + cdrEvent.idurationInMinutes;
                            }

                        }
                        //8
                        if (cdrEvent.sEventType == "ad-hocconferencestarted")
                        {
                            if (oNode.SelectSingleNode("struct/member[name = \"creator\"]/value/struct/member[name = \"participantid\"]/value/string") != null)
                            {
                                int.TryParse(oNode.SelectSingleNode("struct/member[name = \"creator\"]/value/struct/member[name = \"participantid\"]/value/string").InnerText.ToString().Trim(), out cdrEvent.iparticipantId);
                                cdrEvent.sMessage += " participantid: " + cdrEvent.iparticipantId;
                            }
                        }


                        if (listedConference == null)
                            listedConference = new Dictionary<string, NS_MESSENGER.Conference>();

                        foundConference = listedConference.Where(l => l.Key == cdrEvent.iConfnumname.ToString().Trim()).FirstOrDefault();
                        if (foundConference.Value != null)
                            cdrEvent.confDetails = foundConference.Value;
                        else
                        {
                            cdrEvent.confDetails = new NS_MESSENGER.Conference();
                            cdrEvent.confDetails.iDbNumName = cdrEvent.iConfnumname;
                            db.FetchConfforCallDetailRecord(ref cdrEvent.confDetails, "", MCU);
                            if (cdrEvent.confDetails.iDbID > 0)
                                listedConference.Add(cdrEvent.iConfnumname.ToString().Trim(), cdrEvent.confDetails);
                        }

                        if (cdrEventLists == null)
                            cdrEventLists = new List<NS_MESSENGER.CDREvent>();

                        cdrEventLists.Add(cdrEvent);

                    }
                }

                oNodeList = xd.SelectNodes("//param/value/struct/member");
                eventsRemainin = 0;
                for (ndeCnt = 0; ndeCnt < oNodeList.Count; ndeCnt++)
                {
                    oNode = oNodeList[ndeCnt];

                    if (oNode.SelectSingleNode("//member[name = \"nextIndex\"]/value/int") != null)
                        int.TryParse(oNode.SelectSingleNode("//member[name = \"nextIndex\"]/value/int").InnerText, out index);
                    logger.Trace("nextIndex: " + index);

                    if (oNode.SelectSingleNode("//member[name = \"eventsRemaining\"]/value/boolean") != null)
                        int.TryParse(oNode.SelectSingleNode("//member[name = \"eventsRemaining\"]/value/boolean").InnerText, out eventsRemainin);
                }
				//ZD 101348
                if (eventsRemainin > 0 && CDRCount <= 500)
                    goto GetCDR;

                if (cdrEventLists != null)
                {
                    if (!ProcessandInsertCDR(ref cdrEventLists, ref MCU, ref listedConference, ref ConfNumnamelist))
                    {
                        logger.Trace("Insert into CDR table Process is failed in Codian MSE.");
                    }
                }
                else
                {
                    logger.Trace("No Conference to add in CDR Tables in Codian MSE");
                }

                //ZD 101348
                if (eventsRemainin > 0)
                {
                    CDRCount = 0;
                    goto GetCDR;
                }


				//ZD 101348 - Commented as new table is created.
                //if (!ProcessandInsertVMRCDR(ref MCU))
                //{
                //    logger.Exception(100, "Insert into VMR CDR table Process is failed.");
                //    //return false;
                //}
            }
            catch (Exception ex)
            {
                logger.Trace("CDRforCodianMSESeries failed: " + ex.Message);
                return false;
            }
            return true;
        }
        #endregion

        //FB 2797 End

        //FB 2683 Start

        #region CDRforPolycomRMX
        /// <summary>
        /// RMX CDR
        /// </summary>
        /// <param name="MCU"></param>
        /// <returns></returns>
        public bool CDRforPolycomRMX(NS_MESSENGER.MCU MCU)
        {
            bool ret = false;
            string responseXML = "";
            XDocument xdoc;
            DataSet ds;
            List<NS_MESSENGER.CDREvent> CDREventlist = null;
            try
            {
                ret = false; ds = new DataSet();
                ret = db.selectMCUGUID(ref ds, MCU);
                if (!ret)
                {
                    logger.Trace("Error in fetching the PolycomRMXGUID");
                    return false;
                }

                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    xdoc = new XDocument();

                    NS_POLYCOM.Polycom PolycomRMX = new NS_POLYCOM.Polycom(configParams);

                    ret = false;
                    ret = PolycomRMX.PolycomRMXCDR(MCU, ds.Tables[0].Rows[i]["confuidonmcu"].ToString(), ref responseXML);
                    if (!ret)
                    {
                        logger.Trace("Error in Fetching the CDR Data");
                        continue;
                    }

                    logger.Trace(responseXML);
                    xdoc = XDocument.Parse(responseXML);

                    CDREventlist = new List<NS_MESSENGER.CDREvent>();

                    CDREventlist = (from time in xdoc.Descendants("CDR_SUMMARY")
                                    select new NS_MESSENGER.CDREvent
                                        {
                                            sTime = (DateTime.Parse(time.Element("RESERVE_START_TIME").Value)).ToString()
                                        }).ToList();


                    ret = false;
                    ret = db.InsertCDRinMyvrm(ref CDREventlist, MCU, ref responseXML, null); //ALLDEV-773
                    if (!ret)
                    {
                        logger.Trace("Error in updating Raw Data");
                        continue; //FB 2944
                    }

                    List<NS_MESSENGER.MCU.CDRConference> Conf = (from conf in xdoc.Descendants("CDR_FULL")
                                                                 from confend in xdoc.Descendants("CDR_EVENT").Where(x => ((x.Elements("CONF_END").Any())))
                                                                 from confstart in xdoc.Descendants("CDR_EVENT").Where(x => x.Elements("CONF_START").Any())
                                                                 select new NS_MESSENGER.MCU.CDRConference
                                                                 {
                                                                     Confid = ds.Tables[0].Rows[i]["confid"].ToString(),
                                                                     Instanceid = ds.Tables[0].Rows[i]["instanceid"].ToString(),
                                                                     ConfNumName = ds.Tables[0].Rows[i]["confnumname"].ToString(),
                                                                     Orgid = ds.Tables[0].Rows[i]["orgid"].ToString(),
                                                                     McuID = ds.Tables[0].Rows[i]["bridgeid"].ToString(),
                                                                     McuName = ds.Tables[0].Rows[i]["bridgename"].ToString(),
                                                                     McuTypeID = ds.Tables[0].Rows[i]["bridgetypeid"].ToString(),
                                                                     McuType = ds.Tables[0].Rows[i]["BridgeType"].ToString(),
                                                                     McuAddress = ds.Tables[0].Rows[i]["bridgeipisdnaddress"].ToString(),
                                                                     ConfTimeZone = ds.Tables[0].Rows[i]["TimeZone"].ToString(),
                                                                     Host = ds.Tables[0].Rows[i]["Host"].ToString(),
                                                                     McuConfGuid = conf.Element("CDR_SUMMARY").Element("ID").Value,
                                                                     Title = conf.Element("CDR_SUMMARY").Element("DISPLAY_NAME").Value,
                                                                     ScheduledStart = ds.Tables[0].Rows[i]["scheduledstart"].ToString(),
                                                                     ScheduledEnd = ds.Tables[0].Rows[i]["Scheduledend"].ToString(),
                                                                     EventDate = confstart.Element("TIME_STAMP").Value, //FB 2593K
                                                                     Duration = Math.Round((DateTime.Parse(confend.Element("TIME_STAMP").Value) - DateTime.Parse(conf.Element("CDR_SUMMARY").Element("START_TIME").Value)).TotalMinutes).ToString()
                                                                 }).ToList();


                    if (Conf.Count <= 0) //FB 2944
                        continue;

                    ret = false;
                    ret = ProcessandInsertPolycomRMXCDR(Conf, responseXML, MCU);
                    if (!ret)
                    {
                        logger.Trace("ProcessandInsertPolycomRMXCDR Failed");
                        continue;
                        //return false;//FB 2944
                    }

                    ret = false;
                    ret = db.UpdateCDRFetchStatus(Conf[0].ConfNumName);
                    if (!ret)
                    {
                        logger.Trace("ProcessandInsertPolycomRMXCDR Failed");
                        //return false;//FB 2944
                    }

                }
                return true;
            }
            catch (Exception ex)
            {
                logger.Trace("CDRforPolycomRMX" + ex.Message);
                return false;
            }
        }
        #endregion

        #region CDRforPolycomRPRM
        /// <summary>
        /// CDRforPolycomRPRM
        /// </summary>
        /// <param name="MCU"></param>
        /// <param name="ServicePath"></param>
        /// <returns></returns>
        public bool CDRforPolycomRPRM(NS_MESSENGER.MCU MCU, string ServicePath)
        {
            bool ret = false;
            string DirectoryFile = "";
            DataSet RPRMCSVDataSet = new DataSet();
            DataSet GUIDDataSet = new DataSet();
            System.Data.DataTable CSVTable = new System.Data.DataTable();
            System.Data.DataTable ConfTable = new System.Data.DataTable();
            System.Data.DataTable CallTable = new System.Data.DataTable();
            System.Data.DataTable ConfDetailsTable = new System.Data.DataTable();
            DateTime LastModifiedTime = new DateTime();
            NS_MESSENGER.CDREvent.VMRCDR VMRCDR = new NS_MESSENGER.CDREvent.VMRCDR();//FB 2593
            try
            {
                ret = false;
                ret = db.selectMCUGUID(ref GUIDDataSet, MCU);
                if (!ret)
                {
                    logger.Trace("Error in fetching the PolycomGUID");
                    return false;
                }

                DirectoryFile = ServicePath;
                NS_POLYCOMRPRM.POLYCOMRPRM PolycomRPRM = new NS_POLYCOMRPRM.POLYCOMRPRM(configParams);
                ret = PolycomRPRM.PolycomRPRMCDR(MCU, ref DirectoryFile, ref LastModifiedTime);

                if (!ret)
                {
                    logger.Trace("Error in getting CDR from RPRM");
                    return false;
                }
                string[] excelFiles = Directory.GetFiles(DirectoryFile, "*.csv");
                for (int i = 0; i < excelFiles.Count(); i++)
                {
                    using (CachedCsvReader csv = new CachedCsvReader(new StreamReader(excelFiles[i]), true))
                    {

                        CSVTable.Load(csv);
                        if (System.IO.Path.GetFileName(excelFiles[i]).ToString().Contains("conference"))
                            CSVTable.TableName = "Conference";
                        else
                            CSVTable.TableName = "Calls";
                        RPRMCSVDataSet.Tables.Add(CSVTable);
                        db.InsertRPRMCDRinMyvrm(CSVTable, excelFiles[i]);
                    }
                    CSVTable = new DataTable();

                }
                if (Directory.Exists(DirectoryFile))
                {
                    DirectoryInfo dir = new DirectoryInfo(DirectoryFile);
                    dir.Delete(true);
                }
                List<NS_MESSENGER.MCU.CDRConference> ConfList = null;
                List<NS_MESSENGER.MCU.CDRParty> ConfParty = null;

                List<string> PartyGUIDList = (RPRMCSVDataSet.Tables["Calls"].Rows.OfType<DataRow>().Select(dr => dr["CallUuid"].ToString()).ToList());

                ret = false; DataSet ds = new DataSet();
                ret = db.GetConferenceIDfromParty(PartyGUIDList, ref ds);
                if (!ret)
                {
                    logger.Trace("Error in GetConferenceIDfromParty");
                    return false;
                }

                for (int j = 0; j < RPRMCSVDataSet.Tables["Conference"].Rows.Count; j++)
                {
                    RPRMCSVDataSet.Tables["Conference"].DefaultView.RowFilter = "ConfUuid='" + RPRMCSVDataSet.Tables["Conference"].Rows[j]["confuuid"] + "'";
                    ConfTable = RPRMCSVDataSet.Tables["Conference"].DefaultView.ToTable();
                    RPRMCSVDataSet.Tables["Calls"].DefaultView.RowFilter = "refConfUUid='" + RPRMCSVDataSet.Tables["Conference"].Rows[j]["confuuid"] + "'";
                    CallTable = RPRMCSVDataSet.Tables["Calls"].DefaultView.ToTable();
                    GUIDDataSet.Tables[0].DefaultView.RowFilter = "Guid='" + RPRMCSVDataSet.Tables["Conference"].Rows[j]["confuuid"] + "'";
                    ConfDetailsTable = GUIDDataSet.Tables[0].DefaultView.ToTable();

                    if (ConfDetailsTable.Rows.Count > 0)
                    {
                        ret = false;
                        ConfList = new List<NS_MESSENGER.MCU.CDRConference>();
                        ConfList = ConfDetailsTable.Rows.OfType<DataRow>().Select(dr => new NS_MESSENGER.MCU.CDRConference
                        {
                            Confid = dr["confid"].ToString(),
                            Instanceid = dr["instanceid"].ToString(),
                            Orgid = dr["orgid"].ToString(),
                            ConfNumName = dr["confnumname"].ToString(),
                            McuID = dr["bridgeid"].ToString(),
                            McuName = dr["bridgename"].ToString(),
                            McuTypeID = dr["bridgetypeid"].ToString(),
                            McuType = dr["BridgeType"].ToString(),
                            McuAddress = dr["bridgeipisdnaddress"].ToString(),
                            Host = dr["Host"].ToString(),
                            ConfTimeZone = dr["TimeZone"].ToString(),
                            Title = dr["Title"].ToString(),
                            ScheduledStart = dr["scheduledstart"].ToString(),
                            ScheduledEnd = dr["Scheduledend"].ToString(),
                            EventDate = dr["scheduledstart"].ToString()
                        }).ToList();

                        ConfList[0].McuConfGuid = ConfTable.Rows[0]["confUUID"].ToString();
                        ConfList[0].Duration = Math.Round(((DateTime.Parse(ConfTable.Rows[0]["endTime"].ToString()) - DateTime.Parse(ConfTable.Rows[0]["startTime"].ToString())).TotalMinutes)).ToString();

                        ret = false;
                        ret = db.InsertCDRConference(ConfList[0]);
                        if (!ret)
                        {
                            logger.Trace("Error in InsertCDRConference-RPRM");
                            return false;
                        }
                        if (ds.Tables[0].Rows.Count > 0) //FB 2593
                        {
                            for (int k = 0; k < CallTable.Rows.Count; k++)
                            {
                                for (int m = 0; m < 2; m++)
                                {
                                    ConfParty = new List<NS_MESSENGER.MCU.CDRParty>();
                                    ConfParty = ConfDetailsTable.Rows.OfType<DataRow>().Select(dr => new NS_MESSENGER.MCU.CDRParty
                                    {
                                        confid = dr["confid"].ToString(),
                                        InstanceId = dr["instanceid"].ToString(),
                                        OrgID = dr["orgid"].ToString(),
                                        ConfNumName = dr["confnumname"].ToString(),
                                        ConfTimeZone = dr["TimeZone"].ToString()
                                    }).ToList();
                                    ConfParty[0].ConfGUID = ConfList[0].McuConfGuid.ToString();
                                    ConfParty[0].partyGuid = CallTable.Rows[k]["callUuid"].ToString();
                                    ds.Tables[0].DefaultView.RowFilter = "GUID='" + ConfParty[0].partyGuid + "'";
                                    ds.Tables[0].DefaultView.RowFilter = "GUID='" + ConfParty[0].partyGuid + "' and confid=" + ConfParty[0].confid + " and instanceid=" + ConfParty[0].InstanceId;
                                    if (ds.Tables[0].DefaultView.ToTable().Rows.Count > 0)
                                    {
                                        ConfParty[0].EndpointName = ds.Tables[0].DefaultView.ToTable().Rows[0]["Partyname"].ToString();
                                        ConfParty[0].EndpointAddress = CallTable.Rows[k]["destEndpoint"].ToString();
                                        ConfParty[0].EndpointType = ds.Tables[0].DefaultView.ToTable().Rows[0]["TerminalType"].ToString();
                                        ConfParty[0].ConnectionType = CallTable.Rows[k]["dialIn"].ToString() == "TRUE" ? "1" : "2";
                                        if (m == 1)
                                        {
                                            ConfParty[0].EndpointDateTime = CallTable.Rows[k]["startTime"].ToString();
                                            ConfParty[0].EndpointConnectStatus = "1";
                                            ConfParty[0].DisconnectReason = "";
                                        }
                                        else
                                        {
                                            ConfParty[0].EndpointDateTime = CallTable.Rows[k]["endTime"].ToString();
                                            ConfParty[0].EndpointConnectStatus = "2";
                                            ConfParty[0].DisconnectReason = CallTable.Rows[k]["cause"].ToString();
                                        }

                                        int index = ConfParty[0].EndpointDateTime.IndexOf('+');
                                        ConfParty[0].EndpointDateTime = ConfParty[0].EndpointDateTime.Remove(index);
                                        ret = false;
                                        ret = db.InsertCDRParty(ConfParty[0]);
                                        if (!ret)
                                        {
                                            logger.Trace("Error in InsertCDR Party-RPRM");
                                        }
                                    }
                                }
                            }
                        }
                        ret = false;
                        ret = db.UpdateCDRFetchStatus(ConfList[0].ConfNumName);
                        if (!ret)
                        {
                            logger.Trace("ProcessandInsertPolycomRMXCDR Failed-RPRM");
                            return false;
                        }

                    }


                }
                ret = false;
                ret = db.UpdateLastModifiedDateTime(MCU);
                if (!ret)
                {
                    logger.Trace("error in updating LastModified DateTime-RPRM");
                    return false;
                }
                //2593 RPRM Starts
                if (RPRMCSVDataSet.Tables["Calls"] != null)
                {
                    System.Data.DataTable FilteredcallTable = new System.Data.DataTable();
                    System.Data.DataTable FilteredconfTable = new System.Data.DataTable();

                    RPRMCSVDataSet.Tables["Calls"].DefaultView.RowFilter = "calltype='VMR' and dialIn ='TRUE'";

                    FilteredcallTable = RPRMCSVDataSet.Tables["Calls"].DefaultView.ToTable();
                    var ExistinglistArray = FilteredcallTable.Rows.OfType<DataRow>().Select(dr => dr["refConfUUID"].ToString()).ToArray();
                    var ExistcommaSeperated = string.Join("','", ExistinglistArray);
                    RPRMCSVDataSet.Tables["Conference"].DefaultView.RowFilter = "confUUID in ('" + ExistcommaSeperated + "')";
                    FilteredconfTable = RPRMCSVDataSet.Tables["Conference"].DefaultView.ToTable();

                    for (int i = 0; i < FilteredconfTable.Rows.Count; i++)
                    {
                        int TotalPortActual = 0, TotalMinActual = 0, mod = 0, totalsec = 0;

                        FilteredcallTable.DefaultView.RowFilter = "refconfuuid='" + FilteredconfTable.Rows[i]["confuuid"] + "'";
                        for (int k = 0; k < FilteredcallTable.DefaultView.ToTable().Rows.Count; k++)
                        {
                            totalsec += int.Parse(Math.Round((DateTime.Parse(FilteredcallTable.DefaultView.ToTable().Rows[k]["endTime"].ToString()) - DateTime.Parse(FilteredcallTable.DefaultView.ToTable().Rows[k]["startTime"].ToString())).TotalSeconds).ToString());
                        }

                        TotalPortActual = totalsec / 60;
                        mod = totalsec % 60;

                        if (mod >= 30)
                            TotalPortActual++;

                        totalsec = int.Parse(Math.Round((DateTime.Parse(FilteredconfTable.Rows[i]["endTime"].ToString()) - DateTime.Parse(FilteredconfTable.Rows[i]["startTime"].ToString())).TotalSeconds).ToString());

                        TotalMinActual = totalsec / 60;
                        mod = totalsec % 60;

                        if (mod >= 30)
                            TotalMinActual++;

                        VMRCDR.TotalMinActual = TotalMinActual.ToString();
                        VMRCDR.TotalPortMinActual = TotalPortActual.ToString();
                        VMRCDR.MCUAddress = MCU.sIp;
                        VMRCDR.MCUid = MCU.iDbId.ToString();
                        VMRCDR.MCUname = MCU.sName;
                        VMRCDR.MCUtype = MCU.sMcuType.ToString();
                        VMRCDR.OrgId = MCU.iOrgId.ToString();
                        VMRCDR.BridgeTypeID = MCU.sMcuType.ToString();
                        VMRCDR.VMRAddress = FilteredconfTable.Rows[i]["roomID"].ToString();

                        VMRCDR.MCUConfGUID = FilteredconfTable.Rows[i]["confuuid"].ToString().Trim();
                        VMRCDR.StartDate = (DateTime.Parse(FilteredconfTable.Rows[i]["startTime"].ToString())).ToString();

                        ret = false;
                        ret = db.InsertVMRConference(VMRCDR);
                        if (!ret)
                        {
                            logger.Trace("Failed to insert VMR-RPRM");
                            return false;
                        }
                    }

                }
                //2593 RPRM Ends

                return true;
            }
            catch (Exception ex)
            {
                logger.Trace("CDRforPolycomRPRM" + ex.Message);
                return false;
            }
        }
        #endregion

        //FB 2683 End

        #region ProcessandInsertCDR
        /// <summary>
        /// ProcessandInsertCDR
        /// </summary>
        /// <param name="cdrEventLists"></param>
        /// <param name="evntMCU"></param>
        /// <param name="listedConference"></param>
        /// <param name="ConfGUIDlist"></param>
        /// <param name="partyGUIDlist"></param>
        /// <returns></returns>
        public bool ProcessandInsertCDR(ref List<NS_MESSENGER.CDREvent> cdrEventLists, ref NS_MESSENGER.MCU evntMCU, ref Dictionary<string, NS_MESSENGER.Conference> listedConference, ref List<string> ConfGUIDlist)
        {
            bool ret = false;
            string receivexml = "";
            try
            {
                ret = db.InsertCDRinMyvrm(ref cdrEventLists, evntMCU, ref receivexml, null);//FB 2683 ALLDEV-773
                if (!ret)
                {
                    logger.Exception(100, "InsertCDRinMyvrm Method is failed.");
                    return false;
                }

                ret = false;
                ret = ProcessandInsertConferenceCDR(listedConference, evntMCU, ConfGUIDlist);
                if (!ret)
                {
                    logger.Exception(100, "InsertCDRinMyvrm Method is failed.");
                    return false;
                }

            }
            catch (Exception ex)
            {
                logger.Trace("ProcessandInsertCDR failed: " + ex.Message);
                return false;
            }
            return true;
        }
        #endregion

        #region ProcessandInsertVMRCDR
        /// <summary>
        /// ProcessandInsertVMRCDR
        /// </summary>
        /// <param name="MCU"></param>
        /// <returns></returns>
        public bool ProcessandInsertVMRCDR(ref NS_MESSENGER.MCU MCU)
        {
            bool ret = false;
            List<string> GUIDlist = new List<string>();
            NS_MESSENGER.CDREvent.VMRCDR VMRCDR = new NS_MESSENGER.CDREvent.VMRCDR();
            int TotalPortMinActual = 0, TotalSec = 0;
            bool AlreadyExist = false;
            DateTime confStartDate = DateTime.UtcNow;//ZD 101348
            try
            {
                #region VMR Conference

                GUIDlist = new List<string>();
                ret = db.FetchVMRConferenceUniqueid(ref GUIDlist, MCU);
                if (!ret)
                {
                    logger.Trace("FetchVMRConferenceGUID Method is failed.");
                    return false;
                }

                if (GUIDlist.Count > 0)
                {
                    for (int v = 0; v < GUIDlist.Count; v++)
                    {
                        string id = GUIDlist[v];
                        confStartDate = DateTime.UtcNow;//ZD 101348
                        TotalPortMinActual = 0;
                        AlreadyExist = false;
                        ret = db.FetchConferenceCDREvent(id, ref VMRCDR, MCU, ref TotalPortMinActual, 2, ref AlreadyExist, ref confStartDate); //1= Normal,2=VMR//ZD 101348
                        if (!ret)
                        {
                            logger.Exception(100, "FetchVMRConference Method is failed.");
                            continue;
                        }

                        if (string.IsNullOrEmpty(VMRCDR.MCUConfGUID))
                            continue;

                        int TotalPortActual = 0;
                        int mod = 0;

                        TotalPortActual = TotalPortMinActual / 60;
                        mod = TotalPortMinActual % 60;

                        if (mod >= 30)
                            TotalPortActual++;

                        VMRCDR.TotalPortMinActual = TotalPortActual.ToString();

                        int.TryParse(VMRCDR.TotalMinActual, out TotalSec);

                        if (MCU.etType == NS_MESSENGER.MCU.eType.CISCOTP)
                        {
                            TotalPortActual = TotalSec / 60;
                            mod = TotalSec % 60;

                            if (mod >= 30)
                                TotalPortActual++;

                            VMRCDR.TotalMinActual = TotalPortActual.ToString();
                        }
                        else
                        {
                            VMRCDR.TotalMinActual = TotalSec.ToString();
                        }

                        VMRCDR.MCUAddress = MCU.sIp;
                        VMRCDR.MCUid = MCU.iDbId.ToString();
                        VMRCDR.MCUname = MCU.sName;
                        VMRCDR.MCUtype = MCU.sMcuType.ToString();
                        VMRCDR.OrgId = MCU.iOrgId.ToString();

                        if (AlreadyExist)
                            ret = db.UpdateVMRConference(VMRCDR);
                        else
                            ret = db.InsertVMRConference(VMRCDR);

                        if (!ret)
                        {
                            logger.Exception(100, "InsertVMRConference Method is failed.");
                            return false;
                        }
                    }
                }
                #endregion

            }
            catch (Exception ex)
            {
                logger.Trace("ProcessandInsertVMRCDR" + ex.Message);
                return false;
            }
            return true;
        }
        #endregion

        #region ProcessandInsertConferenceCDR
        /// <summary>
        /// Process and Insert Conference CDR
        /// </summary>
        /// <param name="PolycomRMXCDRLst"></param>
        /// <param name="mcu"></param>
        /// <returns></returns>
        public bool ProcessandInsertConferenceCDR(Dictionary<string, NS_MESSENGER.Conference> listedConference, NS_MESSENGER.MCU MCU, List<string> ConfGuid)
        {
            DataSet ds = new DataSet();
            NS_MESSENGER.Conference conf = new NS_MESSENGER.Conference();
            NS_MESSENGER.MCU.CDRConference ConfCDR = new NS_MESSENGER.MCU.CDRConference();
            NS_MESSENGER.CDREvent.VMRCDR VMRCDR = new NS_MESSENGER.CDREvent.VMRCDR();
            string GUID = "";
            bool ret = false, AlreadyExist = false;
            int totaltime = 0, TotalPortActual = 0, mod = 0, TotalSec = 0;
            DateTime confStartDate = DateTime.UtcNow;//ZD 101348
            try
            {
                for (int i = 0; i < ConfGuid.Count; i++)
                {
                    ConfCDR = new NS_MESSENGER.MCU.CDRConference();
                    VMRCDR = new NS_MESSENGER.CDREvent.VMRCDR();

                    GUID = ConfGuid[i];

                    if (listedConference.ContainsKey(GUID))
                    {
                        conf = listedConference[GUID];
						//ZD 101348
                        confStartDate = DateTime.UtcNow;
                        ret = db.FetchConferenceCDREvent(GUID, ref VMRCDR, MCU, ref totaltime, conf.iPermanent, ref AlreadyExist, ref confStartDate);//1=Permanent VMR, 0=Normal //ZD 101348
                        if (!ret)
                        {
                            logger.Trace("Failed to insert CDR Conference");
                            continue;
                        }
                        else
                        {
                            TotalPortActual = 0; mod = 0; TotalSec = 0;

                            ConfCDR.Confid = conf.iDbID.ToString();
                            ConfCDR.Instanceid = conf.iInstanceID.ToString();
                            ConfCDR.ConfNumName = conf.iDbNumName.ToString();
                            ConfCDR.ConfTimeZone = conf.iTimezoneID.ToString();

                            int.TryParse(VMRCDR.TotalMinActual, out TotalSec);

                            if (MCU.etType == NS_MESSENGER.MCU.eType.CISCOTP)
                            {
                                TotalPortActual = TotalSec / 60;
                                mod = TotalSec % 60;

                                if (mod >= 30)
                                    TotalPortActual++;

                                ConfCDR.Duration = TotalPortActual.ToString();
                            }
                            else
                            {
                                ConfCDR.Duration = TotalSec.ToString();
                            }
                            ConfCDR.Host = conf.sHostName;
                            ConfCDR.McuAddress = MCU.sIp;
                            ConfCDR.McuConfGuid = VMRCDR.VMRAddress;//NumericId
                            ConfCDR.McuID = MCU.iDbId.ToString();
                            ConfCDR.McuName = MCU.sName;
                            ConfCDR.McuType = MCU.etType.ToString();
                            ConfCDR.McuTypeID = MCU.sMcuType.ToString();
                            ConfCDR.Orgid = conf.iOrgID.ToString();
							//ZD 101348 Starts
                            if (conf.iPermanent == 1)
                            {
                                int confDuration = 0;
                                int.TryParse(ConfCDR.Duration, out confDuration);
                                ConfCDR.ScheduledStart = confStartDate.ToString();
                                ConfCDR.ScheduledEnd = confStartDate.AddMinutes(confDuration).ToString();
                            }
                            else
                            {
                                ConfCDR.ScheduledStart = conf.dtStartDateTime.ToString();
                                ConfCDR.ScheduledEnd = conf.dtEndDateTime.ToString();
                            }
							//ZD 101348 End	
                            ConfCDR.Title = conf.sExternalName;
                            ConfCDR.EventId = VMRCDR.EventId;
                            ConfCDR.ConfGUID = VMRCDR.MCUConfGUID;//FB 2797 //UniqueId
                            ConfCDR.EventDate = VMRCDR.StartDate;//FB 2797
                            ConfCDR.IsVMRConf = conf.iIsVMR.ToString();//ZD 101348
                            ConfCDR.IsPermanentConf = conf.iPermanent.ToString();//ZD 101348

                            ret = db.InsertCDRConference(ConfCDR);
                            if (!ret)
                            {
                                logger.Trace("Failed to insert CDR Conference");
                                return false;
                            }

                            ret = false;

                            if (MCU.etType == NS_MESSENGER.MCU.eType.CISCOTP)
                                ret = ProcessandInsertParticipant8710CDR(conf, MCU);

                            else if (MCU.etType == NS_MESSENGER.MCU.eType.CODIAN)
                                ret = ProcessandInsertParticipant8000SeriesCDR(conf, MCU, VMRCDR.MCUConfGUID); //VMRCDR.MCUConfGUID = Unique Id

                            if (!ret)
                            {
                                logger.Exception(100, "InsertCDRinMyvrm Method is failed.");
                                return false;
                            }
                        }
                    }
                }
                return true;
            }
            catch (Exception ex)
            {
                logger.Trace("ProcessandInsertConferenceCDR failed: " + ex.Message);
                return false;
            }
        }
        #endregion

        #region ProcessandInsertParticipant8710CDR
        /// <summary>
        /// Process and Insert Participant 8710 CDR
        /// </summary>
        /// <param name="PolycomRMXCDRLst"></param>
        /// <param name="mcu"></param>
        /// <returns></returns>
        public bool ProcessandInsertParticipant8710CDR(NS_MESSENGER.Conference Conference, NS_MESSENGER.MCU MCU)
        {
            DataSet ds = new DataSet();
            DataSet ds1 = new DataSet();
            NS_MESSENGER.MCU.CDRParty partyCDR = new NS_MESSENGER.MCU.CDRParty();
            string EventType = "", dsTable1 = "CDR";
            bool ret = false;
            try
            {
                ret = db.GetPartyDetails(Conference.iDbID, Conference.iInstanceID, Conference.iDbNumName, ref ds, Conference.sGUID, Conference.iPermanent);//ZD 101348
                if (!ret)
                {
                    logger.Trace("Error in Fetch Participant Details");
                    return false;
                }
                for (int t1 = 0; t1 < ds.Tables.Count; t1++)
                {
                    DataTable colTable = ds.Tables[t1];

                    for (int i = 0; i < colTable.Rows.Count; i++)
                    {
                        partyCDR = new NS_MESSENGER.MCU.CDRParty();

                        partyCDR.partyGuid = colTable.Rows[i]["GUID"].ToString().Trim();

                        ds1 = new DataSet();
                        ret = db.FetchPartyCDREvent(partyCDR.partyGuid, 0, ref ds1, MCU);
                        if (!ret)
                        {
                            logger.Trace("Error in Fetch Participant Details for partyid = " + partyCDR.partyGuid + " and Confid =" + Conference.iDbNumName);
                            //return false;
                        }

                        for (int j = 0; j < ds1.Tables[dsTable1].Rows.Count; j++)
                        {
                            if (ds1.Tables[dsTable1].Rows[j]["EventType"] != null)
                            {
                                if (ds1.Tables[dsTable1].Rows[j]["EventType"].ToString() != "")
                                {
                                    EventType = ds1.Tables[dsTable1].Rows[j]["EventType"].ToString();
                                }
                            }
                            if (ds1.Tables[dsTable1].Rows[j]["CDRIndex"] != null)
                            {
                                if (ds1.Tables[dsTable1].Rows[j]["CDRIndex"].ToString() != "")
                                {
                                    partyCDR.EventId = ds1.Tables[dsTable1].Rows[j]["CDRIndex"].ToString();
                                }
                            }
                            if (EventType == "participantConnected" || EventType == "participantDisconnected")
                            {
                                if (EventType == "participantConnected")
                                {
                                    partyCDR.EndpointConnectStatus = Convert.ToInt16(NS_MESSENGER.CDREvent.EptConnectStatus.Connected).ToString();

                                    if (ds1.Tables[dsTable1].Rows[j]["EndpointIPAddress"] != null)
                                    {
                                        if (ds1.Tables[dsTable1].Rows[j]["EndpointIPAddress"].ToString() != "")
                                        {
                                            partyCDR.EndpointAddress = ds1.Tables[dsTable1].Rows[j]["EndpointIPAddress"].ToString();
                                        }
                                    }
                                    if (ds1.Tables[dsTable1].Rows[j]["time"] != null)
                                    {
                                        if (ds1.Tables[dsTable1].Rows[j]["time"].ToString() != "")
                                        {
                                            partyCDR.EndpointDateTime = ds1.Tables[dsTable1].Rows[j]["time"].ToString();
                                        }
                                    }
                                }

                                if (EventType == "participantDisconnected")
                                {
                                    if (ds1.Tables[dsTable1].Rows[j]["time"] != null)
                                    {
                                        if (ds1.Tables[dsTable1].Rows[j]["time"].ToString() != "")
                                        {
                                            partyCDR.EndpointDateTime = ds1.Tables[dsTable1].Rows[j]["time"].ToString();
                                        }
                                    }

                                    partyCDR.EndpointConnectStatus = Convert.ToInt16(NS_MESSENGER.CDREvent.EptConnectStatus.Disconnected).ToString();

                                    if (ds1.Tables[dsTable1].Rows[j]["DisconnectReason"] != null)
                                    {
                                        if (ds1.Tables[dsTable1].Rows[j]["DisconnectReason"].ToString() != "")
                                        {
                                            partyCDR.DisconnectReason = ds1.Tables[dsTable1].Rows[j]["DisconnectReason"].ToString();
                                        }
                                    }
                                }

                                partyCDR.ConfGUID = Conference.sGUID;
                                partyCDR.confid = Conference.iDbID.ToString();
                                partyCDR.ConfNumName = Conference.iDbNumName.ToString();
                                partyCDR.EndpointAddress = colTable.Rows[i]["ipisdnaddress"].ToString().Trim();
                                partyCDR.ConnectionType = colTable.Rows[i]["ConnectionType"].ToString().Trim();
                                partyCDR.EndpointName = colTable.Rows[i]["Partyname"].ToString().Trim();
                                partyCDR.EndpointType = colTable.Rows[i]["TerminalType"].ToString().Trim();
                                partyCDR.InstanceId = Conference.iInstanceID.ToString();
                                partyCDR.OrgID = Conference.iOrgID.ToString();
                                partyCDR.IsVMRConf = Conference.iIsVMR.ToString();//ZD 101348
                                partyCDR.IsPermanentConf = Conference.iPermanent.ToString();//ZD 101348

                                ret = db.InsertCDRParty(partyCDR);
                                if (!ret)
                                {
                                    logger.Exception(100, "InsertCDRinRMX Method is failed.");
                                }
                            }
                        }
                    }
                }
                return true;
            }
            catch (Exception ex)
            {
                logger.Trace("ProcessandInsertParticipant8710CDR failed: " + ex.Message);
                return false;
            }
        }
        #endregion

        //Need to discuss
        #region ProcessandInsertParticipant8000SeriesCDR
        /// <summary>
        /// Process and Insert Participant 8710 Series CDR
        /// </summary>
        /// <param name="PolycomRMXCDRLst"></param>
        /// <param name="mcu"></param>
        /// <returns></returns>
        public bool ProcessandInsertParticipant8000SeriesCDR(NS_MESSENGER.Conference Conference, NS_MESSENGER.MCU MCU, string ConfUID)
        {
            DataSet ds = new DataSet();
            DataSet ds1 = new DataSet();
            NS_MESSENGER.MCU.CDRParty partyCDR = new NS_MESSENGER.MCU.CDRParty();
            string EventType = "", dsTable1 = "CDR";
            bool ret = false;
            List<int> endpointid = new List<int>();
            try
            {
                ret = db.GetPartyDetails(Conference.iDbID, Conference.iInstanceID, Conference.iDbNumName, ref ds, Conference.sGUID, Conference.iPermanent);//ZD 101348
                if (!ret)
                {
                    logger.Trace("Error in Fetch Participant Details");
                    return false;
                }
                logger.Trace("ProcessandInsertParticipant8000SeriesCDRConfUID" + ConfUID);

                List<string> partyAddresses = null;//ZD 101348

                for (int t1 = 0; t1 < ds.Tables.Count; t1++)
                {
                    DataTable colTable = ds.Tables[t1];

                    for (int i = 0; i < colTable.Rows.Count; i++)
                    {
                        partyCDR = new NS_MESSENGER.MCU.CDRParty();

                        partyCDR.EndpointAddress = colTable.Rows[i]["ipisdnaddress"].ToString().Trim();
						//ZD 101348
                        if (!string.IsNullOrWhiteSpace(partyCDR.EndpointAddress))
                        {
                            if (partyAddresses == null)
                                partyAddresses = new List<string>();

                            if (partyAddresses.Contains(partyCDR.EndpointAddress))
                            {
                                logger.Trace("partyCDR.EndpointAddressDuplicate" + partyCDR.EndpointAddress);
                                continue;
                            }
                            else
                                partyAddresses.Add(partyCDR.EndpointAddress);
                        }

                        ds1 = new DataSet();
                        endpointid = new List<int>();//ZD 101348
                        ret = db.FetchPartyIdCDREvent(partyCDR.EndpointAddress, ConfUID, ref endpointid);
                        if (!ret)
                        {
                            logger.Trace("Error in Fetch Participant Details for ConfUID = " + ConfUID + " and Confid =" + Conference.iDbNumName);
                        }

                        for (int e = 0; e < endpointid.Count; e++)
                        {
                            ds1 = new DataSet();
                            int eptid = endpointid[e];
                            ret = db.FetchPartyCDREvent(ConfUID, eptid, ref ds1, MCU);
                            if (!ret)
                            {
                                logger.Trace("Error in Fetch Participant Details for ConfUID = " + ConfUID + " and Confid =" + Conference.iDbNumName);
                            }

                            for (int j = 0; j < ds1.Tables[dsTable1].Rows.Count; j++)
                            {
                                if (ds1.Tables[dsTable1].Rows[j]["EventType"] != null)
                                {
                                    if (ds1.Tables[dsTable1].Rows[j]["EventType"].ToString() != "")
                                    {
                                        EventType = ds1.Tables[dsTable1].Rows[j]["EventType"].ToString();
                                    }
                                }
                                //FB 2944 Starts
                                if (EventType == "participantJoined" || EventType == "participantLeft")
                                {
                                    if (ds1.Tables[dsTable1].Rows[j]["time"] != null)
                                    {
                                        if (ds1.Tables[dsTable1].Rows[j]["time"].ToString() != "")
                                        {
                                            partyCDR.EndpointDateTime = ds1.Tables[dsTable1].Rows[j]["time"].ToString();
                                        }
                                    }
                                    if (ds1.Tables[dsTable1].Rows[j]["CDRIndex"] != null)
                                    {
                                        if (ds1.Tables[dsTable1].Rows[j]["CDRIndex"].ToString() != "")
                                        {
                                            partyCDR.EventId = ds1.Tables[dsTable1].Rows[j]["CDRIndex"].ToString();
                                        }
                                    }
                                    if (ds1.Tables[dsTable1].Rows[j]["EndpointId"] != null)
                                    {
                                        if (ds1.Tables[dsTable1].Rows[j]["EndpointId"].ToString() != "")
                                        {
                                            partyCDR.partyGuid = ds1.Tables[dsTable1].Rows[j]["EndpointId"].ToString();
                                        }
                                    }
                                    if (EventType == "participantJoined")
                                    {
                                        partyCDR.EndpointConnectStatus = Convert.ToInt16(NS_MESSENGER.CDREvent.EptConnectStatus.Connected).ToString();
                                    }
                                	//FB 2944 End
                                    if (EventType == "participantLeft")
                                    {
                                        partyCDR.EndpointConnectStatus = Convert.ToInt16(NS_MESSENGER.CDREvent.EptConnectStatus.Disconnected).ToString();
										//ZD 101348 - Commented
                                        //if (ds1.Tables[dsTable1].Rows[j]["EndpointIPAddress"] != null)
                                        //{
                                        //    if (ds1.Tables[dsTable1].Rows[j]["EndpointIPAddress"].ToString() != "")
                                        //    {
                                        //        partyCDR.EndpointAddress = ds1.Tables[dsTable1].Rows[j]["EndpointIPAddress"].ToString();
                                        //    }
                                        //}

                                        if (ds1.Tables[dsTable1].Rows[j]["DisconnectReason"] != null)
                                        {
                                            if (ds1.Tables[dsTable1].Rows[j]["DisconnectReason"].ToString() != "")
                                            {
                                                partyCDR.DisconnectReason = ds1.Tables[dsTable1].Rows[j]["DisconnectReason"].ToString();
                                            }
                                        }
                                    }

                                    partyCDR.ConfGUID = Conference.sGUID;
                                    partyCDR.confid = Conference.iDbID.ToString();
                                    partyCDR.ConfNumName = Conference.iDbNumName.ToString();
                                    partyCDR.ConnectionType = colTable.Rows[i]["ConnectionType"].ToString().Trim();
                                    partyCDR.EndpointName = colTable.Rows[i]["Partyname"].ToString().Trim();
                                    partyCDR.EndpointType = colTable.Rows[i]["TerminalType"].ToString().Trim();
                                    partyCDR.EndpointType = "2";
                                    partyCDR.InstanceId = Conference.iInstanceID.ToString();
                                    partyCDR.OrgID = Conference.iOrgID.ToString();
                                    partyCDR.IsVMRConf = Conference.iIsVMR.ToString(); //ZD 101348
                                    partyCDR.IsPermanentConf = Conference.iPermanent.ToString();//ZD 101348
                                    logger.Trace("partyCDR.IsPermanentConf" + partyCDR.IsPermanentConf);
                                    ret = db.InsertCDRParty(partyCDR);
                                    if (!ret)
                                    {
                                        logger.Exception(100, "InsertCDRinPartyCodian8000 Method is failed.");
                                    }
                                }
                            }
                        }
                    }
                }
                return true;
            }
            catch (Exception ex)
            {
                logger.Trace("ProcessandInsertParticipant8000SeriesCDR failed" + ex.Message);
                return false;
            }
        }
        #endregion

        //FB 2683 Starts  

        #region ProcessandInsertPolycomRMXCDR
        /// <summary>
        /// Process RMX Fetch CDR
        /// </summary>
        /// <param name="PolycomRMXCDRLst"></param>
        /// <param name="mcu"></param>
        /// <returns></returns>
        public bool ProcessandInsertPolycomRMXCDR(List<NS_MESSENGER.MCU.CDRConference> Conf, string responseXML, NS_MESSENGER.MCU mcu)
        {
            bool ret = false;
            DataSet ds = new DataSet();
            try
            {
                ret = db.InsertCDRConference(Conf[0]);
                if (!ret)
                {
                    logger.Trace("Failed to insert CDR Conference");
                    return false;
                }

                XDocument xdoc = XDocument.Parse(responseXML);

                List<string> partyid = xdoc.Descendants("RESERVED_PARTY").Select(D => D.Element("ID").Value).ToList();
                List<string> oppartyadd = xdoc.Descendants("OPERATOR_ADD_PARTY").Select(D => D.Element("ID").Value).ToList();
                partyid.AddRange(oppartyadd);


                ret = db.GetConferenceIDfromParty(partyid, ref ds);
                if (!ret)
                {
                    logger.Trace("Error in Fetch Participant Details");
                    return false;
                }

                var PartyEvent = (from dd in xdoc.Descendants("CDR_EVENT").Where(D => D.Elements("PARTY_DISCONNECTED").Any() || D.Elements("H323_CALL_SETUP").Any()).OrderBy(D => DateTime.Parse(D.Element("TIME_STAMP").Value)) select dd).ToList();

                List<NS_MESSENGER.MCU.CDRParty> party = new List<NS_MESSENGER.MCU.CDRParty>();

                if (ds.Tables[0].Rows.Count > 0) //FB 2593
                {
                    for (int i = 0; i < PartyEvent.Count; i++)
                    {
                        party = (from par in PartyEvent[i].Descendants("PARTY_ID")
                                 select new NS_MESSENGER.MCU.CDRParty
                                 {
                                     confid = Conf[0].Confid,
                                     InstanceId = Conf[0].Instanceid,
                                     ConfNumName = Conf[0].ConfNumName,
                                     OrgID = Conf[0].Orgid,
                                     ConfGUID = Conf[0].McuConfGuid,
                                     partyGuid = par.Value,
                                     ConfTimeZone = Conf[0].ConfTimeZone
                                 }).ToList();


                        ds.Tables[0].DefaultView.RowFilter = "GUID='" + party[0].partyGuid + "' and confid=" + Conf[0].Confid + " and instanceid=" + Conf[0].Instanceid;
                        
                        if (ds.Tables[0].DefaultView.ToTable().Rows.Count > 0)
                        {
                            party[0].EndpointName = ds.Tables[0].DefaultView.ToTable().Rows[0]["Partyname"].ToString();
                            party[0].EndpointType = ds.Tables[0].DefaultView.ToTable().Rows[0]["TerminalType"].ToString();
                            party[0].EndpointConnectStatus = PartyEvent[i].ToString().Contains("H323_CALL_SETUP") == true ? "1" : "2"; // 1 - Connect 2- Disconnect
                            party[0].EndpointAddress = (from parEPT in xdoc.Descendants("CDR_EVENT").Where(D => (D.Elements("H323_CALL_SETUP").Any() && D.Element("H323_CALL_SETUP").Element("PARTY_ID").Value == party[0].partyGuid)) select parEPT.Element("H323_CALL_SETUP").Element("DESTINATION_ADDRESS").Value).FirstOrDefault();
                            party[0].ConnectionType = (from dd in xdoc.Descendants("CDR_EVENT").Where(Data => (Data.Elements("OPERATOR_ADD_PARTY").Any() && Data.Element("OPERATOR_ADD_PARTY").Element("ID").Value == party[0].partyGuid) || (Data.Elements("RESERVED_PARTY").Any() && Data.Element("RESERVED_PARTY").Element("ID").Value == party[0].partyGuid)) select (dd.Elements("OPERATOR_ADD_PARTY").Any() ? dd.Element("OPERATOR_ADD_PARTY").Element("CONNECTION").Value == "dial_out" ? "2" : "1" : dd.Element("RESERVED_PARTY").Element("CONNECTION").Value == "dial_out" ? "2" : "1")).ToList().FirstOrDefault();
                            party[0].EndpointDateTime = PartyEvent[i].Element("TIME_STAMP").Value;
                            if (party[0].EndpointConnectStatus == "2")
                                party[0].DisconnectReason = (from disconnect in PartyEvent[i].Descendants("DESCRIPTION") select disconnect.Value).FirstOrDefault();

                            ret = db.InsertCDRParty(party[0]);
                            if (!ret)
                            {
                                logger.Exception(100, "InsertCDRinRMX Method is failed.");
                            }
                        }
                    }
                }
                return true;
            }
            catch (Exception ex)
            {
                logger.Trace("ProcessandInsertPolycomRMXCDR failed: " + ex.Message);
                return false;
            }
        }
        #endregion

        //FB 2683 Ends

        //ALLDEV-773 Start

        #region CDRforPexip
        /// <summary>
        /// CDRforPexip
        /// </summary>
        /// <param name="MCU"></param>
        /// <param name="CDRDays"></param>
        /// <returns></returns>
        public bool CDRforPexip(NS_MESSENGER.MCU MCU, int CDRDays )
        {
            List<PexipconferenceDetails> PexicConfCDRList = new List<PexipconferenceDetails>();
            List<string> ConfNumnamelist = new List<string>();
            NS_MESSENGER.Conference Conf = new NS_MESSENGER.Conference();
            List<NS_MESSENGER.CDREvent> CDREvent = new List<NS_MESSENGER.CDREvent>();
            KeyValuePair<string, NS_MESSENGER.Conference> foundConference = new KeyValuePair<string, NS_MESSENGER.Conference>();
            listedConference = null;
            string partyGUID = "", alreadyExist = "";
            try
            {
                logger.Trace("Entering into CDRforPexip");


                NS_Pexip.pexip CDRPexip = new NS_Pexip.pexip(configParams);
                if (!CDRPexip.CDRConference(MCU, CDRDays, ref PexicConfCDRList))
                    return false;

                if (PexicConfCDRList.Count > 0)
                {
                    for (int i = 0; i < PexicConfCDRList.Count; i++)
                    {
                        Conf = new NS_MESSENGER.Conference();
                        CDREvent = new List<NS_MESSENGER.CDREvent>();
                        partyGUID = ""; alreadyExist = "0";
                        if (PexicConfCDRList[i].service_type == "conference")
                        {
                            string name = PexicConfCDRList[i].name;
                            int confnumname = 0;
                            if (name.Contains("(") && name.Contains(")"))
                            {
                                int n = name.Split('(').Count();
                                string p = name.Split('(')[n - 1].Split(')')[0];
                                int.TryParse(p, out confnumname);
                            }
                       
                            Conf.iDbNumName = confnumname;
                            db.FetchConfforCallDetailRecord(ref Conf, "", MCU);

                            bool ret = db.InsertCDRinMyvrm(ref CDREvent, MCU, ref alreadyExist, PexicConfCDRList[i]);
                            if (!ret)
                            {
                                logger.Exception(100, "InsertCDRinMyvrm Method is failed.");
                                return false;
                            }

                            if (alreadyExist == "0" && Conf.iDbID > 0)
                            {
                                ret = false;
                                ret = ProcessandInsertPexipConferenceCDR(Conf, MCU, PexicConfCDRList[i]);
                                if (!ret)
                                {
                                    logger.Exception(100, "InsertCDRinMyvrm Method is failed.");
                                    return false;
                                }
                            }
                        }
                        else if(PexicConfCDRList[i].service_type == "gateway")
                        {
                            if (listedConference == null)
                                listedConference = new Dictionary<string, NS_MESSENGER.Conference>();

                            Conf = new NS_MESSENGER.Conference();
                            if(PexicConfCDRList[i].name.Contains("Pexip S4B GW Rule:"))
                            {
                                int n = PexicConfCDRList[i].name.Split(':').Count();
                                partyGUID = PexicConfCDRList[i].name.Split(':')[n - 1];
                            }

                            db.FetchConfforCallDetailRecord(ref Conf, partyGUID, MCU);

                            bool ret = db.InsertCDRinMyvrm(ref CDREvent, MCU, ref alreadyExist, PexicConfCDRList[i]);
                            if (!ret)
                            {
                                logger.Exception(100, "InsertCDRinMyvrm Method is failed.");
                                return false;
                            }                          

                            foundConference = listedConference.Where(l => l.Key == Conf.iDbNumName.ToString().Trim()).FirstOrDefault();
                            if (foundConference.Value != null)
                                Conf = foundConference.Value;
                            else
                            {
                                if (alreadyExist == "0" && Conf.iDbID > 0)
                                {
                                    ret = false;
                                    ret = ProcessandInsertPexipConferenceCDR(Conf, MCU, PexicConfCDRList[i]);
                                    if (!ret)
                                    {
                                        logger.Exception(100, "InsertCDRinMyvrm Method is failed.");
                                        return false;
                                    }
                                }

                                if (Conf.iDbID > 0)
                                    listedConference.Add(Conf.iDbNumName.ToString().Trim(), Conf);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                logger.Trace("CDRforCodianMSESeries failed: " + ex.Message);
                return false;
            }
            return true;
        }
        #endregion

        #region ProcessandInsertPexipConferenceCDR
        /// <summary>
        /// ProcessandInsertPexipConferenceCDR
        /// </summary>
        /// <param name="Conf"></param>
        /// <param name="MCU"></param>
        /// <param name="PexicConfCDR"></param>
        /// <returns></returns>
        public bool ProcessandInsertPexipConferenceCDR(NS_MESSENGER.Conference Conf, NS_MESSENGER.MCU MCU, PexipconferenceDetails PexicConfCDR)
        {
            DataSet ds = new DataSet();
            NS_MESSENGER.MCU.CDRConference ConfCDR = new NS_MESSENGER.MCU.CDRConference();
            bool ret = false;
            int TotalPortActual = 0, mod = 0, TotalSec = 0;
            DateTime confStartDate = DateTime.UtcNow;
            try
            {

                ConfCDR = new NS_MESSENGER.MCU.CDRConference();

                TotalPortActual = 0; mod = 0; TotalSec = 0;

                ConfCDR.Confid = Conf.iDbID.ToString();
                ConfCDR.Instanceid = Conf.iInstanceID.ToString();
                ConfCDR.ConfNumName = Conf.iDbNumName.ToString();
                ConfCDR.ConfTimeZone = Conf.iTimezoneID.ToString();

                TotalSec = PexicConfCDR.duration;

                if (PexicConfCDR.service_type == "Conference")
                {
                    TotalPortActual = TotalSec / 60;
                    mod = TotalSec % 60;

                    if (mod >= 30)
                        TotalPortActual++;

                    ConfCDR.Duration = TotalPortActual.ToString();
                }
                else
                {
                    ConfCDR.Duration = Conf.iDuration.ToString();
                }
                ConfCDR.Host = Conf.sHostName;
                ConfCDR.McuAddress = MCU.sIp;
                ConfCDR.McuConfGuid = Conf.sGUID;
                ConfCDR.McuID = MCU.iDbId.ToString();
                ConfCDR.McuName = MCU.sName;
                ConfCDR.McuType = MCU.etType.ToString();
                ConfCDR.McuTypeID = MCU.sMcuType.ToString();
                ConfCDR.Orgid = Conf.iOrgID.ToString();
               
                if (Conf.iPermanent == 1)
                {
                    int confDuration = 0;
                    int.TryParse(ConfCDR.Duration, out confDuration);
                    ConfCDR.ScheduledStart = confStartDate.ToString();
                    ConfCDR.ScheduledEnd = confStartDate.AddMinutes(confDuration).ToString();
                }
                else
                {
                    ConfCDR.ScheduledStart = Conf.dtStartDateTime.ToString();
                    ConfCDR.ScheduledEnd = Conf.dtEndDateTime.ToString();
                }
             
                ConfCDR.Title = Conf.sExternalName;
                ConfCDR.ConfGUID = Conf.sGUID;
                ConfCDR.EventDate = Conf.dtStartDateTime.ToString(); 
                ConfCDR.IsVMRConf = Conf.iIsVMR.ToString();
                ConfCDR.IsPermanentConf = Conf.iPermanent.ToString();

                ret = db.InsertCDRConference(ConfCDR);
                if (!ret)
                {
                    logger.Trace("Failed to insert CDR Conference");
                    return false;
                }

                ret = false;

                if (MCU.etType == NS_MESSENGER.MCU.eType.CISCOTP)
                    ret = ProcessandInsertPexipParticipant(Conf, MCU, PexicConfCDR);

                if (!ret)
                {
                    logger.Exception(100, "InsertCDRinMyvrm Method is failed.");
                    return false;
                }
                
                return true;
            }
            catch (Exception ex)
            {
                logger.Trace("ProcessandInsertConferenceCDR failed: " + ex.Message);
                return false;
            }
        }
        #endregion

        #region ProcessandInsertPexipParticipant
        /// <summary>
        /// ProcessandInsertPexipParticipant
        /// </summary>
        /// <param name="Conference"></param>
        /// <param name="MCU"></param>
        /// <param name="PexicConfCDR"></param>
        /// <returns></returns>
        public bool ProcessandInsertPexipParticipant(NS_MESSENGER.Conference Conference, NS_MESSENGER.MCU MCU, PexipconferenceDetails PexicConfCDR)
        {
            NS_MESSENGER.MCU.CDRParty partyCDR = new NS_MESSENGER.MCU.CDRParty();
            bool ret = false;
            List<int> endpointid = new List<int>();
            PexipparticipantDetails Pexipparty = new PexipparticipantDetails();
            DataSet ds = new DataSet();
            try
            {
                List<string> partyid = null;

                for (int t1 = 0; t1 < PexicConfCDR.participants.Count; t1++)
                {
                    partyid.Add(PexicConfCDR.PartyDetails[t1].id);
                    ret = db.GetConferenceIDfromParty(partyid, ref ds);
                    if (!ret)
                    {
                        logger.Trace("Error in Fetch Participant Details");
                        return false;
                    }

                    for (int m = 0; m < 2; m++)
                    {
                        Pexipparty = PexicConfCDR.PartyDetails[t1];
                        partyCDR = new NS_MESSENGER.MCU.CDRParty();
                        partyCDR.EndpointAddress = Pexipparty.remote_address;
                        partyCDR.partyGuid = Pexipparty.id;

                        if (m == 1)
                        {
                            partyCDR.EndpointDateTime = Pexipparty.start_time.ToString();
                            partyCDR.EndpointConnectStatus = "1";
                            partyCDR.DisconnectReason = "";
                        }
                        else
                        {
                            partyCDR.EndpointDateTime = Pexipparty.end_time.ToString();
                            partyCDR.EndpointConnectStatus = "2";
                            partyCDR.DisconnectReason = Pexipparty.disconnect_reason;
                        }
                        partyCDR.ConfGUID = Conference.sGUID;
                        partyCDR.confid = Conference.iDbID.ToString();
                        partyCDR.ConfNumName = Conference.iDbNumName.ToString();
                        partyCDR.ConnectionType = Pexipparty.call_direction == "out" ? "2" : "1";
                        if (ds.Tables[0].Rows.Count > 0)
                        {
                           
                            partyCDR.EndpointName = ds.Tables[0].DefaultView.ToTable().Rows[0]["Partyname"].ToString();
                            partyCDR.EndpointType = ds.Tables[0].DefaultView.ToTable().Rows[0]["TerminalType"].ToString();
                        }
                        else
                        {
                            partyCDR.EndpointName = Pexipparty.display_name;
                            partyCDR.EndpointType = "2";
                        }
                        partyCDR.InstanceId = Conference.iInstanceID.ToString();
                        partyCDR.OrgID = Conference.iOrgID.ToString();
                        partyCDR.IsVMRConf = Conference.iIsVMR.ToString(); 
                        partyCDR.IsPermanentConf = Conference.iPermanent.ToString();
                        ret = db.InsertCDRParty(partyCDR);
                        if (!ret)
                        {
                            logger.Exception(100, "ProcessandInsertPexipParticipant Method is failed.");
                        }
                    }
                }

                return true;
            }
            catch (Exception ex)
            {
                logger.Trace("ProcessandInsertParticipant8000SeriesCDR failed" + ex.Message);
                return false;
            }
        }
        #endregion
   
        //ALLDEV-773 End
    }

    public class PexipconferenceDetails
    {
        public int duration { get; set; }
        public DateTime end_time { get; set; }
        public string id { get; set; }
        public int instant_message_count { get; set; }
        public string name { get; set; }
        public int participant_count { get; set; }
        public List<string> participants { get; set; }
        public string resource_uri { get; set; }
        public string service_type { get; set; }
        public DateTime start_time { get; set; }
        public string tag { get; set; }
        public List<PexipparticipantDetails> PartyDetails { get; set; }
    }
    
    public class PexipparticipantDetails
    {
        public string bandwidth { get; set; }
        public string call_direction { get; set; }
        public string call_uuid { get; set; }
        public string conference { get; set; }
        public string conference_name { get; set; }
        public string disconnect_reason { get; set; }
        public string display_name { get; set; }
        public int duration { get; set; }
        public string encryption { get; set; }
        public DateTime end_time { get; set; }
        public int has_media { get; set; }
        public string id { get; set; }
        public int is_streaming { get; set; }
        public int license_count { get; set; }
        public string local_alias { get; set; }
        public string media_node { get; set; }
        public string parent_id { get; set; }
        public string protocol { get; set; }
        public string remote_address { get; set; }
        public string remote_alias { get; set; }
        public int remote_port { get; set; }
        public string resource_uri { get; set; }
        public string role { get; set; }
        public string service_tag { get; set; }
        public string service_type { get; set; }
        public string signalling_node { get; set; }
        public DateTime start_time { get; set; }
        public string system_location { get; set; }
        public string vendor { get; set; }
    }

}
