//ZD 100147 Start
/* Copyright (C) 2015 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
//ZD 100147 ZD 100886 End
/* FILE : Messenger.cs
 * DESCRIPTION : All messenger objects used in the assembly are defined here.
 * AUTHOR : Kapil M
 */
namespace NS_MESSENGER
{
    using System;
    using System.Net;
    using System.Collections;
    using System.Runtime.Serialization.Formatters.Binary;
    using System.IO;
    using System.Collections.Generic;
    
    //ZD 104021 Start
    public enum BJNMeetingType
    {
        PersonalMeeting = 1, OneTimeMeeting = 2
    };
    //ZD 104021 End

    [Serializable] //FB 2441
    class MCU
    {
        internal enum eType { ACCORDv6, ACCORDv7, RMX, TANDBERG, RADVISION, CISCO, CODIAN, CMA, Lifesize, CISCOTP, RPRM, RADVISIONIVIEW, TMSScheduling, Webex, Pexip, BLUEJEANS };//CMA FB 2261 FB 2501 Call Monitoring //FB 2441//FB 2556 //FB 2718 //ZD 101217 //ZD 103263
        internal string sName, sIp, sLogin, sPwd, sStation, sBJNAPPkey, sBJNAppsecret, sBJNAccessToken;//ZD 104021
        internal string sToken, sUserToken;
        internal eType etType;
        internal int iHttpPort, iMaxConcurrentAudioCalls, iMaxConcurrentVideoCalls, iURLAccess, iBJNExpiry, ienterpriseId;//ZD 100113 //ZD 104021
        internal DateTime dBJNExpiryDateTime, dBJNLastmodifieddatetime; //ZD 104021
        internal string sSoftwareVer;
        internal double dblSoftwareVer;
        internal int iDbId, iTimezoneID;
        //FB 1642 - DTMF
        //internal string preConfCode, preLPin, postLPin; commented for FB 2655
        internal int iEnableIVR; //FB 1766
        internal string sIVRServiceName, sMultipleAssistant; //FB 1766 //ZD 100369_MCU
        internal int iEnableRecording, ConfServiceID, iLPR, iPollCount,iPoolOrder; //FB 2016 //FB 1907 //ZD 100369_MCU//ZD 104256
        internal string sisdnGateway; //MSE 8000
        internal string iISDNAudioPrefix;  //FB 2003
        internal string iISDNVideoPrefix;  //FB 2003
        internal int iDefaultLO, sMcuType,IViewOrgID;  //FB 2335 //FB 2448 //FB 2556
        internal string sDMADomain,sTemplateName,sDMALogin,sDMAip,sDMAPassword, sAdminName, sAdminEmail, sRPRMLogin, sRPRMEmailaddress,sRPRMDomain; // FB 2441 //FB 2709
        internal int iDMASendmail, iDMAHttpport,iSynchronous, iAdminID, ilogincount;// FB 2441 //FB 2709
        internal int iEnableCDR, iDeleteCDRDays, iOrgId;//FB 2660 //FB 2593
		internal string sDMADialinprefix = ""; //FB 2689
        internal enum PollState { FAIL = -1, PASS = 1 }; //ZD 100369_MCU
        internal string sBridgeExtNo = "", sBridgeDomain = "", sPoolOrderName, sAlias2, sAlias3, sConferenceDialinName; // ZD 100768 //ZD 100522//ZD 104256 //ALLDEV-854  //C20 S4B
        internal int iVideoLayoutId, iFamilyLayout, iEnableAlias; //ZD 101869 //ALLDEV-854 
        
        //FB 2593 Starts
        public class CDRConference
        {
            public string Confid { get; set; }
            public string Instanceid { get; set; }
            public string Orgid { get; set; }
            public string Title { get; set; }
            public string ConfNumName { get; set; }
            public string McuID { get; set; }
            public string McuName { get; set; }
            public string McuTypeID { get; set; }
            public string McuType { get; set; }
            public string McuAddress { get; set; }
            public string McuConfGuid { get; set; }
            public string Host { get; set; }
            public string ScheduledStart { get; set; }
            public string ScheduledEnd { get; set; }
            public string Duration { get; set; }
            public string ConfTimeZone { get; set; }
            public string EventId { get; set; }
            public string EventType { get; set; }
            public string ConfGUID { get; set; }
            public string EventDate { get; set; }
            public string IsVMRConf { get; set; }//ZD 101348
            public string IsPermanentConf { get; set; }//ZD 101348
        }
        public class CDRParty
        {
            public string confid { get; set; }
            public string InstanceId { get; set; }
            public string ConfNumName { get; set; }
            public string OrgID { get; set; }
            public string ConfGUID { get; set; }
            public string partyGuid { get; set; }
            public string EndpointName { get; set; }
            public string EndpointAddress { get; set; }
            public string EndpointType { get; set; }
            public string ConnectionType { get; set; }
            public string EndpointDateTime { get; set; }
            public string EndpointConnectStatus { get; set; }
            public string DisconnectReason { get; set; }
            public string EventId { get; set; }
            public string ConfTimeZone { get; set; }
            public string IsVMRConf { get; set; } //ZD 101348
            public string IsPermanentConf { get; set; } //ZD 101348
        }
        //FB 2593

        //FB 2683 Start

        public class PolycomRPRMCDR
        {
            public string Calluuid { get; set; }
            public string confuuid { get; set; }
            public string Version { get; set; }
            public string conf_Type { get; set; }
            public string conf_cluster { get; set; }
            public DateTime conf_startTime { get; set; }
            public string conf_Endtime { get; set; }
            public string conf_userID { get; set; }
            public string conf_roomid { get; set; }
            public string conf_Partycount { get; set; }
            public string classofservice { get; set; }
            public string UserDataA { get; set; }
            public string UserDataB { get; set; }
            public string UserDataC { get; set; }
            public string UserDataD { get; set; }
            public string UserDataE { get; set; }
            public string ept_type { get; set; }
            public string ept_callType { get; set; }
            public string ept_dialin { get; set; }
            public string ept_starttime { get; set; }
            public string ept_endtime { get; set; }
            public string ept_orgendpoint { get; set; }
            public string ept_dialstring { get; set; }
            public string ept_destEndpoint { get; set; }
            public string ept_origSignalType { get; set; }
            public string ept_destSignalType { get; set; }
            public string ept_lastForwardEndpoint { get; set; }
            public string ept_cause { get; set; }
            public string ept_causeSource { get; set; }
            public string ept_bitRate { get; set; }
            public string ept_classOfService { get; set; }
            public string ept_ingressCluster { get; set; }
            public string ept_egressCluster { get; set; }
            public string ept_VMRCluster { get; set; }
            public string ept_VEQCluster { get; set; }
            public string ept_userRole { get; set; }

        }

        //FB 2683 End

        internal MCU()
        {
            sName = sIp = sLogin = sPwd = sStation = sToken = sUserToken = sSoftwareVer = null;
            iHttpPort = 80; // default to port 80
            iTimezoneID = 26; // default to Eastern Time 
            iMaxConcurrentAudioCalls = iMaxConcurrentVideoCalls = 0;
            //FB 1642 - DTMF
            //preConfCode = preLPin = postLPin = null; commented for FB 2655
            iEnableIVR = 0; //default to 0-diabled FB 1766
            sIVRServiceName = null; //FB 1766
            iEnableRecording = 0; //default to 0-false FB 1907
            sisdnGateway = "";// MSE 8000
            iISDNAudioPrefix = iISDNVideoPrefix = ""; //FB 2003
            ConfServiceID = 71; //FB 2016
			iLPR = 0; // FB 1907
            iDefaultLO = -1;  //FB 2335

        }
    }
    //FB 2591
    class MCUProfile
    {
        internal int iId;
        internal string sName, sDisplayname;
    }

    //ZD 104221 start
    class MCUPoolOrder
    {
        internal int iId;
        internal string sName, sDisplayname;
    }

    //ZD 104221  End


    //ZD 101217
    class SystemLocation
    {
        internal string id, name;
    }

    [Serializable] //FB 2441

    //FB 2659 - Starts
    class ExtMCUSilo
    {
        internal int iBridgeId, iOrgID, iBridgeTypeId;
        internal string sOrgName, sAlias, sBillingCode;
    }
    [Serializable]
    class ExtMCUService
    {
        internal int iBridgeId, iBridgeTypeId, iMemberId, iServiceId;
        internal string sServiceName, sDescription, sPrefix;
    }
    [Serializable]
    //FB 2659 - End
    //ZD 100755 Inncrewin
    class RadVisionConfParam
    {
        private string _userID = string.Empty; //ZD 100890
        private string _VirtualRoomId = string.Empty; //ZD 100890
        private string _staticID = string.Empty;
        private int _servicePrefix = 403;
        private PRIORITY _priority = PRIORITY.UNSPECIFIED;
        private StreamStatusResult _AllowStreaming = StreamStatusResult.OFF;
        private StreamStatusResult _StreamingStatus = StreamStatusResult.OFF;
        private TimeSpan _afterLeft = new TimeSpan(0, 30, 0);
        private TerminationStatus _terminationCndtn = TerminationStatus.NORMAL;
        private string _strDuratnLeft = string.Empty;

        public enum PRIORITY { DELAY, LOCAL, UNSPECIFIED }
        public enum StreamStatusResult { ON, OFF, DISABLED, UNDEFINED, TEMPORARILY_OFF }
        public enum TerminationStatus { NORMAL, AFTER_ALL_PARTIES_LEFT, AFTER_HOST_LEFT }

        public string UserID { get { return _userID; } set { _userID = value; } } //ZD 100890
        public string VirtualRoomId { get { return _VirtualRoomId; } set { _VirtualRoomId = value; } } //ZD 100890
        public string StaticIDNumber { get { return _staticID; } set { _staticID = value; } }
        public int ServicePrefix { get { return _servicePrefix; } set { _servicePrefix = value; } }
        public PRIORITY Priority { get { return _priority; } set { _priority = value; } }
        public StreamStatusResult AllowStreaming { get { return _AllowStreaming; } set { _AllowStreaming = value; } }
        public StreamStatusResult StreamingStatus { get { return _StreamingStatus; } set { _StreamingStatus = value; } }
        public bool AutoExtend { get; set; }
        public bool WaitingRoom { get; set; }
        public TimeSpan SetDurationAfterLeft { get { return _afterLeft; } set { _afterLeft = value; } }
        public string getDurationAfterLeft
        {
            get
            {
                _strDuratnLeft = "PT";
                if (_afterLeft.Days > 0)
                    _strDuratnLeft += _afterLeft.Days.ToString() + "D";
                if (_afterLeft.Hours > 0)
                    _strDuratnLeft += _afterLeft.Hours.ToString() + "H";
                if (_afterLeft.Minutes > 0)
                    _strDuratnLeft += _afterLeft.Minutes.ToString() + "M";
                if (_afterLeft.Seconds > 0)
                    _strDuratnLeft += _afterLeft.Seconds.ToString() + "S";

                return _strDuratnLeft;
            }
        }
        public TerminationStatus TerminationCondition { get { return _terminationCndtn; } set { _terminationCndtn = value; } }
        public bool OneTimePINRequired { get; set; }
        public bool Public { get; set; }
        public bool Default { get; set; }

    }
    //ZD 100755 Inncrewint
    [Serializable]
    class Conference
    {
        internal RadVisionConfParam radParam;//ZD 100755 Inncrewin
        // Used for storing conf info.
        [Serializable] //FB 2441
        internal struct sStatus
        {
            internal int iMcuID; //bridge-specific id (to be retreived from the bridge.)
            internal bool bIsEmpty, bIsResourceDeficient;
            internal int iTotalConnectedParties;
        };
        [Serializable] //FB 2441
        internal struct sColorCode
        {
            internal int iRed; //red code
            internal int iGreen; //green code
            internal int iBlue; //blue code
        };
        internal string sDbName, sPwd, sMcuName, sExternalName, sPermanentconfName, sURI, sHostPwd;//ZD 100522_S1 //ZD 101217 //ALLDEV-826
        internal int iDbNumName, iuniqueId, iRoomVMRID; //FB 2797 //ZD 100522

        //internal enum eLayout {_1x1,_1x2,_2x1,_2x2,_1and5,_3x3};
        internal enum eMediaType { AUDIO, AUDIO_VIDEO, AUDIO_VIDEO_DATA };
        internal enum eVideoSession { SWITCHING, TRANSCODING, CONTINOUS_PRESENCE };
        internal enum eNetwork { IP, ISDN };
        //internal enum eLineRate {K56,K64,K2x56,K2x64,K128,K256,K384,K512,K768,M1152,M1472,M1536,M1920};
        internal enum eAudioCodec { AUTO, G728, G711_56, G722_24, G722_32, G722_56 };
        internal enum eVideoCodec { AUTO, H261, H263, H264 };
        internal enum eType { POINT_TO_POINT, MULTI_POINT, ROOM_CONFERENCE };
        internal enum eMsgServiceType { NONE, WELCOME_ONLY, ATTENDED, IVR };
        internal enum eStatus { SCHEDULED, PENDING, TERMINATED, ONGOING, OnMCU, COMPLETED, DELETED, WAITLIST };//ZD 102532
        internal int iDbID, iMcuID, iPwd, iHostPwd; //ALLDEV-826
        internal int iInstanceID;
        internal Queue qParties;
        internal MCU cMcu; //bridge info
        internal sStatus stStatus;
        internal eMediaType etMediaType;
        //internal eLineRate etLineRate ;
        internal LineRate stLineRate;

        internal eVideoSession etVideoSession;
        internal eAudioCodec etAudioCodec;
        internal eVideoCodec etVideoCodec;
        //internal eLayout etVideoLayout; 
        internal eNetwork etNetwork;
        internal DateTime dtStartDateTime, dtStartDateTimeInUTC, dtStartDateTimeInLocalTZ,
                          dtSetUpDateTime,dtEndDateTime, dtModifiedDateTime;//FB 2363s
        internal string sName_StartDateTimeInLocalTZ;
        internal int iDuration; // in minutes
        internal bool bLectureMode; internal string sLecturer;
        internal bool bMessageOverlay;//FB 2486
        internal bool bAutoTerminate;
        internal bool bH239; //dual stream mode
        internal bool bConferenceOnPort;
        internal bool bEncryption;
        internal bool bEntryNotification;
        internal bool bEndTimeNotification;
        internal bool bExitNotification;
        internal eMsgServiceType etMessageServiceType;
        internal string sMessageServiceName;
        internal sColorCode stLayoutBorderColor;
        internal sColorCode stSpeakerNotation;
        internal int iVideoLayout, iFamilylayout;//ZD 101869
        internal int iMaxAudioPorts;
        internal int iMaxVideoPorts;
        internal int iTimezoneID;
        internal eType etType;
        internal int iHostUserID;
        internal string sHostEmail, sDescription, sHostPassword, sHostLastName, sHostFirstName;//ZD 100753 //C20 S4B
        internal bool isSingleDialIn, isVIP; //FB 2363s
        internal bool bEntryQueueAccess;
        internal eStatus etStatus;
        internal string sHostName, ESType, ESId, sMeetandGreet, sDedicatedvnoc, sAvtech; //FB 2363s
        internal int iOrgID;//FB 2335
        internal int iImmediate;//FB 2440
        internal int iIsVMR;//FB 2447
        internal string sInternalBridgeNumber;//FB 2447
        internal string sExternalBridgeNumber;//FB 2447
        //FB 2501 Call Monitoring Start
        internal int ilockorunlock, iMessDuration, iMessPosition;
        internal string sMessage = "", sGUID = "";
        internal int PortNumber, iConfRMXServiceID , iConfPoolOrderID; //FB 2839 //ZD 104256
        internal string ServerAddress;
        //FB 2501 Call Monitoring End
        internal bool bE164, bH323;//FB 2636
        internal int iPolycomSendMail, iRPRM, iRecurring,iPushtoExternal,iRPRMUserID, iAccessType, iStatus;//FB 2636 //FB 2441 //FB 2709 //FB 2556T //FB 2710
        internal string sPolycomTemplate, sDialString, sEtag, sRPRMLogin, sRPRMDomain, sDialinNumber;// FB 2441 //FB 2709 //FB 2659
        internal string IViewNumber, sConfPoolOrderName, sconfLyncMeetingURL;//FB 2556 //ZD 104256 //ALLDEV-862
        internal int iEndpointCount; //FB 2556
        internal int iMcuSetupTime, iMCUTeardonwnTime,iWebExconf,iWebexExceptionHandler; //FB 2998 //ZD 100221
        internal int iconfMode = 1, iConfType = 2, iOBTP = 0;//ZD 100694 //ZD 100513
		internal bool bIsCasecadeofsametype = false;// ZD 100768
        internal string sWebEXUserName, sWebEXPassword, sWebEXSiteID , sWebEXPartnerID , sWebExURL ,sWebEXEmail,sWebEXMeetingpwd,sWebEXMeetingKey; // ZD 100221
        internal Queue Recurrence;// ZD 100221
        internal int iMeetingId, iPermanent, iSystemLocation, iBJNConf, iCascadeBJN, iBJNMeetingType; //ZD 100167 //ZD 100522 //ZD 104021
        internal string sSystemLocationName, sBJNUniqueid, sBJNUserId, sBJNMeetingid, sBJNPasscode, sBJNHostPasscode; //ZD 101522 //ZD 104021
        internal int iEnableHostGuestPwd, iEnableConfpassword, iIsLyncConf, iEnableS4BEWS;  //ALLDEV-826 //ALLDEV-862
        internal int iEnableSyncLaunchBuffer, iSyncLaunchBuffer;  //ALLDEV-837
        internal int iPexipConf = 0, iPexipCOnfTYpe = 1, iPexipStaticAlias =0; //ALLDEV-782
        internal string sPexipStaticAlias = "", PexipAlias = ""; //ALLDEV-782

        internal Conference()
        {
            // constructor
            sDbName = sPwd = sMcuName = sDescription = sLecturer = sHostPwd = null; //ALLDEV-826
            qParties = new Queue();
            cMcu = new MCU();
            iDbID = iInstanceID = iMcuID = iPwd = iVideoLayout = iDuration = iHostPwd = 0; //ALLDEV-826
            iMaxAudioPorts = iMaxVideoPorts = 0;
            etType = 0; iTimezoneID = 26;
            bLectureMode = bAutoTerminate = bH239 = bConferenceOnPort = bEncryption = bEntryNotification = bEndTimeNotification = bExitNotification = false;
            stLayoutBorderColor.iBlue = stLayoutBorderColor.iGreen = stLayoutBorderColor.iRed = 0;
            stSpeakerNotation.iBlue = stSpeakerNotation.iGreen = stSpeakerNotation.iRed = 0;
            stStatus.bIsEmpty = stStatus.bIsResourceDeficient = isSingleDialIn = bEntryQueueAccess = bLectureMode = false;
            stStatus.iMcuID = stStatus.iTotalConnectedParties = 0;
            iIsVMR = 0;//FB 2447
            sInternalBridgeNumber = sExternalBridgeNumber = "";//FB 2447
            iEnableSyncLaunchBuffer = iSyncLaunchBuffer = 0;    //ALLDEV-837
            iEnableHostGuestPwd = iEnableConfpassword = 0; //ALLDEV-826
            Recurrence = new Queue(); //ZD 100221
        }


        internal void CopyTo(ref NS_MESSENGER.Conference newConf)
        {
            newConf.iDbID = this.iDbID;
            newConf.iInstanceID = this.iInstanceID;
            newConf.sDbName = this.sDbName;
            newConf.iMcuID = this.iMcuID;
            newConf.iDbNumName = this.iDbNumName;
            newConf.dtStartDateTime = this.dtStartDateTime;
            newConf.dtStartDateTimeInUTC = this.dtStartDateTimeInUTC;
            newConf.sDialinNumber = this.sDialinNumber;//FB 2636//FB 2659
            //newConf.etVideoLayout = this.etVideoLayout;
            newConf.etVideoSession = this.etVideoSession;
            newConf.etMediaType = this.etMediaType;
            newConf.etNetwork = this.etNetwork;
            newConf.sMcuName = this.sMcuName;
            newConf.stLineRate = this.stLineRate;
            newConf.bLectureMode = this.bLectureMode;
            newConf.iDuration = this.iDuration;
            newConf.bAutoTerminate = this.bAutoTerminate;
            newConf.bH239 = this.bH239;
            newConf.bConferenceOnPort = this.bConferenceOnPort;
            newConf.bEncryption = this.bEncryption;
            newConf.iVideoLayout = this.iVideoLayout;

            newConf.cMcu.iDbId = this.cMcu.iDbId;
            newConf.cMcu.sIp = this.cMcu.sIp;
            newConf.cMcu.sLogin = this.cMcu.sLogin;
            newConf.cMcu.sName = this.cMcu.sName;
            newConf.cMcu.sPwd = this.cMcu.sPwd;
            newConf.cMcu.sStation = this.cMcu.sStation;
            newConf.cMcu.etType = this.cMcu.etType;
            newConf.cMcu.dblSoftwareVer = this.cMcu.dblSoftwareVer;
            newConf.cMcu.iTimezoneID = this.cMcu.iTimezoneID;
            newConf.cMcu.iHttpPort = this.cMcu.iHttpPort;
            
            newConf.isSingleDialIn = this.isSingleDialIn;
            newConf.sPwd = this.sPwd;
            newConf.iPwd = this.iPwd;
            newConf.bEntryQueueAccess = this.bEntryQueueAccess;

            //FB 1642 - DTMF Commented for FB 2655
            //newConf.cMcu.postLPin = this.cMcu.postLPin;
            //newConf.cMcu.preLPin = this.cMcu.preLPin;
            //newConf.cMcu.preConfCode = this.cMcu.preConfCode;

            //FB 2636 
            newConf.bH323 = this.bH323;
            newConf.bE164 = this.bE164;

            //ZD 104021 Start
            newConf.iBJNConf = this.iBJNConf;
            newConf.iBJNMeetingType = this.iBJNMeetingType;
            newConf.sBJNHostPasscode = this.sBJNHostPasscode;
            newConf.sBJNMeetingid = this.sBJNMeetingid;
            newConf.sBJNPasscode = this.sBJNPasscode;
            newConf.sBJNUniqueid = this.sBJNUniqueid;
            newConf.sBJNUserId = this.sBJNUserId;
            //ZD 104021 End

            //ALLDEV-826 Starts
            newConf.iEnableConfpassword = this.iEnableConfpassword;
            newConf.iEnableHostGuestPwd = this.iEnableHostGuestPwd;
            newConf.sHostPwd = this.sHostPwd;
            newConf.iHostPwd = this.iHostPwd;
            //ALLDEV-826 Ends
			//ALLDEV-837 Starts
            newConf.iEnableSyncLaunchBuffer = this.iEnableSyncLaunchBuffer;
            newConf.iSyncLaunchBuffer = this.iSyncLaunchBuffer;
            //ALLDEV-837 Ends
        }


    }
    [Serializable] //FB 2441
    struct LineRate
    {
        internal enum eLineRate { K56, K64, K2x56, K2x64, K128, K192, K256,K320, K384, K512, K768, M1024, M1152,M1250, M1472, M1536, M1792, M1920, M2048,M2560,M3072,M3584, M4096 };
        internal eLineRate etLineRate;
    }
    [Serializable] //FB 2441
    class Party
    {
        // Used for storing party info.
        internal enum eProtocol { IP, ISDN, MPI, SIP }; //FB 2390
        internal enum eConnectionType { DIAL_IN = 1, DIAL_OUT, DIRECT };
        internal enum eOngoingStatus { DIS_CONNECT, PARTIAL_CONNECT, FULL_CONNECT, ONLINE, UNREACHABLE }; //Blue Status
        internal enum eType { USER, ROOM, GUEST, CASCADE_LINK, VMR, BJN };//FB 2447 //ZD 104021
        //internal enum eLineRate {K56,K64,K2x56,K2x64,K128,K256,K384,K512,K768,M1024,M1472};
        internal enum eCascadeRole { MASTER, SLAVE };
        internal enum eVideoProtocol { AUTO, H261, H263, H264 };
        internal enum eCallType { AUDIO, VIDEO };
        internal enum eVideoEquipment { ENDPOINT, RECORDER };
        internal enum eModelType { POLYCOM_VSX, POLYCOM_HDX, TANDBERG, CODIAN_VCR, OTHER, POLYCOM_OTX, POLYCOM_RPX, CISCO, MXP, ViewStation, CISCO_TS_TX }; //FB 2335 //FB 2390 //ZD 100519
        internal enum eAddressType { IP_ADDRESS, H323_ID, E_164, ISDN_PHONE_NUMBER, MPI, SIP};//FB 2390
        internal enum eCallerCalleeType { CALLER, CALLEE };
        //internal enum eLineRate { K56, K64, K2x56, K2x64, K128, K256, K384, K512, K768, M1152, M1472, M1536, M1920 };
        internal LineRate stLineRate;
        internal eCallerCalleeType etCallerCallee;
        internal string sMcuName, sName;
        internal int iMcuId, iDbId, iAPIPortNo, iconfnumname, ip2pCallid = -1, SysLocationID;//Code added for API Port//FB 2390 //ZD 104821
        internal string sAddress, sMcuAddress, sGatewayAddress;	 //ip or isdn address	
        internal string sNetworkURL;//FB 2595
        internal int iSecureport;//FB 2595
        //internal eLineRate elrMcuLineRate;
        internal eProtocol etProtocol;
        internal eConnectionType etConnType;
        internal eOngoingStatus etStatus;
        internal eType etType;
        internal MCU cMcu;
        internal eVideoProtocol etVideoProtocol;
        internal bool bMute;
        internal string sDisplayLayout;
        internal eCascadeRole etCascadeRole;
        internal eCallType etCallType;
        internal eVideoEquipment etVideoEquipment;
        internal eModelType etModelType;
        internal eAddressType etAddressType;

        internal bool bIsOutsideNetwork;
        internal string sGateKeeeperAddress = "";//ZD 100132
        internal string sMcuServiceName;
        internal string sLogin = "", sPwd = ""; //ZD 100814 ZD 100962
        internal string sConfNameOnMcu;
        internal string sDTMFSequence;
        //internal eLineRate etLineRate;
        internal bool bIsLecturer;
        internal bool bIsMessageOverlay;//FB 2486
        //Polycom CMA Start
        internal bool isRoom,isTelePresence;//FB 2400
        internal string Emailaddress = "",MulticodecAddress="";//FB 2400
        //FB 2501 Call Monitoring Start
        internal string sFECCdirection = "", sMessagedirection = "", sMessage = "", sGUID = "";
        internal int iMessDuration = 0, iAudioMode = 0, iAudioGained = 0, iMessPosition;
        internal String sRxAudioPacketsReceived = "", sRxAudioPacketErrors = "", sRxAudioPacketsMissing = "", sTxAudioPacketsSent = "";//ZD 100632
        internal String sRxVideoPacketsReceived = "", sRxVideoPacketErrors = "", sRxVideoPacketsMissing = "", sTxVideoPacketsSent = "";//ZD 100632
        internal bool bMuteRxaudio, bMuteRxvideo, bMuteTxvideo, bSetFocus, bactiveSpeaker;//ZD 100174 //ZD 100628
        internal String sStream = "";
        //FB 2501 Call Monitoring End
        //FB 2501 P2P Call Monitoring Start
        internal Int32 iTerminalType = 0, iSSHSupport = 0;//ZD 101363
        //FB 2501 P2P Call Monitoring End
        //Polycom CMA End
        internal string preConfCode, preLPin, postLPin, AudioDialInPrefix; // FB 2655
        internal bool bAudioBridgeParty, IsStaticIDEnabled;// FB 2655 //ZD 100890
        internal int iEndpointId, iVideoEquipmentid; //FB 2989 Note: iEndpointId is Table id. iDbId is adding 1000 and 2000. //c20 S4B
        internal String sFirstName = "", sLastName = "", sStaticID = "", sWorkPhone = "", sCellPhone = "", siViewUserID = "", sVirtualRoomId = ""; //ZD 100890
        internal string sCascadeBridgeExtNo = ""; // ZD 100768
        internal Party()
        {
            cMcu = new MCU();
            bMute = false;
            sLogin = sPwd = null;
            isRoom = false;//Polycom CMA
            preConfCode = preLPin = postLPin = AudioDialInPrefix = ""; //FB 2655
            sStream = "";//FB 2640
            sPwd = "";// ZD 100616
            sCascadeBridgeExtNo = ""; // ZD 100768
        }
    }


    class Email
    {
        // Used for storing a single email info.		
        internal string From, To, CC, BCC, Subject, Body, Attachment;
        internal int RetryCount, UUID, isCalender;//ICAL Fixes;
        internal int orgID; //Added for FB 1710
        internal bool isHTML;
        internal DateTime LastRetryDateTime;
        internal Email()
        {
            From = To = CC = BCC = Subject = Body = null;
            RetryCount =UUID=isCalender = 0;//ICAL Fixes
            isHTML = true;
        }
    }


    class Smtp
    {
        // Used for storing smtp server info.
        internal int PortNumber, ConnectionTimeOut , RetryCount; // in millisecs //FB 2552
        internal string ServerAddress, Login, Password, CompanyMailAddress, DisplayName, FooterMessage, WebsiteURL;//Vidyo //FB 2599
        internal Smtp()
        {
            PortNumber = 25; ConnectionTimeOut = 10000;
            ServerAddress = null; Login = null; Password = null; CompanyMailAddress = null; WebsiteURL = "";//Vidyo //FB 2599
        }
    }


    class AdminSettings
    {
        internal int delta;
        internal bool autoTerminate;
        internal bool allowDialOut;
        internal bool allowP2P;
        internal int retries;
        internal AdminSettings()
        {
            delta = 60;
            autoTerminate = false;
            allowDialOut = true;
            allowP2P = false;
            retries = 0;
        }
    }

    public class ConfigParams //ZD 104116
    {
        internal enum eDatabaseType { SQLSERVER, ORACLE, MYSQL, DB2 };
        internal enum eLogLevel { SYSTEM_ERROR, USER_ERROR, WARNING, INFO, DEBUG };

        // FB case# 582
        internal struct sAutoTerminateCall
        {
            internal bool On;
            internal int Time_Before_First_Join;
            internal int Time_After_Last_Quit;
        }
        internal sAutoTerminateCall stAutoTerminateCall;

        internal string localConfigPath, globalConfigPath, logFilePath, databaseLogin, databaseServer, databasePwd, databaseName, client, rootFilePath, ldapLoginKey, scheduledReportsAspxUrl, trustedConnection; //FB 2700
        internal DateTime ldapCycleTime;
        internal eDatabaseType databaseType;
        internal bool fileTraceEnabled, ldapEnabled, bAutoRestrictDbLogFileGrowth;
        internal eLogLevel logLevel;
        internal int logKeepDays, databaseConTimeout;
        internal string mailLogoPath; //FB 1658 - Embedded Image
        //internal string audioDialNoPrefix = ""; //Disney New Audio Add on Request Commented for FB 2655

        internal struct sDTMF_MGC
        {
            internal string PreConfCode;
            internal string PreLeaderPin;
            internal string PostLeaderPin;
        }
        internal sDTMF_MGC stDTMF_MGC;

        internal struct sDTMF_Codian
        {
            internal string PreConfCode;
            internal string PreLeaderPin;
            internal string PostLeaderPin;
        }
        internal sDTMF_Codian stDTMF_Codian;

        public ConfigParams() //ZD 104116
        {
            fileTraceEnabled = ldapEnabled = bAutoRestrictDbLogFileGrowth = false;
            rootFilePath = null;
            ldapLoginKey = "cn";
            DateTime dt = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 03, 00, 00);
            ldapCycleTime = dt;
            logKeepDays = 0;
            databaseConTimeout = 10;
            trustedConnection = "";//FB 2700
            // FB case #582
            stAutoTerminateCall.On = false; stAutoTerminateCall.Time_After_Last_Quit = 0; stAutoTerminateCall.Time_Before_First_Join = 0;

            // Fb case #1632
            stDTMF_MGC.PostLeaderPin = stDTMF_MGC.PreConfCode = stDTMF_MGC.PreLeaderPin = stDTMF_Codian.PostLeaderPin = stDTMF_Codian.PreConfCode = stDTMF_Codian.PreLeaderPin = null;
        }
    }

    class LdapSettings
    {

        internal string serverAddress, serverLogin, serverPassword, loginKey, searchFilter, domainPrefix, scheduleDays;
        internal int connPort, sessionTimeout, AuthenticationType;//FB 2993 LDAP
        internal DateTime scheduleTime, syncTime;

        internal LdapSettings()
        {
            serverAddress = serverLogin = serverPassword = searchFilter = loginKey = domainPrefix = null;
            connPort = 389;
            sessionTimeout = 20;
        }
    }

    class LDAP
    {

        internal string serverAddress, serverLogin, serverPassword, domainPrefix;
        internal int connPort, sessionTimeout, scheduleInterval;
        internal LDAP()
        {
            serverAddress = serverLogin = serverPassword = domainPrefix = null;
            connPort = 389;
            sessionTimeout = 20;
            scheduleInterval = 1440; // 1 day
        }
    }

    class LdapUser
    {
        internal int userid;
        internal string firstName, lastName, email, telephone, login;
        internal DateTime whenCreated;

        internal LdapUser()
        {
            userid = 0;
            firstName = lastName = email = telephone = login = null;
            whenCreated = DateTime.Now;
        }
    }

    class Alert
    {
        internal int confID;
        internal int instanceID;
        internal string message;
        internal DateTime timestamp;
        internal int typeID;

        internal Alert()
        {
            confID = instanceID = typeID = 0;
        }
    }

    class WorkOrder
    {
        internal enum eType { AUDIO_VISUAL, CATERING, HOUSEKEEPING, GENERIC };
        internal int iCategoryType, iAdminUserID, iConfID, iInstanceID, iRoomID;
        internal eType etType;
        internal string sName, sComments;
        internal DateTime dtCompletedBy;
        internal WorkOrder()
        {
        }
    }

    class ESSettings //FB 2363s
    {
        internal string sCustomerName, sCustomerID, sPartnerName, sPartnerEmail;
        internal string sPartnerURL, sUserName, sPassword;
        internal DateTime dModifiedDate;
        internal string sESType;
        internal Int32 sTimeout;

    }
    class sysExtSchedEvent 
    {
        internal string sRequestID, sTypeID, sCustomerID, sStatus;
        internal string sStatusMessage, sRequestCall, sResponseCall,sConfnumname;
        internal string sEventTrigger;
        internal DateTime dEventTime, dStatusDate;
        internal int sRetryCount, sOrgID;
	}
	//FB 2392 - Starts
    class timeZoneDetails
    {       
        internal int iTimeZoneID,iDST;
        internal double iOffset;
    }

    class SysSettings
    {
        internal string WhyGoURL, WhyGoUserName, WhyGoPassword, WhygoAdminEmail; //ZD 100694
        internal int WhygoUserID = 1333;
    }
	//FB 2392-End
    //ZD 103263 Start
    class BJNRoomDetails
    {
        internal string numericId, moderatorPasscode, name, userID;        
    }
    //ZD 103263 End
    class CDREvent
    {
        internal String sConferenceGUID, sParticipantGUID, sEventType, sConfTime, sConfName, sUri, sPinProtected;
        internal String sCallID, sCallDirection, sCallProtocol, sEndpointIPAddress, sEndpointDisplayName,
        sEndpointURI, sEndpointConfiguredName, sTimeInConference, sDisconnectReason,
        sMaxSimultaneousAVParticipants, sMaxSimultaneousAudioOnlyParticipants, sName, sActiveTime, sEncryptedTime,
        sTime, sMessage, sScheduledTime, //FB 2593
        //FB 2797 Start
        sbillingCode, sownername, sScheduleddate, sScheduleddurationinminutes, sDirection, sdn, sh323Alias,
        sEndpointDirection, sMediaEncryptionStatus, sMediaFromResolution, sMediaFromVideoCodec, sMediaFromaudioCodec,
        sMediaToresolution, sMediaTovideoCodec, sMediaToaudioCodec, sregisteredWithGatekeeper;
        //FB 2797 End
        internal DateTime dTime;
        internal int iBridgeId, iBridgeType,iNumericId, iMaxSimultaneousAudioVideoParticipants, iMaxSimultaneousAudioOnlyParticipants, iIndex,
        iTotalAudioVideoParticipants, iTotalAudioOnlyParticipants, iSessionDuration, iMaxBandwidth, iBandwidth, iPacketsReceived, iPacketsLost, iWidth, iHeight,
        iDuration, //FB 2593
         //FB 2797 Start
        iConfnumname, iuniqueId, iparticipantId, itimeInConferenceInMinutes, iMediaFrombandwidth, iMediaTobandwidth, iTotalstreamingParticipantsAllowed, imaxSimultaneousStreaming,
        iAudioVideoParticipants, iTimeInConference, iAudioOnlyParticipants, istreamingParticipantsAllowed, iduration, idurationInMinutes;
        //FB 2797 End
        internal Conference confDetails = null;
        internal enum EptConnectStatus { Connected = 1, Disconnected = 2 };

        public class VMRCDR
        {
            public string MCUConfGUID { get; set; }
            public string VMRAddress { get; set; }
            public string StartDate { get; set; }
            public string TotalPortMinActual { get; set; }
            public string TotalMinActual { get; set; }
            public string MCUid { get; set; }
            public string MCUname { get; set; }
            public string MCUtype { get; set; }
            public string MCUAddress { get; set; }
            public string EventId { get; set; }
            public string OrgId { get; set; }
            public string BridgeTypeID { get; set; } //FB 2593 VMR CDR
            public string IsVMRConf { get; set; } //ZD 101348
            public string IsPermanentConf { get; set; } //ZD 101348
            
        }

        internal CDREvent()
        {
            // empty all variables
            sConferenceGUID = " ";
            sParticipantGUID = " ";
            sEventType = " ";
            sConfTime = " ";
            sConfName = " ";
            sUri = " ";
            sPinProtected = " ";
            sCallID = " ";
            sCallDirection = " ";
            sCallProtocol = " ";
            sEndpointIPAddress = " ";
            sEndpointDisplayName = " ";
            sEndpointURI = " ";
            sEndpointConfiguredName = " ";
            sTimeInConference = " ";
            sDisconnectReason = " ";
            sMaxSimultaneousAVParticipants = " ";
            sMaxSimultaneousAudioOnlyParticipants = " ";
            sName = " ";
            sActiveTime = " ";
            sEncryptedTime = " ";
            sTime = " ";
            sMessage = " ";

            iBridgeId = 0;
            iBridgeType = 0;
            iNumericId = 0;
            iMaxSimultaneousAudioVideoParticipants = 0;
            iMaxSimultaneousAudioOnlyParticipants = 0;
            iIndex = 0;
            iTotalAudioVideoParticipants = 0;
            iTotalAudioOnlyParticipants = 0;
            iSessionDuration = 0;
            iMaxBandwidth = 0;
            iBandwidth = 0;
            iPacketsReceived = 0;
            iPacketsLost = 0;
            iWidth = 0;
            iHeight = 0;
        }

    }
    /**  FB 2599 - Start **/
    class VidyoSettings //FB 2262
    {
        internal string sProxyAddress, sPort;
        internal string sPartnerURL, sUserName, sPassword;
        internal DateTime dModifiedDate;
        internal enum callType { VidyoPortalAdminService, VidyoPortalUserService};
        internal String[] namespaceType = new String[2] { "http://portal.vidyo.com/admin/v1_1", "http://portal.vidyo.com/user/v1_1" };

    }
    /**  FB 2599 - End **/
    //FB 2441 Starts
    class ObjectHelper
    {
        public static T DeepClone<T>(T obj)
        {
            using (var ms = new MemoryStream())
            {
                var formatter = new BinaryFormatter();
                formatter.Serialize(ms, obj);
                ms.Position = 0;

                return (T)formatter.Deserialize(ms);
            }
        }

    }
	//FB 2441 End
    //FB 2556 Start
    public enum AccessMode
    {
        FromService = 1, FromWebsite = 0
    };
    //FB 2556 End

    //ZD 100221 Starts
    [Serializable]
    class Recurrence
    {
        internal int iconfid, iInstanceID, Iconfnumname, itimezoneID, iDuration, iRecurType, iSubType, iyearMonth, idayno, igap, iendType, ioccurrence, idirty;
        internal DateTime dtstartTime,dtendTime;
        internal string sDays,sRecurringPattern;
    }
    public enum RecurDayName
    {
        SUNDAY = 1, MONDAY, TUESDAY, WEDNESDAY, THURSDAY, FRIDAY,SATURDAY
    }
    //ZD 100221 Ends
    //ZD 101527 Starts
    class Endpoint
    {
        internal int uID, endpointId, protocol = 0, connectiontype = 0, addresstype = 0, deleted = 0, outsidenetwork = 0, videoequipmentid = 0, linerateid = 0, bridgeid = -1, profileId = 0, isDefault = 0, encrypted = 0;
        internal int MCUAddressType = -1, TelnetAPI = 0, orgId = 11, CalendarInvite = 0, ApiPortNo = 23, isTelePresence = 0, Extendpoint = 0, EptOnlineStatus = 0, PublicEndPoint = 0, Secureport = 0, Secured = 0, EptCurrentStatus = 0, ManufacturerID = 0;
        internal int LastModifiedUser = 11, ProfileType = 1, IsP2PDefault = 0, IsTestEquipment = 0, CreateCategory = 1;
        internal string name = "", password = "", address = "", endptURL = "", profileName = "", MCUAddress = "", ExchangeID = "", ConferenceCode = "", LeaderPin = "", MultiCodecAddress = "", RearSecCameraAddress = "";
        internal DateTime Lastmodifieddate;

    }
    class SyncEndpoint
    {
        internal int uID, endpointId, protocol = 0, connectiontype = 0, addresstype = 0, videoequipmentid = 0, profileId = 0, isDefault = 0, identifierValue = 0;
        internal int ProfileType = 1, orgId = 11, mcuID = 0, ManufacturerID = 0;
        internal string name = "", address = "", profileName = "";
    }
    class SyncRoom
    {
        internal int RoomId = 0, EndpointId = 0, OrgId = 0, EndpointCategory = 0, identifierValue=0;
        internal string RoomName = "", EndpointName = "";
    }
    //ZD 101527 Start
    public class RPRMEndpoints
    {
        public List<RPRMEndpointsProfile> EptProfile;
        public string EndpointName { get; set; }
        public string EndpointProfileName { get; set; }
        public bool h323 { get; set; }
        public bool isdn { get; set; }
        public bool sip { get; set; }
        public string address { get; set; }
        public string ProfileXML { get; set; }
        public int orgID { get; set; }
        public int identifier { get; set; }
        public int mcuID { get; set; }
        public int ManufacturerID { get; set; }
        public string ModelType { get; set; }
    }

    public class RPRMEndpointsProfile
    {
        public string EndpointProfileName { get; set; }
        public int aliastype { get; set; }
        public string address { get; set; }
    }
    //ZD 101527 Ends
	//ZD 102198 Start
    public class StringHelper
    {
        #region GetString Between

        public string GetStringInBetween(string strBegin, string strEnd, string strSource, bool includeBegin, bool includeEnd)
        {
            string result = "";
            try
            {

                int iIndexOfBegin = strSource.IndexOf(strBegin);
                if (iIndexOfBegin != -1)
                {

                    if (includeBegin)
                        iIndexOfBegin -= strBegin.Length;
                    strSource = strSource.Substring(iIndexOfBegin
                        + strBegin.Length);
                    int iEnd = strSource.IndexOf(strEnd);
                    if (iEnd != -1)
                    {
                        if (includeEnd)
                            iEnd += strEnd.Length;
                        result = strSource.Substring(0, iEnd);

                    }
                }
                else
                    result = strSource;
            }
            catch (Exception ex)
            {
                result = "";
            }
            return result;
        }

        #endregion
    }
	//ZD 102198 End
    //ZD 103184 Starts
    public class User
    {
        public string ParticipantGUID { get; set; }
        public int Status { get; set; }
        public string IPISDNAddress { get; set; }
        public string DialAddress { get; set; }
    }
    //ZD 103184 Ends

    //ALLDEV-842 Starts
    class EM7Details
    {
        internal int OrgId = 0, EM7OrgID = 0;
        internal string EM7URL = "", EM7Username = "", EM7Password = "", EM7Port = "";
    }
    //ALLDEV-842 Ends

    //ALLDEV-782 Starts
    public class ActiveUser
    {
        internal int OrgId = 0, UserID = 0;
        internal string Email = "", PexipVMRAlias = "", userName = "", PexipALiasURI = "";
    }
     
    //ALLDEV-782 Ends
}
namespace NS_MESSENGER_CUSTOMIZATIONS
{
    using System;
    using System.Collections;
    class Room
    {
        internal int roomID;
        internal int tier2ID;
        internal int tier1ID;
        internal string name;
        internal string filePath;
        internal string topTierName, timezoneID_Name;
        //2329 start -WhyGo 
        internal Decimal EHCost, AHCost, OHCost, genericSellPrice, CHCost;
        internal int WhygoRoomId, timezoneID, videoAvailable, Capacity;
        internal int isCrazyHoursSupported,IsEarlyHoursEnabled, IsAfterHourEnabled, isAutomatic, isHDCapable, isInternetCapable;
        internal int isVCCapable, isTP, isIPDedicated, isIPCConnectionCapable, isIPCapable, isInternetFree, isISDNCapable;
        internal int Is24HoursEnabled, Open24Cost, speed;
        internal int isDST;
        internal string RoomDescription, ExtraNotes, Country, State, AssitantEmail, myVRMID; //ZD 100888
        internal string CountryName, StateName ;
        internal string ipAddress, isdnAddress, IPSpeed, ISDNSpeed;
        internal string Type,Zipcode, Address1, City;
        internal string extraNotes, RoomPhone, Maplink;
        internal string internetBiller, geoCodeAddress, CateringOptionsAvailable, AUXEquipment, CurrencyType, URL;
        internal string internetPriceCurrency;
        internal string CHtartTime, CHEndTime,  EHStartTime, EHEndTime, AHStartTime, AHEndTime, openHours, OHStartTime, OHEndTime;
        internal string latitude, longitude,MiddleTierName;
        internal DateTime PublicRoomLastModified;
        internal double timezoneOffset;
        internal int AHFullyAuto, CHFullyAuto, EHFullyAuto; //FB 2543
        //2329 end -WhyGo
        internal Queue confList;
        internal Room()
        {
            confList = new Queue();
            timezoneID = 26;
        }
    }

    class tierInfo
    {
        internal int l3LocationId, l2LocationId;
        internal String l3LocationName, l2LocationName;

        internal tierInfo()
        {
            l2LocationName = l3LocationName = "";
        }
    }

    class Conference
    {
        internal DateTime startDate;
        internal DateTime startTime;
        internal DateTime endTime;
        internal int uniqueID;
        internal int confID;
        internal int instanceID;
        internal string name;
        internal string description;
        internal Queue roomList;
        internal Queue cateringOrders;
        internal Queue resourceOrders;
        internal Queue participantList;
        internal Queue locationList;
        internal Conference()
        {
            roomList = new Queue();
            cateringOrders = new Queue();
            resourceOrders = new Queue();
            participantList = new Queue();
            locationList = new Queue();
        }
    }

    class GenericRoom //FB 2329
    {
        
        internal string companyName;
        internal string contactName, contactPhone,contactEmail;
        internal string transmissionType, transmissionSpeed, transmissionNumber;
        internal int timeZoneId, numberOfAttendees ;
        internal string attendeeName, attendeePhone, attendeeEmail, ipOrISDN;
        internal string bridgeDetail, extraNotes, speed, City;
        internal int[] catering, equipment;
        internal GenericRoom()
        {
        }
    }



    class Tier2
    {
        internal string name;
        internal int id;
        internal int timezoneId;
        internal string filePath, timezoneId_Name;
    }


}


namespace NS_PERSISTENCE
{
    #region references
    using System;
    using System.Collections;
    #endregion

    class Conference
    {

        internal enum eType { FUTURE_VIDEO_CONF, ROOM_CONF, IMMED_CONF, POINT_TO_POINT, TEMPLATE_CONF };

        internal int iConfID, iInstanceID, iDurationMin, iOwnerID, iTimezoneID;
        internal int iRecur_RecurType, iRecur_DailyType, iRecur_DayGap, iRecur_EndType, iRecur_Occurrence;
        internal DateTime dtStartDateForNonRecurConf, dtStartTime, dtStartDateTime;
        internal eType etType;
        internal bool isImmediate, isPublic, isOpenForRegistration, isRecurring;
        internal string sConfName, sDescription, sPassword;
        internal int iVideoLayout, iVideoSessionID, iManualVideoLayout, iLectureMode, iLineRateID, iAudioalgorithmID, iVideoProtocolID, iConfType, iMediaID;
        internal Queue roomList;
        internal Queue partyList;

        internal Conference()
        {
            iConfID = iInstanceID = 0;
            isImmediate = isPublic = isOpenForRegistration = isRecurring = false;
            sDescription = sPassword = "";
            roomList = new Queue();
            partyList = new Queue();
            iVideoLayout = iVideoSessionID = iManualVideoLayout = iLectureMode = iLineRateID = iAudioalgorithmID = iVideoProtocolID = iConfType = iMediaID = 0;
            etType = 0;
            iRecur_RecurType = iRecur_DailyType = iRecur_DayGap = iRecur_EndType = iRecur_Occurrence = 0;
            dtStartDateForNonRecurConf = dtStartTime = dtStartDateTime = DateTime.Now;
        }
    }
    class Room
    {
        internal int roomID;
        internal string name;

        internal Room()
        {
            roomID = 0;
        }
    }

    class Party
    {
        internal int userID;
        internal string name, email;

        internal Party()
        {
            userID = 0;
            name = email = null;
        }
    }

    class DiffChanges
    {
        internal int eventType, userID, confID, instanceID, userTimezoneID;
        internal string message, userName, userEmail, userTimezone;
        internal DateTime timestamp;

        internal DiffChanges()
        {
            userTimezone = null;
            userTimezoneID = 26;
        }
    }
}
