<%--ZD 100147 start--%>
<%--/* Copyright (C) 2015 myVRM - All Rights Reserved	
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.		
*
* You should have received a copy of the myVRM license with	
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/--%><%--ZD 100147 ZD 100886 End--%>
<%@ Page Language="C#" AutoEventWireup="true" Inherits="OrganisationSettings" %>

<%@ Register TagPrefix="cc1" Namespace="myVRMWebControls" Assembly="myVRMWebControls" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajax" %>
<%@ Register Assembly="MetaBuilders.WebControls.ComboBox" Namespace="MetaBuilders.WebControls"
    TagPrefix="mbcbb" %>
<%--added for FB 1710 start--%>
<%@ Register Assembly="DevExpress.SpellChecker.v10.2.Core, Version=10.2.11.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.XtraSpellChecker" TagPrefix="dxXSC" %>
<%@ Register Assembly="DevExpress.Web.ASPxSpellChecker.v10.2, Version=10.2.11.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxSpellChecker" TagPrefix="dxSC" %>
<%@ Register Assembly="DevExpress.Web.ASPxHtmlEditor.v10.2, Version=10.2.11.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxHtmlEditor" TagPrefix="dxHE" %>
<%@ Register Assembly="DevExpress.Web.ASPxEditors.v10.2, Version=10.2.11.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dxE" %>
<%@ Register Assembly="DevExpress.Web.v10.2, Version=10.2.11.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxPanel" TagPrefix="dxP" %>
<%@ Register Assembly="DevExpress.Web.v10.2, Version=10.2.11.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxRoundPanel" TagPrefix="dxRP" %>
    <%@ Register Assembly="DevExpress.Web.v10.2, Version=10.2.11.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGlobalEvents" TagPrefix="dx" %>
<%--added for FB 1710 end--%>
<%--FB 2779 Starts--%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<meta http-equiv="X-UA-Compatible" content="IE=8" />
<!-- #INCLUDE FILE="inc/maintopNET.aspx" -->
<%--FB 2779 Ends--%>

<script type="text/javascript" language="JavaScript" src="inc/functions.js">
</script> <%-- ZD 102723 --%>

<script type="text/javascript" src="script/calview.js"></script>

<script type="text/javascript" language="javascript" src='script/lib.js'></script>

<script type="text/javascript" src="inc/functions.js"></script>

<style type="text/css" >
    .iemask
    {
        -ms-filter:"progid:DXImageTransform.Microsoft.Alpha(Opacity=50)";
    }
</style>

<script language="JavaScript">
<!--
//if (navigator.userAgent.indexOf('Trident') > -1) {
    document.onkeydown = function (evt) {
        evt = evt || window.event;
        var keyCode = evt.keyCode;
        if (keyCode == 27) {
            if (document.getElementById("divFlrPlanList").style.display == "block") {
                var str = document.activeElement.type;
                if (!(str == "text" || str == "textarea" || str == "password")) {
                    document.getElementById("btnConfArchiveClose").click();
                    return false;
                }
            }
        }
        fnOnKeyDown(evt);
    };
//}
//ZD 100604 start
var img = new Image();
img.src = "../en/image/wait1.gif";
//ZD 100604 End
//FB Case 807 starts here
function deleteApprover(id)
{
	eval("document.getElementById('hdnApprover" + (id+1) + "')").value = "";
	eval("document.getElementById('txtApprover" + (id+1) + "')").value = "";
}
//FB 2599 Start
function PreservePassword()// FB 2262
{
    document.getElementById("hdnVidyoPassword").value = document.getElementById("txtvidyoPassword1").value; //FB 2363
    document.getElementById("hdnLDAPPassword").value = document.getElementById("txtLDAPAccountPassword2").value; //ZD 101443
}
//FB 2599 End

function getYourOwnEmailList (i)
{
    if (i == -2)//Login Management
    {
//        url = "dispatcher/conferencedispatcher.asp?frm=roomassist&frmname=frmMainroom&cmd=GetEmailList&emailListPage=1&wintype=pop";
      if(queryField("sb") > 0 )
            url = "emaillist2.aspx?t=e&frm=approverNET&wintype=ifr&fn=frmMainsuperadministrator&n=";
            else
            url = "emaillist2main.aspx?t=e&frm=approverNET&fn=frmMainsuperadministrator&n=";
    }
    else
    {
//        url = "dispatcher/conferencedispatcher.asp?frm=approver&frmname=frmMainroom&no=" + i + "&cmd=GetEmailList&emailListPage=1&wintype=pop";
        url = "emaillist2main.aspx?t=e&frm=approverNET&fn=frmMainsuperadministrator&n=" + i;
	}
	//url = "dispatcher/conferencedispatcher.asp?frm=approverNET&frmname=frmMainsuperadministrator&no=" + i + "&cmd=GetEmailList&emailListPage=1&wintype=pop";
	if (!window.winrtc) {	// has not yet been defined
	    winrtc = window.open(url, "", "width=920,height=470,top=0,left=0,resizable=yes,scrollbars=yes,status=no"); //FB 2735
		winrtc.focus();
	} else // has been defined
	    if (!winrtc.closed) {     // still open
	    	winrtc.close();
	    	winrtc = window.open(url, "", "width=920,height=470,top=0,left=0,resizable=yes,scrollbars=yes,status=no"); //FB 2735
			winrtc.focus();
		} else {
		winrtc = window.open(url, "", "width=920,height=470,top=0,left=0,resizable=yes,scrollbars=yes,status=no"); //FB 2735
	        winrtc.focus();
		}
				
}


 //code added for custom attribute fixes

function OpenEntityCode()
{
    //window.location.replace("ViewCustomAttributes.aspx"); //FB 2565
    DataLoading(1); // ZD 100176
    window.location = 'ViewCustomAttributes.aspx';
}
// Added for FB 1758
function fnReset() {  
		//ZD 100420  
        document.getElementById("txtTestEmailId").value = "";
        document.getElementById("regTestemail").innerText = "";
        document.getElementById("reqvalid").innerText = "";
		//ZD 100420
        document.getElementById("reqvalid2").innerText = "";
        DataLoading(1); //ZD 100176
    }
//FB 1849    
function fnChangeOrganization()
{
    var btnchng = document.getElementById("BtnChangeOrganization");
    var drporg = document.getElementById("DrpOrganization");
    var cnfrm = confirm('<%=fnGetTranslatedText("Hereafter all the transactions performed in system will be for the selected organization.Do you wish to continue") %>');
    
    if(cnfrm)
        return true;
    else
        return false;
   
}
//FB 2052
function OpenDayColor()
{
    //window.location.replace("HolidayDetails.aspx"); //FB 2565
    DataLoading(1); // ZD 100176
    window.location='HolidayDetails.aspx';
}
//-->
//FB 2343
function WorkingDayDetails()
{
    //window.location.replace("WorkingDays.aspx"); //FB 2565
    DataLoading(1); // ZD 100176
    window.location = 'WorkingDays.aspx';
}
//FB 2486
function OpenManageMsg()
{
    //window.location.replace("ManageMessages.aspx"); //FB 2565
    DataLoading(1); // ZD 100176
    window.location = 'ManageMessages.aspx';
}
//FB 2410
function OpenBatchReport()
{
    DataLoading(1); // ZD 100176
    window.location = 'ManageBatchReport.aspx';
}
// ZD 100456
function OpenDataImport() {
    DataLoading(1);
    window.location = 'ManageDataImport.aspx';
}

//FB 3054 Starts
//ZD 101443 start
function PasswordChange(par, par1) {
    if (par == 1)
    {
      document.getElementById("hdnPasschange").value = true;
       if(par1=1)
            document.getElementById("hdnPW1Visit").value = true;
       else
            document.getElementById("hdnPW2Visit").value = true;
   }
   else if (par == 2) {

       document.getElementById("hdnLDAPPW").value = true;
       if (par1 == 1)
           document.getElementById("hdnLDAP1Visit").value = true;
       else
           document.getElementById("hdnLDAP2Visit").value = true;
   }
   else if(par == 3)//ALLDEV-833
   {
       document.getElementById("hdnEWSPassChange").value = true;
       if (par1 == 3)
           document.getElementById("hdnEWSPwdVisit").value = true;
   }


}
function fnTextFocus(xid, par, par2) {
     var obj = document.getElementById(xid);
    // ZD 100263 Starts
	//ALLDEV-833 - Start
     if (par2 == 1)
     {
        var obj1 = document.getElementById("txtvidyoPassword1");
        var obj2 = document.getElementById("txtvidyoPassword2");
        if (obj1.value == "" && obj2.value == "")
        {
            document.getElementById("txtvidyoPassword1").style.backgroundImage = "";
            document.getElementById("txtvidyoPassword2").style.backgroundImage = "";
            document.getElementById("txtvidyoPassword1").value = "";
            document.getElementById("txtvidyoPassword2").value = "";
        }
    return false;
    // ZD 100263 Ends
    if (par == 1)
    {
        if (document.getElementById("hdnPW2Visit") != null)
        {
            if (document.getElementById("hdnPW2Visit").value == "false")
            {
                    document.getElementById("txtvidyoPassword1").value = "";
                    document.getElementById("hdnVidyoPassword").value = "";
                    document.getElementById("txtvidyoPassword2").value = "";
            }
            else
            {
                    document.getElementById("txtvidyoPassword1").value = "";
                    document.getElementById("hdnVidyoPassword").value = "";
            }
        }
        else
        {
                document.getElementById("txtvidyoPassword1").value = "";
                document.getElementById("hdnVidyoPassword").value = "";
                document.getElementById("txtvidyoPassword2").value = "";
        }
     }
     else
     {
        if (document.getElementById("hdnPW1Visit") != null)
        {
            if (document.getElementById("hdnPW1Visit").value == "false")
            {
                    document.getElementById("txtvidyoPassword1").value = "";
                    document.getElementById("hdnVidyoPassword").value = "";
                    document.getElementById("txtvidyoPassword2").value = "";
            }
            else
            {
                    document.getElementById("txtvidyoPassword2").value = "";
                    document.getElementById("hdnVidyoPassword").value = "";
            }

         }
        else
        {
                document.getElementById("txtvidyoPassword2").value = "";
                document.getElementById("hdnVidyoPassword").value = "";
        }
    }
    
  }
     else if (par2 == 2)
     {
        // ZD 100263 Starts
        var obj1 = document.getElementById("txtLDAPAccountPassword1");
        var obj2 = document.getElementById("txtLDAPAccountPassword2");
        if (obj1.value == "" && obj2.value == "") {
            document.getElementById("txtLDAPAccountPassword1").style.backgroundImage = "";
            document.getElementById("txtLDAPAccountPassword2").style.backgroundImage = "";
            document.getElementById("txtLDAPAccountPassword1").value = "";
            document.getElementById("txtLDAPAccountPassword2").value = "";
        }
        return false;
        // ZD 100263 Ends
        if (par == 1) {

            if (document.getElementById("hdnLDAP2Visit") != null) {
                if (document.getElementById("hdnLDAP2Visit").value == "false") {
                    document.getElementById("txtLDAPAccountPassword1").value = "";
                    document.getElementById("txtLDAPAccountPassword1").value = "";
                } else {
                    document.getElementById("txtLDAPAccountPassword1").value = "";
                }
            }
            else {
                document.getElementById("txtLDAPAccountPassword1").value = "";
                document.getElementById("txtLDAPAccountPassword2").value = "";
            }
        } else {
            if (document.getElementById("hdnLDAP1Visit") != null) {
                if (document.getElementById("hdnLDAP1Visit").value == "false") {
                    document.getElementById("txtLDAPAccountPassword1").value = "";
                    document.getElementById("txtLDAPAccountPassword2").value = "";
                } else {
                    document.getElementById("txtLDAPAccountPassword2").value = "";
                }
            }
            else {
                document.getElementById("txtLDAPAccountPassword1").value = "";
                document.getElementById("txtLDAPAccountPassword2").value = "";
            }
        }

     }
     else if (par2 == 3)
     {
         var obj1 = document.getElementById("txtEWSPassword");         
         if (obj1.value == "") {
             document.getElementById("txtEWSPassword").style.backgroundImage = "";
             document.getElementById("txtEWSPassword").value = "";             
         }
         return false;
         if(par == 1)
         {
             if (document.getElementById("hdnEWSPwdVisit") != null)
             {
                 document.getElementById("txtEWSPassword").value = "";
             }
         }

     }
	//ALLDEV-833 - End
    if (document.getElementById("CompareValidator2") != null) {
        ValidatorEnable(document.getElementById('CompareValidator2'), false);
        ValidatorEnable(document.getElementById('CompareValidator2'), true);
    }
} //ZD 101443 End
//FB 3054 Ends
//ZD 100176 start
function DataLoading(val) {
    if (val == "1")
        document.getElementById("dataLoadingDIV").style.display = 'block'; //ZD 100678
    else
        document.getElementById("dataLoadingDIV").style.display = 'none'; //ZD 100678
}
//ZD 100176 End
//ZD 101443 start
function LDAPTC() {
    document.getElementById("btnTestLDAPConnection").disabled = "true";
    document.getElementById("btnTestLDAPConnection").className = "btndisable";
}

//ZD 101443 End
</script>

<script type="text/javascript" src="script/approverdetails.js"> <%-- ZD 102723 --%>

</script>

<html>
<body>
   <div id="topDiv1" style="width:1600px; height:1400px; z-index: 10000px; position:absolute; overlap: left:0px; top:0px; background-color:White;";></div>  <%--FB 2738--%>
    <%--UI Changes for FB 1849--%>
    <form name="frmOrgSettings" id="frmOrgSettings" method="Post" action="OrganisationSettings.aspx"
    language="JavaScript" runat="server">
     <%--ZD 101022 start--%>
        <asp:ScriptManager ID="scpMgrUI" runat="server" EnableScriptLocalization="true" >
		    <Scripts>                
			    <asp:ScriptReference Path= "~/ResourceScript/StringResources.js" ResourceUICultures="<%$ Resources:WebResources, UICulture%>"  />
		    </Scripts>
	    </asp:ScriptManager> <%--ZD 101022 End--%>
    <%--FB Icon--%>
   <%-- <asp:ScriptManager ID="CalendarScriptManager" runat="server" AsyncPostBackTimeout="600">
    </asp:ScriptManager>--%>
    <%--FB 1849--%>
    <center>
        <input type="hidden" id="helpPage" value="92">
        <input type="hidden" id="hdnVidyoPassword" runat="server" /> <%--FB 2262 //FB 2599--%>
        <input type="hidden" id="hdnMailServer" runat="server" />
        <input type="hidden" id="hdnLDAPPassword" runat="server" />
        <input type="hidden" id="hdnLDAP1Visit" value="false" runat="server" /> <%--ZD 101443--%>
        <input type="hidden" id="hdnLDAP2Visit" value="false" runat="server" /> <%--ZD 101443--%>
        <input type="hidden" id="hdnLDAPPW" runat="server" /><%--ZD 101443--%>
        <%--<input type="hidden" id="hdnFooterMsg" runat="server" />--%><%--FB 2681--%><%--FB 3011--%>
        <input type="hidden" id="hdnPasschange" runat="server" /> <%--FB 3054--%>
          <input type="hidden" id="hdnPW1Visit" value="false" runat="server"/> <%--FB 3054--%>
       <input type="hidden" id="hdnPW2Visit" value="false" runat="server"/> <%--FB 3054--%>
        <input type="hidden" id="hdnEWSPassChange" runat="server" /><%--ALLDEV-833--%>
        <input type="hidden" id="hdnEWSPwdVisit" value="false" runat="server"/><%--ALLDEV-833--%>
        <h3>
            <asp:Literal Text="<%$ Resources:WebResources, OrganisationSettings_OrganizationSe%>" runat="server" /></h3>
        <asp:Label ID="errLabel" runat="server" Text="" CssClass="lblError"></asp:Label><br />
    </center>
    <div id="dataLoadingDIV" name="dataLoadingDIV" style="display:none" align="center" >
          <img border='0' src='image/wait1.gif'  alt='<asp:Literal ID="Literal1" Text='<%$ Resources:WebResources, Loading%>' runat='server' />' />
    </div> <%--ZD 100678 End--%>
    <%--FB 1710 Alignment change start--%>
    <table width="100%" border="0">
    <tr><%--FB 2681--%>
        <td>
            <%--FB 1849--%>
            <%--FB 1982--%>
            <table width="100%" border="0">
                <tr>
                    <td>
                        <table width="100%">
                            <%--FB 1849--%>
                            <tr>
                                <td>
                                    <table width="100%" border="0">
                                        <tr valign="top" id="trSwt" runat="server">
                                            <td colspan="2" align="right" valign="top" style="display: none">
                                                <a id="ChgOrg" runat="server" href="#" class="blueblodtext"><asp:Literal Text="<%$ Resources:WebResources, OrganisationSettings_ChgOrg%>" runat="server"></asp:Literal></a>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td width="40%" valign="top">
                                                <table width="100%">
                                                    <%--FB 1982--%>
                                                    <%--TD Width Updated for FB 2050--%>
                                                    <tr>
                                                        <td align="left" class="subtitleblueblodtext" colspan="5" style="height: 21; font-weight: bold">
                                                            <asp:Literal Text="<%$ Resources:WebResources, OrganisationSettings_OrganizationRe%>" runat="server" />
                                                            <input style="border:none;" type="text" onfocus="document.getElementById('Button1').focus();" />
                                                        </td>
                                                        <%--CSS Project--%>
                                                    </tr>
                                                    <tr><%--FB 2579 Start--%>
                                                        <td align="right" height="21" style="font-weight: bold; width: 5%">
                                                        </td>
                                                        <td style="width: 45%" align="left" valign="top" class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, OrganisationSettings_VideoRooms%>" runat="server"></asp:Literal></td>
                                                        <!-- FB 2050 -->
                                                        <td style="width: 50%" align="left" valign="top" colspan="3">
                                                            <!-- FB 2050 -->
                                                            <asp:Label ID="LblActRooms" runat="server" ForeColor="DarkGray" BorderColor="#cccccc"
                                                                BorderStyle="Solid" BorderWidth="1px" Width="40px" Style="text-align: right"></asp:Label>
                                                        </td>
                                                    </tr>
                                                     <tr>
                                                        <td align="right" height="21" style="font-weight: bold">
                                                        </td>
                                                        <td align="left" valign="top" class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, OrganisationSettings_EndPoints%>" runat="server"></asp:Literal></td>
                                                        <td align="left" class="altblackblodttext" valign="top" colspan="3">
                                                            <asp:Label ID="LblEpts" runat="server" ForeColor="DarkGray" BorderColor="#cccccc"
                                                                BorderStyle="Solid" BorderWidth="1px" Width="40px" Style="text-align: right"></asp:Label>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td align="right" height="21" style="font-weight: bold">
                                                        </td>
                                                        <td align="left" valign="top" class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, OrganisationSettings_NonVideoRooms%>" runat="server"></asp:Literal></td>
                                                        <td align="left" valign="top" colspan="3">
                                                            <asp:Label ID="LblNonVidRooms" runat="server" ForeColor="DarkGray" BorderColor="#cccccc"
                                                                BorderStyle="Solid" BorderWidth="1px" Width="40px" Style="text-align: right"></asp:Label>
                                                        </td>
                                                    </tr>
                                                    <%--FB 2586 START--%>
                                                     <tr>
                                                        <td align="left" height="21" style="font-weight: bold">
                                                        </td>
                                                        <td align="left" valign="top" class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, OrganisationSettings_VMRRooms%>" runat="server"></asp:Literal></td>
                                                        <td align="left" valign="top" colspan="3">
                                                            <asp:Label ID="LblVMRRooms" runat="server" ForeColor="DarkGray" BorderColor="#cccccc"
                                                                BorderStyle="Solid" BorderWidth="1px" Width="40px" Style="text-align: right"></asp:Label>
                                                        </td>
                                                    </tr>
                                                    <%--FB 2586 END--%>

                                                    <%--ZD 101098 START--%>
                                                     <tr>
                                                        <td align="left" height="21" style="font-weight: bold">
                                                        </td>
                                                        <td align="left" valign="top" class="blackblodtext">
                                                            <asp:Literal Text="<%$ Resources:WebResources, DefaultLicense_iControlRooms%>" runat="server"></asp:Literal></td>
                                                        <td align="left" valign="top" colspan="3">
                                                            <asp:Label ID="LbliControlRoom" runat="server" ForeColor="DarkGray" BorderColor="#cccccc"
                                                                BorderStyle="Solid" BorderWidth="1px" Width="40px" Style="text-align: right"></asp:Label>
                                                        </td>
                                                    </tr>
                                                    <%--ZD 101098 END--%>
                                                    
                                                    <tr>
                                                        <td align="right" height="21" style="font-weight: bold">
                                                        </td>
                                                        <td align="left" valign="top" class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, OrganisationSettings_GuestRooms%>" runat="server"></asp:Literal></td>
                                                        <td align="left" class="altblackblodttext" valign="top" colspan="3">
                                                            <asp:Label ID="LblGstRooms" runat="server" ForeColor="DarkGray" BorderColor="#cccccc"
                                                                BorderStyle="Solid" BorderWidth="1px" Width="40px" Style="text-align: right"></asp:Label>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td align="right" height="21" style="font-weight: bold">
                                                        </td>
                                                        <td align="left" valign="top" class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, OrganisationSettings_StandardMCU%>" runat="server"></asp:Literal></td>
                                                        <td align="left" class="altblackblodttext" valign="top" colspan="3">
                                                            <asp:Label ID="LblActMCU" runat="server" ForeColor="DarkGray" BorderColor="#cccccc"
                                                                BorderStyle="Solid" BorderWidth="1px" Width="40px" Style="text-align: right"></asp:Label>
                                                        </td>
                                                        </tr>
                                                        <%--FB 2486 Start--%>
                                                        <tr>
                                                        <td align="right" height="21" style="font-weight: bold">
                                                        </td>
                                                        <td align="left" valign="top" class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, OrganisationSettings_EnhancedMCU%>" runat="server"></asp:Literal></td>
                                                        <td align="left" class="altblackblodttext" valign="top" colspan="3">
                                                            <asp:Label ID="LblEnchancedMCU" runat="server" ForeColor="DarkGray" BorderColor="#cccccc"
                                                                BorderStyle="Solid" BorderWidth="1px" Width="40px" Style="text-align: right"></asp:Label>
                                                        </td>
                                                        </tr>
                                                        <%--FB 2486 End--%>
													<%--FB 2694 START--%>
                                                     <tr>
                                                        <td align="left" height="21" style="font-weight: bold">
                                                        </td>
                                                        <td align="left" valign="top" class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, OrganisationSettings_HotdeskingVide%>" runat="server"></asp:Literal></td>
                                                        <td align="left" valign="top" colspan="3">
                                                            <asp:Label ID="LblHDVRooms" runat="server" ForeColor="DarkGray" BorderColor="#cccccc"
                                                                BorderStyle="Solid" BorderWidth="1px" Width="40px" Style="text-align: right"></asp:Label>
                                                        </td>
                                                    </tr>
                                                     <tr>
                                                        <td align="left" height="21" style="font-weight: bold">
                                                        </td>
                                                        <td align="left" valign="top" class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, OrganisationSettings_HotdeskingNonV%>" runat="server"></asp:Literal></td>
                                                        <td align="left" valign="top" colspan="3">
                                                            <asp:Label ID="LblHDNVRooms" runat="server" ForeColor="DarkGray" BorderColor="#cccccc"
                                                                BorderStyle="Solid" BorderWidth="1px" Width="40px" Style="text-align: right"></asp:Label>
                                                        </td>
                                                    </tr>
                                                    <%--FB 2694 END--%>
                                                    <tr>
                                                        <td align="right" height="21" style="font-weight: bold">
                                                        </td>
                                                        <td align="left" valign="top" class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, OrganisationSettings_Users%>" runat="server"></asp:Literal></td>
                                                        <td align="left" class="altblackblodttext" valign="top" colspan="3" nowrap="nowrap">
                                                            <asp:Label ID="LblActUsers" runat="server" ForeColor="DarkGray" BorderColor="#cccccc"
                                                                BorderStyle="Solid" BorderWidth="1px" Width="40px" Style="text-align: right"></asp:Label><span
                                                                    id="SpanActiveUsers" runat="server" ></span>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td align="right" height="21" style="font-weight: bold">
                                                        </td>
                                                        <td align="left" valign="top" class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, OrganisationSettings_OutlookUsers%>" runat="server"></asp:Literal></td>
                                                        <%--FB 2098--%>
                                                        <td align="left" class="altblackblodttext" valign="top" colspan="3">
                                                            <asp:Label ID="LblExchUser" runat="server" ForeColor="DarkGray" BorderColor="#cccccc"
                                                                BorderStyle="Solid" BorderWidth="1px" Width="40px" Style="text-align: right"></asp:Label>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td align="right" height="21" style="font-weight: bold">
                                                        </td>
                                                        <td align="left" valign="top" class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, OrganisationSettings_NotesUsers%>" runat="server"></asp:Literal></td>
                                                        <%--FB 2098--%>
                                                        <td align="left" class="altblackblodttext" valign="top" colspan="3">
                                                            <asp:Label ID="LblDuser" runat="server" ForeColor="DarkGray" BorderColor="#cccccc"
                                                                BorderStyle="Solid" BorderWidth="1px" Width="40px" Style="text-align: right"></asp:Label>
                                                        </td>
                                                    </tr>
                                                    
                                                    <tr>
                                                        <%--FB 1979--%>
                                                        <td align="right" height="21" style="font-weight: bold">
                                                        </td>
                                                        <td align="left" valign="top" class="blackblodtext">
                                                            <asp:Literal Text="<%$ Resources:WebResources, OrganisationSettings_MobileUsers%>" runat="server" />
                                                        </td>
                                                        <td align="left" class="altblackblodttext" valign="top" colspan="3">
                                                            <asp:Label ID="LblMobUser" runat="server" ForeColor="DarkGray" BorderColor="#cccccc"
                                                                BorderStyle="Solid" BorderWidth="1px" Width="40px" Style="text-align: right"></asp:Label>
                                                        </td>
                                                    </tr>
                                                     <%--ZD 100221 Start--%>
                                                     <tr>
                                                        <td align="right" height="21" style="font-weight: bold">
                                                        </td>
                                                        <td align="left" valign="top" class="blackblodtext">
                                                            <asp:Literal Text='<%$ Resources:WebResources, WebExUsers%>' runat='server' />
                                                        </td>
                                                        <td align="left" class="altblackblodttext" valign="top" colspan="3">
                                                            <asp:Label ID="LblWebexUser" runat="server" ForeColor="DarkGray" BorderColor="#cccccc"
                                                                BorderStyle="Solid" BorderWidth="1px" Width="40px" Style="text-align: right"></asp:Label>
                                                        </td>
                                                    </tr>
                                                     <%--ZD 100221 End--%>
                                                    <tr>
                                                        <%--FB 2693--%>
                                                        <td align="right" height="21" style="font-weight: bold">
                                                        </td>
                                                        <td align="left" valign="top" class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, OrganisationSettings_PCUsers%>" runat="server"></asp:Literal></td>
                                                        <td align="left" class="altblackblodttext" valign="top" colspan="3">
                                                            <asp:Label ID="LblPCUser" runat="server" ForeColor="DarkGray" BorderColor="#cccccc"
                                                                BorderStyle="Solid" BorderWidth="1px" Width="40px" Style="text-align: right"></asp:Label>
                                                        </td>
                                                    </tr>
                                                    <%--FB 2426 Start--%>
                                                    
                                                    <tr>
                                                        <td align="right" height="21" style="font-weight: bold">
                                                        </td>
                                                        <td align="left" valign="top" class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, OrganisationSettings_GuestRoomsPer%>" runat="server"></asp:Literal></td>
                                                        <td align="left" class="altblackblodttext" valign="top" colspan="3">
                                                            <asp:Label ID="LblGstRoomsPerUser" runat="server" ForeColor="DarkGray" BorderColor="#cccccc"
                                                                BorderStyle="Solid" BorderWidth="1px" Width="40px" Style="text-align: right"></asp:Label>
                                                        </td>
                                                    </tr>
                                                    
                                                    <%--FB 2426 End--%>
                                                    <%--ZD 104021 Starts--%>
                                                    <tr>
                                                        <td align="right" height="21" style="font-weight: bold">
                                                        </td>
                                                        <td align="left" valign="top" class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, ManageOrganizationProfile_BJNRoomPer%>" runat="server"></asp:Literal></td>
                                                        <td align="left" class="altblackblodttext" valign="top" colspan="3">
                                                            <asp:Label ID="LblBJNPerUser" runat="server" ForeColor="DarkGray" BorderColor="#cccccc"
                                                                BorderStyle="Solid" BorderWidth="1px" Width="40px" Style="text-align: right"></asp:Label>
                                                                <span id="SpanBJNUsers" runat="server" ></span> <%--ZD  104116--%>
                                                        </td>
                                                    </tr>
                                                    <%--ZD 104021 End--%>
                                                    <%--ZD 101443 Starts--%>
                                                    <tr>
                                                        <td align="right" height="21" style="font-weight: bold">
                                                        </td>
                                                        <td align="left" valign="top" class="blackblodtext"><asp:Literal ID="LiteralAD" Text="<%$ Resources:WebResources, OrganisationSettings_ActiveDirectory%>" runat="server"></asp:Literal></td>
                                                        <td align="left" class="altblackblodttext" valign="top" colspan="3">
                                                            <asp:Label id="LblActDirectory" runat="server" ></asp:Label>
                                                        </td>
                                                    </tr>
                                                    <%--ZD 101443 End--%>
                                                    <tr>
                                                        <td align="right" height="21" style="font-weight: bold">
                                                        </td>
                                                        <td align="left" valign="top" class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, OrganisationSettings_CateringModule%>" runat="server"></asp:Literal><!-- FB 2570 -->
                                                        </td>
                                                        <td align="left" class="altblackblodttext" valign="top" colspan="3">
                                                            <asp:Label id="LblFdMod" runat="server" text="<%$ Resources:WebResources, OrganisationSettings_LblFdMod%>"></asp:Label>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td align="right" height="21" style="font-weight: bold">
                                                        </td>
                                                        <td align="left" valign="top" class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, OrganisationSettings_FacilityServic%>" runat="server"></asp:Literal></td>
                                                        <td align="left" class="altblackblodttext" valign="top" colspan="3">
                                                            <asp:Label id="LblRsMod" runat="server" text="<%$ Resources:WebResources, OrganisationSettings_LblRsMod%>"></asp:Label>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td align="right" height="21" style="font-weight: bold">
                                                        </td>
                                                        <td align="left" valign="top" class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, OrganisationSettings_AudiovisualMod%>" runat="server"></asp:Literal></td>
                                                        <td align="left" class="altblackblodttext" valign="top" colspan="3">
                                                            <asp:Label id="LblFacility" runat="server" text="<%$ Resources:WebResources, OrganisationSettings_LblFacility%>"></asp:Label>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td align="right" height="21" style="font-weight: bold">
                                                        </td>
                                                        <td align="left" valign="top" class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, OrganisationSettings_APIModule%>" runat="server"></asp:Literal></td>
                                                        <td align="left" class="altblackblodttext" valign="top" colspan="3">
                                                            <asp:Label id="LblApi" runat="server" text="<%$ Resources:WebResources, OrganisationSettings_LblApi%>"></asp:Label>
                                                        </td>
                                                    </tr>
                                                    <%--ZD 104021 Starts--%>
                                                    <%-- <tr>
                                                        <td align="right" height="21" style="font-weight: bold">
                                                        </td>
                                                        <td align="left" valign="top" class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, OrganisationSettings_BlueJeans%>" runat="server"></asp:Literal></td>
                                                        <td align="left" class="altblackblodttext" valign="top" colspan="3">
                                                            <asp:Label id="LblBJ" runat="server" text="<%$ Resources:WebResources, OrganisationSettings_LblBJ%>"></asp:Label>
                                                        </td>
                                                    </tr>--%>
                                                    <%--ZD 104021 End--%>
                                                     <tr>
                                                        <td align="right" height="21" style="font-weight: bold">
                                                        </td>
                                                        <td align="left" valign="top" class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, OrganisationSettings_Jabber%>" runat="server"></asp:Literal></td>
                                                        <td align="left" class="altblackblodttext" valign="top" colspan="3">
                                                            <asp:Label id="LblJabber" runat="server" text="<%$ Resources:WebResources, OrganisationSettings_LblJabber%>"></asp:Label>
                                                        </td>
                                                    </tr>
                                                     <tr>
                                                        <td align="right" height="21" style="font-weight: bold">
                                                        </td>
                                                        <td align="left" valign="top" class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, OrganisationSettings_Lync%>" runat="server"></asp:Literal></td>
                                                        <td align="left" class="altblackblodttext" valign="top" colspan="3">
                                                            <asp:Label id="LblLync" runat="server" text="<%$ Resources:WebResources, OrganisationSettings_LblLync%>"></asp:Label>
                                                        </td>
                                                    </tr>
                                                     <tr style="display:none;"> <%--ZD 102004--%>
                                                        <td align="right" height="21" style="font-weight: bold">
                                                        </td>
                                                        <td align="left" valign="top" class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, OrganisationSettings_Vidtel%>" runat="server"></asp:Literal></td>
                                                        <td align="left" class="altblackblodttext" valign="top" colspan="3">
                                                            <asp:Label id="LblVidtel" runat="server" text="<%$ Resources:WebResources, OrganisationSettings_LblVidtel%>"></asp:Label>
                                                        </td>
                                                    </tr>
                                                    <%--FB 2593 Start--%>
                                                    <tr>
                                                        <td align="right" height="21" style="font-weight: bold">
                                                        </td>
                                                        <td align="left" valign="top" class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, OrganisationSettings_AdvRepMod%>" runat="server"></asp:Literal></td>
                                                        <td align="left" class="altblackblodttext" valign="top" colspan="3">
                                                            <asp:Label id="LblAdvReport" runat="server" text="<%$ Resources:WebResources, OrganisationSettings_LblAdvReport%>"></asp:Label>
                                                        </td>
                                                    </tr>
                                                    <%--FB 2593 End--%>
                                                    <%--FB 2347--%>
                                                    <%--<tr>
                                                        <td align="right" height="21" style="font-weight: bold">
                                                        </td>
                                                        <td align="left" valign="top" class="blackblodtext">
                                                            PC Module
                                                        </td>
                                                        <td align="left" class="altblackblodttext" valign="top" colspan="3">
                                                            <asp:Label ID="LblPC" runat="server" Text="Disabled"></asp:Label>
                                                        </td>
                                                    </tr>--%>
                                                     <%--FB 2262 //FB 2599 FB 2645 Starts--%>
                                                     <%if (Session["Cloud"] != null && Session["Cloud"].ToString().Equals("1"))
                                                       {%> 
                                                    <tr>
                                                        <td align="right" height="21" style="font-weight: bold">
                                                        </td>
                                                        <td align="left" valign="top" class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, OrganisationSettings_Vidyo%>" runat="server"></asp:Literal></td>
                                                        <td align="left" class="altblackblodttext" valign="top" colspan="3">
                                                            <asp:Label id="LblCloud" runat="server" text="<%$ Resources:WebResources, OrganisationSettings_LblCloud%>"></asp:Label>
                                                        </td>
                                                    </tr>
                                                    <%} %>
                                                    <%--FB 2262 //FB 2599 Ends--%>
                                                    <%--FB 2594 FB 2645 Starts--%>
                                                    <% if (Session["EnablePublicRooms"] != null && Session["EnablePublicRooms"].ToString().Equals("1"))
                                                       { %>
                                                    <tr>
                                                        <td align="right" height="21" style="font-weight: bold">
                                                        </td>
                                                        <td align="left" valign="top" class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, OrganisationSettings_PublicRoomSer%>" runat="server"></asp:Literal></td>
                                                        <td align="left" class="altblackblodttext" valign="top" colspan="3">
                                                            <asp:Label id="lblPublicRoom" runat="server" text="<%$ Resources:WebResources, OrganisationSettings_lblPublicRoom%>"></asp:Label>
                                                        </td>
                                                    </tr>
                                                    <% } %>
                                                    <%--FB 2594 FB 2645 Ends--%>
													<%--FB 2579 End--%>
                                                    </tr>
                                                    <tr>
                                                        <td align="left" class="subtitleblueblodtext" colspan="5"><asp:Literal Text="<%$ Resources:WebResources, OrganisationSettings_SystemApprover%>" runat="server"></asp:Literal></td>
                                                    </tr>
                                                    <tr>
                                                        <td align="right" height="21" style="font-weight: bold">
                                                        </td>
                                                        <td align="right" style="font-weight: bold" class="blackblodtext">
                                                        </td>
                                                        <%--FB 1982 --%>
                                                        <td align="left" style="font-weight: bold; width: 35%" class="style3"><asp:Literal Text="<%$ Resources:WebResources, OrganisationSettings_ApproverName%>" runat="server"></asp:Literal></td>
                                                        <td id="tdlblAction" height="21" style="font-weight: bold; width: 10%" class="blackblodtext" runat="server"><asp:Literal Text="<%$ Resources:WebResources, OrganisationSettings_tdlblAction%>" runat="server"></asp:Literal></td>
                                                        <%--FB 1982 --%>
                                                        <td style="width: 5%">
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td align="center" height="21" style="font-weight: bold">
                                                        </td>
                                                        <td align="left" height="21" style="font-weight: bold" class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, OrganisationSettings_PrimaryApprove%>" runat="server"></asp:Literal></td>
                                                        <td style="text-align: left" class="style4">
                                                            <asp:TextBox ID="txtApprover1" runat="server" CssClass="altText" Enabled="False"
                                                                Style="width: 95%"></asp:TextBox>
                                                        </td>
                                                        <%--FB 1982 --%>
                                                        <td align="left" id="tdAction" runat="server"> <%--FB 2594--%>
                                                            <a id="ImgApprover1" href="javascript: getYourOwnEmailList(0);" onmouseover="window.status='';return true;"><%--ZD 100420--%>
                                                                <img id="Img1" border="0" src="image/edit.gif" alt="Edit" style="cursor:pointer;" runat="server" title="<%$ Resources:WebResources, AddressBook%>" /></a> <%--FB 2798--%>
                                                            <%--FB 1982--%>
                                                            <a href="javascript: deleteApprover(0);" onmouseover="window.status='';return true;">
                                                                <img border="0" src="image/btn_delete.gif" alt="delete" width="16" height="16" style="cursor:pointer;" runat="server" title="<%$ Resources:WebResources, Delete%>" /></a> <%--FB 2798--%><%--ZD 100419--%>
                                                        </td>
                                                        <td>
                                                            <asp:TextBox ID="hdnApprover1" runat="server" Width="0px" BackColor="White" BorderColor="White"
                                                                BorderStyle="None" Style="display: none"></asp:TextBox>
                                                            <%--FB 1982 --%>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td align="left">
                                                        </td>
                                                        <td align="left" style="font-weight: bold" class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, OrganisationSettings_SecondaryAppro%>" runat="server"></asp:Literal></td>
                                                        <td style="text-align: left;" class="blackblodtext">
                                                            <asp:TextBox ID="txtApprover2" runat="server" CssClass="altText" Enabled="False"
                                                                Style="width: 95%"></asp:TextBox>
                                                        </td>
                                                        <%--FB 1982 --%>
                                                        <td align="left" id="tdAction2" runat="server"> <%--FB 2594--%>
                                                            <a href="javascript: getYourOwnEmailList(1);" onmouseover="window.status='';return true;">
                                                                <img id="Img2" border="0" src="image/edit.gif" style="cursor:pointer;" runat="server" runat="server" title="<%$ Resources:WebResources, myVRMAddressBook%>"/></a> <%--FB 2798--%>
                                                            <%--FB 1982--%>
                                                            <a href="javascript: deleteApprover(1);" onmouseover="window.status='';return true;">
                                                                <img border="0" src="image/btn_delete.gif" alt="delete" width="16" height="16" style="cursor:pointer;" runat="server" title="<%$ Resources:WebResources, Delete%>"></a> <%--FB 2798--%>
                                                        </td>
                                                        <td>
                                                            <asp:TextBox ID="hdnApprover2" runat="server" Width="0px" BackColor="White" BorderColor="White"
                                                                BorderStyle="None" Style="display: none"></asp:TextBox>
                                                        </td>
                                                        <%--FB 1982 --%>
                                                    </tr>
                                                    <tr>
                                                        <td align="left" height="21%" style="font-weight: bold">
                                                        </td>
                                                        <td align="left" height="21" style="font-weight: bold" class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, OrganisationSettings_SecondaryAppro2%>" runat="server"></asp:Literal></td>
                                                        <td style="text-align: left;" class="style5">
                                                            <asp:TextBox ID="txtApprover3" runat="server" CssClass="altText" Enabled="False"
                                                                Style="width: 95%"></asp:TextBox>
                                                        </td>
                                                        <%--FB 1982 --%>
                                                        <td align="left" id="tdAction3" runat="server"> <%--FB 2594--%>
                                                            <a href="javascript: getYourOwnEmailList(2);" onmouseover="window.status='';return true;">
                                                                <img id="Img3" border="0" src="image/edit.gif"  style="cursor:pointer;" runat="server" title="<%$ Resources:WebResources, myVRMAddressBook%>" /></a> <%--FB 2798--%>
                                                            <%--FB 1982--%>
                                                            <a href="javascript: deleteApprover(2);" onmouseover="window.status='';return true;">
                                                                <img border="0" src="image/btn_delete.gif" alt="delete" width="16" height="16" style="cursor:pointer;" runat="server" title="<%$ Resources:WebResources, Delete%>"></a> <%--FB 2798--%>
                                                        </td>
                                                        <td style="height: 21px;">
                                                            <asp:TextBox ID="hdnApprover3" runat="server" Width="0px" BackColor="White" BorderColor="White"
                                                                BorderStyle="None" Style="display: none"></asp:TextBox>
                                                        </td>
                                                        <%--FB 1982 --%>
                                                    </tr>
                                                    
                                                    <tr>
                                                        <td align="left" class="subtitleblueblodtext" colspan="5"><asp:Literal Text="<%$ Resources:WebResources, OrganisationSettings_RoomUsageRepo%>" runat="server"></asp:Literal></td>
                                                    </tr>
                                                    <tr>
                                                         <td align="right" height="21" style="font-weight: bold">
                                                        </td>
                                                        <td align="left" class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, OrganisationSettings_AccountingWork%>" runat="server"></asp:Literal></td>
                                                        <td style="height: 21px;" colspan="3">
                                                            <input type="button" name="btnManageDayColor" value="<asp:Literal Text='<%$ Resources:WebResources, Configure%>' runat='server' />" class="altMedium0BlueButtonFormat"
                                                                onclick="javascript:WorkingDayDetails();" />&nbsp;
                                                        </td>                                                      
                                                    </tr>
                                                    <tr>
                                                         <td align="right" height="21" style="font-weight: bold">
                                                        </td>
                                                        <td align="left" class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, OrganisationSettings_NumberofAccou%>" runat="server"></asp:Literal></td>
                                                        <td style="height: 21px;" colspan="3">
                                                            <asp:DropDownList ID="lstWorkingHours" runat="server" CssClass="altSelectFormat"
                                                                Style="width: 30%;">
                                                                <asp:ListItem Value="1">1</asp:ListItem>
                                                                <asp:ListItem Value="2">2</asp:ListItem>
                                                                <asp:ListItem Value="3">3</asp:ListItem>
                                                                <asp:ListItem Value="4">4</asp:ListItem>
                                                                <asp:ListItem Value="5">5</asp:ListItem>
                                                                <asp:ListItem Value="6">6</asp:ListItem>
                                                                <asp:ListItem Value="7">7</asp:ListItem>
                                                                <asp:ListItem Selected="True" Value="8">8</asp:ListItem>
                                                                <asp:ListItem Value="9">9</asp:ListItem>
                                                                <asp:ListItem Value="10">10</asp:ListItem>
                                                                <asp:ListItem Value="11">11</asp:ListItem>
                                                                <asp:ListItem Value="12">12</asp:ListItem>
                                                                <asp:ListItem Value="13">13</asp:ListItem>
                                                                <asp:ListItem Value="14">14</asp:ListItem>
                                                                <asp:ListItem Value="15">15</asp:ListItem>
                                                                <asp:ListItem Value="16">16</asp:ListItem>
                                                                <asp:ListItem Value="17">17</asp:ListItem>
                                                                <asp:ListItem Value="18">18</asp:ListItem>
                                                                <asp:ListItem Value="19">19</asp:ListItem>
                                                                <asp:ListItem Value="20">20</asp:ListItem>
                                                                <asp:ListItem Value="21">21</asp:ListItem>
                                                                <asp:ListItem Value="22">22</asp:ListItem>
                                                                <asp:ListItem Value="23">23</asp:ListItem>
                                                                <asp:ListItem Value="24">24</asp:ListItem>
                                                            </asp:DropDownList>
                                                        </td>
                                                    </tr>
                                                   <%-- FB 2501 EM7 Starts--%>
                                                   <%-- FB 2598 EnableEM7 Starts tr-id --%>
                                                    <tr id="trEM7OrgSetting" runat="server">
                                                    
                                                        <td align="left" class="subtitleblueblodtext" colspan="5"><asp:Literal Text="<%$ Resources:WebResources, OrganisationSettings_EM7Organizatio%>" runat="server"></asp:Literal></td>
                                                    </tr>
                                                    <tr id="trEM7Organization" runat="server">
                                                     <td align="right" height="10px" style="font-weight: bold">
                                                        </td>
                                                        <td align="left" class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, EM7Organization%>" runat="server"></asp:Literal></td>
                                                        <td>
                                                                <asp:DropDownList ID="lstEM7Orgsilo" runat="server" CssClass="altSelectFormat" Style="width: 95%">
                                                                <asp:ListItem Value="-1" Text="<%$ Resources:WebResources, NoItems%>"></asp:ListItem>
                                                                </asp:DropDownList>
                                                        </td>
                                                    </tr>
                                                    <%-- FB 2598 EnableEM7 tr-id  Ends--%>
                                                   <%-- FB 2501 EM7 Ends--%>
                                                </table>
                                            </td>
                                            <td width="40%" valign="top">
                                                <%--FB 1982--%>
                                                <table width="90%" border="0" cellpadding="2" cellspacing="0">
                                                    <%--FB 1982--%>
                                                    <tr>
                                                        <td align="left" class="subtitleblueblodtext" colspan="5" style="height: 21; font-weight: bold">
                                                            <asp:Literal Text="<%$ Resources:WebResources, OrganisationSettings_UserInterface%>" runat="server" />

                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td width="2%">
                                                        </td>
                                                        <td style="font-weight: bold; width: 15%" class="blackblodtext" colspan="2"><asp:Literal Text="<%$ Resources:WebResources, OrganisationSettings_UIDesignSetti%>" runat="server" /> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                                                        <td align="left" width="20%">
															<%--ZD 100420--%>
                                                            <%--<asp:Button  ID="Button1" runat="server" CssClass="altLongBlueButtonFormat" OnClick="btnChangeUIDesign_Click" OnClientClick="javascript:DataLoading(1)"
                                                                Text="Change UI Design" />--%>
                                                                <button  ID="Button1" runat="server" Class="altLongBlueButtonFormat" onclick="javascript:OpenChangeUIDesign(); return false;" style="width:257px;" ><asp:Literal Text='<%$ Resources:WebResources, OrganisationSettings_Button1%>' runat='server' /></button><%--ZD 102102--%>
															<%--ZD 100420--%>
                                                        </td>
                                                        <td width="1%">
                                                        </td>
                                                    </tr>
                                                    <tr style="display:none"><%--FB 2786--%>
                                                        <td width="2%">
                                                        </td>
                                                        <td style="font-weight: bold" class="blackblodtext" colspan="2"><asp:Literal Text="<%$ Resources:WebResources, OrganisationSettings_UITextSetting%>" runat="server"></asp:Literal> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                                                        <td align="left">
                                                            <input type="button" id="Button2" disabled="disabled" value="Change UI Text" class="altLongBlueButtonFormat"
                                                                onclick="fnTransferPage()" /> <%--FB 2719--%>
                                                        </td>
                                                        <td width="1%">
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <%--FB 2154--%>
                                                        <td align="right" height="21" style="font-weight: bold" width="2%">
                                                        </td>
                                                        <td colspan="2" valign="top" style="font-weight: bold" class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, OrganisationSettings_EmailDomain%>" runat="server"></asp:Literal></td>
                                                        <td valign="top">
															<%--ZD 100420--%>
                                                            <%--<asp:Button ID="btnEmailDomain" runat="server" Text="Manage Email Domain" OnClick="EditEmaiDomain" OnClientClick="javascript:DataLoading(1)"
                                                                class="altLongBlueButtonFormat" />--%><%--ZD 100176--%>
                                                            <button ID="btnEmailDomain" runat="server" onclick="javascript:OpenEmailDomain(); return false;" class="altLongBlueButtonFormat" style="width:257px;"><asp:Literal Text='<%$ Resources:WebResources, OrganisationSettings_btnEmailDomain%>' runat='server' /></button><%--ZD 102102--%>
															<%--ZD 100420--%>
                                                        </td>
                                                    </tr>
                                                    <tr style="height: 10px">
                                                        <td colspan="5">
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="width: 15%" align="left" class="subtitleblueblodtext" valign="top" colspan="5"><asp:Literal Text="<%$ Resources:WebResources, OrganisationSettings_BillingOptions%>" runat="server"></asp:Literal></td>
                                                    </tr>
                                                    <tr>
                                                        <td width="2%">
                                                        </td>
                                                        <td align="left" height="21" style="font-weight: bold; width: 32%" class="blackblodtext" colspan="2"><asp:Literal Text="<%$ Resources:WebResources, OrganisationSettings_BillingScheme%>" runat="server"></asp:Literal></td>
                                                        <td style="height: 21px;">
                                                            <asp:DropDownList ID="lstBillingScheme" runat="server" CssClass="altSelectFormat">
                                                            </asp:DropDownList>
                                                        </td>
                                                        <td width="1%">
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td width="2%">
                                                        </td>
                                                        <td align="left" height="21" style="font-weight: bold; width: 30%" class="blackblodtext" colspan="2"><asp:Literal Text="<%$ Resources:WebResources, OrganisationSettings_AllowOverAllo%>" runat="server"></asp:Literal></td>
                                                        <td style="height: 21px;">
                                                            <%--FB 2565 Starts--%>
                                                            <asp:DropDownList ID="drpAlloverAllocation" runat="server" Style="width: 30%;" CssClass="altSelectFormat">
                                                                <asp:ListItem Value="0" Text="<%$ Resources:WebResources, No%>"></asp:ListItem>
                                                                <asp:ListItem Value="1" Text="<%$ Resources:WebResources, Yes%>"></asp:ListItem>
                                                            </asp:DropDownList>
                                                            <%--<asp:CheckBox ID="chkAllowOver" runat="server" />--%>
                                                            <%--FB 2565 Ends--%>
                                                        </td>
                                                        <td width="1%">
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td width="2%">
                                                        </td>
                                                        <td align="left" height="21" style="font-weight: bold; width: 45%" class="blackblodtext"
                                                            colspan="2">
                                                            <%if (Application["Client"].ToString().ToUpper() == "MOJ"){%>Allow Billing on Point-<br />
                                                            to-point Hearing<%}else{ %><asp:Literal Text="<%$ Resources:WebResources, PttoPtBillingConferences%>" runat="server"></asp:Literal><%}%><%--added for FB 1428 Start--%>
                                                        </td>
                                                        <td style="height: 21px;">
                                                            <%--FB 2565 Starts--%>
                                                                <asp:DropDownList ID="drpAllowP2PConf" runat="server" Style="width: 30%;" CssClass="altSelectFormat">
                                                                <asp:ListItem Value="0" Text="<%$ Resources:WebResources, No%>"></asp:ListItem>
                                                                <asp:ListItem Value="1" Text="<%$ Resources:WebResources, Yes%>"></asp:ListItem>
                                                            </asp:DropDownList>
                                                            <%--<asp:CheckBox ID="chkP2P" runat="server" />--%>
                                                            <%--FB 2565 Ends--%>
                                                        </td>
                                                        <td width="1%">
                                                        </td>
                                                    </tr>
                                                    <%--FB 2045 Start--%>
                                                    <%--ZD 102102--%>
                                                    <%--ZD 102138 Hide --%>
                                                    <%--<tr style="display:none;">
                                                        <td width="2%">
                                                        </td>
                                                        <td align="left" height="21" style="font-weight: bold; width: 30%" class="blackblodtext" colspan="2"><asp:Literal Text="<%$ Resources:WebResources, OrganisationSettings_EntityCode%>" runat="server"></asp:Literal></td>
                                                        <td style="height: 21px;">
                                                                <button ID="btnEntityCode" runat="server" class="altLongBlueButtonFormat" onclick="javascript:OpenManageEntityCode(); return false;" style="width:257px;" ><asp:Literal Text='<%$ Resources:WebResources, OrganisationSettings_btnEntityCode%>' runat='server' /></button>
															
                                                        </td>
                                                        <td width="1%">
                                                        </td>
                                                    </tr>--%>
                                                    <%--ZD 100420--%>
                                                    <%--FB 2045 End--%>
                                                    <%--FB 1830 starts--%>
                                                    <tr>
                                                        <td style="width: 15%" align="left" class="subtitleblueblodtext" valign="top" colspan="5"><asp:Literal Text="<%$ Resources:WebResources, OrganisationSettings_LanguageSettin%>" runat="server"></asp:Literal></td>
                                                    </tr>
                                                    <tr>
                                                        <td width="2%">
                                                        </td>
                                                        <td align="left" height="21" style="font-weight: bold; width: 32%" class="blackblodtext" colspan="2"><asp:Literal Text="<%$ Resources:WebResources, OrganisationSettings_PreferredLangu%>" runat="server"></asp:Literal></td>
                                                        <td style="height: 21px;">
                                                            <asp:DropDownList ID="drporglang" runat="server" CssClass="altSelectFormat" DataTextField="name"
                                                                DataValueField="ID">
                                                            </asp:DropDownList>
                                                        </td>
                                                        <td width="1%">
                                                        </td>
                                                    </tr>
                                                    <%--FB 1830 ends--%>
                                                    <tr>
                                                        <td align="left" class="subtitleblueblodtext" colspan="5"><asp:Literal Text="<%$ Resources:WebResources, OrganisationSettings_CustomOption%>" runat="server"></asp:Literal></td>
                                                    </tr>
                                                    <%if(!(Application["Client"].ToString().ToUpper().Equals(ns_MyVRMNet.vrmClient.MOJ))){%>
                                                    <%--Added for FB 1425 MOJ--%>
                                                    <tr>
                                                        <td width="2%">
                                                        </td>
                                                        <td align="left" height="21" style="font-weight: bold; width: 32%;" class="blackblodtext" colspan="2"><asp:Literal Text="<%$ Resources:WebResources, OrganisationSettings_EnableCustomO%>" runat="server"></asp:Literal></td>
                                                        <td style="height: 21px;">
                                                            <asp:DropDownList ID="CustomAttributeDrop" runat="server" Style="width: 30%;" CssClass="altSelectFormat">
                                                                <asp:ListItem Value="0" Text="<%$ Resources:WebResources, No%>"></asp:ListItem>
                                                                <asp:ListItem Selected="True" Value="1" Text="<%$ Resources:WebResources, Yes%>"></asp:ListItem>
                                                            </asp:DropDownList>
                                                        </td>
                                                        <td width="1%">
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="3">
                                                        </td>
                                                        <td style="text-align: left" width="12%" colspan="2">
                                                            <br />
                                                            <input type="button" name="btnCustomAttribute"  style="width:257px;" value="<%$ Resources:WebResources, ManageCustomOptions%>" runat="server" class="altLongBlueButtonFormat"
                                                                onclick="javascript:OpenEntityCode();" />&nbsp;
                                                        </td>
                                                    </tr>
                                                    <%--code added for custom attribute end --%>
                                                    <%} %><%--Added for FB 1425 MOJ--%>
                                                    <%-- FB 2486 Start ZD 101757--%>
                                                    <tr id="trActMsgDeli" runat="server">
                                                        <td align="left" class="subtitleblueblodtext" colspan="3"><asp:Literal Text="<%$ Resources:WebResources, OrganisationSettings_ActiveMessage%>" runat="server"></asp:Literal></td>
                                                        <td style="height: 21px;" valign="bottom">
                                                            <input type="button" name="btnManageMsg"  style="width:257px;" value="<%$ Resources:WebResources, ManageRoom_hMgHdg%>" runat="server" class="altLongBlueButtonFormat"
                                                                onclick="javascript:OpenManageMsg();" />&nbsp;
                                                        </td>
                                                    </tr>
                                                    <%-- FB 2486 End--%>
                                                    <%--FB 2052--%>
                                                    <%if (!(Session["isSpecialRecur"].ToString().Equals("0"))){%>
                                                    <%--FB 2343 Start--%>
                                                    <tr>
                                                        <td align="left" class="subtitleblueblodtext" colspan="3"><asp:Literal Text="<%$ Resources:WebResources, OrganisationSettings_SpecialRecurre%>" runat="server"></asp:Literal></td>
                                                        <td style="height: 21px;" valign="bottom">
                                                            <input type="button" name="btnManageDayColor" style="width:257px;" value="<%$ Resources:WebResources, ManageDayColor%>" runat="server" class="altLongBlueButtonFormat"
                                                                onclick="javascript:OpenDayColor();" />&nbsp;
                                                        </td>
                                                    </tr>
                                                    <%} %>
                                                    <%--FB 2410--%>
                                                    <tr>
                                                        <td id="tdManageBatchRpt" runat="server" align="left" colspan="3" valign="top" class="subtitleblueblodtext"><asp:Literal Text="<%$ Resources:WebResources, OrganisationSettings_BatchReport%>" runat="server"></asp:Literal></td> <%--ZD 101674--%>
                                                        <td style="height: 21px;" valign="bottom">
                                                            <button name="btnManageBatchRpt" runat="server" id="btnManageBatchRpt" style="width:257px;" onclick="javascript:OpenBatchReport();return false;"><asp:Literal ID="Literal2" Text='<%$ Resources:WebResources, ConferenceList_btnManage%>' runat='server' /></button><%--ZD 100420--%>
                                                        </td>
                                                    </tr>
                                                    
                                                    <%--FB 2343--%>
                                                    <%--FB 2262 //FB 2599 Starts--%>
                                                    <tr id="trCloud" runat="server" visible="false">
                                                        <td align="left" class="subtitleblueblodtext" colspan="3"><asp:Literal Text="<%$ Resources:WebResources, OrganisationSettings_Vidyo%>" runat="server"></asp:Literal></td>
                                                        <td style="height: 21px;">
                                                        <button ID="btnCloudImport" runat="server" Class="altShortBlueButtonFormat" style="width:257px;" onserverclick="CloudImport" ><asp:Literal Text='<%$ Resources:WebResources, OrganisationSettings_btnCloudImport%>' runat='server' /></button><%--ZD 100420--%>
                                                        </td>
                                                    </tr>
                                                    <%--FB 2262 //FB 2599 Ends--%>
                                                     <%if (Session["UsrRoleName"] != null 
                                                        && (Session["UsrRoleName"].ToString().Equals("Organization Administrator 2") 
                                                        || Session["UsrRoleName"].ToString().Equals("Site Administrator"))){%>
                                                    <tr><%--ZD 100456--%>
                                                        <td align="left" colspan="3" valign="top" class="subtitleblueblodtext">
                                                            <asp:Literal ID="Literal3" Text='<%$ Resources:WebResources, DataImportExport%>' runat='server' />
                                                        </td>
                                                        <td style="height: 21px;" valign="bottom">
                                                            <button name="btnDataImport" runat="server" class="altShortBlueButtonFormat" id="btnDataImport" style="width:257px;" onclick="javascript:OpenDataImport();return false;"><asp:Literal ID="Literal4" Text='<%$ Resources:WebResources, ConferenceList_btnManage%>' runat='server' /></button>
                                                        </td>
                                                    </tr>
                                                    <%} %>
                                                <tr> <%--ZD 101835--%>
                                                    <td align="left" colspan="3" valign="top" class="subtitleblueblodtext">
                                                        <asp:Literal Text='<%$ Resources:WebResources, ArchiveConfiguration%>' runat='server' />
                                                    </td>
                                                    <td style="height: 21px;" valign="bottom">
                                                        <button name="btnConfArchive" runat="server" class="altShortBlueButtonFormat" id="btnConfArchive" style="width:257px;" onclick="javascript:return fnOpenSavedPlans();">
                                                            <asp:Literal Text='<%$ Resources:WebResources, ConferenceList_btnManage%>' runat='server' />
                                                        </button>
                                                    </td>
                                                </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td>
            <table width="100%" border="0">
                <tr style="display: none;">
                    <td align="left" class="subtitleblueblodtext" colspan="3"><asp:Literal Text="<%$ Resources:WebResources, OrganisationSettings_UserInterface%>" runat="server"></asp:Literal></td>
                </tr>
                <tr style="display: none;">
                    <%--Window Dressing--%>
                    <td align="left" height="21" style="font-weight: bold; width: auto" class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, OrganisationSettings_UIDesignSetti%>" runat="server"></asp:Literal></td>
                    <td style="height: 21px; font-weight: bold" width="30%" colspan="3">
                        <asp:Button id="btnChangeUIDesign" runat="server" cssclass="altLongBlueButtonFormat" OnClientClick="javascript:OpenChangeUIDesign(); return false;" text="<%$ Resources:WebResources, OrganisationSettings_btnChangeUIDesign%>"></asp:Button> <%--ZD 102102--%>
                    </td>
                    <td align="right" style="height: 21px; width: 15%;">
                    </td>
                    <td style="height: 21px;" width="35%">
                    </td>
                </tr>
                <%-- Code Added for FB 1428--%>
                <tr style="display: none;">
                    <%--Window Dressing--%>
                    <td align="left" height="21" style="font-weight: bold" class="blackblodtext" style="width: 18%"><asp:Literal Text="<%$ Resources:WebResources, OrganisationSettings_UITextSetting%>" runat="server"></asp:Literal></td>
                    <td style="height: 21px; font-weight: bold" width="30%">
                        <input type="button" id="btnUITextChange" value="Change UI Text" class="altLongBlueButtonFormat"
                            onclick="fnTransferPage()" />
                    </td>
                    <td align="right" style="height: 21px; width: 15%;">
                    </td>
                    <td style="height: 21px;" width="35%">
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td>
            <%--Mail Logo and Footer message--%>
            <table width="100%" style="height: 21%" border="0">
                <%--FB 1982 --%>
                <%--TD Width Updated for FB 2050--%>
                <tr>
                    <td width="100%" align="left" style="font-weight: bold" valign="top" class="subtitleblueblodtext" colspan="5"><asp:Literal Text="<%$ Resources:WebResources, OrganisationSettings_MailSettings%>" runat="server"></asp:Literal></td>
                    <%--FB 1982 --%>
                </tr>
                <tr>
                    <td style="width: 3%">
                    </td>
                    <td style="font-weight: bold; width: 22%" align="left" class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, OrganisationSettings_MailLogo%>" runat="server"></asp:Literal></td>
                    <%--FB 1982--%>
                    <td style="width: 75%" colspan="5" valign="top" align="left">
                        <%--FB 1982 --%>
                        <table style="width: 100%" border="0">
                            <tr>
                                <td> <%--FB 2909 Start--%>
                                    <div>
									<%--ZD 100420--%>
                                     <input id="fileInputtxt" type="text" class="file_input_textbox" readonly="readonly" value='<%$ Resources:WebResources, Nofileselected%>' runat="server" onfocus="var val = this.value; this.value = ''; this.value = val;" />
                                      <div class="file_input_div"><button id="btnBrowse" class="file_input_button"  onclick="document.getElementById('fleMap1').click();return false;"><asp:Literal ID="Literal5" Text='<%$ Resources:WebResources, Browse%>' runat='server' /></button> 
										<%--ZD 100420--%>
                                       <input type="file"  class="file_input_hidden" accept="image/*" id="fleMap1" contenteditable="false" enableviewstate="true" runat="server" onchange="getfilename(this);fnValidateFileName(this.value,1);"/></div></div> <%--FB 3055-Filter in Upload Files--%>
                                       <asp:RegularExpressionValidator ID="regfleMap1" runat="server" Display="Dynamic" ValidationGroup="Submit1" ControlToValidate="fleMap1" CssClass="lblError" ErrorMessage="<%$ Resources:WebResources, Filetypeinvalid%>." ValidationExpression="^.*\.((j|J)(p|P)(e|E)?(g|G)|(g|G)(i|I)(f|F)|(p|P)(n|N)(g|G)|(b|B)(m|M)(p|P))$"></asp:RegularExpressionValidator>
                                    <%--<input type="file" id="fleMap1" contenteditable="false" enableviewstate="true" size="50"
                                        class="altText" runat="server" />--%><%--FB 2909 End--%>
                                    <asp:Button id="btnUploadImages" onclick="UploadMailLogoImages" runat="server" text="<%$ Resources:WebResources, OrganisationSettings_btnUploadImages%>" style="margin-left:2px" cssclass="altLongBlueButtonFormat" validationgroup="Submit1" OnClientClick="return fnUpload1(document.getElementById('fleMap1').value, this.id, 'Submit1');"></asp:Button>  <%--FB 2909 End--%> <%--ZD 102364--%>
                                    <div><%--ZD 102277--%>
                                     <asp:RegularExpressionValidator ID="regFNVal1" ControlToValidate="fleMap1"
                                                    Display="dynamic" runat="server" ValidationGroup="Submit1" SetFocusOnError="true"
                                                    ErrorMessage="<%$ Resources:WebResources, InvalidCharacters45%>"
                                                    ValidationExpression="^[^<>&#]*$"></asp:RegularExpressionValidator>
                                                    </div>
                                    <cc1:ImageControl id="Map1ImageCtrl" width="30" height="30" visible="false" runat="server">
                                    </cc1:ImageControl>
                                    <asp:Label id="lblUploadMap1" text="" visible="false" runat="server"></asp:Label>
                                    <asp:Button id="btnRemoveMap1" cssclass="altMedium0BlueButtonFormat" text="<%$ Resources:WebResources, OrganisationSettings_btnRemoveMap1%>" visible="false" runat="server" oncommand="RemoveFile" commandargument="1"></asp:Button>
                                    <asp:Label id="hdnUploadMap1" text="" visible="false" runat="server"></asp:Label>
                                    <input type="hidden" id="Map1ImageDt" name="Map1ImageDt" runat="server" />
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <%--code added for FB 1710 start--%>
                <tr>
                    <td align="left" colspan="5" height="21">
                    </td>
                </tr>
                
                <tr>
                    <td height="21%">
                    </td>
                    <td align="left" valign="top" class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, OrganisationSettings_FooterMessage%>" runat="server"></asp:Literal></td>
                    <%--FB 1982 --%>
                    <td colspan="3" height="21" align="left" valign="top" class="blackblodtext">
                        <dxHE:ASPxHtmlEditor ID="dxHTMLEditor" AccessibilityCompliant="true"  TabIndex="-1" runat="server" Height="200px" >
                            <SettingsImageUpload UploadImageFolder="~/image/maillogo/"><%--ZD 100420--%>
                                <ValidationSettings MaxFileSize="100000" MaxFileSizeErrorText="<%$ Resources:WebResources, MaxFileSizeErrorText%>" />
                            </SettingsImageUpload>
                            <ClientSideEvents LostFocus="function(s,e){setTimeout('fnSetFocus();',100);}" /><%-- ZD 100369 508 Issue--%>
                        </dxHE:ASPxHtmlEditor>
                        <input type="file" id="fmMap" contenteditable="false" size="50" class="altText" runat="server"
                            visible="false" />
                        <input type="hidden" id="fmMapImage" name="Map1ImageDt" runat="server" height="21%"
                            style="display: none" /><%--FB 1982 --%>
                            
                        
                       
                    </td>
                </tr>
                <tr>
                    <td colspan="5" height="15">
                    </td>
                </tr>
                <%--code added for FB 1710 end--%>
                
                <%--FB 2659 Starts--%>
                <tr id="tdSubject" runat="server">
                <td style="width: 3%">
                    </td>
                <td style="font-weight: bold; width: 22%" align="left" class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, OrganisationSettings_DefaultAppoint%>" runat="server"></asp:Literal></td>
                <td>
                <input type="text" id="txtSubject" runat="server" class="altText" style="width:500px;" />
                </td>
                </tr>
                <tr id="tdInvitation" runat="server">
                <td style="width: 3%">
                    </td>
                <td style="font-weight: bold; width: 22%" align="left" class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, OrganisationSettings_DefaultAppointbody%>" runat="server"></asp:Literal></td>
                <td>
                <br />
                <textarea id="txtInvitaion" runat="server" rows="2" cols="20" class="altText" style="width:500px;height:100px;" ></textarea>
                </td>
                </tr>
                <%--FB 2659 End--%>
                
                <%--FB 1758 Starts--%>
                <tr>
                    <td colspan="5">
                    <br /><%--FB 2659--%>
                        <table colspan="1" width="100%" border="0" cellpadding="0" cellspacing="0">
                            <%--TD Width Updated for FB 2050--%>
                            <tr>
                                <td height="18%" style="width: 3%">
                                </td>
                                <%--//FB 1830 Language  --%>
                                <td align="left" style="width: 22%" class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, OrganisationSettings_TestEmailID%>" runat="server"></asp:Literal></td>
                                <%--//FB 1830 Language--%>
                                <%--FB 1982--%>
                                <td colspan="3" align="left" class="style1">
                                    <asp:TextBox ID="txtTestEmailId" runat="server" CssClass="altText" Width="200px"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="reqvalid" runat="server" ControlToValidate="txtTestEmailId"
                                        ValidationGroup="TestEmail" ErrorMessage="<%$ Resources:WebResources, Required%>" Display="dynamic"></asp:RequiredFieldValidator>
                                    <asp:RegularExpressionValidator ID="regTestemail" runat="server" ControlToValidate="txtTestEmailId"
                                        ErrorMessage="<%$ Resources:WebResources, InvalidEmailAddress%>" Display="dynamic" ValidationExpression="^[a-zA-Z()^][()^\w\.-]*[a-zA-Z0-9()^]@[a-zA-Z0-9][\w\.-]*[a-zA-Z0-9]\.[a-zA-Z][a-zA-Z\.]*[a-zA-Z]$"></asp:RegularExpressionValidator>
                                    <asp:RegularExpressionValidator ID="reqvalid2" ControlToValidate="txtTestEmailId"
                                        Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="<%$ Resources:WebResources, InvalidCharacters%>"
                                        ValidationExpression="^(a-z|A-Z|0-9)*[^\\/<>+;?|!`,\[\]{}\x22;=:#$%&'~]*$"></asp:RegularExpressionValidator>
                                    <asp:Button ID="btntestmail" Text="<%$ Resources:WebResources, OrganisationSettings_btntestmail%>" ValidationGroup="TestEmail" CssClass="altMedium0BlueButtonFormat"
                                        runat="server" OnClick="TestEmailConnection" Width="170px" />
                                </td>
                            </tr>
                            <%--FB 1830 Starts--%>
                            <tr>
                                <td align="left" height="15" style="font-weight: bold">
                                    <%--//FB 1830 Language--%>
                                    <%--FB 1982--%>
                                </td>
                            </tr>
                            <tr>
                                <td align="left" height="21" style="font-weight: bold" style="width: 3%">
                                </td>
                                <%--FB 1982--%>
                                <td align="left" class="blackblodtext" style="width: 22%"><asp:Literal Text="<%$ Resources:WebResources, OrganisationSettings_EmailLanguage%>" runat="server"></asp:Literal></td>
                                <%--FB 1982--%>
                                <td align="left" height="21" style="width: 28%">
                                    <%--FB 1860 start--%>
                                    <%--FB 1982 and FB 2050--%>
                                    <asp:Button id="btnDefine" runat="server" text="<%$ Resources:WebResources, OrganisationSettings_btnDefine%>" onclick="DefineEmailLanguage" onclientclick="javascript:DataLoading(1);" cssclass="altMedium0BlueButtonFormat"></asp:Button><%--ZD 100176--%> 
                                    <%--FB 1982 FB 2104--%>
                                    <asp:TextBox ID="txtEmailLang" runat="server" ReadOnly="true" CssClass="altText"
                                        Visible="false"></asp:TextBox>&nbsp;<%--FB 1830 - DeleteEmailLang 2104--%>
                                    <asp:ImageButton ImageUrl="image/btn_delete.gif" runat="server" ID="delEmailLang" AlternateText="Delete Email Language"
                                        ToolTip="<%$ Resources:WebResources, DeleteEmailLanguage%>" OnClick="DeleteEmailLangugage" OnClientClick="javascript:return fnDelEmailLan()" /> <%--ZD 100419--%>
                                    <%--FB 1830 - DeleteEmailLang--%>
                                </td>
                                <td align="left" class="blackblodtext" style="width: 18%">
                                    <%-- FB 2050 --%>
                                    <asp:Label id="LblBlockEmails" runat="server" text="<%$ Resources:WebResources, OrganisationSettings_LblBlockEmails%>" nowrap=""></asp:Label> &nbsp;
                                    <asp:CheckBox id="ChkBlockEmails" runat="server"></asp:CheckBox>
                                </td>
                                <td align="left" valign="top" width="29%"> 
                                    <asp:Button id="BtnBlockEmails" style="display: none;" runat="server" text="<%$ Resources:WebResources, OrganisationSettings_BtnBlockEmails%>" onclick="EditBlockEmails" width="29%" class="altShortBlueButtonFormat"></asp:Button>
                                    <%--FB 1982--%>
                                    <%--FB 2164--%>
                                </td>
                            </tr>
                        </table>
                        <%--FB 1982--%>
                    </td>
                    <%--FB 1860 end--%>
                </tr>
            </table>
            <%--FB 1982 end--%>
            <%--FB 1830 Ends--%>
            <%--FB 1758 Ends--%>
            <%--Window Dressing end--%>
            <tr style="display:none"> <%--ZD 101476--%>
                <%--FB 2337--%>
                <td>
                    <table width="100%" style="height: 21%" border="0" cellpadding="0" cellspacing="0"> <%--FB 2555--%>
                        <tr>
                            <td align="left" height="21" style="font-weight: bold; width: 3%">
                            </td>
                            <td align="left" class="blackblodtext" style="width: 22%; display:none;"><asp:Literal Text="<%$ Resources:WebResources, OrganisationSettings_EndUserLicens%>" runat="server"></asp:Literal></td> 
                            <td align="left" height="21" width="28%" style="display:none"> <%--FB 2555--%> <%--FB 2848--%>
                                <asp:Button id="btnCustLicAgrmnt" runat="server" text="<%$ Resources:WebResources, OrganisationSettings_btnCustLicAgrmnt%>" cssclass="altMedium0BlueButtonFormat" onclick="CustomizeLicenseAgreement"></asp:Button>
                            </td> 
                            <%--FB 2555 Starts--%><%--ZD 101476 start--%>
                            <td align="left" class="blackblodtext" style="width: 21%;padding-left: 1px;display:none;"><asp:Literal Text="<%$ Resources:WebResources, OrganisationSettings_EmailDateForm%>" runat="server"></asp:Literal></td><%--FB 2848--%>
                            <td align="left" height="21" style="width:71%;display:none;"><%--FB 2848--%>
                            <asp:DropDownList ID="drpEmailDateFormat" runat="server" CssClass="altSelectFormat" style="width: 20%"><%--FB 2848--%>
                                <asp:ListItem Value="0" Text="<%$ Resources:WebResources, UserPreference%>"></asp:ListItem>
                                <asp:ListItem Value="1" Text="<%$ Resources:WebResources, EuropeanddMmmYYYY%>"></asp:ListItem></asp:DropDownList>
                            </td> 
                            <%--FB 2555 Ends--%> <%--ZD 101476 End--%> 
                        </tr>
                    </table>
                </td>
            </tr>
            <%if((Application["External"].ToString() != "")){%>
            <tr>
                <td>
                    <table width="100%" style="height: 21%" border="0">
                        <tr>
                            <td width="100%" align="left" style="font-weight: bold" valign="top" class="subtitleblueblodtext" colspan="5"><asp:Literal Text="<%$ Resources:WebResources, OrganisationSettings_ExternalSchedu%>" runat="server"></asp:Literal></td>
                        </tr>
                        <tr>
                            <td style="width: 3%">
                            </td>
                            <td class="blackblodtext" style="width: 22%"><asp:Literal Text="<%$ Resources:WebResources, OrganisationSettings_CustomerName%>" runat="server"></asp:Literal></td>
                            <td style="width: 28%">
                                <asp:TextBox ID="txtCustomerName" runat="server" CssClass="altText" Width="200px"></asp:TextBox>
                                <asp:RegularExpressionValidator ID="RegularExpressionValidator12" ControlToValidate="txtCustomerName"
                                    ValidationGroup="submit" Display="dynamic" runat="server" SetFocusOnError="true"
                                    ErrorMessage="<%$ Resources:WebResources, InvalidCharacters2%>"
                                    ValidationExpression="^(a-z|A-Z|0-9)*[^\\/<>+;?|!`,\[\]{}\x22;=^:@#$%&()']*$"></asp:RegularExpressionValidator>
                            </td>
                            <td class="blackblodtext" style="width: 18%">
                                <asp:Literal ID="Literal6" Text='<%$ Resources:WebResources, OrganisationSettings_CustomerID%>' runat='server' />
                            </td>
                            <td>
                                <asp:TextBox ID="txtCustomerID" runat="server" CssClass="altText" Width="200px"></asp:TextBox>
                                <asp:RegularExpressionValidator ID="RegularExpressionValidator13" ControlToValidate="txtCustomerID"
                                    ValidationGroup="submit" Display="dynamic" runat="server" SetFocusOnError="true"
                                    ErrorMessage="<%$ Resources:WebResources, InvalidCharacters2%>"
                                    ValidationExpression="^(a-z|A-Z|0-9)*[^\\/<>+;?|!`,\[\]{}\x22;=^:@#$%&()']*$"></asp:RegularExpressionValidator>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <%} %>
            <%--FB 2262 //FB 2599 start--%>
            <tr id="trCloudDetails" runat="server" visible="false">
                <td>
                    <table width="100%" style="height: 21%" border="0">
                        <tr>
                            <td width="100%" align="left" style="font-weight: bold" valign="top" class="subtitleblueblodtext" colspan="5"><asp:Literal Text="<%$ Resources:WebResources, OrganisationSettings_Vidyo%>" runat="server"></asp:Literal></td>
                        </tr>     
                        <tr>
                            <td style="width: 3%"></td>
                            <td class="blackblodtext" style="width: 22%"><asp:Literal Text="<%$ Resources:WebResources, OrganisationSettings_VidyoURL%>" runat="server"></asp:Literal></td>
                            <td style="width: 28%">
                                <asp:TextBox ID="txtvidyoURL" runat="server" CssClass="altText" TextMode="MultiLine" MaxLength="150"
                                    Width="200px"></asp:TextBox>
                                <asp:RegularExpressionValidator ID="RegularExpressionValidator11" ControlToValidate="txtvidyoURL"
                                    ValidationGroup="submit" Display="dynamic" runat="server" SetFocusOnError="true"
                                    ErrorMessage="<%$ Resources:WebResources, InvalidCharacters3%>"
                                    ValidationExpression="^(a-z|A-Z|0-9)*[^\\<>+;?|!`,\[\]{}\x22;=^@#$%&()']*$"></asp:RegularExpressionValidator>
                            </td>
                            <td class="blackblodtext" style="width: 18%">
                                <asp:Literal Text='<%$ Resources:WebResources, Login%>' runat='server' />
                            </td>
                            <td>
                                <asp:TextBox ID="txtvidyoLogin" runat="server" CssClass="altText" Rows="3" MaxLength="30"></asp:TextBox>
                                <asp:RegularExpressionValidator ID="RegularExpressionValidator16" ControlToValidate="txtvidyoLogin"
                                    ValidationGroup="submit" Display="dynamic" runat="server" SetFocusOnError="true"
                                    ErrorMessage="<%$ Resources:WebResources, InvalidCharacters2%>"
                                    ValidationExpression="^(a-z|A-Z|0-9)*[^\\/<>+;?|!`,\[\]{}\x22;=^:@#$%&()']*$"></asp:RegularExpressionValidator>
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 3%"></td>
                            <td style="width: 22%" class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, OrganisationSettings_Password%>" runat="server"></asp:Literal></td>
                            <td style="width: 28%">
                                <asp:TextBox ID="txtvidyoPassword1" style="background-image:url('../image/pwdbg.png'); background-repeat:no-repeat" onchange="javascript:PreservePassword()" runat="server" MaxLength="30"
                                    CssClass="altText" TextMode="Password" onblur="PasswordChange(1,1)" onfocus=" fnTextFocus(this.id,1,1)"></asp:TextBox> <%--ZD 101443--%>
                                    <asp:CompareValidator ID="CompareValidator2"
                                    runat="server" ControlToValidate="txtvidyoPassword1" ControlToCompare="txtvidyoPassword2"
                                    ErrorMessage="<%$ Resources:WebResources, Passwordsdonotmatch%>" Font-Names="Verdana" Font-Size="X-Small"
                                    Font-Bold="False" Display="Dynamic" />
                            </td>
                            <td style="width: 18%" class="blackblodtext">
                                <asp:Literal ID="Literal7" Text='<%$ Resources:WebResources, SuperAdministrator_RetypePassword%>' runat='server' />
                            </td>
                            <td style="height: 37px;" width="35%">
                                <asp:TextBox ID="txtvidyoPassword2" style="background-image:url('../image/pwdbg.png'); background-repeat:no-repeat" runat="server" onchange="javascript:PreservePassword()" MaxLength="30"
                                    CssClass="altText" TextMode="Password" onblur="PasswordChange(1,2)" onfocus=" fnTextFocus(this.id,2,1)"></asp:TextBox><%--ZD 101443--%>
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 3%"></td>
                            <td class="blackblodtext" style="width: 22%"><asp:Literal Text="<%$ Resources:WebResources, OrganisationSettings_ProxyAddress%>" runat="server"></asp:Literal></td>
                            <td style="width: 28%">
                                <asp:TextBox ID="txtproxyAdd" runat="server" CssClass="altText"></asp:TextBox>
                                <asp:RegularExpressionValidator ID="regTimeoutVlaue" runat="server" ControlToValidate="txtproxyAdd" ValidationGroup="submit" Display="Dynamic" ErrorMessage="<%$ Resources:WebResources, InvalidIPAddress%>" ValidationExpression="^[^&<>+'dD][0-9'.]*$"></asp:RegularExpressionValidator>
                            </td>
                            <td class="blackblodtext" style="width: 18%">
                                <asp:Literal ID="Literal8" Text='<%$ Resources:WebResources, OrganisationSettings_Port%>' runat='server' />
                            </td>
                            <td>
                                <asp:TextBox ID="txtvidyoPort" runat="server" CssClass="altText" Rows="3" ></asp:TextBox>
                                <asp:RegularExpressionValidator ID="RegularExpressionValidator1" ControlToValidate="txtvidyoPort"
                                    ValidationGroup="submit" Display="dynamic" runat="server" SetFocusOnError="true"
                                    ErrorMessage="<%$ Resources:WebResources, InvalidCharacters2%>"
                                    ValidationExpression="^(a-z|A-Z|0-9)*[^\\/<>+;?|!`,\[\]{}\x22;=^:@#$%&()']*$"></asp:RegularExpressionValidator>
                            </td>
                        </tr>
                        <%--<tr>
                            <td style="width: 3%"></td>
                            <td class="blackblodtext" style="width: 22%">
                                Poll
                            </td>
                            <td style="width: 28%">
                            <%--<input type="button" id="btnvidyoPoll" runat="server" name="btnvidyoPoll" value="Now" class="altShortBlueButtonFormat" >--%>
                                <%--<asp:Button ID="btnvidyoPoll"  runat="server" Text="Now" class="altShortBlueButtonFormat" />
                            </td>
                            <td colspan="2" ></td>
                        </tr>--%>   
                    </table>
                </td>
            </tr>
            <%--FB 2262 //FB 2599 End--%>
            <%-- Commented this, because not deliver for this Phase II delivery
            <tr>
                <td>
                    <table width="100%" style="height: 21%" border="0">
                        <tr>
                            <td width="100%" align="left" style="font-weight: bold" valign="top" class="subtitleblueblodtext"
                                colspan="5">
                                <span class="subtitleblueblodtext">Mailing Error Report Settings</span>
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 3%">
                            </td>                            
                            <td class="blackblodtext" style="width: 22%">
                                Recipient's eMail 
                            </td>
                            <td style="width: 28%">
                                <asp:TextBox ID="txtUsrRptDestination" runat="server" CssClass="altText" Width="200px"></asp:TextBox>
                                <asp:RegularExpressionValidator ID="RegtxtUsrRptDestination" ControlToValidate="txtUsrRptDestination"
                                    ValidationGroup="submit" Display="dynamic" runat="server" SetFocusOnError="true"
                                    ErrorMessage="<br>Invalid email address." ValidationExpression="[a-zA-Z][\w\.-]*[a-zA-Z0-9]@[a-zA-Z0-9][\w\.-]*[a-zA-Z0-9]\.[a-zA-Z][a-zA-Z\.]*[a-zA-Z]$"></asp:RegularExpressionValidator>
                            </td>                        
                            <td class="blackblodtext" style="width: 18%">
                                Frequency
                            </td>
                            <td >
                                <asp:TextBox ID="lstUsrRptFrequencyCount" runat="server" CssClass="altText" Width="70px"></asp:TextBox>
                                (Mins)
                            </td>                            
                        </tr>                        
                    </table>
                </td>
            </tr>--%>
            
            <%--ALLDEV-833 START--%>
            <tr id="trEWSDetails" runat="server">
                <td>
                    <table width="100%" style="height: 21%" border="0">
                        <tr>
                            <td width="100%" align="left" style="font-weight: bold" valign="top" class="subtitleblueblodtext" colspan="5"><asp:Literal Text="<%$ Resources:WebResources, OrganisationSettings_EWS%>" runat="server"></asp:Literal></td>
                        </tr>     
                        <tr>
                            <td style="width: 3%"></td>
                            <td class="blackblodtext" style="width: 22%"><asp:Literal Text="<%$ Resources:WebResources, OrganisationSettings_EWSURL%>" runat="server"></asp:Literal></td>
                            <td style="width: 28%">
                                <asp:TextBox ID="txtEWSURL" runat="server" CssClass="altText" TextMode="MultiLine" MaxLength="150" Width="200px"></asp:TextBox>
                                <asp:RegularExpressionValidator ID="regEWSURL" ControlToValidate="txtEWSURL" Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="<%$ Resources:WebResources, InvalidCharacters33%>" ValidationExpression="^[^<>&]*$"></asp:RegularExpressionValidator>
                            </td>
                            <td class="blackblodtext" style="width: 18%">
                                <asp:Literal Text='<%$ Resources:WebResources, OrganisationSettings_EWSUserName%>' runat='server' />
                            </td>
                            <td>
                                <asp:TextBox ID="txtEWSUserName" runat="server" CssClass="altText" Rows="3" MaxLength="50"></asp:TextBox>
                                <asp:RegularExpressionValidator ID="regEWSUserName" ControlToValidate="txtEWSUserName" Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="<%$ Resources:WebResources, InvalidCharacters33%>" ValidationExpression="^[^<>&]*$"></asp:RegularExpressionValidator>
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 3%"></td>
                            <td style="width: 22%" class="blackblodtext"><asp:Literal ID="Literal18" Text="<%$ Resources:WebResources, OrganisationSettings_EWSPassword%>" runat="server"></asp:Literal></td>
                            <td style="width: 28%">
                                <asp:TextBox ID="txtEWSPassword" style="background-image:url('../image/pwdbg.png'); background-repeat:no-repeat" onchange="javascript:PreservePassword()" runat="server" MaxLength="30"
                                    CssClass="altText" TextMode="Password" onblur="PasswordChange(3,3)" onfocus=" fnTextFocus(this.id,1,3)"></asp:TextBox>
                                <asp:RegularExpressionValidator ID="regEWSPassword" ControlToValidate="txtEWSPassword" Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="<%$ Resources:WebResources, InvalidCharacters33%>" ValidationExpression="^[^<>&]*$"></asp:RegularExpressionValidator>
                            </td>
                            <td class="blackblodtext" style="width: 18%">
                                <asp:Literal Text='<%$ Resources:WebResources, OrganisationSettings_EWSVersion%>' runat='server' />
                            </td>
                            <td>
                                <asp:DropDownList ID="lstEWSVersion" runat="server" CssClass="altSelectFormat" style="width: 46%">
                                    <asp:ListItem Value="2007" Text="2007"></asp:ListItem> <%--ALLDEV-865 Start--%>
                                    <asp:ListItem Value="2010" Text="2010"></asp:ListItem>
                                    <asp:ListItem Value="2010_SP1" Text="2010_SP1"></asp:ListItem>
                                    <asp:ListItem Value="2010_SP2" Text="2010_SP2"></asp:ListItem>                                    
                                    <asp:ListItem Value="2013" Text="2013"></asp:ListItem> <%--ALLDEV-865 End--%>
                                    <asp:ListItem Value="365" Text="365"></asp:ListItem>
                                </asp:DropDownList>
                            </td>
                         </tr>
                   </table>
                </td>
            </tr>
            <%--ALLDEV-833 END--%>
            
            <%--ZD 101443 start--%>
            <tr> 
             <td>
             <table width="100%" border="0" cellpadding="3" cellspacing="1">
               <tr>
                   <td align="left" class="subtitleblueblodtext" colspan="5" style="font-weight: bold"><asp:Literal Text="<%$ Resources:WebResources, SuperAdministrator_MSActiveDirec%>" runat="server"></asp:Literal></td>
               </tr>
               <tr>
                  <td align="left" height="21" style="font-weight: bold" width="3%">
                    </td>
                    <td align="left" height="21" style="font-weight: bold" width="22%" class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, SuperAdministrator_ServerAddress%>" runat="server"></asp:Literal></td>
                    <td style="height: 21px;" width="28%">
                        <asp:TextBox ID="txtLDAPServerAddress" runat="server" CssClass="altText" onfocus="LDAPTC()"></asp:TextBox>
                        <%--                     <asp:RegularExpressionValidator ID="RegularExpressionValidator1" ControlToValidate="txtLDAPServerAddress" Display="dynamic" runat="server" 
                                 ErrorMessage="<br>+'&<>%;:( )& / \ ^#$@ and double quotes are invalid characters for this field." ValidationExpression="[A-Za-z0-9._~?!`* \-]+"></asp:RegularExpressionValidator>                    
        --%>
                        <asp:RegularExpressionValidator runat="server" ID="regAddress" Enabled="false" ControlToValidate="txtLDAPServerAddress"
                            Display="Dynamic" ValidationExpression="^([1-9]|[1-9][0-9]|1[0-9][0-9]|2[0-4][0-9]|25[0-5])(\.([0-9]|[1-9][0-9]|1[0-9][0-9]|2[0-4][0-9]|25[0-5])){3}$"
                            ErrorMessage="<%$ Resources:WebResources, InvalidIPAddress%>"></asp:RegularExpressionValidator>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator7" ValidationGroup="LDAP" ErrorMessage="<%$ Resources:WebResources, Required%>"
                            ControlToValidate="txtLDAPServerAddress" Display="dynamic" runat="server" SetFocusOnError="true"></asp:RequiredFieldValidator>
                    </td>
                    <td align="left" width="18%" style="font-weight: bold" class="blackblodtext"><asp:Literal ID="Literal9" Text="<%$ Resources:WebResources, SuperAdministrator_PortNumber%>" runat="server"></asp:Literal></td>
                    <td>
                        <asp:TextBox ID="txtLDAPServerPort" runat="server" CssClass="altText" Text="389"></asp:TextBox>
                        <span><asp:Literal ID="Literal10" Text="<%$ Resources:WebResources, SuperAdministrator_389isdefault%>" runat="server"></asp:Literal></span><%--ZD 103053--%>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="txtLDAPServerPort"
                            ErrorMessage="Required."></asp:RequiredFieldValidator>
                    </td>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator6" ValidationGroup="LDAP" ErrorMessage="<%$ Resources:WebResources, Required%>"
                        ControlToValidate="txtLDAPServerPort" Display="dynamic" runat="server" SetFocusOnError="true"></asp:RequiredFieldValidator>
               </tr>
               <tr>
                <td align="left" height="21" style="font-weight: bold" width="3%">
                </td>
                <%--Window Dressing--%>
                <td align="left" height="21" style="font-weight: bold" width="22%" class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, SuperAdministrator_AccountLogin%>" runat="server"></asp:Literal></td>
                <td style="height: 21px;" width="28%">
                    <asp:TextBox ID="txtLDAPAccountLogin" runat="server" CssClass="altText"></asp:TextBox>
                     <%--<asp:RegularExpressionValidator ID="RegularExpressionValidator2" ControlToValidate="txtLDAPAccountLogin"
                        Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="<%$ Resources:WebResources, InvalidCharacters2%>"
                        ValidationExpression="^(a-z|A-Z|0-9)*[^\\/<>+;?|!`,\[\]{}\x22;=^:@#$%&()']*$"></asp:RegularExpressionValidator>
                   <asp:RequiredFieldValidator ID="RequiredFieldValidator5" ValidationGroup="LDAP" ErrorMessage="Required" ControlToValidate="txtLDAPAccountLogin" Display="dynamic" runat="server" SetFocusOnError="true"></asp:RequiredFieldValidator>--%><%--PSU--%>
                </td>
                <%--Window Dressing--%>
                <td align="left" style="font-weight: bold" width="18%" class="blackblodtext"><asp:Literal ID="Literal11" Text="<%$ Resources:WebResources, SuperAdministrator_ConnectionTime%>" runat="server"></asp:Literal></td>
                <td style="height: 21px;" >
                    <asp:DropDownList ID="lstLDAPConnectionTimeout" runat="server" CssClass="altSelectFormat">
                        <asp:ListItem Value="10" Text="<%$ Resources:WebResources, 10Seconds%>"></asp:ListItem>
                        <asp:ListItem Value="20" Text="<%$ Resources:WebResources, 20Seconds%>"></asp:ListItem>
                        <asp:ListItem Value="30" Text="<%$ Resources:WebResources, 30Seconds%>"></asp:ListItem>
                        <asp:ListItem Value="60" Text="<%$ Resources:WebResources, 60Seconds%>"></asp:ListItem>
                    </asp:DropDownList>
                </td>
              </tr>
               <tr>
                    <td align="left" height="21" style="font-weight: bold" width="3%">
                    </td>
                    <%--Window Dressing--%>
                    <td align="left" height="21" style="font-weight: bold" width="22%" class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, SuperAdministrator_AccountPasswor%>" runat="server"></asp:Literal></td>
                    <td style="height: 21px;" width="28%">
                        <asp:TextBox ID="txtLDAPAccountPassword1" style="background-image:url('../image/pwdbg.png'); background-repeat:no-repeat" onchange="javascript:PreservePassword();" onblur="PasswordChange(2,1)" onfocus=" fnTextFocus(this.id,1,2)"
                            runat="server" CssClass="altText" TextMode="Password"></asp:TextBox>
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator3" Enabled="false"
                            ControlToValidate="txtLDAPAccountPassword1" Display="dynamic" runat="server"
                            SetFocusOnError="true" ErrorMessage="<%$ Resources:WebResources, InvalidCharacters2%>"
                            ValidationExpression="^(a-z|A-Z|0-9)*[^\\/<>+;?|!`,\[\]{}\x22;=^:@#$%&()']*$"></asp:RegularExpressionValidator>
                        <asp:CompareValidator ID="CompareValidator3" Display="dynamic" runat="server" ControlToValidate="txtLDAPAccountPassword1"
                            ControlToCompare="txtLDAPAccountPassword2" ErrorMessage="<%$ Resources:WebResources, Passwordsdonotmatch%>"
                            Font-Names="Verdana" Font-Size="X-Small" Font-Bold="False" />
                        <%--<asp:RequiredFieldValidator ID="reqLDAPPass" ValidationGroup="LDAP" ErrorMessage="Required" ControlToValidate="txtLDAPAccountPassword1" Display="dynamic" runat="server" SetFocusOnError="true"></asp:RequiredFieldValidator>--%><%--PSU--%>
                    </td>
                    <%--Window Dressing--%>
                    <td align="left" style="font-weight: bold" width="18%" class="blackblodtext"><asp:Literal ID="Literal12" Text="<%$ Resources:WebResources, SuperAdministrator_RetypePassword%>" runat="server"></asp:Literal></td>
                    <td style="height: 21px;">
                        <asp:TextBox ID="txtLDAPAccountPassword2" style="background-image:url('../image/pwdbg.png'); background-repeat:no-repeat" onchange="javascript:PreservePassword();" onblur="PasswordChange(2,2)" onfocus=" fnTextFocus(this.id,2,2)"
                            runat="server" CssClass="altText" TextMode="Password"></asp:TextBox>
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator4" Enabled="false"
                            ControlToValidate="txtLDAPAccountPassword2" Display="dynamic" runat="server"
                            SetFocusOnError="true" ErrorMessage="<%$ Resources:WebResources, InvalidCharacters2%>"
                            ValidationExpression="^(a-z|A-Z|0-9)*[^\\/<>+;?|!`,\[\]{}\x22;=^:@#$%&()']*$"></asp:RegularExpressionValidator>
                        <asp:CompareValidator ID="CompareValidator4" runat="server" ControlToValidate="txtLDAPAccountPassword2"
                            ControlToCompare="txtLDAPAccountPassword1" ErrorMessage="<%$ Resources:WebResources, Passwordsdonotmatch%>"
                            Font-Names="Verdana" Font-Size="X-Small" Font-Bold="False" />
                    </td>
                </tr>
               <tr>
                <td align="left" height="21" style="font-weight: bold" width="3%">
                </td>
                <%--Window Dressing--%>
                <td align="left" height="21" style="font-weight: bold" width="22%" class="blackblodtext" valign="top"><asp:Literal Text="<%$ Resources:WebResources, SuperAdministrator_LoginKey%>" runat="server"></asp:Literal></td>
                <td style="height: 21px;" width="28%" valign="top">
                    <asp:TextBox ID="txtLDAPLoginKey" runat="server" CssClass="altText"></asp:TextBox>
                     <%--<asp:RegularExpressionValidator ID="RegularExpressionValidator5" ControlToValidate="txtLDAPLoginKey"
                        Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="<%$ Resources:WebResources, InvalidCharacters2%>"
                        ValidationExpression="^(a-z|A-Z|0-9)*[^\\/<>+;?|!`,\[\]{}\x22;=^:@#$%&()']*$"></asp:RegularExpressionValidator>--%>
                </td>
                <%--Window Dressing--%>
                <td align="left" height="21" style="font-weight: bold" width="18%" class="blackblodtext" valign="top"><asp:Literal ID="Literal13" Text="<%$ Resources:WebResources, SuperAdministrator_SearchFilter%>" runat="server"></asp:Literal></td>
                <!--Added Regular expression by Vivek to perform junk character validation as a fix for issue number 315 -->
                <td style="height: 21px;">
                    <asp:TextBox ID="txtLDAPSearchFilter" runat="server" CssClass="altText"></asp:TextBox>
                    &nbsp;
                    <br />
                    <span style="color: #666666;"><asp:Literal ID="Literal14" Text="<%$ Resources:WebResources, SuperAdministrator_excnusersdcdom%>" runat="server"></asp:Literal></span>
                    <%--FB 2096--%>
                    <asp:RegularExpressionValidator ID="regLDAPSearchFilter" ControlToValidate="txtLDAPSearchFilter"
                        Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="<%$ Resources:WebResources, SuperAdministrator_invalidChar%>"
                        ValidationExpression="^(a-z|A-Z|0-9)*[^\\/+;?|`\[\]{}\x22;^:@#$%~']*$"></asp:RegularExpressionValidator>
                    <%--FB Case 491: Saima --%>
                </td>
        </tr>
               <tr style="display:none"> <%--FB 2993 LDAP--%>
                <td align="left" height="21" style="font-weight: bold" width="3%">
                </td>
                <%--Window Dressing--%>
                <td align="left" height="21" width="22%" class="blackblodtext" style="display: none"><asp:Literal Text="<%$ Resources:WebResources, SuperAdministrator_Synchronization%>" runat="server"></asp:Literal></td>
                <td align="left" colspan="4" style="font-weight: normal; color: Blue" width="28%">
                    <%--Window Dressing--%>
                    <asp:CheckBoxList ID="chkLstDays" runat="server" RepeatColumns="9" RepeatLayout="Flow"
                        Textalign="left" Style="display: none">
                        <asp:ListItem Text="<%$ Resources:WebResources, Monday%>" Value="1"></asp:ListItem>
                        <asp:ListItem Text="<%$ Resources:WebResources, Tuesday%>" Value="2"></asp:ListItem>
                        <asp:ListItem Text="<%$ Resources:WebResources, Wednesday%>" Value="3"></asp:ListItem>
                        <asp:ListItem Text="<%$ Resources:WebResources, Thursday%>" Value="4"></asp:ListItem>
                        <asp:ListItem Text="<%$ Resources:WebResources, Friday%>" Value="5"></asp:ListItem>
                        <asp:ListItem Text="<%$ Resources:WebResources, Saturday%>" Value="6"></asp:ListItem>
                        <asp:ListItem Text="<%$ Resources:WebResources, Sunday%>" Value="7"></asp:ListItem>
                    </asp:CheckBoxList>
                </td>
               </tr>
               <tr>
            <td align="left" height="21" style="font-weight: bold" width="3%">
            </td>
            <%--Window Dressing--%>
            <td align="left" height="21" style="font-weight: bold" width="22%" class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, SuperAdministrator_DomainPrefix%>" runat="server"></asp:Literal></td>
            <td align="left" width="28%">
                <asp:TextBox ID="txtLDAPPrefix" CssClass="altText" runat="server"></asp:TextBox>
                <%--<asp:RegularExpressionValidator ID="regConfName" ControlToValidate="txtLDAPPrefix"
                    Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="<%$ Resources:WebResources, SuperAdministrator_invalidChars%>"
                    ValidationExpression="^(a-z|A-Z|0-9)*[^<>+;?|!`,\[\]{}\x22;=^:@#$%&()~']*$"></asp:RegularExpressionValidator>--%>
            </td>
            <%--Window Dressing--%>
            <%--FB 2462 START--%>
            <td align="left" height="21" style="font-weight: bold" width="18%" class="blackblodtext" valign="top"><asp:Literal ID="Literal15" Text="<%$ Resources:WebResources, SuperAdministrator_Synchronization%>" runat="server"></asp:Literal></td>
            <td align="left">
                <mbcbb:ComboBox ID="lstLDAPScheduleTime" CssClass="altText" runat="server" Enabled="true" Rows="15" ValidationGroup="submit" AutoPostBack="false"><%--ZD 100284--%> <%--ZD 100420--%>
                    <asp:ListItem Value="01:00 AM" Selected="true"></asp:ListItem>
                    <asp:ListItem Value="02:00 AM"></asp:ListItem>
                    <asp:ListItem Value="03:00 AM"></asp:ListItem>
                    <asp:ListItem Value="04:00 AM"></asp:ListItem>
                    <asp:ListItem Value="05:00 AM"></asp:ListItem>
                    <asp:ListItem Value="06:00 AM"></asp:ListItem>
                    <asp:ListItem Value="07:00 AM"></asp:ListItem>
                    <asp:ListItem Value="08:00 AM"></asp:ListItem>
                    <asp:ListItem Value="09:00 AM"></asp:ListItem>
                    <asp:ListItem Value="10:00 AM"></asp:ListItem>
                    <asp:ListItem Value="11:00 AM"></asp:ListItem>
                    <asp:ListItem Value="12:00 PM"></asp:ListItem>
                    <asp:ListItem Value="01:00 PM"></asp:ListItem>
                    <asp:ListItem Value="02:00 PM"></asp:ListItem>
                    <asp:ListItem Value="03:00 PM"></asp:ListItem>
                    <asp:ListItem Value="04:00 PM"></asp:ListItem>
                    <asp:ListItem Value="05:00 PM"></asp:ListItem>
                    <asp:ListItem Value="06:00 PM"></asp:ListItem>
                    <asp:ListItem Value="07:00 PM"></asp:ListItem>
                    <asp:ListItem Value="08:00 PM"></asp:ListItem>
                    <asp:ListItem Value="09:00 PM"></asp:ListItem>
                    <asp:ListItem Value="10:00 PM"></asp:ListItem>
                    <asp:ListItem Value="11:00 PM"></asp:ListItem>
                    <asp:ListItem Value="12:00 AM" Selected="true"></asp:ListItem>
                </mbcbb:ComboBox> <%--ZD 100284--%>
                <asp:RequiredFieldValidator ID="reqlstLDAPScheduleTime" runat="server" ControlToValidate="lstLDAPScheduleTime:Text" ValidationGroup="submit"
                                                                SetFocusOnError="true" Display="Dynamic" ErrorMessage="<%$ Resources:WebResources, Required_Time%>"></asp:RequiredFieldValidator>
                <asp:RegularExpressionValidator ID="reglstLDAPScheduleTime" runat="server" ControlToValidate="lstLDAPScheduleTime:Text" ValidationGroup="submit"
                    SetFocusOnError="true" Display="Dynamic" ErrorMessage="<%$ Resources:WebResources, InvalidTime%>" ValidationExpression="[0-1][0-9]:[0-5][0-9] ?[A|a|P|p][M|m]"></asp:RegularExpressionValidator>
            </td>
            <%--FB 2462 END--%>
        </tr>
               <tr>
                    <td align="left" height="21" style="font-weight: bold" width="2%">
                    </td>
                    <%--Window Dressing--%>
                    <td align="left" height="21" width="18%" class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, SuperAdministrator_Authentication%>" runat="server"></asp:Literal></td>
                    <td align="left" colspan="4" style="font-weight: normal; color: Blue">
                        <%--Window Dressing--%>
                        <asp:DropDownList ID="drpAuthType" runat="server" Width="150px" RepeatColumns="9" RepeatLayout="Flow" Textalign="left">
                            <asp:ListItem Text="<%$ Resources:WebResources, None%>" Value="0"></asp:ListItem>
                            <asp:ListItem Text="<%$ Resources:WebResources, Signing%>" Value="1"></asp:ListItem>
                            <asp:ListItem Text="<%$ Resources:WebResources, Sealing%>" Value="2"></asp:ListItem>
                            <asp:ListItem Text="<%$ Resources:WebResources, Secure%>" Value="3"></asp:ListItem>
                            <asp:ListItem Text="<%$ Resources:WebResources, Anonymous%>" Value="4"></asp:ListItem>
                            <asp:ListItem Text="<%$ Resources:WebResources, Delegation%>" Value="5"></asp:ListItem>
                            <asp:ListItem Text="<%$ Resources:WebResources, Encryption%>" Value="6"></asp:ListItem>
                            <asp:ListItem Text="<%$ Resources:WebResources, FastBind%>" Value="7"></asp:ListItem>
                            <asp:ListItem Text="<%$ Resources:WebResources, ReadonlyServer%>" Value="8"></asp:ListItem>
                            <asp:ListItem Text="<%$ Resources:WebResources, SecureSocketsLayer%>" Value="9"></asp:ListItem>
                            <asp:ListItem Text="<%$ Resources:WebResources, ServerBind%>" Value="10"></asp:ListItem>
                        </asp:DropDownList>
                    </td>
               </tr>
               <tr>
                <td align="left" colspan="1" style="font-weight: bold" width="2%">
                </td>
                <td align="left" colspan="4" style="font-weight: bold; font-size: small; color: black;
                font-family: verdana; height: 21px; text-align: center;" >
                <button ID="btnTestLDAPConnection"  runat="server"  style="Width:160pt" Class="altLongBlueButtonFormat" Onserverclick="TestLDAPConnection" 
                ValidationGroup="LDAP" OnClientClick="javascript:DataLoading('1');" ><asp:Literal Text="<%$ Resources:WebResources, SuperAdministrator_btnTestLDAPConnection%>" runat="server"></asp:Literal></button><%--ZD 100420 --%>
               </td>
               </tr>
             </table>
             </td>
            </tr>

            <%--ZD 101443 End--%>
            <tr>
                <td align="center" colspan="5" style="font-weight: bold; font-size: small; color: black;
                    font-family: verdana;" height="21%">
                    <%--<input id="btnReset" type="reset" value="Reset" class="altMedium0BlueButtonFormat" runat="server"
                        onclick="javascript:fnReset();" />&nbsp;--%>
						<%--ZD 100420--%>
                        <button ID="btnReset" class="altMedium0BlueButtonFormat" runat="server" 
                             onclick="javascript:fnReset();"><asp:Literal Text='<%$ Resources:WebResources, Reset%>' runat='server' /></button>&nbsp; <%--ZD 100263--%>
                    <button runat="server" type="button" ID="btnSubmit" onserverclick="btnSubmit_Click" style="width:100pt"><asp:Literal Text='<%$ Resources:WebResources, OrganisationSettings_btnSubmit%>' runat='server' /></button><%-- FB 2796--%> <%--ZD 100176--%> <%--ZD 103492--%>
						<%--ZD 100420--%>
                </td>
            </tr>
            <input align="center" type="hidden" name="formname" id="formname" value="frmMainsuperadministrator" />
            <img src="keepalive.asp" name="myPic" alt="Keeplaive" width="1" height="1" style="display:none" /><%--ZD 100419--%>
            <tr>
                <%--FB 1849 Start--%>
                <%--FB 2719 Starts--%>
                <%--<td colspan="5" align="center">
                    <ajax:ModalPopupExtender ID="RoomPopUp" runat="server" TargetControlID="ChgOrg" BackgroundCssClass="modalBackground"
                        PopupControlID="switchOrgPnl" DropShadow="false" Drag="true" CancelControlID="ClosePUp">
                    </ajax:ModalPopupExtender>
                    <asp:Panel ID="switchOrgPnl" runat="server" HorizontalAlign="Center" Width="30%"
                        CssClass="treeSelectedNode">
                        <table width="100%" align="center" border="0">
                            <tr>
                                <td align="center" class="blackblodtext">
                                    <span class="subtitleblueblodtext">Switch Organization</span><br />
                                    <p>
                                        All the transactions performed will be for the below selected organization after
                                        the switch.
                                    </p>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    &nbsp;
                                </td>
                            </tr>
                            <tr>
                                <td align="center">
                                    <asp:DropDownList ID="DrpOrganization" DataTextField="OrganizationName" DataValueField="OrgId"
                                        runat="server" CssClass="altLong0SelectFormat" Width="200px">
                                    </asp:DropDownList>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    &nbsp;
                                </td>
                            </tr>
                            <tr>
                                <td align="center">
                                    <asp:Button runat="server" ID="BtnChangeOrganization" CssClass="altShortBlueButtonFormat"
                                        Text=" Submit " OnClientClick="javascript:return fnChangeOrganization();" OnClick="btnChgOrg_Click">
                                    </asp:Button>
                                    <input align="middle" type="button" runat="server" style="width: 100px; height: 21px"
                                        id="ClosePUp" value=" Close " class="altButtonFormat" onclick="javascript:fnClearOrg();" />
                                </td>
                            </tr>
                        </table>
                    </asp:Panel>
                </td>--%>
                <%--FB 2719 Ends--%>
            </tr>
            <%--FB 1849 End--%>
        </td>
    </tr>
    <%--FB 1982--%>
    </table>
    <%--ZD 101835--%>
    <table>
        <tr>
            <td>
            <div id="clientMask" class="iemask" style="z-index:1000; position:fixed; left:0px; top:0px; width:1366px; height:768px; display:none; background-color:White; opacity:0.5" ></div>
            <div id="divFlrPlanList" style="position:fixed; z-index:1001; width:700px; height:200px; background-color:#eee; display:none; border:3px solid gray; border-radius:15px; top:250px" >
            <table width="80%" align="center" border="0" cellpadding="5" >
                <tr>
                    <td align="center" colspan="2">
                        <span class="subtitleblueblodtext">
                            <asp:Literal Text="<%$ Resources:WebResources, ArchiveConfiguration%>" runat="server"></asp:Literal>
                        </span>
                    </td>
                </tr>
                <tr>
                    <td  class="blackblodtext" align="left">
                        <asp:Literal  Text="<%$ Resources:WebResources, ArchiveConfLbl1%>" runat="server"></asp:Literal>
                    </td>
                    <td align="left">
                            <asp:dropdownlist id="lstArchiveConfOld" runat="server" cssclass="alt2SelectFormat" Width="40px">
                            <asp:ListItem Value="1" Text="1"></asp:ListItem>
                            <asp:ListItem Value="2" Text="3"></asp:ListItem>
                            <asp:ListItem Value="3" Text="6" Selected="True"></asp:ListItem>
                            <asp:ListItem Value="4" Text="9"></asp:ListItem>
                            <asp:ListItem Value="5" Text="12"></asp:ListItem>
                        </asp:dropdownlist> <asp:Literal Text="<%$ Resources:WebResources, ExpressConference_months%>" runat="server"></asp:Literal>
                    </td>
                </tr>
                    <tr>
                    <td class="blackblodtext" align="left">
                        <asp:Literal Text="<%$ Resources:WebResources, ArchiveConfLbl2%>" runat="server"></asp:Literal>
                    </td>
                    <td align="left">
                            <asp:dropdownlist id="lstArchivePeriod" runat="server" cssclass="alt2SelectFormat">
                            <asp:ListItem Value="1" Text="<%$ Resources:WebResources, Monthly%>"></asp:ListItem>
                            <asp:ListItem Value="2" Text="<%$ Resources:WebResources, Quarterly%>" Selected="True"></asp:ListItem>
                            <asp:ListItem Value="3" Text="<%$ Resources:WebResources, SemiAnnually%>"></asp:ListItem>
                            <asp:ListItem Value="4" Text="<%$ Resources:WebResources, Annually%>"></asp:ListItem>
                        </asp:dropdownlist>
                    </td>
                </tr>
                    <tr>
                    <td class="blackblodtext" align="left">
                        <asp:Literal Text="<%$ Resources:WebResources, ArchiveConfLbl3%>" runat="server"></asp:Literal>
                    </td>
                    <td align="left">
                        <mbcbb:ComboBox ID="lstConfArchiveTime" runat="server" Rows="10" Width="100px"  CssClass="altText" style="position: fixed;" CausesValidation="True" AutoPostBack="false"></mbcbb:ComboBox>                                    
                    </td>
                </tr>
                <tr>
                    <td align="center" colspan="2">
                        <br />
                        <button align="middle" runat="server" id="btnConfArchiveClose" class="altMedium0BlueButtonFormat" onclick="javascript:return fnCloseSavedPlans();">
							<asp:Literal Text="<%$ Resources:WebResources, Close%>" runat="server"></asp:Literal>	
                        </button>
                        <button ID="btnConfArchiveSubmit" runat="server" onserverclick="SetConfArchiveConfiguration"  class="altMedium0BlueButtonFormat" ValidationGroup="Submit1">
							<asp:Literal  Text="<%$ Resources:WebResources, Submit%>" runat="server"></asp:Literal>
                        </button>
                    </td>
                </tr>
            </table>
            </div>
            </td>
        </tr>
    </table>

    <script language="javascript" type="text/javascript">

        //FB 3011
//        function fnFooterMsg() //FB 2681
//        {
//            if(FooterMsg.GetHtml() != null)
//                document.getElementById("hdnFooterMsg").value = FooterMsg.GetHtml();            
//        }

// FB 1710 Alignment change  Ends
//Code added fro FB 1428 - CSS Project
    function fnTransferPage()
    {
        //window.location.replace("UITextChange.aspx"); //FB 2565
        window.location = 'UITextChange.aspx';
    }
//FB 1830 - DeleteEmailLang start
  function fnDelEmailLan()
  {
    if (document.getElementById("txtEmailLang").value == "")
        return false;
    else
        return true;
  }
//FB 1830 - DeleteEmailLang end
//FB 1849
    function fnClearOrg()
    {
        var obj1 = document.getElementById('DrpOrganization');
        if(obj1)
        {
            obj1.value = '<%=orgId%>';
        }        
    }
    
    </script>

    </form>
</body>
</html>
<%--code added for Soft Edge button--%>

<script type="text/javascript" src="inc/softedge.js"></script>

<!-- #INCLUDE FILE="inc/mainbottomNET.aspx" -->
<!-- FB 2050 Start -->

<script type="text/javascript">

function scrollWindow()
{
//    document.getElementById("Button1").focus();
    //    refreshStyle();
    //FB 2738 Starts
    document.getElementById("topDiv1").style.display = 'none';
    scrollTo(0, 0);
    //FB 2738 Ends
    
    
}

function refreshImage()
{
    setTimeout("scrollWindow()",500); // FB 2738
   
  var obj = document.getElementById("mainTop");
  if(obj != null)
  {
      var src = obj.src;
      var pos = src.indexOf('?');
      if (pos >= 0) {
         src = src.substr(0, pos);
      }
      var date = new Date();
      obj.src = src + '?v=' + date.getTime();
      
      if(obj.width > 804)
      obj.setAttribute('width','804');
  }
  //refreshStyle();
  setMarqueeWidth();
  return false;
}

function refreshStyle()
{
	var i,a,s;
	a=document.getElementsByTagName('link');
	for(i=0;i<a.length;i++) {
		s=a[i];
		if(s.rel.toLowerCase().indexOf('stylesheet')>=0&&s.href) {
			var h=s.href.replace(/(&|\\?)forceReload=d /,'');
			s.href=h+(h.indexOf('?')>=0?'&':'?')+'forceReload='+(new Date().valueOf());
		}
	}
}

function setMarqueeWidth()
{
    var screenWidth = screen.width - 25;
    if(document.getElementById('martickerDiv')!=null)
        document.getElementById('martickerDiv').style.width = screenWidth + 'px';
        
    if(document.getElementById('marticDiv')!=null)
        document.getElementById('marticDiv').style.width = screenWidth + 'px';
    
    if(document.getElementById('marticker2Div')!=null)
        document.getElementById('marticker2Div').style.width = (screenWidth-15) + 'px';
    
    if(document.getElementById('martic2Div')!=null)
        document.getElementById('martic2Div').style.width = (screenWidth-15) + 'px';
}
//ZD 100420
if (document.getElementById('btnManageBatchRpt') != null)
    document.getElementById('btnManageBatchRpt').setAttribute("onblur", "document.getElementById('ImgApprover1').focus(); document.getElementById('ImgApprover1').setAttribute('onfocus', '');");
if (document.getElementById('btnCloudImport') != null)
    document.getElementById('btnCloudImport').setAttribute("onblur", "document.getElementById('ImgApprover1').focus(); document.getElementById('ImgApprover1').setAttribute('onfocus', '');");

//if (document.getElementById('lstWorkingHours') != null)
    
if (document.getElementById('lstEM7Orgsilo') != null)
    document.getElementById('lstEM7Orgsilo').setAttribute("onblur", "document.getElementById('btnBrowse').focus(); document.getElementById('btnBrowse').setAttribute('onfocus', '');");
else
    document.getElementById('lstWorkingHours').setAttribute("onblur", "document.getElementById('btnBrowse').focus(); document.getElementById('btnBrowse').setAttribute('onfocus', '');");
if (document.getElementById('btnUploadImages') != null)
    document.getElementById('btnBrowse').setAttribute("onblur", "document.getElementById('btnUploadImages').focus(); document.getElementById('btnUploadImages').setAttribute('onfocus', '');");
if (document.getElementById('btnRemoveMap1') != null)
    document.getElementById('btnBrowse').setAttribute("onblur", "document.getElementById('btnRemoveMap1').focus(); document.getElementById('btnRemoveMap1').setAttribute('onfocus', '');");

if (document.getElementById('txtTestEmailId') != null)
    document.getElementById('txtTestEmailId').setAttribute("onblur", "document.getElementById('btntestmail').focus(); document.getElementById('btntestmail').setAttribute('onfocus', '');");
if (document.getElementById('btntestmail') != null)
    document.getElementById('btntestmail').setAttribute("onblur", "document.getElementById('btnDefine').focus(); document.getElementById('btnDefine').setAttribute('onfocus', '');");
if (document.getElementById('ChkBlockEmails') != null)
    document.getElementById('ChkBlockEmails').setAttribute("onblur", "document.getElementById('BtnBlockEmails').focus(); document.getElementById('BtnBlockEmails').setAttribute('onfocus', '');");
//if (document.getElementById('drpEmailDateFormat') != null)
  //  document.getElementById('drpEmailDateFormat').setAttribute("onblur", "document.getElementById('btnReset').focus(); document.getElementById('btnReset').setAttribute('onfocus', '');");
if (document.getElementById('btnReset') != null)
    document.getElementById('btnReset').setAttribute("onblur", "document.getElementById('btnSubmit').focus(); document.getElementById('btnSubmit').setAttribute('onfocus', '');");
//if (document.getElementById('btnSubmit') != null)
    //document.getElementById('btnSubmit').setAttribute("onblur", "document.getElementById('Button1').focus(); document.getElementById('Button1').setAttribute('onfocus', '');");

window.onload = refreshImage;

// ZD 100580 START
var selVal;
function fnHandleChangeLang() {
    if (document.getElementById("delEmailLang") != null) {
        //var stat = confirm('<%= fnGetTranslatedText("All your email customization done in previous language will be lost. Still do you want to continue this operation?")%>');
        var stat = confirm(UserLanguage);
        if (stat) {
            document.getElementById("delEmailLang").click();
        }
        else {
            document.getElementById("drporglang").selectedIndex = selVal;
        }
    }
}
selVal = document.getElementById("drporglang").selectedIndex;
//ZD 100580 END

if(document.getElementById("Map1ImageCtrl") != null) //102438
    document.getElementById("Map1ImageCtrl").setAttribute("alt", "Mail Logo"); //ZD 100419

function fnFocusMenu() {
    if (document.getElementById("menuHome") != null)
        document.getElementById("menuHome").focus();
    else if (document.getElementById("menuHomeBlue") != null)
        document.getElementById("menuHomeBlue").focus();
    else
        document.getElementById("menuHomeRed").focus();
}

var glob = false;
function fnSetFocus() {
    if (navigator.userAgent.indexOf('Trident') > -1 && glob)
        document.getElementById("dxHTMLEditor_TC_T1T").focus();
    if (navigator.userAgent.indexOf('Chrome') > -1 && !glob) { // ZD 100369 508 Issue
        if (document.activeElement.tagName == "A")
            setTimeout('fnFocusMenu();', 100);
    }
    glob = true;
}

//ZD 101443 start
 document.getElementById("reglstLDAPScheduleTime").controltovalidate = "lstLDAPScheduleTime_Text"; //FB 2462
    document.getElementById("reqlstLDAPScheduleTime").controltovalidate = "lstLDAPScheduleTime_Text"; //FB 2462
    
    
    if (document.getElementById("lstLDAPScheduleTime_Text")) {
        var lstLDAPScheduleTime_Text = document.getElementById("lstLDAPScheduleTime_Text");
        lstLDAPScheduleTime_Text.onblur = function() {
        formatTimeNew('lstLDAPScheduleTime_Text', 'reglstLDAPScheduleTime',"<%=Session["timeFormat"]%>")
        };
    }
//ZD 101443 End

//ZD 102102 - Start
function OpenChangeUIDesign()
{    
    DataLoading(1);
    window.location = 'UISettings.aspx';
}

function OpenEmailDomain()
{    
    DataLoading(1);
    window.location = 'ManageEmailDomain.aspx';
}
//ZD 102138 Commented - Starts
//function OpenManageEntityCode()
//{    
//    DataLoading(1);
//    window.location = 'ManageEntityCode.aspx';
//}
//ZD 102138 Commented - End
//ZD 102102 - End
//ZD 102438 start
function fnBlurBrowse()
{
   if(document.activeElement.id == "btnBrowse")
       document.getElementById("btnBrowse").blur();
}

document.onmousemove = fnBlurBrowse;
//ZD 102438 End

//ZD 101835
 setTimeout("document.getElementById('lstConfArchiveTime_Text').style.width = '88px';",1000);
 setTimeout("document.getElementById('lstConfArchiveTime').style.position = 'fixed';",1000);
 document.getElementById("lstConfArchiveTime_Text").readOnly = 'true';
 
function fnOpenSavedPlans() {
    document.getElementById("clientMask").style.display = "block";
    document.getElementById("divFlrPlanList").style.display = "block";
    document.getElementById('lstArchiveConfOld').focus()
    return false;
}

function fnCloseSavedPlans() {
    document.getElementById("clientMask").style.display = "none";
    document.getElementById("divFlrPlanList").style.display = "none";
    return false;
}
 
function fnSetDimension() {
try {
    var w = screen.width;
    var h = screen.height;
    document.getElementById("clientMask").style.width = w + 'px';
    document.getElementById("clientMask").style.height = h + 'px';
    document.getElementById("divFlrPlanList").style.left = Math.round((w - 800) / 2) + 'px';
}
catch (ex) {
    console.log(ex);
}
}
fnSetDimension();
</script>

<!-- FB 2050 End -->
