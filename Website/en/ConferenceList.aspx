<%--ZD 100147 start--%>
<%--/* Copyright (C) 2015 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/--%>
<%--ZD 100147 ZD 100886 End--%>
<%@ Page Language="C#" Inherits="ns_ConferenceList.ConferenceList" ValidateRequest="true" EnableEventValidation="false"%><%--ZD 100170--%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> <!-- FB 2050 -->
<meta http-equiv="X-UA-Compatible" content="IE=8" /> <!-- FB 2050 -->
<% 
if (Request.QueryString["hf"] == null) 
{ 
%>
<!-- #INCLUDE FILE="inc/maintopNET.aspx" -->
<%--ZD 101388 Commented--%>
<%--<!-- FB 2719 Starts -->
<% if (Session["isExpressUser"].ToString() == "1"){%>
<!-- #INCLUDE FILE="inc/maintopNETExp.aspx" -->
<%}else{%>
<!-- #INCLUDE FILE="inc/maintopNET.aspx" -->
<%}%>
<!-- FB 2719 Ends -->--%>

<% 
} 
else 
{ 
    if (Request.QueryString["hf"].ToString().Equals("1")) 
    {
        
        %>
        <!-- #INCLUDE FILE="inc/maintopNET4.aspx" --> <%--Login Management--%>
        <%  
    }
} %>
    <%--ZD 101388 removed--%>

<script type="text/javascript">    // FB 2790
    var path = '<%=Session["OrgCSSPath"]%>';
    if (path == "")
        path = "Organizations/Org_11/CSS/Mirror/Styles/main.css";
    path = path.split('.')[0] + '<%=Session["ThemeType"]%>' + ".css"; // FB 2815
    document.write("<link rel='stylesheet' title='Expedite base styles' type='text/css' href='" + path + "'?'" + new Date().getTime() + "'>"); //ZD 100152
</script>

<%--ZD 100221 Starts--%>
<script type="text/javascript">
    function fnshow(obj1, obj2, obj3) {
    if (obj1 == 1 && obj3 == 0) { //ZD 100221_17Feb 
        alert(ConfListwebexen); return false;
    }
    else {
        if (obj1 == 1 && obj2 == 0 && obj3 == 1) {
            alert(ConfListwebexen); return false;
        }
    }
        
       
       
    }
</script>
<%--ZD 100221 Ends--%>
<script type="text/javascript">
    //ZD 100604 start
    var img = new Image();
    img.src = "../en/image/wait1.gif";
    //ZD 100604 End
	//FB 2822
    function ChangeSortingOrder(par) {        
        DataLoading('1'); 
        var SortingId = par.split("_")[2];
        if (SortingId.indexOf('spnAscending') > -1)
            document.getElementById("hdnSortingOrder").value = "1";        
        else            
            document.getElementById("hdnSortingOrder").value = "0";
        

    } 
    //ZD 103174 - Start
    function GetbrowserLang()
    {
        var browserlang = navigator.userLanguage || navigator.language;

        if (browserlang != "" && browserlang != null)        
            document.getElementById("hdnBrowserLang").value = browserlang;
        else
            document.getElementById("hdnBrowserLang").value = "en-US";
    }
    //ZD 103174 - End

  var servertoday = new Date();

//FB Case 680 Saima this function is introduced to avoid approve conference getting
// called if no conference has been selected.
  function CountChecked() 
  {
      var count = 0;
      var elements = document.getElementsByTagName('input');
      for (i = 0; i < elements.length; i++) {
          if ((elements.item(i).type == "radio") && (elements.item(i).id.indexOf("All") < 0) && (elements.item(i).checked))
              count++;
      }
      if (count <= 0) {
          //added for FB 1428 Start

          if ('<%=Application["Client"]%>' == "MOJ")
              alert(ConfListRecur);
          else
          //added for FB 1428 End	`
              alert(ConfListRecurr);

      }
      document.getElementById("txtSelectionCount").value = count;      
      //ZD 101445       
      if("<%=Session["RoomDenialCommt"]%>" == 1)
      {    
            //var x = document.getElementsByTagName('textarea');
            var Cmmtstatus = true;
            for (i = 0; i < elements.length; i++) 
            {
                if ((elements.item(i).type == "radio") && (elements.item(i).id.indexOf("All") < 0) && (elements.item(i).id.indexOf("rdDeny") > -1) && (elements.item(i).checked))
                {                
                    var y = elements.item(i).id.split('_');

                    var reqtxtMsg = "dgConferenceList_" + y[1] + "_dgResponse_" + y[3] + "_reqtxtMessage";
                    var txtMsg = "dgConferenceList_" + y[1] + "_dgResponse_" + y[3] + "_txtMessage";

                    var reqInstancetxtMsg = "dgConferenceList_" + y[1] + "_dgInstanceList_" + y[3] + "_dgResponse_"+ y[5] + "_reqtxtMessage";
                    var InstancetxtMsg = "dgConferenceList_" + y[1] + "_dgInstanceList_" + y[3] + "_dgResponse_"+ y[5] + "_txtMessage"; 
                    
                    if(document.getElementById(reqtxtMsg)!= null)
                        document.getElementById(reqtxtMsg).style.display = 'none';
                    if(document.getElementById(reqInstancetxtMsg)!= null)
                        document.getElementById(reqInstancetxtMsg).style.display = 'none';

                    if (document.getElementById(InstancetxtMsg) != null)
                    {
                        if (document.getElementById(InstancetxtMsg).value != "") 
                        {
                            document.getElementById(reqInstancetxtMsg).style.display = 'none';
                        }
                        else 
                        {
                            document.getElementById(reqInstancetxtMsg).style.display = 'block';
                            Cmmtstatus = false;
                        }
                    }
                                
                    if (document.getElementById(txtMsg) != null)
                    {
                        if (document.getElementById(txtMsg).value != "") 
                        {
                            document.getElementById(reqtxtMsg).style.display = 'none';
                        }
                        else 
                        {
                            document.getElementById(reqtxtMsg).style.display = 'block';
                            Cmmtstatus = false;
                        }
                    }
                }
            }
            return Cmmtstatus;
        }
        return true;  
}
//FB 2448 - Starts
function viewconfMCUinfo(cid)
{
	url = "ConfMCUInfo.aspx?t=hf&confid=" + cid;
	confMCUdetail = window.open(url, "viewconfMCUinfo", "status=no,width=700,height=400,scrollbars=yes,resizable=yes");
	confMCUdetail.focus();
}
//FB 2448 - End

function goToCal()
{
		//ZD 101388 Commented
       /* if(document.getElementById("lstCalendar") != null)
        {
		    if (document.getElementById("lstCalendar").value == "1"){
			    window.location.href = "PersonalCalendar.aspx?v=1&r=1&hf=&d=" ; //code changed for calendar conversion FB 412
		    }
		    if (document.getElementById("lstCalendar").value == "3"){
			    window.location.href = "roomcalendar.aspx?v=1&r=1&hf=&d=&pub=&m=&comp=" ; //code changed for calendar conversion FB 412
		    }
		}*/
        
		
}
function DataLoading(val)
{
//alert(val);
    if (val=="1")
        document.getElementById("dataLoadingDIV").style.display = 'block'; //ZD 100678
    else
        document.getElementById("dataLoadingDIV").style.display = 'none'; //ZD 100678
}

function roomcalendarview()
{
	window.location.href = "dispatcher/admindispatcher.asp?cmd=ManageConfRoom&f=v";
}

function personalcalendarview()
{
	//window.location.href = "calendarpersonaldaily.aspx?v=1&r=1&hf=&d=" ; //code changed for calendar conversion FB 412
                    window.location.href = "PersonalCalendar.aspx?v=1&r=1&hf=&d=" ; //code changed for calendar conversion FB 412
}

function UndecideAll(obj)
{
  if (obj.tagName == "INPUT" && obj.type == "radio" && obj.checked) 
    {
        var elements = document.getElementsByTagName('input'); 
        for (i=0;i<elements.length;i++)
        if ( (elements.item(i).type == "radio") && (elements.item(i).id.indexOf("rdUndecided") >= 0) ) 
        {
            if(elements.item(i).id!=obj.id) 
            {
                elements.item(i).checked= true; 
            }
        } 
    }
}
function ApproveAll(obj)
{
  if (obj.tagName == "INPUT" && obj.type == "radio" && obj.checked) 
    {
        var elements = document.getElementsByTagName('input'); 
        for (i=0;i<elements.length;i++)
        if ( (elements.item(i).type == "radio") && (elements.item(i).id.indexOf("rdApprove") >= 0) ) 
        {
            if(elements.item(i).id!=obj.id) 
            {
                elements.item(i).checked= true; 
            }
        } 
    }
}
function DenyAll(obj)
{
//alert(obj.checked);
  if (obj.tagName == "INPUT" && obj.type == "radio" && obj.checked) 
    {
        var elements = document.getElementsByTagName('input'); 
        for (i=0;i<elements.length;i++)
        if ( (elements.item(i).type == "radio") && (elements.item(i).id.indexOf("rdDeny") >= 0) ) 
        {
            if(elements.item(i).id!=obj.id) 
            {
                elements.item(i).checked= true; 
            }
        } 
    }
}
function viewconf(cid) {
	url = "ManageConference.aspx?t=hf&confid=" + cid;
	confdetail = window.open(url, "viewconference", "width=1,height=1,resizable=yes,scrollbars=yes,status=no");
	confdetail.focus();
}

function viewapprovalstatus(cid, m, d)
{
	//code added for FB 412 - Start
	//url = "dispatcher/gendispatcher.asp?cmd=GetApprovalStatus&cid=" + cid + "&m==" + m + "&d=" + d;
	m = m.replace("�","'"); //FB 2321
	url = "approvalstatus.aspx?confid=" + cid + "&m=" + m + "&d=" + d;
	//code added for FB 412 - End
	approvalwin = window.open(url, "approvalstatus", "status=no,width=700,height=400,scrollbars=yes,resizable=yes");
	approvalwin.focus()
}

//Code added for FB 1391 -- Start

function CustomEditAlert()
{

  
    var args = CustomEditAlert.arguments;
    //ZD 101597
    var act = true;
    
    if(args != null)
    { 
        var msg = "Some instances of this series have a unique Start Time/End Time.Edit All will globally change the Start Time/End Time for all instances of this series.Press OK to proceed and enter a new Start Time/End Time, or press Cancel to edit individual instances."
        
        if (args[0] == "Y" )
        {
            act = confirm(msg);
            
            if(!act)
                DataLoading('0')
                
           //return act;
        }
    }  

    //ZD 101597
    if(act == true && parseInt('<%=Session["admin"]%>') > 0)
        return fnSelectForm(args[1]);
    else
    {
        if(act)
            DataLoading('1');        
        return act;
    }
    //return true;
}
//Code added for FB 1391 -- End
//FB 2382
function fnCheck()
{
    var chkAllSilo = document.getElementById("chkAllSilo");  
    var hdnChkSilo = document.getElementById("hdnChkSilo");  
    
    if(chkAllSilo)
    {
        if(chkAllSilo.checked == true)
            hdnChkSilo.value = "1";
        else
            hdnChkSilo.value = "0";
    }    
}

//FB 2670
function setPrint() {
    window.open("PrintInterface.aspx", "myVRM", 'status=yes,width=750,height=400,scrollbars=yes,resizable=yes');
    return false;
}
//ZD 100642 Start
function fnInvokePopup(link, xid) {
    if (link != "") 
    {
		//ZD 100718 Starts
        if (link.indexOf("Bridgelist.aspx?") < 0) 
        {
            xid = xid.replace("rdAlternate", "updtLink");
            link = link + "&hdnLnkBtnId=" + xid;
            $('#popupdiv').fadeIn();
            $("#RoomList").attr("src", link);
            $('#PopupRoomList').show();
            $('#PopupRoomList').bPopup({
                fadeSpeed: 'slow',
                followSpeed: 1500,
                modalColor: 'gray'
            });
        }
        else 
        {
            xid = xid.replace("rdAlternate", "updtLink");
            link = link + "&hdnLnkBtnId=" + xid;
            $('#popupdiv').fadeIn();
            $("#MCUList").attr("src", link);
            $('#PopupMCUList').show();
            $('#PopupMCUList').bPopup({
                fadeSpeed: 'slow',
                followSpeed: 1500,
                modalColor: 'gray'
            });
        }//ZD 100718 End
    }  
}

//ZD 101315-Extend conf- Starts
function ShowExtendconf(cid) {
    document.getElementById("hdnConfid").value = cid;
    $('#popupdiv').fadeIn();
    $('#popmsg').show();
    $('#popmsg').bPopup({
        fadeSpeed: 'slow',
        followSpeed: 1500,
        modalColor: 'gray'
    });
}

function HideExtendconf() {
    $('#popupdiv').fadeOut();
    $('#popmsg').hide();
    setTimeout("DataLoading(1);__doPostBack('btnRefreshList', '')", "1500");
}

function Extendconf() {
    document.getElementById("hdnMins").value = $('#msgMinutes').val();
}

function refresh() {
    setTimeout("DataLoading(1);__doPostBack('btnRefreshList', '')", "1500");
}
//ZD 101315-Extend conf- End

function fnTriggerFromPopup(btnid) {
    $('#popupdiv').fadeOut();
    $('#PopupRoomList').hide();
    document.getElementById(btnid).click();
}
//ZD 100642 End
</script>

<script type="text/javascript" src="script/RoomSearch.js"></script> <%--ZD 100642--%>   

<script type="text/javascript" src="script/CallMonitorJquery/jquery.1.4.2.js"></script>

<script type="text/javascript" src="script/CallMonitorJquery/jquery.bpopup-0.7.0.min.js"></script>

<script type="text/javascript" src="script/CallMonitorJquery/json2.js"></script>
<script type="text/javascript" src="../<%=Session["language"] %>/inc/functions.js"></script> <%--ZD 101597--%>

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">

    <title>Conference List</title>
</head>
<body>
    <form id="frmSearchConference" autocomplete="off" runat="server" method="post"><%--ZD 101190--%>
    <%--ZD 101022 start--%>
        <asp:ScriptManager ID="scpMgrUI" runat="server" EnableScriptLocalization="true" >
		    <Scripts>                
			    <asp:ScriptReference Path= "~/ResourceScript/StringResources.js" ResourceUICultures="<%$ Resources:WebResources, UICulture%>"  />
		    </Scripts>
	    </asp:ScriptManager> <%--ZD 101022 End--%>
    <div id="divconflistheight"> <%--ZD 100393--%>
    <input type="hidden" id="hdnJoinID" runat="server" /> <%--FB 1934--%>
    <input type="hidden" id="hdnChkSilo" runat="server" />  <%--FB 2382--%>
    <input type="hidden" id="hdnFiltertype" runat="server" />  <%--FB 2639_Search--%>
    <input name="locstrname" type="hidden" id="locstrname" runat="server" /><%--ZD 100642--%>   
        <input type="hidden" id="hdnTempID" runat="server" /> <%--ZD 101597--%> 
        <input type="hidden" id="hdnEditID" runat="server" /> <%--ZD 101597--%> 
        <input type="hidden" id="hdnEditForm" runat="server" /> <%--ZD 101597--%> 
        <input type="hidden" id="hdnBrowserLang" runat="server" /><%--ZD 103174--%> 

        <center><table border="0" width="98%" cellpadding="2" cellspacing="2">
            <tr id="trlstCalendar" runat="server" ><%--FB 2968--%>
                <td align="center">
                    <h3><asp:Label ID="lblHeader" runat="server" Text="<%$ Resources:WebResources, ConferenceList_lblHeader%>"></asp:Label>
                            <%--ZD 101388 Commented--%>
	                        <%--<asp:DropDownList runat="server" ID="lstCalendar" class="altText" ToolTip="View" onChange="goToCal();javascript:DataLoading1('1');">
                                <asp:ListItem Value="2" Text="<%$ Resources:WebResources, PersonalCalendar_lstcal2%>"></asp:ListItem>
                                <asp:ListItem Value="1" Text="<%$ Resources:WebResources, PersonalCalendar_lstcal1%>"></asp:ListItem>
                                <asp:ListItem Value="3" Text="<%$ Resources:WebResources, PersonalCalendar_lstcal3%>"></asp:ListItem>
                            </asp:DropDownList>--%>
                      
                    </h3>
                    <asp:Label ID="lblTimezone" runat="server" Visible="false" CssClass="subtitleblueblodtext"></asp:Label>
                    <br />
                    <asp:Label ID="errLabel" runat="server" CssClass="lblError"></asp:Label>
                    <div id="dataLoadingDIV" name="dataLoadingDIV" align="center" style="display:none">
                        <img border='0' src='image/wait1.gif' alt='Loading..' />
                     </div><%--ZD 100678--%>
                    <asp:Button ID="btnRefreshList" runat="server" Visible="false" OnClick="Updatelist" />
                    <asp:CheckBox ID="Refreshchk" runat="server" style="display:none" />
                </td>
                
            </tr>
            <tr id="PrintRow" runat="server"> <%--FB 2670--%>
                <td align="right">
                    <asp:ImageButton ID="ImgPrinter" ValidationGroup="group" OnClientClick='Javscript:return setPrint();'
                        runat="server" src="image/print.gif" alt="Print Report" Style="cursor: hand;
                        vertical-align: middle;" ToolTip="<%$ Resources:WebResources, ManageConference_Print%>" /> &nbsp; <%--FB 3034--%>
                    <asp:ImageButton ID="btnPDF" ValidationGroup="group" src="image/adobe.gif" runat="server" OnClick="ExportPDF"
                        Style="vertical-align: middle;" ToolTip="<%$ Resources:WebResources, ManageConference_btnPDF%>" AlternateText="Export To PDF" />&nbsp; <%--ZD 100419--%>
                    <asp:ImageButton ID="btnExcel" ValidationGroup="group" OnClick="ExportExcel" src="image/excel.gif"
                        runat="server" OnClientClick="document.getElementById('hdnChartPrint').value = ''"
                        Style="vertical-align: middle;" ToolTip="<%$ Resources:WebResources, ExporttoExcel%>" AlternateText="Export to Excel" /> &nbsp; <%--ZD 100419--%>
                </td>
                
            </tr>
            <tr><%--rss changes--%>
                <td  align="right" runat="server" id="PublicRSS"> 
                      <asp:ImageButton ID="ImageButton1" AlternateText="<%$ Resources:WebResources, PublicRSSFeed%>"  src="image/rss.gif"  runat="server"  ToolTip="<%$ Resources:WebResources, PublicRSSFeed%>" OnClick="RSSFeed" /> <%--ZD 100419--%> <%--ZD 103531--%>
                </td>
            </tr>
            <%--Added for Org - Start--%>
            <tr id="OrgRow" runat="server" style="display:none;">
                <td> 
                    <table width="95%" align="center" border="0">
                        <tr align="left"> <!-- FB 2050 -->
                            <td align="left" class="subtitleblueblodtext" width="10%"><asp:Label id="lblOrgNames" runat="server" text="<%$ Resources:WebResources, ConferenceList_lblOrgNames%>"></asp:Label> <!-- FB 2050 -->
                            </td>
                            <td valign="bottom" align="left"> <!-- FB 2050 -->
                                 <asp:DropDownList CssClass="altSelectFormat" ID="drpOrgs" runat="server" DataTextField="OrganizationName" OnSelectedIndexChanged="OrgIndexChanged" DataValueField="OrgID" AutoPostBack="true"></asp:DropDownList> <%--Edited for FF--%>
                            </td>
                        </tr>
                     </table>
                 </td>
            </tr>
            <%--Added for Org - End--%>
            <tr>
                <td>
                    <table width="90%" runat="server" id="tblLists">
                        <tr>
                            <%--Window Dressing--%>
                            <td width="50%" align="left" class="blackblodtext"><asp:Label runat="server" ID="lblConfTypeLabel" Text="<%$ Resources:WebResources, ConferenceList_lblConfTypeLabel%>" ></asp:Label><%--FB 2579--%>
                                <asp:DropDownList ID="lstListType" runat="server" Width="200px" CssClass="altSelectFormat" OnSelectedIndexChanged="ChangeListType" AutoPostBack="true" onchange="javascript:DataLoading('1');">
                                    <asp:ListItem Selected="True" Text="<%$ Resources:WebResources, OngoingConferences%>" Value="2"></asp:ListItem>
                                    <asp:ListItem Selected="False" Text="<%$ Resources:WebResources, OnMCUConferences%>" Value="8"></asp:ListItem><%--ZD 100036--%><%--ZD 102200--%>
                                    <asp:ListItem Selected="False" Text="<%$ Resources:WebResources, Reservations%>" Value="3"></asp:ListItem>
                                    <%-- //Note: Deleted and Terminated Conferences -> 9
                                         //Note: Completed Conferences -> 10  --%>
                                    <asp:ListItem Selected="False" Text="<%$ Resources:WebResources, WaitListConferences%>" Value="11"></asp:ListItem>
                                    <asp:ListItem Selected="False" Text="<%$ Resources:WebResources, PublicConferences%>" Value="4"></asp:ListItem>
                                    <asp:ListItem Selected="False" Text="<%$ Resources:WebResources, PendingConferences%>" Value="5"></asp:ListItem>
                                    <asp:ListItem Selected="False" Text="<%$ Resources:WebResources, ApprovalPending%>" Value="6"></asp:ListItem>
                                    <asp:ListItem Selected="False" Text="<%$ Resources:WebResources, ConferenceSupport1%>" Value="7"></asp:ListItem> <%--FB 2501 VNOC--%><%--FB 2844 ZD 101092--%>
                                    <asp:ListItem Selected="False" Text="<%$ Resources:WebResources, HDConfList_HDReservations%>" Value="10"></asp:ListItem> <%--ALLDEV-807--%>
                                </asp:DropDownList><!-- FB 2570 -->       
                            </td>
                            <td class="blackblodtext" width="11%" nowrap="nowrap" id="tdchkSilo"><asp:Literal Text="<%$ Resources:WebResources, ConferenceList_tdchkSilo%>" runat="server"></asp:Literal><%--FB 2274--%><%--FB 2843--%>
                            <asp:CheckBox ID="chkAllSilo" runat="server" OnClick="fnCheck()" OnCheckedChanged="CheckAllSilo"  AutoPostBack="true"/>  <%--FB 2382--%><%--FB 2843--%>                             
                            </td>
                            <td  align="right" runat="server" id="PrivateRSS"> <%--rss changes--%>
                              <asp:ImageButton ID="ImageButton3" AlternateText="Private RSS Feed" src="image/rss.gif"  runat="server"  ToolTip="<%$ Resources:WebResources, PrivateRSSFeed%>" OnClick="PrivateRSSFeed" /> <%--ZD 100419--%>
                            </td>
                        </tr>
                    </table>
                    
                    
                </td>
            </tr>
            <tr>
                <%--UI Changes for ZD 101478--%>
                <td align="center" colspan="2">
                <asp:DataGrid ID="dgConferenceList" AllowSorting="true" runat="server" AutoGenerateColumns="False" Font-Names="Verdana" Font-Size="Small" ShowFooter="true"
                 Width="100%"  style="border-collapse:separate" BorderStyle="None" BorderWidth="0px" GridLines="None" OnItemDataBound="InterpretRole" OnItemCreated="BindRowsDeleteMessage"
                 OnEditCommand="EditConference" OnCancelCommand="DeleteConference" OnDeleteCommand="ManageConference" OnUpdateCommand="CloneConference"><%--Edited For FF--%><%--FB 1982--%>
                    <ItemStyle CssClass="ConferenceTableOddRows" Height="15" VerticalAlign="Top" /><%--ALLDEV-838--%>
                    <AlternatingItemStyle CssClass="ConferenceTableEvenRows" Height="15" /><%--ALLDEV-838--%>
                    <HeaderStyle CssClass="ConferenceTableHeader" Height ="30"/><%--ALLDEV-838--%>            
                    <Columns>
                        <asp:BoundColumn DataField="ConferenceID" Visible="false"></asp:BoundColumn>
                        <asp:BoundColumn DataField="IsHost" Visible="false"></asp:BoundColumn>
                        <asp:BoundColumn DataField="IsParticipant" Visible="false"></asp:BoundColumn>
                        <asp:BoundColumn DataField="ConferenceDuration" Visible="false"></asp:BoundColumn>
                        <asp:BoundColumn DataField="ConferenceStatus" Visible="false"></asp:BoundColumn>
                        <asp:BoundColumn DataField="IsRecur" Visible="false"></asp:BoundColumn>
                        <asp:BoundColumn DataField="OpenForRegistration" Visible="false"></asp:BoundColumn>
                        <asp:BoundColumn DataField="Conference_Id" Visible="false"></asp:BoundColumn>
                        <asp:BoundColumn DataField="ConferenceDateTime" Visible="false"></asp:BoundColumn>
                        <asp:BoundColumn DataField="organizationName" Visible="false"></asp:BoundColumn>
                        <asp:TemplateColumn ItemStyle-Width="100%" HeaderStyle-Width="100%" FooterStyle-HorizontalAlign="Right">
                            <HeaderTemplate>
                                <table width="100%" >
                                    <tr class="ConferenceTableHeader" runat="server" visible='<%# Request.QueryString["t"] == "6" %>'><%--ALLDEV-838--%>
                                          <%--ZD  100821 start--%>
                                        <td width="5%"  class="ConferenceTableHeader">&nbsp;</td><%--ALLDEV-838--%>
                                        <td width="10%" class="ConferenceTableHeader">&nbsp;</td><%--ALLDEV-838--%>
                                        <td width="10%" class="ConferenceTableHeader">&nbsp;</td><%--ALLDEV-838--%>
                                        <td width="10%" class="ConferenceTableHeader">&nbsp;</td><%--ALLDEV-838--%>
                                        <td width="8%" class="ConferenceTableHeader" >&nbsp;</td><%--ALLDEV-838--%>
                                        <td width="5%" class="ConferenceTableHeader">&nbsp;</td><%--ALLDEV-838--%>
                                        <td width="5%" class="ConferenceTableHeader" >&nbsp;</td><%--ALLDEV-838--%>
                                        <td width="5%" class="ConferenceTableHeader" visible='<%# (Request.QueryString["t"] != "6") %>'>&nbsp;</td><%--ALLDEV-838--%>
                                        <td width="5%" class="ConferenceTableHeader" visible='<%# (Request.QueryString["t"] != "6") %>'>&nbsp;</td><%--ALLDEV-838--%>
                                        <td width="10%" class="ConferenceTableHeader" visible='<%# (Request.QueryString["t"] != "6") %>'>&nbsp;</td><%--FB 2274--%><%--ALLDEV-838--%>
                                        <td width="27%" class="ConferenceTableHeader" visible='<%# (Request.QueryString["t"] != "6") %>'>&nbsp;</td><%--ALLDEV-838--%>
                                        <%--ZD  100821 End--%>
                                        <td align="left" width="47%" >
                                            <table id="tblappr1" width="100%" border="0" cellpadding="0" cellspacing="0"> <%-- FB 2987 --%>
                                                <tr>
                                                    <td width="20%"><asp:Literal ID="Literal9" Text='<%$ Resources:WebResources, RoomTree_Level%>' runat='server'></asp:Literal></td>
                                                    <td width="60%">
                                                        <table width="100%" cellpadding="0" cellspacing="0" border="0"><%--ZD 100642_11jan2014--%><%--ZD 100718--%>
                                                            <tr>
                                                                <td class="ConferenceTableHeader" align="center"><asp:Literal Text="<%$ Resources:WebResources, ConferenceList_Undecided%>" runat="server"></asp:Literal></td><%--ALLDEV-838--%>
                                                                <td class="ConferenceTableHeader" align="left"><asp:Literal Text="<%$ Resources:WebResources, ConferenceList_Approve%>" runat="server"></asp:Literal></td><%--ALLDEV-838--%>
                                                                <td class="ConferenceTableHeader" align="left"><asp:Literal ID="Literal2" Text="<%$ Resources:WebResources, Alternate%>" runat="server"></asp:Literal></td> <%--ZD 100642 --%><%--ALLDEV-838--%>
                                                                <td class="ConferenceTableHeader" align="left"><asp:Literal Text="<%$ Resources:WebResources, ConferenceList_Deny%>" runat="server"></asp:Literal></td><%--ALLDEV-838--%>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                    <td align="center" width="20%"><asp:Literal ID="Literal3" Text="<%$ Resources:WebResources, Comment%>" runat="server"></asp:Literal>&nbsp;&nbsp;&nbsp;</td> <%--ZD 100425--%>
                                                </tr>
                                            </table>
                                        </td>
                                        <td width="1%" class="ConferenceTableHeader"></td><%--ALLDEV-838--%>
                                    </tr>
                                    <tr class="ConferenceTableHeader"><%--ALLDEV-838--%>
                                    <%--Window Dressing--%>
                                        <td width="5%" runat="server" id="tdID" align="left" class="ConferenceTableHeader"><%--FB 2274--%><%--ALLDEV-838--%>
                                            <asp:Literal Text="<%$ Resources:WebResources, ConferenceList_tdID%>" runat="server"></asp:Literal><asp:LinkButton  ID="btnSortID" runat="server" CommandArgument="1" OnCommand="SortGrid" OnClientClick="ChangeSortingOrder(this.childNodes[0].id);"><%if (Request.ServerVariables["HTTP_USER_AGENT"].ToString().ToUpper().Contains("TRIDENT")) { %> <asp:Label id="spnAscendingIDIE" runat="server" style="font-family:Wingdings; font-weight:bolder;">&#233;</asp:Label><asp:Label id="spnDescndingIDIE" runat="server" style="font-family:Wingdings; font-weight:bolder;">&#234;</asp:Label><% } else { %><asp:Label id="spnAscendingID" runat="server" style="font-family:Wingdings; font-weight:bolder; font-size:x-large;">&#8593;</asp:Label><asp:Label id="spnDescendingID" runat="server" style="font-family:Wingdings; font-weight:bolder; font-size:x-large;">&#8595;</asp:Label><% } %></asp:LinkButton> <%--Edited for FF--%><%--FB 2822 --%> <%--ZD 100381--%>
                                        </td>
                                        <%--Added for FB 1428 Start--%>
                                                <%if (Application["Client"] == "MOJ")
                                                  { %>
                                                  <td width="10%" runat="server" id="td5" align="left" class="ConferenceTableHeader"> <%--Window Dressing--%><%--ALLDEV-838--%>
                                           <asp:Literal Text="<%$ Resources:WebResources, ConferenceList_td5%>" runat="server"></asp:Literal><asp:LinkButton ID="btnMOJSortName" runat="server" CommandArgument="2" OnCommand="SortGrid" OnClientClick="javascript:DataLoading('1');"><%if (Request.ServerVariables["HTTP_USER_AGENT"].ToString().ToUpper().Contains("TRIDENT"))%> <span style="font-family:Wingdings; font-weight:bold;">&#233;</span><%else %><span style="font-family:Wingdings; font-weight:bolder; font-size:x-large">&#8593;</span></asp:LinkButton> <%--Edited for FF--%>
                                        </td>
                                        <%}
                                                  else
                                                  {%>
                                        <td width="10%" runat="server" id="tdName" align="left" class="ConferenceTableHeader"> <%--Window Dressing--%><%--ALLDEV-838--%>
                                           <asp:Literal Text="<%$ Resources:WebResources, ConferenceList_tdName%>" runat="server"></asp:Literal><asp:LinkButton ID="btnSortName" runat="server" CommandArgument="2" OnCommand="SortGrid" OnClientClick="ChangeSortingOrder(this.childNodes[0].id);"><%if (Request.ServerVariables["HTTP_USER_AGENT"].ToString().ToUpper().Contains("TRIDENT")) {%><asp:Label id="spnAscendingConfNameIE" runat="server" style="font-family:Wingdings; font-weight:bolder;">&#233;</asp:Label><asp:Label id="spnDescndingConfNameIE" runat="server" style="font-family:Wingdings; font-weight:bolder;">&#234;</asp:Label><% } else { %><asp:Label id="spnAscendingConfName" runat="server" style="font-family:Wingdings; font-weight:bolder; font-size:x-large;">&#8593;</asp:Label><asp:Label id="spnDescendingConfName" runat="server" style="font-family:Wingdings; font-weight:bolder; font-size:x-large;">&#8595;</asp:Label><% } %></asp:LinkButton> <%--Edited for FF--%><%--FB 2822--%><%--ZD 100381--%>
                                        </td>
                                         <%}%>
                                         <%--Added for FB 1428 End--%>
                                         <td width="10%" runat="server" id="tdOrgName" align="left" class="ConferenceTableHeader"> <%--Window Dressing--%><%--ALLDEV-838--%>
                                           <asp:Literal Text="<%$ Resources:WebResources, ConferenceList_tdOrgName%>" runat="server"></asp:Literal><asp:LinkButton ID="btnSortorgName" runat="server" CommandArgument="5" OnCommand="SortGrid" OnClientClick="ChangeSortingOrder(this.childNodes[0].id);"><%if (Request.ServerVariables["HTTP_USER_AGENT"].ToString().ToUpper().Contains("TRIDENT")) { %><asp:Label id="spnAscendingSiloNameIE" runat="server" style="font-family:Wingdings; font-weight:bolder;">&#233;</asp:Label><asp:Label id="spnDescndingSiloNameIE" runat="server" style="font-family:Wingdings; font-weight:bolder;">&#234;</asp:Label><% } else { %><asp:Label id="spnAscendingSiloName" runat="server" style="font-family:Wingdings; font-weight:bolder; font-size:x-large;">&#8593;</asp:Label><asp:Label id="spnDescendingSiloName" runat="server" style="font-family:Wingdings; font-weight:bolder; font-size:x-large;">&#8595;</asp:Label><% } %></asp:LinkButton> <%--Edited for FF--%><%--FB 2822--%><%--ZD 100381--%>
                                        </td><%--Edited for FF--%><%--ZD 100381--%>
                                          <%--<td width="10%" runat="server" id="td8" align="left" style="display:none"> 
                                           <asp:Literal Text="<%$ Resources:WebResources, ConferenceList_td8%>" runat="server"></asp:Literal><asp:LinkButton ID="LinkButton1" runat="server" CommandArgument="2" OnCommand="SortGrid" OnClientClick="javascript:DataLoading('1');"><%if (Request.ServerVariables["HTTP_USER_AGENT"].ToString().ToUpper().Contains("TRIDENT"))%> <span style="font-family:Wingdings; font-weight:bold;">&#233;</span><%else %><span style="font-family:Wingdings; font-weight:bolder; font-size:x-large">&#8593;</span></asp:LinkButton> 
                                        </td>--%>
                                                <%--Added for FB 1428 Start--%>
                                                <%if (Application["Client"] == "MOJ")
                                                  { %>
                                                  <td width="10%" runat="server" id="td6" style="text-decoration:underline" align="left" class="ConferenceTableHeader"> <%--Window Dressing--%><%--ALLDEV-838--%>
                                           <asp:Literal Text="<%$ Resources:WebResources, ConferenceList_td6%>" runat="server"></asp:Literal><asp:LinkButton ID="btnMOJSortDateTime" runat="server" CommandArgument="3" OnCommand="SortGrid" OnClientClick="javascript:DataLoading('1');"><%if (Request.ServerVariables["HTTP_USER_AGENT"].ToString().ToUpper().Contains("TRIDENT"))%> <span style="font-family:Wingdings; font-weight:bold;">&#233;</span><%else %><span style="font-family:Wingdings; font-weight:bolder; font-size:x-large">&#8593;</span></asp:LinkButton> <%--Edited for FF--%><%--ZD 100381--%>
                                        </td>
                                        <%}
                                                  else
                                                  {%>
                                        <td width="10%" runat="server" id="tdDateTime" style="text-decoration:underline" align="left" class="ConferenceTableHeader"> <%--Window Dressing--%><%--ALLDEV-838--%>
                                           <asp:Label ID="lblDtTimeHeader" runat="server" Text="<%$ Resources:WebResources, ConferenceList_lblDtTimeHeader%>"></asp:Label> <%--FB 1607--%>
                                           <asp:LinkButton ID="btnSortDateTime" runat="server" CommandArgument="3" OnCommand="SortGrid" OnClientClick="ChangeSortingOrder(this.childNodes[0].id);"><%if (Request.ServerVariables["HTTP_USER_AGENT"].ToString().ToUpper().Contains("TRIDENT")) { %> <asp:Label id="spnAscendingConfDateIE" runat="server" style="font-family:Wingdings; font-weight:bolder;">&#233;</asp:Label><asp:Label id="spnDescndingConfDateIE" runat="server" style="font-family:Wingdings; font-weight:bolder;">&#234;</asp:Label> <% } else { %><asp:Label id="spnAscendingConfDate" runat="server" style="font-family:Wingdings; font-weight:bolder; font-size:x-large;">&#8593;</asp:Label><asp:Label id="spnDescendingConfDate" runat="server" style="font-family:Wingdings; font-weight:bolder; font-size:x-large;">&#8595;</asp:Label><% } %></asp:LinkButton> <%--Edited for FF--%><%--FB 2822--%><%--ZD 100381--%>
                                        </td>
                                        <%}%>
                                                <%--Added for FB 1428 End--%>
                                        <td width="8%" class="ConferenceTableHeader" align="left"><asp:Literal Text="<%$ Resources:WebResources, ConferenceList_Type%>" runat="server"></asp:Literal></td><%--ALLDEV-838--%>
                                        
                                        <%--FB 2501 - Start--%>
                                        <td width="5%" runat="server" id="td9" align="left" class="ConferenceTableHeader"><%--ALLDEV-838--%>
                                           <asp:Literal Text="<%$ Resources:WebResources, ConferenceList_td9%>" runat="server"></asp:Literal><asp:LinkButton ID="btnSortConfMode" runat="server" CommandArgument="4" OnCommand="SortGrid" OnClientClick="ChangeSortingOrder(this.childNodes[0].id);"><%if (Request.ServerVariables["HTTP_USER_AGENT"].ToString().ToUpper().Contains("TRIDENT")) { %> <asp:Label id="spnAscendingConfModeIE" runat="server" style="font-family:Wingdings; font-weight:bolder;">&#233;</asp:Label><asp:Label id="spnDescndingConfModeIE" runat="server" style="font-family:Wingdings; font-weight:bolder;">&#234;</asp:Label><% } else { %><asp:Label id="spnAscendingConfMode" runat="server" style="font-family:Wingdings; font-weight:bolder; font-size:x-large;">&#8593;</asp:Label><asp:Label id="spnDescendingConfMode" runat="server" style="font-family:Wingdings; font-weight:bolder; font-size:x-large;">&#8595;</asp:Label><% } %></asp:LinkButton><%--FB 2822--%><%--ZD 100381--%>
                                        </td>
                                        <%--FB 2501 - End--%>
                                        
                                        <td width="5%" class="ConferenceTableHeader" align="left"><asp:Literal Text="<%$ Resources:WebResources, ConferenceList_Duration%>" runat="server"></asp:Literal></td><%--FB 2274--%><%--ALLDEV-838--%>
                                        
                                        <%--FB 2557 Start --%>
                                        <td id="Td10" width="5%" class="ConferenceTableHeader" align="left" runat="server" visible='<%# (Request.QueryString["t"] != "6") && (Request.QueryString["hf"] != "1")%>' ><asp:Literal Text="<%$ Resources:WebResources, ConferenceList_Td10%>" runat="server"></asp:Literal></td><%--ALLDEV-838--%>
                                        <td id="Td15" width="5%" class="ConferenceTableHeader" align="left" runat="server" visible='<%# (Request.QueryString["t"] != "6") && (Request.QueryString["hf"] != "1") %>' ><asp:Literal Text="<%$ Resources:WebResources, ConferenceList_Td11%>" runat="server"></asp:Literal></td><%--ALLDEV-838--%>
                                        <%--FB 2557 End --%><%--ZD 101092--%>
                                        <td id="Td11" width="10%" class="ConferenceTableHeader" align="left" runat="server" visible='<%# (Request.QueryString["t"] == "7") %>'><asp:Literal Text="<%$ Resources:WebResources, ConferenceSupport%>" runat="server"></asp:Literal></td><%--ALLDEV-838--%>
                                        <td id="td7head" width="27%" class="ConferenceTableHeader" align="center" runat="server" visible='<%# (Request.QueryString["t"] != "6") %>'><asp:Literal ID="Literal1" Text="<%$ Resources:WebResources, ConferenceList_td7head%>" runat="server"></asp:Literal></td><%-- FB 2779 --%><%--ALLDEV-838--%>
                                        <td width="47%" class="ConferenceTableHeader" align="center" runat="server" visible='<%# (Request.QueryString["t"] == "6") %>'> <%--ZD 100821--%><%--ALLDEV-838--%>
                                            <table id="tblappr2" width="100%" cellpadding="0" cellspacing="0"> <%-- FB 2987 --%>
                                                <tr>
                                                    <td width="20%">&nbsp;</td>
                                                    <td width="60%"><%--ZD 100642_11jan2014--%>
                                                        <table width="100%" cellpadding="0" cellspacing="0">
                                                            <tr>
                                                                <td align="center">
                                                                    <asp:RadioButton ID="rdUndecideAll" GroupName="Decision" onclick="javascript:UndecideAll(this);" Checked="true" runat="server" />
                                                                </td>
                                                                <td align="center">
                                                                    <asp:RadioButton ID="rdApproveAll" GroupName="Decision" runat="server" onclick="javascript:ApproveAll(this);"  />
                                                                </td>
                                                                 <td align="center"> <%--ZD 100642 Starts--%>
                                                                    <asp:RadioButton ID="rdAlternateAll" GroupName="Decision" runat="server"  Enabled="false" /> 
                                                                </td> <%--ZD 100642 Ends--%>
                                                                <td align="center">
                                                                    <asp:RadioButton ID="rdDenyAll" GroupName="Decision" runat="server" onclick="javascript:DenyAll(this);" />
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                    <td width="20%">&nbsp;</td>
                                                </tr>
                                            </table>
                                        </td>
                                        <td id="tdSts" class="ConferenceTableHeader" align="left" width="5%"  runat="server" visible='<%# (Request.QueryString["t"] == "5") %>'> <asp:Literal ID="Literal10" Text="<%$ Resources:WebResources, ConfList_ApprovalInfo%>" runat="server"></asp:Literal></td><%-- ZD 100425 ZD 101344 --%>  <%--ALLDEV-838--%>
                                        <td width="1%" class="ConferenceTableHeader"></td><%--ALLDEV-838--%>
                                    </tr>
                                </table>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <table width="100%" border="0"><%--ALLDEV-838--%>
                                    <tr style='background-color: <%#((Request.QueryString["hf"] == "1") && DataBinder.Eval(Container, "DataItem.isVMR").ToString().Equals("1") && DataBinder.Eval(Container, "DataItem.OpenForRegistration").ToString().Equals("1") && !(DataBinder.Eval(Container, "DataItem.RemPublicVMRCount").ToString().Equals("0")))?"#F0E68C":"Transparent"%>'> <%--FB 2550--%>
                                        <td width="5%" align="left" valign="top" ><%--FB 2274--%>
                                            <asp:Label ID="lblUniqueID" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.ConferenceUniqueID") %>'></asp:Label>
                                        </td>
                                        <td width="10%" align="left" valign="top">
                                            <asp:Image ID="imgRecur" AlternateText="Recurring Conference" Height="20" Width="20" ImageUrl="image/recurring.gif" runat="server" ToolTip="Recurring Conference" Visible='<%# DataBinder.Eval(Container, "DataItem.IsRecur").ToString().Trim().Equals("1") %>' /> <%--ZD 100419--%>
                                            <asp:Label ID="lblConfName" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.ConferenceName") %>'></asp:Label>
                                        </td>
                                        <td width="10%" align="left" valign="top" >
                                            <asp:Label ID="lblOrgName" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.organizationName") %>'></asp:Label>
                                        </td>
                                        <%--FB 2448--%>
                                        <%--<td width="10%" align="left" valign="top" style="display:none">
                                            <asp:Label ID="lblisVMR" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.isVMR") %>'></asp:Label>
                                        </td>--%>
                                        <%--FB 2550 Starts--%>
                                        <%--<td width="10%" align="left" valign="top" style="display:none">
                                            <asp:Label ID="lblPublicVMRCount" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.RemPublicVMRCount") %>'></asp:Label>
                                        </td>--%>
                                        <%--<td width="10%" align="left" valign="top" style="display:none">
                                            <asp:Label ID="Label2" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.isVMRJoin") %>'></asp:Label>
                                        </td>--%>
                                        <%--FB 2550 Ends--%>
                                        <td width="10%" align="left" valign="top">
                                            <asp:Label ID="lblDateTime" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.ConferenceDateTime")%>' Visible='<%# !DataBinder.Eval(Container, "DataItem.IsRecur").ToString().Trim().Equals("1") %>'></asp:Label>
                                            <asp:LinkButton ID="btnGetInstances" Text="<%$ Resources:WebResources, ConferenceList_btnGetInstances%>" runat="server" CommandName="Edit" Visible='<%# DataBinder.Eval(Container, "DataItem.IsRecur").ToString().Trim().Equals("1") %>'    OnClientClick='<%# "return fnshow(" + Eval("isWebExMeeting") +","+ Eval("isRecur")+","+"0"+");" %>'></asp:LinkButton> <%--ZD 100221--%>
                                        </td>
                                        <td width="8%" align="left" valign="top">
                                            <asp:Label ID="lblConferenceType" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.ConferenceTypeDescription")%>'></asp:Label>
                                        </td>
                                        <td width="5%" align="left" valign="top"><%--FB 2501--%>
                                            <asp:Label ID="lblConferenceMode" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.StartMode")  %>'></asp:Label>
                                        </td>
                                        <td width="5%" align="left" valign="top"><%--FB 2274--%>
                                            <asp:Label ID="lblDuration" runat="server"></asp:Label>
                                        </td>
                                        
                                        <%--FB 2557 Start --%>
                                        <td id="Td12" width="5%" align="left" valign="top" runat="server" visible='<%# (Request.QueryString["t"] != "6") && (Request.QueryString["hf"] != "1")%>' >
                                            <asp:Label ID="lblOwner" runat="server"></asp:Label>
                                        </td>
                                        <td id="Td13" width="5%" align="left" valign="top" runat="server" visible='<%# (Request.QueryString["t"] != "6")&& (Request.QueryString["hf"] != "1") %>' >
                                            <asp:Label ID="lblAttendee" runat="server"></asp:Label>
                                        </td>
                                        <%--FB 2557 End --%><%--ZD 101092--%>
                                        <td id="Td14" width="10%" align="left" valign="top" runat="server" visible='<%# (Request.QueryString["t"] == "7") %>'>
                                            <asp:Label ID="lblConferenceSupport" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.ConferenceSupport")  %>'></asp:Label>
                                        </td>
                                        <td id="Td7" width="27%" runat="server" valign="top" visible='<%# (Request.QueryString["t"] != "6") %>'>
                                                <table width="100%">
                                                    <tr>
                                                        <td align="left" valign="top" ><asp:LinkButton ID="btnViewDetails" runat="server" Text="<%$ Resources:WebResources, ConferenceList_btnViewDetails%>" Visible='<%# (Request.QueryString["hf"] != "1") %>'></asp:LinkButton></td><%--FB 2501--%>
                                                        <td align="left" visible='<%# (Request.QueryString["hf"] != "1") %>'><asp:LinkButton Text="<%$ Resources:WebResources, ConferenceList_btnClone%>" runat="server" ID="btnClone" CommandName="Update" Visible='<%# ( (Request.QueryString["hf"] != "1") && (DataBinder.Eval(Container, "DataItem.IsHost").ToString().Equals("1") || DataBinder.Eval(Container, "DataItem.isLocAdmin").ToString().Equals("1") || (Int32.Parse(Session["admin"].ToString()) >= 2))) %>' OnClientClick="javascript:DataLoading('1');"></asp:LinkButton></td>
														<%--ZD 100221 Starts--%>
                                                        <td align="left" visible='<%# (Request.QueryString["hf"] != "1") %>'><asp:LinkButton Text="<%$ Resources:WebResources, ConferenceList_btnDelete%>" runat="server" ID="btnDelete" CommandName="Cancel" Visible='<%# ( (Request.QueryString["hf"] != "1")) && (DataBinder.Eval(Container, "DataItem.IsHost").ToString().Equals("1") || DataBinder.Eval(Container, "DataItem.isLocAdmin").ToString().Equals("1") || (Int32.Parse(Session["admin"].ToString()) >= 2)) %>' ></asp:LinkButton></td> <%--//ZD 100221_17Feb commented OnClientClick='<%# "return fnshow(" + Eval("isWebExMeeting") +","+ Eval("isRecur")+","+"1"+");" %>'--%>
                                                        <td align="left" visible='<%# (Request.QueryString["hf"] != "1") %>'>
                                                        <asp:Button style="display:none;" runat="server" ID="Temp" OnClick="EditConf" /> <%--ZD 101597--%>
                                                        <asp:LinkButton Text="<%$ Resources:WebResources, ConferenceList_btnEdit%>" runat="server" ID="btnEdit" CommandName="Edit" Visible='<%# ((Request.QueryString["hf"] != "1")) && (DataBinder.Eval(Container, "DataItem.IsHost").ToString().Equals("1") || DataBinder.Eval(Container, "DataItem.isLocAdmin").ToString().Equals("1") || (Int32.Parse(Session["admin"].ToString()) >= 2)) %>' ></asp:LinkButton></td>
                                                        <td align="left" valign="top" visible='<%# (Request.QueryString["hf"] != "1") %>'><asp:LinkButton Text="<%$ Resources:WebResources, ConferenceList_btnManage%>" runat="server" ID="btnManage" CommandArgument="1" CommandName="Delete" Visible='<%# (Request.QueryString["hf"] != "1") && (DataBinder.Eval(Container, "DataItem.IsHost").ToString().Equals("1") || DataBinder.Eval(Container, "DataItem.isLocAdmin").ToString().Equals("1") || Int32.Parse(Session["admin"].ToString()) >= 2) %>' OnClientClick='<%# "return fnshow(" + Eval("isWebExMeeting") +","+ Eval("isRecur")+","+"1"+");" %>'></asp:LinkButton></td>
														<%--ZD 100221 Ends--%>
														<td align="left" visible='<%# (Request.QueryString["hf"] != "1") %>' ><asp:LinkButton ID="btnExtendtime" runat="server" Text="<%$ Resources:WebResources, ExtendEndTime%>" Visible='<%# (Request.QueryString["hf"] != "1") && (Request.QueryString["t"] == "2") && (DataBinder.Eval(Container, "DataItem.IsHost").ToString().Equals("1") || DataBinder.Eval(Container, "DataItem.isLocAdmin").ToString().Equals("1") || Int32.Parse(Session["admin"].ToString()) >= 2) %>' ></asp:LinkButton></td> <%--ZD 101315-Extend conf--%>                                                        					
                                                        <td align="left" ><asp:LinkButton ID="btnMCUDetails" runat="server" Text="<%$ Resources:WebResources, ConferenceList_btnMCUDetails%>" Visible="false"></asp:LinkButton></td> <%--FB 2448--%>                                                        
                                                        <td align="left" visible='<%# ( (Request.QueryString["hf"] == "1") && DataBinder.Eval(Container, "DataItem.OpenForRegistration").ToString().Equals("1") && (DataBinder.Eval(Container, "DataItem.isVMRJoin").ToString().Equals("1"))) %>'><asp:LinkButton Text="<%$ Resources:WebResources, ConferenceList_btnJoin%>" runat="server" ID="btnJoin" CommandArgument="2" CommandName="Delete" Visible='<%# ( (Request.QueryString["hf"] == "1") && DataBinder.Eval(Container, "DataItem.OpenForRegistration").ToString().Equals("1") && (DataBinder.Eval(Container, "DataItem.isVMRJoin").ToString().Equals("1"))) %>' OnClientClick="javascript:DataLoading('1');"></asp:LinkButton></td> <%--FB 2550--%>
                                                    </tr>
                                                </table>
                                        </td>
                                        <td width="47%" valign="top" runat="server" visible='<%# (Request.QueryString["t"] == "6") %>'> <%--ZD 100821--%>
                                            <asp:DataGrid runat="server" ShowHeader="false" BorderWidth="0"   ID="dgResponse" Width="100%"
                                             AutoGenerateColumns="false" CellPadding="0" CellSpacing="0" style="border-collapse:separate"> <%--Edited for FF--%>
                                            <Columns>
                                                <asp:BoundColumn DataField="ID" Visible="false"></asp:BoundColumn>
                                                <asp:TemplateColumn HeaderText="<%$ Resources:WebResources, Responsefor%>" HeaderStyle-CssClass="ConferenceTableHeader" ItemStyle-Width="20%" ItemStyle-HorizontalAlign="Left"><%--ALLDEV-838--%>
                                                    <ItemStyle HorizontalAlign="left" />
                                                    <ItemTemplate>
                                                        <asp:Label runat="server" ID="lblEntityTypeID" Text='<%# DataBinder.Eval(Container, "DataItem.EntityTypeID") %>' Visible="false"></asp:Label><%--ALLDEV-838--%>
                                                        <asp:Label runat="server" CssClass="blackblodtext" ID="lblEntityTypeName" Text='<%# DataBinder.Eval(Container, "DataItem.EntityTypeName")  + ": " %>' Visible="true" Font-Bold="true"></asp:Label>
                                                        <asp:Label runat="server" ID="lblEntityID" Text='<%# DataBinder.Eval(Container, "DataItem.ID") %>' Visible="false"></asp:Label><%--ALLDEV-838--%>
                                                        <asp:Label runat="server" ID="lblEntityName" Text='<%# DataBinder.Eval(Container, "DataItem.Name") %>' Visible="true"></asp:Label><%--ALLDEV-838--%>
                                                        <asp:Label runat="server" ID="lblActualEntityID" Text='<%# DataBinder.Eval(Container, "DataItem.ID") %>' Visible="false"></asp:Label><%--ZD 100642--%><%--ALLDEV-838--%>
                                                    </ItemTemplate>
                                                </asp:TemplateColumn>
                                                <asp:TemplateColumn HeaderStyle-CssClass="ConferenceTableHeader" ItemStyle-Width="60%"><%--ZD 100642_11jan2014--%><%--ALLDEV-838--%>
                                                    <ItemTemplate>
                                                        <table width="100%" border="0">
                                                            <tr>
                                                                <td align="center"><asp:RadioButton ID="rdUndecided" GroupName="Decision" Checked="true" runat="server" /></td>
                                                                <td align="center"><asp:RadioButton ID="rdApprove" GroupName="Decision" runat="server" /></td>
                                                                <%--ZD 100642 Starts--%> <%--ZD 100718--%>
                                                                <td align="center"><asp:RadioButton ID="rdAlternate" GroupName="Decision" runat="server" CssClass='<%# DataBinder.Eval(Container, "DataItem.EntityParams") %>' onclick='javascript:fnInvokePopup(this.parentNode.className, this.id)' Enabled='<%# DataBinder.Eval(Container, "DataItem.EntityTypeID").ToString().Equals("1") || DataBinder.Eval(Container, "DataItem.EntityTypeID").ToString().Equals("2") %>' />
                                                                <asp:LinkButton ID="updtLink" runat="server" OnClick="ShowAlternate" ></asp:LinkButton>
                                                                <asp:Label ID="lblAlterRoomID" runat="server" style="display: none;"></asp:Label> </td>
                                                                <input name="addRooms" type="button" id="addRooms" onclick="javascript:AddRooms();" style="display: none;" />
                                                                <%--ZD 100642 Ends--%>
                                                                <td align="center"><asp:RadioButton ID="rdDeny" GroupName="Decision" runat="server" /></td>
                                                            </tr>
                                                        </table>
                                                    </ItemTemplate>
                                                </asp:TemplateColumn>
                                                <asp:TemplateColumn ItemStyle-CssClass="tableBody" HeaderStyle-CssClass="ConferenceTableHeader" HeaderText="<%$ Resources:WebResources, approvalstatus_Message%>" ItemStyle-Width="20%" ItemStyle-HorizontalAlign="Left"><%--ALLDEV-838--%>
                                                    <ItemTemplate>
                                                        <asp:TextBox ID="txtMessage" runat="server" ValidationGroup="submit" CssClass="altText" TextMode="MultiLine" Rows="2" Width="100%"></asp:TextBox>
                                                        <asp:requiredfieldvalidator id="reqtxtMessage" runat="server" controltovalidate="txtMessage" display="dynamic" cssclass="lblError" setfocusonerror="true" 
                                                        text="<%$ Resources:WebResources, Required%>" ValidationGroup="submit"></asp:requiredfieldvalidator>
                                                    </ItemTemplate>
                                                </asp:TemplateColumn>
                                            </Columns>
                                            </asp:DataGrid>
                                        </td>
                                        <td align="left" runat="server"  width="5%" visible='<%# (Request.QueryString["t"] == "5" && Request.QueryString["hf"] != "1") %>'><%--ALLDEV-838--%>
                                            <asp:LinkButton Text=" <%$ Resources:WebResources, ConferenceList_btnApprovalStatus%>" runat="server" ID="btnApprovalStatus" Visible='<%# (Request.QueryString["t"] == "5")%>'></asp:LinkButton>
                                        </td>
                                        <td width="1%" visible='<%# (Request.QueryString["t"] != "6") %>'></td><%--ALLDEV-838--%>
                                    </tr>
                                    <tr>
                                        <td colspan="11"><%--FB 2274 FB 2501 ZD 101092--%>
                                          <asp:Table runat="server" ID="tblInstances" Visible="false" BorderColor="black" Width="100%" CellPadding="0" CellSpacing="0" BorderWidth="1" BorderStyle="Inset">
                                            <asp:TableRow>
                                                <asp:TableCell>                                                
                                                    <asp:DataGrid ID="dgInstanceList" runat="server" AutoGenerateColumns="False" Font-Names="Verdana" Font-Size="Small"
                                                     Width="100%" BorderColor="blue" BorderStyle="Solid" BorderWidth="0px" OnItemCreated="BindRowsDeleteMessage" Visible="false"
                                                     OnEditCommand="EditConference" ShowHeader="false" OnCancelCommand="DeleteConference" OnDeleteCommand="ManageConference" OnUpdateCommand="CloneConference" style="border-collapse:separate" GridLines="None" > <%--Edited for FF and FB 2050 --%>
                                                        <ItemStyle CssClass="ConferenceTableOddRows" /><%--ALLDEV-838--%>
                                                        <AlternatingItemStyle CssClass="ConferenceTableEvenRows" /><%--ALLDEV-838--%>
                                                        <HeaderStyle CssClass="ConferenceTableHeader" Height ="30"/><%--ALLDEV-838--%>
                                                        <Columns>
                                                            <asp:BoundColumn DataField="ConferenceID" Visible="false"></asp:BoundColumn>
                                                            <asp:BoundColumn DataField="IsHost" Visible="false"></asp:BoundColumn>
                                                            <asp:BoundColumn DataField="IsParticipant" Visible="false"></asp:BoundColumn>
                                                            <asp:BoundColumn DataField="ConferenceDuration" Visible="false"></asp:BoundColumn>
                                                            <asp:BoundColumn DataField="ConferenceStatus" Visible="false"></asp:BoundColumn>
                                                            <asp:BoundColumn DataField="IsRecur" Visible="false"></asp:BoundColumn>
                                                            <asp:BoundColumn DataField="OpenForRegistration" Visible="false"></asp:BoundColumn>
                                                            <asp:BoundColumn DataField="Conference_Id" Visible="false"></asp:BoundColumn>
                                                            <asp:BoundColumn DataField="ConferenceDateTime" Visible="false"></asp:BoundColumn>
                                                            <asp:BoundColumn DataField="organizationName" Visible="false"></asp:BoundColumn>
                                                            <asp:TemplateColumn ItemStyle-Width="80%" HeaderStyle-Width="80%" FooterStyle-HorizontalAlign="Right">
                                                                <ItemTemplate>
                                                                    <table width="100%" border="0">
                                                                        <tr style='background-color: <%#((Request.QueryString["hf"] == "1") && DataBinder.Eval(Container, "DataItem.isVMR").ToString().Equals("1") && DataBinder.Eval(Container, "DataItem.OpenForRegistration").ToString().Equals("1") && !(DataBinder.Eval(Container, "DataItem.RemPublicVMRCount").ToString().Equals("0")))?"#F0E68C":"Transparent"%>'> <%--FB 2550--%>
                                                                            <td width="5%" align="left" valign="top" ><%--FB 2274--%>
                                                                                <asp:Label ID="lblUniqueID" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.ConferenceUniqueID") %>'></asp:Label>
                                                                            </td>
                                                                            <td width="10%" align="left" valign="top">
                                                                                <asp:Image ID="imgRecur" AlternateText="Recurring Conference"  Height="20" Width="20" ImageUrl="image/recurring.gif" runat="server" ToolTip="Recurring Conference" Visible='<%# DataBinder.Eval(Container, "DataItem.IsRecur").ToString().Trim().Equals("1") %>' /> <%--ZD 100419--%>
                                                                                <asp:Label ID="lblConfName" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.ConferenceName") %>'></asp:Label>
                                                                            </td>
                                                                            <td width="10%" align="left" valign="top" >
                                                                                <asp:Label ID="lblOrgName" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.organizationName") %>'></asp:Label>
                                                                            </td><%--FB 2448--%>
                                                                            <%--<td width="10%" align="left" valign="top" style="display:none">
                                                                                <asp:Label ID="lblisVMR" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.isVMR") %>'></asp:Label>
                                                                            </td>--%>
                                                                            <%--FB 2550 Starts--%>
                                                                           <%-- <td width="10%" align="left" valign="top" style="display:none">
                                                                                <asp:Label ID="lblPublicVMRCount" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.RemPublicVMRCount") %>'></asp:Label>
                                                                            </td>
                                                                            <td width="10%" align="left" valign="top" style="display:none">
                                                                                <asp:Label ID="Label3" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.isVMRJoin") %>'></asp:Label>
                                                                            </td>--%>
                                                                            <%--FB 2550 Ends--%>
                                                                            <td width="10%" align="left" valign="top">
                                                                                <asp:Label ID="lblDateTime" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.ConferenceDateTime")%>' Visible='<%# !DataBinder.Eval(Container, "DataItem.IsRecur").ToString().Trim().Equals("1") %>'></asp:Label>
                                                                                <asp:LinkButton ID="btnGetInstances" Text='<%# DataBinder.Eval(Container, "DataItem.isWebExMeeting").ToString().Trim()%>' runat="server" CommandName="Edit" Visible='<%# DataBinder.Eval(Container, "DataItem.IsRecur").ToString().Trim().Equals("1") %>' CssClass='<%# DataBinder.Eval(Container, "DataItem.isWebExMeeting").ToString().Trim()%>' OnClientClick="if(this.className=='1'){ alert(ConfListwebexen);return false;}"></asp:LinkButton> <%--ZD 100221--%>
                                                                            </td>
                                                                            <td width="8%" align="left" valign="top">
                                                                                <asp:Label ID="lblConferenceType" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.ConferenceTypeDescription")%>'></asp:Label>
                                                                            </td>
                                                                            <td width="5%" align="left" valign="top"><%--FB 2501--%>
                                                                                <asp:Label ID="lblConferenceMode" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.StartMode") %>'></asp:Label>
                                                                            </td>
                                                                            <td width="5%" align="left" valign="top"><%--FB 2274--%>
                                                                                <asp:Label ID="lblDuration" runat="server"></asp:Label>
                                                                            </td>
                                                                            <td id="Td2" width="5%" align="left" valign="top" runat="server" visible='<%# (Request.QueryString["t"] != "6" && (Request.QueryString["hf"] != "1")) %>' >
                                                                                <asp:Label ID="lblOwner"  runat="server"></asp:Label>
                                                                            </td>
                                                                            <td id="Td3" width="5%" align="left" valign="top" runat="server" visible='<%# (Request.QueryString["t"] != "6" && (Request.QueryString["hf"] != "1")) %>' >
                                                                                <asp:Label ID="lblAttendee" runat="server"></asp:Label>
                                                                            </td> <%--ZD 101092--%>
                                                                            <td id="Td14" width="10%" align="left" valign="top" runat="server" visible='<%# (Request.QueryString["t"] == "7") %>'>
                                                                                <asp:Label ID="lblConferenceSupport" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.ConferenceSupport")  %>'></asp:Label>
                                                                            </td>
                                                                            <td id="Td1" width="27%" runat="server" valign="top" visible='<%# (Request.QueryString["t"] != "6") %>'>
                                                                                    <table width="100%" cellspacing="3">
                                                                                        <tr>
                                                                                            <td align="left" ><asp:LinkButton ID="btnViewDetails" runat="server" Text="<%$ Resources:WebResources, View%>" Visible='<%# (Request.QueryString["hf"] != "1") %>'></asp:LinkButton></td>
                                                                                            <td align="left" visible='<%# (Request.QueryString["hf"] != "1") %>'><asp:LinkButton Text="<%$ Resources:WebResources, ConferenceList_btnClone%>" runat="server" ID="btnClone" CommandName="Update" Visible='<%# ( (Request.QueryString["hf"] != "1") && (DataBinder.Eval(Container, "DataItem.IsHost").ToString().Equals("1") || DataBinder.Eval(Container, "DataItem.isLocAdmin").ToString().Equals("1") || (Int32.Parse(Session["admin"].ToString()) >= 2))) %>' OnClientClick="javascript:DataLoading('1');"></asp:LinkButton></td>
                                                                                            <td align="left" visible='<%# (Request.QueryString["hf"] != "1") %>'><asp:LinkButton Text="<%$ Resources:WebResources, ConferenceList_btnDelete%>" runat="server" ID="btnDelete" CommandName="Cancel" Visible='<%# ( (Request.QueryString["hf"] != "1") && !DataBinder.Eval(Container, "DataItem.ConferenceStatus").ToString().Equals("7")) && (DataBinder.Eval(Container, "DataItem.IsHost").ToString().Equals("1") || DataBinder.Eval(Container, "DataItem.isLocAdmin").ToString().Equals("1") || (Int32.Parse(Session["admin"].ToString()) >= 2)) %>' OnClientClick="javascript:DataLoading('1');"></asp:LinkButton></td>
                                                                                            <td align="left" visible='<%# (Request.QueryString["hf"] != "1") %>'><asp:LinkButton Text="<%$ Resources:WebResources, ConferenceList_btnEdit%>" runat="server" ID="btnEdit" CommandName="Edit" Visible='<%# ((Request.QueryString["hf"] != "1") && !DataBinder.Eval(Container, "DataItem.ConferenceStatus").ToString().Equals("7")) && (DataBinder.Eval(Container, "DataItem.IsHost").ToString().Equals("1") || DataBinder.Eval(Container, "DataItem.isLocAdmin").ToString().Equals("1") || (Int32.Parse(Session["admin"].ToString()) >= 2)) %>' ></asp:LinkButton></td>
                                                                                            <td align="left" visible='<%# (Request.QueryString["hf"] != "1") %>'><asp:LinkButton Text="<%$ Resources:WebResources, ConferenceList_btnManage%>" runat="server" ID="btnManage" CommandArgument="1" CommandName="Delete" Visible='<%# (Request.QueryString["hf"] != "1") && (DataBinder.Eval(Container, "DataItem.IsHost").ToString().Equals("1") || DataBinder.Eval(Container, "DataItem.isLocAdmin").ToString().Equals("1") || Int32.Parse(Session["admin"].ToString()) >= 2) %>' OnClientClick="javascript:DataLoading('1');"></asp:LinkButton></td>
                                                                                            <td align="left" visible='<%# ( (Request.QueryString["hf"] == "1") && DataBinder.Eval(Container, "DataItem.OpenForRegistration").ToString().Equals("1")&& (DataBinder.Eval(Container, "DataItem.isVMRJoin").ToString().Equals("1"))) %>'><asp:LinkButton Text="<%$ Resources:WebResources, ConferenceList_btnJoin%>" runat="server" ID="btnJoin" CommandArgument="2" CommandName="Delete" Visible='<%# ( (Request.QueryString["hf"] == "1") && DataBinder.Eval(Container, "DataItem.OpenForRegistration").ToString().Equals("1") && (DataBinder.Eval(Container, "DataItem.isVMRJoin").ToString().Equals("1"))) %>' OnClientClick="javascript:DataLoading('1');"></asp:LinkButton></td>
                                    <%--                                                        <td align="left" ><asp:LinkButton ID="btnViewDetails" runat=server Text="View" Visible='<%# (Request.QueryString["hf"] != "1") %>'></asp:LinkButton></td>
                                                                                            <td align="left" visible='<%# (Request.QueryString["hf"] != "1") %>'><asp:LinkButton Text="Clone" runat="server" ID="btnClone" CommandName="Update" Visible='<%# ( (Request.QueryString["hf"] != "1") && (DataBinder.Eval(Container, "DataItem.IsHost").ToString().Equals("1") || (Int32.Parse(Session["admin"].ToString()) > 0))) %>' OnClientClick="javascript:DataLoading('1');"></asp:LinkButton></td>
                                                                                            <td align="left" visible='<%# (Request.QueryString["hf"] != "1") %>'><asp:LinkButton Text="Delete" runat="server" ID="btnDelete" CommandName="Cancel" Visible='<%# ( (Request.QueryString["hf"] != "1") && !DataBinder.Eval(Container, "DataItem.ConferenceStatus").ToString().Equals("7")) && (DataBinder.Eval(Container, "DataItem.IsHost").ToString().Equals("1") || (Int32.Parse(Session["admin"].ToString()) >= 1)) %>' OnClientClick="javascript:DataLoading('1');"></asp:LinkButton></td>
                                                                                            <td align="left" visible='<%# (Request.QueryString["hf"] != "1") %>'><asp:LinkButton Text="Edit" runat="server" ID="btnEdit" CommandName="Edit" Visible='<%# ((Request.QueryString["hf"] != "1") && !DataBinder.Eval(Container, "DataItem.ConferenceStatus").ToString().Equals("7")) && (DataBinder.Eval(Container, "DataItem.IsHost").ToString().Equals("1") || (Int32.Parse(Session["admin"].ToString()) >= 1)) %>' OnClientClick="javascript:DataLoading('1');"></asp:LinkButton></td>
                                                                                            <td align="left" visible='<%# (Request.QueryString["hf"] != "1") %>'><asp:LinkButton Text="Manage" runat="server" ID="btnManage" CommandArgument="1" CommandName="Delete" Visible='<%# (Request.QueryString["hf"] != "1") && (DataBinder.Eval(Container, "DataItem.IsHost").ToString().Equals("1") || Int32.Parse(Session["admin"].ToString()) >= 1) %>' OnClientClick="javascript:DataLoading('1');"></asp:LinkButton></td>
                                                                                            <td align="left" visible='<%# ( (Request.QueryString["hf"] == "1") && DataBinder.Eval(Container, "DataItem.OpenForRegistration").ToString().Equals("1")) %>'><asp:LinkButton Text="Join" runat="server" ID="btnJoin" CommandArgument="2" CommandName="Delete" Visible='<%# ( (Request.QueryString["hf"] == "1") && DataBinder.Eval(Container, "DataItem.OpenForRegistration").ToString().Equals("1")) %>' OnClientClick="javascript:DataLoading('1');"></asp:LinkButton></td>
                                                                                            <td align="left" visible='<%# (Request.QueryString["hf"] != "1") %>'><asp:LinkButton Text="Status" runat="server" ID="btnApprovalStatus" Visible='<%# (Request.QueryString["t"] == "5")%>'></asp:LinkButton></td>--%>
                                                                                        </tr>
                                                                                    </table>
                                                                            </td>
                                                                            <td id="Td8" align="left" runat="server"  width="5%" visible='<%# (Request.QueryString["t"] == "5" && Request.QueryString["hf"] != "1") %>'> <asp:LinkButton Text="<%$ Resources:WebResources, ConferenceList_btnApprovalStatus%>" runat="server" ID="btnApprovalStatus" Visible='<%# (Request.QueryString["t"] == "5")%>'></asp:LinkButton></td><%--ALLDEV-838--%>

                                                                            <td id="Td4" width="47%" valign="top" runat="server" visible='<%# (Request.QueryString["t"] == "6") %>'>
                                                                                <asp:DataGrid runat="server" ShowHeader="false" BorderWidth="0" BorderStyle="solid" ID="dgResponse" Width="100%"
                                                                                 AutoGenerateColumns="false" CellPadding="0" CellSpacing="0" style="border-collapse:separate"> <%--Edited for FF--%><%--ALLDEV-838--%>
                                                                                <Columns>
                                                                                    <asp:BoundColumn DataField="ID" Visible="false"></asp:BoundColumn>
                                                                                    <asp:TemplateColumn HeaderText="<%$ Resources:WebResources, Responsefor%>" HeaderStyle-CssClass="ConferenceTableHeader" ItemStyle-Width="20%" ItemStyle-HorizontalAlign="Left"><%--ALLDEV-838--%>
                                                                                        <ItemStyle HorizontalAlign="left" />
                                                                                        <ItemTemplate>
                                                                                            <asp:Label runat="server" ID="lblEntityTypeID" Text='<%# DataBinder.Eval(Container, "DataItem.EntityTypeID") %>' Visible="false"></asp:Label>
                                                                                            <asp:Label runat="server" CssClass="blackblodtext" ID="lblEntityTypeName" Text='<%# DataBinder.Eval(Container, "DataItem.EntityTypeName")  + ": " %>' Visible="true" Font-Bold="true"></asp:Label>
                                                                                            <asp:Label runat="server" ID="lblEntityID" Text='<%# DataBinder.Eval(Container, "DataItem.ID") %>' Visible="false"></asp:Label><%--ALLDEV-838--%>
                                                                                            <asp:Label runat="server" ID="lblEntityName" Text='<%# DataBinder.Eval(Container, "DataItem.Name") %>' Visible="true"></asp:Label><%--ALLDEV-838--%>
                                                                                            <asp:Label runat="server" ID="lblActualEntityID" Text='<%# DataBinder.Eval(Container, "DataItem.ID") %>' Visible="false"></asp:Label><%--ZD 100642--%><%--ALLDEV-838--%>
                                                                                        </ItemTemplate>
                                                                                    </asp:TemplateColumn>
                                                                                    <asp:TemplateColumn HeaderStyle-CssClass="ConferenceTableHeader" ItemStyle-Width="60%"><%--ALLDEV-838--%>
                                                                                        <ItemTemplate>
                                                                                            <table width="100%" border="0">
                                                                                                <tr>
                                                                                                    <td align="center"><asp:RadioButton ID="rdUndecided" GroupName="Decision" Checked="true" runat="server" /></td>
                                                                                                    <td align="center"><asp:RadioButton ID="rdApprove" GroupName="Decision" runat="server" /></td>
                                                                                                    <%--ZD 100642 Starts--%><%--ZD 100718--%>
                                                                                                    <td align="center"><asp:RadioButton ID="rdAlternate" GroupName="Decision" runat="server" CssClass='<%# DataBinder.Eval(Container, "DataItem.EntityParams") %>' onclick='javascript:fnInvokePopup(this.parentNode.className, this.id)' Enabled='<%# DataBinder.Eval(Container, "DataItem.EntityTypeID").ToString().Equals("1") || DataBinder.Eval(Container, "DataItem.EntityTypeID").ToString().Equals("2")%>' />
                                                                                                    <asp:LinkButton ID="updtLink" runat="server" OnClick="ShowAlternate" ></asp:LinkButton>
                                                                                                    <asp:Label ID="lblAlterRoomID" runat="server" style="display: none;"></asp:Label> </td> 
                                                                                                    <input name="addRooms" type="button" id="addRooms" onclick="javascript:AddRooms();" style="display: none;" />
                                                                                                    <%--ZD 100642 Ends--%>
                                                                                                    <td align="center"><asp:RadioButton ID="rdDeny" GroupName="Decision" runat="server" /></td>
                                                                                                </tr>
                                                                                            </table>
                                                                                        </ItemTemplate>
                                                                                    </asp:TemplateColumn>
                                                                                    <asp:TemplateColumn HeaderStyle-CssClass="ConferenceTableHeader" HeaderText="<%$ Resources:WebResources, approvalstatus_Message%>" ItemStyle-Width="20%" ItemStyle-HorizontalAlign="Left"><%--ALLDEV-838--%>
                                                                                        <ItemTemplate>
                                                                                            <asp:TextBox ID="txtMessage" runat="server" CssClass="altText" ValidationGroup="submit" TextMode="MultiLine" Rows="2" Width="100%"></asp:TextBox>
                                                                                            <asp:requiredfieldvalidator id="reqtxtMessage" runat="server" controltovalidate="txtMessage" display="dynamic" cssclass="lblError" setfocusonerror="true" 
                                                        text="<%$ Resources:WebResources, Required%>" validationgroup="submit"></asp:requiredfieldvalidator>
                                                                                        </ItemTemplate>
                                                                                    </asp:TemplateColumn>
                                                                                </Columns>
                                                                                
                                                                                </asp:DataGrid>
                                                                            </td>
                                        <td width="1%" visible='<%# (Request.QueryString["t"] != "6") %>'></td><%--ALLDEV-838--%>

                                                                        </tr>
                                                                    </table>
                                                                </ItemTemplate>
                                                            </asp:TemplateColumn>
                                                            <asp:BoundColumn Visible="false" ItemStyle-Width="20px" ItemStyle-HorizontalAlign="Center" HeaderText="<%$ Resources:WebResources, ConnectionStatus%>"></asp:BoundColumn>
                                                            <asp:BoundColumn DataField="StartMode" Visible="false"></asp:BoundColumn><%--FB 2501--%>
                                                            <asp:BoundColumn DataField="isVMR" Visible="false"></asp:BoundColumn> <%--FB 2550--%>
                                                            <asp:BoundColumn DataField="VMRType" Visible="false"></asp:BoundColumn> <%--FB 2550--%>
                                                            <asp:BoundColumn DataField="ConferenceType" Visible="false"></asp:BoundColumn> <%--ZD 100642--%>
                                                            <asp:BoundColumn DataField="IsCloudConference" Visible="false"></asp:BoundColumn> <%--ZD 100642--%>
                                                            <asp:BoundColumn DataField="UserTimeZone" Visible="false"></asp:BoundColumn> <%--ZD 100642--%>
                                                            <asp:BoundColumn DataField="ConfServiceType" Visible="false"></asp:BoundColumn> <%--ZD 100642--%>
															<asp:BoundColumn DataField="isWebExMeeting" Visible="false"></asp:BoundColumn><%--ZD 100221--%>
                                                            <asp:BoundColumn DataField="ConfBufferStartTime" Visible="false"></asp:BoundColumn> <%--ZD 100718--%>
                                                            <asp:BoundColumn DataField="ConfBufferEndTime" Visible="false"></asp:BoundColumn> <%--ZD 100718--%>
                                                            <asp:BoundColumn DataField="IsSynchronous" Visible="false"></asp:BoundColumn> <%--ZD 100036--%>
                                                            <asp:BoundColumn DataField="isExpressConference" Visible="false"></asp:BoundColumn> <%--ZD 101233 CellNo:23--%>
															<asp:BoundColumn DataField="IsPublicConference" Visible="false"></asp:BoundColumn> <%--ZD 101233 CellNo:24--%>		
                   											<asp:BoundColumn DataField="isOBTP" Visible="false"></asp:BoundColumn> <%--ZD 100513 CellNo:25--%>
                                                            <asp:BoundColumn DataField="isRPRMConf"  Visible="false"></asp:BoundColumn> <%--ZD 102997 CellNo:26--%>
                                                            <asp:BoundColumn DataField="ConferenceActualStatus"  Visible="false"></asp:BoundColumn> <%--ALLDEV-779 CellNo:27--%>
                                                            <asp:BoundColumn DataField="isLocAdmin" Visible="false"></asp:BoundColumn> <%--ALLDEV-779 CellNo:28--%>
                                                        </Columns>
                                                    </asp:DataGrid>
                                                </asp:TableCell>
                                            </asp:TableRow>
                                          </asp:Table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="11" align="center"> <%--FB 2274--%>
                                            <asp:Table ID="tblJoin" runat="server" Width="90%" Visible="false" BackColor="lemonchiffon">
                                                <asp:TableRow>
                                                    <asp:TableCell>
                                                        <table width="100%">
                                                            <tr>
                                                                <%--Window Dressing--%>
                                                                <td align="left" class="blackblodtext" colspan="6"><asp:Label ID="lblHdTxt" runat="server" Text="" ></asp:Label></td> <%--FB 2550--%>
                                                            </tr>
                                                            <tr>
                                                                <%--Window Dressing--%>
                                                                <td align="right" class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, ConferenceList_FirstName%>" runat="server"></asp:Literal><span class="reqfldText">*</span></td>
                                                                <td align="left">
                                                                    <asp:TextBox ID="txtJFirstName" runat="server" CssClass="altText"></asp:TextBox>
                                                                     <asp:RegularExpressionValidator ID="RegularExpressionValidator2" ControlToValidate="txtJFirstName" Display="dynamic" runat="server" 
                                                                        ErrorMessage="<%$ Resources:WebResources, InvalidCharacters16%>" ValidationExpression="^(a-z|A-Z|0-9)*[^\\/<>+?|!`\[\]{}\=@#$%&~]*$"></asp:RegularExpressionValidator><%--FB 1888--%>
                                                                    <asp:RequiredFieldValidator ID="regFN" ValidationGroup="Submit" ErrorMessage="<%$ Resources:WebResources, Required%>" runat="server" Display="dynamic" ControlToValidate="txtJFirstName" ></asp:RequiredFieldValidator>
                                                                </td>
                                                                <%--Window Dressing--%>
                                                                <td align="right" class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, ConferenceList_LastName%>" runat="server"></asp:Literal><span class="reqfldText">*</span></td>
                                                                <td align="left">
                                                                    <asp:TextBox ID="txtJLastName" runat="server" CssClass="altText"></asp:TextBox>
                                                                    <asp:RequiredFieldValidator ID="reqLN" ValidationGroup="Submit" ErrorMessage="<%$ Resources:WebResources, Required%>" runat="server" Display="dynamic" ControlToValidate="txtJLastName" ></asp:RequiredFieldValidator>
                                                                     <asp:RegularExpressionValidator ID="RegularExpressionValidator1" ControlToValidate="txtJLastName" Display="dynamic" runat="server" 
                                                                        ErrorMessage="<%$ Resources:WebResources, InvalidCharacters16%>" ValidationExpression="^(a-z|A-Z|0-9)*[^\\/<>+?|!`\[\]{}\=@#$%&~]*$"></asp:RegularExpressionValidator><%--FB 1888 ([A-Za-z0-9\\.\\@_\~#!`$^/ \-]+)--%>
                                                                </td>
                                                                <%--Window Dressing--%>
                                                                <td align="right" class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, ConferenceList_Email%>" runat="server"></asp:Literal><span class="reqfldText">*</span></td>
                                                                <td align="left">
                                                                    <asp:TextBox ID="txtJEmailAddress" runat="server" CssClass="altText"></asp:TextBox>
                                                                    <asp:RequiredFieldValidator ID="reqEmail" ValidationGroup="Submit" ErrorMessage="<%$ Resources:WebResources, Required%>" runat="server" Display="dynamic" ControlToValidate="txtJEmailAddress" ></asp:RequiredFieldValidator>
                                                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator5" ValidationGroup="Submit" ControlToValidate="txtJEmailAddress" Display="dynamic" runat="server" 
                                                                        ErrorMessage="<%$ Resources:WebResources, Invalidemail%>" ValidationExpression="^[a-zA-Z][\w\.-]*[a-zA-Z0-9]@[a-zA-Z0-9][\w\.-]*[a-zA-Z0-9]\.[a-zA-Z][a-zA-Z\.]*[a-zA-Z]$"></asp:RegularExpressionValidator><%--fogbugz case 389--%>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <%--Window Dressing--%>
                                                                <td align="left" colspan="6" class="blackblodtext"><asp:Label id="lblRooms" runat="server" text="<%$ Resources:WebResources, ConferenceList_lblRooms%>" visible="false"></asp:Label> </td> <%--FB 2550--%>
                                                            </tr>
                                                            <tr>
                                                                <td>&nbsp;</td>
                                                                <td colspan="5" align="left">
                                                                        <!--[Viek] Code changed as a fix for Issue 295 -->
                                                                        <asp:RadioButtonList ID="rdJRooms" RepeatDirection="Vertical" RepeatLayout="table" runat="server" DataSource='<%# GetJLocations((String)DataBinder.Eval(Container.DataItem, "Conference_Id").ToString(),(String)DataBinder.Eval(Container.DataItem, "ConferenceType").ToString()) %>' DataTextField="Name" DataValueField="ID" OnSelectedIndexChanged="ChangeJoinOption" AutoPostBack="true"></asp:RadioButtonList>
                                                                        <asp:Label ID="lblJNoRooms" Text="<%$ Resources:WebResources, ConferenceList_lblJNoRooms%>" CssClass="lblError" Visible="false" runat="server"></asp:Label>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td colspan="6">  
                                                                    <asp:Table ID="tblEA" runat="server" Width="100%" Visible="false">
                                                                        <asp:TableRow>
                                                                            <asp:TableCell HorizontalAlign="center">
                                                                                <table width="80%" border="0" cellpadding="0" cellspacing="0" bgcolor="lightgrey">
                                                                                    <tr>
                                                                                         <%--Window Dressing--%>
                                                                                        <td align="left" colspan="4" class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, ConferenceList_Pleaseprovide%>" runat="server"></asp:Literal></td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                         <%--Window Dressing--%>
                                                                                        <td align="right" class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, ConferenceList_ConnectionType%>" runat="server"></asp:Literal></td>
                                                                                        <td align="left">
                                                                                            <asp:DropDownList ID="lstJConnectionType" runat="server" CssClass="altLongSelectFormat">
                                                                                                <asp:ListItem Selected="True" Value="1" Text="<%$ Resources:WebResources, ResponseConference_DialintoMCU%>"></asp:ListItem>
                                                                                                <asp:ListItem Value="0" Text="<%$ Resources:WebResources, ResponseConference_DialoutfromMCU%>"></asp:ListItem>
                                                                                            </asp:DropDownList>
                                                                                        </td>
                                                                                         <%--Window Dressing--%>
                                                                                        <td align="right" class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, ConferenceList_OutsideNetwork%>" runat="server"></asp:Literal></td>
                                                                                        <td align="left">
                                                                                            <asp:CheckBox ID="chkJIsOutSide" runat="server" Checked="true" />
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                         <%--Window Dressing--%>
                                                                                        <td align="right" class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, ConferenceList_InterfaceType%>" runat="server"></asp:Literal></td>
                                                                                        <td align="left">
                                                                                            <asp:DropDownList ID="lstJProtocol" runat="server" CssClass="altLongSelectFormat">
                                                                                                <asp:ListItem Selected="True" Value="IP" Text="IP"></asp:ListItem>
                                                                                                <asp:ListItem Value="ISDN" Text="ISDN"></asp:ListItem>
                                                                                            </asp:DropDownList>
                                                                                        </td>
                                                                                         <%--Window Dressing--%>
                                                                                        <td align="right" class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, ConferenceList_Address%>" runat="server"></asp:Literal><span class="reqfldText">*</span></td>
                                                                                        <td align="left">
                                                                                            <asp:TextBox ID="txtJAddress" CssClass="altText" runat="server"></asp:TextBox>
                                                                                            <asp:RequiredFieldValidator ID="reqJAddress" Display="dynamic" ErrorMessage="<%$ Resources:WebResources, Required%>" runat="server" ControlToValidate="txtJAddress"></asp:RequiredFieldValidator>
                                                                                             <%--FB 1972--%>
                                                                                             <asp:RegularExpressionValidator ID="regConfName" ControlToValidate="txtJAddress" Display="dynamic" runat="server" 
                                                                                                ErrorMessage="<%$ Resources:WebResources, InvalidCharacters32%>" ValidationExpression="[A-Za-z0-9\\.\\@',:;\x22;_\~#!`$^/ \-]+"></asp:RegularExpressionValidator>
                                                                                             <%--<asp:RegularExpressionValidator ID="regConfName" ControlToValidate="txtJAddress" Display="dynamic" runat="server"           
                                                                                                ErrorMessage="(+'-&<>%;:)"+-&<>%*=()?[]{}  and double quotes are invalid characters for this field." ValidationExpression="[A-Za-z0-9\\.\\@_\~#!`$^/ \-]+"></asp:RegularExpressionValidator>--%>
                                                                                        </td>
                                                                                    </tr>
                                                                                </table>
                                                                            </asp:TableCell>
                                                                        </asp:TableRow>
                                                                    </asp:Table>
                                                                    <asp:Table ID="tblRA" runat="server" Visible="false" Width="100%">
                                                                        <asp:TableRow>
                                                                            <asp:TableCell HorizontalAlign="center">
                                                                                <%--Window Dressing--%>
                                                                                <table width="80%" cellpadding="2" cellspacing="5" class="tableBody">
                                                                                    <tr>
                                                                                        <td align="left"><asp:Literal Text="<%$ Resources:WebResources, ConferenceList_Pleasechooseo%>" runat="server"></asp:Literal></td>
                                                                                    </tr>
                                                                                </table>
                                                                            </asp:TableCell>
                                                                        </asp:TableRow>
                                                                    </asp:Table>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td align="right" colspan="3">
                                                                    <asp:Button ID="btnJCancel" Text="<%$ Resources:WebResources, ConferenceList_btnJCancel%>" CssClass="altMedium0BlueButtonFormat" OnClientClick="javascript:DataLoading('1');" OnClick="CancelJoin" runat="server" /> <%--ZD 100369--%>
                                                                </td>
                                                                <td align="left" colspan="3">
                                                                    <asp:Button ID="btnJSubmit" ValidationGroup="Submit" Text="<%$ Resources:WebResources, ConferenceList_btnJSubmit%>" CssClass="altMedium0BlueButtonFormat" OnClientClick="javascript:DataLoading('1');" CommandArgument="3" CommandName="Delete" runat="server" /> <%--ZD 100369--%>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </asp:TableCell>
                                                </asp:TableRow>
                                            </asp:Table>
                                        </td>
                                    </tr>
                                </table>
                            </ItemTemplate>
                            <FooterTemplate>
                                <br /><br />
                                <asp:Button ID="btnReset" CssClass="altLongBlueButtonFormat" Visible='<%# Request.QueryString["t"].ToString().Equals("6") %>' OnClick="ResetConference" runat="server" Text="<%$ Resources:WebResources, Reset%>" />
                                <asp:Button ID="btnApproveConference" CssClass="altLongBlueButtonFormat" Visible='<%# Request.QueryString["t"].ToString().Equals("6") %>' OnClick="ApproveConference" OnClientClick="javascript:return CountChecked();" runat="server" Text="<%$ Resources:WebResources, Submit%>" />
                                <br />
                            <%--Window Dressing--%>
                                <asp:Label ID="Label1" Text="<%$ Resources:WebResources, ConferenceList_Label1%>" runat="server" Font-Bold="true" CssClass="subtitleblueblodtext"></asp:Label><b>:</b>
                                <asp:Label runat="server" ID="lblTotalRecords" Text='<%# DataBinder.Eval(Container, "DataItem.TotalRecords")%>'></asp:Label>
                            </FooterTemplate>
                        </asp:TemplateColumn>
                        <asp:BoundColumn Visible="false" ItemStyle-Width="20px" ItemStyle-HorizontalAlign="Center" HeaderText="<%$ Resources:WebResources, ConnectionStatus%>"></asp:BoundColumn>
                        <asp:BoundColumn DataField="StartMode" Visible="false"></asp:BoundColumn><%--FB 2501--%>
                        <asp:BoundColumn DataField="isVMR" Visible="false"></asp:BoundColumn> <%--FB 2550--%>
                        <asp:BoundColumn DataField="VMRType" Visible="false"></asp:BoundColumn> <%--FB 2550--%>
                        <asp:BoundColumn DataField="ConferenceType" Visible="false"></asp:BoundColumn> <%--ZD 100642--%>
                        <asp:BoundColumn DataField="IsCloudConference" Visible="false"></asp:BoundColumn> <%--ZD 100642--%>
                        <asp:BoundColumn DataField="UserTimeZone" Visible="false"></asp:BoundColumn> <%--ZD 100642--%>
                        <asp:BoundColumn DataField="ConfServiceType" Visible="false"></asp:BoundColumn> <%--ZD 100642--%>
						<asp:BoundColumn DataField="isWebExMeeting" Visible="false"></asp:BoundColumn> <%--ZD 100221--%>
                        <asp:BoundColumn DataField="ConfBufferStartTime" Visible="false"></asp:BoundColumn> <%--ZD 100718--%>
                        <asp:BoundColumn DataField="ConfBufferEndTime" Visible="false"></asp:BoundColumn> <%--ZD 100718--%>
                        <asp:BoundColumn DataField="IsSynchronous" Visible="false"></asp:BoundColumn> <%--ZD 100036--%>
                        <asp:BoundColumn DataField="isExpressConference" Visible="false"></asp:BoundColumn> <%--ZD 101233 CellNo:23--%>
						<asp:BoundColumn DataField="IsPublicConference" Visible="false"></asp:BoundColumn> <%--ZD 101233 CellNo:24--%>
						<asp:BoundColumn DataField="isOBTP" Visible="false"></asp:BoundColumn> <%--ZD 100513 CellNo:25--%>
                        <asp:BoundColumn DataField="isRPRMConf"  Visible="false"></asp:BoundColumn> <%--ZD 102997 CellNo:26--%>
                        <asp:BoundColumn DataField="ConferenceActualStatus" Visible="false"></asp:BoundColumn> <%--ALLDEV-779 CellNo:27--%>
                        <asp:BoundColumn DataField="isLocAdmin" Visible="false"></asp:BoundColumn> <%--ALLDEV-779 CellNo:28--%>
                        </Columns>
                    <ItemStyle Height="20px" />
                
                </asp:DataGrid>
                    <asp:Label id="lblNoConferences" runat="server" text="<%$ Resources:WebResources, ConferenceList_lblNoConferences%>" visible="False" cssclass="lblError"></asp:Label>&nbsp;
                </td>
            </tr>
            <tr>
                <td align="center">
                    <asp:Table ID="tblPage" Visible="false" runat="server">
                        <asp:TableRow ID="TableRow1" runat="server">
                            <%--Window Dressing--%>
                            <asp:TableCell ID="tc1" Font-Bold="True" Font-Names="Verdana" Font-Size="Small" ForeColor="Blue" CssClass="subtitleblueblodtext" runat="server"><asp:Literal ID="Literal8" Text="<%$ Resources:WebResources, ManageAudioAddOnBridges_Pages %>" runat="server"></asp:Literal></asp:TableCell>
                            <asp:TableCell ID="tc2" runat="server"></asp:TableCell>
                        </asp:TableRow>
                    </asp:Table>

                </td>
            </tr>            
            <tr runat="server" id="trGoToLobby" visible="false" align="right"> <%--Fogbugz case 158--%>
                <td colspan="3" align="right">
                    <table cellpadding="2" cellspacing="2" border="0">
                        <tr>
                            <td>
                                <asp:Button id="btnGoBack" cssclass="altMedium0BlueButtonFormat" text="<%$ Resources:WebResources, ConferenceList_btnGoBack%>" runat="server" OnClientClick="javascript:GetbrowserLang();" onclick="GoToLobby"></asp:Button><%--ZD 103174--%> 
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
</center>
   </div>  <%--ZD 100393--%>
   <%--ZD 101597--%>
                <div id="PopupFormList" align="center" style="position: fixed; overflow: hidden;
                    border: 1px; width: 450px; display: none;  top: 300px;left: 510px;height:450px;">
                    <table align="center"class="tableBody" width="80%" cellpadding="5" cellspacing="5">
                        <tr class="tableHeader">
                            <td colspan="2" align="left" class="blackblodtext">
                                <asp:Literal ID="Literal11" Text="<%$ Resources:WebResources, ChooseFormHeading%>" runat="server"></asp:Literal>
                            </td>
                        </tr>
                        <tr >
                            <td align="left" colspan="2" >
                                <asp:RadioButtonList ID="rdEditForm" runat="server" RepeatDirection="Horizontal" RepeatLayout="Table" RepeatColumns="1" CellPadding="3" CellSpacing="3">
                                    <asp:ListItem Text="<%$ Resources:WebResources, LongForm%>" Value="1"></asp:ListItem>
                                    <asp:ListItem Text="<%$ Resources:WebResources, ExpressForm%>" Value="2"></asp:ListItem>
                                </asp:RadioButtonList>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                 <input id="btnOk" runat="server" type="button" class="altMedium0BlueButtonFormat"
                                    onclick="javascript:return fnRedirectForm();" value="<%$ Resources:WebResources, MasterChildReport_cmdConfOk%>" />

                            </td>
                            <td>
                                <input id="btnCancel" runat="server" type="button" class="altMedium0BlueButtonFormat"
                                    onclick="javascript:fnDivClose();" value="<%$ Resources:WebResources, Cancel%>" />
                            </td>
                        </tr>
                    </table>
                </div>             
                <input type="hidden" id="helpPage" value="107" runat="server" />
                <input type="hidden" id="txtSelectionCount" runat="server" />
                <input type="hidden" id="txtSearchType" runat="server" />
                <input type="hidden" id="txtSortBy" runat="server" />
                <input type="hidden" id="txtPublic" runat="server" />
                <input type="hidden" id="txtConferenceSearchType" runat="server" />
                <%--<input type="hidden" id="txtCOMConfigPath" runat="server" value="C:\VRMSchemas_v1.8.3\COMConfig.xml" />--%> <%--ZD 100263_Nov11--%>
                <%--<input type="hidden" id="txtASPILConfigPath" runat="server" value="C:\VRMSchemas_v1.8.3\" />--%> <%--ZD 100263_Nov11--%>
                <%--<input type="hidden" id="hdnAscendingOrder" runat="server" />--%><%--FB 2822--%> <%--ZD 101333--%>
                <input type="hidden" id="hdnSortingOrder" runat="server" value="0" /><%--FB 2822--%>
                <input type="hidden" id="hdnConfid" runat="server" /><%--ZD 101315-Extend conf--%>
                <input type="hidden" id="hdnMins" runat="server" /><%--ZD 101315-Extend conf--%>

<script language="javascript">
    //FB Case 723 - Remove Calendar dropdown from Approval screen
    //ZD 103481 - Start
    function fnRefList() 
    {
        if (document.getElementById("lstListType") != null) //FB 2968 //&& document.getElementById("lstCalendar") != null //ZD 101388 Commented 
        {
            //ZD 101388 Commented Starts
            //if(document.getElementById("lstListType").value == 6)
            //    document.getElementById("lstCalendar").style.display="none";
            //else
            //    document.getElementById("lstCalendar").style.display="";  
            //ZD 101388 Commented End

            var lsttype = document.getElementById("lstListType").value;

            var ChkDisney = "";
            if ('<%=Application["Client"]%>'.toUpperCase() == "DISNEY")
                ChkDisney = "1";

            if (lsttype == 2 || lsttype == 3 || (ChkDisney == "1" && lsttype == 4) || (ChkDisney == "1" && lsttype == 5) || (ChkDisney == "1" && lsttype == 6)) 
            {
                var tim = '15000';
                var chk = document.getElementById("Refreshchk");

                if (chk) {
                    if (chk.checked == true)
                        tim = '45000'
                }
                if (document.getElementById("hdnConfid").value == "") //ZD 101315 - Extend Conf
                {
                    setTimeout("fnRefPageList()", tim);
                }
            }            
        }
    }
    fnRefList();

   function fnRefPageList() {
       if (document.getElementById('PopupFormList') != null) {
           if (document.getElementById('PopupFormList').style.display == 'block') {
               DataLoading(0);
           }
           else {
               DataLoading(1);
               __doPostBack('btnRefreshList', '');
           }
       }
       else {
           DataLoading(1);
           __doPostBack('btnRefreshList', '');
       }
   }
   //ZD 103481 - End
   
   if(document.getElementById("ImageButton3") != null) {
        //ZD 101388 Commented
        //if (isExpressUser == 1) //FB 1779
        //    document.getElementById("ImageButton3").style.display = "none";
        //else
       document.getElementById("ImageButton3").style.display = "block";

}

//FB 2487 - Start
var obj = document.getElementById("errLabel");
if (obj != null) {
    var strInput = obj.innerHTML.toUpperCase();

    if (((strInput.indexOf("SUCCESS") > -1) && !(strInput.indexOf("UNSUCCESS") > -1) && !(strInput.indexOf("ERROR") > -1))
            || ((strInput.indexOf("EXITOSA") > -1) && !(strInput.indexOf("FALLIDA") > -1) && !(strInput.indexOf("ERROR") > -1))
            || ((strInput.indexOf("SUCC�S") > -1 || strInput.indexOf("�TABLIE") > -1) && !(strInput.indexOf("�CHEC") > -1) && !(strInput.indexOf("ERREUR") > -1)) //French            
            ) {
        obj.setAttribute("class", "lblMessage");
        obj.setAttribute("className", "lblMessage");
    }
    else {
        obj.setAttribute("class", "lblError");
        obj.setAttribute("className", "lblError");
    }
}
//FB 2487 - End  

//FB 2598 Starts
    // FB 2779 Starts
    function fnAdjustTd() {
       
        var t = '<%=Request.QueryString["t"]%>'
        var hf = '<%=Request.QueryString["hf"]%>'
        if (t == "4" && hf == "1") { //ZD 100381
            if (document.getElementById("dgConferenceList") != null) {
                if (document.getElementById("dgConferenceList_ctl01_td7head") != null)
                    document.getElementById("dgConferenceList_ctl01_td7head").width = "";
                var dgrow = document.getElementById("dgConferenceList").rows.length;                
                for (var i = 2; i < dgrow; i++) {
                    if (i < 9)
                        i = "0" + i;
                    if (document.getElementById("dgConferenceList_ctl" + i + "_Td7") != null)
                        document.getElementById("dgConferenceList_ctl" + i + "_Td7").width = "";
                }
            }
        }
        //ZD 100821 start
//        // FB 2987 Starts
//        if(t == "6")
//        {
//            if(navigator.userAgent.indexOf('Trident/4.0') > -1 || navigator.userAgent.indexOf('Trident/5.0') > -1)
//            {   //FB 3055
//                if(document.getElementById("tblappr1") != null)
//                    document.getElementById("tblappr1").style.marginLeft = "-15%";
//                if(document.getElementById("tblappr2") != null)
//                    document.getElementById("tblappr2").style.marginLeft = "-23%";
//            }
//        }
//        // FB 2987 Ends //ZD 100821 end
    }
    // FB 2779 Ends

    function removeOption()
    {
        // FB 2779 Starts
        if(navigator.userAgent.indexOf('Trident') > -1)
            fnAdjustTd();
        // FB 2779 Ends
    }

    removeOption();

    //ZD 100718 Starts
    function disableOption() {
        if (document.getElementById("dgConferenceList_ctl01_rdUndecideAll") != null)
            document.getElementById("dgConferenceList_ctl01_rdUndecideAll").disabled = true;

        if (document.getElementById("dgConferenceList_ctl01_rdApproveAll") != null)
            document.getElementById("dgConferenceList_ctl01_rdApproveAll").disabled = true;

        if (document.getElementById("dgConferenceList_ctl01_rdDenyAll") != null)
            document.getElementById("dgConferenceList_ctl01_rdDenyAll").disabled = true;
    }
    //ZD 100718 End

    document.onkeydown = function(evt) {
        evt = evt || window.event;
        var keyCode = evt.keyCode;
        if (keyCode == 8) {
            if (document.getElementById("btnCancel") != null) { // backspace
                var str = document.activeElement.type;
                if (!(str == "text" || str == "textarea" || str == "password")) {
                    document.getElementById("btnCancel").click();
                    return false;
                }
            }
            if (document.getElementById("btnGoBack") != null) { // backspace
                var str = document.activeElement.type;
                if (!(str == "text" || str == "textarea" || str == "password")) {
                    document.getElementById("btnGoBack").click();
                    return false;
                }
            }
        }
        if (typeof fnOnKeyDown == 'function')//ZD 100381
            fnOnKeyDown(evt);
    };    
//FB 2598 Ends
    //ZD 100393 start
    if (document.getElementById("lblNoConferences") != null)
        document.getElementById("divconflistheight").style.height = "400px"
    //ZD 100393 End
    //ZD 101597
    //document.addEventListener("click", handler, true);
    document.onclick = handler; //ZD 101931

    function fnSelectForm() {

        var elementRef = document.getElementById('rdEditForm'); //ZD 102792
        var inputElementArray = elementRef.getElementsByTagName('input');

        var args = fnSelectForm.arguments;
        var hdnEditID = document.getElementById("hdnEditID");
        hdnEditID.value = args[0];
        //ZD 102200 Starts
        if ('<%=Session["hasConference"]%>' == '1' && '<%=Session["hasExpConference"]%>' == "1") {
            document.getElementById("PopupFormList").style.display = 'block';
            if (document.getElementById('rdEditForm_1') != null)
                document.getElementById('rdEditForm_1').checked = false;
            if (document.getElementById('rdEditForm_2') != null)
                document.getElementById('rdEditForm_2').checked = false;

            for (var i = 0; i < inputElementArray.length; i++) {
                var inputElement = inputElementArray[i];
                inputElement.checked = false;
            }
            return false; //ZD 102792
        }
        //ZD 102200 Ends
    }

    function fnRedirectForm() {
    
        var args = fnRedirectForm.arguments;
        var hdnTempID = document.getElementById("hdnTempID");
        var hdnEditForm = document.getElementById("hdnEditForm");
        hdnEditForm.value = "";

        if (hdnTempID.value == "")
            hdnTempID.value = "Temp";

        if (document.getElementById("rdEditForm_0").checked)
            hdnEditForm.value = "1";
        else if (document.getElementById("rdEditForm_1").checked)
            hdnEditForm.value = "2";

        if (hdnEditForm.value != "") {
           
            var btnTemp = document.getElementById(hdnTempID.value);
            if (btnTemp != null)
                btnTemp.click();

            DataLoading(1);

            document.getElementById("PopupFormList").style.display = 'None';
        }
        else
            alert("Please select the form to edit a conference.");

        return false;
    }

    function fnDivClose() {
        var hdnEditForm = document.getElementById("hdnEditForm");
        hdnEditForm.value = "";
        document.getElementById("PopupFormList").style.display = 'none';

        fnRefList(); //ZD 103481
    }


    function handler(e) {
        var obj = document.getElementById('PopupFormList');
        var isoutofpopup = false; //ZD 104319
        if (navigator.userAgent.indexOf('Trident') > -1) {
            if (e == null && e == undefined) {
                e = e || window.event;
                var target = e.target || e.srcElement;
            }

            if (obj.style.display != 'none') {

                if (target.id.indexOf('Temp') < 0 && target.id != "btnCancel" && target.id != "btnOk" && e.target.id.indexOf('rdEditForm') < 0 ) {
                    isoutofpopup = true; //ZD 104319
                }
                //ZD 104319 - start
                if (e.target.htmlFor != undefined) {
                    if (e.target.htmlFor.indexOf('rdEditForm') < 0)
                        isoutofpopup = true;
                    else
                        isoutofpopup = false;
                }
                if(isoutofpopup == true)
                    e.returnValue = false;
                //ZD 104319 - end
            }
        }
        else 
        {
            if (obj.style.display != 'none') 
            {
                if (e.target.id.indexOf('Temp') < 0 && e.target.id != "btnCancel" && e.target.id != "btnOk" && e.target.id.indexOf('rdEditForm') < 0) {
                    isoutofpopup = true; //ZD 104319
                }
                //ZD 104319 - start
                if (e.target.htmlFor != undefined) {
                     if(e.target.htmlFor.indexOf('rdEditForm') < 0)
                         isoutofpopup = true;
                     else
                         isoutofpopup = false;
                }

                if (isoutofpopup == true) {
                     e.stopPropagation();
                     e.preventDefault();
                 }
                 //ZD 104319 - end
            }
        }

    }

    document.onkeydown = function (event) {
        if (event.keyCode == 27) {
            
            var obj = document.getElementById('PopupFormList');

            if (obj != null && obj.style.display != 'none')
                obj.style.display = 'none';
        }
    }

    //ZD 101597 - End
    
</script>

                
</form>
<%--ZD 100642_11jan2014 Starts--%>
<div id="PopupRoomList" class="rounded-corners" style="position: absolute;
    overflow:hidden; border: 0px;
    width: 95%; display: none;">    
    <iframe src="" id="RoomList" name="MCUList" style="height: 630px; border: 0px; overflow:hidden; width: 95%; overflow: hidden;"></iframe>
	</div> <%--ZD 100718 Starts--%>
	<div id="PopupMCUList" class="rounded-corners" style="position: absolute;
    overflow:hidden; border: 0px;
    width: 1000px; display: none;">    
    <iframe src="" id="MCUList" name="MCUList1" style="height: 530px; border: 0px; overflow:hidden; width: 1000px; overflow: hidden;"></iframe>
	</div>	
	<%--ZD 100718 End--%>
<%--ZD 100642_11jan2014 End--%>
<%--ZD 101315-Extend conf Start--%>
    <div id="popmsg" title="MyVRM" style="left: 425px; position: absolute; background-color: White;
        top: 420px; z-index: 9999; width: 350px; height:120px; display: none;">
        <table border="0" cellpadding="0" cellspacing="0" style="background-color:" width="100%" id="tblpopup">
            <tr style="height: 25px;" id="eTime">
                <td style="background-color: #3075AE;" colspan="2">
                    <b style="font-size: small; margin-left: 10px; color: White; font-family: Verdana;
                        font-style: normal;"><asp:Literal ID="Literal4" Text="<%$ Resources:WebResources, MonitorMCU_ExtendConferen%>" runat="server"></asp:Literal></b>
                </td>
            </tr>
            <tr>
                <td height="10px;">
                </td>
            </tr>
            <tr class="pMsgDuration" >
                <td style="padding-left: 10px;">
                    <label><asp:Literal ID="Literal5" Text="<%$ Resources:WebResources, recurNET_Duration%>" runat="server"></asp:Literal>:</label>
                </td>
                <td align="left">                
                    <input type="text" id="msgMinutes" runat="server" class="altText" style="width:100px;"/>                 
                    <label><asp:Literal Text="<%$ Resources:WebResources, ExpressConference_mins%>" runat="server"></asp:Literal></label>                    
                </td>
            </tr>
            <tr style="height: 40px;">
                <td align="center" colspan="2">
                    <div id="popupstatus" style="display: none;">
                    </div>  
                    <button id="btnpopupcancel" class="altMedium0BlueButtonFormat" onclick="javascript:HideExtendconf();"><asp:Literal ID="Literal6" Text="<%$ Resources:WebResources, Cancel%>" runat="server"></asp:Literal></button> &nbsp;&nbsp;
                    <button id="btnpopupSubmit" class="altMedium0BlueButtonFormat" runat="server" onclick="javascript:Extendconf();" onserverclick="SubmitExtendTime" ><asp:Literal ID="Literal7" Text="<%$ Resources:WebResources, ManageConference_SendMsg%>" runat="server"></asp:Literal></button>
                </td>
            </tr>
            <tr>
                <td height="20px;">
                </td>
            </tr>
        </table>
    </div>
    
    <%--ZD 101315-Extend conf End--%>
</body>
</html>
<%--code added for Soft Edge button--%>
<%--FB 1822--%>
<%--<script type="text/javascript" src="inc/softedge.js"></script>--%>
<%--ZD 101388 Commented Starts--%>
<% if (Request.QueryString["hf"] == null) { %>

    <!-- #INCLUDE FILE="inc/mainbottomNET.aspx" -->

<%}%>
<%--ZD 101388 Commented End--%>