<%--ZD 100147 start--%>
<%--/* Copyright (C) 2015 myVRM - All Rights Reserved	
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.		
*
* You should have received a copy of the myVRM license with	
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/--%><%--ZD 100147 ZD 100886 End--%>
<%@ Page Language="C#" Inherits="ns_Conference.ResponseConference" Buffer="true" ValidateRequest="false" %><%--ZD 100170--%>

<%@ Register Assembly="MetaBuilders.WebControls.ComboBox" Namespace="MetaBuilders.WebControls"
    TagPrefix="mbcbb" %>
<%@ Register TagPrefix="cc1" Namespace="myVRMWebControls" Assembly="myVRMWebControls" %>
<%--FB 2136--%>
<%--Edited for FF--%>
<%if (Request.ServerVariables["HTTP_USER_AGENT"].ToString().ToUpper().Contains("MSIE"))
{%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%}
else {%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<%} %>
<meta http-equiv="X-UA-Compatible" content="IE=8" /> <%--FB 2779--%>
<% 
    HttpContext.Current.Application.Lock(); //Added for FB 1648
    //Version and Year upgradation start 
    HttpContext.Current.Application.Remove("CopyrightsDur");
    HttpContext.Current.Application.Remove("Version");
    HttpContext.Current.Application.Add("Version", "2.9316.3.0");//Can Edit for version upgradation //FB 2864
    HttpContext.Current.Application.Add("CopyrightsDur", "2002-2016");//FB 1814//Can Edit for year upgradation //FB 2682
    //Version and Year upgradation end  
    HttpContext.Current.Application.UnLock();
    //Response.Write(Request.QueryString["t"].ToString());
    if (Request.QueryString["t"] != null) //FB 1628
    if (Request.QueryString["t"].ToString().Equals("hf")) { %>
<!-- #INCLUDE FILE="inc/maintop4.aspx" -->
<%-- FB 1861 --%>
<!--  #INCLUDE FILE="../en/inc/Holiday.aspx"  -->
<table width="100%">
    <tr>
        <td>
            <img src="../en/Organizations/Original/Image/lobbytop1024.jpg" width="100%" height="72" alt="Lobby Top Image" /><%--Code added for FB 1694 FB 1830--%> <%--ZD 100419--%>
        </td>
    </tr>
</table>
<% } else { %>
<!-- #INCLUDE FILE="inc/maintopNET.aspx" -->
<% } %>
<style type="text/css">
.RadioButtonWidth input
 { margin-left : 44px }
</style>
<script runat="server">

</script>

<script language="javascript">
  var servertoday = new Date();
function pdfReport()
{
        var loc = document.location.href; 
        var path = '<%=Session["OrgCSSPath"]%>';
        path = path.replace("../", ""); //FB 1830
        path = path.split('.')[0] + '<%=Session["ThemeType"]%>' + ".css"; // FB 2815
        var imagepath = '<%=Session["OrgBanner1600Path"]%>'; 
        loc = loc.substring(0,loc.indexOf("ResponseConference.aspx") - 3);//FB 1830
        var htmlString = document.getElementById("tblMain").innerHTML;
        var toBeRemoved = document.getElementById("tblActions");
        if (toBeRemoved != null)
            htmlString = htmlString.replace(toBeRemoved.innerHTML, "");
        toBeRemoved = document.getElementById("trButtonsHeader");
        if (toBeRemoved != null)
            htmlString = htmlString.replace(toBeRemoved.innerHTML, "");
        toBeRemoved = document.getElementById("trSubmit");
        if (toBeRemoved != null)
            htmlString = htmlString.replace(toBeRemoved.innerHTML, "");
        toBeRemoved = document.getElementById("trButtons");
        if (toBeRemoved != null)
            htmlString = htmlString.replace(toBeRemoved.innerHTML, "");

        htmlString = htmlString.replace(new RegExp("\"","g"), "'");
        //remove all image source relative paths with the absolute path
        htmlString = htmlString.replace(new RegExp("image/","g"), loc + "image/");
        //insert the banner on top of page and style sheet on top.
        htmlString = "<html><link rel='stylesheet' type='text/css' href='" + loc + path +"' /><body><center><table><tr><td></td></tr></table>" + htmlString + "</center></body></html>";
       // htmlString = "<html><link rel='stylesheet' type='text/css' href='" + loc + path +"' /><body><center><table><tr><td><img src='" + loc + imagepath + "' width='100%' height='72'></td></tr></table>" + htmlString + "</center></body></html>";


        if (document.getElementById("tempText") != null)
            document.getElementById("tempText").value = htmlString;
}
</script>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <%--FB 1628 START--%>
    <%--<meta http-equiv="Cache-Control: no-store, no-cache, must-revalidate, post-check=0, pre-check=0,max-age=0" content="no-cache"/> 
<meta http-equiv="pragma" content="no-cache"/> 
<meta http-equiv="expires" content="-1" />
<meta http-equiv="refresh" content="5" />--%>
    <%--FB 1757 - Commented--%>
    <%--<style>
@media print {

    .btprint {
        display:none;
        }
}
</style> --%>
    <title>Accept/Reject Conference</title>

    <script type="text/javascript" src="inc/functions.js"></script>

    <%--FB 1861--%>
    <%--<script type="text/javascript" src="script/cal.js"></script>--%>

    <script type="text/javascript" src="script/cal-flat.js"></script>

    <script type="text/javascript" src="../<%=Session["language"] %>/lang/calendar-en.js"></script>

    <script type="text/javascript" src="script/calendar-setup.js"></script>

    <script type="text/javascript" src="script/calendar-flat-setup.js"></script>

    <link rel="stylesheet" type="text/css" media="all" href="css/calendar-blue.css" />
    <%--FB 1861--%><%--FB 1947--%>
    <%--FB 1757 - Start--%>

    <script language="javascript" type="text/javascript">
    function saveToOutlookCalendar (iscustomrecur, isrecr, f, instInfo) 
  {
	// outlook do not support custom recur. Maybe LN can, but current can not test.
	var confid = document.getElementById("<%=txtConfID.ClientID%>").value;
	//alert(confid);
	instanceInfo = "";
	//instInfo = document.getElementById("Recur").value;
	//alert(f + " : " + instInfo);
	if (parseInt(iscustomrecur,10)) { // ZD 101722
		getCustomRecur(confid);
	} else {
		//if (parseInt(isrecr))
		//	getRecur(confid);
		//else
			setConfInCalendar(f, confid);
	}
  }
  function getCustomRecur(cid)
{
	if (cid != "") {
		if (ifrmPreloading != null)
			ifrmPreloading.window.location.href = "dispatcher/conferencedispatcher.asp?cmd=GetInstances&mode=21&frm=preload&cid=" + cid;
	}
}
function setConfInCalendar(f, instInfo)
{

    var confid = document.getElementById("<%=txtConfID.ClientID%>").value;
    if (confid == "")
        confid = document.getElementById("<%=lblConfUniqueID.ClientID%>").innerHTML;
	//url = "saveconfincal.asp?f=" + f + "&ii=" + confid;
	//alert(f);
	//Code Changed for FB 1410 - Start
//	url = "SetSessionOutXml.aspx?tp=saveconfincal.asp&ii=" + instInfo + "&f=" + f;
	url = "GetSessionOutXml.aspx?ii=" + instInfo + "&f=" + f;
	//Code Changed for FB 1410 - End
//	alert(url);
	if (!window.winrtc) {	// has not yet been defined
		winrtc = window.open(url, "", "width=600,height=350,top=0,left=0,resizable=no,scrollbars=no,status=no");
		winrtc.focus();
	} else {	// has been defined
	    if (!winrtc.closed) {     // still open
	    	winrtc.close();
	        winrtc = window.open(url, "", "width=600,height=350,top=0,left=0,resizable=no,scrollbars=no,status=no");
			winrtc.focus();
		} else {
	        winrtc = window.open(url, "", "width=600,height=350,top=0,left=0,resizable=no,scrollbars=no,status=no");
	        winrtc.focus();
		}
	}
}
    </script>

    <%--FB 1757 - End--%>
    <%--FB 2136 start--%>
    <style type="text/css">
        html.overflow-hidden
        {
            overflow: hidden;
        }
        .innerDiv
        {
            left: 0;
            top: 0;
            vertical-align: top;
            position: absolute;
            float: right; /*background-color:#eeeeee;*/
            width: 160px;
            height: 480px;
            margin-top: 175px;
            margin-left: 25px;
        }
        .imgHolder
        {
            width: 400px;
            height: 300px;
            overflow: hidden;
            background-color: white;
            z-index: 10;
        }
        .area
        {
            position: absolute;
            border-style: dashed;
            border-width: thin;
            background-color: White;
            cursor: move;
            z-index: 100;
            filter: alpha(opacity=50);
            opacity: 0.5;
            width: 90px; /* 240 */
            height: 120px; /* 320 */
        }
        .corner
        {
            position: absolute;
            z-index: 10000;
            right: 0;
            bottom: 0;
            cursor: se-resize;
            border-right-style: solid;
            border-bottom-style: solid;
            width: 20px;
            height: 20px;
            border-width: 3px;
        }
        .previewImg
        {
            position: absolute;
            clip: rect(0px,90px,120px,0px); /*
    width:640px; 
    height:480px; 
    */
            left: 300px;
            top: 25px;
        }
        .mask
        {
            z-index: 25;
            background-color: Gray;
            left: 0px;
            top: 0px;
            position: absolute;
            display: none;
            filter: alpha(opacity=50);
            opacity: 0.5;
        }
        img
        {
            border: none;
        }
    </style>

    <script type="text/javascript">

function noError(){return true;}
window.onerror = noError;


var _startX = 0;            // mouse starting positions
var _startY = 0;
var _offsetX = 0;           // current element offset
var _offsetY = 0;
var _dragElement;           // needs to be passed from OnMouseDown to OnMouseMove
var _oldZIndex = 0;         // we temporarily increase the z-index during drag
//var _debug = $('debug');    // makes life easier
var draging = -1;

var reNameIcon = "";

var globChange = -1;

// mouseX and mouseY use grab mouse position of the clicked label
var mouseX = 0;
var mouseY = 0;

// contains scroll values of div if image greater than 800x600
var divScrollLeft = 0;
var divScrollTop = 0;

var divMoveLeft=0;
var divMoveTop=0;


// to find -area- or -corner- of the image selector
var curClass="";

// update these values to perform zoom
var imgCont_Width=0; //640
var imgCont_Height=0; //480

// use these value for resize
var area_Cur_Width=90;
var area_Cur_Height=120;

// Expected output size
var preview_Width=90;
var preview_Height=120;




InitDragDrop();



function InitDragDrop()
{
    document.onmousedown = OnMouseDown;
    document.onmouseup = OnMouseUp;


}

function findPosX(obj)
{
    var curleft = 0;
    if(obj.offsetParent)
        while(1) 
        {
        curleft += obj.offsetLeft;
        if(!obj.offsetParent)
            break;
        obj = obj.offsetParent;
        }
    else if(obj.x)
        curleft += obj.x;
    return curleft;
}

function findPosY(obj)
{
    var curtop = 0;
    if(obj.offsetParent)
        while(1)
        {
        curtop += obj.offsetTop;
        if(!obj.offsetParent)
            break;
        obj = obj.offsetParent;
        }
    else if(obj.y)
        curtop += obj.y;
    return curtop;
}

function OnMouseDown(e)
{
    // IE is retarded and doesn't pass the event object
    if (e == null) 
        e = window.event; 
    
    // IE uses srcElement, others use target
    var target = e.target != null ? e.target : e.srcElement;
    
    //_debug.innerHTML = target.className == 'drag' 
    //    ? 'draggable element clicked' 
    //    : 'NON-draggable element clicked';

    // This block takes mouse x and y position to display rename div
	if (e.pageX || e.pageY)
	{
		mouseX = e.pageX;
		mouseY = e.pageY;
	}
	else if (e.clientX || e.clientY)
	{
		mouseX = e.clientX + document.body.scrollLeft + document.documentElement.scrollLeft;
		mouseY = e.clientY + document.body.scrollTop + document.documentElement.scrollTop;
	}
	
    // for IE, left click == 1
    // for Firefox, left click == 0
    
    
    //This block prevent the exchange of empty contents by assigning dragging=0;
    var source=target.innerHTML.length;
    
    if (source!=null)
    {
    if(source < 50) // Check content size
    draging = 0;
    else
    draging = 1;
    }
    
    if ((e.button == 1 && window.event != null || e.button == 0) && (target.className == 'area') || (target.className == 'corner'))
    {
        // grab the mouse position
        curClass=target.className;
        
        _startX = e.clientX;
        _startY = e.clientY;
        
        //alert(_startX + ',' + _startY);
        
        // grab the clicked element's position

        _offsetX = ExtractNumber(target.style.left);
        _offsetY = ExtractNumber(target.style.top);
        
        //_startX = findPosX(target) - 10;
        //_startY = findPosY(target) - (document.body.scrollTop+10);
        
        //_startX = findPosX(target);
        //_startY = findPosY(target);
        
        //alert(findPosX(target));
        //alert(findPosY(target));
       
        // bring the clicked element to the front while it is being dragged
        _oldZIndex = target.style.zIndex;
        target.style.zIndex = 100;
        
        // we need to access the element in OnMouseMove
        _dragElement = target;

        // tell our code to start moving the element with the mouse
        document.onmousemove = OnMouseMove;
        
        // cancel out any text selections
        document.body.focus();

        // prevent text selection in IE
        document.onselectstart = function () { return false; };
        // prevent IE from trying to drag an image
        target.ondragstart = function() { return false; };
        
        // prevent text selection (except IE)
        return false;
    }
}


function OnMouseMove(e)
{
    if (e == null) 
        var e = window.event; 
        
    if(curClass=="area")
    {


    var left, top, width, height, tempW, tempH;
    if(_dragElement != null)
    {
    left = document.getElementById(_dragElement.id).style.left;
    top = document.getElementById(_dragElement.id).style.top;
    
    tempW = document.getElementById(_dragElement.id).style.width;
    tempH = document.getElementById(_dragElement.id).style.height;
    width = tempW.substring(0,tempW.length-2)*1; //width2;
    height = tempH.substring(0,tempH.length-2)*1; //height2;
    }
//    document.getElementById('TextX').value = width2.substring(0,width2.length-2)*1; //width2;
//    document.getElementById('TextY').value = height2.substring(0,height2.length-2)*1; //height2;

    
    var valX = _offsetX + e.clientX - _startX;
    var valY = _offsetY + e.clientY - _startY;
    
    //var tempX = valX + width; //(diffX.substring(0,diffX.length-2)*1);
    //var tempY = valY + height; //(diffY.substring(0,diffY.length-2)*1);

    
    if(valX > ((imgCont_Width-1)-width))
    valX=(imgCont_Width-1)-width;
    else if(valX<0)
    valX=1;
    
    if(valY > ((imgCont_Height-1)-height))
    valY=(imgCont_Height-1)-height;
    else if(valY<0)
    valY=1;
    
    //document.getElementById('TextX').value = tempX;
    //document.getElementById('TextY').value = tempY;

    
    if( (valX > 0 && valX < (imgCont_Width-width) ) && (valY > 0 && valY < (imgCont_Height-height) ) )
    {
    // this is the actual "drag code"
    //if(valX < 543 && valX > -1) // add widthX to 543
    _dragElement.style.left = valX + 'px';
    //if(valY < -337 && valY > -601) // add heightY to -337
    _dragElement.style.top = valY + 'px';
    
    //document.getElementById('TextX').value = divScrollLeft + (left.substring(0,left.length-2) * 1);
    //document.getElementById('TextY').value = divScrollTop + (top.substring(0,top.length-2) * 1);
    
    }
    
    
    
    
    
    
    
    updtMovement();
    
    //_debug.innerHTML = '(' + _dragElement.style.left + ', ' + _dragElement.style.top + ')';   
    }
    
    if(curClass=="corner")
    {
    
    if(_offsetX<20)
    {
    _offsetX=90;
    _offsetY=120;
    }
    
    valX = _offsetX + e.clientX - _startX;
    valY = _offsetY + e.clientY - _startY;
    
    
    var left = document.getElementById('areaDiv').style.left;
    var top = document.getElementById('areaDiv').style.top;
    
    var width, height;
    
    var diffX = (valX + 20);
    var diffY = Math.round((valX + 20)*4/3);
    
    var tempW = (valX + 20) + (left.substring(0,left.length-2) * 1);
    var tempH = Math.round((valX + 20)*4/3) + (top.substring(0,top.length-2) * 1);
    
    //document.getElementById('Text1').value = diffX;
    //document.getElementById('Text2').value = diffY;
    
    
    //width = tempW.substring(0,tempW.length-2)*1; //width2;
    //height = tempH.substring(0,tempH.length-2)*1; //height2;
    
    
    /*
    if(tempW > imgCont_Width)
    tempW=imgCont_Width;
    else if(tempW<0)
    tempW=1;
    
    if(tempH > imgCont_Height)
    tempH=imgCont_Height;
    else if(tempH<0)
    tempH=1;
    
    */
    if(diffX > 60 && diffY > 80)
    {
    
    if( (tempW > 0 && tempW < imgCont_Width) && (tempH > 0 && tempH < imgCont_Height) )
    {
    
    //if(valX < 543 && valX > -1) // add widthX to 543
    //if(valY < -337 && valY > -601) // add heightY to -337
    _dragElement.style.left = valX + 'px';
    _dragElement.style.top = Math.round(valX*4/3+5) + 'px';
    //var curWidth = document.getElementById('areaDiv').style.width + 1;

    document.getElementById('areaDiv').style.width = (valX + 20) + 'px';
    document.getElementById('areaDiv').style.height = Math.round((valX + 20)*4/3) + 'px';
    
    area_Cur_Width = (valX + 20) + 'px';
    area_Cur_Height = Math.round((valX + 20)*4/3) + 'px';
    
    }
    
    }
    
    //document.getElementById('Text1').value = _offsetX;
    //document.getElementById('Text2').value = _offsetY;
    
    updtMovement();

    }
}

function OnMouseUp(e)
{
//document.getElementById('pane1').innerHTML="";

//debugger;
//alert(e.target.id);
    if (e == null) 
        e = window.event; 
    
    // IE uses srcElement, others use target
    var target = e.target != null ? e.target : e.srcElement;
    
    var swap=target.innerHTML;

    if (_dragElement != null)
    {
        _dragElement=null;
        return false;
        
        _dragElement.style.zIndex = _oldZIndex;

        // we're done with these events until the next OnMouseDown
        document.onmousemove = null;
        document.onselectstart = null;
        _dragElement.ondragstart = null;

        // this is how we know we're not dragging      
        //_dragElement = null;
        
        _dragElement.style.left=_offsetX;
        _dragElement.style.top=_offsetY;
        
        var elementId = target.id.substring(0,4);
        if((elementId=="pane" || elementId=="cont") && draging != "0")
        {
        target.innerHTML=_dragElement.innerHTML;
        _dragElement.innerHTML=swap;
        globChange = 1;
            if(document.getElementById(target.childNodes[0].childNodes[0].id)!=null)
            {
            //alert('inside targe');
                document.getElementById(target.childNodes[0].childNodes[0].id).removeAttribute("style");
                if(target.id.substring(0,4)=="cont")
                {
                reNameIcon=target.childNodes[2].id;
                changeDefault();
                }
            }
            
            if(document.getElementById(_dragElement.childNodes[0].childNodes[0].id)!=null)
            {
            //alert('inside dragelement');
                document.getElementById(_dragElement.childNodes[0].childNodes[0].id).removeAttribute("style");
                if(_dragElement.id.substring(0,4)=="cont")
                {
                reNameIcon=_dragElement.childNodes[2].id;
                changeDefault();
                }
            }
            

//            if(target.id.substring(0,4)=="pane")
//            document.getElementById(target.childNodes[0].childNodes[0].id).setAttribute("style", "opacity:1;");
//            else
//            document.getElementById(target.childNodes[0].childNodes[0].id).setAttribute("style", "opacity:0.5;");
        }
        //alert(target.id); // destination
        //alert(_dragElement.id); //target
        
        //_debug.innerHTML = 'mouse up';
        _dragElement = null;
    }
}    

function updtMovement()
{
    
    ele=document.getElementById('areaDiv');
    
    var percent=0;
    if(area_Cur_Width*1 != area_Cur_Width)
    {
    percent = area_Cur_Width.substring(0,area_Cur_Width.length-2) / preview_Width;
    }
    else
    {
    percent = area_Cur_Width / preview_Width;
    }
    
    curX = Math.round(findPosX(ele) - divMoveLeft)/percent  - Math.round(1/percent);
    curY = Math.round(findPosY(ele) - divMoveTop)/percent - Math.round(1/percent);
    
    //document.getElementById('Text1').value = area_Cur_Width; //document.getElementById('imgCont').width;
    //document.getElementById('Text2').value = area_Cur_Height; //document.getElementById('imgCont').height;
    //imgPrev
    
    var clipRect = "rect(" + curY + "px," + (curX + preview_Width) + "px," + (curY + preview_Height) + "px," + curX + "px)";
    document.getElementById('imgPrev').style.clip=clipRect; //"rect(0px,50px,50px,0px)";
    //object.style.clip="rect(0px,50px,50px,0px)"

    document.getElementById('imgPrev').style.left = Math.round(300 - curX); // Preview x and y position calculated dynamically
    document.getElementById('imgPrev').style.top = Math.round(25 - curY);
    
    document.getElementById('imgPrev').style.width = imgCont_Width / percent;
    document.getElementById('imgPrev').style.height = imgCont_Height / percent;
    
    
    updateValues();
}

function ExtractNumber(value)
{
    var n = parseInt(value,10); // ZD 101722
	
    return n == null || isNaN(n) ? 0 : n;
}

// this is simply a shortcut for the eyes and fingers
function $(id)
{
    return document.getElementById(id);
}


//function divIn(ob)
//{
//var obj = document.getElementById(ob);
//obj.parentNode.style.border='solid 1px gray';
//}

//function divOut(ob)
//{
//var obj = document.getElementById(ob);
//obj.parentNode.style.border='solid 1px lightgrey';
//}
//    
function updateValues()
{
//left.substring(0,left.length-2) * 1
var leftVal = document.getElementById('areaDiv').style.left;
var topVal = document.getElementById('areaDiv').style.top;
var widthVal = document.getElementById('areaDiv').style.width;
var heightVal = document.getElementById('areaDiv').style.height;

document.getElementById('Text1').value = leftVal.substring(0,leftVal.length-2) + "," + topVal.substring(0,topVal.length-2);
document.getElementById('Text2').value = widthVal.substring(0,widthVal.length-2) + "," + heightVal.substring(0,heightVal.length-2);
}

function checkFileType()
{
var filepath=document.getElementById('uploadImage').value;
var len = filepath.length;
var type=filepath.substring(len-4,len).toUpperCase();
    if(type==".JPG" || type=="JPEG" || type==".GIF" || type == ".PNG" || type == ".TIF" || type == "TIFF" || type == ".BMP")
    {
        document.getElementById('saveLink').click();
        return true;
    }
    else
    {
        alert(ValidImageFormat);
        return false;
    }
}

function updtScroll(left,top)
{
//alert(left);
divScrollLeft = left;
divScrollTop = top;
updtMovement();
}





/*function showDiv(w,h)
{

imgCont_Width = w*1;
imgCont_Height = h*1;

document.getElementById("imgPrev").style.width=imgCont_Width;
document.getElementById("imgPrev").style.height=imgCont_Height;

document.getElementById("areaDiv").style.width=90;
document.getElementById("areaDiv").style.height=120;

//document.getElementById('Text1').value = imgCont_Width;
//document.getElementById('Text2').value = imgCont_Height;


document.body.scrollTop = 0;

document.getElementById("maskDiv").style.display='block';

document.getElementById("maskDiv").style.width=screen.width;
document.getElementById("maskDiv").style.height=screen.height;

document.body.style.overflow = "hidden";



document.getElementById('xModDiv').style.left = Math.round((document.body.clientWidth-400)/2);
document.getElementById('xModDiv').style.top = 150;
//document.getElementById('xModDiv').style.top=Math.round((document.body.clientHeight-480)/2)-100;

divMoveLeft = Math.round((document.body.clientWidth-400)/2);
divMoveTop = 150;
//divMoveTop=Math.round((document.body.clientHeight-480)/2)-100;

document.getElementById('xModDiv').style.display='block';
$$('html').invoke('addClassName', 'overflow-hidden');

updateValues();
}

function hideDiv(divId)
{
document.getElementById(divId).style.display='none';
document.getElementById("maskDiv").style.display='none';

document.body.style.overflow = "auto";
$$('html').invoke('removeClassName', 'overflow-hidden');
}*/


    </script>

    <%--FB 2136 end--%>
</head>
<body>
    <form id="frmResponse" runat="server" autocomplete="off" method="post" onsubmit="return true"><%--ZD 101190--%>
    <%--ZD 101022 start--%>
        <asp:ScriptManager ID="scpMgrUI" runat="server" EnableScriptLocalization="true" >
		    <Scripts>                
			    <asp:ScriptReference Path= "~/ResourceScript/StringResources.js" ResourceUICultures="<%$ Resources:WebResources, UICulture%>"  />
		    </Scripts>
	    </asp:ScriptManager> <%--ZD 101022 End--%>
    <asp:HiddenField ID="txtUserID" runat="server" />
    <asp:HiddenField ID="txtConfID" runat="server" />
    <asp:HiddenField ID="txtDecision" runat="server" />
    <asp:HiddenField ID="hdnImgName" runat="server" />
    <%--FB 2136--%>
    <%--<asp:HiddenField ID="hdnIsEnabledSecBadge" runat="server" />--%>
    <%--FB 2136--%>
    <%--<asp:HiddenField ID="hdnObjAxis" runat="server" />--%>
    <%--FB 2136--%>
    <div>
        <table width="100%" id="tblMain">
            <tr>
                <td align="center">
                    <br />
                    <h3>
                        <asp:Label ID="lblHeader" runat="server" CssClass="lblMessage"></asp:Label><%--FB 2391--%>
                    </h3>
                </td>
            </tr>
            <tr>
                <td align="center">
                    <asp:Label ID="errLabel" runat="server" Text="" CssClass="lblError"></asp:Label>
                    <asp:CustomValidator ID="customAccept" OnServerValidate="ValidateAcceptRegistration"
                        Text="<%$ Resources:WebResources, ResponseConference_InvalidIPISDN%> " CssClass="lblError" runat="server" Display="dynamic"></asp:CustomValidator> 
                </td>
            </tr>
            <tr class="btprint" id="tblActions">
                <td align="left">
                    <table width="98%">
                        <tr>
                            <td width="100%" align="right">
                                <%--FB 1757 - Start--%>
                                <% if (hdnRecur.Value != "1") { %>
                                <asp:ImageButton ID="btnOutlook" ImageUrl="image/saveoutlook.bmp" Width="32" Visible="false" AlternateText="Save To Outlook"
                                    Height="32" runat="server" OnClientClick="javascript:saveToOutlookCalendar('0','0','1','');return false;"
                                    ToolTip="Save to Outlook"></asp:ImageButton><%--FB 2102--%><%--ZD 100419--%>
                                <% } 
                                else{ %>
                                <asp:ImageButton ID="btnOutlookR" ImageUrl="image/saveoutlook.bmp" Width="32" Visible="false" AlternateText="Save To Outlook"
                                    Height="32" runat="server" OnClientClick="javascript:saveToOutlookCalendar('0','1','2','');return false;"
                                    ToolTip="Save to Outlook"></asp:ImageButton><%--FB 2164--%><%--ZD 100419--%>
                                <% } %>
                                <a href="" onclick="this.childNodes[0].click();return false;">
                                <img border="0" src="image/print.gif" width="32" height="32" alt="Print this Page" style="cursor:pointer;" runat="server" title="<%$ Resources:WebResources, ManageConference_Print%>"
                                    onclick="JavaScript:window.print();" /></a><%-- FB 3034--%> <%--ZD 100429--%><%--100420--%>
                                <%--FB 1757 - End--%>
                                <asp:ImageButton ID="btnPDF" ImageUrl="image/adobe.gif" runat="server" OnClick="ExportToPDF" AlternateText="Export to PDF"
                                    OnClientClick="javascript:pdfReport();" ToolTip="<%$ Resources:WebResources, ManageConference_btnPDF%>"></asp:ImageButton> <%--ZD 100419--%><%--ZD 100429--%>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr id="trAcceptConfirm" runat="server" visible="false">
                <td align="center">
                    <table width="80%" bgcolor="lightgrey" cellpadding="2" cellspacing="5">
                        <tr>
                            <td align="center" colspan="2">
                                <h5>
                                    <asp:Literal Text="<%$ Resources:WebResources, ResponseConference_MCUInformation%>" runat="server"></asp:Literal> </h5>
                            </td>
                        </tr>
                        <tr>
                            <td align="left" style="font-weight: bold">
                                <asp:Literal Text="<%$ Resources:WebResources, ResponseConference_MCUName%>" runat="server"></asp:Literal> 
                            </td>
                            <td align="left">
                                <asp:Label ID="lblBridgeName" runat="server"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td align="left" style="font-weight: bold">
                                 <asp:Literal  Text="<%$ Resources:WebResources, ResponseConference_MCUAddress%>" runat="server"></asp:Literal>
                            </td>
                            <td align="left">
                                <asp:Label ID="lblBridgeAddress" runat="server"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td align="left" style="font-weight: bold">
                                <asp:Literal ID="Literal4"  Text="<%$ Resources:WebResources, ResponseConference_MCUAddressTyp%>" runat="server"></asp:Literal> 
                            </td>
                            <td align="left">
                                <asp:Label ID="lblBridgeAddressType" runat="server"></asp:Label>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td align="Left">
                    <table cellspacing="5">
                        <tr>
                            <td>
                                <span class="subtitleblueblodtext"><asp:Literal Text="<%$ Resources:WebResources, ResponseConference_ConferenceDeta%>" runat="server"></asp:Literal> </span>
                            </td>
                      </tr>
                   </table>
                </td>
            </tr>
            <tr>
                <td align="center">
                    <table width="90%"><%--FB 2508--%>
                        <tr>
                            <td align="left" style="font-weight: bold" class="blackblodtext" valign="top" width="120px">
                                <asp:Literal  Text="<%$ Resources:WebResources, ResponseConference_Title%>" runat="server"></asp:Literal> 
                            </td>
                            <td valign="top" style="width:1px"><b>:</b>&nbsp;</td>
                            <td align="left" valign="top" width="600px"><%--FB 2508--%>
                                <asp:Label ID="lblConfName" runat="server" Font-Bold="True" Font-Names="Verdana"
                                    Font-Size="Medium" ForeColor="Green"></asp:Label>
                            </td>
                            <td align="left" style="font-weight: bold" class="blackblodtext" valign="top" width="120px">
                                <asp:Literal   Text="<%$ Resources:WebResources, ResponseConference_UniqueID%>" runat="server"></asp:Literal>  
                            </td>
                            <td valign="top" style="width:1px"><b>:</b>&nbsp;</td>
                            <td align="left" valign="top">
                                <asp:Label ID="lblConfUniqueID" runat="server" Font-Bold="True" Font-Names="Verdana"
                                    Font-Size="x-Small" ForeColor="Red"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td align="left" style="font-weight: bold" class="blackblodtext">
                                <asp:Literal  Text="<%$ Resources:WebResources, SearchConferenceInputParameters_tdReservationDate%>" runat="server"></asp:Literal> 
                            </td>
                            <td style="width:1px"><b>:</b>&nbsp;</td>
                            <td align="left" style="font-weight: normal; font-size: x-small; color: black; font-family: Verdana;"
                                valign="top" width="300">
                                <asp:Label ID="lblConfDate" runat="server" Font-Bold="False" Font-Names="Verdana"
                                    Font-Size="Small"></asp:Label>
                                <asp:Label ID="lblConfTime" runat="server" Font-Bold="False" Font-Names="Verdana"
                                    Font-Size="Small"></asp:Label>
                                <asp:Label ID="lblTimezone" runat="server" Font-Names="Verdana" Font-Size="Small"></asp:Label>
                            </td>
                            <td align="left" style="font-weight: bold" class="blackblodtext">
                               <asp:Literal   Text="<%$ Resources:WebResources, ConferenceList_Duration%>" runat="server"></asp:Literal> 
                            </td>
                            <td style="width:1px"><b>:</b>&nbsp;</td>
                            <td align="left">
                                <asp:Label ID="lblConfDuration" runat="server" Font-Bold="False" Font-Names="Verdana"
                                    Font-Size="Small"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td id="tdpw" runat="server" align="left" style="font-weight: bold" class="blackblodtext"> <%--FB 2446--%>
                                <asp:Literal   Text="<%$ Resources:WebResources, SuperAdministrator_Password%>" runat="server"></asp:Literal> 
                            </td>
                            <td id="tdpwd" runat="server" style="width:1px"><b>:</b>&nbsp;</td>
                            <td id ="tdpw1" runat="server" align="left"><%--FB 2446--%>
                                <asp:Label ID="lblPassword" runat="server" Font-Names="Verdana" Font-Size="Small"></asp:Label>&nbsp;
                            </td>
                            <td align="left" style="font-weight: bold" class="blackblodtext">
                                <asp:Literal Text="<%$ Resources:WebResources, DashBoard_tdType%>" runat="server"></asp:Literal>
                            </td>
                            <td style="width:1px"><b>:</b>&nbsp;</td>
                            <td align="left">
                                <asp:Label ID="lblConfType" runat="server" Font-Bold="False" Font-Names="Verdana"
                                    Font-Size="Small"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td align="left" style="font-weight: bold" class="blackblodtext">
                                <asp:Literal  Text="<%$ Resources:WebResources, ConfirmTemplate_Location%>" runat="server"></asp:Literal> 
                            </td>
                            <td style="width:1px"><b>:</b>&nbsp;</td>
                            <td align="left" colspan="3">
                                <asp:Label ID="lblLocation" runat="server" Font-Bold="False" Font-Names="Verdana"
                                    Font-Size="Small"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td align="left" style="font-weight: bold" class="blackblodtext">
                               <asp:Literal  Text="<%$ Resources:WebResources, DashBoard_Files%>" runat="server"></asp:Literal>
                            </td>
                            <td style="width:1px"><b>:</b>&nbsp;</td>
                            <td align="left" colspan="3">
                                <asp:Label ID="lblFiles" runat="server" Font-Bold="False" Font-Names="Verdana" Font-Size="Small"></asp:Label>
                            </td>
                        </tr><%--FB 2508--%>                        
                    <%--</table>--%><%--ZD 101007--%>
                    <%--<table width="90%">--%><%--ZD 101007--%>
                        <tr>
                            <td align="left" style="font-weight: bold" class="blackblodtext" width="120px" valign="top">
                               <asp:Literal  Text="<%$ Resources:WebResources, Description%>" runat="server"></asp:Literal> 
                            </td>
                            <td style="width:1px" valign="top"><b>:</b>&nbsp;</td>
                            <td align="left" colspan="3" valign="top"><%--FB 2508--%>
                                <asp:Label ID="lblDescription" runat="server" Font-Bold="False" Font-Names="Verdana"
                                    Font-Size="Small"></asp:Label>&nbsp;
                            </td>
                        </tr>
                        </table><%--ZD 101007--%>
                </td>
            </tr>
            <tr id="trRecur" runat="server">
                <td align="center">
                    <table width="95%">
                        <tr>
                            <td align="center">
                                <asp:DataGrid ID="dgInstanceList" runat="server" AutoGenerateColumns="False" Font-Names="Verdana"
                                    Font-Size="Small" Width="100%" BorderColor="blue" BorderStyle="Solid" BorderWidth="0px">
                                    <AlternatingItemStyle BackColor="#ffcc66" />
                                    <HeaderStyle CssClass="tableHeader" Height="30" />
                                    <SelectedItemStyle BackColor="green" />
                                    <Columns>
                                        <asp:BoundColumn DataField="confID" Visible="false"></asp:BoundColumn>
                                        <asp:TemplateColumn ItemStyle-Width="80%" HeaderStyle-Width="80%">
                                            <HeaderTemplate>
                                                <table width="100%" border="0">
                                                    <tr class="tableHeader">
                                                        <%--FB 1982 - Start--%>
                                                        <td width="10%" align="left" class="tableHeader" class="blackblodtext">
                                                             <asp:Literal   Text="<%$ Resources:WebResources, ResponseConference_UniqueID%>" runat="server"></asp:Literal>   
                                                        </td>
                                                        <td width="15%" align="left" class="tableHeader" class="blackblodtext">
                                                            <asp:Literal  Text="<%$ Resources:WebResources, ViewWorkorderDetails_Name%>" runat="server"></asp:Literal>   
                                                        </td>
                                                        <%--ZD 101007 start--%>
                                                        <td width="10%" align="left" class="tableHeader" runat="server" visible='<%# (Session["EnableConfPassword"] != null && Session["EnableConfPassword"].ToString() == "1") %>'>
                                                            <asp:Literal   Text="<%$ Resources:WebResources, ConfirmTemplate_Password%>" runat="server"></asp:Literal> 
                                                        </td>
                                                        <%--ZD 101007 End--%>
                                                        <td width="22%" align="left" class="tableHeader" class="blackblodtext"> <%--ZD 101007--%>
                                                            <asp:Literal   Text="<%$ Resources:WebResources, ResponseConference_DateTime%>" runat="server"></asp:Literal>  
                                                        </td>
                                                        <td class="tableHeader" class="blackblodtext" align="left" style="width:45%"> <%--ZD 101007--%>
                                                            <asp:RadioButtonList CssClass="tableHeader" ID="rdDecideAll" runat="server" RepeatLayout="Flow"
                                                                RepeatDirection="Horizontal" OnSelectedIndexChanged="ChangeSelection" AutoPostBack="true">
                                                                <asp:ListItem Value="0" Text=" <%$ Resources:WebResources, ResponseConference_UndecidedAll%> " Selected="True"></asp:ListItem>
                                                                <%--Code added for FB 1694--%>
                                                                <asp:ListItem Value="1" Text=" <%$ Resources:WebResources, ResponseConference_AcceptAll%> "></asp:ListItem>
                                                                <%--<asp:ListItem Value="0" Text="Change All"></asp:ListItem>--%><%--FB 1193 --%><%--Code Added for FB 1694--%>
                                                                <asp:ListItem Value="3" Text="<%$ Resources:WebResources, ResponseConference_ChangeAll%>"></asp:ListItem>
                                                                <asp:ListItem Value="2" Text="<%$ Resources:WebResources, ResponseConference_RejectAll%> "></asp:ListItem>
                                                                <%--FB 1193 --%>
                                                            </asp:RadioButtonList>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <table width="100%" border="0">
                                                    <tr>
                                                        <td width="10%" align="left">
                                                            <asp:Label ID="lblUniqueID" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.uniqueID") %>'></asp:Label>
                                                        </td>
                                                        <td width="15%" align="left">
                                                            <asp:Label ID="lblConfName" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.confName") %>'></asp:Label>
                                                        </td>
                                                        <%--ZD 101007 start--%>
                                                        <td width="10%"  align="left" runat="server"  visible='<%# (Session["EnableConfPassword"] != null && Session["EnableConfPassword"].ToString() == "1") %>'>
                                                            <asp:Label ID="lblConfPassword" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.password") %>'></asp:Label>
                                                        </td>
                                                        <%--ZD 101007 End--%>
                                                        <td width="20%" align="left"><%--ZD 101007 --%>
                                                            <asp:Label ID="lblDateTime" runat="server" Text='<%# DataBinder.Eval(Container,"DataItem.confDate") %>'></asp:Label>
                                                        </td>
                                                        <td align="left" style="width:45%"><%--ZD 101007 --%>
                                                            <asp:RadioButtonList ID="rdAction" AutoPostBack="true" OnSelectedIndexChanged="ChangeAction" CssClass="RadioButtonWidth"
                                                                runat="server" RepeatLayout="Flow" RepeatDirection="Horizontal"><%--ZD 101007 start--%>
                                                                <asp:ListItem Value="0"  Selected="True" Text="<%$ Resources:WebResources, ConferenceList_Undecided%>"> &nbsp;&nbsp;&nbsp;&nbsp;</asp:ListItem>
                                                                <%--Code added for FB 1694--%>
                                                                <asp:ListItem Value="1"  Text="<%$ Resources:WebResources, LicenseAgreement_BtnSubmit%>" >  &nbsp;&nbsp;&nbsp;&nbsp;</asp:ListItem>
                                                                <%--<asp:ListItem Value="0" Text="Change"></asp:ListItem>--%><%--FB 1167 --%><%--Code added for FB 1694--%>
                                                                <asp:ListItem Value="3" Text ="<%$ Resources:WebResources, ManageConference_btnChangeEndpointLayout%>"> &nbsp;&nbsp;&nbsp;&nbsp;</asp:ListItem>
                                                                <asp:ListItem Value="2" Text="<%$ Resources:WebResources, ResponseConference_Reject%>" >&nbsp;&nbsp;&nbsp;&nbsp;</asp:ListItem> <%--ZD 101007 End--%>
                                                                <%--FB 1167 --%>
                                                            </asp:RadioButtonList>
                                                        </td>
                                                        <%--FB 1982 - End--%>
                                                    </tr><%--FB 2020--%>
                                                    <tr id="trInstanceChange" runat="server" visible="false">
                                                        <td align="center" colspan="4">
                                                            <table width="100%" style="margin-left:20px">
                                                                <tr>
                                                                    <td colspan="2" align="left" style="font-weight: bold" class="blackblodtext">
                                                                       <asp:Literal  Text="<%$ Resources:WebResources, ResponseConference_Pleasetellus%>" runat="server"></asp:Literal>  
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td align="left" width="10%" style="font-weight: bold" class="blackblodtext">
                                                                        <asp:Literal Text="<%$ Resources:WebResources, ResponseConference_DateTime%>" runat="server"></asp:Literal>  
                                                                    </td>
                                                                    <td align="left" class="blackblodtext">
                                                                        <asp:TextBox ID="txtStartDate" runat="server" CssClass="altText"></asp:TextBox>
                                                                        <a href="" onclick="this.childNodes[0].click();return false;"><img src="image/calendar.gif" border="0" width="20" height="20" id="cal_triggerd" runat="server" alt="Date Selector"
                                                                            style="cursor: pointer; vertical-align: bottom;" title="<%$ Resources:WebResources, Dateselector%>" onclick="return showCalendar('txtStartDate', 'cal_triggerd', 1, '%m/%d/%Y');" /></a><%--code added for 1693--%><%--ZD 100419--%><%--100420--%>
                                                                        <span style="vertical-align: middle">@</span>
                                                                        <mbcbb:combobox id="txtStartTime" runat="server" BackColor="White" CssClass="altSelectFormat" Rows="10" CausesValidation="True" style="width: auto"><%--code added for 1693--%>
                                                                            <asp:ListItem Value="01:00 AM" Selected="True">
                                                                            </asp:ListItem>
                                                                            <asp:ListItem Value="02:00 AM">
                                                                            </asp:ListItem>
                                                                            <asp:ListItem Value="03:00 AM">
                                                                            </asp:ListItem>
                                                                            <asp:ListItem Value="04:00 AM">
                                                                            </asp:ListItem>
                                                                            <asp:ListItem Value="05:00 AM">
                                                                            </asp:ListItem>
                                                                            <asp:ListItem Value="06:00 AM">
                                                                            </asp:ListItem>
                                                                            <asp:ListItem Value="07:00 AM">
                                                                            </asp:ListItem>
                                                                            <asp:ListItem Value="08:00 AM">
                                                                            </asp:ListItem>
                                                                            <asp:ListItem Value="09:00 AM">
                                                                            </asp:ListItem>
                                                                            <asp:ListItem Value="10:00 AM">
                                                                            </asp:ListItem>
                                                                            <asp:ListItem Value="11:00 AM">
                                                                            </asp:ListItem>
                                                                            <asp:ListItem Value="12:00 PM">
                                                                            </asp:ListItem>
                                                                            <asp:ListItem Value="01:00 PM">
                                                                            </asp:ListItem>
                                                                            <asp:ListItem Value="02:00 PM">
                                                                            </asp:ListItem>
                                                                            <asp:ListItem Value="03:00 PM">
                                                                            </asp:ListItem>
                                                                            <asp:ListItem Value="04:00 PM">
                                                                            </asp:ListItem>
                                                                            <asp:ListItem Value="05:00 PM">
                                                                            </asp:ListItem>
                                                                            <asp:ListItem Value="06:00 PM">
                                                                            </asp:ListItem>
                                                                            <asp:ListItem Value="07:00 PM">
                                                                            </asp:ListItem>
                                                                            <asp:ListItem Value="08:00 PM">
                                                                            </asp:ListItem>
                                                                            <asp:ListItem Value="09:00 PM">
                                                                            </asp:ListItem>
                                                                            <asp:ListItem Value="10:00 PM">
                                                                            </asp:ListItem>
                                                                            <asp:ListItem Value="11:00 PM">
                                                                            </asp:ListItem>
                                                                            <asp:ListItem Value="12:00 AM">
                                                                            </asp:ListItem>
                                                                        </mbcbb:combobox>
                                                                        &nbsp;&nbsp;Timezone
                                                                        <asp:DropDownList ID="lstTimeZone" runat="server" CssClass="altLong0SelectFormat"
                                                                            DataTextField="timezoneName" DataValueField="timezoneID">
                                                                        </asp:DropDownList>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                    </td>
                                                                    <td align="left" class="blackblodtext">
                                                                        <asp:RequiredFieldValidator ValidationGroup="ChangeConfirm" ID="reqStartTime" runat="server"
                                                                            ControlToValidate="txtStartTime" Display="Dynamic" ErrorMessage="<%$ Resources:WebResources, Required_Time%>"></asp:RequiredFieldValidator>
                                                                        <asp:RegularExpressionValidator ValidationGroup="ChangeConfirm" ID="regStartTime"
                                                                            runat="server" ControlToValidate="txtStartTime" Display="Dynamic" ErrorMessage="<%$ Resources:WebResources, ResponseConference_InvalidTime%> (HH:MM AM/PM)"
                                                                            ValidationExpression="[0-1][0-9]:[0-5][0-9] [A|a|P|p][M|m]"></asp:RegularExpressionValidator>
                                                                        <asp:RequiredFieldValidator ValidationGroup="ChangeConfirm" ID="reqStartData" runat="server"
                                                                            ControlToValidate="txtStartDate" Display="Dynamic" ErrorMessage="<%$ Resources:WebResources, Required_Date%>"></asp:RequiredFieldValidator>
                                                                        <asp:RegularExpressionValidator ValidationGroup="ChangeConfirm" ID="regStartDate"
                                                                            runat="server" ControlToValidate="txtStartDate" Display="Dynamic" ErrorMessage="<%$ Resources:WebResources, ResponseConference_InvalidDate%>(mm/dd/yyyy)"
                                                                            ValidationExpression="\b(0?[1-9]|1[012])[- /.](0?[1-9]|[12][0-9]|3[01])[- /.](19|20)?[0-9]{2}\b"></asp:RegularExpressionValidator>
                                                                        <asp:RequiredFieldValidator ValidationGroup="ChangeConfirm" ID="reqTimezone" runat="server"
                                                                            ControlToValidate="lstTimezone" InitialValue="-1" Display="dynamic" ErrorMessage="<%$ Resources:WebResources, ResponseConference_Timezonereq%>"></asp:RequiredFieldValidator>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td align="left" style="font-weight: bold" class="blackblodtext">
                                                                        <asp:Literal Text="<%$ Resources:WebResources, ConferenceList_Duration%>" runat="server"></asp:Literal><span class="reqfldstarText">*</span>
                                                                    </td>
                                                                    <td align="left" class="blackblodtext">
                                                                        <mbcbb:combobox id="lstDuration" runat="server" BackColor="White" CssClass="altSelectFormat"
                                                                            Rows="10" CausesValidation="True" style="width: auto">
                                                                            <%--code added for 1693--%>
                                                                            <asp:ListItem Value="-1">Please select...</asp:ListItem>
                                                                            <asp:ListItem Value="01:00" Selected="True">01:00</asp:ListItem>
                                                                            <asp:ListItem Value="02:00">02:00</asp:ListItem>
                                                                            <asp:ListItem Value="03:00">03:00</asp:ListItem>
                                                                            <asp:ListItem Value="04:00">04:00</asp:ListItem>
                                                                            <asp:ListItem Value="05:00">05:00</asp:ListItem>
                                                                            <asp:ListItem Value="06:00">06:00</asp:ListItem>
                                                                            <asp:ListItem Value="07:00">07:00</asp:ListItem>
                                                                            <asp:ListItem Value="08:00">08:00</asp:ListItem>
                                                                            <asp:ListItem Value="09:00">09:00</asp:ListItem>
                                                                            <asp:ListItem Value="10:00">10:00</asp:ListItem>
                                                                            <asp:ListItem Value="11:00">11:00</asp:ListItem>
                                                                            <asp:ListItem Value="12:00">12:00</asp:ListItem>
                                                                        </mbcbb:combobox>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;(hh:mm)
                                                                        <asp:RequiredFieldValidator ValidationGroup="ChangeConfirm" InitialValue="-1" ID="reqDuration"
                                                                            runat="server" ControlToValidate="lstDuration" Display="Dynamic" ErrorMessage="<%$ Resources:WebResources, ResponseConference_Durationisreq%>"></asp:RequiredFieldValidator>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td align="left" style="font-weight: bold" class="blackblodtext">
                                                                        <asp:Literal Text="<%$ Resources:WebResources, ResponseConference_Comments%>" runat="server"></asp:Literal> 
                                                                    </td>
                                                                    <td align="left">
                                                                        <asp:TextBox TextMode="MultiLine" Rows="4" CssClass="altText" runat="server" ID="txtComments"
                                                                            Width="25%"></asp:TextBox><%--code added for 1693--%>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                    </Columns>
                                    <ItemStyle Height="20px" />
                                </asp:DataGrid>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr id="trButtonsHeader" runat="server" class="btprint">
                <td align="Left">
                    <table cellspacing="5">
                        <tr>
                            <td>
                                <span class="subtitleblueblodtext"><asp:Literal  Text="<%$ Resources:WebResources, ResponseConference_ChooseanActio%>" runat="server"></asp:Literal> </span>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr runat="server" id="trButtons" class="btprint">
                <td align="center">
                    <table width="90%">
                        <tr>
                            <%--ZD 100420 START- TAB INDEX--%>
                            <td align="center" width="30%">
                               <button id="btnRequestAccept" class="altLongBlueButtonFormat" OnServerClick="AcceptConference" runat="server"> <asp:Literal  Text="<%$ Resources:WebResources, ResponseConference_btnRequestAccept%>" runat="server"></asp:Literal> </button>
                            </td>
                            <td align="center" width="30%">
                                <button id="btnRequestChange" class="altLongBlueButtonFormat" OnServerClick="RequestChange" runat="server"> <asp:Literal Text="<%$ Resources:WebResources,ResponseConference_btnRequestChange%>" runat="server"></asp:Literal></button>
                            </td>
                            <td align="center" width="30%">
                                <button id="btnRequestReject" class="altLongBlueButtonFormat" OnServerClick="RejectConference" runat="server"> <asp:Literal  Text="<%$ Resources:WebResources,ResponseConference_btnRequestReject%>" runat="server"></asp:Literal></button>
                            </td>
                            <%--ZD 100420 END- TAB INDEX--%>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr id="trAccept" runat="server" visible="false">
                <td align="center">
                    <table width="100%">
                        <tr>
                            <td align="center">
                                <h3>
                                    <asp:Label ID="lblAccept" Text="<%$ Resources:WebResources,ResponseConference_lblAccept%> " runat="server"></asp:Label></h3>
                                <table width="95%">
                                    <tr width="100%" id="trAcceptMessage" runat="server">
                                        <td align="left" class="blackblodtext">
                                            <asp:Literal Text="<%$ Resources:WebResources,ResponseConference_Thankyoufora%>" runat="server"></asp:Literal> 
                                            <asp:Label ID="lblUserName" runat="server"></asp:Label>.
                                        </td>
                                    </tr>
                                    <tr width="100%" id="trRgprocess" runat="server"> <%-- FB 2459 --%>
                                        <td align="left" class="blackblodtext">
                                            <asp:Literal Text="<%$ Resources:WebResources,ResponseConference_Pleasesupplyt%>" runat="server"></asp:Literal>  
                                        </td>
                                    </tr>
                                    <tr id="trPkroom" runat="server">  <%-- FB 2459 --%>
                                        <td align="left" class="blackblodtext">
                                            <asp:Literal Text="<%$ Resources:WebResources,ResponseConference_Pleasepickar%>" runat="server"></asp:Literal>   
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left">
                                            <asp:RadioButtonList ID="rdRooms" runat="server" DataTextField="locationName" DataValueField="locationID"
                                                TextAlign="Right" RepeatDirection="Vertical" OnSelectedIndexChanged="ChangeRoom"
                                                AutoPostBack="true">
                                            </asp:RadioButtonList>
                                        </td>
                                    </tr>
                                    <%-- FB 2136 Start --%>
                                    <%--FB 3055-Filter in Upload Files --%>
                                    <%--<tr visible="true" runat="server" id="secBadgeTr1">
                                        <td align="left">
                                            <b> <asp:Literal Text="<%$ Resources:WebResources,ResponseConference_UploadPhotofo %>" runat="server"></asp:Literal> </b>
                                            <asp:Label ID="hdnImageSize" CssClass="blackItalictext" Text=""
                                                runat="server"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr visible="true" runat="server" id="secBadgeTr2">
                                        <td align="left">
                                            <asp:FileUpload Visible="true" accept="image/*" ID="uploadImage" runat="server" class="altText" onChange="javascript:return checkFileType()" />
                                            <cc1:ImageControl ID="badgeIcon1" Width="30" Height="40" Visible="false" runat="server">
                                            </cc1:ImageControl>
                                            <asp:Button ID="btnRemoveImg1" runat="server" Visible="false" OnCommand="RemoveFile"
                                                CommandArgument="3" Text="<%$ Resources:WebResources,ResponseConference_btnRemoveImg1 %>" CssClass="altShortBlueButtonFormat" />
                                        </td>
                                    </tr>--%>
                                    <%-- FB 2136 End --%>
                                    <tr visible="false" runat="server" id="trConnection">
                                        <td>
                                            <asp:Literal Text="<%$ Resources:WebResources,ResponseConference_Pleaseselecty %>" runat="server"></asp:Literal> 
                                        </td>
                                    </tr>
                                    <tr visible="false" runat="server" id="trConnection1">
                                        <td>
                                            <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                                <tr class="tableHeader">
                                                    <td class="tableHeader">
                                                       <asp:Literal Text="<%$ Resources:WebResources,ResponseConference_ConnectionType %>" runat="server"></asp:Literal>  
                                                    </td>
                                                    <td class="tableHeader">
                                                        <asp:Literal Text="<%$ Resources:WebResources,ResponseConference_OutsideNetwork %>" runat="server"></asp:Literal> 
                                                    </td>
                                                    <td class="tableHeader">
                                                        <asp:Literal Text="<%$ Resources:WebResources,ResponseConference_InterfaceType %>" runat="server"></asp:Literal> 
                                                    </td>
                                                    <td class="tableHeader">
                                                       <asp:Literal Text="<%$ Resources:WebResources,ResponseConference_IPISDNAddress %>" runat="server"></asp:Literal>  
                                                    </td>
                                                    <td class="tableHeader">
                                                        <asp:Literal  Text="<%$ Resources:WebResources,ResponseConference_AddressType %>" runat="server"></asp:Literal> 
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <asp:DropDownList CssClass="altLong0SelectFormat" ID="lstConnectionType" DataTextField="Name"
                                                            DataValueField="ID" runat="server">
                                                            <asp:ListItem Selected="true" Value="1" Text="<%$ Resources:WebResources,ResponseConference_DialintoMCU %> "></asp:ListItem>
                                                            <asp:ListItem Value="0" Text="<%$ Resources:WebResources,ResponseConference_DialoutfromMCU %>" ></asp:ListItem>
                                                        </asp:DropDownList>
                                                        <%--FB 1365 --%>
                                                        <asp:RequiredFieldValidator ID="reqConnectionType" ControlToValidate="lstConnectionType"
                                                            ValidationGroup="Confirm" InitialValue="-1" ErrorMessage="<%$ Resources:WebResources,Required %> " runat="server"
                                                            Display="Dynamic"></asp:RequiredFieldValidator> <%--ZD 101244--%>
                                                    </td>
                                                    <td>
                                                        <asp:CheckBox ID="chkOutsideNetwork" runat="server" />
                                                    </td>
                                                    <td>
                                                        <asp:DropDownList CssClass="altLong0SelectFormat" ID="lstInterfaceType" runat="server">
                                                            <asp:ListItem Value="IP" Text="IP" Selected="true"></asp:ListItem>
                                                            <asp:ListItem Value="ISDN" Text="ISDN"></asp:ListItem>
                                                        </asp:DropDownList>
                                                    </td>
                                                    <td>
                                                       <asp:TextBox runat="server" ID="txtAddress" CssClass="altText"></asp:TextBox>
                                                       <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server"  ControlToValidate="txtAddress" ErrorMessage="<%$ Resources:WebResources,Required %>" ValidationGroup="Confirm" Display="Dynamic"></asp:RequiredFieldValidator> <%--FB 3012--%>
                                                    </td>
                                                    <td>
                                                        <asp:DropDownList CssClass="altLong0SelectFormat" ID="lstAddressType" runat="server"
                                                            DataTextField="Name" DataValueField="ID">
                                                        </asp:DropDownList>
                                                        <%--                                                        <asp:RequiredFieldValidator ID="reqAddressType" ControlToValidate="lstAddressType" ValidationGroup="Confirm" InitialValue="-1" ErrorMessage="<br>Required" runat="server" Display="Dynamic"></asp:RequiredFieldValidator>
--%>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr id="trSubmit" class="btprint">
                                        <td colspan="5">
                                            <br />
                                                <button ID="btnConfirmAccept"  type="submit" OnServerClick="ConfirmAccept" class="altLongBlueButtonFormat"
                                                ValidationGroup="Confirm" runat="server"><asp:Literal Text="<%$ Resources:WebResources,ResponseConference_saveLink %>" runat="server"></asp:Literal>  </button><%--ZD 100420--%> <%--ZD 104786 --%>
                                            <%--<asp:CustomValidator id="customvalidation" runat="server" display="dynamic" text="Invalid IP format." OnServerValidate="ValidateIP" />           --%>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr id="trChange" runat="server" visible="false">
                <td align="center">
                    <table width="100%" style="margin-left:20px">
                        <tr>
                            <td colspan="2" align="left" style="font-weight: bold" class="blackblodtext">
                                <asp:Literal  Text="<%$ Resources:WebResources,ResponseConference_Pleasetellus %>" runat="server"></asp:Literal> 
                            </td>
                        </tr>
                        <tr>
                            <td align="left" width="10%" style="font-weight: bold" class="blackblodtext">
                                 <asp:Literal Text="<%$ Resources:WebResources,ResponseConference_DateTime %>" runat="server"></asp:Literal> 
                            </td>
                            <td align="left" class="blackblodtext">
                                <asp:TextBox ID="txtStartDate" runat="server" CssClass="altText"></asp:TextBox>
                                <a href="" onclick="this.childNodes[0].click();return false;"><img src="image/calendar.gif" border="0" width="20" height="20" id="cal_triggerd" alt="Date Selector"
                                    style="cursor: pointer; vertical-align: bottom;" title="<%$ Resources:WebResources, Dateselector%>" runat="server" onclick="return showCalendar('txtStartDate', 'cal_triggerd', 1, '%m/%d/%Y');" /></a><%--code added for 1693--%><%--ZD 100420--%>
                                <span style="vertical-align: middle">@</span><mbcbb:combobox id="txtStartTime" runat="server"
                                    BackColor="White" CssClass="altSelectFormat" Rows="10" CausesValidation="True"
                                    style="width: auto"><%--code added for 1693--%>
                                    <asp:ListItem Value="01:00 AM" Selected="True">
                                    </asp:ListItem>
                                    <asp:ListItem Value="02:00 AM">
                                    </asp:ListItem>
                                    <asp:ListItem Value="03:00 AM">
                                    </asp:ListItem>
                                    <asp:ListItem Value="04:00 AM">
                                    </asp:ListItem>
                                    <asp:ListItem Value="05:00 AM">
                                    </asp:ListItem>
                                    <asp:ListItem Value="06:00 AM">
                                    </asp:ListItem>
                                    <asp:ListItem Value="07:00 AM">
                                    </asp:ListItem>
                                    <asp:ListItem Value="08:00 AM">
                                    </asp:ListItem>
                                    <asp:ListItem Value="09:00 AM">
                                    </asp:ListItem>
                                    <asp:ListItem Value="10:00 AM">
                                    </asp:ListItem>
                                    <asp:ListItem Value="11:00 AM">
                                    </asp:ListItem>
                                    <asp:ListItem Value="12:00 PM">
                                    </asp:ListItem>
                                    <asp:ListItem Value="01:00 PM">
                                    </asp:ListItem>
                                    <asp:ListItem Value="02:00 PM">
                                    </asp:ListItem>
                                    <asp:ListItem Value="03:00 PM">
                                    </asp:ListItem>
                                    <asp:ListItem Value="04:00 PM">
                                    </asp:ListItem>
                                    <asp:ListItem Value="05:00 PM">
                                    </asp:ListItem>
                                    <asp:ListItem Value="06:00 PM">
                                    </asp:ListItem>
                                    <asp:ListItem Value="07:00 PM">
                                    </asp:ListItem>
                                    <asp:ListItem Value="08:00 PM">
                                    </asp:ListItem>
                                    <asp:ListItem Value="09:00 PM">
                                    </asp:ListItem>
                                    <asp:ListItem Value="10:00 PM">
                                    </asp:ListItem>
                                    <asp:ListItem Value="11:00 PM">
                                    </asp:ListItem>
                                    <asp:ListItem Value="12:00 AM">
                                    </asp:ListItem>
                                </mbcbb:combobox>
                                &nbsp;&nbsp;<asp:Literal Text="<%$ Resources:WebResources,ResponseConference_Timezone %>" runat="server"></asp:Literal> 
                                <asp:DropDownList ID="lstTimeZone" runat="server" CssClass="altLong0SelectFormat"
                                    DataTextField="timezoneName" DataValueField="timezoneID">
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td>
                            </td>
                            <td align="left" class="blackblodtext">
                                <asp:RequiredFieldValidator ValidationGroup="ChangeConfirm" ID="reqStartTime" runat="server"
                                    ControlToValidate="txtStartTime" Display="Dynamic" ErrorMessage="<%$ Resources:WebResources, Required_Time%>"></asp:RequiredFieldValidator>
                                <asp:RegularExpressionValidator ValidationGroup="ChangeConfirm" ID="regStartTime"
                                    runat="server" ControlToValidate="txtStartTime" Display="Dynamic" ErrorMessage="<%$ Resources:WebResources,ResponseConference_InvalidTime %> (HH:MM AM/PM)"
                                    ValidationExpression="[0-1][0-9]:[0-5][0-9] [A|a|P|p][M|m]"></asp:RegularExpressionValidator>
                                <asp:RequiredFieldValidator ValidationGroup="ChangeConfirm" ID="reqStartData" runat="server"
                                    ControlToValidate="txtStartDate" Display="Dynamic" ErrorMessage="<%$ Resources:WebResources, Required_Date%>"></asp:RequiredFieldValidator>
                                <asp:RegularExpressionValidator ValidationGroup="ChangeConfirm" ID="regStartDate"
                                    runat="server" ControlToValidate="txtStartDate" Display="Dynamic" ErrorMessage="<%$ Resources:WebResources,ResponseConference_InvalidDate %> (mm/dd/yyyy)"
                                    ValidationExpression="\b(0?[1-9]|1[012])[- /.](0?[1-9]|[12][0-9]|3[01])[- /.](19|20)?[0-9]{2}\b"></asp:RegularExpressionValidator>
                                <asp:RequiredFieldValidator ValidationGroup="ChangeConfirm" ID="reqTimezone" runat="server"
                                    ControlToValidate="lstTimezone" InitialValue="-1" Display="dynamic" ErrorMessage="<%$ Resources:WebResources,ResponseConference_Timezonereq %>"></asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr>
                            <td align="left" style="font-weight: bold" class="blackblodtext">
                                <asp:Literal Text="<%$ Resources:WebResources,ResponseConference_Duration %>" runat="server"></asp:Literal> <span class="reqfldstarText">*</span>
                            </td>
                            <td align="left" class="blackblodtext">
                                <mbcbb:combobox id="lstDuration" runat="server" BackColor="White" CssClass="altSelectFormat"
                                    Rows="10" CausesValidation="True" style="width: auto">
                                    <%--code added for 1693--%>
                                    <asp:ListItem Value="-1">Please select</asp:ListItem>
                                    <asp:ListItem Value="01:00" Selected="True">01:00</asp:ListItem>
                                    <asp:ListItem Value="02:00">02:00</asp:ListItem>
                                    <asp:ListItem Value="03:00">03:00</asp:ListItem>
                                    <asp:ListItem Value="04:00">04:00</asp:ListItem>
                                    <asp:ListItem Value="05:00">05:00</asp:ListItem>
                                    <asp:ListItem Value="06:00">06:00</asp:ListItem>
                                    <asp:ListItem Value="07:00">07:00</asp:ListItem>
                                    <asp:ListItem Value="08:00">08:00</asp:ListItem>
                                    <asp:ListItem Value="09:00">09:00</asp:ListItem>
                                    <asp:ListItem Value="10:00">10:00</asp:ListItem>
                                    <asp:ListItem Value="11:00">11:00</asp:ListItem>
                                    <asp:ListItem Value="12:00">12:00</asp:ListItem>
                                </mbcbb:combobox>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;(hh:mm)
                                <asp:RequiredFieldValidator ValidationGroup="ChangeConfirm" InitialValue="-1" ID="reqDuration"
                                    runat="server" ControlToValidate="lstDuration" Display="Dynamic" ErrorMessage="<%$ Resources:WebResources, ResponseConference_Durationisreq%>"></asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr>
                            <td align="left" style="font-weight: bold" class="blackblodtext">
                                <asp:Literal Text="<%$ Resources:WebResources,ResponseConference_Comments %>" runat="server"></asp:Literal> 
                            </td>
                            <td align="left">
                                <asp:TextBox TextMode="MultiLine" Rows="4" CssClass="altText" runat="server" ID="txtComments"
                                    Width="25%"></asp:TextBox><%--code added for 1693--%>
                            </td>
                        </tr>
                        <tr class="btprint">
                            <td colspan="5" class="blackblodtext">
                                <br />
                                <button id="btnConfirmChange" ValidationGroup="ChangeConfirm" OnServerClick="ConfirmAccept"
                                    class="altLongBlueButtonFormat" runat="server"> <asp:Literal Text="<%$ Resources:WebResources,ResponseConference_saveLink %>" runat="server"></asp:Literal> </button><%--ZD 100420--%>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr id="trRecurButton" runat="server" visible="false" class="btprint">
                <%--FB 1757--%>
                <td align="center">
                    <table cellspacing="5">
                        <tr>
                            <td>
                                <asp:Button ID="btnRecurSubmit" CssClass="altLongBlueButtonFormat" Text="<%$ Resources:WebResources,ResponseConference_saveLink %>"
                                    OnClick="SubmitInvitation" runat="server" />
                                <asp:CustomValidator ID="CustomValidator1" OnServerValidate="ValidateAcceptRegistration"
                                    runat="server" Display="dynamic"></asp:CustomValidator>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr id="trFooter1">
                <td>
                    <br />
                    <br />
                    <br />
                    <hr />
                </td>
            </tr>
            <tr id="trFooter">
                <td align="left" style="height: 52px">
                    <table border="0" cellpadding="2" cellspacing="2" class="btprint" width="100%">
                        <tr valign="bottom">
                            <td>
                                <span style="font-size: 7pt; color: #0000ff"><span class="srcstext2"> <asp:Literal  Text="<%$ Resources:WebResources, ResponseConference_TechSupportCo%>" runat="server"></asp:Literal> 
                                    :
                                    <asp:Label ID="lblContactName" runat="server" Font-Names="Verdana" Font-Size="Small"></asp:Label></span><span
                                        class="contacttext" style="color: #0000ff"></span></span>
                            </td>
                            <td style="font-size: 7pt; color: #0000ff" width="10">
                            </td>
                            <td style="font-size: 7pt; color: #0000ff">
                                <span class="srcstext2"> <asp:Literal ID="Literal2" Text="<%$ Resources:WebResources, ResponseConference_TechSupportEm %>" runat="server"></asp:Literal>
                                    <asp:Label ID="lblContactEmail" runat="server"></asp:Label></span>
                            </td>
                            <td style="font-weight: bold" width="10">
                            </td>
                            <td style="font-size: 7pt; color: #0000ff">
                                <span style="font-size: 7pt"><span class="srcstext2"> <asp:Literal  Text="<%$ Resources:WebResources, ResponseConference_TechSupportPh %>" runat="server"></asp:Literal>
                                    <asp:Label ID="lblContactPhone" runat="server"></asp:Label></span></span>
                            </td>
                        </tr>
                        <tr style="font-size: 7pt; color: #0000ff">
                            <td align="center" colspan="5">
                                <span class="srcstext2">myVRM Version
                                    <%=Application["Version"].ToString()%>,(c)Copyright
                                    <%=Application["CopyrightsDur"].ToString()%>
                                    <a href="http://www.myvrm.com" target="_blank"><strong>myVRM.com</strong></a>. All
                                    Rights Reserved.</span>
                            </td>
                            <%--FB 1648--%>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>

        <script type="text/javascript">
                if (document.getElementById("regStartTime") != null)
                {
                    document.getElementById("regStartTime").controltovalidate = "txtStartTime_Text";
                    document.getElementById("reqDuration").controltovalidate = "lstDuration_Text";
                }
        </script>

        <input type="hidden" id="tempText" name="tempText" runat="server" />
        <input runat="server" id="hdnRecur" type="hidden" /><%--FB 1757--%>
    </div>
    <%--FB 2136 start--%>
    <%--ZD 100419--%>
    <%--<div id="maskDiv" class="mask">
    </div>
    <div id="hiddenDiv" style="display: none">
        <asp:Button ID="saveLink" runat="server" Text="<%$ Resources:WebResources,ResponseConference_saveLink %>" OnClick="uploadImage_Click" />
        <input id="selectedFile" type="text" runat="server" />
        <input id="Text1" runat="server" type="text" />
        <input id="Text2" runat="server" type="text" />
    </div>
    <div id="xModDiv" style="z-index: 50; left: 0px; top: 0px; position: absolute; display: none">
        <div>
            <table style="border-collapse: collapse" border="0">
                <tr>
                    <td>
                        <div class="imgHolder" onmouseover="javascript:return updtScroll(this.scrollLeft,this.scrollTop)"
                            onmouseout="javascript:return updtScroll(this.scrollLeft,this.scrollTop)">
                            <asp:Image ID="imgCont" runat="server" Visible="false" AlternateText="ImageCont" />
                            <div class="innerDiv" style="padding-right: -100px">
                                <br />
                                <br />
                                <table border="0">
                                    <tr>
                                        <td>
                                            <br />
                                            <br />
                                            <asp:Button ID="btnOk" runat="server" Style="margin-left: 45px" Text="<%$ Resources:WebResources,ResponseConference_saveLink %>" CssClass="altShortBlueButtonFormat"
                                                OnClick="saveImage_Click" />
                                        </td>
                                        <td>
                                            <br />
                                            <br />
                                            <input style="margin-left: 45px" id="Button4" type="button" value="<asp:Literal ID="Literal3" runat="server" Text="<%$ Resources:WebResources, ViewBlockedMails_btnCancel%>" />" class="altShortBlueButtonFormat"
                                                onclick="javascript:hideDiv('xModDiv');" />
                                            <asp:Label ID="lblhost" Visible="false" runat="server" Font-Names="Verdana" Font-Size="Small"></asp:Label>&nbsp;
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </td>
                    <td>
                        <div id="prevDiv" style="overflow: hidden">
                            <asp:Image ID="imgPrev" class="previewImg" Style="width: 0px; height: 0px" runat="server" AlternateText="Previous"
                                Visible="false" />
                        </div>
                    </td>
                </tr>
            </table>
            <div id="areaDiv" class="area" style="left: 1px; top: 1px; width: 90; height: 120">
                <div id="cornerDiv" class="corner">
                </div>
            </div>
        </div>
        <div style="margin-left: 320px; margin-top: -315px" class="subtitleblueblodtext">
            <asp:Literal ID="Literal1"  Text="<%$ Resources:WebResources, ResponseConference_Preview %>" runat="server"></asp:Literal></div>
    </div>--%>
    <%--FB 2136 end--%>
    </form>
</body>
</html>
<%--code added for Soft Edge button--%>

<%--FB 2487 - Start--%>
<script type ="text/javascript" >

    if (document.getElementById("errLabel") != null)
        var obj = document.getElementById("errLabel");
    
    if (obj != null) {

        var strInput = obj.innerHTML.toUpperCase();

        if (((strInput.indexOf("SUCCESS") > -1) && !(strInput.indexOf("UNSUCCESS") > -1) && !(strInput.indexOf("ERROR") > -1))
            || ((strInput.indexOf("EXITOSA") > -1) && !(strInput.indexOf("FALLIDA") > -1) && !(strInput.indexOf("ERROR") > -1))
            || ((strInput.indexOf("SUCC�S") > -1 || strInput.indexOf("�TABLIE") > -1) && !(strInput.indexOf("�CHEC") > -1) && !(strInput.indexOf("ERREUR") > -1)) //French            
            ) {
            obj.setAttribute("class", "lblMessage");
            obj.setAttribute("className", "lblMessage");
        }
        else {
            obj.setAttribute("class", "lblError");
            obj.setAttribute("className", "lblError");
        }
    } 
    
    //ZD 100284 - Start
    if (document.getElementById("txtStartTime_Text")) {
        var confstarttime_text = document.getElementById("txtStartTime_Text");
        confstarttime_text.onblur = function() {
        formatTimeNew('txtStartTime_Text', 'regStartTime',"<%=Session["timeFormat"]%>")
        };
    }
    
    var ctrlname = 'dgInstanceList_ctl0' + 2 + '_txtStartTime_Text'
    var regname = 'dgInstanceList_ctl0' + 2 + '_regStartTime'

    var ctrl = document.getElementById(ctrlname);
    var j = 2;
    			
	while(ctrl != null)
	{
	    var ctrlnameN = ctrlname;
	    var regnameN = regname; 
        ctrl.onblur = function() {
        formatTimeNew(ctrlnameN, regnameN,"<%=Session["timeFormat"]%>")
        };

		j = j + 1; 
        var ctrlNum = j;
		if(j < 10)
            ctrlNum = "0" + j;

		ctrlname = 'dgInstanceList_ctl' + ctrlNum + '_txtStartTime_Text'
		regname = 'dgInstanceList_ctl' + ctrlNum + '_regStartTime'
		ctrl = document.getElementById(ctrlname);
	}
    //ZD 100284 - End
			
</script>
<%--FB 2487 End--%>

<script type="text/javascript" src="inc/softedge.js"></script>

<%
    if (!Request.QueryString["t"].ToString().Equals("hf"))
    { %>
<!-- #INCLUDE FILE="inc/mainBottomNet.aspx" -->

<script language="javascript">
    document.getElementById("trFooter").style.display="none";
    document.getElementById("trFooter1").style.display="none";
</script>

<%
    }%>
