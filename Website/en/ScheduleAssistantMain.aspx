﻿<%--ZD 100147 start--%>
<%--/* Copyright (C) 2015 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/--%>

<%@ Page Language="C#" Inherits="ns_MyVRM.ScheduleAssistantMain" Buffer="true" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>ScheduleAssistantMain</title>
    <meta name="Description" content="myVRM (Videoconferencing Resource Management) is a revolutionary Web-based software application that manages any video conferencing environment." />
    <meta name="Keywords" content="VRM, myVRM, Videoconferencing Resource Management, video conferencing, video bridges, video endpoints" />
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <script type="text/javascript">        // FB 2790
        var path = '<%=Session["OrgCSSPath"]%>';
        path = path.split('.')[0] + '<%=Session["ThemeType"]%>' + ".css"; // FB 2815
        document.write("<link rel='stylesheet' title='Expedite base styles' type='text/css' href='" + path + "'?'" + new Date().getTime() + "'>"); //ZD 100152

        function DataLoadingnew(val) {
            if (document.getElementById("dataLoadingDIV") == null)
                return false;
            
            document.getElementById("dataLoadingDIV").style.left = window.screen.width / 2 - 100;
            document.getElementById("dataLoadingDIV").style.top = 100;

            if (val == "1")
                document.getElementById("dataLoadingDIV").style.display = 'block'; //ZD 100678
            else
                document.getElementById("dataLoadingDIV").style.display = 'none'; //ZD 100678
        }
    </script>
    <style type="text/css">
        #tblTimeSlot td {
            border: 1px solid #d3d3d3;
        }

        th, td {
            font-weight: normal;
            font-size: 9pt;
        }

        .ConfBgColor {
            height: 17px;
            z-index: -1;
            position: relative;
            background-color: #ededed;
        }

        .PartyFreeBusyCells {
            height: 17px;
            z-index: -1;
            position: relative;
        }

        .HeaderbgColor {
            background-color: gray;
            height: 17px;
        }
    </style>
    <script type="text/javascript" language="javascript">          

    </script>
</head>
<body bottommargin="0" leftmargin="0" rightmargin="0" topmargin="0" marginheight="0" marginwidth="0">
    <form name="frmScheduleAssistantMain" id="frmScheduleAssistantMain" runat="server" method="POST">
    <input type="hidden" id="hdnConfStartDate" runat="server" />
    <input type="hidden" id="hdnConfEndDate" runat="server" />
        
        <center>
            <asp:Label ID="LblError" runat="server" Text="" CssClass="lblError"></asp:Label>

            <div id="container">
                <table class="tablebody" width="100%" border="0">
                    <tr>
                        <td style="text-align: center" colspan="2">
                            <br />
                            <h3>
                                <asp:Label ID="LblHeading" runat="server" Text="<%$ Resources:WebResources, SchedulingAssistant%>"></asp:Label></h3>
                            <br />
                        </td>
                    </tr>
                </table>
                <div id="dataLoadingDIV" name="dataLoadingDIV" align="center" style="display:none">
                    <img border='0' src='image/wait1.gif' alt='Loading..' />
                </div>
                <table width="100%" border="0">
                    <tr>
                        <td>
                            <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                <tr>
                                    <td style="width:18%;"  valign="top">
                                        <div id="UserDiv" style="border: 0px solid #808080; ">
                                            <table width="100%" border="0" cellpadding="0" cellspacing="0" >
                                                <tr><td>
                                                    <table style="border: 1px solid #808080;" cellpadding="0" cellspacing="0" width="100%">
                                                <tr>
                                                    <td>
                                                        <asp:Table Width="100%" runat="server" ID="tblHeaderUsrList" border="0" CellPadding="0"
                                                            BorderColor="LightGray" CellSpacing="0" align="left">
                                                            <asp:TableHeaderRow Height="52px">
                                                                <asp:TableHeaderCell>
                                                                    <span id="Span1" style="text-align: center; font-weight: bold;" runat="server">
                                                                        <asp:Literal Text="<%$ Resources:WebResources, AllAttendees%>" runat="server"></asp:Literal></span>
                                                                </asp:TableHeaderCell>
                                                            </asp:TableHeaderRow>
                                                            <asp:TableHeaderRow >
                                                                <asp:TableHeaderCell><div class="HeaderbgColor" style="width:100%; height:17px"></div></asp:TableHeaderCell>
                                                            </asp:TableHeaderRow>
                                                        </asp:Table>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <div id="UserMainDiv" style="height: 255px; overflow-x: scroll;overflow-y:scroll; width: 100%;"
                                                            onscroll="fnUpdateTop(0)">
                                                            <asp:Table Width="100%" runat="server" ID="tblUsrList" border="1" CellPadding="0"
                                                                BorderColor="DarkGray" CellSpacing="0"  align="left" Style="text-align:left;">
                                                                <asp:TableHeaderRow >
                                                                    <asp:TableHeaderCell ><div style="width:217px;"></div></asp:TableHeaderCell>
                                                                </asp:TableHeaderRow>
                                                            </asp:Table>
                                                        </div>
                                                    </td>
                                                </tr>
                                            </table>
                                            </td></tr></table>
                                        </div>
                                    </td>
                                    <td style="width: 72%;"  valign="top">
                                        <div style="width: 100%;padding-top:0px;">
                                            <iframe align="left" height="350" name="ifrmSchAssitant" id="ifrmSchAssitant" src="" width="100%" frameborder="0" >
                                                <p>
                                                    <asp:Literal Text="<%$ Resources:WebResources, ConferenceSetup_goto%>" runat="server"></asp:Literal><a href="../en/SchedulingAssistant.aspx?wintype=ifr"></a>
                                                </p>
                                            </iframe>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        <table>
                                            <tr>
                                                <td style="width: 15px; border: 1px solid #d3d3d3; height: 12px; background-color: #354FFC"></td>
                                                <td style="padding-left: 5px; padding-right: 5px">
                                                    <asp:Literal ID="Literal1" Text="<%$ Resources:WebResources, Busy%>" runat="server"></asp:Literal>
                                                </td>
                                                <td style="width: 15px; border: 1px solid #d3d3d3; height: 12px; background-color: orangered"></td>
                                                <td style="padding-left: 5px; padding-right: 5px">
                                                    <asp:Literal ID="Literal2" Text="<%$ Resources:WebResources, Tentative%>" runat="server"></asp:Literal>
                                                </td>
                                                <td style="width: 15px; border: 1px solid #d3d3d3; height: 12px; background-color: #71046D"></td>
                                                <td style="padding-left: 5px; padding-right: 5px">
                                                    <asp:Literal ID="Literal3" Text="<%$ Resources:WebResources, OutofOffice%>" runat="server"></asp:Literal>
                                                </td>
                                                <td style="width: 15px; border: 1px solid #d3d3d3; height: 12px; background-color: #FDFDFD"></td>
                                                <td style="padding-left: 5px; padding-right: 5px">
                                                    <asp:Literal ID="Literal4" Text="<%$ Resources:WebResources, NoInformation%>" runat="server"></asp:Literal>
                                                </td>
                                                <td style="width: 15px; border: 1px solid #d3d3d3; height: 12px; background-color: gray"></td>
                                                <td style="padding-left: 5px; padding-right: 5px">
                                                    <asp:Literal ID="Literal5" Text="<%$ Resources:WebResources, Outsideofworkinghours%>"
                                                        runat="server"></asp:Literal>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>

                                <tr>
                                    <td colspan="2" align="center" style="padding-top:10px;">
                                        <input align="middle" type="button" runat="server" style="cursor: pointer" id="ClosePUp"
                                            value=" <%$ Resources:WebResources, Close%> " onclick="javascript: window.close();"
                                            class="altMedium0BlueButtonFormat" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </div>
        </center>
    </form>
</body>
</html>
<script type="text/javascript">
    
    document.getElementById("ifrmSchAssitant").src = "../en/SchedulingAssistant.aspx?stDate=" + document.getElementById("hdnConfStartDate").value + "&enDate=" + document.getElementById("hdnConfEndDate").value;
    //function fnAssignDivWidth() {
    //    var mainDiv = document.getElementById("MainDiv");
    //    var topDiv = document.getElementById("topDiv");
    //    var DivTimeSlot = document.getElementById("DivTimeSlot");
    //    var confStartHour = document.getElementById("hdnConfStartHour");
    //    var DivConfDateCtl = document.getElementById("DivConfDate");

    //    if (mainDiv) {
    //        var xWidth = window.screen.width - 486;
    //        DivTimeSlot.style.width = xWidth + "px";
    //    }

    //    if (mainDiv) {
    //        var xWidth = window.screen.width - 490;
    //        mainDiv.style.width = xWidth + "px";
    //        mainDiv.scrollLeft = (confStartHour.value - 1) * 58;
    //    }
    //    if (topDiv) {
    //        var xWidth = window.screen.width - (490 + 15);
    //        topDiv.style.width = xWidth + "px";
    //        topDiv.scrollLeft = (confStartHour.value - 1) * 58;
    //    }
    //    if (DivConfDateCtl) {
    //        DivConfDateCtl.style.paddingLeft = "40%";
    //    }
    //}

    //$(document).ready(function () {
    //    fnAssignDivWidth();
    //});


    function fnUpdateTop(par) {
        window.frames['ifrmSchAssitant'].document.getElementById("topDiv").scrollLeft = window.frames['ifrmSchAssitant'].document.getElementById("MainDiv").scrollLeft;

        if (par == 1)
            document.getElementById("UserMainDiv").scrollTop = window.frames['ifrmSchAssitant'].document.getElementById("MainDiv").scrollTop;
        else
            window.frames['ifrmSchAssitant'].document.getElementById("MainDiv").scrollTop = document.getElementById("UserMainDiv").scrollTop;
    }

    document.onkeydown = EscClosePopup;
    function EscClosePopup(e) {
        if (e == null)
            var e = window.event;
        if (e.keyCode == 27) {
            window.close();
        }
    }

    

</script>
