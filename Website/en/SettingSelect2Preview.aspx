<%--ZD 100147 start--%>
<%--/* Copyright (C) 2015 myVRM - All Rights Reserved	
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.		
*
* You should have received a copy of the myVRM license with	
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/--%><%--ZD 100147 ZD 100886 End--%>
    <%@ Page Language="C#" Inherits="ns_SettingSelect.SettingSelect2" %>

    <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
    <html>
        <link rel="stylesheet" title="Expedite base styles" type="text/css" href="css/border-table.css">
  <link rel="stylesheet" title="Expedite base styles" type="text/css" href="css/main-table.css">
  <script type="text/javascript" src="script/wincheck.js"></script> <%--FB 3055-URL--%>
  <script type="text/javascript">      // FB 2790
      var path = '<%=Session["OrgCSSPath"]%>';
      path = path.split('.')[0] + '<%=Session["ThemeType"]%>' + ".css"; // FB 2815
      document.write("<link rel='stylesheet' title='Expedite base styles' type='text/css' href='" + path + "'?'" + new Date().getTime() + "'>"); //ZD 100152
  </script>
<script type="text/javascript" src="inc/functions.js"></script>
    <script language="javascript">
        //ZD 100604 start
        var img = new Image();
        img.src = "../en/image/wait1.gif";
        //ZD 100604 End
    function ExpandCollapse(img, str, frmCheck)
    {
    //alert("here");
        obj = document.getElementById(str);
        //alert(img.src + " : " + str + " : " + obj);
        if (obj != null)
        {
            if (frmCheck == true)
            {
                if (document.getElementById("chkExpandCollapse").checked)
                {
                    img.src = img.src.replace("minus", "plus");
                    obj.style.display = "none";
                    //alert("in if");
                }
                else
                {
                    img.src = img.src.replace("plus", "minus");
                    obj.style.display = "";
                    //alert("in else");
                }
            }
            if (frmCheck == false)
            {
                //alert("in else");
                if (img.src.indexOf("minus") >= 0)
                {
                    img.src = img.src.replace("minus", "plus");
                    obj.style.display = "none";
                }
                else
                {
                    img.src = img.src.replace("plus", "minus");
                    obj.style.display = "";
                }
            }            
        }
    }
    
    function ExpandAll()
    {
        //alert("in expand all");
        ExpandCollapse(document.getElementById("imgTemplate"),"<%=trTemplates1.ClientID %>", true);
        ExpandCollapse(document.getElementById("imgHistory"),"<%=trHistory1.ClientID %>", true);
        ExpandCollapse(document.getElementById("imgAVWO"),"<%=trAVWO1.ClientID %>", true);
        ExpandCollapse(document.getElementById("imgCATWO"),"<%=trCATWO1.ClientID %>", true);
        ExpandCollapse(document.getElementById("imgHKWO"),"<%=trHKWO1.ClientID %>", true);
        ExpandCollapse(document.getElementById("imgSearch"),"<%=trSearch1.ClientID %>", true);
    }
  
    </script>
    <body>
    <form runat="server" id="frmSettingSelect">
    
    <table border="0" width="100%" align="center" cellpadding="0" cellspacing="0">
        <tr>
            <td align="center" colspan="2">
                <h3><%-- Code Added for FB 1428--%>
                    <span ID="Field1">myVRM Lobby</span>
                </h3>   
            </td>
            <td style="font-weight:bold" class="subtitleblueblodtext">
            <input id="chkExpandCollapse" type="checkbox" onclick="javascript:ExpandAll()" class="blackblodtext" />Collapse All
            </td>
              
        </tr>
        <tr>
            <td align="center">
                <asp:Label ID="errLabel" runat="server" CssClass="lblError" ></asp:Label>
            </td>
        </tr>
        
        <tr>
            <td colspan="2" rowspan="3" align="left" valign="top">
                <table border="0" width="100%" align="left" cellpadding="0" cellspacing="0">
                    <tr>
                        <td align="left" valign="top">
                        <asp:Table ID="tblTemplates" runat="server" width="350px" CellPadding="0" CellSpacing="0" BorderStyle="None" BorderWidth="0">
                            <asp:TableRow runat="server" ID="trTemplates" CssClass="PortletHeader">
                                <asp:TableCell ID="TableCell1" Height="30px" runat="server" Width="100%"> 
                                    <table border="0"><tr><td width="98%" valign="middle" align="right">
                                    <%-- Code Added for FB 1428--%>
                                   <span id="Field2"><%if (Application["Client"].ToString().ToUpper() == "MOJ"){%>My Hearing Templates<%}else{ %>My Conference Templates<%} %></span> <%--Edited for FB 1428--%>
                                    </td><td width="5%" valign="bottom">
                    <%--Window Dressing--%>
                                    <asp:ImageButton ID="imgTemplate" runat="server" ImageUrl="image/loc/nolines_minus.gif" Height="25" Width="25" vspace="0" hspace="0"  AlternateText="Expand/Collapse"/> <%--ZD 100419--%>
                                    </td></tr>                                    
                                    </table>
                                </asp:TableCell>
                           </asp:TableRow>
                           <asp:TableRow runat="server" ID="trTemplates1"><asp:TableCell ID="TableCell2" runat="server" Width="100%">
                                <asp:DataGrid CellPadding="0" CellSpacing="0" BorderStyle="None" ID="dgTemplates" runat="server" AutoGenerateColumns="False" Width="100%" ShowFooter="true">
                                    <FooterStyle Font-Bold="True" HorizontalAlign="right"></FooterStyle>
                                    <AlternatingItemStyle CssClass="tableBody"></AlternatingItemStyle>
                                    <ItemStyle CssClass="tableBody"></ItemStyle>
                                    <HeaderStyle CssClass="tableHeader" ></HeaderStyle>
                                    <Columns>
                                    <%--Window Dressing--%>
                                    <asp:TemplateColumn HeaderStyle-CssClass="tableHeader" HeaderText="Template Name">
                                        <ItemTemplate>
                                            <asp:Label ID="lblTemplateName" Text='<%#DataBinder.Eval(Container, "DataItem.name") %>' runat="server" ></asp:Label>
                                        </ItemTemplate>
                                        <FooterTemplate>
                                             <asp:LinkButton runat="server" Text="more>>" ID="btnTemplateMore" OnClick="ViewMyTemplates" OnClientClick="DataLoading(1)"></asp:LinkButton>
                                        </FooterTemplate>
                                    </asp:TemplateColumn>
                                    </Columns>
                                </asp:DataGrid>
                                <asp:Label ID="lblNoTemplate" CssClass="lblError" Visible="false" Text="No Templates Found" runat="server"></asp:Label>
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow runat="server" ID="trTemplates2" Width="100%">
                            <asp:TableCell ID="TableCell3" Height="30px" runat="server" Width="100%">&nbsp;</asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow runat="server" ID="trHistory" Width="100%">
                            <asp:TableCell ID="TableCell4" CssClass="PortletHeader" runat="server" Height="30" Width="100%">
                                    <table><tr><td width="98%" valign="middle" align="right">
                                    <%-- Code Added for FB 1428--%>
                                   <span id="Field3"><%if (Application["Client"].ToString().ToUpper() == "MOJ"){%>My Hearing History<%}else{ %>My Conference History<%} %></span> <%--Edited for FB 1428--%>
                                    </td><td width="2%" valign="middle">
                    <%--Window Dressing--%>
                                    <asp:ImageButton ID="imgHistory" runat="server" ImageUrl="image/loc/nolines_minus.gif" Height="25" Width="25" AlternateText="Expand/Collapse"  /> <%--ZD 100419--%>
                                    </td></tr></table>
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow runat="server" ID="trHistory1" Width="100%">                           
                            <asp:TableCell ID="TableCell5" runat="server" Width="100%">
                                <asp:datagrid CellPadding="0" CellSpacing="0" BorderStyle="None" id="dgHistory" runat="server" autogeneratecolumns="False" width="100%" Visible="False" OnEditCommand="ManageConference">
                                    <AlternatingItemStyle CssClass="tableBody"></AlternatingItemStyle>
                                    <ItemStyle CssClass="tableBody"></ItemStyle>
                                    <HeaderStyle CssClass="tableHeader" ></HeaderStyle>
                                    <Columns>
                                    <asp:BoundColumn DataField="confID" Visible="false">
                                        <HeaderStyle ForeColor="Black"/>
                                    </asp:BoundColumn>
                                    <%--Window Dressing--%>
                                    <asp:TemplateColumn HeaderStyle-CssClass="tableHeader" HeaderText="Conference Name" FooterStyle-HorizontalAlign="right">
                                        <%--<HeaderStyle BackColor="#c1c1c1" ForeColor="black" Font-Bold="True"></HeaderStyle>--%>
                                        <ItemTemplate>
                                            <asp:LinkButton ID="btnConferenceName" CommandName="Edit" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.confName") %>'  OnClientClick="DataLoading(1)" ></asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    </Columns>
                                </asp:datagrid>
                                <asp:Label ID="lblNoHistory" CssClass="lblError" Visible="false" Text="No History Found" runat="server"></asp:Label>
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow runat="server" ID="trTemplates3" Width="100%">
                            <asp:TableCell ID="TableCell6" Height="30px" runat="server" Width="100%">&nbsp;</asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow runat="server" ID="trSearch" Width="100%">
                            <asp:TableCell ID="TableCell7" CssClass="PortletHeader" runat="server" Height="30" Width="100%">
                                <table><tr><td width="98%" valign="middle" align="right">
                                    <asp:Label ID="lblSearchTemplate" runat="server" Text="No Template Defined"></asp:Label>
                                    &nbsp;&nbsp;&nbsp;
                                    </td><td width="2%" valign="middle">
                    <%--Window Dressing--%>
                                    <asp:ImageButton ID="imgSearch" runat="server" ImageUrl="image/loc/nolines_minus.gif" Height="25" Width="25" AlternateText="Expand/Collapse"  /><%--ZD 100419--%>
                                </td></tr></table>
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow runat="server" ID="trSearch1" Width="100%">                           
                            <asp:TableCell ID="TableCell8" runat="server" Width="100%">
                                <asp:datagrid CellPadding="0" CellSpacing="0" BorderStyle="None" id="dgSearch" runat="server" autogeneratecolumns="False" width="100%" Visible="True" ShowFooter="false" OnEditCommand="ManageConference">
                                    <FooterStyle Font-Bold="True" HorizontalAlign="right"></FooterStyle>
                                    <AlternatingItemStyle CssClass="tableBody"></AlternatingItemStyle>
                                    <ItemStyle CssClass="tableBody"></ItemStyle>
                                    <HeaderStyle CssClass="tableHeader" ></HeaderStyle>
                                    <Columns>
                                        <asp:BoundColumn DataField="ConferenceID" Visible="false"></asp:BoundColumn>
                                        <asp:BoundColumn DataField="ConferenceUniqueID" Visible="false" HeaderText="Unique ID" HeaderStyle-ForeColor="blue"></asp:BoundColumn>
                                        <%--Window Dressing--%>
                                        <asp:TemplateColumn HeaderText="Conference Name" ItemStyle-CssClass="tableBody" HeaderStyle-CssClass="tableHeader" FooterStyle-HorizontalAlign="right">
                                            <ItemTemplate>
                                                <asp:LinkButton ID="btnConferenceName" CommandName="Edit" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.ConferenceName") %>'></asp:LinkButton>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                         <%--Window Dressing start --%>
                                        <asp:BoundColumn DataField="ConferenceDateTime" HeaderStyle-CssClass="tableHeader" HeaderText="Date/Time" Visible="false"></asp:BoundColumn>
                                        <asp:TemplateColumn HeaderText="Conference Start Time" ItemStyle-CssClass="tableBody" HeaderStyle-CssClass="tableHeader" FooterStyle-HorizontalAlign="right" ItemStyle-HorizontalAlign="left">
                                        <%--Window Dressing end --%>
                                            <ItemTemplate>
                                                <asp:Label Visible="false" ID="lblDuration" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.ConferenceDateTime") + " mins" %>'></asp:Label> <%--<%# Int32.Parse(DataBinder.Eval(Container, "DataItem.ConferenceDuration").ToString())/60 + " Hr(s) " + Int32.Parse(DataBinder.Eval(Container, "DataItem.ConferenceDuration").ToString())%60 + " Min(s)" %>--%><%-- ZD 100528--%>
                                                <asp:Label ID="lblStartingIn" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.StartWithin") %>'></asp:Label>
                                            </ItemTemplate>
                                            <FooterTemplate>
                                               <asp:LinkButton ID="btnMoreSearch" Visible="true" Text="more>>" runat="server" OnClick="GetSearchConference" OnClientClick="DataLoading(1)"></asp:LinkButton>
                                            </FooterTemplate>
                                        </asp:TemplateColumn>
                                    </Columns>
                                </asp:datagrid>
                                <asp:Label ID="lblNoSearch" CssClass="lblError" runat="server"><%if (Application["Client"].ToString().ToUpper() == "MOJ"){%>No Hearing Found<%}else{ %>No Conferences Found<%} %></asp:Label><%--Edited for FB 1428--%>
                            </asp:TableCell>
                        </asp:TableRow>                        </asp:Table>
                        </td>
                        <td valign="top" align="left">
                        <asp:Table ID="tblWorOrders" runat="server" width="350px" CellPadding="0" CellSpacing="0">

                        <asp:TableRow runat="server" ID="trAVWO"> 
                            <asp:TableCell ID="TableCell9" CssClass="PortletHeader" runat="server" Height="30" Width="100%">
                                <table><tr><td width="98%" valign="middle" align="right">
                                    My Audio/Visual Work Orders
                                    &nbsp;&nbsp;&nbsp;
                                    </td><td width="2%" valign="middle">
                    <%--Window Dressing--%>
                                    <asp:ImageButton ID="imgAVWO" runat="server" ImageUrl="image/loc/nolines_minus.gif" Height="25" Width="25" AlternateText="Expand/Collapse"  /><%--ZD 100419--%>
                                </td></tr></table>
                            </asp:TableCell>
                            </asp:TableRow>
                        <asp:TableRow runat="server" ID="trAVWO1">
                        <asp:TableCell ID="TableCell10" runat="server" >
                            <asp:datagrid CellPadding="0" CellSpacing="0" BorderStyle="None" id="dgAVWO" runat="server" autogeneratecolumns="False" width="100%" ShowFooter="true">
                                    <FooterStyle Font-Bold="True" HorizontalAlign="right"></FooterStyle>
                                    <AlternatingItemStyle CssClass="tableBody"></AlternatingItemStyle>
                                    <ItemStyle CssClass="tableBody"></ItemStyle>
                                    <HeaderStyle CssClass="tableHeader" ></HeaderStyle>
                                    <Columns>
                                        <%--Window Dressing - Start--%>
                                        <asp:BoundColumn DataField="Name" ItemStyle-CssClass="tableBody" HeaderStyle-CssClass="tableHeader" HeaderText="Work Order Name"></asp:BoundColumn>
                                        <asp:TemplateColumn HeaderText="Date/Time" ItemStyle-CssClass="tableBody" HeaderStyle-CssClass="tableHeader">
                                         <%--Window Dressing - end--%>
                                            <ItemTemplate>
                                                <asp:Label ID="Label2" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.StartByDate") + " " + DataBinder.Eval(Container, "DataItem.StartByTime") %>'></asp:Label>            
                                            </ItemTemplate>
                                            <FooterTemplate >
                                                <asp:LinkButton runat="server" Text="more>>" ID="btnAVMore" OnClick="ViewMyPendingAVWorkOrders" OnClientClick="DataLoading(1)"></asp:LinkButton>
                                            </FooterTemplate>
                                        </asp:TemplateColumn>
                                    </Columns>
                                    </asp:datagrid>
                                    <asp:Label ID="lblNoAVWO" Visible="false" Text="No Work Orders found." CssClass="lblError" runat="server"></asp:Label>
                                    </asp:TableCell></asp:TableRow>
                        <asp:TableRow runat="server" ID="trAVWO2"><asp:TableCell ID="TableCell11" Height="30px" runat="server" HorizontalAlign="right" ></asp:TableCell></asp:TableRow>
                            <asp:TableRow runat="server" ID="trCATWO">              
                            <asp:TableCell ID="TableCell12" CssClass="PortletHeader" runat="server" Height="30" Width="100%">
                                    <table><tr><td width="98%" valign="middle" align="right">
                                        My Catering Work Orders
                                        &nbsp;&nbsp;&nbsp;
                                        </td><td width="2%" valign="middle">
                    <%--Window Dressing--%>
                                        <asp:ImageButton ID="imgCATWO" runat="server" ImageUrl="image/loc/nolines_minus.gif" Height="25" Width="25" AlternateText="Expand/Collapse"  /><%--ZD 100419--%>
                                    </td></tr></table>
                                </asp:TableCell>
                                </asp:TableRow><asp:TableRow runat="server" ID="trCATWO1">
                                <asp:TableCell ID="TableCell13" runat="server" >
                                <asp:datagrid CellPadding="0" CellSpacing="0" BorderStyle="None" id="dgCATWO" runat="server" autogeneratecolumns="False" width="100%">
                                    <FooterStyle Font-Bold="True" HorizontalAlign="right"></FooterStyle>
                                    <AlternatingItemStyle CssClass="tableBody"></AlternatingItemStyle>
                                    <ItemStyle CssClass="tableBody"></ItemStyle>
                                    <HeaderStyle CssClass="tableHeader" ></HeaderStyle>
                                    <Columns>
                                        <asp:BoundColumn DataField="Name" ItemStyle-CssClass="tableBody" HeaderStyle-CssClass="tableHeader" HeaderText="Work Order Name" ItemStyle-Width="60%">
                                        </asp:BoundColumn>
                                        <asp:TemplateColumn HeaderStyle-CssClass="tableHeader" ItemStyle-CssClass="tableBody" HeaderText="Date/Time" ItemStyle-Width="60%">
                                            <ItemTemplate>
                                                <asp:Label ID="Label1" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.StartByDate") + " " +  DataBinder.Eval(Container, "DataItem.StartByTime") %>'></asp:Label>            
                                            </ItemTemplate>
                                            <FooterTemplate >
                                                <asp:LinkButton runat="server" Text="more>>" ID="btnCATMore" OnClick="ViewMyPendingCATWorkOrders" OnClientClick="DataLoading(1)"></asp:LinkButton>
                                            </FooterTemplate>
                                        </asp:TemplateColumn>
                                   </Columns>
                                    </asp:datagrid>
                                        <asp:Label ID="lblNoCATWO" Visible="false" Text="No Work Orders found." CssClass="lblError" runat="server"></asp:Label>

                               </asp:TableCell></asp:TableRow>
                               <asp:TableRow runat="server" ID="trCATWO2"><asp:TableCell ID="TableCell14" Height="30px" runat="server" HorizontalAlign="right" >&nbsp;</asp:TableCell></asp:TableRow>
                               <asp:TableRow runat="server" ID="trHKWO">
                                <asp:TableCell ID="TableCell15" CssClass="PortletHeader" runat="server" Height="30" Width="100%">
                                    <table><tr><td width="98%" valign="middle" align="right">
                                    My Facility Work Orders<!-- FB 2570 -->
                                        &nbsp;&nbsp;&nbsp;
                                        </td><td width="2%" valign="middle">
                    <%--Window Dressing--%>
                                        <asp:ImageButton ID="imgHKWO" runat="server" ImageUrl="image/loc/nolines_minus.gif" Height="25" Width="25" AlternateText="Expand/Collapse" /><%--ZD 100419--%>
                                    </td></tr></table>
                            </asp:TableCell>
                           </asp:TableRow><asp:TableRow runat="server" ID="trHKWO1">
                           <asp:TableCell ID="TableCell16" runat="server" >
                                <asp:DataGrid CellPadding="0" CellSpacing="0" BorderStyle="None" id="dgHKWO" runat="server" autogeneratecolumns="False" width="100%">
                                    <FooterStyle Font-Bold="True" HorizontalAlign="right"></FooterStyle>
                                    <AlternatingItemStyle CssClass="tableBody"></AlternatingItemStyle>
                                    <ItemStyle CssClass="tableBody"></ItemStyle>
                                    <HeaderStyle CssClass="tableHeader" ></HeaderStyle>
                                    <Columns>
                                    <%-- Window Dressing --%>
                                    <asp:BoundColumn DataField="Name" ItemStyle-CssClass="tableBody" HeaderText="Work Order Name" HeaderStyle-CssClass="tableHeader" ItemStyle-Width="60%">
                                    </asp:BoundColumn>
                                    <asp:TemplateColumn HeaderText="Date/Time" ItemStyle-CssClass="tableBody" ItemStyle-Width="40%" HeaderStyle-CssClass="tableHeader">
                                        <ItemTemplate>
                                            <asp:Label ID="Label3" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.StartByDate") + " " + DataBinder.Eval(Container, "DataItem.StartByTime") %>'></asp:Label>            
                                        </ItemTemplate>
                                        <FooterTemplate>
                                            <asp:LinkButton runat="server" Text="more>>" ID="btnHKMore" OnClick="ViewMyPendingHKWorkOrders" OnClientClick="DataLoading(1)"></asp:LinkButton>
                                        </FooterTemplate>
                                    </asp:TemplateColumn>
                                    </Columns>
                                </asp:datagrid>
                                <asp:Label ID="lblNoHKWO" Visible="false" Text="No Work Orders found." CssClass="lblError" runat="server"></asp:Label>
                                </asp:TableCell></asp:TableRow>
                                <asp:TableRow runat="server" ID="trHKWO2"><asp:TableCell ID="TableCell17" Height="30px" runat="server" HorizontalAlign="right" ></asp:TableCell></asp:TableRow>
                            </asp:Table>                    
                        </td>
                    </tr>
                </table>
            </td>
            <td rowspan="3" style="width: 20%" valign="top" align="left">
                <asp:table id="actionTable" runat="server" width="235" CellPadding="0" CellSpacing="0">
                    <asp:TableRow ID="TableRow1" runat="server" CssClass="LinksHeader">
                    <%--Window Dressing--%>
                        <asp:TableCell ID="TableCell18" runat="server" VerticalAlign="Middle" HorizontalAlign="right">Links&nbsp;&nbsp;&nbsp;</asp:TableCell>
                    </asp:TableRow>
                </asp:table>
            </td>
        </tr>
    </table> 
    <input type="hidden" id="txtLAlerts" runat="server" value="" /> 
    <input type="hidden" id="txtHAlerts" runat="server" value="" /> 
    <asp:linkbutton id="btnCalendar" runat="server" OnClick="GoToCalendar" OnClientClick="javascript:DataLoading(1);">Calendar</asp:linkbutton>
    <%-- Code changed for FB 1428--%>
    <asp:linkbutton id="btnConference" runat="server" OnClick="CreateNewConference" OnClientClick="DataLoading(1)"><%if (Application["Client"].ToString().ToUpper() == "MOJ"){%>Create New Hearing<%}else{ %>Create New Conference<%} %></asp:linkbutton> <%--Edited For FB 1428--%>
    <asp:linkbutton id="btnAVInvManagement" runat="server" OnClick="InventoryManagement" OnClientClick="DataLoading(1)">Inventory Sets</asp:linkbutton>
    <asp:linkbutton id="btnCATMenuManagement" runat="server" OnClick="MenuManagement" OnClientClick="DataLoading(1)">Catering Menus</asp:linkbutton>
    <asp:linkbutton id="btnHKGroupManagement" runat="server" OnClick="GroupManagement" OnClientClick="DataLoading(1)">Facility Services</asp:linkbutton><!-- FB 2570 -->
    <asp:linkbutton id="btnNewAVWorkorder" runat="server" OnCommand="CreateNewWorkorder" CommandArgument="1" Text="Create New AV Work Order" OnClientClick="DataLoading(1)" ></asp:linkbutton>
    <asp:linkbutton id="btnNewCATWorkorder" runat="server" OnCommand="CreateNewWorkorder" CommandArgument="2" Text="Create New CAT Work Order" OnClientClick="DataLoading(1)"></asp:linkbutton>
    <asp:linkbutton id="btnNewHKWorkorder" runat="server" OnCommand="CreateNewWorkorder" CommandArgument="3" Text="Create New Facility Work Order" OnClientClick="DataLoading(1)"></asp:linkbutton><!-- FB 2570 -->

        &nbsp; &nbsp; &nbsp;
        <input id="opr" type="hidden" value="MODIFY" />
    <input type="hidden" id="helpPage" value="43" />
    <div id="dataLoadingDIV" name="dataLoadingDIV" align="center" style="display:none">
          <img border='0' src='image/wait1.gif' alt='Loading..' />
    </div><%--ZD 100678 End--%>
        <br />
    </form>
    </body>
           <script language="javascript">
            if ("<%=Application["Client"].ToString().ToUpper()%>" != "WASHU")
                ExpandAll();
            else
                document.getElementById("chkExpandCollapse").checked = false;
            if ("<%=Application["Client"].ToString().ToUpper() %>"  == "NGC")
                document.getElementById("txtAlerts").innerText = document.getElementById("txtLAlerts").value;
        </script> 
    </html>
