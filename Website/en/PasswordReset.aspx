﻿
<%--// ZD 103954 Start--%>
<%--/* Copyright (C) 2015 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/--%>
<%@ Page Language="C#" AutoEventWireup="true" Inherits="en_PasswordReset"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<!--Window Dressing Start-->
<link rel="stylesheet" title="Expedite base styles" type="text/css" href="css/border-table.css">
<link rel="stylesheet" title="Expedite base styles" type="text/css" href="css/main-table.css">
<link rel="stylesheet" title="Expedite base styles" type="text/css" href="../en/Organizations/Original/Styles/main.css">
<!--Window Dressing End-->
<script runat="server">
</script>
 
<html xmlns="http://www.w3.org/1999/xhtml">
    <script language="JavaScript" src="inc/functions.js" type="text/javascript"> </script>
     <script type="text/javascript">         // FB 2790
         var path = '<%=Session["OrgCSSPath"]%>';
         if (path == "")
             path = "Organizations/Org_11/CSS/Mirror/Styles/main.css";
         path = path.split('.')[0] + '<%=Session["ThemeType"]%>' + ".css"; // FB 2815
         document.write("<link rel='stylesheet' title='Expedite base styles' type='text/css' href='" + path + "'?'" + new Date().getTime() + "'>"); //ZD 100152
    </script>

  <script type="text/javascript">
      function fnCheck() {
          var pwd1 = document.getElementById("txtOldPassword").value;
          var pwd2 = document.getElementById("txtPassword1").value;
          var pwd3 = document.getElementById("txtPassword2").value;
          var labl = document.getElementById("LblError");
          var browserlang = '<%=Session["browserlang"]%>';
          labl.innerHTML = "";  
          if (pwd1 == "") {
              if ((browserlang.indexOf("en-US") > -1) || (browserlang.indexOf("en") > -1))
                  labl.innerHTML = "Please enter old password";
              if ((browserlang.indexOf("fr-CA") > -1) || (browserlang.indexOf("fr") > -1))
                  labl.innerHTML = "Entrez l'ancien mot de passe";
              if ((browserlang.indexOf("es-US") > -1) || (browserlang.indexOf("es") > -1))
                  labl.innerHTML = "Por favor introduzca su contraseña anterior"; 
              check();
              return false;
          }
          if (pwd2 != "") {
              if (pwd3 == "") {
                  if ((browserlang.indexOf("en-US") > -1) || (browserlang.indexOf("en") > -1))
                      labl.innerHTML = "Please Re-enter password";
                  if ((browserlang.indexOf("fr-CA") > -1) || (browserlang.indexOf("fr") > -1))
                      labl.innerHTML = "S'il vous plaît entrer de nouveau le mot de passe";
                  if ((browserlang.indexOf("es-US") > -1) || (browserlang.indexOf("es") > -1))
                      labl.innerHTML = "Por favor, Vuelva a escribir la contraseña"; 
                check();
                  return false;
              }
          }
          else {

              if ((browserlang.indexOf("en-US") > -1) || (browserlang.indexOf("en") > -1))
                  labl.innerHTML = "Please enter the password"; 
              if ((browserlang.indexOf("fr-CA") > -1) || (browserlang.indexOf("fr") > -1))
                  labl.innerHTML = "Veuillez saisir le mot de passe"; 
              if ((browserlang.indexOf("es-US") > -1) || (browserlang.indexOf("es") > -1))
                  labl.innerHTML = "Por favor, introduzca la contraseña"; 
               check();
              return false;
          }
          if (pwd2 != pwd3) {

              if ((browserlang.indexOf("en-US") > -1) || (browserlang.indexOf("en") > -1))
                  labl.innerHTML = "Passwords do not match"; 
              if ((browserlang.indexOf("fr-CA") > -1) || (browserlang.indexOf("fr") > -1))
                  labl.innerHTML = "Les mots de passe ne coïncident pas";
              if ((browserlang.indexOf("es-US") > -1) || (browserlang.indexOf("es") > -1))
                  labl.innerHTML = "Las contraseñas no coinciden";
              check();
              return false;
          }
           
         check();
         return fnRulePassword();
          
      }
       function check()
       {
         var labl = document.getElementById("LblError");
         var strInput = labl.innerHTML.toUpperCase();
        if (((strInput.indexOf("SUCCESS") > -1) && !(strInput.indexOf("UNSUCCESS") > -1) && !(strInput.indexOf("ERROR") > -1))
        || (strInput.indexOf("QUITTÉ") > -1) || (strInput.indexOf("SATISFACT") > -1))  {
            labl.setAttribute("class", "lblMessage");
            labl.setAttribute("className", "lblMessage");
        }
        else {
            labl.setAttribute("class", "lblError");
            labl.setAttribute("className", "lblError");
        } 
        }    

      function fnRulePassword()
    {
 if(document.getElementById("reqPassword1").style.display == "block" || document.getElementById("cmpValPassword1").style.display == "block")
        {
            window.scrollTo(0,0);
            return false;
        }

        if(document.getElementById("txtPassword1").style.backgroundImage == "" && document.getElementById("txtPassword1").value == "")
        {
            ValidatorEnable(document.getElementById('reqPassword1'), true);
            window.scrollTo(0,0);
            return false;
        }
        var password = document.getElementById("txtPassword1").value;
        if("<%=Session["EnablePasswordRule"]%>" == "1")
        {
            var passed = validatePassword(password, {
	                length:   [6, Infinity],
	                lower:    1,
	                upper:    1,
	                numeric:  1,
	                special:  0,
	                badWords: [],
	                badSequenceLength: 0
                    });
            if(passed == false)
            {
               document.getElementById("cmpValPassword1").innerHTML = RSWeakPassword;  
                document.getElementById("txtPassword1").focus();
                document.getElementById("cmpValPassword1").style.display = "block";
                return false;          
            }
        }
       DataLoading(1); 
       return true;
}
        function fnTabNav(event) {
                setTimeout("if (document.getElementById('btnCancel') != null) {document.getElementById('btnSubmit').focus();document.getElementById('btnSubmit').setAttribute('onfocus', '');}", 100);          
        }
        fnTabNav();
      function validatePassword(pw, options) 
      {
	// default options (allows any password)
	var o = {
		lower:    0,
		upper:    0,
		alpha:    0, /* lower + upper */
		numeric:  0,
		special:  0,
		length:   [0, Infinity],
		custom:   [ /* regexes and/or functions */ ],
		badWords: [],
		badSequenceLength: 0,
		noQwertySequences: false,
		noSequential:      false
	};
	for (var property in options)
		o[property] = options[property];

	var	re = {
			lower:   /[a-z]/g,
			upper:   /[A-Z]/g,
			alpha:   /[A-Z]/gi,
			numeric: /[0-9]/g,
			special: /[\W_]/g
		},
		rule, i;
	// enforce min/max length
	if (pw.length < o.length[0] || pw.length > o.length[1])
		return false;

	// enforce lower/upper/alpha/numeric/special rules
	for (rule in re) {
		if ((pw.match(re[rule]) || []).length < o[rule])
			return false;
	}
	// enforce word ban (case insensitive)
	for (i = 0; i < o.badWords.length; i++) {
		if (pw.toLowerCase().indexOf(o.badWords[i].toLowerCase()) > -1)
			return false;
	}
	// enforce the no sequential, identical characters rule
	if (o.noSequential && /([\S\s])\1/.test(pw))
		return false;
	// enforce alphanumeric/qwerty sequence ban rules
	if (o.badSequenceLength) {
		var	lower   = "abcdefghijklmnopqrstuvwxyz",
			upper   = lower.toUpperCase(),
			numbers = "0123456789",
			qwerty  = "qwertyuiopasdfghjklzxcvbnm",
			start   = o.badSequenceLength - 1,
			seq     = "_" + pw.slice(0, start);
		for (i = start; i < pw.length; i++) {
			seq = seq.slice(1) + pw.charAt(i);
			if (
				lower.indexOf(seq)   > -1 ||
				upper.indexOf(seq)   > -1 ||
				numbers.indexOf(seq) > -1 ||
				(o.noQwertySequences && qwerty.indexOf(seq) > -1)
			) {
				return false;
			}
		}
	}
	// enforce custom regex/function rules
	for (i = 0; i < o.custom.length; i++) {
		rule = o.custom[i];
		if (rule instanceof RegExp) {
			if (!rule.test(pw))
				return false;
		} else if (rule instanceof Function) {
			if (!rule(pw))
				return false;
		}
	}
	return true;
}
      function PreservePassword()
     {
        document.getElementById("txtPassword1_1").value = document.getElementById("txtPassword1").value;
        document.getElementById("txtPassword1_2").value = document.getElementById("txtPassword2").value;
        var password = document.getElementById("txtPassword1").value;
        if("<%=Session["EnablePasswordRule"]%>" == "1")
        {
            var passed = validatePassword(password, {
	                length:   [6, Infinity],
	                lower:    1,
	                upper:    1,
	                numeric:  1,
	                special:  0,
	                badWords: [],
	                badSequenceLength: 0
                    });
            if(passed == false)
            {
                document.getElementById("cmpValPassword1").style.visibility = 'visible';
            }
            else
            {
                document.getElementById("cmpValPassword1").style.visibility = 'hidden';
            }
      }
}
    </script>
<body>
     <center>
        <form id="frmPassword" autocomplete="off" runat="server">
        <asp:ScriptManager ID="scpMgrUI" runat="server" EnableScriptLocalization="true">
		    <Scripts>                
			    <asp:ScriptReference Path= "~/ResourceScript/StringResources.js" ResourceUICultures="<%$ Resources:WebResources, UICulture%>"/>
		    </Scripts>
	    </asp:ScriptManager>
              
    <input id="txtPassword1_1" runat="server" type="hidden" />
    <input id="txtPassword1_2" runat="server" type="hidden" />
            <table>
                <tr style="height: 80px">
                    <td>
                    </td>
                </tr>
            </table>
            <table cellpadding="1" cellspacing="1" width="600" style=
              "border-color:grey;border-width:1px;border-style:Solid;height:450px;overflow:auto"; >
                <tr align="left">
                    <td>
                        <div>
                            <table style="width: 100%">
                                <tr>
                                    <td align="right">
                                        <h3>
                                            <asp:Literal ID="Literal1" Text="<%$ Resources:WebResources, PassReset %>" runat="server"></asp:Literal>
                                        </h3>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="center" style="width: 1168px">
                                        <asp:Label ID="LblError" runat="server" Text="" CssClass="lblError"></asp:Label>
                                    </td>
                                </tr>
                            </table>
                            <table cellpadding="2" cellspacing="2" width="400" style="margin-left:223px">
                                <tr>
                                    <td style="width: 80px">
                                    </td>
                                    <td align="left" colspan="2">
                                        <asp:Label ID="LblMessage" runat="server" Font-Bold="true"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="height: 20px">
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left" colspan="2" style="width:30px">
                                    <b><span class="subtitleblueblodtext" style="margin-left:-63px">
                                        <asp:Literal ID="Literal2" Text="<%$ Resources:WebResources,Passtext %>" runat="server"></asp:Literal></span>
                                        </b>
                                    </td>
                                </tr>
                              <tr>
                               <td style="height: 30px">
                                    </td>
                                </tr>
                                <tr>
                                <td align="left" nowrap="nowrap">
                                <label class="blackblodtext" style="margin-left:-96px">
                                <asp:Literal ID="Literal3" Text="<%$ Resources:WebResources, OldPass%>" runat="server"></asp:Literal><span class="reqfldstarText">*</span>
                                </label>&nbsp;&nbsp;&nbsp;
                                </td> 
                                <td align ="left" style="width: 580px">
                                <asp:TextBox ID="txtOldPassword" runat="server" TextMode="password" autocomplete="off" CssClass="altText" Style="width: 200px;" ></asp:TextBox>
                                </td>
                                </tr>
                                 <tr>
                                    <td style="height: 30px">
                                    </td>
                                </tr>
                                <tr>
                                <td align="left"  nowrap="nowrap">
                                <label class="blackblodtext" style="margin-left:-96px">
                                <asp:Literal ID="Literal4" Text="<%$ Resources:WebResources, ConfSetup_NewPass%>" runat="server"> </asp:Literal><span class="reqfldstarText">*</span>
                                </label> &nbsp;&nbsp;&nbsp;
                                </td>
                                <td align="left" style="width: 580px">
                                <asp:TextBox ID="txtPassword1" runat="server" TextMode="password" onchange="javascript:PreservePassword()" autocomplete= "off"  CssClass="altText" Style="width: 200px;"></asp:TextBox>
                              <asp:RequiredFieldValidator ID="reqPassword1" Enabled="false" ControlToValidate="txtPassword1" Display="Dynamic"
                                ErrorMessage="<%$ Resources:WebResources, Required%>" runat="server"></asp:RequiredFieldValidator>
                                <asp:CompareValidator ID="cmpValPassword1" runat="server" ControlToCompare="txtPassword2"
                                ControlToValidate="txtPassword1" Display="Dynamic" ErrorMessage="<%$ Resources:WebResources, EnterPassword%>"></asp:CompareValidator>
                                </td>
                                </tr>
                                <tr id="Tr1"  runat="server">
                                <td align="left" style="margin-left:-96px">
                        <td colspan = "2" align="left">
                           <span id="spanpassword" runat="server"><asp:Literal ID="Literal7" Text="<%$ Resources:WebResources, ManageUserProfile_spanpassword%>" runat="server"></asp:Literal></span>
                        </td></td>
                        </tr>  <tr>
                                    <td style="height: 30px">
                                    </td>
                                </tr>
                                <tr>
                                <td align="left" nowrap="nowrap">
                                <label class="blackblodtext" style="margin-left:-96px">
                                <asp:Literal ID="Literal5"  Text="<%$ Resources:WebResources, ConferenceSetup_ConfirmPasswor%>" runat="server"> </asp:Literal><span class="reqfldstarText">*</span>
                                </label>  &nbsp;&nbsp;&nbsp
                                </td>
                                <td align="left" style="width: 580px">
                                <asp:TextBox ID="txtPassword2" runat="server" TextMode="password" onchange="javascript:PreservePassword()" autocomplete = "off" CssClass="altText" Style="width: 200px;"></asp:TextBox>
                                <asp:RegularExpressionValidator ID="regPassword2" ControlToValidate="txtPassword2" Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="<%$ Resources:WebResources, InvalidCharacters33%>" ValidationExpression="^[^<>&]*$"></asp:RegularExpressionValidator><%--FB 2339--%>
                                <asp:CompareValidator ID="cmpValPassword2" runat="server" ControlToCompare="txtPassword1"
                                    ControlToValidate="txtPassword2" Display="Dynamic" ErrorMessage="<%$ Resources:WebResources, PasswordNotMatch%>"></asp:CompareValidator>
                                </td>
                                </tr>
                                <tr>
                                  <td style="height: 30px">
                                  </td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        <table width="80%">
                                            <tr>
                                                <td align ="right" style="width:183px"  >
                                                <button id="btnCancel" runat="server" onkeydown="javascript:fnTabNav(event);" class="altMedium0BlueButtonFormat" onclick="javascript:return fnLoginPage()" style="margin-left:-150px" ><asp:Literal ID="Literal6" Text="<%$Resources:WebResources, Cancel%>" runat="server"></asp:Literal></button>
                                                </td>
                                                <td align="left" style="width:234px">
                                                <asp:button id="btnSubmit" CssClass="altMedium0BlueButtonFormat" runat="server" Text="<%$ Resources:WebResources, EmailLogin_BtnSubmit%>" 
                                                OnClientClick="javascript:return fnCheck();" OnClick="BtnSubmit_Click"  />
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </td>
                </tr>
            </table>
        </form>
    </center>
</body>
</html>
    <script type = "text/javascript">
    function fnLoginPage() {
            var browserlang = navigator.userLanguage || navigator.language;
            if (browserlang != "" && browserlang != null)
                window.location.replace('genlogin.aspx?lang=' + browserlang);
            else
                window.location.replace('genlogin.aspx?lang="en-US"');
            return false;
        }
              
        var obj = document.getElementById("LblError");
        if (obj != null) {
            var strInput = obj.innerHTML.toUpperCase();
            var browserlang = '<%=Session["browserlang"]%>';
            if (strInput == "YOUR ACCOUNT PASSWORD HAS BEEN EXPIRED.") {
                if ((browserlang.indexOf("en-US") > -1) || (browserlang.indexOf("en") > -1))
                    obj.innerHTML = "Your account password has been expired.";
                if ((browserlang.indexOf("fr-CA") > -1) || (browserlang.indexOf("fr") > -1))
                    obj.innerHTML = "Le mot de passe de votre compte a expiré."; 
                if ((browserlang.indexOf("es-US") > -1) || (browserlang.indexOf("es") > -1))
                    obj.innerHTML = "Tu contraseña de la cuenta ha caducado.";
            }
         
            if (strInput == "OPERATION SUCCESSFUL.") {

                if ((browserlang.indexOf("en-US") > -1) || (browserlang.indexOf("en") > -1))
                    obj.innerHTML = "Operation Successful.";
                if ((browserlang.indexOf("fr-CA") > -1) || (browserlang.indexOf("fr") > -1))
                    obj.innerHTML = "Opération réalisée avec succès.";
                if ((browserlang.indexOf("es-US") > -1) || (browserlang.indexOf("es") > -1))
                    obj.innerHTML = "Operación Exitosa.";
            }
       if (((strInput.indexOf("SUCCESS") > -1) && !(strInput.indexOf("UNSUCCESS") > -1) && !(strInput.indexOf("ERROR") > -1))
        || (strInput.indexOf("QUITTÉ") > -1) || (strInput.indexOf("SATISFACT") > -1)) {
                obj.setAttribute("class", "lblMessage");
                obj.setAttribute("className", "lblMessage");
            }
            else {
                obj.setAttribute("class", "lblError");
                obj.setAttribute("className", "lblError");
            }
        }
       
</script> 
         
<%--code added for Soft Edge button--%>
<script type="text/javascript" src="inc/softedge.js"></script>
<%--FB 2500--%>
<%--<!-- #INCLUDE FILE="inc/mainbottom2.aspx" -->--%>

