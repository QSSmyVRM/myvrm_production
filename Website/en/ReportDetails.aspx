<%--ZD 100147 start--%>
<%--/* Copyright (C) 2015 myVRM - All Rights Reserved	
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.		
*
* You should have received a copy of the myVRM license with	
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/--%><%--ZD 100147 ZD 100886 End--%>
<%@ Page Language="C#" AutoEventWireup="true" Inherits="MyVRMNet.en_Report_Details" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<meta http-equiv="X-UA-Compatible" content="IE=8" />
<!-- FB 2050 -->
<%@ Register Assembly="DevExpress.XtraCharts.v10.2.Web, Version=10.2.11.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.XtraCharts.Web" TagPrefix="dxchartsui" %>
<%@ Register Assembly="DevExpress.XtraCharts.v10.2, Version=10.2.11.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.XtraCharts" TagPrefix="dxCh" %>
<%@ Register Assembly="DevExpress.Charts.v10.2.Core, Version=10.2.11.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Charts" TagPrefix="dxChartCore" %>
<%@ Register Assembly="DevExpress.Web.ASPxEditors.v10.2, Version=10.2.11.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dxAxEd" %>
<%@ Register Assembly="DevExpress.Web.v10.2, Version=10.2.11.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxSPt" %>
<%@ Register Assembly="DevExpress.Web.ASPxGridView.v10.2, Version=10.2.11.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dxwgv" %>
<%@ Register Assembly="DevExpress.Web.ASPxGridView.v10.2.Export, Version=10.2.11.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView.Export" TagPrefix="dx" %>
    <script language="javascript" type="text/javascript" src='script/lib.js'></script>

    <script type="text/javascript" src="script/cal-flat.js"></script>
    <script type="text/javascript" src="../<%=Session["language"] %>/lang/calendar-en.js"></script>
    <script type="text/javascript" src="script/calview.js"></script>
    <script type="text/javascript" src="script/calendar-setup.js"></script>
    <script type="text/javascript" src="script/calendar-flat-setup.js"></script>
<!-- #INCLUDE FILE="inc/maintopNET.aspx" -->
<%--ZD 101388 Commented--%>
<%--<!-- FB 2719 Starts -->
<% if (Session["isExpressUser"].ToString() == "1"){%>
<!-- #INCLUDE FILE="inc/maintopNETExp.aspx" -->
<%}else{%>
<!-- #INCLUDE FILE="inc/maintopNET.aspx" -->
<%}%>
<!-- FB 2719 Ends -->--%>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">


    
    <link rel="stylesheet" type="text/css" media="all" href="css/calendar-blue.css" />

    <script type="text/javascript" src="inc/functions.js"></script>

    <link href="Css/myVRMStyle.css" type="text/css" rel="stylesheet" />
    <title>Reports</title>
    <style type="text/css">
        .rowrap td
        {
            word-wrap: break-word;
        }
        .verticaltext
        {
            filter: progid:DXImageTransform.Microsoft.BasicImage(rotation=3);
        }
        /* Commented for FB 2050
        .splTab table
        {
        	width:750px;
        }
        */#txtRooms input /* FB 2050 */
        {
            width: 180px;
        }        
    </style>
    <style type="text/css">
        /* FB 2913 Starts */
        .verticaltextyellow
        {
            text-align:center;
            
            background-repeat:no-repeat;
            background-position:center;
        }
        .verticaltexts
        {
            text-align:center;
            background-repeat:no-repeat;
            background-position:center;
        }
        .verticaltextviolet1
        {
            text-align:center;
            background-repeat:no-repeat;
            background-position:center;
        }
        .verticaltextviolet2
        {
            text-align:center;
            background-repeat:no-repeat;
            background-position:center;
        }
        /* FB 2913 Ends */
        .horizontaltexts
        {
            font-weight: bold;
            font-size: 10pt;
            color: black;
            background-color: #C0C0C0;
            text-align: center;
            
        }
    </style>

    <script type="text/javascript">
    if (navigator.userAgent.indexOf('Trident') > -1) {
        document.onkeydown = function(evt) {
            fnOnKeyDown(evt);
        };
    }    
// FB 2050 Starts //FB 2343
    var normalScreen = '#divMonthlyReport{width:750px;}#divWeeklyReport{width:750px;}#divNoData{width:750px;}#divReportDetais{width:750px;}';
    var wideScreen = '#divMonthlyReport{width:1000px;}#divWeeklyReport{width:1000px;}#divNoData{width:1000px;}#divReportDetais{width:1000px;}';
    //ZD 100604 start
    var img = new Image();
    img.src = "../en/image/wait1.gif";
    //ZD 100604 End
    function addCss(cssCode)
    {
    var styleElement = document.createElement("style");
    styleElement.type = "text/css";
        if (styleElement.styleSheet)
        {
        styleElement.styleSheet.cssText = cssCode;
        }
        else
        {
        styleElement.appendChild(document.createTextNode(cssCode));
        }
    document.getElementsByTagName("head")[0].appendChild(styleElement);
    }    
    
    if(screen.width > 1024)
    {
        addCss(wideScreen);
    }
    else
    {
        addCss(normalScreen);
    }
    // FB 2050 Ends
    // FB 2501 starts
    function deleteApprover(id) 
    {
        eval("document.getElementById('hdnApprover" + (id) + "')").value = "";
        eval("document.getElementById('txtApprover" + (id) + "')").value = "";      
    }
    
    function getYourOwnEmailList(i) {
       
        url = "emaillist2main.aspx?t=e&frm=approverNET&fn=Setup&n=" + i;
        if (!window.winrtc) {
            winrtc = window.open(url, "", "width=920,height=470,top=0,left=0,resizable=yes,scrollbars=yes,status=no"); //FB 2735
            winrtc.focus();
        }
        else 
            if (!winrtc.closed) {    
            winrtc.close();
            winrtc = window.open(url, "", "width=920,height=470,top=0,left=0,resizable=yes,scrollbars=yes,status=no"); //FB 2735
            winrtc.focus();
        } else {
        winrtc = window.open(url, "", "width=920,height=470,top=0,left=0,resizable=yes,scrollbars=yes,status=no"); //FB 2735
            winrtc.focus();
        }
    }
    //FB 2501 ends
   

    function deleteVNOC() {
        document.getElementById('txtRVNOCOperator').value = "";
        document.getElementById('hdnVNOCOperator').value = "";
    }
    //FB 2670 Ends
    // FB 2866 start
    document.onclick = getClickId;
    function getClickId(e) {
        e = e || window.event;
        e = e.target || e.srcElement;
        var idlist = e.id
        idlist += e.parentNode.id
        idlist += e.parentNode.parentNode.id
        idlist += e.parentNode.parentNode.parentNode.id
        idlist += e.parentNode.parentNode.parentNode.parentNode.id
        idlist += e.parentNode.parentNode.parentNode.parentNode.parentNode.id
        if (idlist.indexOf("imgRoom") > -1)
            return false;
        if (idlist.indexOf('RoomDiv') == -1)
            document.getElementById('RoomDiv').style.display = 'none';
    }
    // FB 2866 End
    //ZD 100176 start
    function DataLoading(val) {
        if (val == "1")
            document.getElementById("dataLoadingDIV").style.display = 'block'; //ZD 100678
        else
            document.getElementById("dataLoadingDIV").style.display = 'none'; //ZD 100678
    }
    //ZD 100176  End  
    
    </script>

</head>
<body>
    <form id="frmReports" runat="server">
     <%--ZD 101022 start--%>
        <asp:ScriptManager ID="scpMgrUI" runat="server" EnableScriptLocalization="true" >
		    <Scripts>                
			    <asp:ScriptReference Path= "~/ResourceScript/StringResources.js" ResourceUICultures="<%$ Resources:WebResources, UICulture%>"  />
		    </Scripts>
	    </asp:ScriptManager> <%--ZD 101022 End--%>
    <div id="hideScreen" style="position:absolute; background-color:white; top:0; left:0; width:1500px; height:2000px; z-index:1000; display:block"></div><%--FB 2505--%>
    <input type="hidden" runat="server" id="hdnReportType" />
    <input type="hidden" runat="server" id="hdnInputType" />
    <input type="hidden" runat="server" id="hdnInputValue" />
    <input type="hidden" runat="server" id="hdnDateFrom" />
    <input type="hidden" runat="server" id="hdnDateTo" />
    <input type="hidden" name="hdnRoomIDs" id="hdnRoomIDs" runat="server" />
    <input type="hidden" id="hdnChartName" runat="server" />
    <input type="hidden" id="hdnValue" runat="server" />
    <input type="hidden" id="hdntempText" runat="server" />
    <input type="hidden" id="hdnChartPrint" runat="server" />
    <div>
        <table width="100%" border="0">
            <tr>
                <td align="center" colspan="2"><h3><asp:Literal Text="<%$ Resources:WebResources, ReportDetails_Reports%>" runat="server"></asp:Literal></h3><br /><!-- FB 2570 -->
                    <asp:Label id="lblErrLabel" runat="server" cssclass="lblError"></asp:Label>
                </td>
            </tr>
            <tr>
                <td align="right" colspan="2">
                 <div id="dataLoadingDIV" name="dataLoadingDIV" align="center" style="display:none">
                     <img border='0' src='image/wait1.gif' alt='Loading..' />
                </div><%--ZD 100678--%> 
                    <table width="12%" border="0" style="left: 0;" id="tbleBtns"> <%--FB 2928--%>
                        <tr>
                            <td style="width: 8%;" align="left">
                                <asp:ImageButton ID="ImgPrinter" ValidationGroup="group" OnClientClick='Javscript:return setPrint();' runat="server" AlternateText="Print"
                                    src="image/print.gif" alt="Print Report" Style="cursor: hand; vertical-align: middle;"  ToolTip="<%$ Resources:WebResources, ManageConference_Print%>"/><%--FB 2501--%><%-- FB 3034--%><%--ZD 100419--%> <%--ZD 100429--%>
                            </td>
                            <td style="width: 8%" align="left">
                                <asp:ImageButton ID="btnPDF" ValidationGroup="group" OnClick="ExportPDF" src="image/adobe.gif" runat="server"  AlternateText="Export to PDF"
                                    OnClientClick="document.getElementById('hdnChartPrint').value = ''" Style="vertical-align: middle;"
                                    ToolTip="<%$ Resources:WebResources, ManageConference_btnPDF%>" /><%--ZD 100419--%><%--ZD 100429--%>
                            </td>
                            <td style="width: 8%" align="left">
                                <asp:ImageButton ID="btnExcel" ValidationGroup="group" OnClick="ExportExcel" src="image/excel.gif" runat="server" AlternateText="Export to Excel"
                                    OnClientClick="document.getElementById('hdnChartPrint').value = ''" Style="vertical-align: middle;"
                                    ToolTip="<%$ Resources:WebResources, ExporttoExcel%>" /><%--ZD 100419--%><%--ZD 100429--%>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr><%--FB 2593--%>
                <td style="width: 22%" valign="top" onclick='fnAssignValue("<%=cblRoom.ClientID%>")'><%--FB 2501--%><%--FB 2670--%>
                    <div id="InputParamDiv" style="background-color: #f3f3f3; border-color: Blue; border-width: 1px;
                        border-style: Solid; height: 900px;overflow:auto; text-align: left;"> <%--FB 2501--%>
                        <%-- FB 2050 --%>
                        <table width="100%" id="tblControls" style="padding: 0px" border="0" cellspacing="0">
                            <tr>
                                <td class="tableHeader">
                                    <b class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, ReportDetails_Categories%>" runat="server"></asp:Literal></b>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <table width="100%" style="padding: 0px" border="0" cellpadding="5">
                                        <tr>
                                            <td>
                                            <b class="blackblodtext"><asp:Literal ID="Literal1" Text="<%$ Resources:WebResources, MasterChildReport_ReportType%>" runat="server"></asp:Literal></b> <%--ZD 100422--%>
                                                <dxAxEd:ASPxComboBox ID="ReportsList" runat="server" SelectedIndex="0" Width="90%"
                                                    CssClass="altText" ClientInstanceName="ReportsList">
                                                    <ClientSideEvents TextChanged="function(s, e) { return fnShowMenu(); }" />
                                                    <Items>
                                                        <dxAxEd:ListEditItem Text="<%$ Resources:WebResources, ReportDetails_ConferenceSche%>" Value="1" />
                                                        <dxAxEd:ListEditItem Text="<%$ Resources:WebResources, ReportDetails_UserReports%>" Value="2" />
                                                        <dxAxEd:ListEditItem Text="<%$ Resources:WebResources, ReportDetails_UsageReports%>" Value="3" />
                                                        <dxAxEd:ListEditItem Text="<%$ Resources:WebResources, GraphicalReports%>" Value="4" />
                                                        <dxAxEd:ListEditItem Text="<%$ Resources:WebResources, PersonalReports%>" Value="5" />
                                                        <%--<dxAxEd:ListEditItem Text="<%$ Resources:WebResources, UtilizationReport_UtilizationRep%>" Value="8" />--%><%-- ZD 102909--%>
                                                    </Items>
                                                </dxAxEd:ASPxComboBox>
                                            </td>
                                        </tr>
                                        <tr id="ConfScheRptCell" style="display: none;">
                                            <td>
                                                <b class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, ReportDetails_ConferenceSche%>" runat="server"></asp:Literal></b>
                                                <dxAxEd:ASPxComboBox ID="ConfScheRptDivList" runat="server" SelectedIndex="0" Width="58%"
                                                    CssClass="altSelectFormat" ClientInstanceName="ConfScheRptDivList">
                                                    <ClientSideEvents TextChanged="function(s, e) { return fnShowMenu(); }" />
                                                    <Items>
                                                        <dxAxEd:ListEditItem Text="<%$ Resources:WebResources, DailySchedule%>" Value="2" />
                                                        <dxAxEd:ListEditItem Text="<%$ Resources:WebResources, CalendarReport%>" Value="1" />
                                                        <dxAxEd:ListEditItem Text="<%$ Resources:WebResources, PRIAllocation%>" Value="3" />
                                                        <dxAxEd:ListEditItem Text="<%$ Resources:WebResources, ResourceAllocation%>" Value="4" />
                                                        <dxAxEd:ListEditItem Text="<%$ Resources:WebResources, ConferenceReports%>" Value ="5" /><%--FB 2501--%>
                                                     </Items>
                                                </dxAxEd:ASPxComboBox>
                                            </td>
                                        </tr>
                                        <tr id="UserReportsCell" style="display: none;" runat="server">
                                            <td>
                                                <b class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, ReportDetails_UserReports%>" runat="server"></asp:Literal></b>
                                                <dxAxEd:ASPxComboBox ID="lstUserReports" runat="server" SelectedIndex="0" Width="130px"
                                                    CssClass="altSelectFormat">
                                                    <Items>
                                                        <dxAxEd:ListEditItem Text="<%$ Resources:WebResources, ContactList%>" Value="1" />
                                                    </Items>
                                                </dxAxEd:ASPxComboBox>
                                                <%-- FB 2050 --%>
                                            </td>
                                        </tr>
                                        <tr id="UsageReportCell" style="display: none;">
                                            <td>
                                                <b class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, ReportDetails_UsageReports%>" runat="server"></asp:Literal></b>
                                                <dxAxEd:ASPxComboBox ID="lstUsageReports" runat="server" SelectedIndex="0" Width="145px"
                                                    CssClass="altSelectFormat" ClientInstanceName="lstUsageReports">
                                                    <ClientSideEvents TextChanged="function(s, e) { return fnShowMenu(); }" />
                                                </dxAxEd:ASPxComboBox>
                                                <%-- FB 2050 FB 2343 --%>
                                            </td>
                                        </tr>
                                        <tr id="tdAllType" style="display: none;">
                                            <td>
                                                <b class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, ReportDetails_Category%>" runat="server"></asp:Literal></b>
                                                <dxAxEd:ASPxComboBox ID="DrpAllType" runat="server" SelectedIndex="0" Width="130px"
                                                    CssClass="altSelectFormat" ClientInstanceName="DrpAllType">
                                                    <ClientSideEvents TextChanged="function(s, e) { return fnShowMenu(); }" />
                                                </dxAxEd:ASPxComboBox>
                                                <%-- FB 2050 --%>
                                            </td>
                                        </tr>
                                        <tr id="tdConfType" style="display: none;">
                                            <td>
                                                <b class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, ReportDetails_ConferenceType%>" runat="server"></asp:Literal></b>
                                                <dxAxEd:ASPxComboBox ID="lstConfType" runat="server" SelectedIndex="0" Width="65%"
                                                    CssClass="altSelectFormat">
                                                    <Items>
                                                        <dxAxEd:ListEditItem Text="<%$ Resources:WebResources, All%>" Value="1" />
                                                        <dxAxEd:ListEditItem Text="<%$ Resources:WebResources, AudioConferencesOnly%>" Value="6" />
                                                        <dxAxEd:ListEditItem Text="<%$ Resources:WebResources, VideoConferencesOnly%>" Value="2" />
                                                        <dxAxEd:ListEditItem Text="<%$ Resources:WebResources, PointtoPointOnly%>" Value="4" />
                                                        <dxAxEd:ListEditItem Text="<%$ Resources:WebResources, RoomConferencesOnly%>" Value="7" />
                                                    </Items>
                                                </dxAxEd:ASPxComboBox>
                                            </td>
                                        </tr>
                                        <tr id="tdDaily" style="display: none;">
                                            <td>
                                                <b class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, ReportDetails_DailyMonthly%>" runat="server"></asp:Literal></b>
                                                <dxAxEd:ASPxComboBox ID="lstDailyMonthly" runat="server" SelectedIndex="0" Width="55%"
                                                    CssClass="altSelectFormat" ClientInstanceName="lstDailyMonthly">
                                                    <ClientSideEvents TextChanged="function(s, e) { return fnShowMenu(); }" />
                                                    <Items>
                                                        <dxAxEd:ListEditItem Text="<%$ Resources:WebResources, Daily%>" Value="1" />
                                                        <dxAxEd:ListEditItem Text="<%$ Resources:WebResources, Monthly%>" Value="2" />
                                                    </Items>
                                                </dxAxEd:ASPxComboBox>
                                            </td>
                                        </tr>
                                        <tr id="tdStatus" style="display: none;">
                                            <td>
                                                <b class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, ReportDetails_Status%>" runat="server"></asp:Literal></b>
                                                <dxAxEd:ASPxComboBox ID="lstStatusType" runat="server" SelectedIndex="0" Width="55%"
                                                    CssClass="altSelectFormat" ClientInstanceName="lstStatusType">
                                                    <ClientSideEvents TextChanged="function(s, e) { return fnShowMenu(); }" />
                                                    <Items>
                                                        <dxAxEd:ListEditItem Text="<%$ Resources:WebResources, Ongoing%>" Value="2" />
                                                        <dxAxEd:ListEditItem Text="<%$ Resources:WebResources, Pending%>" Value="3" />
                                                        <dxAxEd:ListEditItem Text="<%$ Resources:WebResources, OnMCU%>" Value="8" /> <%--ZD 100036--%>
                                                        <dxAxEd:ListEditItem Text="<%$ Resources:WebResources, Reservation%>" Value="4" />
                                                        <dxAxEd:ListEditItem Text="<%$ Resources:WebResources, Terminated%>" Value="5" />
                                                        <dxAxEd:ListEditItem Text="<%$ Resources:WebResources, Deleted%>" Value="6" />
                                                        <dxAxEd:ListEditItem Text="<%$ Resources:WebResources, Public%>" Value="7" />
                                                    </Items>
                                                </dxAxEd:ASPxComboBox>
                                            </td>
                                        </tr>
                                        <tr id="tdOrg" style="display: none">
                                            <td>
                                                <b class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, ReportDetails_SelectOrganiza%>" runat="server"></asp:Literal></b>
                                                <dxAxEd:ASPxComboBox ID="lstOrg" runat="server" SelectedIndex="0" Width="55%" CssClass="altSelectFormat"
                                                    ValueField="OrgID" TextField="OrganizationName">
                                                </dxAxEd:ASPxComboBox>
                                            </td>
                                        </tr>
                                        <tr id="tdResourse" style="display: none;">
                                            <td><%--ZD 102043--%>
                                                <b class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, ReportDetails_ResourceReport%>" runat="server"></asp:Literal></b>
                                                <dxAxEd:ASPxComboBox ID="lstResourseType" runat="server" SelectedIndex="0" Width="95%"
                                                    CssClass="altSelectFormat" ClientInstanceName="lstResourseType">
                                                    <ClientSideEvents TextChanged="function(s, e) { return fnShowMenu(); }" />
                                                    <Items>
                                                        <dxAxEd:ListEditItem Text="<%$ Resources:WebResources, UserReport%>" Value="1" />
                                                        <dxAxEd:ListEditItem Text="<%$ Resources:WebResources, RoomReport%>" Value="2" />
                                                        <dxAxEd:ListEditItem Text="<%$ Resources:WebResources, EndpointReport%>" Value="3" />
                                                        <dxAxEd:ListEditItem Text="<%$ Resources:WebResources, MCUReport%>" Value="4" />
                                                        <dxAxEd:ListEditItem Text="<%$ Resources:WebResources, DailyScheduleReport%>" Value="5" />
                                                        <dxAxEd:ListEditItem Text="<%$ Resources:WebResources, CDRReports%>" Value="6" /><%--FB 2501 CDR Reports--%>
                                                    </Items>
                                                </dxAxEd:ASPxComboBox>
                                            </td>
                                        </tr>
                                        <tr id="tdUser" style="display: none;">
                                            <td><%--ZD 102043--%>
                                                <b class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, ReportDetails_UserReports%>" runat="server"></asp:Literal></b>
                                                <dxAxEd:ASPxComboBox ID="lstUserType" runat="server" SelectedIndex="0" Width="75%"
                                                    CssClass="altSelectFormat" ClientInstanceName="lstUserType">
                                                    <ClientSideEvents TextChanged="function(s, e) { return fnShowMenu(); }" />
                                                    <Items>
                                                        <dxAxEd:ListEditItem Text="<%$ Resources:WebResources, All%>" Value="1" />
                                                        <dxAxEd:ListEditItem Text="<%$ Resources:WebResources, Active%>" Value="2" />
                                                        <dxAxEd:ListEditItem Text="<%$ Resources:WebResources, Inactive%>" Value="3" />
                                                        <dxAxEd:ListEditItem Text="<%$ Resources:WebResources, Blocked%>" Value="4" />
                                                        <dxAxEd:ListEditItem Text="<%$ Resources:WebResources, ScheduledUsage%>" Value="5" /> <%--ZD 102043--%>
                                                    </Items>
                                                </dxAxEd:ASPxComboBox>
                                            </td>
                                        </tr>
                                        <tr id="tdRoom" style="display: none;">
                                            <td>
                                                <b class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, ReportDetails_RoomReports%>" runat="server"></asp:Literal></b>
                                                <dxAxEd:ASPxComboBox ID="lstRoomType" runat="server" SelectedIndex="0" Width="55%"
                                                    CssClass="altSelectFormat" ClientInstanceName="lstRoomType">
                                                    <ClientSideEvents TextChanged="function(s, e) { return fnShowMenu(); }" />
                                                    <Items>
                                                        <dxAxEd:ListEditItem Text="<%$ Resources:WebResources, Active%>" Value="1" />
                                                        <dxAxEd:ListEditItem Text="<%$ Resources:WebResources, Inactive%>" Value="2" /> <%--ZD 102938--%>
                                                    </Items>
                                                </dxAxEd:ASPxComboBox>
                                            </td>
                                        </tr>
                                        <tr id="tdRoomConf" style="display: none;">
                                            <td>
                                                <b class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, ReportDetails_DailySchedule%>" runat="server"></asp:Literal></b>
                                                <dxAxEd:ASPxComboBox ID="lstConfRoomRpt" runat="server" SelectedIndex="0" Width="55%"
                                                    CssClass="altSelectFormat" ClientInstanceName="lstConfRoomRpt">
                                                    <ClientSideEvents TextChanged="function(s, e) { return fnShowMenu(); }" />
                                                    <Items>
                                                        <dxAxEd:ListEditItem Text="<%$ Resources:WebResources, UsagebyRoomScheduled%>" Value="1" />
                                                        <dxAxEd:ListEditItem Text="<%$ Resources:WebResources, UsagebyRoomActual%>" Value="2" />
                                                        <dxAxEd:ListEditItem Text="<%$ Resources:WebResources, UsagebyRoomConference%>" Value="3" />
                                                    </Items>
                                                </dxAxEd:ASPxComboBox>
                                            </td>
                                        </tr>
                                        <%--ZD ZD 102468 Start--%>
                                        <tr id="trCustomOptions" style="display: none;">
                                            <td>
                                                <b class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, CustomOptions%>" runat="server"></asp:Literal></b>
                                                <dxAxEd:ASPxComboBox ID="lstCustomOptType" runat="server" SelectedIndex="0" Width="75%"
                                                    CssClass="altSelectFormat" ClientInstanceName="lstCustomOptType">
                                                    <ClientSideEvents TextChanged="function(s, e) { return fnShowMenu(); }" />
                                                </dxAxEd:ASPxComboBox>
                                            </td>
                                        </tr>
                                        <%--ZD ZD 102468 End--%>
                                    </table>
                                </td>
                            </tr>
                            <tr id="ParamRow" style="align:left;display:block" > <%--ZD 100393--%>
                                <td class="tableHeader" style="width: 325px; align:left"> <%--ZD 100393--%>
                                    <%-- FB 2050 --%>
                                    <b class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, ReportDetails_Parameters%>" runat="server"></asp:Literal></b>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <table width="100%" border="0" cellpadding="3">
                                        <tr id="tdLoc" style="display: none">
                                            <td>
                                                <b class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, ReportDetails_Locations%>" runat="server"></asp:Literal></b>
                                                <dxAxEd:ASPxComboBox id="DrpLocations" runat="server" selectedindex="0" width="100px" cssclass="altSelectFormat" clientinstancename="DrpLocations">
                                                    <%-- FB 2050 --%>
                                                    <ClientSideEvents TextChanged="function(s, e) { return fnShow(); }" />
                                                    <Items>
                                                        <dxAxEd:ListEditItem Text="<%$ Resources:WebResources, RoomName%>" Value="1" />
                                                        <dxAxEd:ListEditItem Text="<%$ Resources:WebResources, CountryState%>" Value="2" />
                                                        <dxAxEd:ListEditItem Text="<%$ Resources:WebResources, ZipCode%>" Value="3" />
                                                    </Items>
                                                </dxAxEd:ASPxComboBox>
                                            </td>
                                        </tr>
                                        <tr id="tdMCU" style="display: none">
                                            <td>
                                                <b class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, ReportDetails_MCUReports%>" runat="server"></asp:Literal></b> <%--FB 2932--%>
                                                <dxAxEd:ASPxComboBox ID="DrpMCU" runat="server" SelectedIndex="0" Width="100%" CssClass="altSelectFormat"
                                                    ClientInstanceName="DrpMCU">
                                                    <ClientSideEvents TextChanged="function(s, e) { return fnShow(); }" />
                                                    <Items>
                                                        <dxAxEd:ListEditItem Text="<%$ Resources:WebResources, All%>" Value="1" />
                                                        <dxAxEd:ListEditItem Text="<%$ Resources:WebResources, CustomSelectone%>" Value="2" />
                                                    </Items>
                                                </dxAxEd:ASPxComboBox>
                                            </td>
                                        </tr>
                                        <tr id="tdCountry" style="display: none">
                                            <td>
                                                <b class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, ReportDetails_CountryState%>" runat="server"></asp:Literal></b>
                                                <dxAxEd:ASPxComboBox ID="lstCountries" runat="server" SelectedIndex="0" Width="100px"
                                                    CssClass="altSelectFormat" ValueField="ID" TextField="Name">
                                                    <%-- FB 2050 --%>
                                                </dxAxEd:ASPxComboBox>
                                                <dxAxEd:ASPxComboBox ID="lstStates" runat="server" SelectedIndex="0" Width="100px"
                                                    CssClass="altSelectFormat" ValueField="ID" TextField="Code">
                                                    <%-- FB 2050 --%>
                                                </dxAxEd:ASPxComboBox>
                                            </td>
                                        </tr>
                                        <tr id="tdZipCode" style="display: none">
                                            <td>
                                                <b class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, ReportDetails_ZipCode%>" runat="server"></asp:Literal></b>
                                                <dxAxEd:ASPxTextBox ID="txtZipCode" Width="50" runat="server" CssClass="altText">
                                                </dxAxEd:ASPxTextBox>
                                                <asp:RegularExpressionValidator ID="regZipCode" ControlToValidate="txtZipCode" Display="dynamic"
                                                    runat="server" SetFocusOnError="true" ErrorMessage="<%$ Resources:WebResources, InvalidCharacters13%>"
                                                    ValidationGroup="Submit" ValidationExpression="^(a-z|A-Z|0-9)*[^\\<>^+;?|!`\[\]{}\x22;=:@#$%&'~]*$"></asp:RegularExpressionValidator><%--FB 2222--%>
                                            </td>
                                        </tr>
                                          <%--  FB 2501 starts--%>
                                           <tr id="ConferenceIDRow" style="display:none">
                                         <td nowrap="nowrap" >
                                         <span class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, ReportDetails_ConferenceID%>" runat="server"></asp:Literal></span><br />
                                                     <asp:TextBox ID="txtConferenceUniqueID" runat="server" CssClass="altText" Enabled="true" MaxLength="9"></asp:TextBox>                                                                                  
                                                     <asp:Button runat="server" Text="<%$ Resources:WebResources, ReportDetails_btnConferenceUniqueID%>" ID="btnConferenceUniqueID" OnClick ="ViewReport" CommandName ="ConfidSubmit" CssClass="altMedium0BlueButtonFormat" OnClientClick="javascript:fnClearValues();" Width="80px" /> <%--FB 2800--%>
                                                     <asp:RequiredFieldValidator ID="req1" runat="server" ControlToValidate="txtConferenceUniqueID" ErrorMessage="<%$ Resources:WebResources, Required%>" Display="dynamic"></asp:RequiredFieldValidator>
                                                     <asp:RegularExpressionValidator ID="reg1" runat="server" ControlToValidate="txtConferenceUniqueID" ErrorMessage="<%$ Resources:WebResources, Numericonly%>" ValidationExpression="\d+" Display="dynamic"></asp:RegularExpressionValidator>
                                                    
                                         </td>
                                         </tr>
                                        
                                          <tr id="DateSelRow">
                                            <td>
                                                <table border="0" width="80%" style="padding: 0px">
                                                    <tr>
                                                        <td colspan="2" nowrap="nowrap" id ="tdDateTime1" style="display:none;">
                                                            <b class="blackblodtext"> <asp:Literal Text="<%$ Resources:WebResources, ReportDetails_FromDateTime%>" runat="server"></asp:Literal></b>
                                                        </td>
                                                         <td colspan="2"  id ="tdDateTime2" style="display:none;">
                                                            <b class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, ReportDetails_FromDate%>" runat="server"></asp:Literal></b>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td width="45%" nowrap="nowrap">
                                                            <asp:TextBox ID="txtStartDate" CssClass="altText" MaxLength="20" runat="server" Width="70"></asp:TextBox>
                                                            <img src="image/calendar.gif" border="0" width="20" height="20" id="cal_triggerd"
                                                                style="cursor: pointer; vertical-align: middle" title="<asp:Literal Text='<%$ Resources:WebResources, DateSelector%>' runat='server'></asp:Literal>" onblur="javascript:ChangeStartDate(0)"
                                                                onclick="return showCalendar('<%=txtStartDate.ClientID%>', 'cal_triggerd', 0, '<%=format%>');"
                                                                alt="Date selector" /><%--ZD 100419--%>
                                                        </td>
                                                        <td  id="startTime" style="display:none;"><%-- FB 2501 --%>
                                                            <dxAxEd:ASPxComboBox ID="CmbStrtTime" runat="server" SelectedIndex="0" Width="80"
                                                                CssClass="altSelectFormat">
                                                            </dxAxEd:ASPxComboBox>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                     <td colspan="2" nowrap="nowrap" id ="tdDateTimeEnd1" style="display:none;"><%-- FB 2501 --%>
                                                            <b class="blackblodtext"> <asp:Literal Text="<%$ Resources:WebResources, ReportDetails_ToDateTime%>" runat="server"></asp:Literal></b>
                                                        </td>
                                                         <td colspan="2"  id ="tdDateTimeEnd2" style="display:none;">
                                                            <b class="blackblodtext"> <asp:Literal Text="<%$ Resources:WebResources, ReportDetails_ToDate%>" runat="server"></asp:Literal></b>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <asp:TextBox ID="txtEndDate" CssClass="altText" MaxLength="20" runat="server" Width="70"></asp:TextBox>
                                                            <img src="image/calendar.gif" border="0" width="20" height="20" id="cal_trigger1" 
                                                                style="cursor: pointer; vertical-align: middle" title="<asp:Literal Text='<%$ Resources:WebResources, DateSelector%>' runat='server'></asp:Literal>" onblur="javascript:ChangeEndDate(0)"
                                                                onclick="return showCalendar('<%=txtEndDate.ClientID %>', 'cal_trigger1', 0, '<%=format%>');"
                                                                alt="Date selector"/><%--ZD 100419--%>
                                                        </td>
                                                        <td id="endTime" style="display:none;"><%-- FB 2501 --%>
                                                            <dxAxEd:ASPxComboBox ID="CmbEndTime" runat="server" SelectedIndex="0" Width="80"
                                                                CssClass="altSelectFormat">
                                                            </dxAxEd:ASPxComboBox>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr id="trSelectDate">
                                            <%--FB 2155--%>
                                            <td>
                                                <table border="0" width="100%" style="padding: 0px">
                                                    <tr>
                                                        <td colspan="2">
                                                            <b class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, ReportDetails_SelectDate%>" runat="server"></asp:Literal></b>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="2">
                                                            <asp:TextBox ID="txtSelectedDate" CssClass="altText" MaxLength="20" runat="server"
                                                                Width="80"></asp:TextBox>
                                                            <img src="image/calendar.gif" border="0" width="20" height="20" id="cal_trigger2"
                                                                style="cursor: pointer; vertical-align: middle" title="<asp:Literal Text='<%$ Resources:WebResources, DateSelector%>' runat='server'></asp:Literal>" onclick="return showCalendar('<%=txtSelectedDate.ClientID%>', 'cal_trigger2', 0, '<%=format%>');"
                                                                alt="Date selector" /><%--ZD 100419--%>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr id="QueryTypeRow" style="display:none">
                                        <td>
                                        <span class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, ReportDetails_OperatorsForB%>" runat="server"></asp:Literal></span><br />
                                        <dxAxEd:ASPxComboBox ID="QueryType" runat="server" SelectedIndex="0" Width="152" CssClass="altSelectFormat">
                                            <Items>                                              
                                                <dxAxEd:ListEditItem Value="AND" Text="<%$ Resources:WebResources, AND%>"></dxAxEd:ListEditItem>
                                                <dxAxEd:ListEditItem Value="OR" Text="<%$ Resources:WebResources, OR%>"></dxAxEd:ListEditItem>                                                
                                            </Items>  
                                        </dxAxEd:ASPxComboBox>
                                        </td>
                                        </tr>
                                         <tr id="ConferenceTitleRow" style="display:none">
                                             <td>
                                                  <span class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, ReportDetails_ConferenceTitl%>" runat="server"></asp:Literal></span><br />
                                                      <asp:TextBox ID="ConferenceName" runat="server" CssClass="altText" Enabled="true" MaxLength="256"></asp:TextBox>                                                      
                                                      <asp:RegularExpressionValidator ID="regConfName" ControlToValidate="ConferenceName" ValidationGroup="btnviewRep" Display="dynamic" runat="server" ErrorMessage="<%$ Resources:WebResources, InvalidCharacters12%>" ValidationExpression="^[^<>&]*$"></asp:RegularExpressionValidator>
                                             </td>
                                          </tr>
                                        
                                         <tr id="ConferenceHostRow" style="display:none">
                                          
                                           <td>
                                            <span class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, ReportDetails_Host%>" runat="server"></asp:Literal></span><br />
                                               <asp:TextBox ID="txtApprover4" runat="server"  CssClass="altText" Enabled="false"></asp:TextBox>
                                                 &nbsp;<img id="Img8" onclick="javascript:getYourOwnEmailList(3)" src="image/edit.gif" style="cursor:pointer;" title="<%$ Resources:WebResources, AddressBook%>" runat="server" alt="Edit" />  <%--FB 2798--%>  <%--ZD 100419--%>
                                                  <a href="javascript: deleteApprover(4);" onmouseover="window.status='';return true;">
                                                <img border="0" src="image/btn_delete.gif" runat="server" alt="delete" width="16" height="16"  style="cursor:pointer;" title="<%$ Resources:WebResources, Delete%>"></a> <%--FB 2798--%>
                                                <asp:TextBox ID="hdnApprover4" runat="server" BackColor="Transparent" BorderColor="White"
                                                  BorderStyle="None" Width="0px" ForeColor="Black" style="display:none"></asp:TextBox>
                                            </td>
                                         </tr>
                                          <tr id="ConferenceRequestorRow" style="display:none">
                                           <td>
                                            <span class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, ReportDetails_Requestor%>" runat="server"></asp:Literal></span><br />
                                               <asp:TextBox ID="txtApprover6" runat="server" CssClass="altText" Enabled="false"></asp:TextBox>                                                 &nbsp;<img id="Img1" onclick="javascript:getYourOwnEmailList(5)" src="image/edit.gif"  style="cursor:pointer;" 
                                               runat="server" title="<%$ Resources:WebResources, AddressBook%>" alt="Edit" />  <%--FB 2798 --%> <%--ZD 100419--%>                              
                                               <a href="javascript: deleteApprover(6);" onmouseover="window.status='';return true;">
                                                <img border="0" src="image/btn_delete.gif" runat="server" alt="delete" width="16" height="16" style="cursor:pointer;" title="<%$ Resources:WebResources, Delete%>"></a> <%--FB 2798--%>
                                                <asp:TextBox ID="hdnApprover6" runat="server" BackColor="Transparent" BorderColor="White"
                                                  BorderStyle="None" Width="0px" ForeColor="Black" style="display:none"></asp:TextBox>
                                           </td>
                                         </tr>
                                         <tr id="OperatorRow" style="display:none"><%--FB 2670--%>
                                          
                                           <td>
                                            <span class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, ReportDetails_VNOCOperator%>" runat="server"></asp:Literal></span><br />
                                               <asp:TextBox ID="txtRVNOCOperator" runat="server" CssClass="altText" Enabled="false"></asp:TextBox>
                                                &nbsp;<img id="Img2" runat="server" onclick="javascript:getVNOCEmailList()" src="../image/VNOCEdit.gif" title="<%$ Resources:WebResources, SelectVNOCOperator%>" alt="Edit" /><%--FB 2670 FB 2783--%>   <%--ZD 100419--%> <%--ZD 100420--%>                                           
                                                <a href="javascript: deleteVNOC();" onmouseover="window.status='';return true;">
                                                <img border="0" src="image/btn_delete.gif" runat="server" alt="delete" width="16" height="16" style="cursor:pointer;" title="<%$ Resources:WebResources, Delete%>"></a> <%--FB 2798--%>
                                                <asp:TextBox ID="hdnVNOCOperator" runat="server" BackColor="Transparent" BorderColor="White"
                                                  BorderStyle="None" Width="0px" ForeColor="Black" style="display:none"></asp:TextBox>
                                            </td>
                                         </tr>
                                         <tr id="CallurlRow" style="display:none">
                                         <td>
                                             <span class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, ReportDetails_CallURI%>" runat="server"></asp:Literal></span><br />
                                             <asp:TextBox runat="server" CssClass="alttext" ID="CallURL" Width="120px"></asp:TextBox>
                                             <asp:RegularExpressionValidator ID="regCallURL" ControlToValidate="CallURL" ValidationGroup="btnviewRep" Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="<%$ Resources:WebResources, InvalidCharacters21%>" 
                                                    ValidationExpression="^(a-z|A-Z|0-9)*[^\\/<>+?|`\[\]{}\=$%&()~]*$"></asp:RegularExpressionValidator>                                            
                                         </td>
                                         </tr>
                                          <tr id="TimezoneRow" style="display:none">
                                          <td>
                                            <span class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, ReportDetails_TimeZone%>" runat="server"></asp:Literal></span><br />                                                
                                                <dxAxEd:ASPxComboBox ID="lstConferenceTZ" runat="server" SelectedIndex="0" Width="205" CssClass="altSelectFormat" ValueField="timezoneID" TextField="timezoneName">   
                                                </dxAxEd:ASPxComboBox>
                                            </td>
                                        </tr>
                                        <tr id="tdBridge" style="display: none">
                                            <td>
                                                <b class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, ReportDetails_MCU%>" runat="server"></asp:Literal></b>
                                                <dxAxEd:ASPxComboBox ID="lstBridges" runat="server" SelectedIndex="0" Width="205"   
                                                    CssClass="altSelectFormat" ValueField="BridgeID" TextField="BridgeName">    <%--FB 2501--%>
                                                </dxAxEd:ASPxComboBox>
                                            </td>
                                            
                                                                                                  
                                          </tr>
                                          <tr id="EndpointRow" style="display: none">
                                           <td>
                                                <b class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, ReportDetails_Endpoint%>" runat="server"></asp:Literal></b>
                                                <dxAxEd:ASPxComboBox ID="lstEndpoint" runat="server" SelectedIndex="0" Width="205"   
                                                    CssClass="altSelectFormat" ValueField="EndpointID" TextField="EndpointName">   
                                                </dxAxEd:ASPxComboBox>
                                            </td>
                                          </tr>
                                        <%--FB 2501 ends--%>
                                       
                                        <tr id="RoomRow" style="display: none">
                                            <%--FB 2289--%>
                                            <td nowrap>
                                                <span class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, ReportDetails_Room%>" runat="server"></asp:Literal></span><br />
                                                <asp:TextBox ID="txtRooms" runat="server" Width="180" CssClass="altText" Enabled="false"/><%--FB 2501--%>
                                                <img id="imgRoom" src="Image/DDImage.gif" onclick="fnShowRooms()" alt="DDImage" style="cursor: hand; 
                                                    vertical-align: top;" /><%--ZD 100419--%>
                                                <br />
                                                <div id="RoomDiv" class="RoomDiv" runat="server" style="display: none; width: 205px;">
                                                    &nbsp;<asp:CheckBox ID="chkSelectall" runat="server" Text="<%$ Resources:WebResources, ReportDetails_chkSelectall%>" />
                                                    <asp:CheckBoxList ID="cblRoom" runat="server" DataTextField="roomName" DataValueField="roomID">
                                                    </asp:CheckBoxList>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr id="TimeRangeCell" style="display: none;">
                                            <td>
                                                <b><asp:Literal Text="<%$ Resources:WebResources, ReportDetails_TimeRange%>" runat="server"></asp:Literal></b>
                                                <dxAxEd:ASPxComboBox ID="lstReport" runat="server" onclick="javascript:return fnShow();"
                                                    CssClass="altSelectFormat" Width="100px" ClientInstanceName="lstReport" SelectedIndex="0">
                                                    <%-- FB 2050 --%>
                                                    <ClientSideEvents TextChanged="function(s, e) { return fnShow(); }" />
                                                    <Items>
                                                        <dxAxEd:ListEditItem Value="1" Text="<%$ Resources:WebResources, Yesterday%>"></dxAxEd:ListEditItem>
                                                        <dxAxEd:ListEditItem Value="2" Text="<%$ Resources:WebResources, Lastweek%>"></dxAxEd:ListEditItem>
                                                        <dxAxEd:ListEditItem Value="3" Text="<%$ Resources:WebResources, Lastmonth%>"></dxAxEd:ListEditItem>
                                                        <dxAxEd:ListEditItem Value="4" Text="<%$ Resources:WebResources, Thisweek%>"></dxAxEd:ListEditItem>
                                                        <dxAxEd:ListEditItem Value="5" Text="<%$ Resources:WebResources, YeartoDate%>"></dxAxEd:ListEditItem>
                                                        <dxAxEd:ListEditItem Value="6" Text="<%$ Resources:WebResources, CustomDate%>"></dxAxEd:ListEditItem>
                                                    </Items>
                                                </dxAxEd:ASPxComboBox>
                                            </td>
                                        </tr>
                                        <tr id="tdCusFrmDte" style="display: none">
                                            <td>
                                                <b><asp:Literal Text="<%$ Resources:WebResources, ReportDetails_FromDate%>" runat="server"></asp:Literal></b><br />
                                                <asp:TextBox ID="txtCusDateFrm" runat="server" CssClass="altText" onblur="javascript:ChangeEndDate(0)"
                                                    Width="80PX"></asp:TextBox>
                                                <asp:RequiredFieldValidator ID="reqCusFrom" runat="server" ControlToValidate="txtCusDateFrm"
                                                    Display="dynamic" CssClass="lblError" SetFocusOnError="true" Text="" ValidationGroup="DateSubmit"></asp:RequiredFieldValidator>
                                                <img src="image/calendar.gif" style="width: 20; height: 20; vertical-align: bottom;
                                                    cursor: pointer;" alt="Date selector" id="cal_triggerFrm" title="<asp:Literal Text='<%$ Resources:WebResources, DateSelector%>' runat='server'></asp:Literal>" onblur="javascript:ChangeEndDate(0)"
                                                    onclick="return showCalendar('txtCusDateFrm', 'cal_triggerFrm', 0, '<%=format%>');" /><%--ZD 100419--%>
                                            </td>
                                        </tr>
                                        <tr id="tdCusToDte" style="display: none">
                                            <td>
                                                <b><asp:Literal Text="<%$ Resources:WebResources, ReportDetails_ToDate%>" runat="server"></asp:Literal></b><br />
                                                <asp:TextBox ID="txtCusDateTo" runat="server" CssClass="altText" onblur="javascript:ChangeStartDate(0)"
                                                    Width="80PX"></asp:TextBox>
                                                <asp:RequiredFieldValidator ID="reqCusTo" runat="server" ControlToValidate="txtCusDateTo"
                                                    Display="dynamic" CssClass="lblError" SetFocusOnError="true" Text="" ValidationGroup="DateSubmit"></asp:RequiredFieldValidator>
                                                <img src="image/calendar.gif" style="width: 20; height: 20; vertical-align: bottom;
                                                    cursor: pointer;" id="cal_triggerTo" title="<asp:Literal Text='<%$ Resources:WebResources, DateSelector%>' runat='server'></asp:Literal>" onblur="javascript:ChangeStartDate(0)"
                                                    onclick="return showCalendar('txtCusDateTo', 'cal_triggerTo', 0, '<%=format%>');"
                                                    alt="Date selector" /><%--ZD 100419--%>
                                            </td>
                                        </tr>
                                        <tr id="ChrtTypeCell" style="display: none">
                                            <td>
                                                <b><asp:Literal Text="<%$ Resources:WebResources, ReportDetails_ChartType%>" runat="server"></asp:Literal></b>      <%--FB 2930--%>
                                                <dxAxEd:ASPxComboBox ID="DrpChrtType" runat="server" SelectedIndex="0" Width="50%"
                                                    CssClass="altSelectFormat" ClientInstanceName="DrpChrtType">
                                                    <Items>
                                                        <dxAxEd:ListEditItem Text="<%$ Resources:WebResources, Bar%>" Value="1" />
                                                        <dxAxEd:ListEditItem Text="<%$ Resources:WebResources, Line%>" Value="2" />
                                                        <dxAxEd:ListEditItem Text="<%$ Resources:WebResources, Table%>" Value="3" />
                                                    </Items>
                                                </dxAxEd:ASPxComboBox>
                                            </td>
                                        </tr>
                                        <tr id="trWeeklyUsage1" style="display: none">
                                            <td>
                                                <table>
                                                    <tr>
                                                        <td>
                                                            <b><asp:Literal Text="<%$ Resources:WebResources, ReportDetails_StartYear%>" runat="server"></asp:Literal></b>
                                                            <dxAxEd:ASPxComboBox ID="lstWStartYear" runat="server" CssClass="altSelectFormat"
                                                                Width="100px" ClientInstanceName="lstWStartYear" SelectedIndex="0">
                                                            </dxAxEd:ASPxComboBox>
                                                        </td>
                                                        <td>
                                                            <b><asp:Literal Text="<%$ Resources:WebResources, ReportDetails_StartWeek%>" runat="server"></asp:Literal></b>
                                                            <dxAxEd:ASPxComboBox ID="lstWStartMonth" runat="server" CssClass="altSelectFormat"
                                                                Width="100px" ClientInstanceName="lstWStartMonth" SelectedIndex="0">
                                                            </dxAxEd:ASPxComboBox>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr id="trWeeklyUsage2" runat="server" style="display: none">
                                            <td>
                                                <table>
                                                    <tr>
                                                        <td>
                                                            <b><asp:Literal Text="<%$ Resources:WebResources, ReportDetails_EndYear%>" runat="server"></asp:Literal></b>
                                                            <dxAxEd:ASPxComboBox ID="lstWEndYear" runat="server" CssClass="altSelectFormat"
                                                                Width="100px" ClientInstanceName="lstWEndYear" SelectedIndex="0">
                                                            </dxAxEd:ASPxComboBox>
                                                        </td>
                                                        <td>
                                                            <b><asp:Literal Text="<%$ Resources:WebResources, ReportDetails_EndWeek%>" runat="server"></asp:Literal></b>
                                                            <dxAxEd:ASPxComboBox ID="lstWEndMonth" runat="server" CssClass="altSelectFormat"
                                                                Width="100px" ClientInstanceName="lstWEndMonth" SelectedIndex="0">
                                                            </dxAxEd:ASPxComboBox>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr id="trMonthlyUsage1" runat="server" style="display: none">
                                            <td>
                                                <table>
                                                    <tr>
                                                        <td>
                                                            <b><asp:Literal Text="<%$ Resources:WebResources, ReportDetails_StartYear%>" runat="server"></asp:Literal></b>
                                                            <dxAxEd:ASPxComboBox ID="lstMStartYear" runat="server" CssClass="altSelectFormat"
                                                                Width="100px" ClientInstanceName="lstMStartYear" SelectedIndex="0">
                                                            </dxAxEd:ASPxComboBox>
                                                        </td>
                                                        <td>
                                                            <b><asp:Literal Text="<%$ Resources:WebResources, ReportDetails_StartMonth%>" runat="server"></asp:Literal></b>
                                                            <dxAxEd:ASPxComboBox ID="lstMStartMonth" runat="server" CssClass="altSelectFormat"
                                                                Width="100px" ClientInstanceName="lstMStartMonth" SelectedIndex="0">
                                                            </dxAxEd:ASPxComboBox>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr id="trMonthlyUsage2" runat="server" style="display: none">
                                            <td>
                                                <table>
                                                    <tr>
                                                        <td>
                                                            <b><asp:Literal Text="<%$ Resources:WebResources, ReportDetails_EndYear%>" runat="server"></asp:Literal></b>
                                                            <dxAxEd:ASPxComboBox ID="lstMEndYear" runat="server" CssClass="altSelectFormat"
                                                                Width="100px" ClientInstanceName="lstMEndYear" SelectedIndex="0">
                                                            </dxAxEd:ASPxComboBox>
                                                        </td>
                                                        <td>
                                                            <b><asp:Literal Text="<%$ Resources:WebResources, ReportDetails_EndMonth%>" runat="server"></asp:Literal></b>
                                                            <dxAxEd:ASPxComboBox ID="lstMEndMonth" runat="server" CssClass="altSelectFormat"
                                                                Width="100px" ClientInstanceName="lstMEndMonth" SelectedIndex="0">
                                                            </dxAxEd:ASPxComboBox>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td align="right">
                                    <asp:Button ID="btnviewRep" runat="server" Text="<%$ Resources:WebResources, ReportDetails_btnviewRep%>" CssClass="altMedium0BlueButtonFormat"
                                        OnClick="ViewReport" ValidationGroup="DateSubmit" OnClientClick="javascript:return fnSubmit()" style="margin-right:8px;" /> <%--FB 2593--%>
                                </td>
                            </tr>
                            <tr>
                                <td align="center">
                                    <asp:Button ID="btnAdvRep" runat="server" Text="<%$ Resources:WebResources, ReportDetails_btnAdvRep%>" CssClass="altLongBlueButtonFormat"
                                        OnClientClick="javascript:return fnAdvanceReports('A')" Visible="false" />&nbsp;&nbsp;
                                </td>
                            </tr>
                            <tr>
                                <td align="right"> <%--FB 2593--%>
                                    <asp:Button ID="btnMCCRpt" runat="server" Text="<%$ Resources:WebResources, ReportDetails_btnMCCRpt%>" CssClass="altLongBlueButtonFormat"
                                        OnClientClick="javascript:return fnAdvanceReports('M')" Visible="true" />&nbsp;&nbsp;<!-- FB 2570 -->
                                </td>
                            </tr>
                        </table>
                    </div>
                </td>
                <%--Right Pane--%>
                <td valign="top" style="height: 670px">
                    <div id="Div1" style="border-color: Gray; border-width: 1px; border-style: none;
                        width: 100%; height: 470px; text-align: left;">
                        <table width="100%" border="0">
                            <tr>
                                <td>
                                    <table>
                                        <tr>
                                            <td align="left" width="50%">
                                                <span class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, ReportDetails_Organization%>" runat="server"></asp:Literal>:</span>
                                                <asp:Label ID="lblOrgName" runat="server"></asp:Label>
                                            </td>
                                            <td nowrap="nowrap"><%--FB 2501--%>
                                                <h3 style="text-align: left">
                                                    <asp:Label ID="lblHeading" runat="server"></asp:Label></h3>
                                                <%--<asp:Label ID="lblHeading" runat="server" Font-Bold="true" ForeColor="Green" Font-Size="Larger" ></asp:Label>--%>
                                            </td>
                                        </tr>
                                        <%--FB 2501 CDR Reports Starts--%>
                                        <tr id="trMCUDetails" visible="false" runat="server">
                                            <td colspan="2">
                                                <br />
                                                <table width="100%">
                                                    <tr>
                                                        <td nowrap="nowrap">
                                                            <span class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, ReportDetails_MCUName%>" runat="server"></asp:Literal></span><%--ALLBUGS-154--%>
                                                            <asp:Label runat="server" ID="lbl_McuName"></asp:Label>
                                                        </td>
                                                        <td nowrap="nowrap">
                                                            <span class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, ReportDetails_Type%>" runat="server"></asp:Literal></span><%--ALLBUGS-154--%>
                                                            <asp:Label runat="server" ID="lbl_McuType"></asp:Label>
                                                        </td>
                                                        <td nowrap="nowrap">
                                                            <span class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, ReportDetails_Address%>" runat="server"></asp:Literal></span><%--ALLBUGS-154--%>
                                                            <asp:Label runat="server" ID="lbl_IPAddress"></asp:Label>
                                                        </td>
                                                    </tr>
                                               <%--FB 2501 CDR Reports Ends--%>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr id="trWebChart" runat="server" style="display: none" align="center">
                                <td>
                                    <dxchartsui:WebChartControl ID="webChartCtrl" runat="server" Height="400px" Width="700px"
                                        PaletteName="The Trees" ClientInstanceName="webChartCtrl">
                                    </dxchartsui:WebChartControl>
                                </td>
                            </tr>
                            <tr id="trDetailsView" runat="server" style="display: none">
                                <td class="splTab">
                                    <%--FB 2289--%>
                                    <div id="divMonthlyReport" runat="server" style="display: none;">
                                        <table style="height: 420px; overflow: scroll">
                                            <tr>
                                                <td>
                                                    <table id="tblmonthlyRoom" runat="server" style="border-collapse: collapse; border-width: medium;
                                                        border: 1px solid black; border-style: solid;" cellpadding="7">
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                    <div id="divWeeklyReport" runat="server" style="display: none;height: 440px; ">
                                        <table style="height: 440px; overflow: scroll">
                                            <tr>
                                                <td>
                                                    <table id="tblweeklyRoom" runat="server" style="border-collapse: collapse; border-width: medium;
                                                        border: 1px solid black; border-style: solid;" cellpadding="7">
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                    <div id="divNoData" runat="server">
                                        <table style="height: 400px; overflow: scroll">
                                            <tr valign="top" style="font-size: 20pt; font-weight: bold;">
                                                <td> <br />
                                                    <asp:Literal Text="<%$ Resources:WebResources, ReportDetails_NoDatatodisp%>" runat="server"></asp:Literal>
                                                    <asp:LinkButton ID="linkWorkingdays" runat="server" Text="<%$ Resources:WebResources, ReportDetails_linkWorkingdays%>" OnClick="SetWorkingDays"></asp:LinkButton>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                    <div id="divReportDetais" runat="server" style="display: none;">
                                        <dxwgv:ASPxGridView AllowSort="true" ID="gvReportDetails" ClientInstanceName="gvReportDetails"
                                            CssClass="rowrap" runat="server" EnableRowsCache="True" OnHtmlRowCreated="gvReportDetails_HtmlRowCreated"
                                            Styles-Header-Wrap="True" OnHtmlDataCellPrepared="gvReportDetails_HtmlCellCreated"
                                            EnableCallBacks="false" Width="100%" Styles-Cell-HorizontalAlign="Left">
                                            <Settings ShowHorizontalScrollBar="true" ShowVerticalScrollBar="true" VerticalScrollableHeight="305"
                                                ShowGroupPanel="true" ShowFilterRow="true" />
                                            <%-- FB 2050 --%>
                                            <SettingsBehavior AllowMultiSelection="false" />
                                            <SettingsText EmptyDataRow="<%$ Resources:WebResources, NoData%>" GroupPanel="<%$ Resources:WebResources, GridViewGroupMsg%>"/>
                                            <SettingsPager Mode="ShowPager" PageSize="15" AlwaysShowPager="true" Position="Top" Summary-Text="<%$ Resources:WebResources, GridViewPageText%>">
                                            </SettingsPager>
                                        </dxwgv:ASPxGridView>
                                        <dx:ASPxGridViewExporter ID="gridExport" runat="server" GridViewID="gvReportDetails">
                                        </dx:ASPxGridViewExporter>
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </div>
                    <div id="SummaryDiv" style="vertical-align: middle;margin-top: 20px;"> <%--FB 2800--%>
                        <table width="100%" border="0">
                            <tr>
                                <td align="left" colspan="2">
                                    <br />
                                    <span class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, ReportDetails_Summary%>" runat="server"></asp:Literal></span>
                                    <table id="SummaryTable" runat="server" class="tableBody" style="border-style: solid;
                                        border-width: 1px;" border="1" cellpadding="0" cellspacing="0">
                                        <tr>
                                            <td>
                                                <span class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, ReportDetails_MeetingsSummar%>" runat="server"></asp:Literal></span>
                                            </td>
                                            <td>
                                                &nbsp;
                                            </td>
                                            <td>
                                                &nbsp;
                                            </td>
                                            <td>
                                                &nbsp;
                                            </td>
                                            <td>
                                                &nbsp;
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                &nbsp;
                                            </td>
                                            <td>
                                                &nbsp;
                                            </td>
                                            <td>
                                                <span class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, ReportDetails_PtPtMeetings%>" runat="server"></asp:Literal></span>
                                            </td>
                                            <td width="10%">
                                                <asp:Label ID="PtPtCnt" runat="server">&nbsp;</asp:Label>
                                            </td>
                                            <td>
                                                &nbsp;
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                &nbsp;
                                            </td>
                                            <td>
                                                &nbsp;
                                            </td>
                                            <td>
                                                <span class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, ReportDetails_MultiPtMeeting%>" runat="server"></asp:Literal></span>
                                            </td>
                                            <td>
                                                <asp:Label ID="MultiPtCnt" runat="server">&nbsp;</asp:Label>
                                            </td>
                                            <td>
                                                &nbsp;
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                &nbsp;
                                            </td>
                                            <td>
                                                &nbsp;
                                            </td>
                                            <td>
                                                <span class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, ReportDetails_VTCMeetingTot%>" runat="server"></asp:Literal></span>
                                            </td>
                                            <td>
                                                <asp:Label ID="VTCMtTotCnt" runat="server">&nbsp;</asp:Label>
                                            </td>
                                            <td>
                                                &nbsp;
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                &nbsp;
                                            </td>
                                            <td>
                                                <span class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, ReportDetails_InternalMeetin%>" runat="server"></asp:Literal></span>
                                            </td>
                                            <td>
                                                &nbsp;
                                            </td>
                                            <td>
                                                &nbsp;
                                            </td>
                                            <td>
                                                <span class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, ReportDetails_Meetings%>" runat="server"></asp:Literal></span>
                                            </td>
                                        </tr>
                                    </table>
                                    <br />
                                    <table id="DetailsHeadingTable" runat="server" width="80%" class="tableHeader" style="border-width: 1px;"
                                        border="1" cellpadding="3" cellspacing="0">
                                        <tr align="left" style="font-weight: bold;"> <%--ZD 100393--%>
                                            <td width="12%">
                                                &nbsp;
                                            </td>
                                            <td width="10%"><asp:Literal Text="<%$ Resources:WebResources, ReportDetails_Minutes%>" runat="server"></asp:Literal></td>
                                            <td width="10%"><asp:Literal Text="<%$ Resources:WebResources, ReportDetails_Hours%>" runat="server"></asp:Literal></td>
                                            <td width="10%"><asp:Literal Text="<%$ Resources:WebResources, ReportDetails_Connects%>" runat="server"></asp:Literal></td>
                                            <td width="10%"><asp:Literal Text="<%$ Resources:WebResources, ReportDetails_ISDNOutbound%>" runat="server"></asp:Literal><br />
                                                (<asp:Literal ID="Literal2" Text="<%$ Resources:WebResources, Hours%>" runat="server"></asp:Literal>)
                                            </td>
                                            <td width="10%"><asp:Literal Text="<%$ Resources:WebResources, ReportDetails_ISDNOutbound%>" runat="server"></asp:Literal><br />
                                                %
                                            </td>
                                            <td width="10%"><asp:Literal Text="<%$ Resources:WebResources, ReportDetails_VoIPHours%>" runat="server"></asp:Literal></td>
                                            <td width="10%"><asp:Literal Text="<%$ Resources:WebResources, ReportDetails_VoIP%>" runat="server"></asp:Literal></td>
                                            <td width="10%"><asp:Literal Text="<%$ Resources:WebResources, ReportDetails_InboundISDN%>" runat="server"></asp:Literal><br />
                                                (<asp:Literal ID="Literal3" Text="<%$ Resources:WebResources, Hours%>" runat="server"></asp:Literal>)
                                            </td>
                                        </tr>
                                    </table>
                                    <div id="DetailsDiv" runat="server" style="width: 78%">
                                        <asp:Table ID="VTCDetails" runat="server" class="tableBody" Width="100%" Style="border-style: solid;
                                            border-width: 1px;" border="1" CellPadding="3" CellSpacing="0" HorizontalAlign="Left"> <%--ZD 100393--%>
                                        </asp:Table>
                                    </div>
                                    <asp:Table ID="VTCTotal" runat="server" class="tableBody" Height="60%" Width="80%"
                                        Style="border-style: solid; border-width: 1px;" border="1" CellPadding="3" CellSpacing="0">
                                        <%--Edited for FF--%>
                                        <asp:TableRow ID="FirstRow" runat="server" HorizontalAlign="left" Width="100%"> <%--ZD 100393--%>
                                            <asp:TableCell Width="12%">
                                                <asp:ImageButton ID="imgDiv" runat="server" ImageUrl="image/loc/nolines_plus.gif" AlternateText="Expand/Collapse"
                                                    OnClientClick="Javascript:return fnShowImg()" /><%--ZD 100419--%>
                                                <asp:Literal ID="Literal4" Text="<%$ Resources:WebResources, VTCTotals%>" runat="server"></asp:Literal>
                                            </asp:TableCell>
                                        </asp:TableRow>
                                    </asp:Table>
                                </td>
                            </tr>
                        </table>
                    </div>
                </td>
            </tr>
        </table>
    </div>
    
    </form>
    <!-- FB 2050 Start -->

    <script type="text/javascript">
function refreshImage()
{
  //window.scrollTo(0,0);
  //triggerClear();
  //setTimeout("clearValues()",500);
  var obj = document.getElementById("mainTop");
  if(obj != null)
  {
      var src = obj.src;
      var pos = src.indexOf('?');
      if (pos >= 0) {
         src = src.substr(0, pos);
      }
      var date = new Date();
      obj.src = src + '?v=' + date.getTime();
      
      if(obj.width > 804)
      obj.setAttribute('width','804');
  }
  //refreshStyle(); // Commented for Refresh Issue
  setMarqueeWidth();
  return false;
}

function refreshStyle()
{
	var i,a,s;
	a=document.getElementsByTagName('link');
	for(i=0;i<a.length;i++) {
		s=a[i];
		if(s.rel.toLowerCase().indexOf('stylesheet')>=0&&s.href) {
			var h=s.href.replace(/(&|\\?)forceReload=d /,'');
			s.href=h+(h.indexOf('?')>=0?'&':'?')+'forceReload='+(new Date().valueOf());
		}
	}
}

function setMarqueeWidth()
{
    var screenWidth = screen.width - 25;
    if(document.getElementById('martickerDiv')!=null)
        document.getElementById('martickerDiv').style.width = screenWidth + 'px';
        
    if(document.getElementById('marticDiv')!=null)
        document.getElementById('marticDiv').style.width = screenWidth + 'px';
    
    if(document.getElementById('marticker2Div')!=null)
        document.getElementById('marticker2Div').style.width = (screenWidth-15) + 'px';
    
    if(document.getElementById('martic2Div')!=null)
        document.getElementById('martic2Div').style.width = (screenWidth-15) + 'px';
}

window.onload = refreshImage;

function noError(){return true;} // FB 2050
window.onerror = noError;

    </script>

    <!-- FB 2050 End -->
</body>
</html>

<script type="text/javascript" language="javascript">
    
    fnShowMenu();
    fnAssignDefault('S');
        
    function ChangeStartDate(frm)
    {
            var confenddate = '';
            confenddate = GetDefaultDate(document.getElementById("txtCusDateTo").value, '<%=format%>');
            var confstdate = '';
            confstdate = GetDefaultDate(document.getElementById("txtCusDateFrm").value, '<%=format%>');

            if (document.getElementById("txtCusDateTo").value != "")
                reqCusTo.style.display = 'none';

            if (Date.parse(confstdate) > Date.parse(confenddate)) {
                if (frm == "0") {
                    alert(UserDate);
                    rtn = false;
                    return false;
                }
            }
            else {
                rtn = true;
                return true;
            }
        }

    function ChangeEndDate(frm)
    {
            var confenddate = '';
            if (document.getElementById("txtCusDateTo").value != "")
                confenddate = GetDefaultDate(document.getElementById("txtCusDateTo").value, '<%=format%>');
            var confstdate = '';
            if (document.getElementById("txtCusDateFrm").value != "")
                confstdate = GetDefaultDate(document.getElementById("txtCusDateFrm").value, '<%=format%>');

            if (document.getElementById("txtCusDateFrm").value != "")
                reqCusFrom.style.display = 'none';

            if (Date.parse(confenddate) < Date.parse(confstdate)) {
                if (frm == "0") {
                    alert(UserDate);
                    rtn = false;
                    return false;
                }
            }
            else {
                rtn = true;
                return true;
            }
        }


    function fnShowMenu()
    {
            var reportsList = document.getElementById("ReportsList");
            var type = document.getElementById("DrpAllType");
            var confScheRptDivList = document.getElementById("ConfScheRptDivList");
            var lstreport = document.getElementById("lstReport");
            var paramRow = document.getElementById("ParamRow");
            var resourseType = document.getElementById("lstResourseType"); //FB 21555
            
            var rptListue;
            var lstUsageRpts;
            
            reportsList.value = ReportsList.GetValue();
            type = DrpAllType.GetValue();
            confScheRptDivList.value = ConfScheRptDivList.GetValue();
            lstreport.value = lstReport.GetValue();
            resourseType.value = lstResourseType.GetValue();
            lstUsageRpts  = lstUsageReports.GetValue(); //FB 2343
            // FB 2501 Starts
            document.getElementById("ConferenceIDRow").style.display = "none";
            document.getElementById("TimezoneRow").style.display = "none";
            document.getElementById("ConferenceHostRow").style.display = "none";
            document.getElementById("ConferenceRequestorRow").style.display = "none";
            document.getElementById("tdBridge").style.display = "none";
            document.getElementById("ConferenceTitleRow").style.display = "none";
            document.getElementById("EndpointRow").style.display = "none";
            document.getElementById("RoomRow").style.display = "none";
            document.getElementById("OperatorRow").style.display = "none";
            document.getElementById("CallurlRow").style.display = "none";
            document.getElementById("QueryTypeRow").style.display = "none";
            document.getElementById("endTime").style.display ="block";
            document.getElementById("startTime").style.display ="block";
            document.getElementById("tdDateTime1").style.display ="block";
            document.getElementById("tdDateTime2").style.display ="none";
            document.getElementById("tdDateTimeEnd1").style.display ="block";
            document.getElementById("tdDateTimeEnd2").style.display ="none";
            // FB 2501 Ends
            
            if (reportsList)
                rptListValue = reportsList.value;
            
            HideControls(rptListValue);
            
            if (rptListValue != "4") {
                var cmbStrtTime_Text = document.getElementById("CmbStrtTime_Text");
                var cmbEndTime_Text = document.getElementById("CmbEndTime_Text");
                if (cmbStrtTime_Text)
                    cmbStrtTime_Text.style.width = "70px";

                if (cmbEndTime_Text)
                    cmbEndTime_Text.style.width = "70px";
            }

            if (paramRow)
                paramRow.style.display = "Block";
            switch (rptListValue) {
                case "1":
                    document.getElementById("DateSelRow").style.display = "Block";
                    document.getElementById("ConfScheRptCell").style.display = "Block";
                    //FB 2501 Starts
                    switch(confScheRptDivList.value)
                    {
                      case "1":
                         document.getElementById("RoomRow").style.display = "Block";
                         document.getElementById("SummaryDiv").style.display = "Block";
                          break;
                      case "2":
                      case "3":
                      case "4":
                        return false;
                        break;
                      case "5":
                         document.getElementById("ConferenceIDRow").style.display = "Block";
                         document.getElementById("TimezoneRow").style.display = "Block";
                         document.getElementById("ConferenceHostRow").style.display = "Block";
                         document.getElementById("ConferenceRequestorRow").style.display = "Block";
                         document.getElementById("tdBridge").style.display = "Block";
                         document.getElementById("ConferenceTitleRow").style.display = "Block";
                         document.getElementById("EndpointRow").style.display = "Block";
                         document.getElementById("RoomRow").style.display = "Block";
                         document.getElementById("OperatorRow").style.display = "Block";
                         document.getElementById("CallurlRow").style.display = "Block";
                         document.getElementById("QueryTypeRow").style.display = "Block";
                         document.getElementById("endTime").style.display ="none";
                         document.getElementById("startTime").style.display ="none";
                         document.getElementById("tdDateTime1").style.display ="none";
                         document.getElementById("tdDateTime2").style.display ="block";
                         document.getElementById("tdDateTimeEnd1").style.display ="none";
                         document.getElementById("tdDateTimeEnd2").style.display ="block";
                         break;
                        
                     }
                    break;
                    //FB 2501 Ends
                case "2":
                    document.getElementById("UserReportsCell").style.display = "Block";
                    paramRow.style.display = "None";
                    break;
                case "3"://FB 2343
                    document.getElementById("UsageReportCell").style.display = "Block";
                    if(lstUsageRpts == "1" || lstUsageRpts == "2")
                    {
                        document.getElementById("DateSelRow").style.display = "Block";
                    }
                    else if (lstUsageRpts == "3" || lstUsageRpts == "4")
                    {
                        document.getElementById("btnPDF").style.display = "none";
                        document.getElementById("ImgPrinter").style.display = "none";
                        if (lstUsageRpts == "3")
                        {
                            document.getElementById("trWeeklyUsage1").style.display = "block";
                            document.getElementById("trWeeklyUsage2").style.display = "block";
                        }
                        else
                        {
                            document.getElementById("trMonthlyUsage1").style.display = "block";
                            document.getElementById("trMonthlyUsage2").style.display = "block";
                        }
                    }
                    break;
                case "4":
                    document.getElementById("tdAllType").style.display = "Block";
                    var drpchrtValue = DrpChrtType.GetValue();
                    if (type == "1") {
                        document.getElementById("ChrtTypeCell").style.display = "Block";
                        document.getElementById("TimeRangeCell").style.display = "Block";
                        document.getElementById("tdConfType").style.display = "block";
                        fnSetCntrlValue("DrpLocations", "1");
                        fnSetCntrlValue("DrpMCU", "1");
                           //FB 2929 Starts
//                        if (lstreport.value == "6" && drpchrtValue == "1") {
//                            document.getElementById("btnPDF").style.display = "none";
//                            document.getElementById("ImgPrinter").style.display = "none";
//                        }
//                        else if (drpchrtValue != "1") {
//                            document.getElementById("btnExcel").style.display = "none";
//                        }
                          //FB 2929 End
                    }
                    else if (type == "2") {
                        document.getElementById("ChrtTypeCell").style.display = "Block";
                        document.getElementById("TimeRangeCell").style.display = "Block";
                        document.getElementById("tdLoc").style.display = "block";
                        if (DrpLocations.GetValue() == "1")
                            document.getElementById("RoomRow").style.display = "block";

                        fnSetCntrlValue("DrpMCU", "1");
                           //FB 2931 Start
//                        if (drpchrtValue == "1") {
//                            document.getElementById("btnPDF").style.display = "none";
//                            document.getElementById("ImgPrinter").style.display = "none";
//                        }
//                        else if (drpchrtValue != "1") {
//                            document.getElementById("btnExcel").style.display = "none";
//                        }
                           //FB 2931 Start
                        document.getElementById("tdLoc").style.display = "block";
                    }
                    else if (type == "3") {
                        document.getElementById("ChrtTypeCell").style.display = "Block";
                        document.getElementById("tdMCU").style.display = "block";
                        document.getElementById("TimeRangeCell").style.display = "block";

                        fnSetCntrlValue("DrpLocations", "1");
                            //FB 2933 Start
//                        if (drpchrtValue == "1" && DrpMCU.GetValue() != "2") {
//                            document.getElementById("btnPDF").style.display = "none";
//                            document.getElementById("ImgPrinter").style.display = "none";
//                        }
//                        else if (drpchrtValue != "1") {
//                            document.getElementById("btnExcel").style.display = "none";
//                        }
                            //FB 2933 End
                        
                        if(DrpMCU.GetValue() == "2")
                        document.getElementById("tdBridge").style.display = "block";
                        
                    }

                    if (lstreport.value == "6") {
                        document.getElementById("tdCusFrmDte").style.display = "block";
                        document.getElementById("tdCusToDte").style.display = "block";
                    }
                    break;
                case "5":  
                    //FB 2155 - Starts
                    document.getElementById("trSelectDate").style.display = "Block";
                    document.getElementById("tdDaily").style.display = "block";
                    document.getElementById("tdStatus").style.display = "block";
                    break;    
                case "6":
                    document.getElementById("tdResourse").style.display = "block";
                    paramRow.style.display = "None";
                    if(resourseType.value == "1")
                    {
                        document.getElementById("tdUser").style.display = "block";
                    }
                    else if (resourseType.value == "2")
                         document.getElementById("tdRoom").style.display = "block";
                    else if (resourseType.value == "3" || resourseType.value == "4")
                    {
                         document.getElementById("tdRoom").style.display = "none";
                         document.getElementById("tdUser").style.display = "none";
                    }
                    else if (resourseType.value == "5")
                    {
                         document.getElementById("tdRoomConf").style.display = "block";
                         document.getElementById("DateSelRow").style.display = "Block";
                         paramRow.style.display = "Block";
                    }
                    else if (resourseType.value == "6")//FB 2501 CDR Reports
                    {    
                         document.getElementById("tdBridge").style.display = "block";
                         document.getElementById("DateSelRow").style.display = "block";
                    }
                    else if (resourseType.value == "7")//ZD 102468
                    {    
                         document.getElementById("trCustomOptions").style.display = "block";                         
                         document.getElementById("DateSelRow").style.display = "block";
                    }
                    break;
                case "7":
                    document.getElementById("tdOrg").style.display = "block";
                    document.getElementById("tdResourse").style.display = "block";
                    paramRow.style.display = "None";
                    if(resourseType.value == "1")
                    {
                        document.getElementById("tdUser").style.display = "block";
                    }
                    else if (resourseType.value == "2")
                         document.getElementById("tdRoom").style.display = "block";
                    else if (resourseType.value == "3" || resourseType.value == "4")
                    {
                         document.getElementById("tdRoom").style.display = "none";
                         document.getElementById("tdUser").style.display = "none";
                    }
                    else if (resourseType.value == "5")
                    {
                         document.getElementById("tdRoomConf").style.display = "block";
                         document.getElementById("DateSelRow").style.display = "Block";
                         paramRow.style.display = "Block";
                    }
                    break;
                case "8":
                    paramRow.style.display = "None";
                    break;
                //FB 2155 - End
            }
            return true;
        }

    function HideControls()
    {
            var args = HideControls.arguments;
            if (args[0] != "4")
                document.getElementById("tdAllType").style.display = "none";

            if (args[0] != "1")
                document.getElementById("ConfScheRptCell").style.display = "none";

            document.getElementById("tdBridge").style.display = "none"; 
            document.getElementById("tdConfType").style.display = "none";
            document.getElementById("tdLoc").style.display = "none";
            document.getElementById("tdMCU").style.display = "none";
            document.getElementById("tdCountry").style.display = "none";
            document.getElementById("tdZipCode").style.display = "none";
            document.getElementById("UsageReportCell").style.display = "none";
            document.getElementById("TimeRangeCell").style.display = "none";
            document.getElementById("ChrtTypeCell").style.display = "none";
            document.getElementById("DateSelRow").style.display = "none";
            document.getElementById("RoomRow").style.display = "none";
            document.getElementById("UserReportsCell").style.display = "none";
            document.getElementById("tdCusFrmDte").style.display = "none";
            document.getElementById("tdCusToDte").style.display = "none";
            document.getElementById("ImgPrinter").style.display = "Block";
            document.getElementById("btnExcel").style.display = "Block";
            document.getElementById("btnPDF").style.display = "Block";
            document.getElementById("SummaryDiv").style.display = "none";
            document.getElementById("tdDaily").style.display = "none";
            document.getElementById("tdStatus").style.display = "none";
            document.getElementById("tdOrg").style.display = "none";
            document.getElementById("tdResourse").style.display = "none";
            document.getElementById("tdUser").style.display = "none";
            document.getElementById("tdRoom").style.display = "none";
            document.getElementById("tdRoomConf").style.display = "none";
            document.getElementById("trSelectDate").style.display = "none";
            document.getElementById("trMonthlyUsage1").style.display = "none";//FB 2343
            document.getElementById("trMonthlyUsage2").style.display = "none";
            document.getElementById("trWeeklyUsage1").style.display = "none";
            document.getElementById("trWeeklyUsage2").style.display = "none";
            document.getElementById("trCustomOptions").style.display = "none";//ZD 102468
            
        }

    function fnSetCntrlValue(cntrl, value)
     {
            var _cntrl = document.getElementById(cntrl);
            if (_cntrl)
                _cntrl.value = value;
    }

    function fnShowImg()
    {
        var args = fnShow.arguments;
        var detailsDiv = document.getElementById("DetailsDiv");
        var imgDiv = document.getElementById("imgDiv");
        
        if(detailsDiv)
        {
            if(detailsDiv.style.display == "block")
            {
                detailsDiv.style.display = "None";
                if(imgDiv)
                    imgDiv.src = imgDiv.src.replace("minus", "plus");
            }
            else
            {
                 detailsDiv.style.display = "Block";
                if(imgDiv)
                    imgDiv.src = imgDiv.src.replace("plus", "minus");
            }
        }
        
        return false;
    }
    
    function fnShowRooms()
    {
        var _cntrl = document.getElementById('<%=RoomDiv.ClientID%>');
        if (_cntrl.style.display == 'none')
            _cntrl.style.display = 'block';
        else
            _cntrl.style.display = 'none';
    }

    function fnSelectAll()
    {
        var args = fnSelectAll.arguments;
        var rooms = '';
        if (args[0] != null) {
            if (args[1] != null) {
                var this_ = document.getElementById(args[1]);


                fnAssignChecked(this_, args[0].checked);
            }
        }
    }

    function fnAssignChecked(this_, checkValue)
    {

        if (this_ != null) {
            var checkBoxArray = this_.getElementsByTagName('input');

            for (var i = 0; i < checkBoxArray.length; i++) {
                var checkBoxRef = checkBoxArray[i];

                checkBoxRef.checked = checkValue;
            }
        }
    }
            

    function fnDeselectAll()
    {
    
        var args = fnDeselectAll.arguments;

        if (args[0] != null) {
            if (!args[0].checked) {
                if (args[1] != null) {
                    var allCheck = document.getElementById(args[1])
                    if (allCheck) {
                        if (allCheck.checked)
                            allCheck.checked = false;
                    }
                }
            }
        }
        
        var checkBoxArray = document.getElementById("cblRoom");        
         //document.getElementById("chkSelectall").checked = true;FB 2501
		 //FB 2800 Start
        var len=0;
        if(navigator.userAgent.indexOf("MSIE")>-1)
            len = checkBoxArray.childNodes[0].childNodes.length;
        else
            len = checkBoxArray.childNodes[1].childNodes.length;
            
        for (var i = 0; i < len-1; i++)
        //FB 2800 End
         {
           if( document.getElementById(cblRoom.id + '_' + [i]).checked == false)
              document.getElementById("chkSelectall").checked = false;
         }
                
    }


    function fnAssignDefault()
    {
        var args = fnAssignDefault.arguments
        if (args[0] == 'S')
            fnAssignValue('<%=cblRoom.ClientID%>');
        else {
            document.frmReports.chkSelectall.checked = true;
            fnSelectAll(document.frmReports.chkSelectall, '<%=cblRoom.ClientID%>');
            fnAssignValue('<%=cblRoom.ClientID%>');
        }
    }

    function fnAssignValue()
     {
        document.frmReports.txtRooms.value = ''
        document.frmReports.hdnRoomIDs.value = ''

        var args = fnAssignValue.arguments;
        var rooms = '';

        if (args[0] != null) {
            var this_ = document.getElementById(args[0])
            if (this_) {
                var checkBoxArray = this_.getElementsByTagName('input');
                var checkedValues = '';

                for (var i = 0; i < checkBoxArray.length; i++) {
                    var checkBoxRef = checkBoxArray[i];
                    if (checkBoxRef.checked) {

                        var labelArray = checkBoxRef.parentNode.getElementsByTagName('label');      //Edited for FF

                        if (labelArray.length > 0) {
                            if (rooms.length > 0)
                                rooms += ', ';

                            rooms += labelArray[0].innerHTML;
                        }
                    }
                }

                if (rooms != '')
                    document.frmReports.txtRooms.value = rooms;
            }
        }
    }

    function fnShow()
    {
        var reportsList = document.getElementById("ReportsList");
        reportsList.value = ReportsList.GetValue();
        var drpLoc = DrpLocations.GetValue();
        var type = DrpAllType.GetValue();
        var lstreport = lstReport.GetValue();
        
        if (reportsList) {
            if (reportsList.value == "4") {

                if(type == "2")
                {
                    if (drpLoc == "1")
                        document.getElementById("RoomRow").style.display = "block";
                    else
                        document.getElementById("RoomRow").style.display = "None";

                    if (drpLoc == "2")
                        document.getElementById("tdCountry").style.display = "block";
                    else
                        document.getElementById("tdCountry").style.display = "none";

                    if (drpLoc == "3")
                        document.getElementById("tdZipCode").style.display = "block";
                    else
                        document.getElementById("tdZipCode").style.display = "none";
                }

                if (DrpMCU.GetValue() == "2" && type == "3")
                    document.getElementById("tdBridge").style.display = "block";
                else {
                    document.getElementById("TimeRangeCell").style.display = "block";
                    document.getElementById("tdBridge").style.display = "none";
                }
                
                if (lstreport == "6") {
                    document.getElementById("tdCusFrmDte").style.display = "block";
                    document.getElementById("tdCusToDte").style.display = "block";
                }
                else {
                    document.getElementById("tdCusFrmDte").style.display = "none";
                    document.getElementById("tdCusToDte").style.display = "none";
                    document.getElementById("txtCusDateFrm").value = "";
                    document.getElementById("txtCusDateTo").value = "";
                }

                if (type == "0" && lstreport == "0" && DrpChrtType.GetValue() == "0") {
                    document.getElementById("webChartCtrl").Titles = "none";
                }
                if (type == "2" && drpLoc != "1")
                    return false;
            }
        }
    }

    function setPrint()
    {
        var reportsList = ReportsList.GetValue();
        var drpChrttype = DrpChrtType.GetValue();
        
        if (reportsList)
        {
            if (reportsList == "4" && drpChrttype != "3") 
            {
                if(document.getElementById("hdnChartPrint"))
                    document.getElementById("hdnChartPrint").value = "p";
                    
                webChartCtrl.Print();
                return false;
            }
            else {
                window.open("PrintInterface.aspx", "myVRM", 'status=yes,width=750,height=400,scrollbars=yes,resizable=yes');
                return false;
            }
        }
    }


    function pdfReport()
    {
        var reportsList = document.getElementById("ReportsList");
        reportsList = ReportsList.GetValue();
        
        if (reportsList)
        {
            if (reportsList.value == "4" && DrpChrtType.GetValue() != "1") {

                var loc = document.location.href;
                loc = loc.substring(0, loc.indexOf("ReportDetails.aspx"));
                var hdnchartname = document.getElementById("hdnChartName");
                var src = "";
                var srcAry;
                var path = '<%=Session["OrgCSSPath"]%>';
                path = path.split('.')[0] + '<%=Session["ThemeType"]%>' + ".css"; // FB 2815
                if (hdnchartname != "") {
                    var img1 = document.getElementById(hdnchartname.value);
                    src = img1.src;
                    srcAry = src.split("?");
                    img1.src = loc + "/image/Chart1.jpeg?" + srcAry[1];
                }
                var htmlString = document.getElementById("tbReportDts").innerHTML;
                var toBeRemoved = document.getElementById("tblControls");

                if (toBeRemoved != null)
                    htmlString = htmlString.replace(toBeRemoved.innerHTML, "");

                var toBeRemoved = document.getElementById("tbleBtns");

                if (toBeRemoved != null)
                    htmlString = htmlString.replace(toBeRemoved.innerHTML, "");


                htmlString = "<html><link rel='stylesheet' type='text/css' href='" + loc + path + "'><body><center><table><tr><td></td></tr></table>" + htmlString + "</center></body></html>";
                if (document.getElementById("hdntempText") != null)
                    document.getElementById("hdntempText").value = htmlString;

                return true;
            }
        }
    }
    
    //FB 2501
    function fnClearValues()
    {
    if(document.getElementById('ConferenceName').value != null)
        document.getElementById('ConferenceName').value = "";
    if(document.getElementById('CallURL').value != null)    
        document.getElementById('CallURL').value = "";  
    }

    function fnSubmit()
    {
       //FB 2501 - Start
       if(document.getElementById('regConfName')!=null)
            var lblObj1 = document.getElementById('regConfName');
       if(document.getElementById('regCallURL')!=null)     
       var lblObj2 = document.getElementById('regCallURL');
       
       if(lblObj1.style.display != 'none' || lblObj2.style.display != 'none')
       return false;
       //FB 2501 - End

        document.getElementById("hdnChartPrint").value = ""
        document.getElementById("hdnValue").value = "S";
        var reportsList = document.getElementById("ReportsList");
        reportsList.value = ReportsList.GetValue();
        
       var lstUsageRpt = lstUsageReports.GetValue(); //FB 2343
        
        
        var checkBoxArray = document.getElementById("cblRoom");        
        if (reportsList)
        {
            if( (reportsList.value == "1" && ConfScheRptDivList.GetValue() == "1") ||
                    (reportsList.value == "4" && DrpAllType.GetValue() == "2") && checkBoxArray )
            {
                var chkCnt = 0
                //FB 2800 Start
                var len=0;
                if(navigator.userAgent.indexOf("MSIE")>-1)
                    len = checkBoxArray.childNodes[0].childNodes.length;
                else
                    len = checkBoxArray.childNodes[1].childNodes.length;
                
                for (var i = 0; i < len-1 ; i++) 
                //FB 2800 End
                {   
                
                    if( document.getElementById(cblRoom.id + '_' + [i]).checked)
                        chkCnt++;
                }
                if(chkCnt == 0 && DrpLocations.GetValue() != "2" && DrpLocations.GetValue() != "3")//FB 2911
                {
                    alert(ReportSelectRoom);
                    return false;
                }
            }
            DataLoading(1); // ZD 100176
             //FB 2343 - Starts
            if(reportsList.value == "3" && (lstUsageRpt == "3" || lstUsageRpt == "4"))
            {
                var wStartMonth = lstWStartMonth.GetValue();
                var wEndMonth = lstWEndMonth.GetValue();
               
                var mStartMonth = lstMStartMonth.GetValue();
                var mEndMonth = lstMEndMonth.GetValue();
                
                var wStartYear = lstWStartYear.GetValue();
                var wEndYear = lstWEndYear.GetValue();
                
                var mStartYear = lstMStartYear.GetValue();
                var mEndYear = lstMEndYear.GetValue();
                
                if(lstUsageRpt == "3")
                {
                     if(parseInt(wStartYear) > parseInt(wEndYear))
                     {
                        alert(ReportStartYear)
                        return false;
                     }
                     
                    if(parseInt(wEndYear) == parseInt(wStartYear))
                    {
                        if(parseInt(wStartMonth) > parseInt(wEndMonth))
                        {
                            alert(ReportStartWeek)
                            return false;
                        }
                    }  
                }
                else
                {
                    
                    if(parseInt(mStartYear) > parseInt(mEndYear))
                    {
                       alert(ReportStartYear);
                       return false;
                    }
                    
                    if(parseInt(mEndYear) == parseInt(mStartYear))
                    {
                        if(parseInt(mStartMonth) > parseInt(mEndMonth))
                        {
                            alert(ReportStartMonth)
                            return false;
                        }
                    }  
                }
            }
             //FB 2343 - End 
            if (reportsList.value == "1" || reportsList.value == "3" || ( reportsList.value == "6" || reportsList.value == "7" && lstResourseType.GetValue() == "5" ))
            {
                if (document.frmReports.txtStartDate.value != "" && document.frmReports.txtEndDate.value == "")
                {
                    alert(UserToDate);
                    document.form1.txtEndDate.focus();
                    return false;
                }
                else if (document.frmReports.txtEndDate.value != "" && document.frmReports.txtStartDate.value == "")
                {
                    alert(UserFromDate);
                    document.form1.txtStartDate.focus();
                    return false;
                }

                textValue = document.frmReports.txtStartDate.value;
                index = textValue.length;
                startDate = textValue.substring(0, index);
                textValue = document.frmReports.txtEndDate.value;
                index = textValue.length;
                endDate = textValue.substring(0, index);
                if ('<%=format%>' == 'dd/MM/yyyy') // dd/MM/yyyy
                {
                    var mDay = startDate.substring(0, 2);
                    var mMonth = startDate.substring(3, 5);
                    var mYear = startDate.substring(6, 10);
                    var strSeperator = '/';
                    startDate = mMonth + strSeperator + mDay + strSeperator + mYear;
                    var emDay = endDate.substring(0, 2);
                    var emMonth = endDate.substring(3, 5);
                    var emYear = endDate.substring(6, 10);
                    endDate = emMonth + strSeperator + emDay + strSeperator + emYear;
                }

                timeValue = document.frmReports.CmbStrtTime.value;
                if (timeValue != "")
                    var startTime = timeValue.substring(0, textValue.length);//ZD 102468

                timeValue = document.frmReports.CmbEndTime.value;
                if (timeValue != "")
                    var endTime = timeValue.substring(0, timeValue.length);//ZD 102468

                var compareVal = fnCompareDate(startDate, endDate)
                
                if (compareVal == "L")
                {
                    alert(UserDate);
                    return false;
                }
                else 
                {
                    if (compareVal == "E")
                     {
                         if ((startTime != "") && (endTime != ""))
                         {
                            var start = startTime.split(':');
                            var end = endTime.split(':');

                            var stTime = start[1].substring(start[1].length, 2);
                            var endTime = end[1].substring(end[1].length, 2);

                            if ((stTime == " PM") && (endTime == " AM") ) {
                                alert(GraphReportTime);
                                return false;
                            }
                            else if (((stTime == " AM") && (endTime == " AM")) || ((stTime == " PM") && (endTime == " PM")) || (stTime == "" && endTime == "")) {
                            if (start[0] != "12") 
                                {
                                    if (start[0] > end[0]) {
                                        alert(GraphReportTime);
                                        return false;
                                    }
                                }
                                else 
                                {
                                    if ((stTime == " AM") && (endTime == " AM") && (start[0] == end[0])) {
                                        if (start[1] > end[1]) {
                                            alert(GraphReportTime);
                                            return false;
                                        }
                                        else if (start[1] == end[1]) {
                                            alert(GraphReportTime);
                                            return false;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            else if (reportsList.value == "4") 
            {
                if (lstReport.GetValue() == "6") 
                {
                    if (document.getElementById("txtCusDateFrm").value == "" || document.getElementById("txtCusDateTo").value == "") 
                    {
                        if (document.getElementById("txtCusDateFrm").value == "") 
                        {
                            reqCusFrom.style.display = 'block';
                            document.getElementById("txtCusDateFrm").focus();
                            return false;
                        }

                        if (document.getElementById("txtCusDateTo").value == "") 
                        {
                            reqCusTo.style.display = 'block';
                            document.getElementById("txtCusDateTo").focus();
                            return false;
                        }
                    }
                    else if (document.getElementById("txtCusDateTo").value != "") 
                    {
                        reqCusTo.style.display = 'none';
                    }
                    else if (document.getElementById("txtCusDateFrm").value != "") 
                    {
                        reqCusFrom.style.display = 'none';
                    }
                    if (document.getElementById("txtCusDateFrm").value != "" && document.getElementById("txtCusDateTo").value != "") 
                    {
                        ChangeEndDate(0);
                        return rtn;
                    }
                } 
                var txtzipcode = document.getElementById("txtZipCode");
                txtzipcode.value = txtZipCode.GetText();
                //FB 2222
                if (txtzipcode.value != '' && txtzipcode.value.search(/^(a-z|A-Z|0-9)*[^\\/<>^+;?|!`,\[\]{}\x22;=:@#$%&()'~]*$/)==-1)
                {
                    regZipCode.style.display = 'block';
                    txtzipcode.focus();
                    return false;
                }
            }
            else if (reportsList.value == "8")
            {
                //window.location.replace("UtilizationReport.aspx"); //FB 2565
                window.location='UtilizationReport.aspx';
                return false;
            }
        }
    }

    var language = '<%=Session["language"]%>';

//FB 2670 Starts
    function getVNOCEmailList() {
        var Confvnoc = document.getElementById("hdnVNOCOperator");
        url = "VNOCparticipantlist.aspx?cvnoc=" + Confvnoc.value;
        winrtc = window.open(url, "", "width=920,height=470,top=0,left=0,resizable=yes,scrollbars=yes,status=no"); //FB 2735
    }    
     //FB 2047
    function fnAdvanceReports(param) 
    {
      DataLoading(1);//ZD 100176
        if(param == "A")
            window.location='managereports.aspx'; //FB 2565
        else if(param == "M")
            window.location='MasterChildReport.aspx';
            
        return false;
    }
    //FB 2505
    function fnScrollTop() 
    {
       window.scrollTo(0, 0);
       document.getElementById('hideScreen').style.display = "none";
    }

    //ZD 102468 - Start
    setTimeout("fnScrollTop()", 1);


    setTimeout("fnNoData()", 1000);

    function fnNoData()
    {
        try
        {
            if(document.getElementById('gvReportDetails_DXMainTable') != null)
            {
                if(document.getElementById('gvReportDetails_DXMainTable').innerHTML.length < 200)
                {
                    if(document.getElementById('gvReportDetails_DXMainTable').offsetWidth > 1000)
                    {
                        var reportwidth = document.getElementById('gvReportDetails').offsetWidth - document.getElementById('gvReportDetails_DXMainTable').offsetWidth;

                        if(navigator.userAgent.indexOf('Trident') > -1)
                            document.getElementById('gvReportDetails_DXMainTable').childNodes[0].childNodes[0].childNodes[0].childNodes[0].style.marginLeft = reportwidth + "px";
                        else
                            document.getElementById('gvReportDetails_DXMainTable').childNodes[1].childNodes[0].childNodes[1].childNodes[0].style.marginLeft = reportwidth + "px";
                
                    }
                }
            }
        }
        catch(ex)
        {
            console.log(ex);
        }
    }
    //ZD 102468 - End

    
</script>

<!-- #INCLUDE FILE="inc/mainbottomNET.aspx" -->
<%--ZD 101388 Commented--%>
<%--<!-- FB 2719 Starts -->
<% if (Session["isExpressUser"].ToString() == "1"){%>
<!-- #INCLUDE FILE="inc/mainbottomNETExp.aspx" -->
<%}else{%>
<!-- #INCLUDE FILE="inc/mainbottomNET.aspx" -->
<%}%>
<!-- FB 2719 Ends -->--%>

