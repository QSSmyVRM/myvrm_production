<%--ZD 100147 start--%>
<%--/* Copyright (C) 2015 myVRM - All Rights Reserved	
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.		
*
* You should have received a copy of the myVRM license with	
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/--%><%--ZD 100147 ZD 100866 End--%>
<%@ Page language="c#" Inherits="myVRMAdmin.Web.en.Esthetic" %>
<%@ Register TagPrefix="cc1" Namespace="myVRMWebControls" Assembly="myVRMWebControls" %><%--ZD 104387--%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> <!-- FB 2050 -->
<meta http-equiv="X-UA-Compatible" content="IE=8" /> <!-- FB 2050 -->
<!-- <!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" > Commented for FB 2050 -->
<script type="text/javascript" language="JavaScript" src="inc/functions.js"></script> <%-- ZD 102723 --%>
<HTML>
	<!-- #INCLUDE FILE="inc/maintopNET.aspx" --> 
    <%--<style type="text/css">.hdiv{position:absolute;left:0; visibility:hidden;overflow:scroll;}</style>--%>
    <%--ZD 100156--%>
    <style type="text/css">
    .hdiv{ width:300px; height:200px; position:absolute; 
    left:150px; margin-left: auto; margin-right: auto;display:none;
}</style>
	<BODY bottomMargin="0" leftMargin="0" topMargin="0" rightMargin="0">
		<script language="javascript" src="../en/Organizations/Original/Javascript/RGBColorPalette.js"> </script><%-- FB 1830--%>
		<script language="JavaScript">
<!--
  
var imtime1 = parseInt("", 10);
imtime1 = ( (imtime1 < 0.5) || isNaN(imtime1) ) ? 0.5 : imtime1;

//-->
//ZD 100604 start
var img = new Image();
img.src = "../en/image/wait1.gif";
//ZD 100604 End
//Code added for FB 1473 - start
function fnShow()
{
    var args = fnShow.arguments;
    var defaultThemeRow = document.getElementById("DefaultThemeRow");
    var customizeRow = document.getElementById("CustomizeRow");
    var btnsave = document.getElementById("btnSave");
    var btnpreview = document.getElementById("btnPreview");
    
    if(args[0] == "1")
    {
        customizeRow.style.display = '';//Edited For FF..
        btnsave.value = RSsubmit;
        btnpreview.style.display = '';//Edited For FF..
        document.getElementById("hdnValue").value = '';
    }
    else if(args[0] == "0")
    {
        customizeRow.style.display = 'None';
        btnsave.value = RSapplyTheme;
        btnpreview.style.display = 'None';
    }
}

function fnValue()
{
    var args = fnValue.arguments;
    document.getElementById("hdnValue").value = args[0];  
    fnShow('0');
}
//ZD 100156 - Start
function toggleDiv(id,flagit) 
{
    if (flagit=="1")
    {
        if (document.layers) document.layers[''+id+''].visibility = "show"
        else if (document.all) document.all[''+id+''].style.display = "block"
        else if (document.getElementById) document.getElementById('' + id + '').style.display = "block"
    }
    else if (flagit=="0")
    {
        if (document.layers) document.layers[''+id+''].visibility = "hide"
        else if (document.all) document.all['' + id + ''].style.display = "none"
        else if (document.getElementById) document.getElementById('' + id + '').style.display = "none"
    }
}
//ZD 100156 - End
//Code added for FB 1473 - end
//ZD 100176 start
function DataLoading(val) {
    if (val == "1")
        document.getElementById("dataLoadingDIV").style.display = 'block'; //ZD 100678
    else
        document.getElementById("dataLoadingDIV").style.display = 'none'; //ZD 100678
}
//ZD 100176 End

</script>

   <%-- All 'Organizations/Original' path changed to '..' for FB 1830--%>
		<!-- script finished -->
		<TABLE cellPadding="2" width="100%" border="0">
			<TBODY>
				<TR>
					<TD><!--------------------------------- CONTENT START HERE ---------------> <!--------------------------------- CONTENT GOES HERE --------------->  <!-- Java Script Begin -->
						<!-- Java Script End -->
						<FORM id="frmSample" name="frmSample" method="post" runat="server">
                        <input type="hidden" id="Map1ImageDt" runat="server" /><%--ZD 104387--%>
                        <input type="hidden" id="Map2ImageDt" runat="server" /><%--ZD 104387--%>
						<input type="hidden" id="hdnValue" runat="server" />
                         <%--ZD 101022 start--%>
                                        <asp:ScriptManager ID="scpMgrUI" runat="server" EnableScriptLocalization="true" >
		                                    <Scripts>                
			                                    <asp:ScriptReference Path= "~/ResourceScript/StringResources.js" ResourceUICultures="<%$ Resources:WebResources, UICulture%>"  />
		                                    </Scripts>
	                                    </asp:ScriptManager> <%--ZD 101022 End--%>
						<CENTER style="height:30"> <!-- FB 1633 -->
							<H3><asp:Literal Text="<%$ Resources:WebResources, UISettings_UserInterface%>" runat="server" /></H3><!-- FB 2570 --><%--FB 2787--%>
							<asp:label id="errLabel" Runat="server" CssClass="lblError"></asp:label>
							 <div id="dataLoadingDIV" name="dataLoadingDIV" align="center" style="display:none">
                                <img border='0' src='image/wait1.gif' alt='<asp:Literal ID="Literal1" Text='<%$ Resources:WebResources, Loading%>' runat='server' />' />
                            </div><%--ZD 100678 End--%>
						</CENTER>
							<CENTER> 
								<TABLE id="Table5" cellspacing="0" cellpadding="0" width="100%" align="center" border="0"><!-- FB 1633 --> <%--FB 1775--%>
									<TBODY>
									<TR>
											
											<TD align="left" width="20%" height="20"><!-- FB 1633 -->
											<SPAN class="subtitleblueblodtext">&nbsp;<asp:Literal Text="<%$ Resources:WebResources, UISettings_CustomizeImage%>" runat="server"></asp:Literal></SPAN>
											</TD> <%--FB 1775--%>
										</TR>
									    <tr>
											
											<td vAlign="top" align="center" width="100%">
												<table id="tablebanner" cellSpacing="0" cellpadding="2" width="100%" align="left" border="0">
													<tbody>
														<tr><!-- FB 1633 --><%--FB 2579 Start--%>
															<td valign="top" align="left" width="16%" class="blackblodtext">&nbsp;&nbsp;<asp:Literal Text="<%$ Resources:WebResources, UISettings_StandardResolu%>" runat="server"></asp:Literal></td> <%--FB 1775--%>
															<%--ZD 102277 Start--%>
															<td align="left" width="63%" valign="top">
                                                            <%--ZD 104387 Start--%>
                                                            <table cellpadding="0px" cellspacing="0px" width="100%" border="0">
                                                            <tr>
                                                            <td align="left" nowrap="nowrap">
                                                                <div>
                                                                    <input type="text" class="file_input_textbox" readonly="readonly" value='<%$ Resources:WebResources, Nofileselected%>' runat="server"/>
                                                                    <div class="file_input_div"><input type="button" value="<%$ Resources:WebResources, Browse%>" runat="server" class="file_input_button" onclick="document.getElementById('Bannerfile1024').click();return false;"  />
                                                                        <input type="file" class="file_input_hidden" tabindex="-1" id="Bannerfile1024" accept="image/*" contenteditable="false" enableviewstate="true" runat="server" onchange="getfilename(this);fnValidateFileName(this.value,1);"/></div></div><%--FB 3055-Filter in Upload Files--%><%--ZD 100419--%>
                                                                        <table>
                                                                            <tr>
                                                                                <td style="vertical-align:top; padding-left:10px; padding-right:40px;">
															                        <span class="blackItalictext"> <asp:Literal Text="<%$ Resources:WebResources, UISettings_200x75pixels%>" runat="server" /></span><%-- FB 2779 --%>
                                                                                    <asp:RegularExpressionValidator ID="regBannerfile1024" runat="server" Display="Dynamic" ValidationGroup="Submit1" ControlToValidate="Bannerfile1024" CssClass="lblError" ErrorMessage="<%$ Resources:WebResources, Filetypeinvalid%>" ValidationExpression="^.*\.((j|J)(p|P)(e|E)?(g|G)|(g|G)(i|I)(f|F)|(p|P)(n|N)(g|G)|(b|B)(m|M)(p|P))$"></asp:RegularExpressionValidator>                                                                            
                                                                                </td>
                                                                                <td  width="30px" style="vertical-align:top;padding-right:10px;">
                                                                                    <cc1:ImageControl ID="Map1ImageCtrl" Width="30" Height="30" Visible="false" runat="server">
                                                                                        </cc1:ImageControl>  
                                                                                </td>
                                                                                <td>                                                                            
                                                                                    <asp:ImageButton ImageUrl="image/btn_delete.gif" style="vertical-align:top" runat="server" ID="RImg1" AlternateText="<%$ Resources:WebResources, ConferenceList_btnDelete%>" ToolTip="<%$ Resources:WebResources, ConferenceList_btnDelete%>"  OnClientClick="javascript:DataLoading(1);"/><%--ZD 100176--%> <%--ZD 100419--%>                                                                                    
                                                                                </td>                                                                        
                                                                            </tr>
                                                                        </table>
                                                                        <%--ZD 104387 End--%>                                                                        
                                                                </td>
                                                                </tr>
                                                                <tr>
                                                                <td> 
                                                                     <%--ZD 104387 Start--%>                                                                   
                                                                    <asp:Label ID="lblstdres" Text="" Visible="false" ForeColor=Red runat="server"></asp:Label>
                                                                    <asp:RegularExpressionValidator ID="regFNVal1" ControlToValidate="Bannerfile1024" Display="dynamic" runat="server" ValidationGroup="Submit1" SetFocusOnError="true"
                                                                        ErrorMessage="<%$ Resources:WebResources, InvalidCharacters46%>" ValidationExpression="^[^<>&#]*$"></asp:RegularExpressionValidator>
                                                                    <%--ZD 104387 End--%>
                                                                </td>

                                                                </tr>
																<%--ZD 102277 End--%>
															    </table><!-- FB 2739 -->
															</td>															
														</tr>
														<%--Commented for FB 1633 start--%>
														<tr>
                                                        <%--ZD 104387 Start--%>
                                                            <td valign="top" align="left" width="16%" class="blackblodtext" style="padding-top:15px">&nbsp;&nbsp;<asp:Literal Text="<%$ Resources:WebResources, UISettings_CompBackground%>" runat="server"></asp:Literal></td>															
															<td align="left" valign="top" style="padding-top:15px">
                                                                <table cellpadding="0px" cellspacing="0px" width="100%" border="0">
                                                                    <tr>
                                                                        <td align="left" nowrap="nowrap">
                                                                            <div>
                                                                                <input style="height:18px" type="text" class="file_input_textbox" readonly="readonly" value='<%$ Resources:WebResources, Nofileselected%>' runat="server"/>
                                                                                <div class="file_input_div"><input type="button" value="<%$ Resources:WebResources, Browse%>" runat="server" class="file_input_button" onclick="document.getElementById('companylogo').click();return false;"  />
                                                                                    <input type="file" class="file_input_hidden" tabindex="-1" id="companylogo" accept="image/*" contenteditable="false" enableviewstate="true" runat="server" onchange="getfilename(this);fnValidateFileName(this.value,2);"/>
                                                                                </div>
                                                                             </div>
                                                                             <table>
                                                                                <tr>
                                                                                    <td style="vertical-align:top; padding-left:10px; padding-right:27px;">                                                                                
                                                                                        <span class="blackItalictext"> <asp:Literal Text="<%$ Resources:WebResources, UISettings_1600x524pixels%>" runat="server" /></span>                                                                                 
                                                                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" Display="Dynamic" ValidationGroup="Submit1" ControlToValidate="Bannerfile1024" CssClass="lblError" ErrorMessage="<%$ Resources:WebResources, Filetypeinvalid%>" ValidationExpression="^.*\.((j|J)(p|P)(e|E)?(g|G)|(g|G)(i|I)(f|F)|(p|P)(n|N)(g|G)|(b|B)(m|M)(p|P))$"></asp:RegularExpressionValidator>
                                                                                     </td>
                                                                                     <td width="30px" style="vertical-align:top;padding-right:10px;">
                                                                                        <cc1:ImageControl ID="Map2ImageCtrl" Width="30" Height="30" Visible="false" runat="server">
                                                                                            </cc1:ImageControl>
                                                                                     </td>
                                                                                     <td>
                                                                                        <asp:ImageButton ImageUrl="image/btn_delete.gif" style="vertical-align:top" runat="server" ID="RImg3" AlternateText="<%$ Resources:WebResources, ConferenceList_btnDelete%>"  ToolTip="<%$ Resources:WebResources, ConferenceList_btnDelete%>" /> 
                                                                                         &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                                                        <button ID="btnBanner" type="button" runat="server" style="vertical-align:top" validationgroup="Submit1" onclick="javascript:return fnUpload(document.getElementById('Bannerfile1024'), document.getElementById('companylogo'), this.id, 'Submit1');" onserverclick="btnBanner_Click" class="altMedium0BlueButtonFormat"><asp:Literal Text='<%$ Resources:WebResources, UISettings_btnBanner%>' runat='server' /></button>
                                                                                     </td>
                                                                                </tr>													                
                                                                            </table>
															                <asp:Label ID="Label1" Text="" Visible="false" ForeColor=Red runat="server"></asp:Label>
                                                                            <asp:Label ID="Label2" Text="" Visible="false" ForeColor=Red runat="server"></asp:Label>															                
                                                                            <asp:ImageButton ImageUrl="image/btn_delete.gif" runat="server" ID="RImg2" AlternateText="<%$ Resources:WebResources, ConferenceList_btnDelete%>" ToolTip="<%$ Resources:WebResources, ConferenceList_btnDelete%>" visible="false"  /> <%--ZD 100419--%>
															                <INPUT class="altText" id="Bannerfile1600" type="file" size="60" name="Banner1600" runat="server" visible="false">
															                <span class="blackItalictext" > <%--(1380 x 72 pixels)--%></span>															                
															                <%--ZD 100419--%>
															            </td>
                                                                    </tr>
                                                                </table>
                                                                <%--ZD 104387 End--%>															
															</td>
														</tr>
														<%--Commented for FB 1633 end--%>
													</tbody>
												</table>
											</td>
										</tr>
								         <%--Code added for FB 1473 - start--%>
								         <tr> 
								            <td colspan="3" width="100%">
								                <table border="0"  width="100%">
										        	<tr>
											            <td align="left" width="20%"><span class="subtitleblueblodtext"><asp:Literal Text="<%$ Resources:WebResources, UISettings_UIThemes%>" runat="server"></asp:Literal></span></td>
            <%--												<table id="tabletitle" width="25" align="right" border="0">
													            <tr>
														            <td align="center" class="tableHeader" height="20">3</td>
													            </tr>
												            </table></td>
            --%>											<!-- UI Changes for FB 1775 -->
											            <td width="1%" height="20">&nbsp;</td>
											            <td align="left" height="20" ></td>
										            </tr>
										            <tr>
                    						            <td ></td>
									                    <td vAlign="top" align="center" colspan="2">
											                <table id="table1" cellSpacing="0" cellPadding="2" width="100%" align="center" border="0">
												                <tbody>
													                <tr>													                    
														                <td align="left"> <%-- FB 2050 --%><%-- ZD 100425 Starts--%>
															                <table cellspacing="0" cellpadding="5" class="tableBody" border="0" width="95%">
															                    <tr class="tableHeader">
                                                                                    <td align="left" class="tableHeader" width="7%"><asp:Literal Text='<%$ Resources:WebResources, EndpointSearch_SelectEP%>' runat='server' /></td>
                                                                                    <td align="left" class="tableHeader" nowrap="nowrap"><asp:Literal Text="<%$ Resources:WebResources, UISettings_ThemeName%>" runat="server" /></td>
                                                                                    <td align="center" class="tableHeader" width="30%" ><asp:Literal Text="<%$ Resources:WebResources, UISettings_ThemeView%>" runat="server" /></td>
                                                                                    <td align="left" class="tableHeader" width="7%"><asp:Literal Text='<%$ Resources:WebResources, EndpointSearch_SelectEP%>' runat='server' /></td>
                                                                                    <td align="left" class="tableHeader" nowrap="nowrap"><asp:Literal Text="<%$ Resources:WebResources, UISettings_ThemeName%>" runat="server" /></td>
                                                                                    <td align="center" class="tableHeader" width="30%" ><asp:Literal Text="<%$ Resources:WebResources, UISettings_ThemeView%>" runat="server" /></td>
                                                                                    <td align="left" class="tableHeader" width="7%"><asp:Literal Text='<%$ Resources:WebResources, EndpointSearch_SelectEP%>' runat='server' /></td>
                                                                                    <td align="left" class="tableHeader" nowrap="nowrap"><asp:Literal Text="<%$ Resources:WebResources, UISettings_ThemeName%>" runat="server" /></td>
                                                                                    <td align="center" class="tableHeader" width="30%" ><asp:Literal Text="<%$ Resources:WebResources, UISettings_ThemeView%>" runat="server" /></td>										                            
                                                                                </tr><%-- ZD 100425 End--%>
                                                                                 <tr>
                                                                                    <td>
                                                                                        <asp:RadioButton ID="RDefault" onclick="javascript:fnValue('Default')" runat="server" GroupName="Theme"  />
                                                                                    </td>										                            
                                                                                    <td class="tableBody"><asp:Literal Text="<%$ Resources:WebResources, UISettings_Default%>" runat="server"></asp:Literal></td>
                                                                                    <td align="center">
                                                                                        <asp:ImageButton ToolTip="<%$ Resources:WebResources, fullsizeImage%>"  BorderStyle="groove" BorderWidth="1" ID="ImageButton1" OnClientClick="javascript:toggleDiv('DefaultDiv',1);return false;" ImageUrl="Image/Default.bmp"  runat="server" Width="60" Height="40" AlternateText="<%$ Resources:WebResources, DefaultThemeView%>"/> <%--ZD 100419--%>
                                                                                        <div align="center" class='hdiv' id='DefaultDiv' style=" width:950px;height:550px;border-style:groove; background-color:White; border-color:Menu;z-index:8;position:absolute;">
                                                                                            <input type="button" value="<%$ Resources:WebResources, BridgeDetails_BtnClose%>" runat="server" onfocus="this.blur()" name="cls1" class="altShortBlueButtonFormat" onclick="javascript:return toggleDiv('DefaultDiv',0)"/>
                                                                                        <img src='Image/Default.bmp' alt="<asp:Literal Text='<%$ Resources:WebResources, UISettings_Default%>' runat='server' />" /></div><%-- FB 2779 --%> <%--ZD 100419--%>
                                                                                    </td>
                                                                                    <td>
                                                                                        <asp:RadioButton ID="RTheme1" onclick="javascript:fnValue('Theme1')" runat="server" GroupName="Theme"  />
                                                                                    </td>										                            
                                                                                    <td class="tableBody"><asp:Literal Text="<%$ Resources:WebResources, UISettings_Blue%>" runat="server"></asp:Literal></td>
                                                                                    <td align="center">
                                                                                        <asp:ImageButton ToolTip="<%$ Resources:WebResources, fullsizeImage%>"  BorderStyle="groove" BorderWidth="1" ID="Theme1" OnClientClick="javascript:toggleDiv('Theme1Div',1);return false;" ImageUrl="Image/Theme1.bmp"  runat="server" Width="60" Height="40"  AlternateText="<%$ Resources:WebResources, BlueThemeView%>"/> <%--ZD 100419--%> 
                                                                                        <div align="center" class='hdiv' id='Theme1Div' style=" width:950px;height:550px;border-style:groove; background-color:White; border-color:Menu;z-index:8;position:absolute;">
                                                                                            <input type="button" value="<%$ Resources:WebResources, BridgeDetails_BtnClose%>" runat="server" onfocus="this.blur()" name="cls1" class="altShortBlueButtonFormat" onclick="javascript:return toggleDiv('Theme1Div',0)"/>
                                                                                        <img src='Image/Theme1.bmp' alt="Theme1"  /></div><%-- FB 2779 --%> <%--ZD 100419--%>
                                                                                    </td>
                                                                                    <td>
                                                                                        <asp:RadioButton ID="RTheme2" onclick="javascript:fnValue('Theme2')" runat="server" GroupName="Theme"  />
                                                                                    </td>
                                                                                    <td class="tableBody"><asp:Literal Text="<%$ Resources:WebResources, UISettings_Red%>" runat="server"></asp:Literal></td>
                                                                                    <td align="center">
                                                                                        <asp:ImageButton tooltip="<%$ Resources:WebResources, fullsizeImage%>" borderstyle="groove" borderwidth="1" id="Theme2" onclientclick="javascript:toggleDiv('Theme2Div',1);return false;" imageurl="Image/Theme2.bmp" runat="server" width="60" height="40" AlternateText="Red Theme View"></asp:ImageButton>
                                                                                        <div align="center" class='hdiv' id='Theme2Div' style="width:950px;height:550px;border-style:groove; background-color:White; border-color:ActiveBorder;z-index:8;position:absolute;">
                                                                                            <input type="button" value="<%$ Resources:WebResources, BridgeDetails_BtnClose%>" runat="server" onfocus="this.blur()" class="altShortBlueButtonFormat" onclick="javascript:return toggleDiv('Theme2Div',0)"/>
                                                                                        <img src='Image/Theme2.bmp' alt="Theme2"/></div><%-- FB 2779 --%> <%--ZD 100419--%>
                                                                                    </td>
                                                                                </tr>										                        
                                                                                <tr style="display:none"><%-- FB 2739 & FB 2719--%>
                                                                                    <td>
                                                                                        <asp:RadioButton ID="RTheme3" onclick="javascript:fnValue('Theme3')" runat="server" GroupName="Theme"  />
                                                                                    </td>
                                                                                    <td class="tableBody">
                                                                                        Earth
                                                                                    </td>
                                                                                    <td align="center">
                                                                                        <asp:ImageButton ToolTip="<%$ Resources:WebResources, fullsizeImage%>" BorderStyle="groove" BorderWidth="5" ID="Theme3" OnClientClick="javascript:toggleDiv('Theme3Div',1);return false;" ImageUrl="Image/Theme3.bmp" runat="server" Width="60" Height="40"   />
                                                                                        <div align="center" class='hdiv' id='Theme3Div' style="width:950px;height:550px;border-style:groove; background-color:White; border-color:ActiveBorder;z-index:8;position:absolute;">
                                                                                            <input type="button" value="<%$ Resources:WebResources, BridgeDetails_BtnClose%>" runat="server" onfocus="this.blur()" class="altShortBlueButtonFormat" onclick="javascript:return toggleDiv('Theme3Div',0)"/>
                                                                                        <img src='Image/Theme3.bmp' alt="Theme3" /></div> <%--ZD 100419--%>
                                                                                    </td>
                                                                                    <td>
                                                                                        <asp:RadioButton ID="RTheme4" onclick="javascript:fnValue('Theme4')" runat="server" GroupName="Theme"  />
                                                                                    </td>
                                                                                    <td class="tableBody">
                                                                                        Green
                                                                                    </td>
                                                                                    <td align="center">
                                                                                        <asp:ImageButton ToolTip="<%$ Resources:WebResources, fullsizeImage%>" BorderStyle="groove" BorderWidth="5" ID="Theme4" OnClientClick="javascript:toggleDiv('Theme4Div',1);return false;" ImageUrl="Image/Theme4.bmp" runat="server" Width="60" Height="40"   />
                                                                                        <div align="center" class='hdiv' id='Theme4Div' style="width:950px;height:550px;border-style:groove; background-color:White; border-color:ActiveBorder;z-index:8;position:absolute;">
                                                                                            <input type="button" value="<%$ Resources:WebResources, BridgeDetails_BtnClose%>" runat="server" onfocus="this.blur()" class="altShortBlueButtonFormat" onclick="javascript:return toggleDiv('Theme4Div',0)"/>
                                                                                        <img src='Image/Theme4.bmp' alt="Theme4" /></div><%--ZD 100419--%>
                                                                                    </td>										                        									                        
                                                                                    <td>
                                                                                        <asp:RadioButton ID="RTheme5" onclick="javascript:fnValue('Theme5')" runat="server" GroupName="Theme"  />
                                                                                    </td>
                                                                                    <td class="tableBody">
                                                                                        Pastel
                                                                                    </td>
                                                                                    <td align="center">
                                                                                        <asp:ImageButton ToolTip="<%$ Resources:WebResources, fullsizeImage%>" BorderStyle="groove" BorderWidth="5" ID="Theme5" OnClientClick="javascript:toggleDiv('Theme5Div',1);return false;" ImageUrl="Image/Theme5.bmp" runat="server" Width="60" Height="40"   />
                                                                                        <div align="center" class='hdiv' id='Theme5Div' style="width:950px;height:550px;border-style:groove; background-color:White; border-color:ActiveBorder;z-index:8;position:absolute;">
                                                                                            <input type="button" value="<%$ Resources:WebResources, BridgeDetails_BtnClose%>" runat="server" onfocus="this.blur()" class="altShortBlueButtonFormat" onclick="javascript:return toggleDiv('Theme5Div',0)"/>
                                                                                        <img src='Image/Theme5.bmp' alt="Theme5" /></div><%--ZD 100419--%>
                                                                                    </td>
                                                                                 </tr> 
                                                                                <tr style="display:none"><%-- FB 2739 & FB 2719--%>
                                                                                    <td>
                                                                                        <br />
                                                                                        <asp:RadioButton ID="RThemeCustom" OnClick="javascript:return fnShow('1');" runat="server" GroupName="Theme"  />
                                                                                    </td>
                                                                                    <td class="tableBody">
                                                                                        <br /><asp:Literal Text="<%$ Resources:WebResources, UISettings_Custom%>" runat="server"></asp:Literal></td>
                                                                                    <td align="center">
                                                                                    </td>
                                                                                    <td colspan="6"></td>
                                                                                </tr>
                                                                            </table>
                                                                         </td>
                                                                    </tr>
                                                                </tbody>
                                                             </table>
                                                         </td>
                                                    </tr>            
										        </table>
            								</td>
            						    </tr>
										<tr id="CustomizeRow" runat="server" style="display:none" width="100%"><%--ZD 100263--%>
										    <td colspan="3" width="100%">
										    <%--FB 1633 alignment start--%>
										        <table border="0" width="100%"> <%--edited for FF--%>
                                                    <tr> 
                                                        <td align="left" width="20%"><span class="subtitleblueblodtext"><asp:Literal Text="<%$ Resources:WebResources, UISettings_CustomizeMenus%>" runat="server"></asp:Literal></span></td>
											            <td width="1%" height="20">&nbsp;</td>
											            <td align="left" height="20" ></td>                             
											         </tr>
										            <tr>
                    						            <td height="20"></td>
										                <td valign="top" align="left" colspan="2">
										                    <table width="100%" border="0">
										                        <tbody>
														            <tr>
															            <td align="left">
																            <table cellSpacing="0" cellPadding="3" bgColor="#e1e1e1" border="0" width="95%">
																	            <tr class="tableHeader">
																		            <td align="center" width="29%" rowspan="2" nowrap="" class="tableHeader"><asp:Literal Text="<%$ Resources:WebResources, UISettings_Description%>" runat="server"></asp:Literal></td>
																		            <td align="center" width="37%" colspan="2" nowrap="" class="tableHeader"><asp:Literal Text="<%$ Resources:WebResources, UISettings_MenuBar%>" runat="server"></asp:Literal></td>
																		            <td align="center" width="35%" colspan="2" nowrap="" class="tableHeader"><asp:Literal Text="<%$ Resources:WebResources, UISettings_DropdownList%>" runat="server"></asp:Literal></td>
																	            </tr>
																	            <tr class="tableHeader">
																		            <td align="center" class="tableHeader"><asp:Literal Text="<%$ Resources:WebResources, UISettings_Current%>" runat="server"></asp:Literal></td>
																		            <td align="center" class="tableHeader"><asp:Literal Text="<%$ Resources:WebResources, UISettings_New%>" runat="server"></asp:Literal></td>
																		            <td align="center" class="tableHeader"><asp:Literal Text="<%$ Resources:WebResources, UISettings_Current%>" runat="server"></asp:Literal></td>
																		            <td align="center" class="tableHeader"><asp:Literal Text="<%$ Resources:WebResources, UISettings_New%>" runat="server"></asp:Literal></td>
																	            </tr>
																	            <tr>
																		            <td style="PADDING-RIGHT: 0px; PADDING-LEFT: 0px; PADDING-BOTTOM: 0px; PADDING-TOP: 0px" colSpan="5" bgcolor="#ffffff" >
																			            <div id="md" style="BORDER-RIGHT: #e1e1e1 0px solid; BORDER-TOP: #e1e1e1 1px solid; OVERFLOW: auto; BORDER-LEFT: #e1e1e1 0px solid; WIDTH: 100%; BORDER-BOTTOM: #e1e1e1 0px solid; POSITION: relative;">
																				            <table id="Table10"  cellSpacing="1" cellPadding="3" class="tableBody" border="0" width="100%">
																					            <tr>
																						            <td class="tableBody"><asp:Literal Text="<%$ Resources:WebResources, UISettings_MouseOffFont%>" runat="server"></asp:Literal></td>
																						            <td align="left" class="tableBody"><asp:Label id="lblm_offfont" runat="server" borderwidth="1px" bordercolor="#000000" borderstyle="Solid"></asp:Label>&nbsp;<asp:Label id="lblm_offfont1" runat="server"></asp:Label></td>
																						            <td>
																							            <table>
																								            <tr>
																									            <td><asp:textbox EnableViewState="True" id="m_mouseofffont" Runat="server" CssClass="altText" Width="70px"></asp:textbox></td>
																									            <td><img title="<asp:Literal Text='<%$ Resources:WebResources, SelectColor%>' runat='server' />" onclick="FnShowPalette(event,'m_mouseofffont')"
																											            height="23" alt="Color" src="../Image/color.jpg" width="20" name="image3" /><%--ZD 100419--%>    <%-- Organization Css Module  --%>
																									            </td>
																								            </tr>
																							            </table>
																						            </td>
																						            <td align="left" class="tableBody"><asp:Label ID="lbld_offfont" Runat="server" BorderWidth="1px" BorderColor="#000000" BorderStyle="Solid"></asp:Label>&nbsp;<asp:Label ID="lbld_offfont1" Runat="server"></asp:Label></td>
																						            <td>
																							            <table>
																								            <tr>
																									            <td><asp:textbox EnableViewState="True" id="d_mouseofffont" Runat="server" CssClass="altText" Width="70px"></asp:textbox></td>
																									            <td><img title="<asp:Literal Text='<%$ Resources:WebResources, SelectColor%>' runat='server' />" onclick="FnShowPalette(event,'d_mouseofffont')"
																											            height="23" alt="Color" src="../Image/color.jpg" width="20" name="image4"/><%--ZD 100419--%><%-- Organization Css Module  --%>
																									            </td>
																								            </tr>
																							            </table>
																						            </td>
																					            </tr>
																					            <tr>
																						            <td class="tableBody"><asp:Literal Text="<%$ Resources:WebResources, UISettings_MouseOffBackg%>" runat="server"></asp:Literal></td>
																						            <td align="left" class="tableBody"><asp:Label id="lblm_Offback" runat="server" borderwidth="1px" bordercolor="#000000" borderstyle="Solid"></asp:Label>&nbsp;<asp:Label id="lblm_Offback1" runat="server"></asp:Label></td>
																						            <td>
																							            <table>
																								            <tr>
																									            <td><asp:textbox EnableViewState="True" id="m_mouseOffback" Runat="server" CssClass="altText" Width="70px"></asp:textbox></td>
																									            <td>
																										            <img title="<asp:Literal Text='<%$ Resources:WebResources, SelectColor%>' runat='server' />" onclick="FnShowPalette(event,'m_mouseOffback')" height="23" alt="Color" src="../Image/color.jpg" width="20" name="image5"/><%--ZD 100419--%><%-- Organization Css Module  --%>
																									            </td>
																								            </tr>
																							            </table>
																						            </td>
																						            <td align="left" class="tableBody"><asp:Label ID="lbld_Offback" Runat="server" BorderWidth="1px" BorderColor="#000000" BorderStyle="Solid"></asp:Label>&nbsp;<asp:Label ID="lbld_Offback1" Runat="server"></asp:Label></td>
																						            <td>
																							            <table>
																								            <tr>
																									            <td><asp:textbox EnableViewState="True" id="d_mouseOffback" Runat="server" CssClass="altText" Width="70px" ></asp:textbox></td>
																									            <td><img title="<asp:Literal Text='<%$ Resources:WebResources, SelectColor%>' runat='server' />" onclick="FnShowPalette(event,'d_mouseOffback')"
																											            height="23" alt="Color" src="../Image/color.jpg" width="20" name="image6"/><%--ZD 100419--%> <%-- Organization Css Module  --%>
																									            </td>
																								            </tr>
																							            </table>
																						            </td>
																					            </tr>
																					            <tr>
																						            <td class="tableBody"><asp:Literal Text="<%$ Resources:WebResources, UISettings_MouseOnFont%>" runat="server"></asp:Literal></td>
																						            <td align="left" class="tableBody"><asp:Label id="lblm_Onfont" runat="server" borderwidth="1px" bordercolor="#000000" borderstyle="Solid"></asp:Label>&nbsp;<asp:Label id="lblm_Onfont1" runat="server"></asp:Label></td>
																						            <td>
																							            <table>
																								            <tr>
																									            <td><asp:textbox EnableViewState="True" id="m_mouseOnfont" Runat="server" CssClass="altText" Width="70px"></asp:textbox></td>
																									            <td><img title="<asp:Literal Text='<%$ Resources:WebResources, SelectColor%>' runat='server' />" onclick="FnShowPalette(event,'m_mouseOnfont')"
																											            height="23" alt="Color" src="../Image/color.jpg" width="20" name="image7" /><%--ZD 100419--%> <%-- Organization Css Module  --%>
																									            </td>
																								            </tr>
																							            </table>
																						            </td>
																						            <td align="left" class="tableBody"><asp:Label ID="lbld_Onfont" Runat="server" BorderWidth="1px" BorderColor="#000000" BorderStyle="Solid"></asp:Label>&nbsp;<asp:Label ID="lbld_Onfont1" Runat="server"></asp:Label></td>
																						            <td>
																							            <table>
																								            <tr>
																									            <td><asp:textbox EnableViewState="True" id="d_mouseOnfont" Runat="server" CssClass="altText" Width="70px"></asp:textbox></td>
																									            <td><img title="<asp:Literal Text='<%$ Resources:WebResources, SelectColor%>' runat='server' />" onclick="FnShowPalette(event,'d_mouseOnfont')"
																											            height="23" alt="Color" src="../Image/color.jpg" width="20" name="image8"/> <%--ZD 100419--%><%-- Organization Css Module  --%>
																									            </td>
																								            </tr>
																							            </table>
																						            </td>
																					            </tr>
																					            <tr>
																						            <td class="tableBody"><asp:Literal Text="<%$ Resources:WebResources, UISettings_MouseOnBackgr%>" runat="server"></asp:Literal></td>
																						            <td align="left" class="tableBody"><asp:Label id="lblm_Onback" runat="server" borderwidth="1px" bordercolor="#000000" borderstyle="Solid"></asp:Label>&nbsp;<asp:Label id="lblm_Onback1" runat="server"></asp:Label></td>
																						            <td>
																							            <table>
																								            <tr>
																									            <td><asp:textbox EnableViewState="True" id="m_mouseOnback" Runat="server" CssClass="altText" Width="70px"></asp:textbox></td>
																									            <td><img title="<asp:Literal Text='<%$ Resources:WebResources, SelectColor%>' runat='server' />" onclick="FnShowPalette(event,'m_mouseOnback')"
																											            height="23" alt="Color" src="../Image/color.jpg" width="20" name="image9"/><%--ZD 100419--%> <%-- Organization Css Module  --%>
																									            </td>
																								            </tr>
																							            </table>
																						            </td>
																						            <td align="left" class="tableBody"><asp:Label ID="lbld_Onback" Runat="server" BorderWidth="1px" BorderColor="#000000" BorderStyle="Solid"></asp:Label>&nbsp;<asp:Label ID="lbld_Onback1" Runat="server"></asp:Label></td>
																						            <td>
																							            <table>
																								            <tr>
																									            <td><asp:textbox EnableViewState="True" id="d_mouseOnback" Runat="server" CssClass="altText" Width="70px"></asp:textbox></td>
																									            <td><img title="<asp:Literal Text='<%$ Resources:WebResources, SelectColor%>' runat='server' />" onclick="FnShowPalette(event,'d_mouseOnback')"
																											            height="23" alt="Color" src="../Image/color.jpg" width="20" name="image10"/><%--ZD 100419--%>  <%-- Organization Css Module  --%>
																									            </td> 
																								            </tr>
																							            </table>
																						            </td>
																					            </tr>
																					            <tr>
																						            <td class="tableBody"><asp:Literal Text="<%$ Resources:WebResources, UISettings_MenuBorder%>" runat="server"></asp:Literal></td>
																						            <td align="left" class="tableBody"><asp:Label id="lblm_mborder" runat="server" borderwidth="1px" bordercolor="#000000" borderstyle="Solid"></asp:Label>&nbsp;<asp:Label id="lblm_mborder1" runat="server"></asp:Label></td>
																						            <td>
																							            <table>
																								            <tr>
																									            <td><asp:textbox EnableViewState="True" id="m_menuborder" Runat="server" CssClass="altText" Width="70px"></asp:textbox></td>
																									            <td><img title="<asp:Literal Text='<%$ Resources:WebResources, SelectColor%>' runat='server' />" onclick="FnShowPalette(event,'m_menuborder')"
																											            height="23" alt="Color" src="../Image/color.jpg" width="20" name="image11"/><%--ZD 100419--%>  <%-- Organization Css Module  --%>
																									            </td>
																								            </tr>
																							            </table>
																						            </td>
																						            <td align="left" class="tableBody"><asp:Label ID="lbld_mborder" Runat="server" BorderWidth="1px" BorderColor="#000000" BorderStyle="Solid"></asp:Label>&nbsp;<asp:Label ID="lbld_mborder1" Runat="server"></asp:Label></td>
																						            <td>
																							            <table>
																								            <tr>
																									            <td><asp:textbox EnableViewState="True" id="d_menuborder" Runat="server" CssClass="altText" Width="70px"></asp:textbox></td>
																									            <td><img title="<asp:Literal Text='<%$ Resources:WebResources, SelectColor%>' runat='server' />" onclick="FnShowPalette(event,'d_menuborder')"
																											            height="23" alt="Color" src="../Image/color.jpg" width="20" name="image12"/><%--ZD 100419--%> <%-- Organization Css Module  --%>
																									            </td>
																								            </tr>
																							            </table>
																						            </td>
																					            </tr>
																					            <tr style="visibility:hidden;height:0px"><%--Edited for FF--%>
																						            <td class="tableBody"><asp:Literal Text="<%$ Resources:WebResources, UISettings_MenuHeaderFon%>" runat="server"></asp:Literal></td>
																						            <td align="left" class="tableBody"><asp:Label id="lblm_hfontcol" runat="server" borderwidth="1px" bordercolor="#000000" borderstyle="Solid"></asp:Label>&nbsp;<asp:Label id="lblm_hfontcol1" runat="server"></asp:Label></td>
																						            <td>
																							            <table>
																								            <tr>
																									            <td><asp:textbox EnableViewState="True" id="m_headfontcol" Runat="server" CssClass="altText" Width="70px"></asp:textbox></td>
																									            <td><img title="<asp:Literal Text='<%$ Resources:WebResources, SelectColor%>' runat='server' />" onclick="FnShowPalette(event,'m_headfontcol')"
																											            height="23" alt="Color" src="../Image/color.jpg" width="20" name="image13"/><%--ZD 100419--%>  <%-- Organization Css Module  --%>
																									            </td>
																								            </tr>
																							            </table>
																						            </td>
																						            <td align="left" class="tableBody"><asp:Label ID="lbld_hfontcol" Runat="server" BorderWidth="1px" BorderColor="#000000" BorderStyle="Solid"></asp:Label>&nbsp;<asp:Label ID="lbld_hfontcol1" Runat="server"></asp:Label></td>
																						            <td>
																							            <table>
																								            <tr>
																									            <td><asp:textbox EnableViewState="True" id="d_headfontcol" Runat="server" CssClass="altText" Width="70px"></asp:textbox></td>
																									            <td><img title="<asp:Literal Text='<%$ Resources:WebResources, SelectColor%>' runat='server' />" onclick="FnShowPalette(event,'d_headfontcol')"
																											            height="23" alt="Color" src="../Image/color.jpg" width="20" name="image14"/> <%--ZD 100419--%> <%-- Organization Css Module  --%>
																									            </td>
																								            </tr>
																							            </table>
																						            </td>
																					            </tr>
																					            <tr style="visibility:hidden;height:0px"> <%--Edited for FF--%>
																						            <td class="tableBody"><asp:Literal Text="<%$ Resources:WebResources, UISettings_MenuHeaderBac%>" runat="server"></asp:Literal></td>
																						            <td align="left" class="tableBody"><asp:Label id="lblm_hbackcol" runat="server" borderwidth="1px" bordercolor="#000000" borderstyle="Solid"></asp:Label>&nbsp;<asp:Label id="lblm_hbackcol1" runat="server"></asp:Label></td>
																						            <td>
																							            <table>
																								            <tr>
																									            <td><asp:TextBox EnableViewState="True" id="m_headbackcol" Runat="server" CssClass="altText" Width="70px"></asp:textbox></td>
																									            <td><img title="<asp:Literal Text='<%$ Resources:WebResources, SelectColor%>' runat='server' />" onclick="FnShowPalette(event,'m_headbackcol')"
																											            height="23" alt="Color" src="../Image/color.jpg" width="20" name="image15"/><%--ZD 100419--%>   <%-- Organization Css Module  --%>
																									            </td>
																								            </tr>
																							            </table>
																						            </td>
																						            <td align="left" class="tableBody"><asp:Label ID="lbld_hbackcol" Runat="server" BorderWidth="1px" BorderColor="#000000" BorderStyle="Solid"></asp:Label>&nbsp;<asp:Label ID="lbld_hbackcol1" Runat="server"></asp:Label></td>
																						            <td>
																							            <table>
																								            <tr>
																									            <td><asp:textbox EnableViewState="True" id="d_headbackcol" Runat="server" CssClass="altText" Width="70px"></asp:textbox></td>
																									            <td><img title="<asp:Literal Text='<%$ Resources:WebResources, SelectColor%>' runat='server' />" onclick="FnShowPalette(event,'d_headbackcol')"
																											            height="23" alt="Color" src="../Image/color.jpg" width="20" name="image16"/> <%--ZD 100419--%><%-- Organization Css Module  --%>
																									            </td>
																								            </tr>
																							            </table>
																						            </td>
																					            </tr>
																				            </table>
																			            </div>
																		            </td>
																	            </tr>
																            </table>
															            </td>
														            </tr>
													            </tbody>
												            </table>
											            </td>
										            </tr>	
										            <tr>
                                                        <td align="left" width="20%"><span class="subtitleblueblodtext"><asp:Literal Text="<%$ Resources:WebResources, UISettings_TitleFontProp%>" runat="server"></asp:Literal></span></td>											
											            <td width="1%" height="20">&nbsp;</td>
											            <td align="left" height="20" ></td>
										            </tr>
										            <tr>
											            <td height="20"></td>
											            <td vAlign="top" align="center" colspan="2">
												            <table id="tabletitlefont" cellSpacing="0" cellPadding="2" width="100%" align="left" border="0">
													            <tbody>
														            <tr>
															            <td align="left">
																            <table cellSpacing="0" cellPadding="3" class="tableBody" border="0" width="95%">
																	            <tr>
																		            <td align="center" width="15%" class="tableHeader"><asp:Literal Text="<%$ Resources:WebResources, UISettings_Description%>" runat="server"></asp:Literal></td>
																		            <td align="center" width="22%" class="tableHeader"><asp:Literal Text="<%$ Resources:WebResources, UISettings_Current%>" runat="server"></asp:Literal></td>
																		            <td align="center" width="32%" class="tableHeader"><asp:Literal Text="<%$ Resources:WebResources, UISettings_New%>" runat="server"></asp:Literal></td>
																	            </tr>
																                <tr>
																	                <td class="tableBody"><asp:Literal Text="<%$ Resources:WebResources, UISettings_FontName%>" runat="server"></asp:Literal></td>
																	                <td class="tableBody">
																		                <asp:Label ID="lbltitlename" Runat="server"></asp:Label>
        															                </td>
																	                <td>
																	                    <asp:dropdownlist id="drptitlefont" Runat="server" CssClass="altText" Width="150">
																		                    <asp:ListItem Value="" Text="<%$ Resources:WebResources, SelectFont%>"></asp:ListItem>
																		                    <asp:ListItem Value="Arial">Arial</asp:ListItem>
																		                    <asp:ListItem Value="Bookman Old Style">Bookman Old Style</asp:ListItem>
																		                    <asp:ListItem Value="Century Gothic">Century Gothic</asp:ListItem>
																		                    <asp:ListItem Value="Comic Sans Ms">Comic Sans Ms</asp:ListItem>
																		                    <asp:ListItem Value="Courier New">Courier New</asp:ListItem>
																		                    <asp:ListItem Value="Georgia">Georgia</asp:ListItem>
                    													                    <asp:ListItem Value="Microsoft Sans Serif">Microsoft Sans Serif</asp:ListItem>
																				            <asp:ListItem Value="Tahomo">Tahomo</asp:ListItem>
																			                <asp:ListItem Value="Times New Roman">Times New Roman</asp:ListItem>
																			                <asp:ListItem Value="Verdana">Verdana</asp:ListItem>
																		                </asp:dropdownlist>
																		            </td>
																	            </tr>
																	            <tr>
																		            <td class="tableBody"><asp:Literal Text="<%$ Resources:WebResources, UISettings_FontSize%>" runat="server"></asp:Literal></td>
																		            <td class="tableBody">
																			            <asp:Label ID="lbltitlesize" Runat="server"></asp:Label>
																		            </td>
																		            <td>
																			            <asp:dropdownlist id="drptitlesize" Runat="server" CssClass="altText" Width="55">
																				            <asp:ListItem Value="" Text="<%$ Resources:WebResources, Size%>"></asp:ListItem>
																				            <asp:ListItem Value="8">8</asp:ListItem>
																				            <asp:ListItem Value="9">9</asp:ListItem>
																				            <asp:ListItem Value="10">10</asp:ListItem>
																				            <asp:ListItem Value="11">11</asp:ListItem>
																				            <asp:ListItem Value="12">12</asp:ListItem>
																				            <asp:ListItem Value="14">14</asp:ListItem>
																				            <asp:ListItem Value="16">16</asp:ListItem>
																				            <asp:ListItem Value="18">18</asp:ListItem>
																				            <asp:ListItem Value="20">20</asp:ListItem>
																				            <asp:ListItem Value="22">22</asp:ListItem>
																			            </asp:dropdownlist>
																		            </td>
																	            </tr>
																	            <tr>
																	                <td class="tableBody"><asp:Literal Text="<%$ Resources:WebResources, UISettings_FontColor%>" runat="server"></asp:Literal></td>
																	                <td class="tableBody">
																		                <asp:Label ID="lbltitlecolor" Runat="server" BorderWidth="1px" BorderColor="#000000" BorderStyle="Solid"></asp:Label>
																		                <asp:Label ID="lbltitlecolor1" Runat="server"></asp:Label>
																	                </td>
																	                <td>
																		                <asp:textbox EnableViewState="True" id="txttitlecolor" Runat="server" CssClass="altText"></asp:textbox>
																		                <img title="<asp:Literal Text='<%$ Resources:WebResources, SelectColor%>' runat='server' />" onclick="FnShowPalette(event,'txttitlecolor')"
																			                height="23" alt="Color" src="../Image/color.jpg" width="27" name="imgtitlefontcolor" /> <%-- Organization Css Module  --%><%--ZD 100419--%>
																	                </td>
																	            </tr>
																            </table>
															            </td>
														            </tr>
													            </table>
											            </td>
										            </tr>
										            <tr>
										                <td align="left" width="20%"><span class="subtitleblueblodtext"><asp:Literal Text="<%$ Resources:WebResources, UISettings_ButtonFontPro%>" runat="server"></asp:Literal></span></td>											
											            <td width="1%" height="20">&nbsp;</td>
											            <td align="left" height="20" ></td>
										            </tr>
										            <tr>
											            <td height="20"></td>
											            <td vAlign="top" align="center" colspan="2">
												            <table id="tablebutton" cellSpacing="0" cellPadding="2" width="100%" align="left" border="0">
													            <tbody>
														            <tr>
															            <td align="left">
																            <table cellSpacing="0" cellPadding="3" class="tableBody" border="0" width="95%">
																	            <tr>
																		            <td align="center" width="19%" class="tableHeader"><asp:Literal Text="<%$ Resources:WebResources, UISettings_Description%>" runat="server"></asp:Literal></td>
																		            <td align="center" width="22%" class="tableHeader"><asp:Literal Text="<%$ Resources:WebResources, UISettings_Current%>" runat="server"></asp:Literal></td>
																		            <td align="center" width="32%" class="tableHeader"><asp:Literal Text="<%$ Resources:WebResources, UISettings_New%>" runat="server"></asp:Literal></td>
																	            </tr>
																	            <tr>
																		            <td class="tableBody"><asp:Literal Text="<%$ Resources:WebResources, UISettings_FontName%>" runat="server"></asp:Literal></td>
																		            <td class="tableBody"><asp:Label id="lblbttnfontname" runat="server"></asp:Label></td>
																		            <td>
																			            <asp:dropdownlist id="dropbttnfont" Runat="server" CssClass="altText" Width="150">
																				            <asp:ListItem Value="" Text="<%$ Resources:WebResources, SelectFont%>"></asp:ListItem>
																				            <asp:ListItem Value="Arial">Arial</asp:ListItem>
																				            <asp:ListItem Value="Bookman Old Style">Bookman Old Style</asp:ListItem>
																				            <asp:ListItem Value="Century Gothic">Century Gothic</asp:ListItem>
																				            <asp:ListItem Value="Comic Sans Ms">Comic Sans Ms</asp:ListItem>
																				            <asp:ListItem Value="Courier New">Courier New</asp:ListItem>
																				            <asp:ListItem Value="Georgia">Georgia</asp:ListItem>
																				            <asp:ListItem Value="Microsoft Sans Serif">Microsoft Sans Serif</asp:ListItem>
																				            <asp:ListItem Value="Tahomo">Tahomo</asp:ListItem>
																				            <asp:ListItem Value="Times New Roman">Times New Roman</asp:ListItem>
																				            <asp:ListItem Value="Verdana">Verdana</asp:ListItem>
																			            </asp:dropdownlist>
																		            </td>
																	            </tr>
																	            <tr>
																		            <td class="tableBody"><asp:Literal Text="<%$ Resources:WebResources, UISettings_FontSize%>" runat="server"></asp:Literal></td>
																		            <td class="tableBody">
																			            <asp:Label ID="lblbuttonsize" Runat="server"></asp:Label>
																		            </td>
																		            <td>
																			            <asp:dropdownlist id="drpbuttonsize" Runat="server" CssClass="altText" Width="55">
																				            <asp:ListItem Value="" Text="<%$ Resources:WebResources, Size%>"></asp:ListItem>
																				            <asp:ListItem Value="8">8</asp:ListItem>
																				            <asp:ListItem Value="9">9</asp:ListItem>
																				            <asp:ListItem Value="10">10</asp:ListItem>
																				            <asp:ListItem Value="11">11</asp:ListItem>
																				            <asp:ListItem Value="12">12</asp:ListItem>
																				            <asp:ListItem Value="14">14</asp:ListItem>
																				            <asp:ListItem Value="16">16</asp:ListItem>
																				            <asp:ListItem Value="18">18</asp:ListItem>
																				            <asp:ListItem Value="20">20</asp:ListItem>
																				            <asp:ListItem Value="22">22</asp:ListItem>
																			            </asp:dropdownlist>
																		            </td>
																	            </tr>
																	            <tr>
																		            <td class="tableBody"><asp:Literal Text="<%$ Resources:WebResources, UISettings_FontColor%>" runat="server"></asp:Literal></td>
																		            <td class="tableBody">
																			            <asp:Label ID="lblbttnfontcolor" Runat="server" BorderWidth="1px" BorderColor="#000000" BorderStyle="Solid"></asp:Label>
																			            <asp:Label ID="lblbttnfontcolor1" Runat="server"></asp:Label>
																    	            </td>
																		            <td>
																			            <asp:textbox EnableViewState="True" id="txtbuttonfontcolor" Runat="server" CssClass="altText"></asp:textbox>
																			            <img title="<asp:Literal Text='<%$ Resources:WebResources, SelectColor%>' runat='server' />" onclick="FnShowPalette(event,'txtbuttonfontcolor')"
																				            height="23" alt="Color" src="../Image/color.jpg" width="27" name="iimgbuttonfontcolor"/><%--ZD 100419--%>  <%-- Organization Css Module  --%>
																		            </td>
																	            </tr>
																                <tr>
																	                <td nowrap="" class="tableBody"><asp:Literal Text="<%$ Resources:WebResources, UISettings_FontBackground%>" runat="server"></asp:Literal></td>
																	                <td nowrap="" class="tableBody">
																		                <asp:Label id="lblbuttonbgcolor" runat="server" borderwidth="1px" bordercolor="#000000" borderstyle="Solid"></asp:Label>
																		                <asp:Label id="lblbuttonbgcolor1" runat="server"></asp:Label>
																	                </td>
																		            <td>
																			            <asp:textbox EnableViewState="True" id="txtbuttonbgcolor" Runat="server" CssClass="altText"></asp:textbox>
																			            <img title="<asp:Literal Text='<%$ Resources:WebResources, SelectColor%>' runat='server' />" onclick="FnShowPalette(event,'txtbuttonbgcolor')"
																				            height="23" alt="Color" src="../Image/color.jpg" width="27" name="imgbttnbgcolor" /><%--ZD 100419--%><%-- Organization Css Module  --%>
																		            </td>
																	            </tr>
																            </table>
															            </td>
														            </tr>
    												            </tbody>
												            </table>
											            </td>
										            </tr>
            										
										            <TR>
											            <td align="left" width="20%"><span class="subtitleblueblodtext"><asp:Literal Text="<%$ Resources:WebResources, UISettings_BackgroundColo%>" runat="server"></asp:Literal></span></td>											
											            <td width="1%" height="20">&nbsp;</td>
											            <td align="left" height="20" ></td>
										            </TR>
										            <tr>
											            <td height="20"></td>
											            <td vAlign="top" align="center" colspan="2">
												            <table cellSpacing="0" cellPadding="2" width="100%" align="left" border="0">
													            <tbody>
														            <tr>
															            <td align="left">
																            <table cellSpacing="0" cellPadding="3" class="tableBody" border="0" width="95%">
																	            <tr class="tableHeader">
																		            <td align="center" width="20%" class="tableHeader"><asp:Literal Text="<%$ Resources:WebResources, UISettings_Description%>" runat="server"></asp:Literal></td>
																		            <td align="center" width="20%" class="tableHeader"><asp:Literal Text="<%$ Resources:WebResources, UISettings_Current%>" runat="server"></asp:Literal></td>
																		            <td align="center" width="60%" class="tableHeader"><asp:Literal Text="<%$ Resources:WebResources, UISettings_New%>" runat="server"></asp:Literal></td>
																	            </tr>
																	            <tr>
															                        <td class="tableBody"><asp:Literal Text="<%$ Resources:WebResources, UISettings_BackgroundColo%>" runat="server"></asp:Literal></td>
															                        <td>
																                        <table>
																	                        <tr>
																		                        <td class="tableBody">
																			                        <asp:Label ID="lblbodybgcolor1" runat="server" BorderWidth="1px" BorderColor="#000000" BorderStyle="Solid" ></asp:Label>
																			                        <asp:Label ID="lblbodybgcolor" Runat="server" ></asp:Label>
																		                        </td>
																	                        </tr>
																                        </table>
															                        </td>
															                        <td>
																                        <table cellspacing="0" cellpadding="2" width="100%" align="left" border="0">
																	                        <tbody>
																	                            <%--FB 1473--%>
																		                        <tr style="display:none;">
																			                        <td valign="top" align="left" class="tableBody"><asp:radiobutton id="NamedColor" runat="server" groupname="ColorSystem" checked="True"></asp:radiobutton><asp:Literal Text="<%$ Resources:WebResources, UISettings_ColorName%>" runat="server"></asp:Literal></td>
																			                        <td align="left" class="tableBody">
																			                        <asp:dropdownlist id="NamedCol" runat="server" cssclass="altText">
																					                        <asp:ListItem value="" Text="<%$ Resources:WebResources, SelectColor2%>"></asp:ListItem>
																					                        <asp:ListItem value="#008000">Green</asp:ListItem>
																					                        <asp:ListItem value="#00ff00">Lime</asp:ListItem>
																					                        <asp:ListItem value="#008080">Teal</asp:ListItem>
																					                        <asp:ListItem value="#00ffff">Aqua</asp:ListItem>
																					                        <asp:ListItem value="#000080">Navy</asp:ListItem>
																					                        <asp:ListItem value="#0000ff">Blue</asp:ListItem>
																					                        <asp:ListItem value="#800080">Purple</asp:ListItem>
																					                        <asp:ListItem value="#ff00ff">Fuchsia</asp:ListItem>
																					                        <asp:ListItem value="#800000">Maroon</asp:ListItem>
																					                        <asp:ListItem value="#ff0000">Red</asp:ListItem>
																					                        <asp:ListItem value="#808000">Olive</asp:ListItem>
																					                        <asp:ListItem value="Fuchsia">Fuchsia</asp:ListItem>
																					                        <asp:ListItem value="Yellow">Yellow</asp:ListItem>
																					                        <asp:ListItem value="White">White</asp:ListItem>
																					                        <asp:ListItem value="Silver">Silver</asp:ListItem>
																					                        <asp:ListItem value="Black">Black</asp:ListItem>
																					                        <asp:ListItem value="Gray">Gray</asp:ListItem>
																				                        </asp:dropdownlist>
																			                        </td>
																		                        </tr>
																		                        <tr>
																		                            <%-- FB 1473 --%>
																			                        <td valign="middle" align="left" class="tableBody"><asp:radiobutton id="RGBColor" runat="server" groupname="ColorSystem" style="display:none;"></asp:radiobutton><asp:Literal Text="<%$ Resources:WebResources, UISettings_RGBPalette%>" runat="server"></asp:Literal></td>
																			                        <td align="left">
																				                        <table>
																					                        <tr>
																						                        <td><asp:textbox EnableViewState="True" id="TxtRGBColor" Runat="server" CssClass="altText" ></asp:textbox></td>
																						                        <td>
																							                        <img title="<asp:Literal Text='<%$ Resources:WebResources, SelectColor%>' runat='server' />" height="23" alt="Color" onclick="FnShowPalette(event,'TxtRGBColor')" src="../Image/color.jpg"
																								                        width="27" name="rgbimg"/> <%--ZD 100419--%> <%-- Organization Css Module  --%>
																						                        </td>
																					                        </tr>
																				                        </table>
																			                        </td>
																		                        </tr>
																	                        </tbody>
																                        </table>
															                        </td>
														                        </tr>
																            </table>
															            </td>
														            </tr>
													            </tbody>
												            </table>
											            </td>
										            </tr>
            										
										            <TR>
											            <td align="left" width="20%"><span class="subtitleblueblodtext"><asp:Literal Text="<%$ Resources:WebResources, UISettings_GeneralFontPr%>" runat="server"></asp:Literal></span></td>											
											            <td width="1%" height="20">&nbsp;</td>
											            <td align="left" height="20" ></td>
										            </TR>
            										
										            <tr>
											            <td height="20"></td>
											            <td vAlign="top" align="center" colspan="2">
												            <table id="tablegeneral" cellSpacing="0" cellPadding="2" width="100%" align="left" border="0">
													            <tbody>
														            <tr>
															            <td align="left">
																            <table cellSpacing="0" cellPadding="3" class="tableBody" border="0" width="95%">
																	            <tr>
																		            <td align="center" width="15%" class="tableHeader"><asp:Literal Text="<%$ Resources:WebResources, UISettings_Description%>" runat="server"></asp:Literal></td>
																		            <td align="center" width="22%" class="tableHeader"><asp:Literal Text="<%$ Resources:WebResources, UISettings_Current%>" runat="server"></asp:Literal></td>
																		            <td align="center" width="32%" class="tableHeader"><asp:Literal Text="<%$ Resources:WebResources, UISettings_New%>" runat="server"></asp:Literal></td>
																	            </tr>
																	            <tr>
																		            <td class="tableBody"><asp:Literal Text="<%$ Resources:WebResources, UISettings_FontName%>" runat="server"></asp:Literal></td>
																		            <td class="tableBody">
																			            <asp:Label ID="lblgenname" Runat="server" ></asp:Label>
																		            </td>
																	                <td>
																		                <asp:dropdownlist id="drpgenfont" Runat="server" CssClass="altText" Width="150">
																			                <asp:ListItem Value="" Text="<%$ Resources:WebResources, SelectFont%>"></asp:ListItem>
																			                <asp:ListItem Value="Arial">Arial</asp:ListItem>
																			                <asp:ListItem Value="Bookman Old Style">Bookman Old Style</asp:ListItem>
																			                <asp:ListItem Value="Century Gothic">Century Gothic</asp:ListItem>
																			                <asp:ListItem Value="Comic Sans Ms">Comic Sans Ms</asp:ListItem>
																			                <asp:ListItem Value="Courier New">Courier New</asp:ListItem>
																			                <asp:ListItem Value="Georgia">Georgia</asp:ListItem>
																			                <asp:ListItem Value="Microsoft Sans Serif">Microsoft Sans Serif</asp:ListItem>
																			                <asp:ListItem Value="Tahomo">Tahomo</asp:ListItem>
																			                <asp:ListItem Value="Times New Roman">Times New Roman</asp:ListItem>
																			                <asp:ListItem Value="Verdana">Verdana</asp:ListItem>
																		                </asp:dropdownlist>
																	                </td>
																	            </tr>
																	            <tr>
																	                <td class="tableBody"><asp:Literal Text="<%$ Resources:WebResources, UISettings_FontSize%>" runat="server"></asp:Literal></td>
																	                <td class="tableBody">
																		                <asp:Label ID="lblgensize" Runat="server"></asp:Label>
																	                </td>
																	                <td>
                											                            <asp:dropdownlist id="drpgensize" Runat="server" CssClass="altText" Width="55">
															                                <asp:ListItem Value="" Text="<%$ Resources:WebResources, Size%>"></asp:ListItem>
															                                <asp:ListItem Value="8">8</asp:ListItem>
															                                <asp:ListItem Value="9">9</asp:ListItem>
															                                <asp:ListItem Value="10">10</asp:ListItem>
															                                <asp:ListItem Value="11">11</asp:ListItem>
															                                <asp:ListItem Value="12">12</asp:ListItem>
															                                <asp:ListItem Value="14">14</asp:ListItem>
															                                <asp:ListItem Value="16">16</asp:ListItem>
															                                <asp:ListItem Value="18">18</asp:ListItem>
															                                <asp:ListItem Value="20">20</asp:ListItem>
															                                <asp:ListItem Value="22">22</asp:ListItem>
														                                </asp:dropdownlist>
															                        </td>
																	            </tr>
																	            <tr>
																		            <td class="tableBody"><asp:Literal Text="<%$ Resources:WebResources, UISettings_FontColor%>" runat="server"></asp:Literal></td>
																		            <td class="tableBody">
																		                <asp:Label ID="lblgencolor" Runat="server" BorderWidth="1px" BorderColor="#000000" BorderStyle="Solid"></asp:Label>
																		                <asp:Label ID="lblgencolor1" Runat="server"></asp:Label>
																	                </td>
																		            <td>
																			            <asp:textbox EnableViewState="True" id="txtgencolor" Runat="server" CssClass="altText"></asp:textbox>
																			            <img title="<asp:Literal Text='<%$ Resources:WebResources, SelectColor%>' runat='server' />" onclick="FnShowPalette(event,'txtgencolor')" height="23"
																				            alt="Color" src="../Image/color.jpg" width="27" name="imggen"/>  <%-- Organization Css Module  --%> <%--ZD 100419--%>
																		            </td>
																	            </tr>
																            </table>
															            </td>
														            </tr>
													            </tbody>
												            </table>
											            </td>
										            </tr>

										            <tr>
											            <td align="left" width="20%"><span class="subtitleblueblodtext"><asp:Literal Text="<%$ Resources:WebResources, UISettings_SubtitleFontP%>" runat="server"></asp:Literal></span></td>											
											            <td width="1%" height="20">&nbsp;</td>
											            <td align="left" height="20" ></td>
										            </tr>
            										
										            <tr>
											            <td height="20"></td>
											            <td vAlign="top" align="center" colspan="2">
												            <table id="Table11" cellSpacing="0" cellPadding="2" width="100%" align="left" border="0">
													            <tbody>
														            <tr>
															            <td align="left">
																            <table cellSpacing="0" cellPadding="3" class="tableBody" border="0" width="95%">
																	            <tr>
																		            <td align="center" width="15%" class="tableHeader"><asp:Literal Text="<%$ Resources:WebResources, UISettings_Description%>" runat="server"></asp:Literal></td>
																		            <td align="center" width="22%" class="tableHeader"><asp:Literal Text="<%$ Resources:WebResources, UISettings_Current%>" runat="server"></asp:Literal></td>
																		            <td align="center" width="32%" class="tableHeader"><asp:Literal Text="<%$ Resources:WebResources, UISettings_New%>" runat="server"></asp:Literal></td>
																	            </tr>
																	            <tr>
																		            <td class="tableBody"><asp:Literal Text="<%$ Resources:WebResources, UISettings_FontName%>" runat="server"></asp:Literal></td>
																		            <td class="tableBody">
																			            <asp:Label ID="lblsubtitlename" Runat="server"></asp:Label></td>
																		            <td>
																			            <asp:dropdownlist id="drpsubtitlefont" Runat="server" CssClass="altText" Width="150">
																				            <asp:ListItem Value="" Text="<%$ Resources:WebResources, SelectFont%>"></asp:ListItem>
																				            <asp:ListItem Value="Arial">Arial</asp:ListItem>
																				            <asp:ListItem Value="Bookman Old Style">Bookman Old Style</asp:ListItem>
																				            <asp:ListItem Value="Century Gothic">Century Gothic</asp:ListItem>
																				            <asp:ListItem Value="Comic Sans Ms">Comic Sans Ms</asp:ListItem>
																				            <asp:ListItem Value="Courier New">Courier New</asp:ListItem>
																				            <asp:ListItem Value="Georgia">Georgia</asp:ListItem>
																				            <asp:ListItem Value="Microsoft Sans Serif">Microsoft Sans Serif</asp:ListItem>
																				            <asp:ListItem Value="Tahomo">Tahomo</asp:ListItem>
																				            <asp:ListItem Value="Times New Roman">Times New Roman</asp:ListItem>
																				            <asp:ListItem Value="Verdana">Verdana</asp:ListItem>
											    	    					            </asp:dropdownlist>
																		            </td>
																	            </tr>
																	            <tr>
																		            <td class="tableBody"><asp:Literal Text="<%$ Resources:WebResources, UISettings_FontSize%>" runat="server"></asp:Literal></td>
																		            <td class="tableBody">
																			            <asp:Label ID="lblsubtitlesize" Runat="server"></asp:Label>
																		            </td>
																		            <td>
																			            <asp:dropdownlist id="drpsubtitlesize" Runat="server" CssClass="altText" Width="55">
																				            <asp:ListItem Value="" Text="<%$ Resources:WebResources, Size%>"></asp:ListItem>
																				            <asp:ListItem Value="8">8</asp:ListItem>
																				            <asp:ListItem Value="9">9</asp:ListItem>
																				            <asp:ListItem Value="10">10</asp:ListItem>
																				            <asp:ListItem Value="11">11</asp:ListItem>
																				            <asp:ListItem Value="12">12</asp:ListItem>
																				            <asp:ListItem Value="14">14</asp:ListItem>
																				            <asp:ListItem Value="16">16</asp:ListItem>
																				            <asp:ListItem Value="18">18</asp:ListItem>
																				            <asp:ListItem Value="20">20</asp:ListItem>
																				            <asp:ListItem Value="22">22</asp:ListItem>
																			            </asp:dropdownlist>
																		            </td>
																	            </tr>
																	            <tr>
																		            <td class="tableBody"><asp:Literal Text="<%$ Resources:WebResources, UISettings_FontColor%>" runat="server"></asp:Literal></td>
																		            <td class="tableBody">
																			            <asp:Label ID="lblsubtitlecolor" Runat="server" BorderWidth="1px" BorderColor="#000000" BorderStyle="Solid"></asp:Label>
    																		            <asp:Label ID="lblsubtitlecolor1" Runat="server"></asp:Label>
																		            </td>
																		            <td>
																			            <asp:textbox EnableViewState="True" id="txtsubtitlecolor" Runat="server" CssClass="altText"></asp:textbox>
																			            <img title="<asp:Literal Text='<%$ Resources:WebResources, SelectColor%>' runat='server' />" onclick="FnShowPalette(event,'txtsubtitlecolor')"
																				            height="23" alt="Color" src="../Image/color.jpg" width="27" name="imgsubtitlecolor"/> <%-- Organization Css Module  --%><%--ZD 100419--%>
																		            </td>
																	            </tr>
																            </table>
															            </td>
														            </tr>
													            </tbody>
												            </table>
											            </td>
										            </tr>

                                                    <tr>
											            <td align="left" width="20%"><span class="subtitleblueblodtext"><asp:Literal Text="<%$ Resources:WebResources, UISettings_TableProperty%>" runat="server"></asp:Literal></span></td>											
											            <td width="1%" height="20">&nbsp;</td>
											            <td align="left" height="20" ></td>
										            </tr>
            										
										            <tr>
											            <td height="20"></td>
											            <td vAlign="top" align="center" colspan="2">
												            <table id="tbltableheader" cellSpacing="0" cellPadding="2" width="100%" align="left" border="0">
													            <tr>
														            <td align="left">
															            <table cellSpacing="0" cellPadding="3" class="tableBody" border="0" width="95%">
																            <tr class="tableHeader">
																	            <td align="center" width="19%" class="tableHeader"><asp:Literal Text="<%$ Resources:WebResources, UISettings_Description%>" runat="server"></asp:Literal></td>
																	            <td align="center" width="22%" class="tableHeader"><asp:Literal Text="<%$ Resources:WebResources, UISettings_Current%>" runat="server"></asp:Literal></td>
																	            <td align="center" width="32%" class="tableHeader"><asp:Literal Text="<%$ Resources:WebResources, UISettings_New%>" runat="server"></asp:Literal></td>
																            </tr>
																            <tr>
																	            <td class="tableBody"><asp:Literal Text="<%$ Resources:WebResources, UISettings_HeaderFontCol%>" runat="server"></asp:Literal></td>
									        						            <td class="tableBody">
																		            <asp:Label ID="lbltableheaderforecolor" Runat="server" BorderWidth="1px" BorderColor="#000000" BorderStyle="Solid"></asp:Label>
																		            <asp:Label ID="lbltableheaderforecolor1" Runat="server"></asp:Label>
																	            </td>
																	            <td nowrap>
																		            <asp:TextBox EnableViewState="True" id="txttableheaderfontcolor" Runat="server" CssClass="altText"></asp:textbox>
																		            <img title="<asp:Literal Text='<%$ Resources:WebResources, SelectColor%>' runat='server' />" onclick="FnShowPalette(event,'txttableheaderfontcolor')"
																			            height="23" alt="Color" src="../Image/color.jpg" width="27" name="iimgtablefontcolor" /> <%-- Organization Css Module  --%> <%--ZD 100419--%>
																	            </td>
																            </tr>
																            <tr>
																	            <td nowrap class="tableBody"><asp:Literal Text="<%$ Resources:WebResources, UISettings_HeaderBackgrou%>" runat="server"></asp:Literal></td>
																	            <td class="tableBody">
																		            <asp:Label ID="lbltableheaderbackcolor" runat="server" BorderWidth="1px" BorderColor="#000000" BorderStyle="Solid"></asp:Label>
																		            <asp:Label ID="lbltableheaderbackcolor1" Runat="server"></asp:Label>
																	            </td>
																	            <td>
																		            <asp:textbox EnableViewState="True" id="txttableheaderbackcolor" Runat="server" CssClass="altText"></asp:textbox>
																		            <img title="<asp:Literal Text='<%$ Resources:WebResources, SelectColor%>' runat='server' />" onclick="FnShowPalette(event,'txttableheaderbackcolor')"
																			            height="23" alt="Color" src="../Image/color.jpg" width="27" name="imgtblheaderbgcolor" /> <%-- Organization Css Module  --%><%--ZD 100419--%>
																	            </td>
																            </tr>
																            <tr>
																	            <td class="tableBody"><asp:Literal Text="<%$ Resources:WebResources, UISettings_BodyFontColor%>" runat="server"></asp:Literal></td>
									        						            <td class="tableBody">
																		            <asp:Label ID="lbltablebodyforecolor" runat="server" BorderWidth="1px" BorderColor="#000000" BorderStyle="Solid"></asp:Label>
																		            <asp:Label ID="lbltablebodyforecolor1" Runat="server"></asp:Label>
																	            </td>
																	            <td nowrap>
																		            <asp:textbox EnableViewState="True" id="txttablebodyfontcolor" Runat="server" CssClass="altText"></asp:textbox>
																		            <img title="<asp:Literal Text='<%$ Resources:WebResources, SelectColor%>' runat='server' />" onclick="FnShowPalette(event,'txttablebodyfontcolor')"
																			            height="23" alt="Color" src="../Image/color.jpg" width="27" name="iimgtablefontcolor1"/><%--ZD 100419--%> <%-- Organization Css Module  --%>
																	            </td>
																            </tr>
																            <tr>
																	            <td nowrap class="tableBody"><asp:Literal Text="<%$ Resources:WebResources, UISettings_BodyBackground%>" runat="server"></asp:Literal></td>
																	            <td class="tableBody">
																		            <asp:Label ID="lbltablebodybackcolor" Runat="server" BorderWidth="1px" BorderColor="#000000" BorderStyle="Solid"></asp:Label>
																		            <asp:Label ID="lbltablebodybackcolor1" Runat="server"></asp:Label>
																	            </td>
																	            <td>
																		            <asp:textbox EnableViewState="True" id="txttablebodybackcolor" Runat="server" CssClass="altText"></asp:textbox>
																		            <img title="<asp:Literal Text='<%$ Resources:WebResources, SelectColor%>' runat='server' />" onclick="FnShowPalette(event,'txttablebodybackcolor')"
																			            height="23" alt="Color" src="../Image/color.jpg" width="27" name="imgtblheaderbgcolor1" /><%--ZD 100419--%><%-- Organization Css Module  --%>
																	            </td>
																            </tr>
    														            </table>
															            </td>
														            </tr>
												            </table>
											            </td>
										            </tr>
            										
										            <tr>
											            <td align="left" width="20%" colspan="3">
											            <span class="subtitleblueblodtext"><asp:Literal Text="<%$ Resources:WebResources, UISettings_ErrorMessageD%>" runat="server"></asp:Literal></span></td>											
										            </tr>
            										
										            <tr>
											            <td height="20"></td>
											            <td vAlign="top" align="center" colspan="2">
												            <table id="Table3" cellSpacing="0" cellPadding="2" width="100%" align="left" border="0">
													            <tr>
														            <td align="left">
															            <table cellSpacing="0" cellPadding="3" class="tableBody" border="0" width="95%">
																            <tr class="tableHeader">
																	            <td align="center" width="19%" class="tableHeader"><asp:Literal Text="<%$ Resources:WebResources, UISettings_Description%>" runat="server"></asp:Literal></td>
																	            <td align="center" width="22%" class="tableHeader"><asp:Literal Text="<%$ Resources:WebResources, UISettings_Current%>" runat="server"></asp:Literal></td>
																	            <td align="center" width="32%" class="tableHeader"><asp:Literal Text="<%$ Resources:WebResources, UISettings_New%>" runat="server"></asp:Literal></td>
																            </tr>
																            <tr>
																	            <td class="tableBody"><asp:Literal Text="<%$ Resources:WebResources, UISettings_FontColor%>" runat="server"></asp:Literal></td>
									        						            <td class="tableBody">
																		            <asp:Label ID="lblerrormessageforecolor" Runat="server" BorderWidth="1px" BorderColor="#000000" BorderStyle="Solid"></asp:Label>
																		            <asp:Label ID="lblerrormessageforecolor1" Runat="server"></asp:Label>
																	            </td>
																	            <td nowrap>
																		            <asp:textbox EnableViewState="True" id="txterrormessagefontcolor" Runat="server" CssClass="altText"></asp:textbox>
																		            <img title="<asp:Literal Text='<%$ Resources:WebResources, SelectColor%>' runat='server' />" onclick="FnShowPalette(event,'txterrormessagefontcolor')"
																			            height="23" alt="Color" src="../Image/color.jpg" width="27" name="iimgerrormessagefontcolor"/><%-- Organization Css Module  --%><%--ZD 100419--%>
																	            </td>
																            </tr>
																            <tr visible="false" runat="server" id="ErRow">
																	            <td nowrap class="tableBody"><asp:Literal Text="<%$ Resources:WebResources, UISettings_BackgroundColo%>" runat="server"></asp:Literal></td>
																	            <td class="tableBody">
																		            <asp:Label ID="lblerrormessagebackcolor" Runat="server" BorderWidth="1px" BorderColor="#000000" BorderStyle="Solid"></asp:Label>
																		            <asp:Label ID="lblerrormessagebackcolor1" Runat="server"></asp:Label>
																	            </td>
																	            <td>
																		            <asp:textbox EnableViewState="True" id="txterrormessagebackcolor" Runat="server" CssClass="altText"></asp:textbox>
																		            <img title="<asp:Literal Text='<%$ Resources:WebResources, SelectColor%>' runat='server' />" onclick="FnShowPalette(event,'txterrormessagebackcolor')"
																			            height="23" alt="Color" src="../Image/color.jpg" width="27" name="imgerrormessagebgcolor"/><%--ZD 100419--%> <%-- Organization Css Module  --%>
																	            </td>
																            </tr>
																            <tr>
																	            <td class="tableBody"><asp:Literal Text="<%$ Resources:WebResources, UISettings_FontSize%>" runat="server"></asp:Literal></td>
																		            <td class="tableBody">
																			            <asp:Label ID="lblerrormessagefontsize" Runat="server"></asp:Label>
																		            </td>
																		            <td>
																			            <asp:dropdownlist id="drperrormessagefontsize" Runat="server" CssClass="altText" Width="55">
																				            <asp:ListItem Value="" Text="<%$ Resources:WebResources, Size%>"></asp:ListItem>
																				            <asp:ListItem Value="8">8</asp:ListItem>
																				            <asp:ListItem Value="9">9</asp:ListItem>
																				            <asp:ListItem Value="10">10</asp:ListItem>
																				            <asp:ListItem Value="11">11</asp:ListItem>
																				            <asp:ListItem Value="12">12</asp:ListItem>
																				            <asp:ListItem Value="14">14</asp:ListItem>
																				            <asp:ListItem Value="16">16</asp:ListItem>
																				            <asp:ListItem Value="18">18</asp:ListItem>
																				            <asp:ListItem Value="20">20</asp:ListItem>
																				            <asp:ListItem Value="22">22</asp:ListItem>
																			            </asp:dropdownlist>
																		            </td>
																            </tr>  													</table>
															            </td>
														            </tr>
												            </table>
											            </td>
										            </tr>
            										
                                                    <tr>
                                                        <td align="left" width="20%" colspan="3"><span class="subtitleblueblodtext"><asp:Literal Text="<%$ Resources:WebResources, UISettings_InputTextProp%>" runat="server"></asp:Literal></span></td><%--FB 2579 End--%>										
										            </tr>
            										
										            <tr>
											            <td height="20"></td>
											            <td vAlign="top" align="center" colspan="2">
												            <table id="tblInputTextBox" cellSpacing="0" cellPadding="2" width="100%" align="left" border="0">
													            <tr>
														            <td align="left">
															            <table cellSpacing="0" cellPadding="3" class="tableBody" border="0" width="95%">
																            <tr class="tableHeader">
																	            <td align="center" width="19%" class="tableHeader"><asp:Literal Text="<%$ Resources:WebResources, UISettings_Description%>" runat="server"></asp:Literal></td>
																	            <td align="center" width="22%" class="tableHeader"><asp:Literal Text="<%$ Resources:WebResources, UISettings_Current%>" runat="server"></asp:Literal></td>
																	            <td align="center" width="32%" class="tableHeader"><asp:Literal Text="<%$ Resources:WebResources, UISettings_New%>" runat="server"></asp:Literal></td>
																            </tr>
																            <tr>
																	            <td class="tableBody"><asp:Literal Text="<%$ Resources:WebResources, UISettings_FontColor%>" runat="server"></asp:Literal></td>
									        						            <td class="tableBody">
																		            <asp:Label ID="lblinputtextforecolor" Runat="server" BorderWidth="1px" BorderColor="#000000" BorderStyle="Solid"></asp:Label>
																		            <asp:Label ID="lblinputtextforecolor1" Runat="server"></asp:Label>
																	            </td>
																	            <td nowrap>
																		            <asp:textbox EnableViewState="True" id="txtinputtextfontcolor" Runat="server" CssClass="altText"></asp:textbox>
																		            <img title="<asp:Literal Text='<%$ Resources:WebResources, SelectColor%>' runat='server' />" onclick="FnShowPalette(event,'txtinputtextfontcolor')"
																			            height="23" alt="Color" src="../Image/color.jpg" width="27" name="imginputtextfontcolor"/> <%--ZD 100419--%><%-- Organization Css Module  --%> <%--ZD 100419--%>
																	            </td>
																            </tr>
																            <tr>
																	            <td nowrap class="tableBody"><asp:Literal Text="<%$ Resources:WebResources, UISettings_BackgroundColo%>" runat="server"></asp:Literal></td>
																	            <td class="tableBody">
																		            <asp:Label ID="lblinputtextbackcolor" Runat="server" BorderWidth="1px" BorderColor="#000000" BorderStyle="Solid"></asp:Label>
																		            <asp:Label ID="lblinputtextbackcolor1" Runat="server"></asp:Label>
																	            </td>
																	            <td>
																		            <asp:textbox EnableViewState="True" id="txtinputtextbackcolor" Runat="server" CssClass="altText"></asp:textbox>
																		            <img title="<asp:Literal Text='<%$ Resources:WebResources, SelectColor%>' runat='server' />" onclick="FnShowPalette(event,'txtinputtextbackcolor')"
																			            height="23" alt="Color" src="../Image/color.jpg" width="27" name="imginputtextbgcolor"/> <%--ZD 100419--%><%-- Organization Css Module  --%>
																	            </td>
																            </tr>
																            <tr>
																	            <td nowrap class="tableBody"><asp:Literal Text="<%$ Resources:WebResources, UISettings_BorderColor%>" runat="server"></asp:Literal></td>
																	            <td class="tableBody">
																		            <asp:Label ID="lblinputtextbordercolor" Runat="server" BorderWidth="1px" BorderColor="#000000" BorderStyle="Solid"></asp:Label>
																		            <asp:Label ID="lblinputtextbordercolor1" Runat="server"></asp:Label>
																	            </td>
																	            <td>
																		            <asp:textbox EnableViewState="True" id="txtinputtextbordercolor" Runat="server" CssClass="altText"></asp:textbox>
																		            <img title="<asp:Literal Text='<%$ Resources:WebResources, SelectColor%>' runat='server' />" onclick="FnShowPalette(event,'txtinputtextbordercolor')"
																			            height="23" alt="Color" src="../Image/color.jpg" width="27" name="imginputtextbordercolor" /><%--ZD 100419--%> <%-- Organization Css Module  --%>
																	            </td>
																            </tr>
																            <tr>
																	            <td class="tableBody"><asp:Literal Text="<%$ Resources:WebResources, UISettings_FontSize%>" runat="server"></asp:Literal></td>
																		            <td class="tableBody">
																			            <asp:Label ID="lblinputtextfontsize" Runat="server"></asp:Label>
																		            </td>
																		            <td>
																			            <asp:dropdownlist id="drpinputtextfontsize" Runat="server" CssClass="altText" Width="55">
																				            <asp:ListItem Value="" Text="<%$ Resources:WebResources, Size%>"></asp:ListItem>
																				            <asp:ListItem Value="8">8</asp:ListItem>
																				            <asp:ListItem Value="9">9</asp:ListItem>
																				            <asp:ListItem Value="10">10</asp:ListItem>
																				            <asp:ListItem Value="11">11</asp:ListItem>
																				            <asp:ListItem Value="12">12</asp:ListItem>
																				            <asp:ListItem Value="14">14</asp:ListItem>
																				            <asp:ListItem Value="16">16</asp:ListItem>
																				            <asp:ListItem Value="18">18</asp:ListItem>
																				            <asp:ListItem Value="20">20</asp:ListItem>
																				            <asp:ListItem Value="22">22</asp:ListItem>
																			            </asp:dropdownlist>
																		            </td>
																            </tr>
                                                                            <tr>
																		            <td class="tableBody"><asp:Literal Text="<%$ Resources:WebResources, UISettings_FontName%>" runat="server"></asp:Literal></td>
																		            <td class="tableBody">
																			            <asp:Label ID="lblinputtextfontname" Runat="server" ></asp:Label>
																		            </td>
																	                <td>
																		                <asp:dropdownlist id="drpinputtextfontname" Runat="server" CssClass="altText" Width="150">
																			                <asp:ListItem Value="" Text="<%$ Resources:WebResources, SelectFont%>"></asp:ListItem>
																			                <asp:ListItem Value="Arial">Arial</asp:ListItem>
																			                <asp:ListItem Value="Bookman Old Style">Bookman Old Style</asp:ListItem>
																			                <asp:ListItem Value="Century Gothic">Century Gothic</asp:ListItem>
																			                <asp:ListItem Value="Comic Sans Ms">Comic Sans Ms</asp:ListItem>
																			                <asp:ListItem Value="Courier New">Courier New</asp:ListItem>
																			                <asp:ListItem Value="Georgia">Georgia</asp:ListItem>
																			                <asp:ListItem Value="Microsoft Sans Serif">Microsoft Sans Serif</asp:ListItem>
																			                <asp:ListItem Value="Tahomo">Tahomo</asp:ListItem>
																			                <asp:ListItem Value="Times New Roman">Times New Roman</asp:ListItem>
																			                <asp:ListItem Value="Verdana">Verdana</asp:ListItem>
																		                </asp:dropdownlist>
																	                </td>
																	            </tr>											
															                    </table>
															            </td>
														            </tr>
												            </table>
											            </td>
										            </tr>
            										 <%--FB 1633 alignment end--%>
                                                </table>
								             </td>
								         </tr>
								         <%--Code added for FB 1473 - end--%>
									</TBODY>
								</TABLE>
								<BR>
								<TABLE id="Table17" cellSpacing="3" cellPadding="0" width="70%" border="0">
									<TBODY>
										<TR>
										    <%--code added for Soft Edge Button--%>
										    <td>
											    <input type='submit' name='SoftEdgeTest1' style='max-height:0px;max-width:0px;height:0px;width:0px;display:none'/> <%--Edited for FF--%>
										    </td>
											<TD align="center">
												<asp:button id="btnOriginal" runat="server" cssclass="altShort0BlueButtonFormat" text="<%$ Resources:WebResources, UISettings_btnOriginal%>" visible="false"></asp:button>
											</TD>
											<TD align="center" style="display:none"><!-- FB 2739 --><%--ZD 100263--%>
												<asp:button id="btnPreview" enabled="false" runat="server" cssclass="altShort0BlueButtonFormat" text="<%$ Resources:WebResources, UISettings_btnPreview%>"></asp:button>
											</TD>
											<TD align="center">
												<%--<asp:Button CssClass="altMedium0BlueButtonFormat" id="btnCancel" OnClientClick="FnCancel();" runat="server" Text="<%$ Resources:WebResources, Cancel%>" name="btnCancel" />--%>
                                                <input class="altMedium0BlueButtonFormat" id="btnCancel" onclick="javascript:return FnCancel();" type="button" name="btnCancel" value="<asp:Literal runat="server" Text="<%$ Resources:WebResources, Cancel%>" />" >
											</TD><%--ZD 100428--%>
											<TD align="center"><!-- FB 2739 -->
												<asp:button id="btnSave" enabled="true" runat="server" text="<%$ Resources:WebResources, UISettings_btnSave%>" width="150pt"></asp:button> <%--FB 2719--%> <%--FB 2796--%>
											</TD>
										</TR>
									</TBODY>
								</TABLE>
							</CENTER>
						</FORM>
					</TD>
				</TR>
<tr><td height=20></td></tr>
			</TBODY>
		</TABLE>
		<script language="javascript">
			function Browser() 
			{
			var ua, s, i;
			this.isIE    = false;
			this.isNS    = false;
			this.version = null;

			ua = navigator.userAgent;

			s = "MSIE";
			if ((i = ua.indexOf(s)) >= 0) {
				this.isIE = true;
				this.version = parseFloat(ua.substr(i + s.length));
				return;
			}

			s = "Netscape6/";
			if ((i = ua.indexOf(s)) >= 0) {
				this.isNS = true;
				this.version = parseFloat(ua.substr(i + s.length));
				return;
			}

			// Treat any other "Gecko" browser as NS 6.1.

			s = "Gecko";
			if ((i = ua.indexOf(s)) >= 0) {
				this.isNS = true;
				this.version = 6.1;
				return;
			}
			}

			var browser = new Browser();
			FnHideShow();
			//Div Height Patch for Netscape 
			if (browser.isNS) 
			{
				var divmd = document.getElementById("md");
				divmd.style.height = "260px"; 		
			}
			//Div Height Patch for Netscape 


			function FnShowPalette()
			{
				var args = FnShowPalette.arguments;
				var x,y,e;
				e= args[0]; 
				if (!e) e = window.event;
				if (browser.isIE) 
				{
					x = e.clientX + document.documentElement.scrollLeft + document.body.scrollLeft;
					y = e.clientY + document.documentElement.scrollTop + document.body.scrollTop;
				}
				if (browser.isNS) 
				{
					x = e.clientX + window.scrollX;
					y = e.clientY + window.scrollY;
				}
				show_RGBPalette(args[1],x,y);
			}

			function FnHideShow()
			{
				if(document.frmSample.ColorSystem[0].checked == true)
				{
					document.frmSample.NamedCol.disabled = false;
				}
				else
				{
					document.frmSample.NamedCol.disabled = true;
				}
				
				if(document.frmSample.ColorSystem[1].checked == true)
				{
					document.frmSample.TxtRGBColor.disabled = false;
					document.frmSample.rgbimg.disabled = false;					
//					document.frmSample.rgbimg.onclick = function(event)
//					{	FnShowPalette(event,'TxtRGBColor');	}
				}
				else
				{
					document.frmSample.TxtRGBColor.disabled = true;
					document.frmSample.rgbimg.disabled = true;
//					document.frmSample.rgbimg.onclick = function(){}
				}
			}

			function FnValidateForm()
			{
				if(document.frmSample.ColorSystem[0].checked == true)
				{
					if(document.frmSample.NamedCol.value == "0")
					{
					    alert(RSselectColor);
						return false;
					}
				}
				
				if(document.frmSample.ColorSystem[1].checked == true)
				{
					if(document.frmSample.TxtRGBColor.value == "")
					{
					    alert(RSRGBColor);
						return false;
					}
				}
				return true;	
			}

			function FnConfirm()
			{
                //Code added for FB 1473 - start
			    var hdnvalue = document.getElementById("hdnValue");
                var rThemeCustom = document.getElementById("RThemeCustom");
                
                if(rThemeCustom)
                {
                    if(!rThemeCustom.checked && hdnvalue.value == "")
                    {
                        alert(RSSelectTheme);
						return false;
                    }
                }

                //ZD 101475 - Start
                //Code added for FB 1473 - end
				//ZD 100429
                //var isConfrim = confirm(RSValueLostalert);
                //if (isConfrim == true) {

                    DataLoading(1); //ZD 100176
//                    return true;
//				}
//				else
//				{
//					return false;
                    //				}
                //ZD 101475 - End
			}
			
			function FnPreview()
			{
				window.open('Preview.aspx?styletype=P','Preview','resizable=yes,scrollbars=yes,toolbar=no, width=900,height=600')
			}

			function FnCancel() {
			    DataLoading(1); //ZD 100176
			    window.location.replace('OrganisationSettings.aspx');

			}
			function FnOriginal()
			{
				document.frmSample.submit();
            }
			//ZD 100420
            if (document.getElementById('RImg1') != null)
                document.getElementById('RImg1').setAttribute("onblur", "document.getElementById('btnBanner').focus(); document.getElementById('btnBanner').setAttribute('onfocus', '');");
            if (document.getElementById('btnBanner') != null)
                document.getElementById('btnBanner').setAttribute("onblur", "document.getElementById('RDefault').focus(); document.getElementById('RDefault').setAttribute('onfocus', '');");
            if (document.getElementById('ImageButton1') != null)
                document.getElementById('ImageButton1').setAttribute("onblur", "document.getElementById('RTheme1').focus(); document.getElementById('RTheme1').setAttribute('onfocus', '');");
            if (document.getElementById('Theme1') != null)
                document.getElementById('Theme1').setAttribute("onblur", "document.getElementById('RTheme2').focus(); document.getElementById('RTheme2').setAttribute('onfocus', '');");
            if (document.getElementById('Theme2') != null)
                document.getElementById('Theme2').setAttribute("onblur", "document.getElementById('btnCancel').focus(); document.getElementById('btnCancel').setAttribute('onfocus', '');");

            if (document.getElementById('btnCancel') != null)
                document.getElementById('btnCancel').setAttribute("onblur", "document.getElementById('btnSave').focus(); document.getElementById('btnSave').setAttribute('onfocus', '');");

            //ZD 100420
			//ZD 100428 START- Close the popup window using the esc key
            document.onkeydown = function(evt) {
                evt = evt || window.event;
                var keyCode = evt.keyCode;
                if (keyCode == 8) {
                    if (document.getElementById("btnCancel") != null) { // backspace
                        var str = document.activeElement.type;
                        if (!(str == "text" || str == "textarea" || str == "password")) {
                            document.getElementById("btnCancel").click();
                            return false;
                        }
                    }
                    if (document.getElementById("btnGoBack") != null) { // backspace
                        var str = document.activeElement.type;
                        if (!(str == "text" || str == "textarea" || str == "password")) {
                            document.getElementById("btnGoBack").click();
                            return false;
                        }
                    }
                }
                fnOnKeyDown(evt);
            };

            //ZD 104387 - Start
            function fnUpload(obj1, obj2, btnid, grpname) {
                if (!Page_ClientValidate(grpname))
                    return Page_IsValid;

                var val = "";
                if (obj1 != null && obj1.value != "")
                    val += obj1.value;
                if (obj2 != null && obj2.value != "")
                    val += obj2.value;

                if (val == "") {
                    alert(WithoutImage);
                    return false;
                }
                else {
                    __doPostBack(btnid, '');
                }
            }
            //ZD 104387 - End
			
			function EscClosePopup() {
			        if (document.getElementById('DefaultDiv') != null &&
                    document.getElementById('Theme1Div') != null && document.getElementById('Theme2Div') != null) {
			            document.getElementById('DefaultDiv').style.display = "none";
			            document.getElementById('Theme1Div').style.display = "none";
			            document.getElementById('Theme2Div').style.display = "none";
			            
			        }
			}
			//ZD 100428 END

		</script>
	</BODY>

</HTML>
<%--code added for Soft Edge button--%>
<script type="text/javascript" src="inc/softedge.js"></script>
<!-- #INCLUDE FILE="inc/mainbottomNET.aspx" -->