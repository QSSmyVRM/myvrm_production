//ZD 100147 Start
/* Copyright (C) 2015 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
//ZD 100147 End //ZD 100886
using System;
using System.Data;
using System.Xml;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Collections;
using System.Text.RegularExpressions;

/// <summary>
/// Summary description for Tier1
/// </summary>
/// 
namespace ns_DataImport
{
    public class User
    {
        int externalDatabaseType;
        DataTable masterDT;
        string configPath;
        ns_Logger.Logger log;
        myVRMNet.NETFunctions obj;
        MyVRMNet.LoginManagement obj1; //ZD 100456
        DataTable dtZone;
        DataView dvZone;
        Hashtable lstBridgeaddress = null;
        Hashtable lstUsrRoles = null;
        System.Web.UI.WebControls.DropDownList lstVideoEquipment;
        MyVRMNet.Util utilObj; //ALLDEV-782
        public User(int external, DataTable masterDataTable, string config)
        {
            externalDatabaseType = external;
            masterDT = masterDataTable;
            configPath = config;
            log = new ns_Logger.Logger();
            obj = new myVRMNet.NETFunctions();
            obj1 = new MyVRMNet.LoginManagement();//ZD 100456
            utilObj = new MyVRMNet.Util(); //ALLDEV-782
        }

        public bool Process(ref int cnt,ref bool alluserimport,ref DataTable dterror)
        {
            DataRow dr;
            //DataRow drNext; 
            String outXML = "";
            XmlDocument failedlist = new XmlDocument();
            XmlNodeList nodelist2;
            XmlNode newnode = null;
            DataRow errow = null;
            string lotus = "0", exchange = "0", mobile = "0", roleid = "0";
            String bridgeID = "";
            String inXML = "";
            string pattern = "",validatepattern = "",UserList ="";//ZD 104862
            inXML += "<login>";
            inXML += obj.OrgXMLElement();//Organization Module Fixes
            inXML += "  <userID>" + HttpContext.Current.Session["userID"].ToString() + "</userID>";
            inXML += "</login>";
            outXML = obj.CallMyVRMServer("GetBridgeList", inXML, configPath);
            XmlDocument xmldoc = new XmlDocument();
            xmldoc.LoadXml(outXML);
            XmlNodeList nodes = xmldoc.SelectNodes("//bridgeInfo/bridges/bridge");
            if (nodes.Count > 0)
            {
                for (int j = 0; j < nodes.Count; j++)
                {
                    if (lstBridgeaddress == null)
                        lstBridgeaddress = new Hashtable();

                    if (!lstBridgeaddress.Contains(obj.GetTranslatedText(nodes[j]["name"].InnerText.ToString().Trim())))
                        lstBridgeaddress.Add(obj.GetTranslatedText(nodes[j]["name"].InnerText.ToString().Trim()), nodes[j]["ID"].InnerText.ToString());
                }

            }

            //ZD 100456
            String inxml = "<login>" + obj.OrgXMLElement() +
                    "  <userID>" + HttpContext.Current.Session["userID"].ToString() + "</userID>" +
                    "  <foodModuleEnable>1</foodModuleEnable><roomModuleEnable>1</roomModuleEnable>" +
                    "  <hkModuleEnable>1</hkModuleEnable></login>";

            String outxml = obj.CallMyVRMServer("GetUserRoles", inxml, configPath);
            xmldoc.LoadXml(outxml);
            XmlNodeList xmlnodes = xmldoc.SelectNodes("//userRole/roles/role");
            if (xmlnodes.Count > 0)
            {
                for (int j = 0; j < xmlnodes.Count; j++)
                {
                    if (lstUsrRoles == null)
                        lstUsrRoles = new Hashtable();

                    if (!lstUsrRoles.Contains(obj.GetTranslatedText(xmlnodes[j]["name"].InnerText.ToString().Trim())))
                        lstUsrRoles.Add(obj.GetTranslatedText(xmlnodes[j]["name"].InnerText.ToString().Trim()), xmlnodes[j]["ID"].InnerText.ToString());
                }
            }
            
            lstVideoEquipment = new DropDownList();
            lstVideoEquipment.DataValueField = "VideoEquipmentID";
            lstVideoEquipment.DataTextField = "VideoEquipmentName";
            obj.BindVideoEquipment(lstVideoEquipment);
            
            for (int j = 0; j < masterDT.Rows.Count; j++)
            {
                dr = masterDT.Rows[j];
                errow = dterror.NewRow();
                String lstConferenceTZ = null, tdzone = "";
                int errorusernum = 0;
                bridgeID = "0";
                errorusernum = j + 2;
                //drNext = masterDT.Rows[j + 1];
                //ZD 103896
                if (dr["id"].ToString().ToLower().IndexOf(obj.GetTranslatedText("note").ToLower()) >= 0)
                    continue; //ZD 104862

				//ZD 103055
                string tzone = "";
                if (masterDT.Columns.Contains(obj.GetTranslatedText("Time Zone")))
                    tzone = dr[obj.GetTranslatedText("Time Zone")].ToString();
                else
                    tzone = dr[obj.GetTranslatedText("Timezone")].ToString();

                if (dr[obj.GetTranslatedText("First Name")].ToString() == "" || dr[obj.GetTranslatedText("Last Name")].ToString() == "" 
                    || dr[obj.GetTranslatedText("Email")].ToString() == "" || dr[obj.GetTranslatedText("Initial Password")].ToString() == ""
                    || tzone  == "")
                {
                    //ZD 100456
                    if (dr[obj.GetTranslatedText("First Name")].ToString() == "" && dr[obj.GetTranslatedText("Last Name")].ToString() == "" 
                        && dr[obj.GetTranslatedText("Email")].ToString() == "" && dr[obj.GetTranslatedText("Initial Password")].ToString() == ""
                        && tzone == "")
                        continue;

                    if (dr[obj.GetTranslatedText("First Name")].ToString() == "")
                    {
                        errow["Row No"] = errorusernum.ToString();
                        errow["Reason"] = obj.GetTranslatedText("User doesn't have first name");
                        alluserimport = false;
                        dterror.Rows.Add(errow);
                        continue;
                    }
                    if (dr[obj.GetTranslatedText("Last Name")].ToString() == "")
                    {
                        errow["Row No"] = errorusernum.ToString();
                        errow["Reason"] = obj.GetTranslatedText("User doesn't have Last name");
                        alluserimport = false;
                        dterror.Rows.Add(errow);
                        continue;
                    }
                    if (dr[obj.GetTranslatedText("Email")].ToString() == "")
                    {
                        errow["Row No"] = errorusernum.ToString();
                        errow["Reason"] = obj.GetTranslatedText("User doesn't have Email Id");
                        alluserimport = false;
                        dterror.Rows.Add(errow);
                        continue;
                    }
                    if (dr[obj.GetTranslatedText("Initial Password")].ToString() == "")
                    {
                        errow["Row No"] = errorusernum.ToString();
                        errow["Reason"] = obj.GetTranslatedText("User doesn't have Password");
                        alluserimport = false;
                        dterror.Rows.Add(errow);
                        continue;
                    }
                    if (tzone == "")//ZD 103055
                    {
                        errow["Row No"] = errorusernum.ToString();
                        errow["Reason"] = obj.GetTranslatedText("User doesn't have Timezone");
                        alluserimport = false;
                        dterror.Rows.Add(errow);
                        continue;
                    }
                }
                else
                {
                    pattern = @"^(a-z|A-Z|0-9)*[^\\/<>+?|!`\[\]{}\=@#$%&~]*$";
                    Regex check = new Regex(pattern);
                    if (!check.IsMatch(dr[obj.GetTranslatedText("First Name")].ToString(), 0))
                    {
                        errow["Row No"] = errorusernum.ToString();
                        errow["Reason"] = "<br>& < > + % ? | = ! ` [ ] { } # $ @ and ~ are invalid characters.";
                        alluserimport = false;
                        dterror.Rows.Add(errow);
                        continue;
                    }
                    if (!check.IsMatch(dr[obj.GetTranslatedText("Last Name")].ToString(), 0))
                    {
                        errow["Row No"] = errorusernum.ToString();
                        errow["Reason"] = "<br>& < > + % ? | = ! ` [ ] { } # $ @ and ~ are invalid characters.";
                        alluserimport = false;
                        dterror.Rows.Add(errow);
                        continue;
                    }
                    if (dr[obj.GetTranslatedText("Email")].ToString() != "")
                    {
                        String email = dr[obj.GetTranslatedText("Email")].ToString();
                        if (!email.Contains("@"))
                        {
                            errow["Row No"] = errorusernum.ToString();
                            errow["Reason"] = obj.GetTranslatedText("User doesn't have valid Email Id");
                            alluserimport = false;
                            dterror.Rows.Add(errow);
                            continue;
                        }
                        String em = email.Split('@')[1];
                        if(!em.Contains("."))
                        {
                            errow["Row No"] = errorusernum.ToString();
                            errow["Reason"] = obj.GetTranslatedText("User doesn't have valid Email Id");
                            alluserimport = false;
                            dterror.Rows.Add(errow);
                            continue;
                        }
                    }
                    validatepattern = @"^[^<>&]*$"; //ZD 104850 start
                    Regex Checkinvalid = new Regex(validatepattern);
                    if(dr[obj.GetTranslatedText("AD/LDAP Login")].ToString()!= "")
                    {
                        
                        if (!Checkinvalid.IsMatch(dr[obj.GetTranslatedText("AD/LDAP Login")].ToString(), 0))
                        {
                            errow["Row No"] = errorusernum.ToString();
                            errow["Reason"] = obj.GetTranslatedText("AD/LDAP Login contain amp; lt; and gt; invalid characters.").Replace("amp;", "&").Replace("gt;", ">").Replace("lt;", "<");
                            alluserimport = false;
                            dterror.Rows.Add(errow);
                            continue;
                        }
                    }

                    if (dr[obj.GetTranslatedText("AD/LDAP Domain")].ToString() != "")
                    {
                        if (!Checkinvalid.IsMatch(dr[obj.GetTranslatedText("AD/LDAP Domain")].ToString(), 0))
                        {
                            errow["Row No"] = errorusernum.ToString();
                            errow["Reason"] = obj.GetTranslatedText("AD/LDAP Domain contain amp; lt; and  gt;  invalid characters.").Replace("amp;", "&").Replace("gt;", ">").Replace("lt;", "<");
                            alluserimport = false;
                            dterror.Rows.Add(errow);
                            continue;
                        }
                        

                    } //ZD 104850 End

                    //ALLDEV-782 Starts
                    validatepattern = @"^[^<>#&$%{}\[\];:]*$";
                    Checkinvalid = new Regex(validatepattern);
                    if (dr[obj.GetTranslatedText("Pexip Static VMR")].ToString() != "")
                    {

                        if (!Checkinvalid.IsMatch(dr[obj.GetTranslatedText("Pexip Static VMR")].ToString(), 0))
                        {
                            errow["Row No"] = errorusernum.ToString();
                            errow["Reason"] = obj.GetTranslatedText("Pexip Static VMR contain amp; # lt; $%{}[];: and gt; invalid characters.").Replace("amp;", "&").Replace("gt;", ">").Replace("lt;", "<");
                            alluserimport = false;
                            dterror.Rows.Add(errow);
                            continue;
                        }
                    }
                    //ALLDEV-782 Ends

                    //ZD 100456
                    //string outuser = dr["Outlook/Notes"].ToString();
                    //if(outuser == "Outlook")
                    //{
                    //     lotus = "0"; exchange = "1"; mobile = "0";
                    //}
                    //else if(outuser == "Notes")
                    //{
                    //    lotus = "1"; exchange = "0"; mobile = "0";
                    //}
                    //else if (outuser == "Mobile")
                    //{
                    //    lotus = "0"; exchange = "0"; mobile = "1";
                    //}
                    //else
                    //{
                    //    lotus = "0"; exchange = "0"; mobile = "0";
                    //}

                    exchange = "0";
                    if (dr[obj.GetTranslatedText("Outlook")].ToString().ToLower().Trim() == obj.GetTranslatedText("yes").ToLower())
                        exchange = "1";

                    lotus = "0";
                    if (dr[obj.GetTranslatedText("IBM Notes Forms")].ToString().ToLower().Trim() == obj.GetTranslatedText("yes").ToLower())
                        lotus = "1"; //ZD 102743

                    string userrole = "";
                    if(masterDT.Columns.Contains(obj.GetTranslatedText("User Role (User/Admin/Super Admin etc)")))
                        userrole = dr[obj.GetTranslatedText("User Role (User/Admin/Super Admin etc)")].ToString().Trim();//ZD 100456
                    else
                        userrole = dr[obj.GetTranslatedText("User Role (User/Admin/Super Adminetc)")].ToString().Trim();//ZD 100456
                    roleid = "1";
                    if (userrole == "Super Admin")
                    {
                        roleid = "3";
                    }
                    else if (userrole == "Admin")
                    {
                        roleid = "2";
                    }//ZD 100456
                    else
                    {
                        if (lstUsrRoles != null)
                            if (lstUsrRoles.Contains(obj.GetTranslatedText(userrole)))
                                roleid = lstUsrRoles[obj.GetTranslatedText(userrole)].ToString();
                    }

                    //if (userrole.ToLower() == "site administrator" && HttpContext.Current.Session["organizationID"].ToString() != HttpContext.Current.Session["loginUserSiloID"].ToString())
                    if (userrole.ToLower() == "site administrator" && HttpContext.Current.Session["organizationID"].ToString() != "11")
                    {
                        roleid = "1";
                    }

                    bridgeID = "";
                    if (lstBridgeaddress != null)
                        if (lstBridgeaddress.Contains(dr[obj.GetTranslatedText("Assigned MCU (only in case of multiple)")].ToString().Trim()))
                            bridgeID = lstBridgeaddress[dr[obj.GetTranslatedText("Assigned MCU (only in case of multiple)")].ToString().Trim()].ToString();
                                        
                    lstConferenceTZ = tzone;//ZD 103055
                    
                    tdzone = GetTimeZoneID(lstConferenceTZ);

                    //ZD 104021
                    string BJNUserName = "";
                    if (masterDT.Columns.Contains(obj.GetTranslatedText("Blue Jeans User Name")))
                        BJNUserName = dr[obj.GetTranslatedText("Blue Jeans User Name")].ToString();

                    //ALLDEV-782
                    string pexipStaticVMR = "";
                    if (masterDT.Columns.Contains(obj.GetTranslatedText("Pexip Static VMR")))
                        pexipStaticVMR = utilObj.ReplaceInXMLSpecialCharacters(dr[obj.GetTranslatedText("Pexip Static VMR")].ToString());


                    //ZD 100456
                    String usrID = "";
                    if (dr["id"].ToString() != "")
                    {
                        usrID = dr["id"].ToString();
                        obj1.simpleDecrypt(ref usrID);
                    }

                    String userInXML = Create_userInXML(dr[obj.GetTranslatedText("First Name")].ToString(), dr[obj.GetTranslatedText("Last Name")].ToString()
                        , dr[obj.GetTranslatedText("Email")].ToString(), dr[obj.GetTranslatedText("Initial Password")].ToString()
                        , tdzone, exchange, lotus, mobile, roleid, bridgeID, usrID, BJNUserName, dr[obj.GetTranslatedText("AD/LDAP Login")].ToString(),
                        dr[obj.GetTranslatedText("AD/LDAP Domain")].ToString(), pexipStaticVMR); //ZD 100456 //ZD 104021 //ZD 104850 //ALLDEV-782
                    log.Trace("<br>" + userInXML);
                    outXML = obj.CallMyVRMServer("SetUser", userInXML, configPath);
                    log.Trace(outXML);
                    if (outXML.IndexOf("<error>") >= 0)
                    {
                        alluserimport = false;
                        //write to a log file                 
                        log.Trace(obj.ShowErrorMessage(outXML));
                        failedlist.LoadXml(outXML);
                        newnode = failedlist.SelectSingleNode("//error/message");
                        nodelist2 = failedlist.SelectNodes("//error/message");
                        if (nodelist2.Count > 0)
                        {
                            errow["Row No"] = errorusernum.ToString();
                            errow["Reason"] = newnode.InnerXml;
                            dterror.Rows.Add(errow);
                        }
                    }
                    else
                    {
                        XmlDocument Xd = new XmlDocument();
                        Xd.LoadXml(outXML);
                        string userID = Xd.SelectSingleNode("//SetUser/userID").InnerText;
                        if (UserList == "") //ZD 104862 start
                            UserList = userID;
                        else
                            UserList += "," + userID; //ZD 104862 End

                        String endpointID = "";
                        if (usrID == "new" || usrID == "")
                            endpointID = "new";

                        //ZD 104821 Starts
                        string SysLoc = "";
                        if (dr[obj.GetTranslatedText("System Location")] != null)
                            SysLoc = dr[obj.GetTranslatedText("System Location")].ToString();
                        //ZD 104821 Ends
                        String strinXML = GenerateInxml(outXML
                            , dr[obj.GetTranslatedText("First Name")].ToString() + " " + dr[obj.GetTranslatedText("Last Name")].ToString(), endpointID, bridgeID, SysLoc); //ZD 104821

                        outXML = obj.CallMyVRMServer("SetEndpoint", strinXML, configPath);

                        log.Trace("Success");
                        cnt++;
                    }
                }
            }
            if (UserList != "" && HttpContext.Current.Session["EnableBlockUserDI"].ToString() == "1") //ZD 104862 start
            {
                inXML = "<SetBlockUserFromDI>";
                inXML += "<UserID>" + HttpContext.Current.Session["userID"].ToString() + "</UserID>";
                inXML += obj.OrgXMLElement();
                inXML += "<Userlist>"+ UserList +"</Userlist>";
                inXML += "</SetBlockUserFromDI>";
                outXML = obj.CallMyVRMServer("SetUsrBlockStatusFrmDataimport", inXML, configPath); //ZD 104862 End
            }
            return true;
        }

        private String Create_userInXML(String fName, String lName, String userEmail, String password, String tdzone, String exchange, String lotus
            , String mobile, String roleid, String bridgeid, String usrID, string BJNUserName, string ADLdapLogin, string ADLdapusrDomain, string pexipStaticVMR) //ZD 100456 //ZD 104021 //ALLDEV-782
        {
            try
            {
                //ZD 100456
                String editfrom = "D";
                if (usrID == "")
                {
                    usrID = "new";
                    editfrom = "ND"; //ZD 104862
                }
                String inXML = ""; 
                inXML += "<saveUser>";
                inXML += obj.OrgXMLElement();
                inXML += "<login>";
                inXML += "<userID>" + HttpContext.Current.Session["userID"].ToString() + "</userID>";
                inXML += "</login>";
                inXML += "<EditFrom>" + editfrom + "</EditFrom>"; //ZD 100456
                inXML += "<user>";
                inXML += "<userID>" + usrID + "</userID>"; //ZD 100456
                inXML += "<userName>";
                inXML += "<firstName>" + fName + "</firstName>";
                inXML += "<lastName>" + lName + "</lastName>";
                inXML += "</userName>";
                inXML += "<login>" + ADLdapLogin + "</login>"; //ZD 100456 //104850
                inXML += "<UserDomain>" + ADLdapusrDomain + "</UserDomain>";//ZD 104850
                inXML += "<password>" + UserPassword(password) + "</password>";//FB 2519
                inXML += "<isPwdChanged>1</isPwdChanged>";//ZD 100781
                inXML += "<userEmail>" + userEmail + "</userEmail>";
                inXML += "<alternativeEmail></alternativeEmail>";
                inXML += "<SavedSearch>-1</SavedSearch>";
                inXML += "<workPhone></workPhone>";
                inXML += "<cellPhone></cellPhone>";
                inXML += "<sendBoth>0</sendBoth>";
                inXML += "<enableParticipants>1</enableParticipants>"; //FB 2519
                inXML += "<enableAdditionalOption>1</enableAdditionalOption>"; //ZD 101093
                inXML += "<enableAV>1</enableAV>"; //FB 2519
                inXML += "<enableAVWorkOrder>1</enableAVWorkOrder>";//ZD 101122
                inXML += "<enableCateringWO>1</enableCateringWO>";
                inXML += "<enableFacilityWO>1</enableFacilityWO>";
                inXML += "<emailClient>-1</emailClient>";//FB 2519
                inXML += "<userInterface>2</userInterface>";
                if (tdzone == "")
                    tdzone = "26";
                inXML += "<timeZone>" + tdzone + "</timeZone>";
                inXML += "<location></location>";
                inXML += "<dateFormat>MM/dd/yyyy</dateFormat>";
                inXML += "<tickerStatus>1</tickerStatus>";
                inXML += "<tickerPosition>0</tickerPosition>";
                inXML += "<tickerSpeed>3</tickerSpeed>";
                inXML += "<tickerBackground>#660066</tickerBackground>";
                inXML += "<tickerDisplay>0</tickerDisplay>";
                inXML += "<rssFeedLink></rssFeedLink>";
                inXML += "<tickerStatus1>1</tickerStatus1>";
                inXML += "<tickerPosition1>0</tickerPosition1>";
                inXML += "<tickerSpeed1>3</tickerSpeed1>";
                inXML += "<tickerBackground1>#99ff99</tickerBackground1>";
                inXML += "<tickerDisplay1>0</tickerDisplay1>";
                inXML += "<rssFeedLink1></rssFeedLink1>";
                inXML += "<lineRateID>768</lineRateID>";
                inXML += "<videoProtocol>IP</videoProtocol>";
                inXML += "<conferenceCode></conferenceCode>";
                inXML += "<leaderPin></leaderPin>";
                inXML += "<IPISDNAddress></IPISDNAddress>";
                inXML += "<connectionType>1</connectionType>";
                inXML += "<videoEquipmentID></videoEquipmentID>";
                inXML += "<isOutside>0</isOutside>";
                inXML += "<addressTypeID>1</addressTypeID>";
                inXML += "<group></group>";
                inXML += "<ccGroup></ccGroup>";
                inXML += "<roleID>" + roleid + "</roleID>";
                inXML += "<initialTime>10000</initialTime>";
                inXML += "<expiryDate></expiryDate>";
                inXML += "<bridgeID>" + bridgeid + "</bridgeID>";
                inXML += "<languageID></languageID>";
                inXML += "<EmailLang></EmailLang>";
                inXML += "<departments>";
                inXML += "</departments>";
                inXML += "<status>";
                inXML += "<deleted>0</deleted>";
                inXML += "<locked>0</locked>";
                inXML += "</status>";
                inXML += "<creditCard>0</creditCard>";
                inXML += "<IntVideoNum></IntVideoNum>";
                inXML += "<ExtVideoNum></ExtVideoNum>";
                inXML += "</user>";
                inXML += "<emailMask>1</emailMask>";
                inXML += "<exchangeUser>" + exchange + "</exchangeUser>";
                //ZD 104862 - Start
                inXML += "<dominoUser></dominoUser>"; 
                //inXML += "<dominoUser>" + lotus + "</dominoUser>";
                inXML += "<mobileUser></mobileUser>";
                //inXML += "<mobileUser>" + mobile + "</mobileUser>";
                //ZD 104862 - End
                inXML += "<audioaddon>0</audioaddon>";
                //FB 2519 start
                inXML += "<location></location>";
                inXML += "<SavedSearch></SavedSearch>";
                inXML += "<ExchangeID></ExchangeID>";
                inXML += "<HelpRequsetorEmail></HelpRequsetorEmail>";
                inXML += "<HelpRequsetorPhone></HelpRequsetorPhone>";
                inXML += "<RFIDValue></RFIDValue>";
                inXML += "<SendSurveyEmail></SendSurveyEmail>";
                inXML += "<EnableVNOCselection>0</EnableVNOCselection>";
                inXML += "<Secured>0</Secured>";
                inXML += "<PrivateVMR></PrivateVMR>";
                inXML += "<EnablePCUser>0</EnablePCUser>";
                //FB 2519 End
                //ZD 104021
                if (!string.IsNullOrEmpty(BJNUserName))
                    inXML += "<EnableBJNConf>1</EnableBJNConf>";
                else
                    inXML += "<EnableBJNConf>0</EnableBJNConf>";
                inXML += "<BJNUserName>" + BJNUserName + "</BJNUserName>";
                inXML += " <PexipStaticVMR>" + pexipStaticVMR + "</PexipStaticVMR>"; //ALLDEV-782
                inXML += "</saveUser>";
                return inXML;
            }
            catch (Exception ex)
            {
                return "";
            }
        }

        protected String GetTimeZoneID(String tZoneStr)
       {

           DataSet dsZone = new DataSet();
           myVRMNet.NETFunctions obj;
           String tZoneID = "";
           String tZone = "";
           String[] tZoneArr = null;
           try
           {
               //code added for BCS - start
               //Time Zone
               String zoneInXML = "<GetTimezones><UserID>" + HttpContext.Current.Session["userID"].ToString() + "</UserID></GetTimezones>";
               String zoneOutXML;
               tZone = tZoneStr;
               if (dtZone == null)
               {
                   if (dsZone.Tables.Count == 0)
                   {
                       obj = new myVRMNet.NETFunctions();
                       zoneOutXML = obj.CallMyVRMServer("GetTimezones", zoneInXML, HttpContext.Current.Application["MyVRMServer_ConfigPath"].ToString());
                       obj = null;
                       XmlDocument xmldoc = new XmlDocument();
                       xmldoc.LoadXml(zoneOutXML);
                       XmlNodeList nodes = xmldoc.SelectNodes("//Timezones/timezones/timezone");
                       if (nodes.Count > 0)
                       {
                           XmlTextReader xtr;
                           foreach (XmlNode node in nodes)
                           {
                               xtr = new XmlTextReader(node.OuterXml, XmlNodeType.Element, new XmlParserContext(null, null, null, XmlSpace.None));
                               dsZone.ReadXml(xtr, XmlReadMode.InferSchema);
                           }
                           if (dsZone.Tables.Count > 0)
                           {
                               dvZone = new DataView(dsZone.Tables[0]);
                               dtZone = dvZone.Table;
                           }
                       }
                       //code added for BCS - start
                   }
               }
               if (tZone != "")
               {
                   tZoneArr = tZone.Split(' ');
                   int tzonelength = tZoneArr.Length;
                   String t1 = null, t2 = null;
                   if (tzonelength == 1)
                   {
                       t1 = tZoneArr[0];
                   }
                   else
                   {
                       t1 = tZoneArr[0];
                       t2 = tZoneArr[1];
                   }
                   foreach (DataRow row in dtZone.Rows)
                   {
                       if (tzonelength == 1)
                       {
                           if (row["timezoneName"].ToString().Contains(t1))
                           {

                               tZoneID = row["timezoneID"].ToString();
                               break;
                           }
                       }
                       else
                       {
                           //ZD 100456
                           if (row["timezoneName"].ToString() == tZone)
                           {
                               tZoneID = row["timezoneID"].ToString();
                               break;
                           }
                           else if (row["StandardName"].ToString().Contains(t1) && row["StandardName"].ToString().Contains(t2))
                           {
                               tZoneID = row["timezoneID"].ToString();
                               break;
                           }
                           else if (row["timezoneName"].ToString().Contains(t1) && row["timezoneName"].ToString().Contains(t2))
                           {
                               tZoneID = row["timezoneID"].ToString();
                               break;
                           }
                       }

                   }

               }

           }
           catch (Exception ex)
           {
               throw ex;
           }

           return tZoneID;
       }
        //FB 2519 start
        protected string UserPassword(string PW)
        {
            string Encrypted = "";
            XmlDocument docs = null;
            try
            {
                string inxmls = "<System><Cipher>" + PW + "</Cipher></System>";

                string outXML = obj.CallMyVRMServer("GetEncrpytedText", inxmls, HttpContext.Current.Application["MyVRMServer_ConfigPath"].ToString());

                if (outXML.IndexOf("<error>") < 0)
                {
                    docs = new XmlDocument();
                    docs.LoadXml(outXML);
                    XmlNode nde = docs.SelectSingleNode("System/Cipher");
                    if (nde != null)
                        Encrypted = nde.InnerXml;
                }
                else
                {
                     obj.ShowErrorMessage(outXML);
                }
            }
            catch (Exception ex)
            {
            }
            return Encrypted;
        }
        //FB 2519 End

        #region GenerateInxml
        protected String GenerateInxml(String outXML, String eptName, String endpointID, String bridgeID , string SystemLoc) //ZD 104821
        {
            try
            {

                XmlDocument xmldoc = new XmlDocument();
                xmldoc.LoadXml(outXML);
                String usrid = xmldoc.SelectSingleNode("//SetUser/userID").InnerText;

                //ZD 101362
                if (bridgeID.Trim() == "")
                    bridgeID = "-1"; //ZD 101580
              
                String inXML = "<SetEndpoint>";
                inXML += obj.OrgXMLElement();
                inXML += "      <EndpointID>" + endpointID + "</EndpointID>";
                inXML += "      <EndpointName>" + eptName + "</EndpointName>";
                inXML += "      <EntityType>U</EntityType>";
                inXML += "      <UserID>" + usrid + "</UserID>";
                inXML += "      <Profiles>";
                inXML += "        <Profile>";
                inXML += "          <ProfileID>0</ProfileID>";
                inXML += "          <ProfileName>" + eptName + "</ProfileName>";
                inXML += "          <Default>0</Default>";
                inXML += "          <EncryptionPreferred>0</EncryptionPreferred>";
                inXML += "          <AddressType>-1</AddressType>"; //ZD 101580
                inXML += "          <Password></Password>";
                inXML += "          <Address></Address>";
                inXML += "          <URL></URL>";
                inXML += "          <IsOutside>0</IsOutside>";
                inXML += "          <SysLocationName>" + SystemLoc + "</SysLocationName>"; //ZD 104821
                inXML += "          <ConnectionType>-1</ConnectionType>"; //ZD 101580
                inXML += "          <VideoEquipment>-1</VideoEquipment>"; //ZD 101580
                inXML += "          <LineRate>768</LineRate>";
                inXML += "          <Bridge>" + bridgeID + "</Bridge>";
                inXML += "          <MCUAddress></MCUAddress>";
                inXML += "          <ApiPortno>23</ApiPortno>";
                inXML += "          <DefaultProtocol>-1</DefaultProtocol>"; //ZD 101580
                if(endpointID == "new")
                    inXML += "          <EditFrom></EditFrom>"; //ZD 100456
                else
                    inXML += "          <EditFrom>D</EditFrom>"; //ZD 100456
                inXML += "          <MCUAddressType>-1</MCUAddressType>"; //ZD 101580
                inXML += "          <Deleted>0</Deleted>";
                inXML += "          <ExchangeID></ExchangeID>";
                inXML += "          <TelnetAPI>0</TelnetAPI>";
                inXML += "          <SSHSupport>0</SSHSupport>";//ZD 101363
                inXML += "          <conferenceCode></conferenceCode>";
                inXML += "          <leaderPin></leaderPin>";
                inXML += "        </Profile>";
                inXML += "      </Profiles>";
                inXML += "    </SetEndpoint>";
                return inXML;
            }
            catch (Exception ex)
            {
                log.Trace("GenerateInxml:" + ex.Message);
                return "<error></error>";
            }
        }
        #endregion
    }
}