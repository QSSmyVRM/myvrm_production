/* Copyright (C) 2015 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
//ZD 100147 ZD 100886
using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml;
using System.IO;
using System.Text;
using System.Collections.Specialized;
using System.Collections.Generic;
using System.Web.Services;
using System.Text.RegularExpressions;

namespace en_ExpressConference
{
    public partial class ExpressConference : System.Web.UI.Page
    {
        #region Private Data Members
        Boolean isSetCustom = false;

        protected System.Web.UI.ScriptManager ConfScriptManager;

        protected System.Web.UI.WebControls.TextBox hdnApprover4;
        protected System.Web.UI.WebControls.TextBox hdnApprover7; //FB 2501
        protected System.Web.UI.WebControls.TextBox hdnApproverMail;//FB 2501
        protected System.Web.UI.WebControls.TextBox hdnRequestorMail;//FB 2501 
        protected System.Web.UI.HtmlControls.HtmlInputHidden RecurringText, hdnIsHDBusy; //ALLDEV-807
        protected System.Web.UI.WebControls.DataGrid dgConflict;
        protected System.Web.UI.WebControls.Table tblConflict;
        protected System.Web.UI.WebControls.TextBox txtApprover4;
        protected System.Web.UI.WebControls.TextBox txtApprover7; //FB 2501
        protected System.Web.UI.WebControls.TextBox confEndDate;
        protected System.Web.UI.WebControls.TextBox confStartDate;
        protected System.Web.UI.WebControls.TextBox txtModifyType;
        protected System.Web.UI.WebControls.TextBox ConferenceName;
        protected System.Web.UI.WebControls.TextBox ConferenceDescription;
        protected System.Web.UI.WebControls.TextBox txtPartysInfo;
        protected System.Web.UI.WebControls.TextBox txtUsersStr;
        protected System.Web.UI.WebControls.TextBox txtTimeCheck;
        protected System.Web.UI.HtmlControls.HtmlButton btnUploadFiles; //ZD 100420
        protected AjaxControlToolkit.ModalPopupExtender ModalPopupExtender1;
        protected AjaxControlToolkit.ModalPopupExtender RoomPopUp;
        protected AjaxControlToolkit.ModalPopupExtender SubModalPopupExtender;
        protected AjaxControlToolkit.ModalPopupExtender p2pConfirmPopup; //ZD 100815
        protected System.Web.UI.WebControls.Label showConfMsg;
        protected System.Web.UI.UpdatePanel UpdatePanel1;
        protected System.Web.UI.UpdatePanel UpdatePanelRooms;
        protected System.Web.UI.WebControls.Button btnConfSubmit;
        protected System.Web.UI.WebControls.Button btnDummy; //FB 1830 Email Edit
        protected System.Web.UI.WebControls.Label aFileUp;
        protected System.Web.UI.WebControls.Label plblGuestLocation;//FB 2426
        protected System.Web.UI.WebControls.TextBox RecurDurationhr;
        protected System.Web.UI.WebControls.TextBox RecurDurationmi;
        protected System.Web.UI.WebControls.TextBox EndText;
        //protected System.Web.UI.WebControls.RadioButtonList RecurType; ZD 103929
        protected System.Web.UI.WebControls.RadioButton DEveryDay;
        protected System.Web.UI.WebControls.TextBox DayGap;
        protected System.Web.UI.WebControls.RadioButton DWeekDay;
        protected System.Web.UI.WebControls.Panel Daily;
        protected System.Web.UI.WebControls.TextBox WeekGap;
        protected System.Web.UI.WebControls.CheckBoxList WeekDay;
        protected System.Web.UI.WebControls.Panel Weekly;
        protected System.Web.UI.WebControls.RadioButton MEveryMthR1;
        protected System.Web.UI.WebControls.TextBox MonthDayNo;
        protected System.Web.UI.WebControls.TextBox MonthGap1;
        protected System.Web.UI.WebControls.RadioButton MEveryMthR2;
        protected System.Web.UI.WebControls.DropDownList MonthWeekDayNo;
        protected System.Web.UI.WebControls.DropDownList MonthWeekDay;
        protected System.Web.UI.WebControls.DropDownList lstTemplates;//ZD 100570 Inncrewin 23-12-2013
        protected System.Web.UI.WebControls.TextBox MonthGap2;
        protected System.Web.UI.WebControls.Panel Monthly;
        protected System.Web.UI.WebControls.RadioButton YEveryYr1;
        protected System.Web.UI.WebControls.DropDownList YearMonth1;
        protected System.Web.UI.WebControls.TextBox YearMonthDay;
        protected System.Web.UI.WebControls.RadioButton YEveryYr2;
        protected System.Web.UI.WebControls.DropDownList YearMonthWeekDayNo;
        protected System.Web.UI.WebControls.DropDownList YearMonthWeekDay;
        protected System.Web.UI.WebControls.DropDownList YearMonth2;
        protected System.Web.UI.WebControls.Panel Yearly;
        protected System.Web.UI.WebControls.ListBox CustomDate;
        protected System.Web.UI.WebControls.Button btnsortDates;
        protected System.Web.UI.WebControls.Panel Custom;
        protected System.Web.UI.WebControls.TextBox StartDate;
        protected System.Web.UI.WebControls.RadioButton EndType;
        protected System.Web.UI.WebControls.RadioButton REndAfter;
        protected System.Web.UI.WebControls.TextBox Occurrence;
        protected System.Web.UI.WebControls.RadioButton REndBy;
        protected System.Web.UI.WebControls.TextBox EndDate;
        protected System.Web.UI.WebControls.Button Cancel;
        protected System.Web.UI.WebControls.Button Reset;
        protected System.Web.UI.WebControls.Button RecurSubmit;
        protected System.Web.UI.WebControls.Button RecurSubmit1;
        protected System.Web.UI.HtmlControls.HtmlTableRow RangeRow;
        protected System.Web.UI.WebControls.DropDownList RecurSetuphr;
        protected System.Web.UI.WebControls.DropDownList RecurSetupmi;
        protected System.Web.UI.WebControls.DropDownList RecurSetupap;
        protected System.Web.UI.WebControls.DropDownList RecurTeardownhr;
        protected System.Web.UI.WebControls.DropDownList RecurTeardownmi;
        protected System.Web.UI.WebControls.DropDownList RecurTeardownap;
        protected System.Web.UI.HtmlControls.HtmlInputHidden hdnValue;
        protected System.Web.UI.HtmlControls.HtmlInputHidden hdnRecurValue;

        protected MetaBuilders.WebControls.ComboBox confStartTime;
        protected MetaBuilders.WebControls.ComboBox confEndTime;
        protected MetaBuilders.WebControls.ComboBox lstDuration;
        protected MetaBuilders.WebControls.ComboBox startByTime;
        protected MetaBuilders.WebControls.ComboBox completedByTime;

        protected System.Web.UI.HtmlControls.HtmlInputHidden hdnSetupTime;
        protected System.Web.UI.HtmlControls.HtmlInputHidden hdnBufferStr;
        protected System.Web.UI.HtmlControls.HtmlInputHidden hdnTeardownTime;
        protected System.Web.UI.HtmlControls.HtmlInputHidden hdnAVParamState;
        protected System.Web.UI.HtmlControls.HtmlInputHidden hdnextusrcnt;
        protected System.Web.UI.HtmlControls.HtmlInputHidden hdnconftype;
        protected System.Web.UI.HtmlControls.HtmlInputHidden hdnGusetLoc; //ZD 101057
        protected System.Web.UI.HtmlControls.HtmlInputHidden Recur;
        protected System.Web.UI.HtmlControls.HtmlInputHidden confPassword;
        protected System.Web.UI.HtmlControls.HtmlInputHidden RecurFlag;
        protected System.Web.UI.HtmlControls.HtmlInputHidden selectedloc;
        protected System.Web.UI.WebControls.Button btnCompare;
        protected System.Web.UI.WebControls.Panel pnlNoData;

        protected System.Web.UI.WebControls.Label plblSetupDTime;
        protected System.Web.UI.WebControls.Label plblTeardownDTime;
        protected System.Web.UI.WebControls.Label lblTeardownDateTime;
        protected System.Web.UI.WebControls.Label lblSetupDateTime;
        protected System.Web.UI.WebControls.Label lblConfHeader;
        protected System.Web.UI.WebControls.Label lblConfDuration;
        //FB 2634
        //protected System.Web.UI.WebControls.TextBox SetupDate;
        //protected System.Web.UI.WebControls.TextBox TearDownDate;
        //protected MetaBuilders.WebControls.ComboBox SetupTime;
        //protected MetaBuilders.WebControls.ComboBox TeardownTime;
        //protected System.Web.UI.WebControls.TextBox SetupDateTime;
        //protected System.Web.UI.WebControls.TextBox TearDownDateTime;
        //protected System.Web.UI.WebControls.RegularExpressionValidator regTearDownStartTime;
        //protected System.Web.UI.WebControls.RegularExpressionValidator regSetupStartTime;
        protected System.Web.UI.WebControls.RequiredFieldValidator reqStartTime;
        protected System.Web.UI.WebControls.RequiredFieldValidator reqStartDate;
        protected System.Web.UI.WebControls.RequiredFieldValidator reqEndTime;
        protected System.Web.UI.WebControls.RequiredFieldValidator reqEndDate;
        protected System.Web.UI.WebControls.RegularExpressionValidator regStartTime;
        protected System.Web.UI.WebControls.RegularExpressionValidator regEndTime;
        protected System.Web.UI.WebControls.Table tblHost;
        protected System.Web.UI.WebControls.Table tblAudioServices;
        protected System.Web.UI.WebControls.Table tblSpecial;
        protected System.Web.UI.WebControls.Label lblExpHead; // FB 2570
        protected System.Web.UI.WebControls.Label errLabel;
        protected System.Web.UI.WebControls.Label lblConfID;
        protected System.Web.UI.WebControls.Label lblUpload1;
        protected System.Web.UI.WebControls.Label hdnUpload1;
        protected System.Web.UI.WebControls.Button btnRemove1;
        protected System.Web.UI.HtmlControls.HtmlInputFile FileUpload1;
        protected System.Web.UI.HtmlControls.HtmlInputFile FileUpload2;
        protected System.Web.UI.HtmlControls.HtmlInputHidden txtHasCalendar;
        protected System.Web.UI.WebControls.Label lblUpload2;
        protected System.Web.UI.WebControls.Label hdnUpload2;
        protected System.Web.UI.WebControls.Button btnRemove2;
        protected System.Web.UI.HtmlControls.HtmlInputFile FileUpload3;
        protected System.Web.UI.WebControls.Label lblUpload3;
        protected System.Web.UI.WebControls.Label hdnUpload3;
        protected System.Web.UI.WebControls.Button btnRemove3;
        protected System.Web.UI.WebControls.DropDownList lstConferenceTZ;
        protected System.Web.UI.WebControls.ListBox RoomList; //FB 2367
        protected System.Web.UI.HtmlControls.HtmlInputHidden locstrname;//FB 2367
        protected System.Web.UI.HtmlControls.HtmlGenericControl ifrmPartylist;
        protected System.Web.UI.HtmlControls.HtmlTableCell tdConfCode;
        protected System.Web.UI.HtmlControls.HtmlTableCell tdLeaderPin, tdPartyCode; //ZD 101446
        protected System.Web.UI.HtmlControls.HtmlTableCell tdlblConfcode;
        protected System.Web.UI.HtmlControls.HtmlTableCell tdlblLeaderPin, tdlblPartyCode; //ZD 101446
        protected System.Web.UI.HtmlControls.HtmlTableCell tdRegConfCode;
        protected System.Web.UI.HtmlControls.HtmlTableCell tdRegLeaderPin, tdRegPartyCode; //ZD 101446
        protected System.Web.UI.HtmlControls.HtmlTableRow trsplinst; //FB 2349
        protected System.Web.UI.HtmlControls.HtmlTableRow trMCUProfile1, trPoolOrderSelection;//FB 3063 //ZD 101931 //ZD 104256
        protected System.Web.UI.HtmlControls.HtmlTableCell tdMCUProfile, tdPoolOrder;//FB 3063 //ZD 104256
        protected System.Web.UI.HtmlControls.HtmlTableRow trConfTemp;//ZD 100570 Inncrewin
        protected String format = "MM/dd/yyyy";
        String tformat = "hh:mm tt";
        protected String client = "";
        protected String enableEntity = "";
        protected string enableaudiobridge = "0";//FB 2443

        protected String isInstanceEdit = "";
        protected String isCustomEdit = "";
        protected String enableBufferZone = "";
        protected string partysInfo;
        public string strConfStartTime;
        public string strConfEndTime;
        protected Int32 CustomSelectedLimit = 100;
        protected String timeZone = "0";

        //protected Enyim.Caching.MemcachedClient memClient = null;//ZD 103496 //ZD 104482
        //protected int memcacheEnabled = 0;//ZD 103496 //ZD 104482

        MyVRMNet.Util utilObj = null; //FB 2236
        protected ns_Logger.Logger log = null;
        private myVRMNet.NETFunctions obj = null;
        protected System.Web.UI.WebControls.CheckBoxList lstRoomSelection;
        protected System.Web.UI.WebControls.TreeView treeRoomSelection;
        protected static string selRooms;

        protected System.Web.UI.WebControls.Panel pnlLevelView;
        protected System.Web.UI.WebControls.Panel pnlListView;
        protected System.Web.UI.WebControls.RadioButtonList rdSelView;

        protected System.Web.UI.WebControls.Button btnRoomSelect;

        protected System.Web.UI.WebControls.Label lblFileList;
        protected System.Web.UI.WebControls.RadioButtonList RdListAudioPartys;
        protected System.Web.UI.WebControls.TextBox txtNoLines;
        protected System.Web.UI.WebControls.TextBox txtAudioDialNo;
        protected System.Web.UI.WebControls.TextBox txtConfCode;
        protected System.Web.UI.WebControls.TextBox txtLeaderPin, txtPartyCode; //ZD 101446
        protected System.Web.UI.HtmlControls.HtmlInputHidden hdnBridgeId;
        protected System.Web.UI.HtmlControls.HtmlInputHidden hdnAudioInsIDs;
        protected System.Web.UI.HtmlControls.HtmlInputHidden hdnHostIDs;
        protected System.Web.UI.WebControls.RadioButtonList lstAudioParty;//FB 2341(d)
        protected System.Web.UI.WebControls.Label lblConfCode;
        protected System.Web.UI.WebControls.Label lblLeaderPin;
        protected String enableConferenceCode = "0";
        protected String enableLeaderPin = "0", enablePartCode="0"; //ZD 101446
        private String strRoomName = "";
        private String strRoomIds = "";
        myVRMNet.CustomAttributes CAObj = null;
        private string custControlIDs = "";
        private bool isFormSubmit = false;
        private string audioDialString = "";
        private Hashtable usrEndpoints = null;
        private string isCOMError = "";
        int audioPartyId = 0, LOAlertShow = 0, audioAddressType = 1, audioProtocol = 1;//ZD 101971 //ZD 103574 
        protected String language = "";//FB 1830
        //FB 1716   
        protected System.Web.UI.HtmlControls.HtmlInputHidden hdnDuration;
        protected System.Web.UI.HtmlControls.HtmlInputHidden hdnChange;
        public string isEditMode = "0";
        String[] pipeDelim = { "||" }; //FB 1888
        String[] ExclamDelim = { "!!" };//FB 1888
        String[] DoublePlusDelim = { "++" };//FB 2367
        //FB 1911
        protected System.Web.UI.HtmlControls.HtmlInputHidden RecurSpec;
        protected System.Web.UI.HtmlControls.HtmlInputHidden hdnSpecRec;
        protected System.Web.UI.HtmlControls.HtmlControl divConcSupport;//FB 2341
        protected System.Web.UI.HtmlControls.HtmlControl divAudioConf;
        //protected CheckBoxList ChklstConcSupport;//FB 2377
        protected System.Web.UI.WebControls.CheckBox chkStartNow;//FB 2341
        //FB 2620 Starts
        //protected System.Web.UI.WebControls.CheckBox chkVMR;//FB 2376
        protected System.Web.UI.WebControls.DropDownList lstVMR;
        //protected System.Web.UI.HtmlControls.HtmlInputHidden isVMR;//FB 2376  
        //FB 2620 End
        protected System.Web.UI.HtmlControls.HtmlTableRow trVMR;//FB 2376
        protected System.Web.UI.WebControls.Label plblConfVMR; //FB 2376
        protected System.Web.UI.WebControls.TextBox txtintbridge;//FB 2376
        protected System.Web.UI.WebControls.TextBox txtextbridge;//FB 2376
        protected System.Web.UI.HtmlControls.HtmlInputHidden hdnintbridge;//FB 2376 
        protected System.Web.UI.HtmlControls.HtmlInputHidden hdnextbridge;//FB 2376 
        protected System.Web.UI.WebControls.DropDownList lstLineRate; //FB 2394
        protected System.Web.UI.WebControls.DropDownList lstStartMode; //FB 2501
        protected Int32 OrgLineRate = 0; //FB 2429
        protected Int32 OrgSetupTime = 0, OrgTearDownTime = 0, EnableBufferZone = 0; //FB 2398
        protected System.Web.UI.HtmlControls.HtmlTable tblAudioser; //FB 2443
        // FB 2426 Starts
        protected System.Web.UI.WebControls.Button btnGuestLocation;
        protected System.Web.UI.WebControls.Button btnGuestLocation2;
        //protected System.Web.UI.HtmlControls.HtmlButton btnGuestLocation;//ZD 100420

        protected System.Web.UI.WebControls.Image Image6;
        protected AjaxControlToolkit.ModalPopupExtender guestLocationPopup;
        protected System.Web.UI.HtmlControls.HtmlTableRow trPreviewGuestLocation;
        protected System.Web.UI.WebControls.DropDownList lstIPlinerate;
        protected System.Web.UI.WebControls.DropDownList lstSIPlinerate;
        protected System.Web.UI.WebControls.DropDownList lstISDNlinerate;
        protected System.Web.UI.WebControls.DropDownList lstIPVideoEquipment;
        protected System.Web.UI.WebControls.DropDownList lstSIPVideoEquipment;
        protected System.Web.UI.WebControls.DropDownList lstISDNVideoEquipment;
        protected System.Web.UI.WebControls.DropDownList lstIPConnectionType;
        protected System.Web.UI.WebControls.DropDownList lstSIPConnectionType;
        protected System.Web.UI.WebControls.DropDownList lstISDNConnectionType;
        protected System.Web.UI.WebControls.DropDownList lstCountries;
        protected System.Web.UI.HtmlControls.HtmlInputHidden hdnGuestRoomID;

        //ZD 100619 Starts
        protected System.Web.UI.WebControls.DataGrid dgProfiles;
        protected System.Web.UI.WebControls.TextBox txtContactPhone;
        protected System.Web.UI.WebControls.DropDownList lstGuestConnectionType;
        protected System.Web.UI.HtmlControls.HtmlInputButton btnGuestAddProfile; 

        protected System.Web.UI.WebControls.TextBox txtsiteName;
        protected System.Web.UI.WebControls.TextBox txtApprover5;
        protected System.Web.UI.WebControls.TextBox txtEmailId;
        protected System.Web.UI.WebControls.TextBox txtPhone;
        protected System.Web.UI.WebControls.TextBox txtAddress;
        protected System.Web.UI.WebControls.TextBox txtState;
        protected System.Web.UI.WebControls.TextBox txtCity;
        protected System.Web.UI.WebControls.TextBox txtZipcode;
        
        protected System.Web.UI.HtmlControls.HtmlInputHidden hdnGuestRoom;
        protected System.Web.UI.WebControls.DataGrid dgOnflyGuestRoomlist;
        protected System.Web.UI.WebControls.Button btnGuestLocationSubmit;
        protected System.Web.UI.HtmlControls.HtmlTableRow OnFlyRowGuestRoom;
        
        protected System.Web.UI.HtmlControls.HtmlInputHidden hdnGuestloc;
        protected System.Web.UI.HtmlControls.HtmlInputHidden hdnSetStartNow;//FB 1825
        DataTable onflyGrid = null;
        DataTable onflyGridProfiles = null;
        ArrayList colNames = null;
        protected System.Web.UI.WebControls.CheckBox chkPublic;//FB 2226
        protected System.Web.UI.WebControls.CheckBox chkOpenForRegistration;
        protected System.Web.UI.HtmlControls.HtmlTableRow trPublic;

        // FB 2426 Ends

        //FB 2501 Starts        
        protected System.Web.UI.WebControls.ImageButton delConfVNOC;
        protected System.Web.UI.HtmlControls.HtmlImage imgVNOC; //FB 2608
        protected System.Web.UI.HtmlControls.HtmlImage imgdeleteVNOC; //FB 2608
        protected System.Web.UI.WebControls.TextBox ConferencePassword;
        protected System.Web.UI.WebControls.TextBox ConferencePassword2;
        //protected System.Web.UI.WebControls.Panel pnlPassword1;
        //protected System.Web.UI.WebControls.Panel pnlPassword2;
        public int EnableConferencePassword = 0; //ZD 100704
        public bool isVNOC = false;

        //FB 2501 Ends
        //FB 2670 START
        protected System.Web.UI.WebControls.TextBox txtVNOCOperator;
        protected System.Web.UI.WebControls.TextBox hdnVNOCOperator;
        //FB 2670 END
        protected int EnableVNOCselection = 1;
        protected System.Web.UI.HtmlControls.HtmlInputCheckBox chkOnSiteAVSupport; //FB 2632
        protected System.Web.UI.HtmlControls.HtmlInputCheckBox chkConciergeMonitoring;
        protected System.Web.UI.HtmlControls.HtmlInputCheckBox chkMeetandGreet;
        protected System.Web.UI.HtmlControls.HtmlInputCheckBox chkDedicatedVNOCOperator;
        protected System.Web.UI.HtmlControls.HtmlTableCell tdDedicatedVNOC;

        protected System.Web.UI.WebControls.CheckBox chkFECC; // FB 2583 FECC For Express
        protected System.Web.UI.HtmlControls.HtmlTableCell tdFECC;//FB 2583 FECC For Express
        //protected System.Web.UI.HtmlControls.HtmlTableRow trFECC;//FB 2583 FECC For Express
        protected string EnableFECC = "";//FB 2543 //ZD 101931
        protected System.Web.UI.HtmlControls.HtmlTableRow trNetworkState;//FB 2595
        //FB 2634
        protected System.Web.UI.WebControls.TextBox SetupDuration;
        protected System.Web.UI.WebControls.TextBox TearDownDuration;
        protected System.Web.UI.HtmlControls.HtmlTableRow SetupRow;
        protected System.Web.UI.WebControls.Table tblCustomAttribute;   //FB 2592
        protected System.Web.UI.WebControls.Table tblMCUProfile, tblPoolOrder;//FB 2839 //ZD 104256
        protected System.Web.UI.HtmlControls.HtmlInputHidden hdnCusID; //FB 2592
        //FB 2670 START
        protected System.Web.UI.HtmlControls.HtmlTableCell tdOnSiteAVSupport;
        protected System.Web.UI.HtmlControls.HtmlTableCell tdMeetandGreet;
        protected System.Web.UI.HtmlControls.HtmlTableCell tdConciergeMonitoring;
        //protected System.Web.UI.HtmlControls.HtmlTableCell tdDedicatedVNOC;
        //FB 2670 END
        // FB 2641 Start
        protected System.Web.UI.HtmlControls.HtmlTableRow trLineRate;
        protected System.Web.UI.HtmlControls.HtmlTableRow trStartMode;
        protected int EnableLinerate = 0;
        protected int EnableStartMode = 0;
        // FB 2641 End
        //FB 2693 Starts
        protected System.Web.UI.HtmlControls.HtmlInputCheckBox chkPCConf;
        //protected System.Web.UI.HtmlControls.HtmlInputRadioButton rdBJ;//ZD 104021
        protected System.Web.UI.HtmlControls.HtmlInputRadioButton rdJB;
        protected System.Web.UI.HtmlControls.HtmlInputRadioButton rdLync;
        //protected System.Web.UI.HtmlControls.HtmlInputRadioButton rdVidtel; //ZD 102004
        protected System.Web.UI.HtmlControls.HtmlTable tblPcConf;
        //protected System.Web.UI.HtmlControls.HtmlTableCell tdBJ;//ZD 104021
        protected System.Web.UI.HtmlControls.HtmlTableCell tdJB;
        protected System.Web.UI.HtmlControls.HtmlTableCell tdLy;
        protected System.Web.UI.HtmlControls.HtmlTableCell tdVid;
        int PCVendorType = 0;
        //FB 2693 Ends
        //FB 2717 Starts
        public int isCloudEnabled = 0;
        protected System.Web.UI.WebControls.CheckBox chkCloudConferencing;
        protected System.Web.UI.HtmlControls.HtmlTableRow trCloudConferencing;
        protected System.Web.UI.HtmlControls.HtmlInputHidden hdnMcuProfileSelected, hdnMcuPoolOrder;//FB 2839//ZD 104256
        //FB 2717 End
        //FB 2659 - Starts
        protected int enableCloudInstallation = 0;
        public System.Web.UI.HtmlControls.HtmlTable tblSeatsAvailability;
        protected System.Web.UI.HtmlControls.HtmlTableRow trseatavailable;
        //FB 2659 - End
        //FB 2870 Start
        public string EnableNumericID;
        protected System.Web.UI.WebControls.CheckBox ChkEnableNumericID;
        protected System.Web.UI.WebControls.TextBox txtNumeridID;
        protected System.Web.UI.HtmlControls.HtmlTableRow TrCTNumericID;
        //FB 2870 End

        //FB 2998
        protected System.Web.UI.HtmlControls.HtmlTableRow MCUConnectDisplayRow;
        protected System.Web.UI.HtmlControls.HtmlTableCell ConnectCell;
        protected System.Web.UI.HtmlControls.HtmlTableCell DisconnectCell;
        protected System.Web.UI.WebControls.TextBox txtMCUConnect;
        protected System.Web.UI.WebControls.TextBox txtMCUDisConnect;
        protected System.Web.UI.WebControls.ImageButton imgSetup;
        protected System.Web.UI.WebControls.ImageButton imgMCUConnect;
        protected System.Web.UI.WebControls.ImageButton imgMCUDisconnect;
        protected System.Web.UI.WebControls.Label lblMCUConnect;
        public String mcuSetupDisplay = "";
        public String mcuTearDisplay = "";
        public String EnableProfileSel = "0", EnablePoolOrder = "0";//ZD 101931//ZD 104256
        public bool RMXbrid = false, PoolOrderMCU = false; //FB 3063 //ZD 104256
        //FB 2993 Starts
        protected System.Web.UI.WebControls.DropDownList drpNtwkClsfxtn;
        //public System.Web.UI.HtmlControls.HtmlInputHidden hdnNetworkSwitching;
        protected int NetworkSwitching = 0;
        //FB 2993 Ends
        //ZD 100221 Starts
        protected System.Web.UI.WebControls.CheckBox chkWebex;
        protected System.Web.UI.WebControls.TextBox txtPW1;
        protected System.Web.UI.WebControls.TextBox txtPW2;
        int EnableWebExConf;
        public System.Web.UI.HtmlControls.HtmlInputHidden hdnWebExCOnf;
        protected System.Web.UI.HtmlControls.HtmlTableRow trWebex;
        protected System.Web.UI.HtmlControls.HtmlInputHidden hdnPasschange;
        protected System.Web.UI.HtmlControls.HtmlTableRow divWebPW;
        protected System.Web.UI.HtmlControls.HtmlTableRow divWebCPW;
        //ZD 100221 Ends

        protected System.Web.UI.HtmlControls.HtmlTableRow trAdvancedSettings; //ZD 100166
        protected System.Web.UI.HtmlControls.HtmlTableRow Advancesetting; //ZD 101931    
        protected System.Web.UI.WebControls.CheckBox chkAdvancesetting; //ZD 101931    
        protected string AdvSettings = "0"; //ZD 101931  

        protected System.Web.UI.HtmlControls.HtmlTableRow trPCConf; //ZD 100166
        protected System.Web.UI.WebControls.RegularExpressionValidator regConfDisc; // ZD 100263
        //ZD 100574 - Inncrewin Starts
        protected System.Web.UI.WebControls.TextBox txtDescription_Exp;
        protected System.Web.UI.WebControls.TextBox txtHost_exp;
        protected System.Web.UI.WebControls.TextBox hdntxtHost_exp;
        protected System.Web.UI.WebControls.TextBox TxtHostMail;//ZD 101226
        protected System.Web.UI.HtmlControls.HtmlTableRow trHostRow;
        protected System.Web.UI.HtmlControls.HtmlTableRow trDescription;
        public System.Web.UI.HtmlControls.HtmlInputHidden hdnWebExPwd;
        //ZD 100574 - Inncrewin Ends

        protected System.Web.UI.HtmlControls.HtmlInputHidden hdnSaveData; //ZD 100875

        //ZD 100704 - Starts
        protected System.Web.UI.WebControls.DropDownList lstConferenceType;
        public System.Web.UI.HtmlControls.HtmlInputHidden hdnConferenceName,hdnHDConfName; //ALLDEV-807
        protected System.Web.UI.HtmlControls.HtmlTableRow trConfTitle;
        protected System.Web.UI.HtmlControls.HtmlTableRow trConfType;
        protected System.Web.UI.HtmlControls.HtmlTableRow trConcSupport;
        protected System.Web.UI.HtmlControls.HtmlTableRow trConfDesc;
        protected System.Web.UI.HtmlControls.HtmlTableRow trConfPass;
        protected System.Web.UI.HtmlControls.HtmlTableRow trConfPass1;


        protected int P2PEnable = 0;
        protected int EnableRoomConfType = 0;
        protected int EnableHotdeskingConfType = 0;
        protected int EnableAudioVideoConfType = 0;
        protected int EnableAudioOnlyConfType = 0;
        protected int EnableExpressConfType = 0;
        protected int EnablePublicConf = 0;
        protected int EnableAudioBridges = 0;
        protected int GuestRooms = 0;
        protected int isSpecialRecur = 0;
        protected int EnableImmConf = 0;
        protected int EnablePCUser = 0;
        //ZD 100704 - End
		int EnableWebExIntg;//ZD 100935
        public System.Web.UI.HtmlControls.HtmlInputHidden hdnEnableWebExIngt; //ZD 100935
        //ZD 100890 Start
        protected System.Web.UI.HtmlControls.HtmlTableRow trStatic;
        protected System.Web.UI.WebControls.CheckBox chkStatic;
        protected System.Web.UI.WebControls.TextBox txtStatic;
        protected System.Web.UI.HtmlControls.HtmlGenericControl divStaticID;
        //ZD 100890 End
        protected System.Web.UI.HtmlControls.HtmlInputHidden hdnHostWebEx, hdnSchdWebEx; //ZD 101015

        //ZD 100834 Starts
        protected System.Web.UI.HtmlControls.HtmlInputHidden isPCCOnf;
        protected System.Web.UI.HtmlControls.HtmlTableRow trExternalUsers;
        protected System.Web.UI.WebControls.DataGrid dgUsers;
        protected System.Web.UI.WebControls.Label lblNoUsers;
        protected int enableDetailedExpressForm = 0;
        protected System.Web.UI.HtmlControls.HtmlTableRow trCheckOptions;
        protected System.Web.UI.HtmlControls.HtmlTableRow trchkDetails;
        //ZD 100834 End
        //ZD 100619 Starts
        bool isExternalUsrAvail = true;
        protected System.Web.UI.WebControls.TextBox hdnApprover5;
        protected System.Web.UI.WebControls.TextBox hdnGusetAdmin;
        protected System.Web.UI.HtmlControls.HtmlInputHidden hdnROWID;
        protected CustomValidator cvSubmit;
        protected int bridgeType;
        List<int> GuestLocMCU = null;
        int GLMCU = 0; //ZD 100619
		protected System.Web.UI.UpdatePanel updtguest;
        protected System.Web.UI.UpdatePanel updtGuestLocation;//ZD100619
        //ZD 100619 Ends
		//ZD 101229 exchange round trip starts
        protected System.Web.UI.HtmlControls.HtmlInputHidden hdnicalID;
        protected System.Web.UI.HtmlControls.HtmlInputHidden hdnconfOriginID;
        //ZD 101229 exchange round trip ends
 		//ZD 101120 Starts
        protected System.Web.UI.HtmlControls.HtmlInputHidden hdnGLWarningPopUp;
        protected System.Web.UI.HtmlControls.HtmlInputHidden hdnGLApprTime;
        protected System.Web.UI.HtmlControls.HtmlInputHidden hdnGLConfTime;
        protected System.Web.UI.HtmlControls.HtmlInputHidden hdnGLCount;
        int GLWaringPopUp = 0, GLApprlTime = 0;
        //ZD 101120 Ends
		protected System.Web.UI.HtmlControls.HtmlGenericControl divSubModalPopup;//ZD 101226
        protected System.Web.UI.HtmlControls.HtmlInputHidden hdnAudioId; // ZD 101253
 		//ZD 100522 Starts
        protected System.Web.UI.HtmlControls.HtmlInputHidden hdnROOMPIN;
        protected System.Web.UI.HtmlControls.HtmlInputHidden hdnPINlen;
        protected System.Web.UI.HtmlControls.HtmlTable divChangeVMRPIN;
        protected System.Web.UI.HtmlControls.HtmlButton btnChngPIN;
        protected System.Web.UI.HtmlControls.HtmlTableRow trVMRPIN1;
        protected System.Web.UI.HtmlControls.HtmlTableRow trVMRPIN2;
        protected System.Web.UI.WebControls.TextBox txtVMRPIN1;
        protected System.Web.UI.WebControls.TextBox txtVMRPIN2;
        public string VMRBridge = "0";
        protected int VMRPINChange = 0, PINlength = 0;
        protected System.Web.UI.WebControls.RegularExpressionValidator numPassword1;
        protected System.Web.UI.WebControls.RegularExpressionValidator RegVMRPIN;
        //ZD 100522 Ends

        //ZD 101233 START
        public System.Web.UI.HtmlControls.HtmlInputHidden hdnCrossrecurEnable;
        public System.Web.UI.HtmlControls.HtmlInputHidden hdnCrossdynInvite;
        public System.Web.UI.HtmlControls.HtmlInputHidden hdnCrossroomModule;
        public System.Web.UI.HtmlControls.HtmlInputHidden hdnCrossfoodModule;
        public System.Web.UI.HtmlControls.HtmlInputHidden hdnCrosshkModule;
        public System.Web.UI.HtmlControls.HtmlInputHidden hdnCrossisVIP;
        public System.Web.UI.HtmlControls.HtmlInputHidden hdnCrossEnableRoomServiceType;
        public System.Web.UI.HtmlControls.HtmlInputHidden hdnCrossisSpecialRecur;
        public System.Web.UI.HtmlControls.HtmlInputHidden hdnCrossConferenceCode;
        public System.Web.UI.HtmlControls.HtmlInputHidden hdnCrossLeaderPin;
        public System.Web.UI.HtmlControls.HtmlInputHidden hdnCrossAdvAvParams, hdnCrossPartyCode; //ZD 101446
        public System.Web.UI.HtmlControls.HtmlInputHidden hdnCrossEnableBufferZone;
        public System.Web.UI.HtmlControls.HtmlInputHidden hdnCrossEnableEntity;
        public System.Web.UI.HtmlControls.HtmlInputHidden hdnCrossAudioParams;
        public System.Web.UI.HtmlControls.HtmlInputHidden hdnCrossdefaultPublic;
        public System.Web.UI.HtmlControls.HtmlInputHidden hdnCrossP2PEnable;
        public System.Web.UI.HtmlControls.HtmlInputHidden hdnCrossEnableRoomConfType;
        public System.Web.UI.HtmlControls.HtmlInputHidden hdnCrossEnableHotdeskingConfType;
        public System.Web.UI.HtmlControls.HtmlInputHidden hdnCrossEnableAudioVideoConfType;
        public System.Web.UI.HtmlControls.HtmlInputHidden hdnCrossDefaultConferenceType;
        public System.Web.UI.HtmlControls.HtmlInputHidden hdnCrossEnableAudioOnlyConfType;
        public System.Web.UI.HtmlControls.HtmlInputHidden hdnCrossenableAV;
        public System.Web.UI.HtmlControls.HtmlInputHidden hdnCrossenableParticipants;
        public System.Web.UI.HtmlControls.HtmlInputHidden hdnCrossisMultiLingual;
        public System.Web.UI.HtmlControls.HtmlInputHidden hdnCrossroomExpandLevel;
        public System.Web.UI.HtmlControls.HtmlInputHidden hdnCrossEnableImmConf;
        public System.Web.UI.HtmlControls.HtmlInputHidden hdnCrossEnableAudioBridges;
        public System.Web.UI.HtmlControls.HtmlInputHidden hdnCrossDedicatedVideo;
        public System.Web.UI.HtmlControls.HtmlInputHidden hdnCrossAddtoGroup;
        public System.Web.UI.HtmlControls.HtmlInputHidden hdnCrossEnablePublicConf;
        public System.Web.UI.HtmlControls.HtmlInputHidden hdnCrossEnableConfPassword;
        public System.Web.UI.HtmlControls.HtmlInputHidden hdnCrossEnableRoomParam;
        public System.Web.UI.HtmlControls.HtmlInputHidden hdnCrossEnableSurvey;
        public System.Web.UI.HtmlControls.HtmlInputHidden hdnCrossSetupTime; 
        public System.Web.UI.HtmlControls.HtmlInputHidden hdnCrossTearDownTime; 
        public System.Web.UI.HtmlControls.HtmlInputHidden hdnCrossMeetGreetBufferTime; 
        public System.Web.UI.HtmlControls.HtmlInputHidden hdnCrossEnableLinerate;
        public System.Web.UI.HtmlControls.HtmlInputHidden hdnCrossEnableStartMode;
        protected System.Web.UI.HtmlControls.HtmlInputHidden hdnTxtMsg;
        protected System.Web.UI.HtmlControls.HtmlInputHidden hdnCrossEnableSmartP2P;
        public System.Web.UI.HtmlControls.HtmlInputHidden hdnEnableNumericID;
        public System.Web.UI.HtmlControls.HtmlInputHidden hdnEnableProfileSelection, hdnEnablePoolOrderSelection;//ZD 104256
        public System.Web.UI.HtmlControls.HtmlInputHidden hdnMCUSetupTime;
        public System.Web.UI.HtmlControls.HtmlInputHidden hdnMCUTeardownTime;
        public System.Web.UI.HtmlControls.HtmlInputHidden hdnMCUConnectDisplay;
        public System.Web.UI.HtmlControls.HtmlInputHidden hdnMCUDisconnectDisplay;
        public System.Web.UI.HtmlControls.HtmlInputHidden hdnNetworkSwitching;
        protected System.Web.UI.HtmlControls.HtmlInputHidden hdnVMRPINChange;
        public System.Web.UI.HtmlControls.HtmlInputHidden hdnEnableFECC;//ZD 101931

        
        public string roomExpand;
        public string P2PEnableOrgOption;
        public string EnableRoomConfTypeOrgOption;
        public string EnableHotdeskingConfTypeOrgOption;
        public string DefaultConferenceType;
        public string EnableAudioVideoConfTypeOrgOption;
        public string EnableAudioOnlyConfTypeOrgOption;
        public string EnableIsVip;
        public string EnableServiceType;
        public string foodModule;
        public string hkModule;
        public string roomModule;
        public string isMulti;
        public string EnablePublicConference;
        public string Enablerecurrence;
        public string Enableopenforregistration;
        public string EnableConferencePasswordOrgOption;
        public string EnableSurvey;
        protected String defaultPublic = "";
        protected String EnableRoomParam = "0";
        protected int OrgMeetGrettBuffer = 0; 
        //ZD 101233 END
        
        //ZD 101815 Starts
        protected System.Web.UI.HtmlControls.HtmlInputHidden isIPAddress;
        protected System.Web.UI.HtmlControls.HtmlInputHidden isTestedModel;
        protected System.Web.UI.HtmlControls.HtmlInputHidden isP2PGL;
        protected System.Web.UI.HtmlControls.HtmlInputHidden hdnSmartP2PTotalEps;
        protected System.Web.UI.HtmlControls.HtmlInputHidden hdnSmartP2PNotify;  
        protected System.Web.UI.HtmlControls.HtmlInputHidden isP2PConfirm; 
        protected System.Web.UI.HtmlControls.HtmlInputHidden hdnisEditMode; 
        int usrCnt = 0, EnableSmartP2P = 0;
        protected int enablePar = 0; //ZD 101388
        //ZD 101815 Ends
        protected System.Web.UI.HtmlControls.HtmlInputHidden hnDetailedExpForm;
        int isDetaileDExpressForm = 0;
		protected System.Web.UI.HtmlControls.HtmlGenericControl topExpDiv;
        //ZD 100513 Starts
        protected System.Web.UI.HtmlControls.HtmlInputHidden hdnEnablEOBTP;
        protected System.Web.UI.HtmlControls.HtmlInputHidden hdnWebExLaunch;
        int EnableOBTP = 0, WebExLaunchMode = 0;
        string ConerenceType = "2";
        //ZD 100513 Ends
		//ZD 101490
        protected System.Web.UI.HtmlControls.HtmlInputHidden hdnEnableExpressConfType;
        protected int EnableTemplateBooking = 0;//ZD 101562
        protected System.Web.UI.HtmlControls.HtmlInputHidden hdntemplatebooking;//ZD 101562
        protected System.Web.UI.WebControls.ImageButton imgTDBConferencenote; //ZD 101720
		//ZD 101755 start
        protected System.Web.UI.HtmlControls.HtmlTableRow TearDownRow;
        protected System.Web.UI.WebControls.ImageButton imgTear;
        protected System.Web.UI.HtmlControls.HtmlInputHidden hdnSetuptimeDisplay;
        protected System.Web.UI.HtmlControls.HtmlInputHidden hdnTeardowntimeDisplay;
        protected int EnableSetupTimeDisplay = 0, EnableTeardownTimeDisplay = 0;
        //ZD 101755 End
        //ZD 101869 Starts
        protected System.Web.UI.WebControls.TextBox ImagesPath; 
        protected System.Web.UI.WebControls.TextBox ImageFiles;
        protected System.Web.UI.WebControls.TextBox ImageFilesBT;
        protected System.Web.UI.WebControls.TextBox txtSelectedImage;
        protected System.Web.UI.HtmlControls.HtmlInputHidden hdnFamilyLayout;
        //ZD 101869 End
        //ZD 101931 start
        protected System.Web.UI.HtmlControls.HtmlInputHidden hdnShowVideoLayout;
        protected int ShowVideoLayout = 0;
        protected System.Web.UI.HtmlControls.HtmlTableCell tdVideoDisplay;
        protected System.Web.UI.HtmlControls.HtmlTableRow trLayout;//ZD 104256
        protected System.Web.UI.HtmlControls.HtmlTableCell tdimgVideoDisplay;
        //ZD 101931 End

        //ZD 102330 start //ZD 102330 start
        protected System.Web.UI.HtmlControls.HtmlInputText txtFileUpload1;
        protected System.Web.UI.HtmlControls.HtmlInputText txtFileUpload2;
        protected System.Web.UI.HtmlControls.HtmlInputText txtFileUpload3;
        //ZD 102330 End
        protected int isStaticIDEnabled = 0; //ZD 102585
		protected System.Web.UI.HtmlControls.HtmlInputHidden hdnIsRecurConference;//ZD 102963
        protected System.Web.UI.HtmlControls.HtmlInputHidden hdnSelEntityCodeVal; //ZD 102909
		protected System.Web.UI.HtmlControls.HtmlInputHidden hdnRemoveEntityVal; //ZD 103265
		protected System.Web.UI.HtmlControls.HtmlInputHidden hdnChgCallerCallee; //ZD 103402
        protected System.Web.UI.HtmlControls.HtmlInputHidden hdnisBJNConf; //ZD 103263
        bool isClone = false; //ZD 103830
        //ZD 103624
        protected System.Web.UI.WebControls.RegularExpressionValidator regStartDate;
        protected System.Web.UI.WebControls.RegularExpressionValidator regEndDate;
        //ZD 103550 - Start        
        public string EnableBJNSelectOption;
        public string EnableBJNMeetingID;
        public string EnableBJNIntegration;
        public string EnableBlueJeans;
        protected System.Web.UI.WebControls.CheckBox chkBJNMeetingID;        
        protected System.Web.UI.HtmlControls.HtmlInputHidden hdnCrossBJNSelectOption;        
        protected System.Web.UI.HtmlControls.HtmlInputHidden hdnCrossBJNIntegration;
        protected System.Web.UI.HtmlControls.HtmlInputHidden hdnCrossEnableBlueJeans;
        protected System.Web.UI.HtmlControls.HtmlTableRow trBJNMeeting;
        protected System.Web.UI.WebControls.DropDownList DrpBJNMeetingID;
        //ZD 103550 - End
        public string EnableBJNDisplay;//ZD 104116
        protected System.Web.UI.HtmlControls.HtmlInputHidden hdnCrossBJNDisplay; //ZD 104116
        protected System.Web.UI.HtmlControls.HtmlInputHidden hdnDialString; //ZD 104289

        protected Boolean isAVSettingsSelected = false; //ZD 104357
		protected System.Web.UI.HtmlControls.HtmlInputHidden hdnSelectedAudioDetails; //ALLDEV-814
        protected System.Web.UI.HtmlControls.HtmlInputHidden hasVisited; //ALLDEV-814        
        protected System.Web.UI.WebControls.CheckBox chkRecurrence;//ALLDEV-814
        protected System.Web.UI.UpdatePanel UpdatePanel3;
        //ALLDEV-826 Starts
        protected System.Web.UI.HtmlControls.HtmlTableRow trHostPwd;
        protected System.Web.UI.HtmlControls.HtmlTableRow trCfmHostPwd;
        protected System.Web.UI.WebControls.Literal litNumericOrGuestPwd;
        protected System.Web.UI.WebControls.TextBox txtHostPwd;
        protected System.Web.UI.WebControls.TextBox txtCfmHostPwd;        
        protected System.Web.UI.WebControls.RegularExpressionValidator regValHostPwd;
        protected System.Web.UI.HtmlControls.HtmlInputHidden hdnHostPassword;
        public System.Web.UI.HtmlControls.HtmlInputHidden hdnCrossEnableHostGuestPwd;
        public string EnableHostGuestPwdOrgOption;
        public int EnableHostGuestPwd = 0;        
        string xhostpassword = "";
        //ALLDEV-826 Ends        
        string xconfpassword = "";
		//ALLDEV-833 - Start
        protected System.Web.UI.HtmlControls.HtmlInputHidden hdngetScreenWidth;
        protected System.Web.UI.HtmlControls.HtmlTableCell tdExchangeLookup;
        protected System.Web.UI.HtmlControls.HtmlTableCell tdSchedulingAssistant;
        protected System.Web.UI.HtmlControls.HtmlButton btnExchangeLookup;
        protected System.Web.UI.WebControls.Button btnSchedulingAssistant;
        protected System.Web.UI.UpdatePanel UpdateSchedulingAssistant;
        //ALLDEV-833 - End
		//ALLDEV-782 Starts
        public string EnablePexipSelectOption, EnablePexipMeetingID, EnablePexipIntegration, EnablePexipDisplay;
        protected System.Web.UI.WebControls.CheckBox chkPexipMtgType;
        protected System.Web.UI.HtmlControls.HtmlInputHidden hdnCrossPexipSelectOption, hdnCrossPexipDisplay;
        protected System.Web.UI.HtmlControls.HtmlInputHidden hdnCrossPexipIntegration, hdnChangePexipVal;
        //protected System.Web.UI.HtmlControls.HtmlInputHidden hdnCrossEnablePexip;
        protected System.Web.UI.HtmlControls.HtmlTableRow trPexipMeeting;
        protected System.Web.UI.WebControls.DropDownList drpPexipMtgType;
        protected System.Web.UI.HtmlControls.HtmlInputHidden hdnisPexipConf;
        //ALLDEV-782 Ends
        
        #endregion

        #region Public Constructor

        public ExpressConference()
        {
            utilObj = new MyVRMNet.Util(); //FB 2236
            log = new ns_Logger.Logger();
            obj = new myVRMNet.NETFunctions();
            selRooms = ", ";            
        }
        #endregion

        //ZD 101022
        #region InitializeCulture
        protected override void InitializeCulture()
        {
            if (Session["UserCulture"] != null)
            {
                UICulture = Session["UserCulture"].ToString();
                Culture = Session["UserCulture"].ToString();
                //ZD 101714
                if (Session["UserCulture"].ToString() == "fr-CA")
                    Culture = "en-US";
                base.InitializeCulture();
            }
        }
        #endregion

        #region Page Load

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                hdnSaveData.Value = ""; //ZD 100875

                // ZD 100263 Starts
                if (obj == null)
                    obj = new myVRMNet.NETFunctions();
                obj.AccessandURLConformityCheck("expressconference.aspx", Request.Url.AbsoluteUri.ToLower()); // ZD 100263
                Session.Remove("Confbridge"); //ZD 100298
                //ZD 100663
                MyVRMNet.LoginManagement loginMgmt = null;
                string userdetails = "";
                StringBuilder a = new StringBuilder();
                if (Request.QueryString["tp"] != null)
                {
                    if (Request.QueryString["tp"] == "CR")
                    {
                        loginMgmt = new MyVRMNet.LoginManagement();

                        if (Request.QueryString["req"] != null)
                            userdetails = Request.QueryString["req"];

                        loginMgmt.simpleDecrypt(ref userdetails);
                        string[] id = userdetails.Split(',');

                        loginMgmt.queyStraVal = id[0].ToString();
                        loginMgmt.qStrPVal = id[1].ToString();

                        String url = loginMgmt.GetHomeCommand();
                        if (Request.QueryString["id"] != null)
                        {
                            if (Request.QueryString["id"] != "")
                            {
                                String confID = Request.QueryString["id"].ToString();
                                loginMgmt.simpleDecrypt(ref confID);
                                Session.Add("confid", confID);
                                Session.Add("confTempID", confID);
                            }
                        }
                    }
                } //ZD 101942 Starts
                else if (Request.QueryString["id"] != null)
                {
                    if (Request.QueryString["id"] != "")
                    {
                        String confID = Request.QueryString["id"].ToString();
                        Session.Add("confid", confID);
                        Session.Add("confTempID", confID);
                    }
                }
                //ZD 101942 Ends

                if (Session["roomCascadingControl"] != null && Session["roomCascadingControl"].ToString().Equals("1") && !IsPostBack) //ZD 101388
                {
                    if (Request.QueryString["t"] == null)
                    {
                        log.Trace("Page: ExpressConference.aspx.cs, Function: Page_Load, Reason: Querystring t is null");
                        Response.Redirect("ShowError.aspx");
                    }
                    regConfDisc.Enabled = true;
                }

                // ZD 100263 Ends
                //ZD 100335 start
                if (Request.Cookies["hdnscreenres"] != null)
                    Session["hdnscreenresoul"] = Request.Cookies["hdnscreenres"].Value;
                //ZD 100335 End
                if (Session["language"] == null)//FB 1830
                    Session["language"] = "en";
                if (Session["language"].ToString() != "")
                    language = Session["language"].ToString();

                //ZD 101388 Starts
                if (Session["enableParticipants"] != null)
                {
                    if (!Session["enableParticipants"].ToString().Equals(""))
                        int.TryParse(Session["enableParticipants"].ToString(), out enablePar);
                }
                //ZD 101388 End

                // FB 2570 Starts
                if (Request.QueryString["t"] != null)
                {
                    if (Request.QueryString["t"].ToString() == "")//FB 1830 Email Edit
                    {
                        //isEditMode = "1";
                        hdnisEditMode.Value = "1";
                        lblExpHead.Text = obj.GetTranslatedText("Edit Conference"); //ZD 100288
                    }
                    else
                    {
                        //isEditMode = "0";
                        hdnisEditMode.Value = "0";
                        lblExpHead.Text = obj.GetTranslatedText("Express New Conference"); //ZD 100288
                    }
                }
                if (hdnisEditMode == null && hdnisEditMode.Value == "") //ZD 100815
                    isEditMode = "0";
                else
                    isEditMode = hdnisEditMode.Value;
                // FB 2570 Ends
                if (Application["roomExpandLevel"] == null)
                    Application.Add("roomExpandLevel", "1");

                if (Application["Client"] == null)//FB 2341
                    Application["Client"] = "";

                client = Application["Client"].ToString();
                //if (client.ToUpper() == "DISNEY") //FB 2359
                //{
                //    divConcSupport.Attributes.Add("style", "display:block");//FB 2341
                //    divAudioConf.Attributes.Add("style", "display:block");
                //}
                //else
                //{
                //    divConcSupport.Attributes.Add("style", "display:none");//FB 2341
                //    divAudioConf.Attributes.Add("style", "display:none");
                //}



                //ZD 100704 Starts
                ////FB 2501
                //if (lstConferenceType.SelectedValue.Equals("4") || lstConferenceType.SelectedValue.Equals("7") || lstVMR.SelectedIndex > 0 || lstConferenceType.SelectedValue.Equals("8")) //FB 2694 //FB 2620
                //{
                //    trStartMode.Attributes.Add("style", "display:None");//FB 2641 
                //}
                //else
                //{
                //    //FB 2641 start
                //    if (EnableStartMode == 1)
                //    {
                //        trStartMode.Attributes.Add("style", "display:");
                //    }
                //    else
                //    {
                //        trStartMode.Attributes.Add("style", "display:None");
                //    }
                //    //FB 2641 End

                //}

                //ZD 101233 START
                CrossSilo();
                CrossSiloSession();
                //ZD 101233 END

                //ZD 103496 //commented for ZD 104482
                //if (Session["MemcacheEnabled"] != null && !string.IsNullOrEmpty(Session["MemcacheEnabled"].ToString()))
                //    int.TryParse(Session["MemcacheEnabled"].ToString(), out memcacheEnabled);

                if (Session["EnableImmConf"] != null && !string.IsNullOrEmpty(Session["EnableImmConf"].ToString()))
                    int.TryParse(Session["EnableImmConf"].ToString(), out EnableImmConf);

                if (Session["isSpecialRecur"] != null && !string.IsNullOrEmpty(Session["isSpecialRecur"].ToString()))
                    int.TryParse(Session["isSpecialRecur"].ToString(), out isSpecialRecur);

                if (Session["EnableCloudInstallation"] != null && !string.IsNullOrEmpty(Session["EnableCloudInstallation"].ToString()))
                    int.TryParse(Session["EnableCloudInstallation"].ToString(), out enableCloudInstallation);

                if (Session["EnableExpressConfType"] != null && !string.IsNullOrEmpty(Session["EnableExpressConfType"].ToString()))
                    int.TryParse(Session["EnableExpressConfType"].ToString(), out EnableExpressConfType);

                if (Session["GuestRooms"] != null && !string.IsNullOrEmpty(Session["GuestRooms"].ToString()))
                    int.TryParse(Session["GuestRooms"].ToString(), out GuestRooms);

                //ZD 100815 Starts
                if (Session["SmartP2PNotify"] != null && !string.IsNullOrEmpty(Session["SmartP2PNotify"].ToString())) 
                    hdnSmartP2PNotify.Value = Session["SmartP2PNotify"].ToString();
                if (Session["EnableSmartP2P"] != null && !string.IsNullOrEmpty(Session["EnableSmartP2P"].ToString()))
                    int.TryParse(Session["EnableSmartP2P"].ToString(), out EnableSmartP2P);
                //ZD 100815 Ends
                 
                if (Session["VMRPINChange"] != null) //ZD 100522
                    int.TryParse(Session["VMRPINChange"].ToString(), out VMRPINChange);
                hdnPINlen.Value = Session["PasswordCharLength"].ToString();
                if (EnableExpressConfType <= 0 || enableCloudInstallation == 1)
                {
                    trConfType.Attributes.Add("style", "display:none");
                    lstConferenceType.ClearSelection();
                    lstConferenceType.SelectedValue = ns_MyVRMNet.vrmConfType.AudioVideo;
                    hdnconftype.Value = ns_MyVRMNet.vrmConfType.AudioVideo;
                }
                else
                {
                    trConfType.Attributes.Add("style", "display:");
                }
                //ZD 100704 End

                //FB 2694 Starts
                trConfDesc.Visible = true;
                trConfTitle.Visible = true;
                //trConfTemp.Visible = true; //ZD 100522
                //FB 2694 Ends

                //ZD 101120 Starts
                hdnGLWarningPopUp.Value = Session["EnableGuestLocWarningMsg"].ToString();
                hdnGLApprTime.Value = Session["GuestLocApprovalTime"].ToString();
                int.TryParse(Session["EnableGuestLocWarningMsg"].ToString(), out GLWaringPopUp);
                //ZD 101120 Ends

                // FB 2608 Start
                if (Session["EnableVNOCselection"] != null)
                {
                    if (Session["EnableVNOCselection"].ToString() == "0" || Session["EnableDedicatedVNOC"].ToString() == "0")//FB 2670)
                    {
                        EnableVNOCselection = 0;
                        txtVNOCOperator.Visible = false;//FB 2670
                        imgVNOC.Visible = false;
                        imgdeleteVNOC.Visible = false;
                        tdDedicatedVNOC.Visible = false;
                    }
                    else
                    {
                        EnableVNOCselection = 1;
                        txtVNOCOperator.Visible = true;//FB 2670
                        imgVNOC.Visible = true;
                        imgdeleteVNOC.Visible = true;
                        tdDedicatedVNOC.Visible = true;
                    }
                }
                // FB 2608 End

                if (Session["FormatDateType"] != null)
                {
                    if (Session["FormatDateType"].ToString() != "")
                        format = Session["FormatDateType"].ToString();
                }

                strConfStartTime = confStartTime.Text;
                strConfEndTime = confEndTime.Text;
                if (Session["timeZoneDisplay"] != null)
                {
                    if (Session["timeZoneDisplay"].ToString() != "")
                        timeZone = Session["timeZoneDisplay"].ToString();
                }

                if (Application["EnableBufferZone"] == null)
                {
                    Application["EnableBufferZone"] = "1";
                }

                if (Application["EnableAudioAddOn"] == null)
                {
                    Application["EnableAudioAddOn"] = "1";
                }

                if (Application["EnableEntity"] == null)
                    Application["EnableEntity"] = "1";

                enableEntity = Application["EnableEntity"].ToString();
                enableBufferZone = Application["EnableBufferZone"].ToString();

                if (Session["timeZoneDisplay"] != null)
                {
                    if (Session["timeZoneDisplay"].ToString() != "")
                        timeZone = Session["timeZoneDisplay"].ToString();
                }

                //ZD 104163 - Start
                string Dateformat = "MM/DD/YYYY";
                if (format == "dd/MM/yyyy")
                    Dateformat = "DD/MM/YYYY";
                //ZD 103624
                regEndDate.ErrorMessage = obj.GetTranslatedText("Invalid Date") + " (" + Dateformat + ")";
                regStartDate.ErrorMessage = obj.GetTranslatedText("Invalid Date") + " (" + Dateformat + ")";
                //ZD 104163 - End

                Session["timeFormat"] = ((Session["timeFormat"] == null) ? "1" : Session["timeFormat"]);
                Session["FormatDateType"] = ((Session["FormatDateType"] == null) ? "1" : Session["FormatDateType"]);
                Session["timeZoneDisplay"] = ((Session["timeZoneDisplay"] == null) ? "1" : Session["timeZoneDisplay"]);
                Application["interval"] = ((Application["interval"] == null) ? "30" : "30"); //Providea
                tformat = (Session["timeFormat"].ToString().Equals("0") ? "HH:mm" : "hh:mm tt");

                //FB 2588 Starts
                if (Session["timeFormat"].ToString().Equals("2"))
                    tformat = "HHmmZ";

                if (Session["ConferenceCode"] != null)
                    enableConferenceCode = Session["ConferenceCode"].ToString();
                if (Session["LeaderPin"] != null)
                    enableLeaderPin = Session["LeaderPin"].ToString();
                if (Session["EnablePartyCode"] != null) //ZD101446
                    enablePartCode = Session["EnablePartyCode"].ToString();

                //Modified for FB 1982 - Start
                if (enableConferenceCode == "0")
                {
                    tdlblConfcode.Attributes.Add("style", "display:none");
                    tdConfCode.Attributes.Add("style", "display:none");
                    tdRegConfCode.Attributes.Add("style", "display:none");
                }

                if (enableLeaderPin == "0")
                {
                    tdlblLeaderPin.Attributes.Add("style", "display:none");
                    tdLeaderPin.Attributes.Add("style", "display:none");
                    tdRegLeaderPin.Attributes.Add("style", "display:none");
                }
                //FB 2443 - Start
                if (enablePartCode == "0") //ZD 101446
                {
                    tdlblPartyCode.Attributes.Add("style", "display:none");
                    tdPartyCode.Attributes.Add("style", "display:none");
                    tdRegPartyCode.Attributes.Add("style", "display:none");
                }

                //ZD 100704 Starts //ZD 101931
                if (hdnCrossEnableAudioBridges != null && hdnCrossEnableAudioBridges.Value != "")
                    int.TryParse(hdnCrossEnableAudioBridges.Value, out EnableAudioBridges);
                else
                {
                    if (Session["EnableAudioBridges"] != null && !string.IsNullOrEmpty(Session["EnableAudioBridges"].ToString()))
                    {
                        int.TryParse(Session["EnableAudioBridges"].ToString(), out EnableAudioBridges);
                    }
                }
                if (EnableAudioBridges <= 0)
                    tblAudioser.Visible = false;
                else
                    tblAudioser.Visible = true;
                //ZD 100704 End
                //FB 2443 - End
                //Modified for FB 1982 - End

                //if (lstAudioParty.SelectedValue.Equals("-1"))//FB 2341
                //{
                //    txtAudioDialNo.Enabled = false;
                //    txtConfCode.Enabled = false;
                //    txtLeaderPin.Enabled = false;
                //}

                if (Session["EnableBufferZone"] != null) //FB 2398
                    if (Session["EnableBufferZone"].ToString() != "")
                        Int32.TryParse(Session["EnableBufferZone"].ToString(), out EnableBufferZone);

                //FB 2998 - Start //ZD 101931
                if (hdnMCUConnectDisplay != null && hdnMCUConnectDisplay.Value != "")
                    mcuSetupDisplay = hdnMCUConnectDisplay.Value;
                else if (Session["MCUSetupDisplay"] != null)
                    mcuSetupDisplay = Session["MCUSetupDisplay"].ToString();

                if (hdnMCUDisconnectDisplay != null && hdnMCUDisconnectDisplay.Value != "")
                    mcuTearDisplay = hdnMCUDisconnectDisplay.Value;
                else if (Session["MCUTearDisplay"] != null)
                    mcuTearDisplay = Session["MCUTearDisplay"].ToString();

                if ((mcuSetupDisplay == "1" || mcuTearDisplay == "1")) //ZD 100085
                {
                    MCUConnectDisplayRow.Style.Add("display", "block");

                    if (mcuSetupDisplay == "1" && mcuTearDisplay == "1")
                        lblMCUConnect.Text = obj.GetTranslatedText("MCU Connect / Disconnect");
                    else if (mcuSetupDisplay == "1")
                        lblMCUConnect.Text = obj.GetTranslatedText("MCU Connect");
                    else if (mcuTearDisplay == "1")
                        lblMCUConnect.Text = obj.GetTranslatedText("MCU Disconnect");
                }
                else//ZD 100085 Starts
                {
                    MCUConnectDisplayRow.Style.Add("display", "none");

                }//ZD 100085 End
                //FB 2998 - End

                if (Session["OrgSetupTime"] != null) //FB 2398
                    if (Session["OrgSetupTime"].ToString() != "")
                        Int32.TryParse(Session["OrgSetupTime"].ToString(), out OrgSetupTime);

                if (Session["OrgTearDownTime"] != null)//FB 2398
                    if (Session["OrgTearDownTime"].ToString() != "")
                        Int32.TryParse(Session["OrgTearDownTime"].ToString(), out OrgTearDownTime);

                if (Session["OrgLineRate"] != null)//FB 2429
                {
                    if (!Session["OrgLineRate"].ToString().Equals(""))
                        Int32.TryParse(Session["OrgLineRate"].ToString(), out OrgLineRate);
                }

                //FB 2693 Starts //ZD 104021
                //if (Session["EnableBlueJeans"] != null)
                //{
                //    if (Session["EnableBlueJeans"].ToString().Equals("0"))
                //        tdBJ.Attributes.Add("style", "display:none");
                //}
                if (Session["EnableJabber"] != null)
                {
                    if (Session["EnableJabber"].ToString().Equals("0"))
                        tdJB.Attributes.Add("style", "display:none");
                }
                if (Session["EnableLync"] != null)
                {
                    if (Session["EnableLync"].ToString().Equals("0"))
                        tdLy.Attributes.Add("style", "display:none");
                }
                if (Session["EnableVidtel"] != null)
                {
                    if (Session["EnableVidtel"].ToString().Equals("0"))
                        tdVid.Attributes.Add("style", "display:none");
                }
                //FB 2693 Ends
                //ZD 100935 Starts
                if (Session["EnableWebExIntg"] != null)
                    int.TryParse(Session["EnableWebExIntg"].ToString(), out EnableWebExIntg);
                hdnEnableWebExIngt.Value = EnableWebExIntg.ToString(); //ZD 101015

                //ZD 101931
                if (hdnEnableProfileSelection != null && hdnEnableProfileSelection.Value != "")
                    EnableProfileSel = hdnEnableProfileSelection.Value;
                else if (Session["EnableProfileSelection"] != null)
                    EnableProfileSel = Session["EnableProfileSelection"].ToString();

                if (hdnEnablePoolOrderSelection != null && hdnEnablePoolOrderSelection.Value != "") //ZD 104256
                    EnablePoolOrder = hdnEnablePoolOrderSelection.Value;
                else if (Session["EnablePoolOrderSelection"] != null)
                    EnablePoolOrder = Session["EnablePoolOrderSelection"].ToString();

                if (hdnEnableFECC != null && hdnEnableFECC.Value != "")
                    EnableFECC = hdnEnableFECC.Value;
                else if (Session["EnableFECC"] != null)
                    EnableFECC = Session["EnableFECC"].ToString();


                string DefaultFECC = Session["DefaultFECC"].ToString();
                //EnableFECC = Session["EnableFECC"].ToString(); //ZD 101931

                if (EnableFECC == "1") //YES//FB 2592
                {
                    tdFECC.Attributes.Add("Style", "display:inline;"); //FB 2595
                    chkFECC.Visible = true;
                }
                else
                {
                    tdFECC.Attributes.Add("Style", "display:none;"); //FB 2595
                    chkFECC.Visible = false;
                }
                //FB 2583 End
                //FB 2641 start
                if (Session["EnableStartMode"] != null)
                    if (!string.IsNullOrEmpty(Session["EnableStartMode"].ToString()))
                        int.TryParse(Session["EnableStartMode"].ToString(), out EnableStartMode);

                if (Session["EnableLinerate"] != null)
                    if (!string.IsNullOrEmpty(Session["EnableLinerate"].ToString()))
                        int.TryParse(Session["EnableLinerate"].ToString(), out EnableLinerate);

                if (EnableLinerate == 1)
                {
                    trLineRate.Attributes.Add("Style", "Display:;");
                }
                else
                {
                    trLineRate.Attributes.Add("Style", "Display:None;");
                }

                if (EnableStartMode == 1)
                {
                    trStartMode.Attributes.Add("Style", "Display:;");
                }
                else
                {
                    trStartMode.Attributes.Add("Style", "Display:None;");
                }

                //FB 2641 End

                

                //FB 2670 START
                string EnableOnsiteAV = Session["EnableOnsiteAV"].ToString();
                string EnableMeetandGreet = Session["EnableMeetandGreet"].ToString();
                string EnableConciergeMonitoring = Session["EnableConciergeMonitoring"].ToString();
                string EnableDedicatedVNOC = Session["EnableDedicatedVNOC"].ToString();

                if (EnableOnsiteAV == "0" && EnableMeetandGreet == "0" && EnableConciergeMonitoring == "0" && EnableDedicatedVNOC == "0" ||
                    EnableOnsiteAV == "0" && EnableMeetandGreet == "0" && EnableConciergeMonitoring == "0" && EnableDedicatedVNOC == "1" && EnableVNOCselection == 0)
                    divConcSupport.Attributes.Add("style", "display:None");
                else
                    divConcSupport.Attributes.Add("style", "display:Block");

                if (EnableOnsiteAV == "1")
                    tdOnSiteAVSupport.Attributes.Add("Style", "Display:Block;");
                else
                    tdOnSiteAVSupport.Attributes.Add("Style", "Display:None;");

                if (EnableMeetandGreet == "1")
                    tdMeetandGreet.Attributes.Add("Style", "Display:Block;");
                else
                    tdMeetandGreet.Attributes.Add("Style", "Display:None;");

                if (EnableConciergeMonitoring == "1")
                    tdConciergeMonitoring.Attributes.Add("Style", "Display:Block;");
                else
                    tdConciergeMonitoring.Attributes.Add("Style", "Display:None;");

                if (EnableDedicatedVNOC == "1")
                    tdDedicatedVNOC.Attributes.Add("Style", "Display:Block;");
                else
                    tdDedicatedVNOC.Attributes.Add("Style", "Display:None;");
                //FB 2670 END

                //FB 2501 - Starts
                if (Session["EnableConfPassword"] != null && !string.IsNullOrEmpty(Session["EnableConfPassword"].ToString()))
                    int.TryParse(Session["EnableConfPassword"].ToString(), out EnableConferencePassword);
                //ALLDEV-826 Starts
                if (Session["EnableHostGuestPwd"] != null && !string.IsNullOrEmpty(Session["EnableHostGuestPwd"].ToString()))
                    int.TryParse(Session["EnableHostGuestPwd"].ToString(), out EnableHostGuestPwd);
                //ALLDEV-826 Ends
                if (EnableConferencePassword == 1)  //FB 2274
                {
                    trConfPass.Attributes.Add("style", "display:"); //ZD 100704
                    trConfPass1.Attributes.Add("style", "display:"); //ZD 100704
                    litNumericOrGuestPwd.Text = obj.GetTranslatedText("Numeric Password");    //ALLDEV-826
                }
                //ALLDEV-826 Starts
                else if (EnableHostGuestPwd == 1)
                {
                    trConfPass.Attributes.Add("style", "display:");
                    trConfPass1.Attributes.Add("style", "display:");                    
                    trHostPwd.Attributes.Add("style", "display:");
                    trCfmHostPwd.Attributes.Add("style", "display:");
                    litNumericOrGuestPwd.Text = obj.GetTranslatedText("Guest Password");
                }
                //ALLDEV-826 Ends
                else
                {
                    trConfPass.Attributes.Add("style", "display:None"); //ZD 100704
                    trConfPass1.Attributes.Add("style", "display:None"); //ZD 100704 
                    trHostPwd.Attributes.Add("style", "display:None");  //ALLDEV-826
                    trCfmHostPwd.Attributes.Add("style", "display:None");   //ALLDEV-826
                }

                ConferencePassword.Attributes.Add("value", confPassword.Value);
                ConferencePassword2.Attributes.Add("value", confPassword.Value);

                //ALLDEV-826 Starts
                txtHostPwd.Attributes.Add("value", hdnHostPassword.Value);
                txtCfmHostPwd.Attributes.Add("value", hdnHostPassword.Value);
                //ALLDEV-826 Ends
                //FB 2501 - End
                //FB 2659 - Starts
                //ZD 100166 Starts
                if (enableCloudInstallation == 1)
                {
                    lstVMR.ClearSelection();
                    lstVMR.SelectedValue = "0";
                    trseatavailable.Visible = true;
                    trAdvancedSettings.Attributes.Add("style", "display:none");
                    trCheckOptions.Attributes.Add("style", "display:none");
                    trchkDetails.Attributes.Add("style", "display:none");
                    trPCConf.Attributes.Add("style", "display:none");
                    trVMR.Attributes.Add("style", "display:none");
                    //ZD 100574 - Inncrewin Starts
                    trHostRow.Attributes.Add("style", "");
                    trHostRow.Visible = true;
                    trDescription.Attributes.Add("style", "");
                    //trConfTemp.Attributes.Add("style", "display:");// ZD 100570 24-12-2013 Inncrewin //ZD 100522
                    //ZD 100574 - Inncrewin end
                    trStatic.Visible = true; //ZD 100890

                    //ZD 101720 START
                    imgTDBConferencenote.Visible = true;
                    imgTDBConferencenote.ToolTip = "Conferences must be scheduled at least 20 minutes from the current time. Use the 'Start Now' option or 'Instant Conference' to schedule a meeting immediately";
                    imgTDBConferencenote.ToolTip = obj.GetTranslatedText(imgTDBConferencenote.ToolTip);
                    //ZD 101720 END
                }
                else
                {
                    trseatavailable.Visible = false;
                    trAdvancedSettings.Attributes.Add("style", "display:");
                    //ZD 100574 - Inncrewin Starts
                    trHostRow.Visible = false;
                    trDescription.Attributes.Add("style", "display:none");
                    //trConfTemp.Attributes.Add("style", "display:None");// ZD 100570 24-12-2013 Inncrewin //ZD 100522
                    //ZD 100574 - Inncrewin Starts	
                    trStatic.Visible = false;//ZD 100890
                    imgTDBConferencenote.Visible = false;//ZD 101720
                }
                //ZD 100166 End
                //FB 2659 - End

                if (Session["Cloud"] != null) //FB 2717
                    if (Session["Cloud"].ToString().Trim() == "1")
                        isCloudEnabled = 1;

                if (isCloudEnabled == 1)
                    trCloudConferencing.Attributes.Add("style", "display:");
                else
                    trCloudConferencing.Attributes.Add("style", "display:none");

                //FB 2993 Starts
                if (Session["NetworkSwitching"] != null)
                {
                    int.TryParse(Session["NetworkSwitching"].ToString(), out NetworkSwitching);

                    if (NetworkSwitching == 2)
                        trNetworkState.Visible = true;
                    else if (NetworkSwitching == 3)
                    {
                        trNetworkState.Visible = false;
                        drpNtwkClsfxtn.ClearSelection();
                        drpNtwkClsfxtn.SelectedValue = "0";
                    }
                    else
                    {
                        trNetworkState.Visible = false;
                        drpNtwkClsfxtn.ClearSelection();
                        drpNtwkClsfxtn.SelectedValue = "1";
                    }

                }
                //FB 2993 Ends

                //ZD 100834 Starts
                if (Session["EnableDetailedExpressForm"] != null)
                    int.TryParse(Session["EnableDetailedExpressForm"].ToString(), out enableDetailedExpressForm);

                if (enableDetailedExpressForm == 1)
                    trExternalUsers.Visible = true;
                else
                    trExternalUsers.Visible = false;
                //ZD 100834 End

                //ZD 101562 START
                if (Session["EnableTemplateBooking"] != null)
                    int.TryParse(Session["EnableTemplateBooking"].ToString(), out EnableTemplateBooking);
                if (EnableTemplateBooking == 1)
                    trConfTemp.Attributes.Add("Style", "Display:;");
                else
                    trConfTemp.Attributes.Add("Style", "Display:None;");
                //ZD 101562 END

                //ZD 101755  start
                if (Session["EnableSetupTimeDisplay"] != null && !string.IsNullOrEmpty(Session["EnableSetupTimeDisplay"].ToString()))
                    int.TryParse(Session["EnableSetupTimeDisplay"].ToString(), out EnableSetupTimeDisplay);
                if (EnableSetupTimeDisplay == 1 && !lstConferenceType.SelectedValue.Equals("8") && enableBufferZone == "1")
                    SetupRow.Attributes.Add("style", "Display:;");
                else
                    SetupRow.Attributes.Add("style", "Display:None;");

                if (Session["EnableTeardownTimeDisplay"] != null && !string.IsNullOrEmpty(Session["EnableTeardownTimeDisplay"].ToString()))
                    int.TryParse(Session["EnableTeardownTimeDisplay"].ToString(), out EnableTeardownTimeDisplay);
                if (EnableTeardownTimeDisplay == 1 && !lstConferenceType.SelectedValue.Equals("8") && enableBufferZone == "1")
                    TearDownRow.Attributes.Add("style", "Display:;");
                else
                    TearDownRow.Attributes.Add("style", "Display:None;");
                //ZD 101755 End

                //ZD 101931 start
                if (Session["ShowVideoLayout"] != null && !string.IsNullOrEmpty(Session["ShowVideoLayout"].ToString()))
                    int.TryParse(Session["ShowVideoLayout"].ToString(), out ShowVideoLayout);
                if (ShowVideoLayout == 2 || ShowVideoLayout == 0)
                {
                    tdVideoDisplay.Attributes.Add("style", "Display:;");
                    tdimgVideoDisplay.Attributes.Add("style", "Display:;");
                }
                else
                {
                    tdVideoDisplay.Attributes.Add("style", "Display:None;");
                    tdimgVideoDisplay.Attributes.Add("style", "Display:None;");
                }
                //ZD 101931 End

                //ALLDEV-833 START
                if (Session["EnableEWSPartyFreeBusy"] != null)
                {

                    if (Session["EnableEWSPartyFreeBusy"].ToString() == "0")
                    {
                        tdExchangeLookup.Visible = false;
                        tdSchedulingAssistant.Visible = false;
                    }
                    else
                    {
                        tdExchangeLookup.Visible = true;
                        tdSchedulingAssistant.Visible = true;
                    }
                }
                //ALLDEV-833 END

                if (!IsPostBack)
                {
                    //ZD 103216
                    if (Session["RoomHostDetails"] != null)
                        Session.Remove("RoomHostDetails");   

                    hdnROWID.Value = "0"; //ZD 100619
                    //FB 2426 Start
                    Session["hdModalDiv"] = "none";//ZD 101500                                        
                    obj.BindDialingOptions(lstGuestConnectionType);
                    lstGuestConnectionType.Items.Remove(lstGuestConnectionType.Items.FindByValue("-1"));
                    lstGuestConnectionType.Items.Remove(lstGuestConnectionType.Items.FindByValue("3"));
                    obj.GetCountryCodes(lstCountries);
                    lstCountries.Items.FindByValue("225").Selected = true;
                    //ZD 100619 Starts
                    //txtApprover5.Attributes.Add("readonly", "");
                    //txtEmailId.Attributes.Add("readonly", "");
                    //ZD 100619 Ends
                    Session["OnlyFlyRoom"] = null;
                    Session["onflyGridProfiles"] = null; //ZD 100619 
                    if (GuestRooms <= 0) //ZD 100704
                    {
                        OnFlyRowGuestRoom.Attributes.Add("style", "display:none");
                        btnGuestLocation.Attributes.Add("style", "display:none");
                        Image6.Attributes.Add("style", "display:none");
                    }
                    //FB 2426 End

                    //ZD 100704 Starts
                    if (EnableExpressConfType == 1 && Session["DefaultConferenceType"] != null && !string.IsNullOrEmpty(Session["DefaultConferenceType"].ToString()))
                    {
                        lstConferenceType.ClearSelection();
                        if (lstConferenceType.Items.FindByValue(Session["DefaultConferenceType"].ToString()) != null)
                            lstConferenceType.Items.FindByValue(Session["DefaultConferenceType"].ToString()).Selected = true;
                    }
                    //ZD 103550 - Start
                    if (Session["BJNMeetingType"] != null)
                    {
                        DrpBJNMeetingID.ClearSelection();
                        EnableBJNMeetingID = Session["BJNMeetingType"].ToString();
                        if (EnableBJNMeetingID != "")
                            DrpBJNMeetingID.Items.FindByValue(EnableBJNMeetingID).Selected = true;
                    }
                    //ZD 103550 - End

                    //ZD 104116 - Start
                    if (Session["BJNDisplay"] != null)
                    {
                        if (Session["BJNDisplay"].ToString() == "1")
                            chkBJNMeetingID.Checked = true;
                        else
                            chkBJNMeetingID.Checked = false;
                    }
                    //ZD 104116 - End
                    //ALLDEV-782 Starts
                    if (Session["PexipMeetingType"] != null)
                    {
                        drpPexipMtgType.ClearSelection();
                        EnablePexipMeetingID = Session["PexipMeetingType"].ToString();
                        if (EnablePexipMeetingID != "" && EnablePexipMeetingID != "0")
                            drpPexipMtgType.Items.FindByValue(EnablePexipMeetingID).Selected = true;
                    }
                    if (Session["PexipDisplay"] != null)
                    {
                        if (Session["PexipDisplay"].ToString() == "1")
                            chkPexipMtgType.Checked = true;
                        else
                            chkPexipMtgType.Checked = false;
                    }
                    //ALLDEV-782 Ends
                    //ALLBUGS-41
                    if (!IsPostBack && Request.QueryString["rms2"] == null && Request.QueryString["t"].ToString() == "n")
                        Session["locstr"] = null;

                    // ZD 102123 Start
                    string DConftype = "";
                    if (Request.QueryString["HConfType"] != null)
                        DConftype = Request.QueryString["HConfType"].ToString();
                    ViewState["DConftype"] = DConftype;
                    if (DConftype == "8" && DConftype != "")
                    {
                        if (lstConferenceType.Items.FindByValue("8") != null && EnableHotdeskingConfTypeOrgOption.Equals("1"))
                        {
                            lstConferenceType.ClearSelection();
                            lstConferenceType.Items.FindByValue("8").Selected = true;
                        }
                        if (Request.QueryString["rms2"] != null && Request.QueryString["rms2"] != "")
                        {
                            Session["RoomStrValue"] = Request.QueryString["rms2"].ToString();
                            Session["locstr"] = Request.QueryString["rms2"].ToString(); //ALLBUS-41
                        }
                    }
                    // ZD 102123 End

                    //FB 2226 - Start
                    if (Session["EnablePublicConf"] != null && !string.IsNullOrEmpty(Session["EnablePublicConf"].ToString()))
                    {
                        int.TryParse(Session["EnablePublicConf"].ToString(), out EnablePublicConf);
                    }

                    if (EnablePublicConf <= 0)
                        trPublic.Visible = false;
                    else
                        trPublic.Visible = true;
                    //ZD 100704 End

                    if (Session["defaultPublic"] != null)
                    {
                        if (Session["defaultPublic"].ToString() == "1")
                            chkPublic.Checked = true;
                        else
                            chkPublic.Checked = false;
                    }
                    //FB 2226 - End
                    txtVNOCOperator.Attributes.Add("readonly", ""); //FB 2501//FB 2670
                    //ZD 100570 Inncrewin 23-12-2013 Start
                    if (lstTemplates.Items.Count == 0)
                        obj.GetTemplateNames(lstTemplates);

                    for (int i = 0; i <= lstTemplates.Items.Count - 1; i++)
                        lstTemplates.Items[i].Attributes.Add("Title", lstTemplates.Items[i].Text);
                    //ZD 100570 Inncrewin 23-12-2013 End
                    //FB 2870 Start
                    if (Session["EnableNumericID"] != null)
                        EnableNumericID = Session["EnableNumericID"].ToString();

                    if (EnableNumericID == "1")
                    {
                        TrCTNumericID.Visible = true;
                    }
                    else
                    {
                        TrCTNumericID.Visible = false;
                        ChkEnableNumericID.Checked = false;
                    }

                    HtmlGenericControl MyDiv = (HtmlGenericControl)Page.FindControl("DivNumeridID");

                    if (ChkEnableNumericID.Checked)
                        MyDiv.Attributes.Add("style", "display:block");
                    else
                        MyDiv.Attributes.Add("style", "display:none");

                    //FB 2870 End
                    isFormSubmit = false;

                    Session.Remove("IsInstanceEdit");
                    //FB 2501 - Start
                    //lstDuration.Text = "01:00";
                    if (Session["DefaultConfDuration"] != null)
                    {
                        int ConfDuration = Convert.ToInt32(Session["DefaultConfDuration"]);
                        int ConfHours = ConfDuration / 60;
                        String hrs = (ConfHours < 10) ? "0" + ConfHours.ToString() : ConfHours.ToString();
                        int ConfMinutes = ConfDuration % 60;
                        String Mins = (ConfMinutes < 10) ? "0" + ConfMinutes.ToString() : ConfMinutes.ToString();
                        lstDuration.Text = hrs + ":" + Mins;
                    }
                    //FB 2501 - End   

                    if (Session["hasCalendar"] != null)
                        txtHasCalendar.Value = Session["hasCalendar"].ToString();

                    chkStartNow.Attributes.Add("onclick", "javascript:ChangeImmediate('S')"); //FB 2341
                    confEndTime.Attributes.Add("onblur", "javascript:CheckDuration()");
                    chkPublic.Attributes.Add("onclick", "javascript:ChangePublic()");//FB 2226
                    ChkEnableNumericID.Attributes.Add("onclick", "javascript:ChangeNumeric()");//FB 2870
                    lstAudioParty.Attributes.Add("onchange", "javascript:SelectAudioParty()");
                    ConferencePassword.Attributes.Add("onchange", "javascript:SavePassword()");//FB 2501
                    txtHostPwd.Attributes.Add("onchange", "javascript:SaveHostPassword()"); //ALLDEV-826
                    txtHost_exp.Attributes.Add("onchange", "javascript:saveHostName()");//ZD 100568 inncrewin 12/27/2013
                    //FB 2634

                    lstLineRate.ClearSelection(); //FB 2394  start
                    obj.BindLineRate(lstLineRate);

                    //FB 2429 - Starts
                    lstLineRate.SelectedValue = OrgLineRate.ToString();
                    if (OrgLineRate <= 0)
                    {
                        lstLineRate.ClearSelection();
                        lstLineRate.SelectedValue = "384";
                    }
                    //FB 2501 starts
                    if (Session["StartMode"].ToString() != null)
                        lstStartMode.SelectedValue = Session["StartMode"].ToString();
                    //FB 2501 Ends
                    if (Session["isExpressUser"] != null)
                    {
                        if (Session["isExpressUser"].ToString() == "1")
                        {
                            if (Session["isExpressManage"] != null && Session["isExpressUserAdv"] != null)
                            {
                                if (Session["isExpressManage"].ToString() == "1" || Session["isExpressUserAdv"].ToString() == "1")
                                    lstLineRate.Enabled = true;
                                else
                                    lstLineRate.Enabled = false;
                            }
                        }
                    }
                    //ZD 100522 Starts
                    if (hdnROOMPIN.Value == "1")
                    {
                        divChangeVMRPIN.Attributes.Add("style", "Display:");
                        trVMRPIN1.Attributes.Add("style", "Display:");
                        trVMRPIN2.Attributes.Add("style", "Display:");
                    }
                    //ZD 100522 Ends
                    //FB 2998
                    txtMCUConnect.Text = Session["McuSetupTime"].ToString();
                    txtMCUDisConnect.Text = Session["MCUTeardonwnTime"].ToString();
                    imgSetup.ToolTip = obj.GetTranslatedText("The number of minutes prior to the start of the conference that will be used to prepare the room for the meeting.");
                    imgTear.ToolTip = obj.GetTranslatedText("The number of minutes after the conference has ended to prepare the room for the next meeting."); //ZD 101755 
                    imgMCUConnect.ToolTip = obj.GetTranslatedText("The number of minutes that the MCU is to connect the endpoints, prior to the arrival of the participants into the conference.");
                    imgMCUDisconnect.ToolTip = obj.GetTranslatedText("The number of minutes that the MCU is to disconnect the endpoints, prior to the departure of the participants from the conference.");

                    //FB 2583 Start
                    if (DefaultFECC == "1" && EnableFECC != "2") //DD2
                        chkFECC.Checked = true;
                    else
                        chkFECC.Checked = false;
                    //FB 2583 End

                    String inXML = "";
                    String outXML;
                    confStartTime.Items.Clear();
                    confEndTime.Items.Clear();
                    obj.BindTimeToListBox(confStartTime, true, true);
                    obj.BindTimeToListBox(confEndTime, true, true);
                    if (Session["timeFormat"] != null)
                        if (Session["timeFormat"].ToString().Equals("0"))
                        {
                            //FB 2634
                            if (regEndTime != null && regStartTime != null)
                            {
                                regEndTime.ValidationExpression = "[0-2][0-9]:[0-5][0-9]";
                                regEndTime.ErrorMessage = obj.GetTranslatedText("Invalid Time (HH:MM)");//ZD 104163
                                regStartTime.ValidationExpression = "[0-2][0-9]:[0-5][0-9]";
                                regStartTime.ErrorMessage = obj.GetTranslatedText("Invalid Time (HH:MM)");//ZD 104163
                            }
                            //FB 2634	
                            //regSetupStartTime.ValidationExpression = "[0-2][0-9]:[0-5][0-9]";
                            //regSetupStartTime.ErrorMessage = obj.GetTranslatedText("Invalid Time (HH:mm)");
                            //regTearDownStartTime.ValidationExpression = "[0-2][0-9]:[0-5][0-9]";
                            //regTearDownStartTime.ErrorMessage = obj.GetTranslatedText("Invalid Time (HH:mm)");
                        }
                        else if (Session["timeFormat"].ToString().Equals("2"))//FB 2588
                        {
                            regEndTime.ValidationExpression = "[0-2][0-9][0-5][0-9][Z|z]";
                            regEndTime.ErrorMessage = obj.GetTranslatedText("Invalid Time (HHmmZ)");
                            regStartTime.ValidationExpression = "[0-2][0-9][0-5][0-9][Z|z]";
                            regStartTime.ErrorMessage = obj.GetTranslatedText("Invalid Time (HHmmZ)");
                        }
                    BindRecurrenceDefault();

                    GetVideoLayouts();//ZD 101869
                    Session["isTemplate"] = "0";  //ZD 103870
                    switch (Request.QueryString["t"].ToString())
                    {
                        case "n": // for new conference
                            {
                                Session["multisiloOrganizationID"] = null; //ZD_101233 Session Issue
                                //FB 2501 starts
                                txtApprover7.Text = Session["userName"].ToString();
                                hdnApprover7.Text = Session["userId"].ToString();
                                hdnRequestorMail.Text = Session["userEmail"].ToString();
                                hdnApproverMail.Text = Session["userEmail"].ToString();
                                TxtHostMail.Text = Session["userEmail"].ToString();//ZD 101226
                                //FB 2501 ends
                                txtApprover4.Text = Session["userName"].ToString();
                                hdnApprover4.Text = Session["userId"].ToString();

                                txtHost_exp.Text = Session["userName"].ToString();//ZD 101226
                                hdntxtHost_exp.Text = Session["userId"].ToString();//ZD 101226

                                inXML = "<login>" + obj.OrgXMLElement() + "<userID>" + Session["userID"] + "</userID></login>";
                                outXML = obj.CallMyVRMServer("GetNewConference", inXML, Application["MyVRMServer_ConfigPath"].ToString()); //FB 2027
                                Session.Add("outXML", outXML);
                                if (outXML.IndexOf("<error>") < 0)
                                    BindDataNew();
                                else
                                {
                                    errLabel.Text = obj.ShowErrorMessage(outXML);
                                    errLabel.Visible = true;
                                }
                                FetchAudioParticipants("n");  //ALLDEV-814 - Moved place
                                break;
                            }
                        case "o": //if you want to clone a conference
                            {
                                Session["multisiloOrganizationID"] = null; //ZD_101233 Session Issue
                                inXML = "<login>" + obj.OrgXMLElement() + "<userID>" + Session["userID"].ToString() + "</userID><selectType>1</selectType><selectID>" + Session["confid"].ToString() + "</selectID></login>";
                                outXML = obj.CallMyVRMServer("GetOldConference", inXML, Application["MyVRMServer_ConfigPath"].ToString());
                                Session.Add("outXML", outXML);
                                if (outXML.IndexOf("<error>") < 0)
                                    BindDataOld();
                                else
                                {
                                    errLabel.Text = obj.ShowErrorMessage(outXML);
                                    errLabel.Visible = true;
                                }
                                break;
                            }
                        case "t": //if you want to create a conference from a template
                            {
                                Session["isTemplate"] = "1"; //ZD 103870
                                Session["multisiloOrganizationID"] = null; //ZD_101233 Session Issue
                                inXML = "<login>" + obj.OrgXMLElement() + "<userID>" + Session["userID"].ToString() + "</userID><selectType>2</selectType><selectID>" + Session["confid"].ToString() + "</selectID></login>";
                                outXML = obj.CallMyVRMServer("GetOldConference", inXML, Application["MyVRMServer_ConfigPath"].ToString()); //FB 2027
                                Session.Add("outXML", outXML);
                                if (outXML.IndexOf("<error>") < 0)
                                    BindDataOld();
                                else
                                {
                                    errLabel.Text = obj.ShowErrorMessage(outXML);
                                    errLabel.Visible = true;
                                }
                                FetchAudioParticipants("t");//ZD 100570 24-12-2013 Inncrewin //ALLDEV-814 - Moved place
                                break;
                            }
                        case "": // for edit conference
                            {
                                inXML = "<login>" + obj.OrgXMLElement() + "<userID>" + Session["userID"].ToString() + "</userID><selectType>1</selectType><selectID>" + Session["confid"].ToString() + "</selectID></login>";
                                outXML = obj.CallMyVRMServer("GetOldConference", inXML, Application["MyVRMServer_ConfigPath"].ToString()); //FB 2027
                                outXML = outXML.Replace("& ", "&amp; ");
                                Session.Add("outXML", outXML);
                                if (outXML.IndexOf("<error>") < 0)
                                {
                                    BindDataOld();

                                    FetchAudioParticipants("e");
                                }
                                else
                                {
                                    errLabel.Visible = true;
                                }

                                //FB 1825 Start
                                if (!(Session["admin"].ToString().Trim() == "2"))
                                {

                                    hdnSetStartNow.Value = "hide";
                                }
                                //FB 1825 End
                                break;
                            }
                    }

                    if (Request.QueryString["op"] != null)//FB 2341
                    {
                        if (Request.QueryString["op"].ToString().Equals("2"))
                            chkStartNow.Checked = true;
                    }
                }
                else
                {
                    isFormSubmit = true;
                    //ZD 101052 Starts - Files are saved in upload path tiwce.
                    //if (Request.Params.Get("__EVENTTARGET").ToString().IndexOf("btnUploadFiles") >= 0)
                    //{
                    //    UploadFiles(new Object(), new EventArgs());
                    //}
                    //ZD 101052 End

                    //FB 2634
                    if ((RecurFlag.Value.Equals(1)) || (chkStartNow.Checked)) //FB 2341
                    {
                        reqStartTime.Enabled = false;
                        regStartTime.Enabled = false;
                        reqEndTime.Enabled = false;
                        //reqEndTime.Enabled = false;
                    }

                }
                // ZD 102585 starts
                if (Session["IsStaticIDEnabled"] != null && Session["IsStaticIDEnabled"].ToString() != null)
                    int.TryParse(Session["IsStaticIDEnabled"].ToString(), out isStaticIDEnabled);

                if (isStaticIDEnabled == 1) 
                    trStatic.Style.Add("display", "table-row");
                else
                {
                    trStatic.Style.Add("display", "none");
                    chkStatic.Checked = false;
                    txtStatic.Text = "";
                }
                // ZD 102585 End

                //FB 2599 Start
                if (isCloudEnabled == 1 && chkCloudConferencing.Checked) //FB 2262-S
                {
                    //ZD 100704 starts
                    lstConferenceType.ClearSelection();
                    lstConferenceType.SelectedValue = ns_MyVRMNet.vrmConfType.AudioVideo;
                    trConfType.Attributes.Add("style", "display:none");
                    //ZD 100704 End
                    lstVMR.ClearSelection();
                    lstVMR.SelectedValue = "3";
                    lstVMR.Enabled = false;
                    OnFlyRowGuestRoom.Attributes.Add("style", "display:none");
                    btnGuestLocation.Attributes.Add("style", "display:none");
                    Image6.Attributes.Add("style", "display:none");
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "changeVMR", "changeVMR();", true);
                }
                else
                {
                    //trConfType.Attributes.Add("style", "display:");//ZD 100704
                    lstVMR.Enabled = true;//Vidyo_J
                    if (GuestRooms > 0) //ZD 100704
                    {
                        OnFlyRowGuestRoom.Attributes.Add("style", "display:");
                        btnGuestLocation.Attributes.Add("style", "display:");
                        Image6.Attributes.Add("style", "display:");
                    }
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "changeVMR", "changeVMR();", true);
                }
                //FB 2599 End


                HideRecurButton();
                //FB 2592
                FillCustomAttributeTable();
                 
                hdnMcuProfileSelected.Value = "";//FB 2839
                hdnMcuPoolOrder.Value = ""; //ZD 104256
                FillMCUProfile();//FB 2839
				//ZD 104357
                if (isAVSettingsSelected)
                {
                    chkAdvancesetting.Checked = true;
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "changeVMR", "openAdvancesetting();", true);                    
                }

                //ZD 101233 START //ZD 102312
                if (Session["EnableExpressConfType"] != null)
                {
                    if (Session["EnableExpressConfType"].ToString() != "0")
                    {
                        if (!client.ToString().ToUpper().Equals(ns_MyVRMNet.vrmClient.MOJ))
                        {
                            if (P2PEnableOrgOption.Equals("0"))
                                lstConferenceType.Items.Remove(lstConferenceType.Items.FindByValue("4"));
							//ZD 103095 starts
                            if (EnableRoomConfTypeOrgOption.Equals("0"))
                                lstConferenceType.Items.Remove(lstConferenceType.Items.FindByValue("7"));
                            if (EnableAudioVideoConfTypeOrgOption.Equals("0"))
                                lstConferenceType.Items.Remove(lstConferenceType.Items.FindByValue("2"));
                            if (EnableAudioOnlyConfTypeOrgOption.Equals("0"))
                                lstConferenceType.Items.Remove(lstConferenceType.Items.FindByValue("6"));
                            if (EnableHotdeskingConfTypeOrgOption.Equals("0"))
                                lstConferenceType.Items.Remove(lstConferenceType.Items.FindByValue("8"));
							//ZD 103095 End
                            if (EnableOBTP == 0)
                                lstConferenceType.Items.Remove(lstConferenceType.Items.FindByValue("9"));//ZD 100513
                        }
                    }
                }


                if (EnablePublicConference != null && Enableopenforregistration != null)
                    if (EnablePublicConference == "1" && Enableopenforregistration == "0")
                    {
                        trPublic.Visible = true;
                    }
                    else if (EnablePublicConference == "1" && Enableopenforregistration == "1")
                    {
                        trPublic.Visible = true;
                    }
                    else if (EnablePublicConference == "0")
                    {
                        trPublic.Visible = false;
                    }

                if (EnableConferencePasswordOrgOption != null)
                    if (EnableConferencePasswordOrgOption == "0")
                    {
                        //ALLDEV-826 Starts
                        if(EnableHostGuestPwdOrgOption != null)
                            if (EnableHostGuestPwdOrgOption == "0")
                            {//ALLDEV-826 Ends
                                trConfPass.Visible = false;
                                trConfPass1.Visible = false;
                            } //ALLDEV-826
                    }
                //ALLDEV-826 Starts
                if(EnableHostGuestPwdOrgOption != null)
                    if (EnableHostGuestPwdOrgOption == "0")
                    {
                        if (EnableConferencePasswordOrgOption != null)
                            if (EnableConferencePasswordOrgOption == "0")
                            {
                                trConfPass.Visible = false;
                                trConfPass1.Visible = false;
                            }
                        trHostPwd.Visible = false;
                        trCfmHostPwd.Visible = false;
                    }
                //ALLDEV-826 Ends
              //ZD 101233 END

                if (!IsPostBack)
                {
                    FillRequestorPhoneNo();
                    DisplayUserEndpoints(); //ZD 100834
                }
                if (Session["timeZoneDisplay"] != null)
                {
                    if (Session["timeZoneDisplay"].ToString() != "")
                        timeZone = Session["timeZoneDisplay"].ToString();
                }

                //ZD 100707 Start
                if (Session["EnablePersonaVMR"].ToString() == "0")
                    lstVMR.Items.Remove(lstVMR.Items.FindByValue("1"));
                if (Session["EnableRoomVMR"].ToString() == "0")
                    lstVMR.Items.Remove(lstVMR.Items.FindByValue("2"));
                if (Session["EnableExternalVMR"].ToString() == "0")
                    lstVMR.Items.Remove(lstVMR.Items.FindByValue("3"));

                if (Session["ShowHideVMR"].ToString() == "0" || (Session["EnablePersonaVMR"].ToString() == "0" && Session["EnableRoomVMR"].ToString() == "0" && Session["EnableExternalVMR"].ToString() == "0"))
                    trVMR.Attributes.Add("Style", "display:none");
                else
                    trVMR.Attributes.Add("style", "display:");

                //ZD 100707 End
                //ZD 102086 & ZD 102089
                if (locstrname.Value == "")
                {
                    string roomID = "";
                    string selectRooms = "";
                    if (Session["RoomStrValue"] != null)
                        selectRooms = Session["RoomStrValue"].ToString();

                    if (Request.QueryString["rms"] != null)
                        roomID = Request.QueryString["rms"];

                    if (selectRooms != "" && roomID != "")
                    {
						//ZD 102357
                        //string[] rmStr = selectRooms.Split(DoublePlusDelim, StringSplitOptions.RemoveEmptyEntries);                        
                        //for (int r = 0; r < rmStr.Length; r++)
                        //{
                        //    if (roomID == rmStr[r].Split('|')[0])
                        //        locstrname.Value = rmStr[r];
                        //}

                        string[] rmStr = selectRooms.Split(DoublePlusDelim, StringSplitOptions.RemoveEmptyEntries);
                        string[] rmIDVal = roomID.Split(',');
                        for (int r = 0; r < rmStr.Length; r++)
                        {
                            if (rmIDVal[r] == rmStr[r].Split('|')[0])
                            {
                                if (locstrname.Value == "")
                                    locstrname.Value = rmStr[r];
                                else
                                    locstrname.Value = locstrname.Value + "++" + rmStr[r];
                            }
                        }
                    }
                    else
                        RoomList.Items.Clear(); //ZD 104069

                    Session["RoomStrValue"] = "";
                }

                //FB 100922 start
                if (locstrname.Value != null && locstrname.Value != "")
                {
                    String[] roomsSelcted = locstrname.Value.Split(DoublePlusDelim, StringSplitOptions.RemoveEmptyEntries);
                    ListItem item = null;
                    RoomList.Items.Clear();
                    selectedloc.Value = ""; //ZD 102086
                    for (int i = 0; i < roomsSelcted.Length; i++)
                    {
                        if (roomsSelcted[i].Trim() == "")
                            continue;

                        item = new ListItem();
                        item.Value = roomsSelcted[i].Split('|')[0];
                        item.Text = roomsSelcted[i].Split('|')[1];
                        //ZD 102086
                        if (selectedloc.Value == "")
                            selectedloc.Value = item.Value;
                        else
                            selectedloc.Value += "," + item.Value;

                        RoomList.Items.Add(item);
                    }
                }
                //FB 100922 end
				//ZD 100873 start 
                if (Session["EnablePCUser"] != null && !string.IsNullOrEmpty(Session["EnablePCUser"].ToString()))
                    int.TryParse(Session["EnablePCUser"].ToString(), out EnablePCUser);

                if (EnablePCUser <= 0)
                    trPCConf.Attributes.Add("style", "display:none");
                else if (chkBJNMeetingID.Checked == false || chkPexipMtgType.Checked == false) //ZD 103550 //ALLDEV-782
                    trPCConf.Attributes.Add("style", "display:");
                //ZD 100873 End 

                //ZD 100704 Starts //ZD 103095 -Commented-Already it defined above
                //if (Session["P2PEnable"] != null && !string.IsNullOrEmpty(Session["P2PEnable"].ToString()))
                //    int.TryParse(Session["P2PEnable"].ToString(), out P2PEnable);

                //if (Session["EnableRoomConfType"] != null && !string.IsNullOrEmpty(Session["EnableRoomConfType"].ToString()))
                //    int.TryParse(Session["EnableRoomConfType"].ToString(), out EnableRoomConfType);

                //if (Session["EnableHotdeskingConference"] != null && !string.IsNullOrEmpty(Session["EnableHotdeskingConference"].ToString()))
                //    int.TryParse(Session["EnableHotdeskingConference"].ToString(), out EnableHotdeskingConfType);

                //if (Session["EnableAudioVideoConfType"] != null && !string.IsNullOrEmpty(Session["EnableAudioVideoConfType"].ToString()))
                //    int.TryParse(Session["EnableAudioVideoConfType"].ToString(), out EnableAudioVideoConfType);

                //if (Session["EnableAudioOnlyConfType"] != null && !string.IsNullOrEmpty(Session["EnableAudioOnlyConfType"].ToString()))
                //    int.TryParse(Session["EnableAudioOnlyConfType"].ToString(), out EnableAudioOnlyConfType);

                //ZD 102312
                //if (Session["EnableExpressConfType"] != null)
                //{
                //    if (Session["EnableExpressConfType"].ToString() != "0")
                //    {
                //        if (P2PEnable <= 0)
                //            lstConferenceType.Items.Remove(lstConferenceType.Items.FindByValue("4"));
                //        if (EnableRoomConfType <= 0)
                //            lstConferenceType.Items.Remove(lstConferenceType.Items.FindByValue("7"));
                //        if (EnableAudioVideoConfType <= 0)
                //            lstConferenceType.Items.Remove(lstConferenceType.Items.FindByValue("2"));
                //        if (EnableAudioOnlyConfType <= 0)
                //            lstConferenceType.Items.Remove(lstConferenceType.Items.FindByValue("6"));
                //        if (EnableHotdeskingConfType <= 0)
                //            lstConferenceType.Items.Remove(lstConferenceType.Items.FindByValue("8"));
                //        if (EnableOBTP <= 0)
                //            lstConferenceType.Items.Remove(lstConferenceType.Items.FindByValue("9"));//ZD 100513
                //    }
                //}
                //ZD 100704 End

                //ZD 100522 Starts
                char[] ConfPassLenghth; int passlength = 0;
                int.TryParse(hdnPINlen.Value, out passlength);
                numPassword1.Enabled = false; RegVMRPIN.Enabled = false; regValHostPwd.Enabled = false; //ALLDEV-826
                numPassword1.ErrorMessage = "<br/>" + obj.GetTranslatedText("Minimum Length is ") + hdnPINlen.Value + ". " + obj.GetTranslatedText("Numeric values only") + ".";//ZD 101344//ZD 103843
                RegVMRPIN.ErrorMessage = "<br/>" + obj.GetTranslatedText("Minimum Length is ") + hdnPINlen.Value + ". <br/>" + obj.GetTranslatedText("Numeric values only") + ".";//ZD 101344//ZD 103843
                regValHostPwd.ErrorMessage = "<br/>" + obj.GetTranslatedText("Minimum Length is ") + hdnPINlen.Value + ". " + obj.GetTranslatedText("Numeric values only") + ".";  //ALLDEV-826
                if (ConferencePassword.Text != "" && hdnisBJNConf.Value == "0") //ZD 103263
                {
                    ConfPassLenghth = ConferencePassword.Text.ToCharArray();
                    if (ConfPassLenghth.Length < passlength)
                    {
                        numPassword1.ValidationExpression = @"^([1-9])([0-9]{" + hdnPINlen.Value + ",8}\\d+)";
                        numPassword1.ErrorMessage = obj.GetTranslatedText("Minimum Length is ") + hdnPINlen.Value + ". <br/>" + obj.GetTranslatedText("Numeric values only") + ".";//ZD 101344//ZD 103843
                        numPassword1.Enabled = true;
                    }
                }
                if (lstVMR.SelectedValue == "2" && txtVMRPIN1.Text != "" && hdnisBJNConf.Value == "0") //ZD 103263 //104133
                {
                    ConfPassLenghth = txtVMRPIN1.Text.ToCharArray();
                    if (VMRPINChange == 1) //104133
                    {
                        if (ConfPassLenghth.Length < passlength)
                        {
                            RegVMRPIN.ValidationExpression = @"^([1-9])([0-9]{" + hdnPINlen.Value + ",8}\\d+)";
                            RegVMRPIN.ErrorMessage = obj.GetTranslatedText("Minimum Length is ") + hdnPINlen.Value + ". " + obj.GetTranslatedText("Numeric values only") + ".";//ZD 101344//ZD 103843
                            RegVMRPIN.Enabled = true;
                        }
                    }
                }
                //ALLDEV-826 Starts
                if (txtHostPwd.Text != "")
                {
                    ConfPassLenghth = txtHostPwd.Text.ToCharArray();
                    if (ConfPassLenghth.Length < passlength)
                    {
                        regValHostPwd.ValidationExpression = @"^([1-9])([0-9]{" + hdnPINlen.Value + ",8}\\d+)";
                        regValHostPwd.ErrorMessage = obj.GetTranslatedText("Minimum Length is ") + hdnPINlen.Value + ". <br/>" + obj.GetTranslatedText("Numeric values only") + ".";
                        regValHostPwd.Enabled = true;
                    }
                }
                //ALLDEV-826 Ends
                //ZD 100522 Ends

                //ZD 103550 - Start  //ZD 104116 - Start          
                if (EnableBJNDisplay == "1") 
                {
                    trBJNMeeting.Visible = true;
                    trBJNMeeting.Attributes.Add("style", "display:");
                }
                else
                {
                    trBJNMeeting.Visible = false;
                    trBJNMeeting.Attributes.Add("style", "display:none");
                }
                if (EnableBJNSelectOption == "1")
                {
                    HtmlGenericControl DivBJNMeeting = (HtmlGenericControl)Page.FindControl("DivBJNMeetingID");

                    if (chkBJNMeetingID.Checked == true)
                        DivBJNMeeting.Attributes.Add("style", "display:block");
                    else
                        DivBJNMeeting.Attributes.Add("style", "display:none");
                }

                //ZD 103550 - End
                 //ALLDEV-782 Starts
                if (EnablePexipDisplay == "1" && EnablePexipIntegration == "1") 
                {
                    trPexipMeeting.Visible = true;
                    trPexipMeeting.Attributes.Add("style", "display:");
                    if (isEditMode == "0")
                        chkPexipMtgType.Checked = true;
                }
                else
                {
                    trPexipMeeting.Visible = false;
                    trPexipMeeting.Attributes.Add("style", "display:none");
                }
                if (hdnChangePexipVal.Value == "0")
                    chkPexipMtgType.Checked = false;
                else if (hdnChangePexipVal.Value == "1")
                    chkPexipMtgType.Checked = true;

                if ((lstConferenceType.SelectedValue.Equals("7")) || (lstConferenceType.SelectedValue.Equals("4")) || (lstConferenceType.SelectedValue.Equals("8")) || lstVMR.SelectedIndex > 0 || chkPCConf.Checked)
                    chkPexipMtgType.Checked = false;
                
                if (EnablePexipSelectOption == "1")
                {
                    HtmlGenericControl DivPexipMeeting = (HtmlGenericControl)Page.FindControl("DivPexipMtgType");

                    if (chkPexipMtgType.Checked == true)
                        DivPexipMeeting.Attributes.Add("style", "display:block");
                    else
                        DivPexipMeeting.Attributes.Add("style", "display:none");
                }

                //ALLDEV-782 Ends

                //if ((EnableBJNIntegration == "1" && EnableBJNDisplay == "0") || (EnablePexipSelectOption == "1" && EnablePexipDisplay == "0")) //ALLDEV-782
                if (EnableBJNIntegration == "1" || EnablePexipIntegration == "1")
                {
                    trVMR.Attributes.Add("style", "display:none");
                    trPCConf.Attributes.Add("style", "display:none");
                }
                //ZD 104116 - End
              
            }
            catch (Exception ex)
            {
                log.Trace("Simple Conference :Page_load" + ex.Message);

            }
        }
        #endregion

        #region Add Rooms
        //FB 2367
        protected void btnRoomSelect_Click(Object sender, EventArgs e)
        {
            try
            {
                SyncRoomSelection();
                RoomList.Items.Clear();
                RoomList.Enabled = true;
                string locsName = ""; //ZD 103870
                foreach (TreeNode tn in treeRoomSelection.CheckedNodes)
                {
                    if (tn.Depth == 3)
                    {
                        if (strRoomIds == "")
                            strRoomIds = tn.Value;
                        else
                            strRoomIds = strRoomIds + "," + tn.Value;

                        if (strRoomName == "")
                            strRoomName = tn.Text;
                        else
                            strRoomName = strRoomName + "$" + tn.Text;

                        if (locsName == "")//ZD 103870
                            locsName = tn.Value + "|" + tn.Text;
                        else
                            locsName += "++" + tn.Value + "|" + tn.Text;
                    }
                }

                if (locstrname.Value == "")//ZD 103870
                    locstrname.Value = locsName;

                String[] strArrRms;
                strArrRms = strRoomName.Split('$');
                String[] strArrRmsIds;
                strArrRmsIds = strRoomIds.Split(',');

                if (strArrRms != null && strArrRmsIds != null)
                {
                    if (strArrRms.Length == strArrRmsIds.Length)
                    {
						//ZD 100834 Starts
                        ListItem liRms = null; 
                        for (int i = 0; i < strArrRms.Length; i++)
                        {
                            if (strArrRms[i].Trim() == "") 
                                continue;
                            liRms = new ListItem();
							//ZD 100834 End
                            liRms.Value = strArrRmsIds[i].ToString();
                            liRms.Text = strArrRms[i].ToString();
                            RoomList.Items.Add(liRms);
                        }
                    }
                }
                if (RoomList.Items.Count > 0)
                    RoomList.Enabled = true;
                else
                    RoomList.Enabled = true;
            }
            catch (Exception ex)
            {
                //ZD 100263
                log.Trace("Add Rooms : " + ex.Message);
                errLabel.Text = obj.ShowSystemMessage();
                //errLabel.Text = "Add Rooms : " + ex.Message;
            }
        }
        #endregion

        #region FillRequestorPhoneNo
        private void FillRequestorPhoneNo()
        {
            string workNo = "";
            string cellNo = "";
            try
            {
                if (Session["workPhone"] != null)
                    workNo = Session["workPhone"].ToString();

                if (Session["cellPhone"] != null)
                    cellNo = Session["cellPhone"].ToString();

                string[] hostIDArr = hdnHostIDs.Value.Trim().Split('?');
                if (hostIDArr != null)
                {
                    if (hostIDArr.Length > 1)
                    {
                        TextBox txtWork = (TextBox)this.FindControl(hostIDArr[0]);
                        if (txtWork.Text.Trim() == "")
                            txtWork.Text = workNo;

                        TextBox txtCell = (TextBox)this.FindControl(hostIDArr[1]);
                        if (txtCell.Text.Trim() == "")
                            txtCell.Text = cellNo;
                    }
                }
            }
            catch
            { }
        }
        #endregion

        #region CalculateDuration
        public void CalculateDuration(Object server, EventArgs e)
        {
            CalculateDuration();
        }

        #endregion

        #region BindDataNew
        /// <summary>
        /// This method will be used on new conference Creation
        /// </summary>
        private void BindDataNew()
        {
            XmlNode usrNode = null;//FB 2376 
            try
            {
                XmlDocument xmlDOC = new XmlDocument();
                xmlDOC.LoadXml(Session["outxml"].ToString());
                XmlNode node = (XmlNode)xmlDOC.DocumentElement;
                XmlNodeList nodes = node.SelectNodes("//conference/confInfo/timezones/timezone");
                lblConfID.Text = "new";
                int length = nodes.Count;
                //obj.GetSystemDateTime(Application["COM_ConfigPath"].ToString());// FB 1735
                obj.GetSystemDateTime(Application["MyVRMServer_ConfigPath"].ToString());// FB 2027


                //FB 2717 Starts
                lstVMR.ClearSelection();
                lstVMR.SelectedIndex = 0; //FB 2780
                //FB 2717 End

                //FB 2634
                if (EnableBufferZone == 1)
                {
                    SetupDuration.Text = OrgSetupTime.ToString();
                    //TearDownDuration.Text = "0"; //ZD 100085
                    TearDownDuration.Text = OrgTearDownTime.ToString();//ZD 101755
                }
                else
                {
                    SetupDuration.Text = "0";
                    TearDownDuration.Text = "0";
                }
                DateTime startDateTime = DateTime.Parse(Session["systemDate"].ToString() + " " + Session["systemTime"].ToString());
                if (Request.QueryString["sd"] != null)
                    startDateTime = DateTime.Parse(Request.QueryString["sd"].ToString() + " " + Request.QueryString["st"].ToString());
                else
                {
                    //ZD 102532
                    if (ViewState["DConftype"] != null && ViewState["DConftype"].ToString() == "8")
                        startDateTime = startDateTime.AddMinutes(6); //Doubt - shall we add extra one minute for selecting other rooms.else it will throw error - Please enter a valid conference start date / time. Error Code : 223
                    else if (startDateTime.Minute < 45)
                        startDateTime = startDateTime.AddMinutes(60 - startDateTime.Minute);
                    else
                        startDateTime = startDateTime.AddMinutes(120 - startDateTime.Minute);
                }

                //FB 2634 //ZD 103918
                if (Request.QueryString["sd"] == null)
                {
                    if (EnableBufferZone == 1 && !lstConferenceType.SelectedValue.Equals(ns_MyVRMNet.vrmConfType.HotDesking))//ZD 102532
                        startDateTime = startDateTime.AddMinutes(Double.Parse(OrgSetupTime.ToString()));
                }
                confStartDate.Text = startDateTime.ToString(format);
                confStartTime.Text = startDateTime.ToString(tformat);
                //FB 2364
                //FB 2398 Start
                //if (EnableBufferZone == 1) 
                //{
                //    startDateTime =  startDateTime.AddMinutes(OrgSetupTime);
                //}
                //FB 2398 End
                //FB 2501 - Start
                DateTime endDateTime = DateTime.Now;
                String ConfDuration = Session["DefaultConfDuration"].ToString();
                if (Request.QueryString["dur"] != null)//ZD 103918
                    endDateTime = startDateTime.AddMinutes(Int32.Parse(Request.QueryString["dur"].ToString()));
                else
                {
                    if (ConfDuration != null)
                        endDateTime = startDateTime.AddMinutes(Int32.Parse(ConfDuration));
                }                

                //DateTime endDateTime = startDateTime.AddMinutes(60);
                //if (client.ToString().ToUpper().Equals(ns_MyVRMNet.vrmClient.MOJ))
                //    endDateTime = startDateTime.AddMinutes(Int32.Parse(ConfDuration));
                //FB 2501 - End

                confEndDate.Text = endDateTime.ToString(format);
                confEndTime.Text = endDateTime.ToString(tformat);
                confStartTime.SelectedValue = confStartTime.Text;
                confEndTime.SelectedValue = confEndTime.Text;
                CalculateDuration();
                //FB 2634
                //SetupDate.Text = startDateTime.ToString(format);
                //SetupTime.Text = startDateTime.ToString(tformat);
                //TearDownDate.Text = endDateTime.ToString(format);
                //TeardownTime.Text = endDateTime.ToString(tformat);
                //SetupTime.SelectedValue = confStartTime.Text;
                //TeardownTime.SelectedValue = confEndTime.Text;

                String selTZ = "-1";
                lstConferenceTZ.ClearSelection();
                obj.GetTimezones(lstConferenceTZ, ref selTZ);
                if (node.SelectSingleNode("/conference/confInfo/timeZone") != null)
                    lstConferenceTZ.Items.FindByValue(node.SelectSingleNode("/conference/confInfo/timeZone").InnerText).Selected = true;
                txtModifyType.Text = "0";
                LoadCommonValues(node);
                
                //RefreshRoom(null, null);//ZD 102008 //ZD 102086
                /** FB 2376 **/
                usrNode = xmlDOC.SelectSingleNode("conference/userInfo/internalBridge");
                if (usrNode != null)
                {
                    txtintbridge.Text = usrNode.InnerText;
                    hdnintbridge.Value = usrNode.InnerText;
                }

                usrNode = xmlDOC.SelectSingleNode("conference/userInfo/externalBridge");
                if (usrNode != null)
                {
                    txtextbridge.Text = usrNode.InnerText;
                    hdnextbridge.Value = usrNode.InnerText;
                }
                /** FB 2376 **/

                //ZD 100890 Start
                usrNode = xmlDOC.SelectSingleNode("conference/userInfo/StaticID");
                if (usrNode != null)
                    txtStatic.Text = usrNode.InnerText;
                //ZD 100890 End
                //ZD 104289
                string conferenceCode = "", leaderPin = "", participantCode = "";
                if (xmlDOC.SelectSingleNode("conference/userInfo/hostConferenceCode") != null)
                    conferenceCode = xmlDOC.SelectSingleNode("conference/userInfo/hostConferenceCode").InnerText;
                if (xmlDOC.SelectSingleNode("conference/userInfo/hostLeaderPin") != null)
                    leaderPin = xmlDOC.SelectSingleNode("conference/userInfo/hostLeaderPin").InnerText;
                if (xmlDOC.SelectSingleNode("conference/userInfo/hostParticipantCode") != null)
                    participantCode = xmlDOC.SelectSingleNode("conference/userInfo/hostParticipantCode").InnerText;

                hdnDialString.Value = conferenceCode + "�" + leaderPin + "�" + participantCode;
                //ZD 101015 Starts
                usrNode = xmlDOC.SelectSingleNode("conference/userInfo/userWebExStatus");
                if (usrNode != null)
                    int.TryParse(usrNode.InnerText.ToString(), out EnableWebExConf);

                hdnHostWebEx.Value = EnableWebExConf.ToString();
                hdnSchdWebEx.Value = EnableWebExConf.ToString();

                if (EnableWebExIntg == 1 && EnableWebExConf == 1)
                {
                    trWebex.Attributes.Add("style", "display:");
                    if (WebExLaunchMode == 0) //ZD 100513
                    {
                        chkWebex.Checked = true;
                        //hdnWebExPwd.Value = " ";
                    }
                }
                else
                {
                    trWebex.Attributes.Add("style", "display:none");
                    chkWebex.Checked = false;
                    divWebPW.Attributes.Add("style", "display:none");
                    divWebCPW.Attributes.Add("style", "display:none");
                }
                //ZD 101015 Ends
                CheckPCVendor();//FB 2693 
                //ZD 100513 Starts
                ConerenceType = lstConferenceType.SelectedValue;
                if (ConerenceType == "9")
                    ConerenceType = "2";
                hdnconftype.Value = ConerenceType;//ZD 100704
                //ZD 100513 ENDS
                //hdnconftype.Value = "2";//ZD 100570 24-12-2013 Inncrewin

                LoadGuestEndpointProfile(0); //ZD 100619
            }
            catch (Exception ex)
            {
                errLabel.Visible = true;
                log.Trace("BindDataNew: " + ex.Message);
            }
        }
        #endregion

        #region BindDataOld
        /// <summary>
        /// To get Created Conferences in Edit/Clone Mode
        /// </summary>

        private void BindDataOld()
        {
            try
            {   
                lblConfHeader.Text = obj.GetTranslatedText("Edit");//FB 1830 - Translation
                XmlDocument xmlDOC = new XmlDocument();
                xmlDOC.LoadXml(Session["outxml"].ToString());
                XmlNode node = (XmlNode)xmlDOC.DocumentElement;
                string recurring = "";
                XmlNodeList ConfVNOCnodes; //FB 2670

                DateTime setupStartDateTime = DateTime.MinValue;
                DateTime teardownStartDateTime = DateTime.MinValue;
                Double setupDuration = Double.MinValue;
                Double teardownDuration = Double.MinValue;
                string setupDur = "0";
                string tearDownDur = "0";
                //string[] tmpstrs;//FB 2341//FB 2377
                //string tmpstr = "";
                //int i, tmpint;
                int hdnGuestPfRowID = 0; //ZD 100619

                if (!Request.QueryString["t"].ToString().Equals("t"))
                {
                    recurring = node.SelectSingleNode("//conference/confInfo/recurring").InnerText;
                    lblConfID.Text = node.SelectSingleNode("//conference/confInfo/confID").InnerText;
                }

                //ZD 101229 exchange round trip starts
                if(node.SelectSingleNode("//conference/confInfo/confOrigin")!=null)
                     hdnconfOriginID.Value= node.SelectSingleNode("//conference/confInfo/confOrigin").InnerText;
                if(node.SelectSingleNode("//conference/confInfo/IcalID")!=null)
                     hdnicalID.Value = node.SelectSingleNode("//conference/confInfo/IcalID").InnerText;
                //ZD 101229 exchange round trip ends

                //FB 2550 - Starts
                
                if (Request.QueryString["t"].ToString().Equals("o"))
                {
                    isClone = true;
                }
                //FB 2550 - End

                //ZD 101233 START
                string multiOrgID = Session["organizationID"].ToString();
                if (node.SelectSingleNode("/conference/confInfo/ConfOrgID") != null)
                {
                    if (multiOrgID != node.SelectSingleNode("/conference/confInfo/ConfOrgID").InnerText.Trim())
                    {
                        if (Session["multisiloOrganizationID"] == null)
                        {
                            Session.Add("multisiloOrganizationID", node.SelectSingleNode("/conference/confInfo/ConfOrgID").InnerText.Trim());
                        }
                        else
                            Session["multisiloOrganizationID"] = node.SelectSingleNode("/conference/confInfo/ConfOrgID").InnerText.Trim();
                        hdnCrossAddtoGroup.Value = Session["multisiloOrganizationID"].ToString();
                    }
                    else
                        Session["multisiloOrganizationID"] = null;
                }

                //btnGuestLocation.Disabled = false;
                if (Session["multisiloOrganizationID"] != null && Session["multisiloOrganizationID"].ToString() != "0")
                {
                    if (Session["multisiloOrganizationID"].ToString() != Session["organizationID"].ToString())
                    {
                        lstTemplates.Enabled = false;
                        //btnGuestLocation.Disabled = true;
                    }
                }
                //ZD 101233 START

                if (!Request.QueryString["t"].ToString().Equals("t") && !Request.QueryString["t"].ToString().Equals("o"))
                {
                    if (node.SelectSingleNode("//conference/isinstanceedit") != null)
                    {
                        if (node.SelectSingleNode("//conference/isinstanceedit").InnerText == "1")
                        {
                            isInstanceEdit = "Y";

                            Session.Remove("IsInstanceEdit");
                            Session.Add("IsInstanceEdit", isInstanceEdit);
                        }
                    }
                }

                if (recurring.Equals("1")) //in case of editing a recurring conference
                {
                    XmlNode usrnode = xmlDOC.SelectSingleNode("conference/userInfo");
                    if (usrnode != null)
                        usrnode.InnerXml += "<userId>" + Session["userID"].ToString() + "</userId>";

                    string recOutxml = obj.CallMyVRMServer("GetIfDirtyorPast", xmlDOC.InnerXml, Application["MyVRMServer_ConfigPath"].ToString());
                    if (recOutxml != "")
                    {
                        if (recOutxml.IndexOf("<error>") < 0)
                        {
                            xmlDOC.LoadXml(recOutxml);
                            node = null;
                            node = (XmlNode)xmlDOC.DocumentElement;
                        }
                    }
                }
                ConferenceName.Text = node.SelectSingleNode("//conference/confInfo/confName").InnerText;
				
				if (node.SelectSingleNode("//conference/confInfo/IsHDBusy") != null && node.SelectSingleNode("//conference/confInfo/IsHDBusy").InnerText.Equals("1")) //ALLDEV-807
                    hdnIsHDBusy.Value = "1";
                else
                    hdnIsHDBusy.Value = "0";
                ConferenceDescription.Text = utilObj.ReplaceOutXMLSpecialCharacters(node.SelectSingleNode("//conference/confInfo/description").InnerText.Replace("N/A", ""), 1); //FB 2236_s
                txtDescription_Exp.Text = utilObj.ReplaceOutXMLSpecialCharacters(node.SelectSingleNode("//conference/confInfo/description").InnerText.Replace("N/A", ""), 1); //ZD 100570 Inncrewin
                //FB 2501 starts
                txtApprover7.Text = node.SelectSingleNode("//conference/confInfo/lastModifiedByName").InnerText;
                hdnApprover7.Text = node.SelectSingleNode("//conference/confInfo/lastModifiedById").InnerText;
                //FB 2501 ends

                txtApprover4.Text = node.SelectSingleNode("//conference/confInfo/hostName").InnerText;
                hdnApprover4.Text = node.SelectSingleNode("//conference/confInfo/hostId").InnerText;
                txtHost_exp.Text = node.SelectSingleNode("//conference/confInfo/hostName").InnerText;//ZD 101226
                hdntxtHost_exp.Text = node.SelectSingleNode("//conference/confInfo/hostId").InnerText;
                //FB 2501 - Starts
                xconfpassword = node.SelectSingleNode("//conference/confInfo/confPassword").InnerText.Trim();
                ConferencePassword.Attributes.Add("value", xconfpassword);
                ConferencePassword2.Attributes.Add("value", xconfpassword);
                confPassword.Value = xconfpassword;  //FB 2991
                               
                //ALLDEV-826 Starts
                xhostpassword = node.SelectSingleNode("//conference/confInfo/hostPassword").InnerText.Trim();
                txtHostPwd.Attributes.Add("value", xhostpassword);
                txtCfmHostPwd.Attributes.Add("value", xhostpassword);
                hdnHostPassword.Value = xhostpassword;
                //ALLDEV-826 Ends
                //ZD 100522 Starts
                divChangeVMRPIN.Attributes.Add("style", "Display:None");
                if (node.SelectSingleNode("//conference/confInfo/isVMR") != null)
                {
                    if (node.SelectSingleNode("//conference/confInfo/isVMR").InnerText == "2")
                    {
                        txtVMRPIN1.Text = xconfpassword;
                        txtVMRPIN2.Text = xconfpassword;
                    }
                }

                if (VMRPINChange == 1)
                {
                    if (!string.IsNullOrEmpty(txtVMRPIN1.Text))
                    {
                        divChangeVMRPIN.Attributes.Add("style", "Display:");
                        trVMRPIN1.Attributes.Add("style", "Display:");
                        trVMRPIN2.Attributes.Add("style", "Display:");
                    }
                }
                //ZD 100522 Ends
                //ZD 103263 - Starts
                if (node.SelectSingleNode("//conference/confInfo/isBJNConf") != null)
                    hdnisBJNConf.Value = node.SelectSingleNode("//conference/confInfo/isBJNConf").InnerText.Trim();
                else
                    hdnisBJNConf.Value = "0";
                //ZD 103263 - End
                //ZD 104289
                string conferenceCode = "", leaderPin = "", participantCode = "";
                if (node.SelectSingleNode("//conference/confInfo/hostConferenceCode") != null)
                    conferenceCode = node.SelectSingleNode("//conference/confInfo/hostConferenceCode").InnerText.ToString();
                if (node.SelectSingleNode("//conference/confInfo/hostLeaderPin") != null)
                    leaderPin = node.SelectSingleNode("//conference/confInfo/hostLeaderPin").InnerText.ToString();
                if (node.SelectSingleNode("//conference/confInfo/hostParticipantCode") != null)
                    participantCode = node.SelectSingleNode("//conference/confInfo/hostParticipantCode").InnerText.ToString();

                hdnDialString.Value = conferenceCode + "�" + leaderPin + "�" + participantCode;

                //ZD 101015 Starts
                if (node.SelectSingleNode("//conference/confInfo/ConfWebExStatus") != null)
                    int.TryParse(node.SelectSingleNode("//conference/confInfo/ConfWebExStatus").InnerText.ToString(), out EnableWebExConf);

                if (node.SelectSingleNode("//conference/confInfo/hostWebExStatus") != null)
                    hdnHostWebEx.Value = node.SelectSingleNode("//conference/confInfo/hostWebExStatus").InnerText.ToString();

                if (node.SelectSingleNode("//conference/confInfo/schedularWebExStatus") != null)
                    hdnSchdWebEx.Value = node.SelectSingleNode("//conference/confInfo/schedularWebExStatus").InnerText.ToString();
                
                if (EnableWebExIntg == 1 && EnableWebExConf == 1)
                    trWebex.Attributes.Add("style", "display:");
                else
                {
                    trWebex.Attributes.Add("style", "display:none");
                    chkWebex.Checked = false;
                    divWebPW.Attributes.Add("style", "display:none");
                    divWebCPW.Attributes.Add("style", "display:none");
                }
                //ZD 101015 Ends
                //FB 2501 - End
                string confType = node.SelectSingleNode("//conference/confInfo/createBy").InnerText;
                //ZD 100513 Starts
                string OBTPconf = "0";
                if (node.SelectSingleNode("//conference/confInfo/isOBTP") != null)
                    OBTPconf = node.SelectSingleNode("//conference/confInfo/isOBTP").InnerText;
                if (confType == "2" && OBTPconf == "1")
                    confType = "9";
                //ZD 100513 Ends
                if (Request.QueryString["t"] != null)
                {
                    if (Request.QueryString["t"].ToString().Equals("t")) // if the conference is from a template then we need to generate current date and time like new conference
                        confType = node.SelectSingleNode("//conference/confInfo/confType").InnerText;
                }
                if (!client.ToString().ToUpper().Equals(ns_MyVRMNet.vrmClient.MOJ))
                {
                    if (confType.Equals(""))
                        confType = Application["DefaultConferenceType"].ToString();
                }
                else
                    confType = "7"; // 7- Room Conference

                hdnconftype.Value = confType;

                //ZD 100704 Starts
                lstConferenceType.ClearSelection();
                if (lstConferenceType.Items.FindByValue(confType) != null)
                    lstConferenceType.Items.FindByValue(confType).Selected = true;
                //ZD 100704 End
                XmlNodeList nodes = node.SelectNodes("//conference/confInfo/timezones/timezone");
                int length = nodes.Count;
                String selTZ = "-1";
                lstConferenceTZ.ClearSelection();
                obj.GetTimezones(lstConferenceTZ, ref selTZ);

                lstConferenceTZ.Items.FindByValue(node.SelectSingleNode("//conference/confInfo/timeZone").InnerText).Selected = true;
                LoadCommonValues(node);                
                if (node.SelectSingleNode("//conference/confInfo/bufferZone/SetupDur") != null)
                {
                    if (node.SelectSingleNode("//conference/confInfo/bufferZone/SetupDur").InnerText != "")
                    {
                        setupDur = node.SelectSingleNode("//conference/confInfo/bufferZone/SetupDur").InnerText;
                    }
                }

                if (node.SelectSingleNode("//conference/confInfo/bufferZone/TearDownDur") != null)
                {
                    if (node.SelectSingleNode("//conference/confInfo/bufferZone/TearDownDur").InnerText != "")
                    {
                        tearDownDur = node.SelectSingleNode("//conference/confInfo/bufferZone/TearDownDur").InnerText;
                    }
                }
                Double.TryParse(setupDur, out setupDuration);
                Double.TryParse(tearDownDur, out teardownDuration);

                //FB 2634
                SetupDuration.Text = Math.Abs(setupDuration).ToString();
                TearDownDuration.Text = Math.Abs(teardownDuration).ToString();

                //FB 1716 - Start
                double actualDur;
                if (recurring.Equals("1"))
                    Double.TryParse(node.SelectSingleNode("//conference/confInfo/appointmentTime/durationMin").InnerText, out actualDur);
                else
                    Double.TryParse(node.SelectSingleNode("//conference/confInfo/durationMin").InnerText, out actualDur);


                //ZD 100570 Inncrewin 23-12-2013 Start
                try
                {
                    if (Session["confid"] != null) //Added for Template List
                        lstTemplates.Items.FindByValue(Session["confid"].ToString()).Selected = true;
                    else if (Session["defaultConfTemp"].ToString() != "0") //FB 1719 //FB 1746
                        lstTemplates.Items.FindByValue(Session["defaultConfTemp"].ToString()).Selected = true;
                    else
                        lstTemplates.Items[0].Selected = true;
                }
                catch (Exception e)
                { log.Trace(e.Message); }
                //ZD 100570 Inncrewin 23-12-2013 end
                //FB 2634
                //hdnDuration.Value = (actualDur - setupDuration - teardownDuration) +
                //    "&" + setupDuration + "&" + teardownDuration;

                //hdnDuration.Value = (actualDur - setupDuration - teardownDuration).ToString();
                hdnDuration.Value = actualDur.ToString();//ZD 100085

                //FB 1716 - End
                //ZD 100085	Starts
                //if (EnableBufferZone == 1 && (setupDur != "0" || tearDownDur != "0"))
                //{
                //chkEnableBuffer.Checked = true; // New Design Code Review Needed
                //}
                //ZD 100085	End
                if (recurring.Equals("1")) //in case of editing a recurring conference
                {
                    string recurstr = node.SelectSingleNode("//conference/confInfo/appointmentTime").InnerXml;
                    recurstr += node.SelectSingleNode("//conference/confInfo/recurrencePattern").InnerXml;
                    if (node.SelectNodes("//conference/confInfo/recurrenceRange").Count != 0)
                        recurstr += node.SelectSingleNode("//conference/confInfo/recurrenceRange").InnerXml;
                    recurstr = "<recurstr>" + recurstr + "</recurstr>";
                    string tzstr = "<TimeZone>" + xmlDOC.SelectSingleNode("//conference/confInfo/timezones").InnerXml + "</TimeZone>";
                    string rst = obj.AssembleRecur(recurstr, tzstr);
                    string[] rst_array = rst.Split('|');
                    string recur = rst_array[0];

                    string recDtString = "";
                    string tempRec = "";
                    String[] recDateArr = recur.Split('#');
                    recur = "";
                    if (recDateArr.Length > 0)
                    {
                        tempRec = recDateArr[recDateArr.Length - 1];
                        if (tempRec != "")
                        {
                            String[] dtsArr = tempRec.Split('&');
                            if (dtsArr.Length > 0)
                            {
                                for (int lp = 0; lp < dtsArr.Length; lp++)
                                {
                                    if (dtsArr[lp].IndexOf("/") > 0)
                                    {
                                        //ZD 100995 start
                                        DateTime strtdate = DateTime.Parse(dtsArr[lp]);
                                        dtsArr[lp] = strtdate.ToString(format); //ZD 100995 End
                                        //dtsArr[lp] = myVRMNet.NETFunctions.GetFormattedDate(dtsArr[lp]);
                                    }
                                    if (recDtString == "")
                                        recDtString = dtsArr[lp];
                                    else
                                        recDtString += "&" + dtsArr[lp];
                                }
                            }
                        }
                        for (int lp = 0; lp < recDateArr.Length - 1; lp++)
                        {
                            if (recur == "")
                                recur = recDateArr[lp];
                            else
                                recur += "#" + recDateArr[lp];
                        }
                        recur += "#" + recDtString;
                    }
                    Recur.Value = recur;
                    string SelectedTimeZoneName = rst_array[1];
                    DateTime startDateTime = DateTime.Parse(Session["systemDate"].ToString() + " " + Session["systemTime"].ToString());

                    if (Request.QueryString["sd"] != null)
                        startDateTime = DateTime.Parse(Request.QueryString["sd"].ToString() + " " + Request.QueryString["st"].ToString());
                    else
                    {
                        if (startDateTime.Minute < 45)
                            startDateTime = startDateTime.AddMinutes(60 - startDateTime.Minute);
                        else
                            startDateTime = startDateTime.AddMinutes(120 - startDateTime.Minute);
                    }
                    confStartDate.Text = startDateTime.ToString(format);
                    confStartTime.Text = startDateTime.ToString(tformat);
                    DateTime endDateTime = startDateTime.AddMinutes(60);

                    confEndDate.Text = endDateTime.ToString(format);
                    confEndTime.Text = endDateTime.ToString(tformat);
                    string startHour = "0", startMin = "0", startSet = "AM";
                    double duration = 0;

                    if (node.SelectSingleNode("//conference/confInfo/appointmentTime/startHour") != null)
                    {
                        if (node.SelectSingleNode("//conference/confInfo/appointmentTime/startHour").InnerText != "")
                        {
                            startHour = node.SelectSingleNode("//conference/confInfo/appointmentTime/startHour").InnerText;
                        }
                    }
                    if (node.SelectSingleNode("//conference/confInfo/appointmentTime/startMin") != null)
                    {
                        if (node.SelectSingleNode("//conference/confInfo/appointmentTime/startMin").InnerText != "")
                        {
                            startMin = node.SelectSingleNode("//conference/confInfo/appointmentTime/startMin").InnerText;
                        }
                    }
                    if (node.SelectSingleNode("//conference/confInfo/appointmentTime/startSet") != null)
                    {
                        if (node.SelectSingleNode("//conference/confInfo/appointmentTime/startSet").InnerText != "")
                        {
                            startSet = node.SelectSingleNode("//conference/confInfo/appointmentTime/startSet").InnerText;
                        }
                    }

                    string durationMin = node.SelectSingleNode("//conference/confInfo/appointmentTime/durationMin").InnerText;
                    txtModifyType.Text = "1";
                    Double.TryParse(durationMin, out duration);
                    //SetupDate.Text = confStartDate.Text; //FB 2634
                    //FB 2266 - Starts
                    //endDateTime = startDateTime.AddMinutes(duration);
                    //confEndDate.Text = endDateTime.ToString(format);
                    //confEndTime.Text = endDateTime.ToString(tformat);
                    //FB 2266 - Starts
                    //TearDownDate.Text = confEndDate.Text; //FB 2634

                    if (startHour.Trim() == "")
                        startHour = "0";
                    if (startMin.Trim() == "")
                        startMin = "0";
                    if (startSet.Trim() == "")
                        startSet = "AM";
                    //ZD 100085 Starts
                    DateTime setupTime = Convert.ToDateTime(startHour + ":" + startMin + " " + startSet);
                    //setupTime = setupTime.AddMinutes(setupDuration);
                    string sTime = setupTime.ToString(tformat);
                    confStartTime.Text = sTime; //FB 2634
                    confStartTime.SelectedValue = sTime;

                    DateTime endTime = Convert.ToDateTime(startHour + ":" + startMin + " " + startSet);
                    endTime = endTime.AddMinutes(duration);
                    //endTime = endTime.AddMinutes(-teardownDuration);
                    string tTime = endTime.ToString(tformat);
                    //TeardownTime.Text = confEndTime.Text; FB 2634
                    confEndTime.Text = tTime; //FB 2634
                    confEndTime.SelectedValue = tTime;

                    SetupDuration.Text = setupDuration.ToString();
                    TearDownDuration.Text = teardownDuration.ToString();
                    //ZD 100085 End
                    hdnBufferStr.Value = sTime + "&" + tTime;
                    hdnSetupTime.Value = setupDur;
                    hdnTeardownTime.Value = tearDownDur;

                    //FB 2634                   
                }
                else
                {
                    int syear = 0, smonth = 0, sday = 0, sHour = 0, sMin = 0;
                    string sSet = "AM";
                    DateTime startDateTime = DateTime.Now;
                    if (Request.QueryString["t"] != null)
                        if (Request.QueryString["t"].ToString().Equals("t")) // if the conference is from a template then we need to generate current date and time like new conference
                        {
                            startDateTime = DateTime.Parse(DateTime.Now.ToString("MM/dd/yyyy") + " " + DateTime.Now.ToString(tformat));
                            if (client.ToString().ToUpper().Equals(ns_MyVRMNet.vrmClient.MOJ))
                                startDateTime = Convert.ToDateTime(Session["systemDate"].ToString() + " " + Session["systemTime"].ToString());
                            if (startDateTime.Minute < 45)
                                startDateTime = startDateTime.AddMinutes(60 - startDateTime.Minute);
                            else
                                startDateTime = startDateTime.AddMinutes(120 - startDateTime.Minute);

                            syear = startDateTime.Year;
                            smonth = startDateTime.Month;
                            sday = startDateTime.Day;
                            sHour = startDateTime.Hour;
                            sMin = startDateTime.Minute;
                            sSet = startDateTime.ToString("tt").ToUpper();
                        }
                        else // if it is an old conference or if we clone a conference, then we will take the conference information returned by COM
                        {
                            string confstatus = "0";
                            if (node.SelectSingleNode("//conference/confInfo/Status") != null)
                                confstatus = node.SelectSingleNode("//conference/confInfo/Status").InnerText;
                            if (confstatus == "7")
                            {
                                startDateTime = DateTime.Parse(DateTime.Now.ToString("MM/dd/yyyy") + " " + DateTime.Now.ToString(tformat));
                                if (startDateTime.Minute < 45)
                                    startDateTime = startDateTime.AddMinutes(60 - startDateTime.Minute);
                                else
                                    startDateTime = startDateTime.AddMinutes(120 - startDateTime.Minute);

                                syear = startDateTime.Year;
                                smonth = startDateTime.Month;
                                sday = startDateTime.Day;
                                sHour = startDateTime.Hour;
                                sMin = startDateTime.Minute;
                                sSet = startDateTime.ToString("tt").ToUpper();
                            }
                            else
                            {
                                syear = Convert.ToInt16(node.SelectSingleNode("//conference/confInfo/startDate").InnerText.Split('/')[2]);
                                smonth = Convert.ToInt16(node.SelectSingleNode("//conference/confInfo/startDate").InnerText.Split('/')[0]);
                                sday = Convert.ToInt16(node.SelectSingleNode("//conference/confInfo/startDate").InnerText.Split('/')[1]);
                                sHour = Convert.ToInt16(node.SelectSingleNode("//conference/confInfo/startHour").InnerText);
                                sMin = Convert.ToInt16(node.SelectSingleNode("//conference/confInfo/startMin").InnerText);
                                sSet = node.SelectSingleNode("//conference/confInfo/startSet").InnerText;
                                startDateTime = DateTime.Parse(smonth + "/" + sday + "/" + syear + " " + sHour + ":" + sMin + " " + sSet);
                            }
                        }
                    confStartDate.Text = startDateTime.ToString(format);//ZD 100995
                    confStartTime.Text = startDateTime.ToString(tformat);
                    TimeSpan durationMin = new TimeSpan(0, Convert.ToInt32(node.SelectSingleNode("//conference/confInfo/durationMin").InnerText), 0); // * 1000000000 * 60);
                    confEndDate.Text = startDateTime.Add(durationMin).ToString(format); //ZD 100995

                    if (client.ToString().ToUpper().Equals(ns_MyVRMNet.vrmClient.MOJ))
                        confEndTime.Text = (startDateTime.AddMinutes(15)).ToString(tformat);
                    else
                        confEndTime.Text = (startDateTime.AddMinutes(Convert.ToInt32(node.SelectSingleNode("//conference/confInfo/durationMin").InnerText))).ToString(tformat);

                    string durMin = node.SelectSingleNode("//conference/confInfo/durationMin").InnerText;
                    double duration = 0;
                    Double.TryParse(durMin, out duration);

                    confEndTime.Text = startDateTime.AddMinutes(duration).ToString(tformat);
                    //ZD 100085 Starts
                    //setupStartDateTime = startDateTime.AddMinutes(setupDuration);
                    //FB 2634
                    //SetupDate.Text = myVRMNet.NETFunctions.GetFormattedDate(setupStartDateTime);
                    //SetupTime.Text = setupStartDateTime.ToString(tformat);
                    SetupDuration.Text = setupDuration.ToString();
                    //confStartDate.Text = myVRMNet.NETFunctions.GetFormattedDate(setupStartDateTime);
                    //confStartTime.Text = setupStartDateTime.ToString(tformat);
                    TearDownDuration.Text = teardownDuration.ToString();

                    //DateTime endDateTime = startDateTime.AddMinutes(duration);
                    //teardownStartDateTime = endDateTime.AddMinutes(-teardownDuration);

                    //confEndDate.Text = myVRMNet.NETFunctions.GetFormattedDate(teardownStartDateTime);
                    //confEndTime.Text = teardownStartDateTime.ToString(tformat);
                    //ZD 100085	End
                    txtModifyType.Text = "0";
                    CalculateDuration();

                    //FB 2634
                }

                // FB 2620 Starts

                //FB 2376
                //if (node.SelectSingleNode("//conference/confInfo/isVMR") != null)
                //{
                //    if (node.SelectSingleNode("//conference/confInfo/isVMR").InnerText.Equals("1"))
                //        chkVMR.Checked = true;
                //    else
                //        chkVMR.Checked = false;
                //}
                lstVMR.ClearSelection();
                if (node.SelectSingleNode("//conference/confInfo/isVMR") != null)
                    if (lstVMR.Items.FindByValue(node.SelectSingleNode("//conference/confInfo/isVMR").InnerText) != null)
                        lstVMR.SelectedValue = node.SelectSingleNode("//conference/confInfo/isVMR").InnerText.Trim();
                // FB 2620 Ends

                //ZD 103550 - Start
                if (node.SelectSingleNode("//conference/confInfo/BJNMeetingType").InnerText != null)
                    DrpBJNMeetingID.SelectedValue = node.SelectSingleNode("//conference/confInfo/BJNMeetingType").InnerText;

                if (node.SelectSingleNode("//conference/confInfo/isBJNConf").InnerText != null)
                {
                    if (node.SelectSingleNode("//conference/confInfo/isBJNConf").InnerText == "1")
                        chkBJNMeetingID.Checked = true;
                    else
                        chkBJNMeetingID.Checked = false;
                }
                if (EnableBJNSelectOption == "1")
                {
                    HtmlGenericControl DivBJNMeeting = (HtmlGenericControl)Page.FindControl("DivBJNMeetingID");
                    if (chkBJNMeetingID.Checked == true)
                        DivBJNMeeting.Attributes.Add("style", "display:block");
                    else
                        DivBJNMeeting.Attributes.Add("style", "display:none");
                }

              
                //ZD 103550 - End
                //ALLDEV-782 Starts
                if (node.SelectSingleNode("//conference/confInfo/PexipMeetingType").InnerText != null)
                    drpPexipMtgType.SelectedValue = node.SelectSingleNode("//conference/confInfo/PexipMeetingType").InnerText;

                if (node.SelectSingleNode("//conference/confInfo/isPexipConf").InnerText != null)
                {
                    if (node.SelectSingleNode("//conference/confInfo/isPexipConf").InnerText == "1")
                        chkPexipMtgType.Checked = true;
                    else
                        chkPexipMtgType.Checked = false;
                }
                if (EnablePexipSelectOption == "1")
                {
                    HtmlGenericControl DivPexipMeeting = (HtmlGenericControl)Page.FindControl("DivPexipMtgType");
                    if (chkPexipMtgType.Checked == true)
                        DivPexipMeeting.Attributes.Add("style", "display:block");
                    else
                        DivPexipMeeting.Attributes.Add("style", "display:none");
                }
                //ALLDEV-782 Ends

                if (chkBJNMeetingID.Checked || chkPexipMtgType.Checked)
                {
                    trPCConf.Attributes.Add("style", "display:none");
                    trVMR.Attributes.Add("style", "display:none");
                }
                if (lstVMR.SelectedValue == "2" && (chkBJNMeetingID.Checked == true || chkPexipMtgType.Checked == true)) //ALLDEV-782
                    trVMR.Attributes.Add("style", "display:");


                nodes = node.SelectNodes("//conference/confInfo/partys/party");

                getParty(nodes, isClone);//FB 2550
                string advAVParam = node.SelectSingleNode("//conference/confInfo/advAVParam").OuterXml;
                Session.Add("AdvAVParam", advAVParam);

                //ZD 101869 Starts
                string videoLayout = "-1";
                int layoutId = 0;
                if (node.SelectSingleNode("//conference/confInfo/advAVParam/videoLayout") != null)
                    videoLayout = node.SelectSingleNode("//conference/confInfo/advAVParam/videoLayout").InnerText;
                if (videoLayout.Trim().Equals(""))
                    videoLayout = "01";
                int.TryParse(videoLayout, out layoutId);
                //ZD 104357
                if (layoutId > 1 && ShowVideoLayout != 1)
                    isAVSettingsSelected = true;
                txtSelectedImage.Text = layoutId.ToString("00");

                if (node.SelectSingleNode("//conference/confInfo/advAVParam/FamilyLayout") != null)
                    hdnFamilyLayout.Value = node.SelectSingleNode("//conference/confInfo/advAVParam/FamilyLayout").InnerText;
                //ZD 101869 End


                //FB 2377 - Starts
                /*tmpstrs = ((tmpstr = node.SelectSingleNode("//conference/confInfo/ConceirgeSupport").InnerText)).Split(',');//FB 2341
                tmpint = ((tmpstr.Trim()).Equals("")) ? -1 : tmpstrs.Length;
                for (i = 0; i < tmpint; i++)
                    ChklstConcSupport.Items[Convert.ToInt16(tmpstrs[i]) - 1].Selected = true;*/
                //FB 2377 - End

                //FB 2226 - Start
                if (node.SelectSingleNode("//conference/confInfo/publicConf") != null)
                {
                    if (node.SelectSingleNode("//conference/confInfo/publicConf").InnerText.Equals("1"))
                        chkPublic.Checked = true;
                    else
                        chkPublic.Checked = false;
                }
                if (node.SelectSingleNode("//conference/confInfo/dynamicInvite") != null)
                {
                    if (node.SelectSingleNode("//conference/confInfo/dynamicInvite").InnerText.Equals("1"))
                        chkOpenForRegistration.Checked = true;
                    else
                        chkOpenForRegistration.Checked = false;
                }
                //FB 2226 - End
				//ZD 103095
                if (node.SelectSingleNode("//conference/confInfo/isRecurConference") != null && node.SelectSingleNode("//conference/confInfo/isRecurConference").InnerText.Equals("1"))
                    hdnIsRecurConference.Value = "1";
                else
                    hdnIsRecurConference.Value = "0";
                //FB 2717 Start
                if (node.SelectSingleNode("//conference/confInfo/CloudConferencing") != null)
                {
                    if (node.SelectSingleNode("//conference/confInfo/CloudConferencing").InnerText.Equals("1")) //FB 2448
                    {
                        chkCloudConferencing.Checked = true;
                    }
                    else
                        chkCloudConferencing.Checked = false;
                }
                //FB 2717 End
                //ZD 100221 Starts
                if (node.SelectSingleNode("//conference/confInfo/WebExConf") != null)
                {
                    if (node.SelectSingleNode("//conference/confInfo/WebExConf").InnerText.Equals("1")) //FB 2448
                    {
                        if (EnableWebExIntg == 1 && EnableWebExConf == 1) //ZD 101015
                        {
                            chkWebex.Checked = true;

                            if (node.SelectSingleNode("//conference/confInfo/WebExConfPW") != null)
                            {
                                Session.Add("WebExPass", node.SelectSingleNode("//conference/confInfo/WebExConfPW").InnerText.Trim());
                                divWebPW.Attributes.Add("style", "display:block");
                                divWebCPW.Attributes.Add("style", "display:block");
                                txtPW1.Attributes.Add("value", ns_MyVRMNet.vrmPassword.WebEXPW);
                                txtPW2.Attributes.Add("value", ns_MyVRMNet.vrmPassword.WebEXPW);
                            }
                        }
                    }
                    else
                        chkWebex.Checked = false;
                }
                //ZD 100221 Ends
                if (EnableFECC == "1") //YES//FB 2543//FB 2592
                    //if (!chkVMR.Checked)
                    if (lstVMR.SelectedIndex > 0) //FB 2620
                    {
                        tdFECC.Attributes.Add("Style", "display:none;"); //FB 2595
                        chkFECC.Visible = false;
                    }
                    else
                    {
                        tdFECC.Attributes.Add("Style", "display:inline;"); //FB 2595
                        chkFECC.Visible = true;
                    }
                /** FB 2376 **/
                if (node.SelectSingleNode("//advAVParam/internalBridge") != null)
                {
                    txtintbridge.Text = node.SelectSingleNode("//advAVParam/internalBridge").InnerText;
                    hdnintbridge.Value = node.SelectSingleNode("//advAVParam/internalBridge").InnerText;
                }

                if (node.SelectSingleNode("//advAVParam/externalBridge") != null)
                {
                    txtextbridge.Text = node.SelectSingleNode("//advAVParam/externalBridge").InnerText;
                    hdnextbridge.Value = node.SelectSingleNode("//advAVParam/externalBridge").InnerText;
                }

                /** FB 2376 **/

                //ZD 100890 Start

                if (node.SelectSingleNode("//conference/confInfo/EnableStaticID") != null)
                {
                    if (node.SelectSingleNode("//conference/confInfo/EnableStaticID").InnerText.Equals("1"))
                        chkStatic.Checked = true;
                    else
                        chkStatic.Checked = false;
                }

                if (node.SelectSingleNode("//conference/confInfo/StaticID") != null)
                    txtStatic.Text = node.SelectSingleNode("//conference/confInfo/StaticID").InnerText;

                if (!chkStatic.Checked)
                {
                    txtStatic.Text = "";
                    divStaticID.Attributes.Add("Style", "display:none");
                }
                else
                    divStaticID.Attributes.Add("Style", "display:block");

                //ZD 100890 End


                // FB 2583 FECC for EXPRESS Starts
                if (node.SelectSingleNode("//advAVParam/FECCMode").InnerText.Equals("1"))
                    chkFECC.Checked = true;
                else
                    chkFECC.Checked = false;
                // FB 2583 FECC for EXPRESS Ends
                //ZD 104357 - Start
                if (EnableFECC == "1")
                {
                    if (chkFECC.Checked == true)
                        isAVSettingsSelected = true;                    
                }
				//ZD 104357 - End
                //FB 2632 Starts
                if (node.SelectSingleNode("//conference/confInfo/ConciergeSupport/OnSiteAVSupport") != null)
                {
                    if (node.SelectSingleNode("//conference/confInfo/ConciergeSupport/OnSiteAVSupport").InnerText.Equals("1"))
                        chkOnSiteAVSupport.Checked = true;
                    else
                        chkOnSiteAVSupport.Checked = false;
                }

                if (node.SelectSingleNode("//conference/confInfo/ConciergeSupport/MeetandGreet") != null)
                {
                    if (node.SelectSingleNode("//conference/confInfo/ConciergeSupport/MeetandGreet").InnerText.Equals("1"))
                        chkMeetandGreet.Checked = true;
                    else
                        chkMeetandGreet.Checked = false;
                }

                if (node.SelectSingleNode("//conference/confInfo/ConciergeSupport/ConciergeMonitoring") != null)
                {
                    if (node.SelectSingleNode("//conference/confInfo/ConciergeSupport/ConciergeMonitoring").InnerText.Equals("1"))
                        chkConciergeMonitoring.Checked = true;
                    else
                        chkConciergeMonitoring.Checked = false;
                }

                if (node.SelectSingleNode("//conference/confInfo/ConciergeSupport/DedicatedVNOCOperator") != null)
                {
                    if (node.SelectSingleNode("//conference/confInfo/ConciergeSupport/DedicatedVNOCOperator").InnerText.Equals("1"))
                        chkDedicatedVNOCOperator.Checked = true;
                    else
                        chkDedicatedVNOCOperator.Checked = false;
                }
                //FB 2632 Ends

                //FB 2501 Starts FB 2670 START

                ConfVNOCnodes = node.SelectNodes("//conference/confInfo/ConciergeSupport/ConfVNOCOperators/VNOCOperatorID");
                if (ConfVNOCnodes.Count > 0)
                {
                    hdnVNOCOperator.Text = "";
                    for (int i = 0; i < ConfVNOCnodes.Count; i++)
                    {
                        if (ConfVNOCnodes[i].InnerText != null)
                        {
                            if (hdnVNOCOperator.Text == "")
                                hdnVNOCOperator.Text = ConfVNOCnodes[i].InnerText.Trim();
                            else
                                hdnVNOCOperator.Text += "," + ConfVNOCnodes[i].InnerText.Trim();
                        }
                    }
                }
                ConfVNOCnodes = null;
                ConfVNOCnodes = node.SelectNodes("//conference/confInfo/ConciergeSupport/ConfVNOCOperators/VNOCOperator");
                if (ConfVNOCnodes.Count > 0)
                {
                    txtVNOCOperator.Text = "";
                    for (int i = 0; i < ConfVNOCnodes.Count; i++)
                    {
                        {
                            if (ConfVNOCnodes[i].InnerText != null)
                            {
                                if (hdnVNOCOperator.Text == "")
                                    txtVNOCOperator.Text = ConfVNOCnodes[i].InnerText.Trim();
                                else
                                    txtVNOCOperator.Text += "\n" + ConfVNOCnodes[i].InnerText.Trim();
                            }
                        }
                    }
                }
                if (txtVNOCOperator.Text != "")
                    chkDedicatedVNOCOperator.Checked = true;
                //FB 2501 Ends FB 2670 END
                //FB 2608 - Starts
                if (isClone && EnableVNOCselection == 0)
                {
                    txtVNOCOperator.Text = "";//FB 2670
                }
                //FB 2608 - End

                // FB 2693 Starts
                if (node.SelectSingleNode("//conference/confInfo/isPCconference") != null)
                {
                    isPCCOnf.Value = "0";//ZD 100834

                    chkPCConf.Checked = true;
                    if (node.SelectSingleNode("//conference/confInfo/isPCconference").InnerText.Equals("0"))
                    {
                        chkPCConf.Checked = false;
                        CheckPCVendor();
                    }

                    if (node.SelectSingleNode("//conference/confInfo/isPCconference").InnerText.Equals("1"))
                    {
                        isPCCOnf.Value = "1"; //ZD 100834
                        tblPcConf.Style.Add("display", "block");
                        if (node.SelectSingleNode("//conference/confInfo/pcVendorId") != null)
                        {
                            int nodeValue = Int32.Parse(node.SelectSingleNode("//conference/confInfo/pcVendorId").InnerText);
                            switch (nodeValue)
                            {
                                //ZD 104021
                                //case 1:
                                //    if (Session["EnableBlueJeans"].ToString().Equals("1"))
                                //        rdBJ.Checked = true;
                                //    else
                                //        CheckPCVendor();
                                //    break;
                                case 2:
                                    if (Session["EnableJabber"].ToString().Equals("1"))
                                        rdJB.Checked = true;
                                    else
                                        CheckPCVendor();
                                    break;
                                case 3:
                                    if (Session["EnableLync"].ToString().Equals("1"))
                                        rdLync.Checked = true;
                                    else
                                        CheckPCVendor();
                                    break;
                                //ZD 102004
                                //case 4:
                                //    if (Session["EnableVidtel"].ToString().Equals("1"))
                                //        rdVidtel.Checked = true;
                                //    else
                                //        CheckPCVendor();
                                //    break;
                                default:
                                    rdJB.Checked = true;
                                    break;
                            }
                        }
                    }
                }
                // FB 2693 Ends


                if ((Request.QueryString["t"].ToString().Equals("t")))
                {
                    lblConfID.Text = "new";
                    lblConfHeader.Text = obj.GetTranslatedText("Create New");//FB 1830 - Translation
                }
                if ((Request.QueryString["t"].ToString().Equals("o")))
                {
                    lblConfHeader.Text = obj.GetTranslatedText("Create New");//FB 1830 - Translation
                }
                String confStatus = xmlDOC.SelectSingleNode("//conference/confInfo/Status").InnerText;
                txtTimeCheck.Text = "0";
                nodes = xmlDOC.SelectNodes("//conference/confInfo/fileUpload/file");
                GetUploadedFiles(nodes);
                // RefreshRoom(null, null);
                btnRoomSelect_Click(null, null);
                //ifrmPartylist.Attributes["src"] = "settings2partyNET.aspx?wintype=ifr"; //ZD 100834

                if (Request.QueryString["t"].ToString() == "")
                {
                    GetAdvancedAVSettings();
                }
                
                //FB 2426 Start
                XmlNodeList profNodes = null;
                string address = "", connType = "", Password = "", LineRate = "";
                int addType = 0, IsDefault = 0;
                nodes = node.SelectNodes("//conference/confInfo/ConfGuestRooms/ConfGuestRoom"); //ZD 101224
                //ZD 100705
                DataRow dr = null;
                for (int i = 0; i < nodes.Count; i++)
                {
                    //ZD 100705
                    if (i == 0)
                    {
                        CreateDtColumnNames();
                        onflyGrid = obj.LoadDataTable(null, colNames);

                        if (!onflyGrid.Columns.Contains("RowUID"))
                            onflyGrid.Columns.Add("RowUID");
                    }
                    dr = onflyGrid.NewRow();
                    dr["RowUID"] = hdnROWID.Value;//onflyGrid.Rows.Count;
                    int.TryParse(hdnROWID.Value, out  hdnGuestPfRowID);

                    if (nodes[i].SelectSingleNode("RoomID") != null)
                        dr["RoomID"] = nodes[i].SelectSingleNode("RoomID").InnerText.Trim();

                    if (nodes[i].SelectSingleNode("GuestRoomName") != null)
                        dr["RoomName"] = nodes[i].SelectSingleNode("GuestRoomName").InnerText.Trim();

                    if (nodes[i].SelectSingleNode("ContactName") != null)
                        dr["ContactName"] = nodes[i].SelectSingleNode("ContactName").InnerText.Trim();

                    if (nodes[i].SelectSingleNode("ContactEmail") != null)
                        dr["ContactEmail"] = nodes[i].SelectSingleNode("ContactEmail").InnerText.Trim();

                    if (nodes[i].SelectSingleNode("ContactPhoneNo") != null)
                        dr["ContactPhoneNo"] = nodes[i].SelectSingleNode("ContactPhoneNo").InnerText.Trim();

                    if (nodes[i].SelectSingleNode("RoomAddress") != null)
                        dr["ContactAddress"] = nodes[i].SelectSingleNode("RoomAddress").InnerText.Trim();

                    if (nodes[i].SelectSingleNode("GuestContactPhoneNo") != null) //ZD 100619
                        dr["GuestContactPhoneNo"] = nodes[i].SelectSingleNode("GuestContactPhoneNo").InnerText.Trim();

                    if (nodes[i].SelectSingleNode("DftConnecttype") != null) //ZD 100619
                        dr["DftConnecttype"] = nodes[i].SelectSingleNode("DftConnecttype").InnerText.Trim();

                    if (nodes[i].SelectSingleNode("ContactEmail") != null) //ZD 100619
                        dr["GuestAdmin"] = nodes[i].SelectSingleNode("ContactEmail").InnerText.Trim();

                    if (nodes[i].SelectSingleNode("GusetAdminID") != null) //ZD 100619
                        dr["GuestAdminID"] = nodes[i].SelectSingleNode("GusetAdminID").InnerText.Trim();

                    if (nodes[i].SelectSingleNode("State") != null)
                        dr["State"] = nodes[i].SelectSingleNode("State").InnerText.Trim();

                    if (nodes[i].SelectSingleNode("City") != null)
                        dr["City"] = nodes[i].SelectSingleNode("City").InnerText.Trim();

                    if (nodes[i].SelectSingleNode("ZipCode") != null)
                        dr["ZIP"] = nodes[i].SelectSingleNode("ZipCode").InnerText.Trim();

                    if (nodes[i].SelectSingleNode("Country") != null)
                        dr["Country"] = nodes[i].SelectSingleNode("Country").InnerText.Trim();

                    profNodes = nodes[i].SelectNodes("Profiles/Profile");
                    LoadProfiles(profNodes, nodes[i].SelectSingleNode("Profiles/Profile/DefaultProfileID").InnerText, hdnGuestPfRowID);
                    onflyGrid.Rows.Add(dr);
                    hdnROWID.Value = (i + 1).ToString();
                }

                BindOptionData();

                if (nodes.Count == 0)
                    LoadGuestEndpointProfile(0);
                //dgOnflyGuestRoomlist.Visible = true;//Doubt 100619
                //FB 2426 end
                if (node.SelectSingleNode("//advAVParam/maxLineRateID") != null) //FB 2394
                    if (node.SelectSingleNode("//advAVParam/maxLineRateID").InnerText.Trim() != "")
                    {
                        lstLineRate.ClearSelection();
                        lstLineRate.SelectedValue = node.SelectSingleNode("//advAVParam/maxLineRateID").InnerText;
                    }
                //ZD 100522 Starts
                if (lstLineRate.SelectedValue == "-1") 
                {
                    lstLineRate.ClearSelection();
                    lstLineRate.SelectedValue = OrgLineRate.ToString();
                }
                //ZD 100522 Ends
                //FB 2501 starts
                if (node.SelectSingleNode("//conference/confInfo/StartMode").InnerText != null)
                    lstStartMode.SelectedValue = node.SelectSingleNode("//conference/confInfo/StartMode").InnerText;
                //FB 2501 Ends

                //FB 2595 - Starts //FB 2993 Starts
                drpNtwkClsfxtn.ClearSelection();
                if (node.SelectSingleNode("//conference/confInfo/Secured") != null)
                    if (drpNtwkClsfxtn.Items.FindByValue(node.SelectSingleNode("//conference/confInfo/Secured").InnerText) != null)
                        drpNtwkClsfxtn.SelectedValue = node.SelectSingleNode("//conference/confInfo/Secured").InnerText;
                //FB 2595 End //FB 2993 Ends
                //FB 2870 Start
                if (node.SelectSingleNode("//conference/confInfo/EnableNumericID") != null)
                {
                    if (node.SelectSingleNode("//conference/confInfo/EnableNumericID").InnerText.Equals("1"))
                        ChkEnableNumericID.Checked = true;
                    else
                        ChkEnableNumericID.Checked = false;
                }

                if (node.SelectSingleNode("//conference/confInfo/CTNumericID") != null)
                    txtNumeridID.Text = node.SelectSingleNode("//conference/confInfo/CTNumericID").InnerText;

                HtmlGenericControl MyDiv = (HtmlGenericControl)Page.FindControl("DivNumeridID");

                if (ChkEnableNumericID.Checked)
                    MyDiv.Attributes.Add("style", "display:block");
                else
                    MyDiv.Attributes.Add("style", "display:none");

                //FB 2870 End

                //FB 2998
                if (node.SelectSingleNode("//conference/confInfo/McuSetupTime") != null)
                    txtMCUConnect.Text = node.SelectSingleNode("//conference/confInfo/McuSetupTime").InnerText;
                
                if (mcuSetupDisplay == "0")
                    txtMCUConnect.Text = Session["McuSetupTime"].ToString();
                //ZD 104357
                if (txtMCUConnect.Text != "0" && txtMCUConnect.Text != Session["McuSetupTime"].ToString() && mcuSetupDisplay == "1")
                    isAVSettingsSelected = true;

                if (node.SelectSingleNode("//conference/confInfo/MCUTeardonwnTime") != null)
                    txtMCUDisConnect.Text = node.SelectSingleNode("//conference/confInfo/MCUTeardonwnTime").InnerText;

                if (mcuTearDisplay == "0")
                    txtMCUDisConnect.Text = Session["MCUTeardonwnTime"].ToString();

                //ZD 104357
                if (txtMCUDisConnect.Text != "0" && txtMCUDisConnect.Text != Session["MCUTeardonwnTime"].ToString() && mcuTearDisplay == "1")
                    isAVSettingsSelected = true;

               
            }
            catch (Exception ex)
            {
                errLabel.Visible = true;
                log.Trace("BindDataOld: " + ex.Message);
            }
        }
        #endregion

        #region BindRecurrenceDefault

        private void BindRecurrenceDefault()
        {
            try
            {

                if (!IsPostBack)
                {
                    Int32 r = 0;
                    //ZD 103929
                    //foreach (myVRMNet.NETFunctions.RecurringPattern myValue in Enum.GetValues(typeof(myVRMNet.NETFunctions.RecurringPattern)))
                    //{
                    //    ListItem li = new ListItem(obj.GetTranslatedText(myValue.ToString()), ((Int32)myValue).ToString());//FB 2272 - Starts
                    //    RecurType.Items.Insert(r++, li);
                    //}

                    r = 0;
                    foreach (myVRMNet.NETFunctions.WeekDays myValue in Enum.GetValues(typeof(myVRMNet.NETFunctions.WeekDays)))
                    {
                        ListItem li = new ListItem(obj.GetTranslatedText(myValue.ToString()), ((Int32)myValue).ToString());//FB 2272
                        WeekDay.Items.Insert(r++, li);
                    }

                    //WeekDay.DataSource = Enum.GetNames(typeof(myVRMNet.NETFunctions.WeekDays));
                    //WeekDay.DataBind();

                    r = 0;
                    foreach (myVRMNet.NETFunctions.DayCount myValue in Enum.GetValues(typeof(myVRMNet.NETFunctions.DayCount)))
                    {
                        ListItem li = new ListItem(obj.GetTranslatedText(myValue.ToString()), ((Int32)myValue).ToString());//FB 2272
                        MonthWeekDayNo.Items.Insert(r++, li);
                    }
                    //MonthWeekDayNo.DataSource = Enum.GetNames(typeof(myVRMNet.NETFunctions.DayCount));
                    //MonthWeekDayNo.DataBind();

                    r = 0;
                    foreach (myVRMNet.NETFunctions.DaysType myValue in Enum.GetValues(typeof(myVRMNet.NETFunctions.DaysType)))
                    {
                        ListItem li = new ListItem(obj.GetTranslatedText(myValue.ToString()), ((Int32)myValue).ToString());//FB 2272
                        MonthWeekDay.Items.Insert(r++, li);
                    }
                    //MonthWeekDay.DataSource = Enum.GetNames(typeof(myVRMNet.NETFunctions.DaysType));
                    //MonthWeekDay.DataBind();

                    r = 0;
                    foreach (myVRMNet.NETFunctions.DayCount myValue in Enum.GetValues(typeof(myVRMNet.NETFunctions.DayCount)))
                    {
                        ListItem li = new ListItem(obj.GetTranslatedText(myValue.ToString()), ((Int32)myValue).ToString());//FB 2272
                        YearMonthWeekDayNo.Items.Insert(r++, li);
                    }
                    //YearMonthWeekDayNo.DataSource = Enum.GetNames(typeof(myVRMNet.NETFunctions.DayCount));
                    //YearMonthWeekDayNo.DataBind();

                    r = 0;
                    foreach (myVRMNet.NETFunctions.DaysType myValue in Enum.GetValues(typeof(myVRMNet.NETFunctions.DaysType)))
                    {
                        ListItem li = new ListItem(obj.GetTranslatedText(myValue.ToString()), ((Int32)myValue).ToString());//FB 2272
                        YearMonthWeekDay.Items.Insert(r++, li);
                    }
                    //YearMonthWeekDay.DataSource = Enum.GetNames(typeof(myVRMNet.NETFunctions.DaysType));
                    //YearMonthWeekDay.DataBind();

                    r = 0;
                    foreach (myVRMNet.NETFunctions.MonthNames myValue in Enum.GetValues(typeof(myVRMNet.NETFunctions.MonthNames)))
                    {
                        ListItem li = new ListItem(obj.GetTranslatedText(myValue.ToString()), ((Int32)myValue).ToString());//FB 2272
                        YearMonth1.Items.Insert(r++, li);
                    }
                    //YearMonth1.DataSource = Enum.GetNames(typeof(myVRMNet.NETFunctions.MonthNames));
                    //YearMonth1.DataBind();

                    r = 0;
                    foreach (myVRMNet.NETFunctions.MonthNames myValue in Enum.GetValues(typeof(myVRMNet.NETFunctions.MonthNames)))
                    {
                        ListItem li = new ListItem(obj.GetTranslatedText(myValue.ToString()), ((Int32)myValue).ToString());//FB 2272
                        YearMonth2.Items.Insert(r++, li);
                    }
                    //YearMonth2.DataSource = Enum.GetNames(typeof(myVRMNet.NETFunctions.MonthNames));
                    //YearMonth2.DataBind();
                    //FB 2272 - End
                }
            }
            catch (Exception ex)
            {
                log.Trace("BindRecurrenceDefault : " + ex.Message);
            }
        }

        #endregion

        #region Hide recur button
        /// <summary>
        /// To hide the recur button on instance edit
        /// </summary>
        protected void HideRecurButton()
        {
            try
            {
                isInstanceEdit = "";
                if (Session["IsInstanceEdit"] != null)
                {
                    if (Session["IsInstanceEdit"].ToString() != "")
                        isInstanceEdit = Session["IsInstanceEdit"].ToString();
                }
            }
            catch (Exception ex)
            {
                log.Trace("Hide recur button : " + ex.Message);
            }
        }

        #endregion

        #region SetEndTime
        private void SetEndTime()
        {
            try
            {
                //FB 2634
                //if (Recur.Value.Trim().Equals(""))
                //{
                //    log.Trace("In SetEndTime");
                //    DateTime t = Convert.ToDateTime(myVRMNet.NETFunctions.GetDefaultDate(SetupDate.Text) + " " + SetupTime.Text); 
                //    DateTime et = Convert.ToDateTime(myVRMNet.NETFunctions.GetDefaultDate(confEndDate.Text) + " " + confEndTime.Text);
                //    if (et <= t)
                //    {
                //        t = t.AddHours(1.00);
                //        if (client.ToString().ToUpper().Equals(ns_MyVRMNet.vrmClient.MOJ)) 
                //            t = t.AddMinutes(15);
                //        confEndDate.Text = myVRMNet.NETFunctions.GetFormattedDate(t);
                //        confEndTime.Text = t.ToString(tformat);
                //    }
                //    CalculateDuration();
                //}
            }
            catch (Exception ex)
            {
                log.Trace("Error SetEndTime: " + ex.Message);
                DisplayDialog("Invalid Start Date/Time.");
            }
        }
        #endregion

        #region confStartTime_TextChanged
        protected void confStartTime_TextChanged(object sender, EventArgs e)
        {
            try
            {
                log.Trace("In confStartTime_TextChanged");
                SetEndTime();
            }
            catch (Exception ex)
            {
                log.Trace("Error confStartTime_TextChanged: " + ex.Message);
            }
        }
        #endregion

        #region confStartTime_SelectedIndexChanged
        protected void confStartTime_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                log.Trace("In confStartTime_SelectedIndexChanged");
                SetEndTime();
            }
            catch (Exception ex)
            {
                log.Trace("Error confStartTime_SelectedIndexChanged : " + ex.Message);
            }
        }
        #endregion

        #region confEndTime_SelectedIndexChanged

        protected void confEndTime_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                StartLessThanEndTime();
            }
            catch (Exception ex)
            {
                log.Trace("confEndTime_SelectedIndexChanged : " + ex.Message);
            }
        }

        #endregion

        #region confEndTime_TextChanged

        protected void confEndTime_TextChanged(object sender, EventArgs e)
        {
            try
            {
                StartLessThanEndTime();
            }
            catch (Exception ex)
            {
                log.Trace("confEndTime_TextChanged : " + ex.Message);
            }
        }

        #endregion

        #region StartLessThanEndTime
        private void StartLessThanEndTime()
        {
            try
            {
                log.Trace("In StartLessThenEndTime");
                //FB 2634
                //DateTime tStart = Convert.ToDateTime(myVRMNet.NETFunctions.GetDefaultDate(SetupDate.Text) + " " + SetupTime.Text); //FB 2634
                //DateTime tEnd = Convert.ToDateTime(myVRMNet.NETFunctions.GetDefaultDate(confEndDate.Text) + " " + confEndTime.Text);
                //log.Trace(tStart.ToLongDateString() + " : " + tEnd.ToLongDateString());
                //if (tStart >= tEnd)
                //{
                //    log.Trace("in if");
                //    DisplayDialog("End time will be changed because it should be greater than start time.");
                //    ResetEndTime();
                //    confEndTime.Focus();
                //}
            }
            catch (Exception ex)
            {
                log.Trace("Error StartLessThrenEndTime : " + ex.Message);
                DisplayDialog("Invalid Date/Time.");
                ResetEndTime();
                confEndTime.Focus();
            }
        }
        #endregion

        #region ResetEndTime
        private void ResetEndTime()
        {
            try
            {
                log.Trace("In ResetEndTime");
                DateTime tReset = Convert.ToDateTime(confStartDate.Text + " " + myVRMNet.NETFunctions.ChangeTimeFormat(confStartTime.Text)); //FB 2588
                tReset = tReset.AddHours(1.00);
                confEndDate.Text = myVRMNet.NETFunctions.GetFormattedDate(tReset);
                confEndTime.Text = tReset.ToString(tformat);
                CalculateDuration();
            }
            catch (Exception ex)
            {
                log.Trace("ResetEndTime: : " + ex.Message);
                DisplayDialog("Invalid Date/Time.");
            }
        }
        #endregion

        #region CalculateDuration
        /// <summary>
        /// To calculate Conference Duration
        /// </summary>
        public void CalculateDuration()
        {
            try
            {
                log.Trace("In CalculateDuration");
                TimeSpan durationMin = DateTime.Parse(myVRMNet.NETFunctions.GetDefaultDate(confEndDate.Text) + " " + confEndTime.Text).Subtract(DateTime.Parse(myVRMNet.NETFunctions.GetDefaultDate(confStartDate.Text) + " " + myVRMNet.NETFunctions.ChangeTimeFormat(confStartTime.Text))); //FB 2588
                
                lblConfDuration.Text = "";
                if (durationMin.TotalMinutes < 0)
                    lblConfDuration.Text = obj.GetTranslatedText("Invalid duration");//FB 1830 - Translation

                if (Math.Floor(durationMin.TotalDays) > 0)
                    lblConfDuration.Text = Math.Floor(durationMin.TotalDays) + " days ";
                if (Math.Floor(durationMin.TotalHours) > 0)
                    lblConfDuration.Text += durationMin.Hours + " hrs ";
                if (durationMin.Minutes > 0)
                    lblConfDuration.Text += durationMin.Minutes + " mins";
            }
            catch (Exception ex)
            {
                log.Trace("CalculateDuration : " + ex.Message);
                lblConfDuration.Text = obj.GetTranslatedText("Invalid duration");//FB 1830 - Translation
            }
        }

        #endregion

        #region  Get Party details
		//ZD 100834 Starts
        private void getParty(XmlNodeList nodes, bool isClone) //FB 2550
        {
            string vmrParty = "0";
            string pcParty = "0";
            string address = "0";
            int pLength = nodes.Count;
            partysInfo = "";
            string partyroomid = ""; //ZD 102916
            for (int i = 0; i < pLength; i++)
            {
                //FB 2550
                if ((isClone && nodes[i].SelectSingleNode("partyPublicVMR").InnerText.Equals("1")) || (nodes[i].SelectSingleNode("AudioBridgeParty") != null && nodes[i].SelectSingleNode("AudioBridgeParty").InnerText.Equals("1"))) //ZD 100834
                    continue;
                
                vmrParty = "0";
                pcParty = "0";

                if (nodes[i].SelectSingleNode("partyAddress").InnerText.Trim() != "") //FB 2376
                    address = nodes[i].SelectSingleNode("partyAddress").InnerText.Trim();

                partysInfo += nodes[i].SelectSingleNode("partyID").InnerText + "!!";
                partysInfo += nodes[i].SelectSingleNode("partyFirstName").InnerText + "!!"; //FB 1640 .Replace(",", " ") 

                if (nodes[i].SelectSingleNode("partyLastName").InnerText.Trim() == "")//FB 2023
                    partysInfo += " !!"; //FB 2388
                else
                    partysInfo += nodes[i].SelectSingleNode("partyLastName").InnerText + "!!";

                partysInfo += nodes[i].SelectSingleNode("partyEmail").InnerText + "!!";

                if (nodes[i].SelectSingleNode("partyInvite").InnerText.Equals("0")) //CC
                    partysInfo += "0!!0!!1!!";
                else if (nodes[i].SelectSingleNode("partyInvite").InnerText.Equals("1")) //External Attendee
                    partysInfo += "1!!0!!0!!";
                else if (nodes[i].SelectSingleNode("partyInvite").InnerText.Equals("2")) //Room attendee
                    partysInfo += "0!!1!!0!!";
                else if (nodes[i].SelectSingleNode("partyInvite").InnerText.Equals("4")) //VMR 2376
                {
                    vmrParty = "1";
                    if (lstVMR.SelectedIndex > 0)
                        partysInfo += "0!!0!!0!!";
                    else
                    {
                        vmrParty = "0";
                        partysInfo += "0!!1!!0!!";
                    }
                }
                else if (nodes[i].SelectSingleNode("partyInvite").InnerText.Equals("3"))
                {
                    pcParty = "1";
                    partysInfo += "0!!0!!0!!";

                }
                if (nodes[i].SelectSingleNode("partyNotify").InnerText.Equals("1"))
                    partysInfo += "1!!";
                else
                    partysInfo += "0!!";
                
                if (nodes[i].SelectSingleNode("partyAudVid").InnerText.Equals("2")) //AudioVideo //Code changed for FB 1744
                    partysInfo += "1!!0!!";
                else if (nodes[i].SelectSingleNode("partyAudVid").InnerText.Equals("1")) //Audio only //Code changed for FB 1744
                    partysInfo += "0!!1!!";
                else
                    partysInfo += "0!!0!!"; //None
                
                if (nodes[i].SelectNodes("partyAddressType").Count > 0)
                {
                    partysInfo += nodes[i].SelectSingleNode("partyProtocol").InnerText + "!!";
                    partysInfo += nodes[i].SelectSingleNode("partyConnectionType").InnerText + "!!";
                    partysInfo += address + "!!";//FB 2376
                    partysInfo += nodes[i].SelectSingleNode("partyAddressType").InnerText + "!!";
                    partysInfo += nodes[i].SelectSingleNode("partyIsOutside").InnerText + "!!";
                }
                else
                    partysInfo += "!!0!!-3!!0!!0!!0!!"; //FB 1888 end  2376
                if (nodes[i].SelectSingleNode("Survey").InnerText.Equals("1"))
                    partysInfo += "0!!1!!";
                else
                    partysInfo += "0!!0!!";

                partysInfo += pcParty + "!!" + vmrParty;
                //FB 2550 Starts  //ZD 102916 Starts
                if (nodes[i].SelectSingleNode("partyPublicVMR").InnerText.Equals("1"))
                    partysInfo += "!!1$";
                else
                    partysInfo += "!!0$";
                //FB 2550 Ends
                if (!string.IsNullOrEmpty(nodes[i].SelectSingleNode("partyRoomID").InnerText))
                    partyroomid = nodes[i].SelectSingleNode("partyRoomID").InnerText;
                else
                    partyroomid = "0";

                partysInfo += partyroomid + "||";
                //ZD 102916 Ends
				//ZD 100834 End
                txtPartysInfo.Text = partysInfo;
            }
            
        }
        #endregion

        #region GetUploadedFiles

        protected void GetUploadedFiles(XmlNodeList nodes)
        {
            try
            {
                int i = 0;
                foreach (XmlNode node in nodes)
                    if (!node.InnerText.Equals(""))
                    {
                        i++;
                        String fPath = node.InnerText;
                        if (node.InnerText != "")
                        {
                            fPath = fPath.Replace("\\", "/");
                            int startIndex = fPath.IndexOf("/" + language + "/");//FB 1830
                            int len = fPath.Length - 1;
                            fPath = Request.Url.ToString().Substring(0, Request.Url.ToString().IndexOf("/" + language + "/") + 3) + fPath.Substring(startIndex + 3);//FB 1830
                            switch (i)
                            {
                                case 1:
                                    lblUpload1.Text = "<a href='" + fPath + "' target='_blank'>" + getUploadFilePath(node.InnerText) + "</a>";
                                    hdnUpload1.Text = node.InnerText;
                                    btnRemove1.Visible = true;
                                    FileUpload1.Visible = false;
                                    lblUpload1.Visible = true;
                                    aFileUp.Text = obj.GetTranslatedText("Show Uploaded Files");//FB 1830 - Translation
                                    break;
                                case 2:
                                    lblUpload2.Text = "<a href='" + fPath + "' target='_blank'>" + getUploadFilePath(node.InnerText) + "</a>";
                                    hdnUpload2.Text = node.InnerText;
                                    btnRemove2.Visible = true;
                                    FileUpload2.Visible = false;
                                    lblUpload2.Visible = true;
                                    aFileUp.Text = obj.GetTranslatedText("Show Uploaded Files");//FB 1830 - Translation
                                    break;
                                case 3:
                                    lblUpload3.Text = "<a href='" + fPath + "' target='_blank'>" + getUploadFilePath(node.InnerText) + "</a>";
                                    hdnUpload3.Text = node.InnerText;
                                    btnRemove3.Visible = true;
                                    FileUpload3.Visible = false;
                                    lblUpload3.Visible = true;
                                    aFileUp.Text = obj.GetTranslatedText("Show Uploaded Files");//FB 1830 - Translation
                                    break;
                            }
                        }
                    }
                GetFileList();
            }
            catch (Exception ex)
            {
                log.Trace("GetUploadedFiles : " + ex.Message);
            }
        }

        #endregion

        #region getUploadFilePath
        protected string getUploadFilePath(string fpn)
        {
            String fPath = String.Empty;
            if (fpn.Equals(""))
                fPath = "";
            else
            {
                char[] splitter = { '\\' };
                String[] fa = fpn.Split('\\');
                if (fa.Length.Equals(0))
                    fPath = "";
                else
                    fPath = fa[fa.Length - 1];
            }
            return fPath;
        }
        #endregion

        #region RemoveFile

        protected void RemoveFile(Object sender, CommandEventArgs e)
        {
            try
            {
                Label lblTemp = new Label();
                HtmlInputFile inTemp = new HtmlInputFile();
                Label lblFname = new Label();
                Label lblHdnName = new Label();
                Button btnTemp = new Button();
                switch (e.CommandArgument.ToString())
                {
                    case "1":
                        lblTemp = hdnUpload1;
                        inTemp = FileUpload1;
                        lblFname = lblUpload1;
                        lblHdnName = hdnUpload1;
                        btnTemp = btnRemove1;
                        aFileUp.Text = obj.GetTranslatedText("Attach Files");//FB 1830 - Translation
                        txtFileUpload1.Value = obj.GetTranslatedText("No file selected"); //ZD 102330 
                        break;
                    case "2":
                        lblTemp = hdnUpload2;
                        inTemp = FileUpload2;
                        lblFname = lblUpload2;
                        lblHdnName = hdnUpload2;
                        btnTemp = btnRemove2;
                        aFileUp.Text = obj.GetTranslatedText("Attach Files");//FB 1830 - Translation
                        txtFileUpload2.Value = obj.GetTranslatedText("No file selected"); //ZD 102330 
                        break;
                    case "3":
                        lblTemp = hdnUpload3;
                        inTemp = FileUpload3;
                        lblFname = lblUpload3;
                        lblHdnName = hdnUpload3;
                        btnTemp = btnRemove3;
                        aFileUp.Text = obj.GetTranslatedText("Attach Files");//FB 1830 - Translation
                        txtFileUpload3.Value = obj.GetTranslatedText("No file selected"); //ZD 102330 
                        break;
                }
                if (lblTemp.Text != "")
                {
                    FileInfo fi = new FileInfo(lblTemp.Text);
                    if (fi.Exists)
                        fi.Delete();
                    inTemp.Visible = true;
                    inTemp.Disabled = false;
                    lblFname.Visible = false;
                    lblFname.Text = "";
                    lblHdnName.Visible = false;
                    lblHdnName.Text = "";
                    btnTemp.Visible = false;
                }
            }
            catch (Exception ex)
            {
                log.Trace("RemoveFile : " + ex.Message);
            }
        }

        #endregion

        #region UploadFiles

        protected void UploadFiles(Object sender, EventArgs e)
        {
            try
            {
                String fName;
                HttpPostedFile myFile;
                int nFileLen;
                byte[] myData;
                String errMsg = "";

                //ZD 100263
                string filextn = "";
                string[] strExtension = null;
                string[] strSplitExtension = null;//ZD 101052
                bool filecontains = false;
                string uploadFileExtn = "";
                int EnableFileWhiteList = 0;
                if (Session["EnableFileWhiteList"] != null && !string.IsNullOrEmpty(Session["EnableFileWhiteList"].ToString()))
                {
                    int.TryParse(Session["EnableFileWhiteList"].ToString(), out EnableFileWhiteList);
                }

                if (Session["FileWhiteList"] != null && !string.IsNullOrEmpty(Session["FileWhiteList"].ToString()))
                {
                    uploadFileExtn = Session["FileWhiteList"].ToString();
                }

                if (!FileUpload1.Value.Equals(""))
                {
                    fName = Path.GetFileName(FileUpload1.Value);

                    //ZD 100263 Starts
                    if (EnableFileWhiteList > 0 && uploadFileExtn != "")
                    {
						//ZD 101052 Starts
                        strExtension = uploadFileExtn.Split(';');
                        strSplitExtension = fName.Split('.');
                        if (strSplitExtension.Length > 2)
                        {
                            errLabel.Text = obj.GetTranslatedText("File type is invalid. Please select a new file and try again.");
                            errLabel.Visible = true;
                            return;
                        }

                        filextn = Path.GetExtension(fName);
                        filextn = filextn.Replace(".", "");
						//ZD 101052 End
                        filecontains = strExtension.Contains(filextn, StringComparer.OrdinalIgnoreCase);
                        if (!filecontains)
                        {
                            errLabel.Text = obj.GetTranslatedText("File type is invalid. Please select a new file and try again.");
                            errLabel.Visible = true;
                            return;
                        }
                    }
                    //ZD 100263 End

                    myFile = FileUpload1.PostedFile;
                    nFileLen = myFile.ContentLength;
                    myData = new byte[nFileLen];
                    myFile.InputStream.Read(myData, 0, nFileLen);
                    if (nFileLen <= 10000000)
                    {
                        WriteToFile(HttpContext.Current.Request.MapPath(".").ToString() + "\\upload\\" + fName, ref myData);
                        FileUpload1.Disabled = true;
                        lblUpload1.Text = fName;
                        hdnUpload1.Text = HttpContext.Current.Request.MapPath(".").ToString() + "\\upload\\" + fName;
                        FileUpload1.Visible = false;
                        lblUpload1.Visible = true;
                        btnRemove1.Visible = true;
                        aFileUp.Text = obj.GetTranslatedText("Show Uploaded Files");//FB 1830 - Translation
                    }
                    else
                        errMsg += "Attachment 1 is greater than 10MB. File has not been uploaded.";
                }

                if (!FileUpload2.Value.Equals(""))
                {
                    fName = Path.GetFileName(FileUpload2.Value);

                    //ZD 100263 Starts
                    if (EnableFileWhiteList > 0 && uploadFileExtn != "")
                    {
						//ZD 101052 Starts
                        strExtension = uploadFileExtn.Split(';');
                        strSplitExtension = fName.Split('.');
                        if (strSplitExtension.Length > 2)
                        {
                            errLabel.Text = obj.GetTranslatedText("File type is invalid. Please select a new file and try again.");
                            errLabel.Visible = true;
                            return;
                        }
                        
                        filextn = Path.GetExtension(fName);
                        filextn = filextn.Replace(".", "");
						//ZD 101052 End
                        filecontains = strExtension.Contains(filextn, StringComparer.OrdinalIgnoreCase);
                        if (!filecontains)
                        {
                            errLabel.Text = obj.GetTranslatedText("File type is invalid. Please select a new file and try again.");
                            errLabel.Visible = true;
                            return;
                        }
                    }
                    //ZD 100263 End

                    myFile = FileUpload2.PostedFile;
                    nFileLen = myFile.ContentLength;
                    myData = new byte[nFileLen];
                    myFile.InputStream.Read(myData, 0, nFileLen);
                    if (FileUpload2.Value.Equals(FileUpload1.Value))
                        errMsg += "Attachment 2 has already been uploaded.";
                    else
                    {
                        if (nFileLen <= 10000000)
                        {
                            WriteToFile(HttpContext.Current.Request.MapPath(".").ToString() + "\\upload\\" + fName, ref myData);
                            FileUpload2.Disabled = true;
                            lblUpload2.Text = fName;
                            hdnUpload2.Text = HttpContext.Current.Request.MapPath(".").ToString() + "\\upload\\" + fName;
                            FileUpload2.Visible = false;
                            lblUpload2.Visible = true;
                            btnRemove2.Visible = true;
                            aFileUp.Text = obj.GetTranslatedText("Show Uploaded Files");//FB 1830 - Translation
                        }
                        else
                            errMsg += "Attachment 3 is greater than 10MB. File has not been uploaded.";
                    }
                }

                if (!FileUpload3.Value.Equals(""))
                {
                    fName = Path.GetFileName(FileUpload3.Value);

                    //ZD 100263 Starts
                    if (EnableFileWhiteList > 0 && uploadFileExtn != "")
                    {
						//ZD 101052 Starts
                        strExtension = uploadFileExtn.Split(';');

                        strSplitExtension = fName.Split('.');
                        if (strSplitExtension.Length > 2)
                        {
                            errLabel.Text = obj.GetTranslatedText("File type is invalid. Please select a new file and try again.");
                            errLabel.Visible = true;
                            return;
                        }
                        
                        filextn = Path.GetExtension(fName);
                        filextn = filextn.Replace(".", "");
						//ZD 101052 End
                        filecontains = strExtension.Contains(filextn, StringComparer.OrdinalIgnoreCase);
                        if (!filecontains)
                        {
                            errLabel.Text = obj.GetTranslatedText("File type is invalid. Please select a new file and try again.");
                            errLabel.Visible = true;
                            return;
                        }
                    }
                    //ZD 100263 End

                    myFile = FileUpload3.PostedFile;
                    nFileLen = myFile.ContentLength;
                    myData = new byte[nFileLen];
                    myFile.InputStream.Read(myData, 0, nFileLen);
                    if (FileUpload3.Value.Equals(FileUpload1.Value) || FileUpload3.Value.Equals(FileUpload2.Value))
                        errMsg += "Attachment 3 has already been uploaded.";
                    else
                    {
                        if (nFileLen <= 10000000)
                        {
                            WriteToFile(HttpContext.Current.Request.MapPath(".").ToString() + "\\upload\\" + fName, ref myData);
                            FileUpload3.Disabled = true;
                            hdnUpload3.Text = HttpContext.Current.Request.MapPath(".").ToString() + "\\upload\\" + fName;
                            lblUpload3.Text = fName;
                            lblUpload3.Visible = true;
                            FileUpload3.Visible = false;
                            btnRemove3.Visible = true;
                            aFileUp.Text = obj.GetTranslatedText("Show Uploaded Files");//FB 1830 - Translation
                        }
                        else
                            errMsg += "Attachment 3 is greater than 10MB. File has not been uploaded.";
                    }
                }
                     //ZD 102330 start
                    txtFileUpload1.Value = obj.GetTranslatedText("No file selected");
                    txtFileUpload2.Value = obj.GetTranslatedText("No file selected");
                    txtFileUpload3.Value = obj.GetTranslatedText("No file selected");
                    //ZD 102330 End

                if (FileUpload1.Value.Equals("") && FileUpload2.Value.Equals("") && FileUpload3.Value.Equals(""))
                {
                    if (lblFileList.Text.Trim() == "")
                        aFileUp.Text = obj.GetTranslatedText("Attach Files");//FB 1830 - Translation
                }

                GetFileList();

                if (lblFileList.Text.Trim() != "")
                    aFileUp.Text = obj.GetTranslatedText("Show Uploaded Files");//FB 1830 - Translation
            }
            catch (Exception ex)
            {
                log.Trace("Error in uploading files. : " + ex.Message);
                errLabel.Visible = true;
            }
        }

        #endregion

        #region WriteToFile

        private void WriteToFile(string strPath, ref byte[] Buffer)
        {
            try
            {
                // Create a file
                FileStream newFile = new FileStream(strPath, FileMode.Create);

                // Write data to the file
                newFile.Write(Buffer, 0, Buffer.Length);

                // Close file
                newFile.Close();
            }
            catch (Exception ex)
            {
                log.Trace("WriteToFile : " + ex.Message);
            }
        }

        #endregion

        #region DisplayDialog
        protected void DisplayDialog(string strDisplayMessage)
        {
            try
            {
                string script = "<Script language='javascript'>";
                script += "callalert('" + strDisplayMessage + "')";
                script += "</script>";
                Literal literal = new Literal();
                literal.Text = script;
                Page.FindControl("frmSettings2").Controls.Add(literal);
            }
            catch (Exception ex)
            {
                log.Trace("DisplayDialog: " + ex.Message);
            }
        }

        #endregion

        #region rdSelView_SelectedIndexChanged

        protected void rdSelView_SelectedIndexChanged(object sender, EventArgs e)
        {
            selRooms = "";
            Int32 cnt = 0;
            Int32 mCnt = 0;
            Int32 tCnt = 0;
            if (rdSelView.SelectedValue.Equals("2"))
            {
                pnlListView.Visible = true;
                pnlLevelView.Visible = false;
                lstRoomSelection.ClearSelection();
                HtmlInputCheckBox selectAll = (HtmlInputCheckBox)FindControl("selectAllCheckBox");

                if (selectedloc.Value.Trim() != "")
                {
                    foreach (ListItem lstItem in lstRoomSelection.Items)
                    {
                        for (int i = 0; i < selectedloc.Value.Trim().Split(',').Length; i++)
                            if (lstItem.Value.Equals(selectedloc.Value.Split(',')[i].Trim()))
                            {
                                lstItem.Selected = true;
                                cnt = cnt + 1;
                            }
                    }

                    if (selectAll != null)
                    {
                        if (cnt == lstRoomSelection.Items.Count)
                            selectAll.Checked = true;
                        else
                            selectAll.Checked = false;
                    }
                }
                else
                {
                    if (selectAll != null)
                        selectAll.Checked = false;
                }
            }
            else
            {
                pnlLevelView.Visible = true;
                pnlListView.Visible = false;

                foreach (TreeNode tnTop in treeRoomSelection.Nodes[0].ChildNodes)
                {
                    tCnt = 0;
                    foreach (TreeNode tnMid in tnTop.ChildNodes)
                    {
                        mCnt = 0;
                        foreach (TreeNode tn in tnMid.ChildNodes)
                        {
                            tn.Checked = false;
                            if (selectedloc.Value.Trim() != "")
                            {
                                for (int i = 0; i < selectedloc.Value.Trim().Split(',').Length; i++)
                                    if (tn.Depth.Equals(3) && tn.Value.Equals(selectedloc.Value.Split(',')[i].Trim()))
                                    {
                                        tn.Checked = true;
                                        mCnt++;
                                        if(locstrname.Value == "")
                                        locstrname.Value = tn.Value + "|" + tn.Text;
                                    }
                            }
                        }
                        if (mCnt == tnMid.ChildNodes.Count)
                        {
                            tnMid.Checked = true;
                            tCnt++;
                        }
                        else
                            tnMid.Checked = false;
                    }

                    if (tCnt == tnTop.ChildNodes.Count)
                    {
                        tnTop.Checked = true;
                        treeRoomSelection.Nodes[0].Checked = true;
                    }
                    else
                    {
                        tnTop.Checked = false;
                        treeRoomSelection.Nodes[0].Checked = false;
                    }
                }

            }
        }

        #endregion

        #region LoadCommonValues

        public void LoadCommonValues(XmlNode node)
        {
            CrossSilo(); //ZD 101233
            XmlNodeList nodes = null;
            lstRoomSelection.Items.Clear();
            treeRoomSelection.Nodes.Clear();
            TreeNode tn = new TreeNode("All");
            tn.Expanded = true;
            treeRoomSelection.Nodes.Add(tn);
            nodes = node.SelectNodes("/conference/confInfo/locationList");
            PreSelectRooms(nodes);
            nodes = node.SelectNodes("/conference/confInfo/locationList/level3List/level3");
            GetLocationList(nodes);

            if (nodes.Count == 0)
            {
                rdSelView.Enabled = false;
                pnlListView.Visible = false;
                pnlLevelView.Visible = false;
                btnCompare.Enabled = false;
                pnlNoData.Visible = false;
            }

            //ZD 103216 - Start
            nodes = node.SelectNodes("/conference/confInfo/locationList/RoomAttendees/RoomAttendee");
            Hashtable RmHostAttendee = new Hashtable();
            for (int i = 0; i < nodes.Count; i++)
            {
                RmHostAttendee[nodes[i].SelectSingleNode("RoomID").InnerText.Trim()] = nodes[i].SelectSingleNode("HostRoom").InnerText + "|" + nodes[i].SelectSingleNode("NoofAttendees").InnerText;
            }

            if (RmHostAttendee.Count > 0)
                Session["RoomHostDetails"] = RmHostAttendee;

        }
        #endregion

        #region PreSelectRooms

        protected void PreSelectRooms(XmlNodeList nodes)
        {
            XmlNodeList selnodes = nodes.Item(0).SelectNodes("selected/level1ID");
            int selLength = selnodes.Count;

            if (selLength >= 1)
                for (int n = 0; n < selLength; n++)
                    selRooms += selnodes.Item(n).InnerText + ", ";
            if (Request.QueryString["rms"] != null)
                selRooms += Request.QueryString["rms"].ToString();
            selectedloc.Value = selRooms.Trim(',');
        }

        #endregion

        #region GetLocationList

        protected void GetLocationList(XmlNodeList nodes)
        {
            try
            {
                ListItem li = new ListItem(obj.GetTranslatedText("Please select...."), "0");
                int nodes2Count = 0;
                int length = nodes.Count;
                for (int i = 0; i < length; i++)
                {
                    TreeNode tn3 = new TreeNode(nodes.Item(i).SelectSingleNode("level3Name").InnerText, nodes.Item(i).SelectSingleNode("level3ID").InnerText);
                    tn3.SelectAction = TreeNodeSelectAction.None;
                    treeRoomSelection.Nodes[0].ChildNodes.Add(tn3);
                    XmlNodeList nodes2 = nodes.Item(i).SelectNodes("level2List/level2");
                    tn3.Expanded = true;
                    int length2 = nodes2.Count;
                    if (!Application["roomExpandLevel"].ToString().ToLower().Equals("list"))
                        if (Application["roomExpandLevel"] != null)
                        {
                            if (Application["roomExpandLevel"].ToString() != "")
                            {
                                if (Int32.Parse(Application["roomExpandLevel"].ToString()) >= 2)
                                    tn3.Expanded = true;
                            }
                        }
                        else
                            tn3.Expanded = false;
                    for (int j = 0; j < length2; j++)
                    {
                        TreeNode tn2 = new TreeNode(nodes2.Item(j).SelectSingleNode("level2Name").InnerText, nodes2.Item(j).SelectSingleNode("level2ID").InnerText);

                        tn2.SelectAction = TreeNodeSelectAction.None;
                        treeRoomSelection.Nodes[0].ChildNodes[i].ChildNodes.Add(tn2);
                        XmlNodeList nodes1 = nodes2.Item(j).SelectNodes("level1List/level1");
                        int length1 = nodes1.Count;
                        tn2.Expanded = true;
                        nodes2Count = 0;
                        for (int k = 0; k < length1; k++)
                        {
                            TreeNode tn = new TreeNode(nodes1.Item(k).SelectSingleNode("level1Name").InnerText, nodes1.Item(k).SelectSingleNode("level1ID").InnerText);
                            tn.ToolTip = nodes1.Item(k).SelectSingleNode("level1ID").InnerText;
                            tn.Value = nodes1.Item(k).SelectSingleNode("level1ID").InnerText;
                            treeRoomSelection.Nodes[0].ChildNodes[i].ChildNodes[j].ChildNodes.Add(tn);
                            tn.NavigateUrl = @"javascript:chkresource('" + nodes1.Item(k).SelectSingleNode("level1ID").InnerText + "');";
                            string l1Name = "<a href='#'  title='" + nodes1.Item(k).SelectSingleNode("level1ID").InnerText + "'  onclick='javascript:chkresource(\"" + nodes1.Item(k).SelectSingleNode("level1ID").InnerText + "\");'>" + nodes1.Item(k).SelectSingleNode("level1Name").InnerText + "</a>";
                            li = new ListItem(l1Name, nodes1.Item(k).SelectSingleNode("level1ID").InnerText);
                            lstRoomSelection.Font.Size = FontUnit.Smaller;
                            lstRoomSelection.ForeColor = System.Drawing.Color.ForestGreen;
                            lstRoomSelection.Items.Add(li);
                            if (selRooms.IndexOf(" " + nodes1.Item(k).SelectSingleNode("level1ID").InnerText + ",") >= 0)
                            {
                                nodes2Count++;
                                tn.Checked = true;
                                li = new ListItem(tn.Text, tn.Value);
                            }
                        }

                        if (nodes1.Count.Equals(nodes2Count))
                            tn2.Checked = true;
                        if (!Application["roomExpandLevel"].ToString().ToLower().Equals("list"))
                            if (Application["roomExpandLevel"] != null)
                            {
                                if (Application["roomExpandLevel"].ToString() != "")
                                {
                                    if (Int32.Parse(Application["roomExpandLevel"].ToString()) >= 3)
                                        tn2.Expanded = true;
                                }
                            }
                            else
                                tn2.Expanded = false;
                    }
                }
                for (int i = 0; i < lstRoomSelection.Items.Count - 1; i++)
                    for (int j = i + 1; j < lstRoomSelection.Items.Count; j++)
                        if (String.Compare(lstRoomSelection.Items[i].Text, lstRoomSelection.Items[i].Text.IndexOf(">"), lstRoomSelection.Items[j].Text, lstRoomSelection.Items[j].Text.IndexOf(">"), lstRoomSelection.Items[i].Text.Length, true) > 0)
                        {
                            ListItem liTemp = new ListItem(lstRoomSelection.Items[i].Text, lstRoomSelection.Items[i].Value);
                            lstRoomSelection.Items[i].Value = lstRoomSelection.Items[j].Value;
                            lstRoomSelection.Items[i].Text = lstRoomSelection.Items[j].Text;
                            lstRoomSelection.Items[j].Value = liTemp.Value;
                            lstRoomSelection.Items[j].Text = liTemp.Text;
                        }
                foreach (ListItem listItem in lstRoomSelection.Items)
                {
                    for (int r = 0; r < selRooms.Split(',').Length - 1; r++)
                    {

                        if (listItem.Value.Equals(selRooms.Split(',')[r].Trim()))
                        {
                            listItem.Selected = true;
                        }
                    }
                }
                if (Application["RoomListView"].ToString().ToUpper().Equals("LIST"))
                {
                    rdSelView.Items.FindByValue("2").Selected = true;
                    rdSelView.SelectedIndex = 1;
                    rdSelView_SelectedIndexChanged(new object(), new EventArgs());
                }
            }
            catch (Exception ex)
            {
                errLabel.Visible = true;
                log.Trace(ex.Message);
            }
        }

        #endregion

        #region treeRoomSelection_TreeNodeCheckChanged


        protected void treeRoomSelection_TreeNodeCheckChanged(object sender, EventArgs e)
        {
            foreach (TreeNode tnTop in treeRoomSelection.Nodes[0].ChildNodes)
            {
                if (tnTop.Parent.Checked.Equals(true)) tnTop.Checked = true;
                foreach (TreeNode tnMid in tnTop.ChildNodes)
                {
                    if (tnMid.Parent.Checked.Equals(true)) tnMid.Checked = true;
                    foreach (TreeNode tn in tnMid.ChildNodes)
                    {
                        if (tn.Parent.Checked.Equals(true)) tn.Checked = true;
                    }
                }
            }
            ListItem li = new ListItem(obj.GetTranslatedText("Please select...."), "0");//FB 2272
            if (treeRoomSelection.CheckedNodes.Count > 0)
                foreach (TreeNode tn in treeRoomSelection.CheckedNodes)
                    if (tn.Depth.Equals(3))
                    {
                        selRooms += ", " + tn.Value;
                        li = new ListItem(tn.Text, tn.Value);
                    }
        }

        #endregion

        #region treeRoomSelection_SelectedNodeChanged

        protected void treeRoomSelection_SelectedNodeChanged(object sender, EventArgs e)
        {
            errLabel.Visible = true;
            errLabel.Text = "";
        }

        #endregion

        #region FillCustomAttributeTable

        private void FillCustomAttributeTable()
        {
            XmlDocument xmlDOC = new XmlDocument();
            xmlDOC.LoadXml(Session["outxml"].ToString());
            XmlNode node = (XmlNode)xmlDOC.DocumentElement;

            XmlNode node1 = node.SelectSingleNode("//conference/confInfo/CustomAttributesList");
            if (node1 != null)
            {
                string caxml = node1.OuterXml;
                if (CAObj == null)
                    CAObj = new myVRMNet.CustomAttributes();

                //FB 2592
                //custControlIDs = CAObj.CreateExpressAttributes(caxml, tblHost, tblWeb, tblSpecial, isFormSubmit, ref hdnHostIDs);
                custControlIDs = CAObj.CreateExpressAttributes(caxml, tblHost, tblSpecial, tblCustomAttribute, true, ref hdnHostIDs);

                hdnCusID.Value = custControlIDs;//FB 2592
                //FB 2349 Start
                if (tblSpecial.Rows.Count <= 0)
                    trsplinst.Attributes.Add("Style", "Display:None");
                //FB 2349 End
            }
        }
        #endregion
        //ZD 100570 Inncrewin 23-12-2013 Start
        #region UpdateTemplates
        protected void UpdateTemplates(Object sender, EventArgs e)
        {
            try
            {
                if (!lstTemplates.SelectedValue.Equals("-1"))
                {
                    Session.Remove("confid");
                    Session.Add("confid", lstTemplates.SelectedValue);
                    Session.Remove("confTempID");
                    Session.Add("confTempID", lstTemplates.SelectedValue);
                    Response.Redirect("ExpressConference.aspx?t=t");
                }
                else
                {
                    Response.Redirect("ExpressConference.aspx?t=n&op=1");
                }

            }
            catch (System.Threading.ThreadAbortException)
            { }
            catch (Exception ex)
            {
                log.Trace("UpdateTemplates: " + ex.StackTrace + " : " + ex.Message);
            }
        }
        #endregion
        //ZD 100570 Inncrewin 23-12-2013 end
        //FB 2839
        #region FillMCUProfile
        /// <summary>
        /// FillMCUProfile
        /// </summary>
        private void FillMCUProfile()
        {
            try
            {
                String inXML = "<GetBridges>" + obj.OrgXMLElement() + "<UserID>" + Session["userID"].ToString() + "</UserID></GetBridges>";
                String outXML = obj.CallMyVRMServer("GetBridges", inXML, Application["MyVRMServer_ConfigPath"].ToString());
                bool isProfilePoolEnabled = false; //ZD 104622
                XmlDocument xmldoc = new XmlDocument();
                xmldoc.LoadXml(outXML);
                XmlNode node;
                StringBuilder inXML1 = new StringBuilder();
                XmlNodeList nodes = xmldoc.SelectNodes("//Bridges/Bridge");
                int bridgetypeid = 0, MCUProfID = 0;
                string BridgeName = "", BridgeID = "", ConfServiceID = "", PoolOrderID = ""; //ZD 104256


                # region MCU Profile Binding
                for (int row = 0; row < nodes.Count; row++)
                {
                    node = nodes[row];
                    if (node.SelectSingleNode("BridgeType") != null)
                        Int32.TryParse(node.SelectSingleNode("BridgeType").InnerText, out bridgetypeid);

                    //ZD 100298 Start //ZD 103787 Binding the Polycom MCU profiles
                    if (!(bridgetypeid == 8 || bridgetypeid == 13 || bridgetypeid == 17 || bridgetypeid == 18 || bridgetypeid == 19))
                        continue;
                    //RMXbrid = true; //FB 3063

                    //ZD 100298 End

                    if (node.SelectSingleNode("BridgeName") != null)
                        BridgeName = node.SelectSingleNode("BridgeName").InnerText;

                    if (node.SelectSingleNode("BridgeID") != null)
                        BridgeID = node.SelectSingleNode("BridgeID").InnerText;

                    if (node.SelectSingleNode("ConferenceServiceID") != null)
                        ConfServiceID = node.SelectSingleNode("ConferenceServiceID").InnerText;
                    if (ConfServiceID == "0") //ZD 100298
                        ConfServiceID = "-1";

                    TableRow rw = new TableRow();

                    TableCell cell = new TableCell();
                    cell.Width = Unit.Percentage(32);
                    Label text = new Label();
                    text.CssClass = "blackblodtext";
                    text.Text = BridgeName;
                    cell.Controls.Add(text);
                    TableCell cell1 = new TableCell();
                    inXML1 = new StringBuilder();
                    inXML1.Append("<GetMCUProfiles>");
                    inXML1.Append("<UserID>" + Session["userID"].ToString() + "</UserID>" + "<OrganizationID>" + Session["OrganizationID"].ToString() + "</OrganizationID>");
                    inXML1.Append("<MCUId>" + BridgeID + "</MCUId>");
                    inXML1.Append("</GetMCUProfiles>");
                    string outXML1 = obj.CallMyVRMServer("GetMCUProfiles", inXML1.ToString(), Application["MyVRMServer_ConfigPath"].ToString());

                    XmlDocument xmldoc1 = new XmlDocument();
                    xmldoc1.LoadXml(outXML1);
                    XmlNodeList nodes1 = xmldoc1.SelectNodes("//GetMCUProfiles/Profile");
                    XmlNode node1;

                    DropDownList lstMCUProfiles = new DropDownList();
                    lstMCUProfiles.CssClass = "altText";
                    lstMCUProfiles.ID = "DrpMCU_" + row;
                    lstMCUProfiles.Width = Unit.Percentage(55); //ZD 100298

                    ListItem li = new ListItem();

                    if (nodes1.Count > 0)
                    {
                        for (int i = 0; i < nodes1.Count; i++)
                        {
                            if (i == 0)
                            {
                                li = new ListItem(obj.GetTranslatedText("None"), "-1"); //ZD 100298
                                lstMCUProfiles.Items.Add(li);
                            }
                            node1 = nodes1[i];
                            li = new ListItem(node1.SelectSingleNode("Name").InnerText, node1.SelectSingleNode("Id").InnerText);
                            lstMCUProfiles.Items.Add(li);
                        }
                        RMXbrid = true; //FB 3052
                        cell1.Controls.Add(lstMCUProfiles);
                        rw.Cells.Add(cell);
                        rw.Cells.Add(cell1);

                        tblMCUProfile.Controls.Add(rw);
                    }
                    else
                    {
                        li = new ListItem(obj.GetTranslatedText("None"), "-1", true); //ZD 100298
                        lstMCUProfiles.Items.Add(li);
                        lstMCUProfiles.Visible = false; //FB 3052
                    }

                    if (Session["EnableProfileSelection"] != null)
                    {

                        string nd1 = "";
                        if (Session["Confbridge"] != null)
                        {
                            nd1 = Session["Confbridge"].ToString();
                            XmlDocument xmldoc2 = new XmlDocument();
                            xmldoc2.LoadXml(nd1);

                            XmlNodeList endpointNodes = xmldoc2.SelectNodes("descendant::GetAdvancedAVSettings/Endpoints/Endpoint[Type='R']");
                            if (endpointNodes != null)
                            {
                                foreach (XmlNode nd in endpointNodes)
                                {
                                    if (BridgeID == nd.SelectSingleNode("BridgeID").InnerText.Trim())
                                    {
                                        ConfServiceID = nd.SelectSingleNode("BridgeProfileID").InnerText.Trim();
                                        isProfilePoolEnabled = true;//ZD 104622
                                    }
                                }
                            }

                            endpointNodes = xmldoc2.SelectNodes("descendant::GetAdvancedAVSettings/Endpoints/Endpoint[Type='U']");
                            if (endpointNodes != null)
                            {
                                foreach (XmlNode nd in endpointNodes)
                                {
                                    if (BridgeID == nd.SelectSingleNode("BridgeID").InnerText.Trim())
                                    {
                                        ConfServiceID = nd.SelectSingleNode("BridgeProfileID").InnerText.Trim();
                                        isProfilePoolEnabled = true;//ZD 104622
                                    }
                                }
                            }
                        }
                    }
                    if (ConfServiceID == "0") //ZD 100298
                        ConfServiceID = "-1";
                    int.TryParse(ConfServiceID, out MCUProfID);
                    lstMCUProfiles.Items.FindByValue(ConfServiceID).Selected = true;
                    hdnMcuProfileSelected.Value += BridgeID + "?" + lstMCUProfiles.ID + "$";

                }
                #endregion

                #region Pool Order
                for (int row = 0; row < nodes.Count; row++)
                {
                    node = nodes[row];
                    if (node.SelectSingleNode("BridgeType") != null)
                        int.TryParse(node.SelectSingleNode("BridgeType").InnerText, out bridgetypeid);

                    if (bridgetypeid != 13)
                        continue;

                    if (node.SelectSingleNode("BridgeName") != null)
                        BridgeName = node.SelectSingleNode("BridgeName").InnerText;

                    if (node.SelectSingleNode("BridgeID") != null)
                        BridgeID = node.SelectSingleNode("BridgeID").InnerText;

                    if (node.SelectSingleNode("ConferencePoolOrderID") != null)
                        PoolOrderID = node.SelectSingleNode("ConferencePoolOrderID").InnerText;
                    if (PoolOrderID == "0")
                        PoolOrderID = "-1";


                    # region Pool Order Binding

                    TableRow rw = new TableRow();

                    TableCell cell = new TableCell();
                    cell.Width = Unit.Percentage(32);
                    Label text = new Label();
                    text.CssClass = "blackblodtext";
                    text.Text = BridgeName;
                    cell.Controls.Add(text);
                    TableCell cell1 = new TableCell();
                    inXML1 = new StringBuilder();
                    inXML1.Append("<GetMCUPoolOrders>");
                    inXML1.Append("<UserID>" + Session["userID"].ToString() + "</UserID>" + "<OrganizationID>" + Session["OrganizationID"].ToString() + "</OrganizationID>");
                    inXML1.Append("<MCUId>" + BridgeID + "</MCUId>");
                    inXML1.Append("</GetMCUPoolOrders>");
                    string outXML1 = obj.CallMyVRMServer("GetMCUPoolOrders", inXML1.ToString(), Application["MyVRMServer_ConfigPath"].ToString());

                    XmlDocument xmldoc1 = new XmlDocument();
                    xmldoc1.LoadXml(outXML1);
                    XmlNodeList nodes1 = xmldoc1.SelectNodes("//GetMCUPoolOrders/PoolOrder");
                    XmlNode node1;

                    DropDownList lstPoolOrder = new DropDownList();
                    lstPoolOrder.CssClass = "altText";
                    lstPoolOrder.ID = "DrpMCUPool_" + row;
                    lstPoolOrder.Width = Unit.Percentage(55);

                    ListItem li = new ListItem();

                    if (nodes1.Count > 0)
                    {
                        for (int i = 0; i < nodes1.Count; i++)
                        {
                            if (i == 0)
                            {
                                li = new ListItem(obj.GetTranslatedText("None"), "-1");
                                lstPoolOrder.Items.Add(li);
                            }
                            node1 = nodes1[i];
                            li = new ListItem(node1.SelectSingleNode("Name").InnerText, node1.SelectSingleNode("Id").InnerText);
                            lstPoolOrder.Items.Add(li);
                        }
                        PoolOrderMCU = true;
                        cell1.Controls.Add(lstPoolOrder);
                        rw.Cells.Add(cell);
                        rw.Cells.Add(cell1);

                        tblPoolOrder.Controls.Add(rw);
                    }
                    else
                    {
                        li = new ListItem(obj.GetTranslatedText("None"), "-1", true);
                        lstPoolOrder.Items.Add(li);
                        lstPoolOrder.Visible = false;
                    }

                    if (EnablePoolOrder == "1")
                    {
                        string nd1 = "";
                        if (Session["Confbridge"] != null)
                        {
                            nd1 = Session["Confbridge"].ToString();
                            XmlDocument xmldoc2 = new XmlDocument();
                            xmldoc2.LoadXml(nd1);

                            XmlNodeList endpointNodes = xmldoc2.SelectNodes("descendant::GetAdvancedAVSettings/Endpoints/Endpoint[Type='R']");
                            if (endpointNodes != null)
                            {
                                foreach (XmlNode nd in endpointNodes)
                                {
                                    if (BridgeID == nd.SelectSingleNode("BridgeID").InnerText.Trim())
                                    {
                                        PoolOrderID = nd.SelectSingleNode("BridgePoolOrder").InnerText.Trim();
                                        isProfilePoolEnabled = true;//ZD 104622
                                    }
                                }
                            }

                            //if (PoolOrderID == "-1" || PoolOrderID == "0") //ZD 104256
                            //{
                                endpointNodes = xmldoc2.SelectNodes("descendant::GetAdvancedAVSettings/Endpoints/Endpoint[Type='U']");
                                if (endpointNodes != null)
                                {
                                    foreach (XmlNode nd in endpointNodes)
                                    {
                                        if (BridgeID == nd.SelectSingleNode("BridgeID").InnerText.Trim())
                                        {
                                            PoolOrderID = nd.SelectSingleNode("BridgePoolOrder").InnerText.Trim();
                                            isProfilePoolEnabled = true;//ZD 104622
                                        }
                                    }

                                }
                            //}
                        }
                    }
                   
                    if (PoolOrderID == "0")
                        PoolOrderID = "-1";

                    lstPoolOrder.Items.FindByValue(PoolOrderID).Selected = true;
                    hdnMcuPoolOrder.Value += BridgeID + "?" + lstPoolOrder.ID + "$";
                    #endregion
                }
                //ZD 101931 Start
                #endregion

                //ZD 101931 Start
                //FB 3063 Start //ZD 100704
                if (RMXbrid && (lstConferenceType.SelectedValue == ns_MyVRMNet.vrmConfType.AudioVideo || lstConferenceType.SelectedValue.Equals(ns_MyVRMNet.vrmConfType.OBTP) || lstConferenceType.SelectedValue == ns_MyVRMNet.vrmConfType.AudioOnly ||lstVMR.SelectedValue == "2")) //ZD 100513
                tdMCUProfile.Visible = true;
                else
                    tdMCUProfile.Visible = false;
                if (RMXbrid == true)
                    tdMCUProfile.Visible = true;
                //FB 3063 End

                if (EnableProfileSel == "1" && RMXbrid) //ZD 104256
                    trMCUProfile1.Visible = true;
                else
                    trMCUProfile1.Visible = false;

                //ZD 104256 Starts
                if (PoolOrderMCU && (lstConferenceType.SelectedValue == ns_MyVRMNet.vrmConfType.AudioVideo || lstConferenceType.SelectedValue.Equals(ns_MyVRMNet.vrmConfType.OBTP) || lstConferenceType.SelectedValue == ns_MyVRMNet.vrmConfType.AudioOnly || lstVMR.SelectedValue == "2"))
                    tdPoolOrder.Visible = true;
                else
                    tdPoolOrder.Visible = false;

                if (PoolOrderMCU && EnablePoolOrder == "1")
                    trPoolOrderSelection.Visible = true;
                else
                    trPoolOrderSelection.Visible = false;

                //ZD 104256 Ends

                if ((isProfilePoolEnabled && (EnableProfileSel == "1"|| EnablePoolOrder == "1")) && isEditMode == "1") //ZD 104256 //ZD 104622
                {
                    isAVSettingsSelected = true;
                    if ((lstConferenceType.SelectedValue == ns_MyVRMNet.vrmConfType.RoomOnly && EnableAudioBridges == 0) || (lstConferenceType.SelectedValue == ns_MyVRMNet.vrmConfType.P2P && EnableAudioBridges == 0))
                        isAVSettingsSelected = false;
                    if ((lstVMR.SelectedValue == "1" && EnableAudioBridges == 0) || (lstVMR.SelectedValue == "3" && EnableAudioBridges == 0))
                        isAVSettingsSelected = false;
                }
            }
            catch (Exception ex)
            {
                log.Trace("FillMCUProfile : " + ex.Message);
            }
        }

        #endregion

        #region SetConference

        //FB 1830 Email Edit - start
        protected void EmailDecision(object sender, EventArgs e)
        {
            try
            {
                ValidatP2PSelection();
                //FB 2634
                //if (ValidateConferenceDur() && CheckTime()) //Code added for Time Zone
                //if (CheckTime()) //Code added for Time Zone
                //      NotificationAlert();

                //FB 2634
            }
            catch (Exception ex)
            {
                log.Trace("EmailDecision : " + ex.Message);
                Session["hdModalDiv"] = "none";//ZD 101500
                errLabel.Visible = true;
                //ZD 100263
                errLabel.Text = obj.ShowSystemMessage();
                //errLabel.Text = ex.Message;
            }
        }

        private void NotificationAlert()
        {
            try
            {
                //ZD 101120 Starts
                int tzID = 26, PopUp = 0, GLsetupDuration = 0, ApprTime = 48, ExternalParty = 0, GLtearDuration = 0;
                DateTime PopUpDate = DateTime.UtcNow;
                if (GLWaringPopUp > 0)
                {
                    if (hdnGLApprTime.Value == "0")
                        ApprTime = 24;
                    else if (hdnGLApprTime.Value == "2")
                        ApprTime = 72;
                    else
                        ApprTime = 48;

                    if (enableBufferZone == "1")
                    {
                        Int32.TryParse(SetupDuration.Text, out GLsetupDuration);
                        Int32.TryParse(TearDownDuration.Text, out GLtearDuration);
                    }
                    else
                    {
                        GLsetupDuration = 0;
                        GLtearDuration = 0;
                    }

                    PopUpDate = PopUpDate.AddHours(ApprTime);

                    ExternalParty = txtPartysInfo.Text.Split(pipeDelim, StringSplitOptions.RemoveEmptyEntries).Length + dgOnflyGuestRoomlist.Items.Count;
                    for (int i = 0; i < txtPartysInfo.Text.Split(pipeDelim, StringSplitOptions.RemoveEmptyEntries).Length; i++)
                    {
                        if (!((txtPartysInfo.Text.Split(pipeDelim, StringSplitOptions.RemoveEmptyEntries)[i].Split(ExclamDelim, StringSplitOptions.RemoveEmptyEntries)[8].Trim().Equals("1")) && (txtPartysInfo.Text.Split(pipeDelim, StringSplitOptions.RemoveEmptyEntries)[i].Split(ExclamDelim, StringSplitOptions.RemoveEmptyEntries)[4].Trim().Equals("1"))))
                            ExternalParty--;
                    }
                    hdnGLCount.Value = ExternalParty.ToString();
                    int.TryParse(lstConferenceTZ.SelectedValue, out tzID);

                    DateTime GLTime = DateTime.Parse(myVRMNet.NETFunctions.GetDefaultDate(confStartDate.Text) + " " + myVRMNet.NETFunctions.ChangeTimeFormat(confStartTime.Text));

                    DateTime GLEndTime = DateTime.Parse(myVRMNet.NETFunctions.GetDefaultDate(confEndDate.Text) + " " + myVRMNet.NETFunctions.ChangeTimeFormat(confEndTime.Text));

                    GLTime = GLTime.AddMinutes(-GLsetupDuration);
                    GLEndTime = GLEndTime.AddMinutes(GLtearDuration);

                    obj.changeToGMTTime(tzID, ref GLTime);
                    obj.changeToGMTTime(tzID, ref GLEndTime);

                    PopUp = DateTime.Compare(PopUpDate, GLEndTime);

                    hdnGLConfTime.Value = "0";
                    if (PopUp > 0)
                        hdnGLConfTime.Value = "1";
                    else if (PopUp < 0)
                        hdnGLConfTime.Value = "2";
                }
                //ZD 101120 Ends
                //ZD 101226
                if (enableCloudInstallation == 1)
                {
                    Session["hdModalDiv"] = "block";//ZD 101500
                    SetConference(null, null);
                }
                else
                {
                    Session["hdModalDiv"] = "block";//ZD 101500                   
                    String respStr = "<script>fnemailalert();</script>";
                    ClientScript.RegisterStartupScript(GetType(), "EmailScript", respStr);
                }

            }
            catch (Exception e)
            {
                log.Trace("NotificationAlert : " + e.Message);
            }
        }
        //FB 1830 Email Edit - end

        protected void SetConference(object sender, EventArgs e)
        {
            try
            {
                if (ValidateEndpoints() && ValidateIPISDNAddress()) //ZD 100704//ZD 100834
                {
                    SetConference();
                }
            }
            catch (Exception ex)
            {
                log.Trace("SetConference : " + ex.Message);
                Session["hdModalDiv"] = "none";//ZD 101500
                errLabel.Visible = true;
                //ZD 100263
                errLabel.Text = obj.ShowSystemMessage();
                //errLabel.Text = ex.Message;
            }
        }

        //ZD 100704 Starts
        #region ValidateEndpoints
        /// <summary>
        /// ValidateEndpoints
        /// </summary>
        /// <returns></returns>
        protected bool ValidateEndpoints()
        {
            try
            {
                string errMsg = "";
                string[] locs = null;
                int eptCount = 0;
                //ZD 100513 Starts
                ConerenceType = lstConferenceType.SelectedValue;
                if (ConerenceType == "9")
                    ConerenceType = "2";
                hdnconftype.Value = ConerenceType;
                //ZD 100513 ENDS

                if (lstConferenceType.SelectedValue.Equals(ns_MyVRMNet.vrmConfType.P2P))
                {
                    if (selectedloc.Value != "")
                    {
                        locs = selectedloc.Value.Split(',');
                        for (int i = 0; i < locs.Length; i++)
                        {
                            if (string.IsNullOrEmpty(locs[i]) || locs[i].Trim() == "")
                                continue;
                            eptCount++;
                        }
                    }

                    if (lstAudioParty.SelectedValue != "-1" && lstAudioParty.SelectedValue != "")
                        eptCount++;

                    //ZD 100834
                    if (dgUsers != null && dgUsers.Items.Count > 0)
                        eptCount += dgUsers.Items.Count;

                    //ZD 100815
                    if (dgOnflyGuestRoomlist != null && dgOnflyGuestRoomlist.Items.Count > 0)
                        eptCount += dgOnflyGuestRoomlist.Items.Count;

                    if (eptCount != 2)
                    {
                        errLabel.Visible = true;
                        Session["hdModalDiv"] = "none";//ZD 101500
                        errLabel.Text = obj.GetTranslatedText("Only two Endpoints can be selected for Point-To-Point Conference.");
                        return false;
                    }
                }


                //ZD 100704 Starts
                if (lstConferenceType.SelectedValue.Equals(ns_MyVRMNet.vrmConfType.HotDesking))
                {
                    SetupDuration.Text = "0";
                    confPassword.Value = "";
                    ConferencePassword.Text = "";
                    chkPCConf.Checked = false;
                    chkStartNow.Checked = false;
                    ChkEnableNumericID.Checked = false;
                    chkFECC.Checked = false;
                    txtMCUConnect.Text = "";
                    txtMCUDisConnect.Text = "";
                    txtPartysInfo.Text = "";

                    chkOnSiteAVSupport.Checked = false;
                    chkMeetandGreet.Checked = false;
                    chkConciergeMonitoring.Checked = false;
                    chkDedicatedVNOCOperator.Checked = false;
                    hdnVNOCOperator.Text = "";
                    txtVNOCOperator.Text = "";
                }
                //ZD 100704 End
                //ZD 100834 Starts
                bool isError = false;
                if (!lstConferenceType.SelectedValue.Equals(ns_MyVRMNet.vrmConfType.RoomOnly) && !lstConferenceType.SelectedValue.Equals(ns_MyVRMNet.vrmConfType.P2P)) //FB Case 770 Saima
                {
                    foreach (DataGridItem dgi in dgUsers.Items)
                    {
                        DropDownList lstBridges = (DropDownList)dgi.FindControl("lstBridges");
                        DropDownList lstAddressType = (DropDownList)dgi.FindControl("lstAddressType");
                        if (lstBridges.SelectedValue.Equals("-1") && lstBridges.SelectedItem.Text.Equals("No Items...")) //FB 2164
                        {
                            errMsg = "<br>" + obj.GetTranslatedText("Please select MCU for user:") + ((Label)dgi.FindControl("lblUserName")).Text; //FB JAPAN
                        }
                        //commented for ZD 103595- Operations - Remove MPI Functionality and Verbiage START
                        //if (lstAddressType.SelectedValue.Equals(ns_MyVRMNet.vrmAddressType.MPI))
                        //    if (lstBridges.SelectedValue.Equals("-1"))
                        //    {
                        //        errMsg = "<br>" + obj.GetTranslatedText("Please select MCU for user:") + ((Label)dgi.FindControl("lblUserName")).Text; //FB JAPAN//FB 2272
                        //        isError = true;
                        //    }
                        //    else if (!IsValidBridge(lstBridges.SelectedValue))
                        //    {
                        //        errMsg = "<br>" + obj.GetTranslatedText("Selected bridge for user ") + ((Label)dgi.FindControl("lblUserName")).Text + obj.GetTranslatedText(" does not support MPI calls."); //FB JAPAN
                        //        isError = true;
                        //    }
                        //commented for ZD 103595- Operations - Remove MPI Functionality and Verbiage END
                    }

                    if (isError)
                    {
                        Session["hdModalDiv"] = "none";//ZD 101500
                        errLabel.Visible = true;
                        errLabel.Text = errMsg;
                        return false;
                    }
                }
                //ZD 100834 End
            }
            catch (Exception ex)
            {
                log.Trace("ValidateEndpoints Error: " + ex.Message);
                errLabel.Visible = true;
                errLabel.Text = obj.GetTranslatedText(obj.ShowSystemMessage());
                return false;
            }
            return true;
        }
        #endregion
        //ZD 100704 End

        protected void SetConferenceCustom(object sender, EventArgs e)
        {
            try
            {
                isSetCustom = true;
                SetConference();
            }
            catch (Exception ex)
            {
                log.Trace("SetConferenceCustom : " + ex.Message);
                Session["hdModalDiv"] = "none";//ZD 101500
                errLabel.Visible = true;
                //ZD 100263
                errLabel.Text = obj.ShowSystemMessage();
                //errLabel.Text = ex.Message;
            }
        }
        //ZD 100704 Starts
        #region ValidateBridgeAdmin
        /// <summary>
        /// Method to validate the MCU admin status
        /// </summary>
        /// <returns></returns>
        private bool ValidateBridgeAdmin(DataTable dtRooms)
        {
            bool adminStatus = true;
            StringBuilder inXML = new StringBuilder();

            inXML.Append("<Bridges>");
            inXML.Append(obj.OrgXMLElement());
            for (int i = 0; i < dtRooms.Rows.Count; i++)
            {
                inXML.Append("<ID>" + dtRooms.Rows[i]["BridgeID"].ToString().Trim() + "</ID>");
            }
            inXML.Append("</Bridges>");

            string outXML = obj.CallMyVRMServer("FetchBridgeInfo", inXML.ToString(), Application["MyVRMServer_Configpath"].ToString());
            XmlDocument xd = new XmlDocument();
            errLabel.Text = "";
            if (outXML != "")
            {
                xd.LoadXml(outXML);
                XmlNodeList nodes = xd.SelectNodes("bridges/bridge");
                if (nodes != null)
                {
                    foreach (XmlNode node in nodes)
                    {
                        if (node.SelectSingleNode("userstate") != null)
                        {
                            if (node.SelectSingleNode("userstate").InnerText == "I")
                            {
                                if (errLabel.Text == "")
                                    errLabel.Text += "Error:MCU (" + node.SelectSingleNode("name").InnerText + ") Admin User - " + node.SelectSingleNode("administrator").InnerText.Trim() + obj.GetTranslatedText(" is In-Active. Please make sure the user is Active.");
                                else
                                    errLabel.Text += "<br>Error:MCU (" + node.SelectSingleNode("name").InnerText + ") Admin User - " + node.SelectSingleNode("administrator").InnerText.Trim() + obj.GetTranslatedText(" is In-Active. Please make sure the user is Active.");

                                adminStatus = false;
                            }
                            else if (node.SelectSingleNode("userstate").InnerText == "D")
                            {
                                if (errLabel.Text == "")
                                    errLabel.Text += "Error:MCU (" + node.SelectSingleNode("name").InnerText + ")" + obj.GetErrorMessage(430);
                                else
                                    errLabel.Text += "<br>Error:MCU (" + node.SelectSingleNode("name").InnerText + ")" + obj.GetErrorMessage(430);

                                adminStatus = false;
                            }
                        }
                    }
                }
            }
            return adminStatus;
        }
        #endregion
        //ZD 100704 End

        //ZD 102195 Start
        protected bool SetConference()
        {
            List<int> SelRooms; 
            DataTable dtRooms = new DataTable();
            int v = 0; 
            string ConfPWD = "";
            String[] roomsSelcted = null;
            ListItem item = null;
            StringBuilder inxml = new StringBuilder();
            Hashtable mculidt = new Hashtable();
            Hashtable MCUPoolOrder = new Hashtable(); //ZD 104256
            string bridgeId = "-1";
            string optionId = "", optionValue = "", BID = "";
            string SelEntityCodeVal = "";//ZD 102909
            DataTable dtEntityCode1 = new DataTable();            
            try
            {
                AudioPartySelectedProperty(); //ALLDEV-814
                
                //ZD 103870 Starts
                bool isTemplate = false;
                if (Session["isTemplate"] != null)
                    if (Session["isTemplate"].ToString() == "1")
                        isTemplate = true;
                //ZD 103870 Ends

                Session["hdModalDiv"] = "block";
                CreateDataTable(dtRooms);

                int maxDuration = 24;
                if (Application["MaxConferenceDurationInHours"] != null)
                    if (!Application["MaxConferenceDurationInHours"].ToString().Trim().Equals(""))
                        int.TryParse(Application["MaxConferenceDurationInHours"].ToString().Trim(), out maxDuration);

                roomsSelcted = locstrname.Value.Split(DoublePlusDelim, StringSplitOptions.RemoveEmptyEntries);
                RoomList.Items.Clear();
                for (int i = 0; i < roomsSelcted.Length; i++)
                {
                    if (roomsSelcted[i].Trim() == "")
                        continue;

                    item = new ListItem();
                    item.Value = roomsSelcted[i].Split('|')[0];
                    item.Text = roomsSelcted[i].Split('|')[1];
                    RoomList.Items.Add(item);
                }

                if (lstLineRate.SelectedValue.Trim() == "-1")
                {
                    lstLineRate.ClearSelection();
                    lstLineRate.SelectedValue = "384";
                }

                inxml.Append("<conference>");
                inxml.Append(obj.OrgXMLElement());
                inxml.Append("<requestorID>" + hdnApprover7.Text + "</requestorID>");
                inxml.Append("<userID>" + Session["userid"].ToString() + "</userID>");
                
                inxml.Append("<confInfo>");
                if (Request.QueryString["t"] != null && Request.QueryString["t"].ToString().Equals("o"))
                {
                    inxml.Append("<confID>new</confID>");
                }
                else
                    inxml.Append("<confID>" + lblConfID.Text + "</confID>");

                inxml.Append("<isHDBusy>" + hdnIsHDBusy.Value + "</isHDBusy>"); //ALLDEV-807
                inxml.Append("<editFromWeb>1</editFromWeb>");
                inxml.Append("<isExpressConference>1</isExpressConference>");
                inxml.Append("<language>" + language + "</language>");

                if (hdnIsHDBusy != null && hdnIsHDBusy.Value == "1") //ALLDEV-807
                    inxml.Append(" <confName>" + hdnHDConfName.Value + "</confName>");
                else
                    inxml.Append(" <confName>" + ConferenceName.Text + "</confName>");

                if (enableCloudInstallation == 1) 
                    inxml.Append("<confHost>" + hdntxtHost_exp.Text + "</confHost>"); 
                else
                    inxml.Append("<confHost>" + hdnApprover4.Text + "</confHost>");
                inxml.Append("<confOrigin>0</confOrigin>");
                inxml.Append("<timeCheck>" + txtTimeCheck.Text + "</timeCheck>");

                if (lstConferenceType.SelectedValue.Equals(ns_MyVRMNet.vrmConfType.RoomOnly) || lstConferenceType.SelectedValue.Equals(ns_MyVRMNet.vrmConfType.HotDesking))
                {
                    //ZD 103876
                    if (!lstConferenceType.SelectedValue.Equals(ns_MyVRMNet.vrmConfType.P2P))
                        confPassword.Value = "";
                    txtVMRPIN1.Text = "";
                }
                
                ConfPWD = confPassword.Value;
                if (lstVMR.SelectedValue == "2" && !string.IsNullOrEmpty(txtVMRPIN1.Text))
                    ConfPWD = txtVMRPIN1.Text;
                inxml.Append("<confPassword>" + ConfPWD + "</confPassword>");
                inxml.Append("<hostPassword>" + txtHostPwd.Text + "</hostPassword>");   //ALLDEV-826
                inxml.Append("<isRoomVMRPINChange>" + hdnROOMPIN.Value + "</isRoomVMRPINChange>");
                
                if (lstConferenceType.SelectedValue.Equals(ns_MyVRMNet.vrmConfType.P2P) || lstConferenceType.SelectedValue.Equals(ns_MyVRMNet.vrmConfType.RoomOnly) || lstConferenceType.SelectedValue.Equals(ns_MyVRMNet.vrmConfType.HotDesking) || (lstVMR.SelectedIndex > 0 && lstVMR.SelectedValue != "2") || chkPCConf.Checked)
                {
                    lstStartMode.ClearSelection();
                    lstStartMode.SelectedValue = "0";
                }
                inxml.Append("<isVMR>" + lstVMR.SelectedValue + "</isVMR>");
                inxml.Append("<IcalID>" + hdnicalID.Value + "</IcalID>");
                if (chkCloudConferencing.Checked)
                    inxml.Append("<CloudConferencing>1</CloudConferencing>");
                else
                    inxml.Append("<CloudConferencing>0</CloudConferencing>");

                if (hdnSchdWebEx.Value == "1" && hdnHostWebEx.Value == "1")
                {
                    if (chkWebex.Checked)
                    {
                        inxml.Append("<WebExConf>1</WebExConf>");
                        if (hdnPasschange.Value != "" && hdnPasschange.Value == "true")
                            inxml.Append("<WebExConfPW>" + Password(hdnWebExPwd.Value) + "</WebExConfPW>");
                        else
                        {
                            if (Session["WebExPass"] != null && Session["WebExPass"].ToString() != "")
                                inxml.Append("<WebExConfPW>" + Session["WebExPass"].ToString() + "</WebExConfPW>");
                        }
                    }
                    else
                    {
                        inxml.Append("<WebExConf>0</WebExConf>");
                        inxml.Append("<WebExConfPW></WebExConfPW>");
                    }
                }
                else
                {
                    inxml.Append("<WebExConf>0</WebExConf>");
                    inxml.Append("<WebExConfPW></WebExConfPW>");
                }
                
                if (chkStatic.Checked)
                {
                    inxml.Append("<EnableStaticID>1</EnableStaticID>");
                    inxml.Append("<MeetingId>0</MeetingId>");
                    inxml.Append("<StaticID>" + txtStatic.Text + "</StaticID>");
                }
                else
                {
                    inxml.Append("<EnableStaticID>0</EnableStaticID>");
                    inxml.Append("<MeetingId>1</MeetingId>");
                    inxml.Append("<StaticID></StaticID>");
                }

                if (chkPCConf.Checked)
                {
                    inxml.Append("<isPCconference>1</isPCconference>");

                    string value = "2";
                    //if (rdBJ.Checked)//ZD 104021
                    //    value = "1";
                    if (rdJB.Checked)
                        value = "2";
                    else if (rdLync.Checked)
                        value = "3";
                
                    inxml.Append("<pcVendorId>" + value + "</pcVendorId>");

                }
                else
                {
                    inxml.Append("<isPCconference>0</isPCconference>");
                    inxml.Append("<pcVendorId>0</pcVendorId>");
                }
                
                int durationMin;
                
                Int32 setupDuration = Int32.MinValue;
                Int32 tearDuration = Int32.MinValue;

                obj.isBufferChecked = true; 
                
                if (lstConferenceType.SelectedValue.Equals(ns_MyVRMNet.vrmConfType.P2P) || lstConferenceType.SelectedValue.Equals(ns_MyVRMNet.vrmConfType.RoomOnly) || lstConferenceType.SelectedValue.Equals(ns_MyVRMNet.vrmConfType.HotDesking) || (lstVMR.SelectedIndex > 0 && lstVMR.SelectedValue != "2") || chkCloudConferencing.Checked || chkPCConf.Checked) 
                    inxml.Append("<StartMode>0</StartMode>");
                else
                    inxml.Append("<StartMode>" + lstStartMode.SelectedValue + "</StartMode>");
                
                if (Session["MeetandGreetBuffer"] != null)
                {
                    if (!Session["MeetandGreetBuffer"].ToString().Equals(""))
                    {
                        inxml.Append("<MeetandGreetBuffer>" + Session["MeetandGreetBuffer"].ToString() + "</MeetandGreetBuffer>");
                    }
                }
                if (NetworkSwitching == 2)
                    inxml.Append("<Secured>" + drpNtwkClsfxtn.SelectedValue + "</Secured>");
                else
                    inxml.Append("<Secured>0</Secured>");
                if (ChkEnableNumericID.Checked)
                {
                    inxml.Append("<EnableNumericID>1</EnableNumericID>");
                    inxml.Append("<CTNumericID>" + txtNumeridID.Text + "</CTNumericID>");
                }
                else
                {
                    inxml.Append("<EnableNumericID>0</EnableNumericID>");
                    inxml.Append("<CTNumericID></CTNumericID>");
                }
                //ZD 103550 - Start //ZD 104116 - Start
                if (EnableBlueJeans == "1")
                {
                    if (EnableBJNIntegration == "1" && EnableBJNDisplay == "0")
                        inxml.Append("<isBJNConf>1</isBJNConf>");
                    if (EnableBJNIntegration == "1" && (chkBJNMeetingID != null && chkBJNMeetingID.Checked == true))
                        inxml.Append("<isBJNConf>1</isBJNConf>");
                    else
                        inxml.Append("<isBJNConf>0</isBJNConf>");

                    if (EnableBJNSelectOption == "1")
                        inxml.Append("<BJNMeetingType>" + DrpBJNMeetingID.SelectedValue + "</BJNMeetingType>");
                    else
                        inxml.Append("<BJNMeetingType></BJNMeetingType>");
                }
                //ZD 103550 - End //ZD 104116 - End      
                //ALLDEV-782 Starts
                if (EnablePexipIntegration == "1")
                {
                    if (EnablePexipDisplay == "0" || (chkPexipMtgType != null && chkPexipMtgType.Checked == true))
                        inxml.Append("<isPexipConf>1</isPexipConf>");
                    else
                        inxml.Append("<isPexipConf>0</isPexipConf>");

                    if (EnablePexipSelectOption == "1")
                        inxml.Append("<PexipMeetingType>" + drpPexipMtgType.SelectedValue + "</PexipMeetingType>");
                    else
                        inxml.Append("<PexipMeetingType></PexipMeetingType>");
                }
                //ALLDEV-782 Ends
                int MCUPreStartDur = 0, MCUPreEndDur = 0;
                if (txtMCUConnect.Text.Trim() != "" && (lstConferenceType.SelectedValue.Equals(ns_MyVRMNet.vrmConfType.AudioVideo) || lstConferenceType.SelectedValue.Equals(ns_MyVRMNet.vrmConfType.OBTP) || lstConferenceType.SelectedValue.Equals(ns_MyVRMNet.vrmConfType.AudioOnly)) && !chkStartNow.Checked && lstVMR.SelectedIndex == 0 && !chkPCConf.Checked) 
                {
                    int.TryParse(txtMCUConnect.Text, out MCUPreStartDur);
                    inxml.Append("<McuSetupTime>" + txtMCUConnect.Text + "</McuSetupTime>");
                }
                else
                    inxml.Append("<McuSetupTime>0</McuSetupTime>");

                if (txtMCUDisConnect.Text.Trim() != "" && (lstConferenceType.SelectedValue.Equals(ns_MyVRMNet.vrmConfType.AudioVideo) || lstConferenceType.SelectedValue.Equals(ns_MyVRMNet.vrmConfType.OBTP) || lstConferenceType.SelectedValue.Equals(ns_MyVRMNet.vrmConfType.AudioOnly)) && !chkStartNow.Checked && lstVMR.SelectedIndex == 0 && !chkPCConf.Checked)
                {
                    int.TryParse(txtMCUDisConnect.Text, out MCUPreEndDur);
                    inxml.Append("<MCUTeardonwnTime>" + txtMCUDisConnect.Text + "</MCUTeardonwnTime>");
                }
                else
                    inxml.Append("<MCUTeardonwnTime>0</MCUTeardonwnTime>");

                if (chkStartNow.Checked || (Recur.Value == "" && RecurSpec.Value == ""))
                {
                    if (chkStartNow.Checked)
                    {
                        inxml.Append("<immediate>1</immediate>");
                        inxml.Append("<recurring>0</recurring>");
                        inxml.Append("<recurringText></recurringText>");
                        inxml.Append("<startDate></startDate>");
                        inxml.Append("<startHour></startHour>");
                        inxml.Append("<startMin></startMin>");
                        inxml.Append("<startSet></startSet>");
                        inxml.Append("<timeZone>" + lstConferenceTZ.SelectedValue + "</timeZone>");
                        durationMin = Convert.ToInt32(lstDuration.Text.Split(':')[0]) * 60 + Convert.ToInt32(lstDuration.Text.Split(':')[1]);

                        inxml.Append("<setupDuration>0</setupDuration>");
                        inxml.Append("<teardownDuration>0</teardownDuration>");
                        inxml.Append("<setupDateTime></setupDateTime>");
                        inxml.Append("<teardownDateTime></teardownDateTime>");
                    }
                    else
                    {
                        inxml.Append("<immediate>0</immediate>");

                        int sHour = 0;
                        if (Session["timeFormat"] != null)
                        {
                            if (Session["timeFormat"].ToString().Equals("1"))
                            {
                                sHour = Convert.ToInt16(confStartTime.Text.Split(' ')[0].Split(':')[0]);

                                if ((confStartTime.Text.Split(' ')[1] == "PM") && (sHour != 12))
                                    sHour += 12;
                                if ((confStartTime.Text.Split(' ')[1] == "AM") && (sHour == 12))
                                    sHour -= 12;
                            }
                        }
                        DateTime dStart = DateTime.Parse(myVRMNet.NETFunctions.GetDefaultDate(confStartDate.Text) + " " + myVRMNet.NETFunctions.ChangeTimeFormat(confStartTime.Text)); 

                        if (Session["timeFormat"] != null)
                        {
                            if (Session["timeFormat"].ToString().Equals("1"))
                            {
                                sHour = Convert.ToInt16(confEndTime.Text.Split(' ')[0].Split(':')[0]);
                                if ((confEndTime.Text.Split(' ')[1] == "PM") && (sHour != 12))
                                    sHour += 12;
                                if ((confEndTime.Text.Split(' ')[1] == "AM") && (sHour == 12))
                                    sHour -= 12;
                            }
                        }

                        DateTime dEnd = DateTime.Parse(myVRMNet.NETFunctions.GetDefaultDate(confEndDate.Text) + " " + myVRMNet.NETFunctions.ChangeTimeFormat(confEndTime.Text));
                        TimeSpan ts = dEnd.Subtract(dStart);
                        durationMin = Int32.Parse(ts.TotalMinutes.ToString());
                        if (EnableBufferZone == 1 && !lstConferenceType.SelectedValue.Equals(ns_MyVRMNet.vrmConfType.HotDesking))//ZD 102823
                        {
                            Int32.TryParse(SetupDuration.Text, out setupDuration);
                            Int32.TryParse(TearDownDuration.Text, out tearDuration);
                        }
                        else
                        {
                            setupDuration = 0;
                            tearDuration = 0;
                        }
                        inxml.Append("<recurring>0</recurring>");
                        inxml.Append("<recurringText></recurringText>");
                        inxml.Append("<startDate>" + myVRMNet.NETFunctions.GetDefaultDate(dStart.ToString(format)) + "</startDate>");
                        inxml.Append("<startHour>" + dStart.ToString("hh") + "</startHour>");
                        inxml.Append("<startMin>" + dStart.ToString("mm") + "</startMin>");
                        inxml.Append("<startSet>" + dStart.ToString("tt") + "</startSet>");

                        inxml.Append("<timeZone>" + lstConferenceTZ.SelectedValue + "</timeZone>");

                        DateTime sDateTime = DateTime.Parse(myVRMNet.NETFunctions.GetDefaultDate(confStartDate.Text) + " " + myVRMNet.NETFunctions.ChangeTimeFormat(confStartTime.Text)); 
                        DateTime tDateTime = DateTime.Parse(myVRMNet.NETFunctions.GetDefaultDate(confEndDate.Text) + " " + myVRMNet.NETFunctions.ChangeTimeFormat(confEndTime.Text));
                        sDateTime = sDateTime.AddMinutes(-setupDuration);
                        tDateTime = tDateTime.AddMinutes(tearDuration);//ZD 102823
                       
                        if (EnableBufferZone == 0)
                        {
                            sDateTime = dStart;
                            tDateTime = dEnd;
                        }
                        inxml.Append("<setupDuration>" + setupDuration + "</setupDuration>");
                        inxml.Append("<teardownDuration>" + tearDuration + "</teardownDuration>");
                        inxml.Append("<setupDateTime>" + sDateTime + "</setupDateTime>");
                        inxml.Append("<teardownDateTime>" + tDateTime + "</teardownDateTime>");
                    }
                    inxml.Append("<createBy>" + lstConferenceType.SelectedValue.ToString() + "</createBy>"); 
                    
                    int totalDuration = 0, buffDur = 0, mcuPreEndPostive = 0, minDiff = 0;
                    if (MCUPreEndDur < 0)
                        mcuPreEndPostive = MCUPreEndDur * -1;

                    if (MCUPreEndDur > 0 || mcuPreEndPostive <= tearDuration)
                        buffDur = tearDuration;
                    else if (MCUPreEndDur < 0 && mcuPreEndPostive > tearDuration)
                        buffDur = mcuPreEndPostive;

                    if (setupDuration > MCUPreStartDur)
                        totalDuration = durationMin + setupDuration + buffDur;
                    else
                        totalDuration = durationMin + MCUPreStartDur + buffDur;

                    minDiff = durationMin - MCUPreEndDur;
                    if (durationMin >= 15) //103925 && totalDuration <= (maxDuration * 60))
                        inxml.Append("<durationMin>" + durationMin.ToString() + "</durationMin>");
                    else
                    {
                        if (durationMin < 15 || minDiff < 15)
                            errLabel.Text = obj.GetTranslatedText(ns_MyVRMNet.ErrorList.InvalidDuration);

                        //if (totalDuration > (maxDuration * 60)) //103925 
                        //    errLabel.Text = obj.GetTranslatedText(ns_MyVRMNet.ErrorList.ExceedDuration); 

                        errLabel.Visible = true;
                        Session["hdModalDiv"] = "none";
                        return false;
                    }
                }
                else
                {
                    inxml.Append("<immediate>0</immediate>");
                    inxml.Append("<createBy>" + lstConferenceType.SelectedValue.ToString() + "</createBy>");

                    int sDur = 0;
                    int tDur = 0;

                    if (hdnSetupTime.Value == "" || hdnSetupTime.Value == null)
                        hdnSetupTime.Value = "0";

                    if (hdnTeardownTime.Value == "" || hdnTeardownTime.Value == null)
                        hdnTeardownTime.Value = "0";

                    Int32.TryParse(hdnSetupTime.Value, out sDur);
                    Int32.TryParse(hdnTeardownTime.Value, out tDur);

                    if (EnableBufferZone == 0)
                    {
                        sDur = 0;
                        tDur = 0;

                    }
                    else
                    {
                        Int32.TryParse(SetupDuration.Text, out sDur);
                        Int32.TryParse(TearDownDuration.Text, out tDur);
                    }

                    inxml.Append("<setupDuration>" + sDur + "</setupDuration>");
                    inxml.Append("<teardownDuration>" + tDur + "</teardownDuration>");
                    inxml.Append("<setupDateTime></setupDateTime>");
                    inxml.Append("<teardownDateTime></teardownDateTime>");

                    int recurDurHour = 0, recurDurMin;
                    int.TryParse(RecurDurationhr.Text, out recurDurHour);

                    int.TryParse(RecurDurationmi.Text, out recurDurMin);

                    int totalRecurDur = recurDurHour * 60 + recurDurMin;
                    int totalDuration = totalRecurDur + sDur + tDur + MCUPreStartDur - MCUPreEndDur;

                    if (totalRecurDur < 15)  // 103925 || totalDuration > (maxDuration * 60))
                    {
                        if (totalRecurDur < 15)
                            errLabel.Text = obj.GetTranslatedText(ns_MyVRMNet.ErrorList.InvalidDuration);

                        //if (totalDuration > (maxDuration * 60)) 103925 
                        //    errLabel.Text = obj.GetTranslatedText(ns_MyVRMNet.ErrorList.ExceedDuration);  // 103925 

                        errLabel.Visible = true;
                        Session["hdModalDiv"] = "none";
                        return false;
                    }

                    string bufferxml = "<setupDuration>" + sDur + "</setupDuration>";
                    bufferxml += "<teardownDuration>" + tDur + "</teardownDuration>";
                    bufferxml += "<PreStartDuration>" + MCUPreStartDur + "</PreStartDuration>";
                    bufferxml += "<PreEndDuration>" + MCUPreEndDur + "</PreEndDuration>";
                    //ZD 100085 End
                    inxml.Append("<recurring>1</recurring>");
                    inxml.Append("<recurringText>" + RecurringText.Value + "</recurringText>");

                    if (isSpecialRecur == 1 && hdnSpecRec.Value == "1" && isEditMode == "0") 
                    {
                        string recXML = "";
                        string strInXml = "";
                        strInXml = obj.AppendSpecialRecur(RecurSpec.Value, bufferxml, lstConferenceTZ.SelectedValue, ref recXML);
                        if (strInXml.ToString().IndexOf("<error>") >= 0)
                        {
                            Session["hdModalDiv"] = "none";
                            XmlDocument xDoc = new XmlDocument();
                            xDoc.LoadXml(strInXml);
                            errLabel.Text = xDoc.SelectSingleNode("error").InnerText;
                            errLabel.Visible = true;
                            return false;
                        }
                        else
                            inxml.Append(strInXml);
                    }
                    else
                        inxml.Append(obj.AppendRecur(Recur.Value, bufferxml, lstConferenceTZ.SelectedValue));
                }
                if (enableCloudInstallation == 1)
                    inxml.Append("<description>" + utilObj.ReplaceInXMLSpecialCharacters(txtDescription_Exp.Text) + "</description>"); 
                else
                    inxml.Append("<description>" + utilObj.ReplaceInXMLSpecialCharacters(ConferenceDescription.Text) + "</description>");

                if (chkPublic.Checked)
                {
                    inxml.Append("<publicConf>1</publicConf>");
                    if (chkOpenForRegistration.Checked)
                        inxml.Append("<dynamicInvite>1</dynamicInvite>");
                    else
                        inxml.Append("<dynamicInvite>0</dynamicInvite>");
                }
                else
                {
                    inxml.Append("<publicConf>0</publicConf>");
                    inxml.Append("<dynamicInvite>0</dynamicInvite>");
                }

                #region Load Locations


                inxml.Append("<locationList>");

                //New Work Starts
                inxml.Append("<Type>R</Type>");

                bool isError = false; int roomID;
                string[] locs = selectedloc.Value.Split(',');
                SelRooms = new List<int>();
                bool isCaller = false;
                Boolean isRoomExists = false;//ZD 103243

                for (int i = 0; i < locs.Length; i++)
                {
                    if (string.IsNullOrEmpty(locs[i]) || locs[i].Trim() == "")
                        continue;

                    isRoomExists = true;//ZD 103243

                    int.TryParse(locs[i], out roomID);
                    if (!SelRooms.Contains(roomID))
                        SelRooms.Add(roomID);
                    else
                        continue;

                    if (lstVMR.SelectedIndex == 0 || lstVMR.SelectedValue == "2")
                        dtRooms.Rows.Add(AddRoomEndpoint(locs[i], dtRooms, ref isError, isCaller));

                    if (isError)
                    {
                        Session["hdModalDiv"] = "none";
                        errLabel.Visible = true;
                        return false;
                    }
                }

                //ZD 103243
                if (lstConferenceType.SelectedValue == "7")
                {
                    if (!isRoomExists)
                    {
                        Session["hdModalDiv"] = "none";
                        errLabel.Text = obj.GetTranslatedText("Please select at least one room to the conference.");
                        errLabel.Visible = true;
                        return false;
                    }
                }

                for (int i = 0; i < dtRooms.Rows.Count; i++)
                {
                    if (VMRBridge != "0")
                        dtRooms.Rows[i]["BridgeID"] = VMRBridge;

                    if (usrCnt == 0)
                    {
                        usrCnt = usrCnt + 1;
                        if ((dtRooms.Rows[i]["AddressType"].ToString().Trim() == "1") && (dtRooms.Rows[i]["IsTestEquipment"].ToString().Trim() == "1"))
                        {
                            isCaller = true;
                            dtRooms.Rows[i]["Connect2"] = "1";
                        }
                        else
                        {
                            isCaller = false;
                            dtRooms.Rows[i]["Connect2"] = "0";
                        }
                    }
                    else if (usrCnt == 1)
                    {
                        usrCnt = usrCnt + 1;
                        if (!isCaller)
                        {
                            if ((dtRooms.Rows[i]["AddressType"].ToString().Trim() == "1") && (dtRooms.Rows[i]["IsTestEquipment"].ToString().Trim() == "1"))
                                dtRooms.Rows[i]["Connect2"] = "1";
                            else
                            {
                                if (dtRooms.Rows[i]["IsTestEquipment"].ToString().Trim() != "1")
                                    isTestedModel.Value = "1";
                                if (dtRooms.Rows[i]["AddressType"].ToString().Trim() != "1")
                                    isIPAddress.Value = "1";
                                hdnSmartP2PTotalEps.Value = "0";
                            }

                        }
                        else
                            dtRooms.Rows[i]["Connect2"] = "0";
                    }
                }

                if (dtRooms != null && dtRooms.Rows.Count > 0 && (lstVMR.SelectedIndex == 0 || lstVMR.SelectedValue == "2") && !chkPCConf.Checked && (lstConferenceType.SelectedValue.Equals(ns_MyVRMNet.vrmConfType.AudioOnly) || lstConferenceType.SelectedValue.Equals(ns_MyVRMNet.vrmConfType.OBTP) || lstConferenceType.SelectedValue.Equals(ns_MyVRMNet.vrmConfType.AudioVideo)))
                {
                    if (!ValidateBridgeAdmin(dtRooms))
                    {
                        Session["hdModalDiv"] = "none";
                        errLabel.Visible = true;
                        return false;
                    }
                }
                for (int t = 0; t < locs.Length; t++)
                {
					//ZD 103191
                    string[] ProfileList = hdnMcuProfileSelected.Value.Split('$');
                    string bridgeProfileID = "", BridgeValue = "";
                    DropDownList drpTemp = null; string[] attributeDt;
                    if (ProfileList.Length > 0)
                    {
                        for (int s = 0; s < ProfileList.Length - 1; s++)
                        {
                            BridgeValue = ProfileList[s];
                            attributeDt = BridgeValue.Split('?');
                            if (attributeDt.Length > 0)
                            {
                                bridgeId = attributeDt[0];
                                bridgeProfileID = attributeDt[1];
                                drpTemp = (DropDownList)tblMCUProfile.FindControl(bridgeProfileID);
                                if (drpTemp != null)
                                {
                                    optionId = drpTemp.SelectedValue;
                                    optionValue = drpTemp.SelectedItem.Text;
                                }
                                if (optionId == "")
                                    optionValue = "";

                                if (!mculidt.Contains(bridgeId))
                                    mculidt.Add(bridgeId, optionId);
                            }
                        }
                    }

                    //ZD 104256 Starts
                    string[] PoolOrderList = hdnMcuPoolOrder.Value.Split('$');
                    string bridgePoolOrder = "";
                    DropDownList drpPOtmp = null;
                    if (PoolOrderList.Length > 0)
                    {
                        for (int s = 0; s < PoolOrderList.Length - 1; s++)
                        {
                            BridgeValue = PoolOrderList[s];
                            attributeDt = BridgeValue.Split('?');
                            if (attributeDt.Length > 0)
                            {
                                bridgeId = attributeDt[0];
                                bridgePoolOrder = attributeDt[1];
                                drpPOtmp = (DropDownList)tblMCUProfile.FindControl(bridgePoolOrder);
                                if (drpPOtmp != null)
                                {
                                    optionId = drpPOtmp.SelectedValue;
                                    optionValue = drpPOtmp.SelectedItem.Text;
                                }
                                if (optionId == "")
                                    optionValue = "";

                                if (!MCUPoolOrder.Contains(bridgeId))
                                    MCUPoolOrder.Add(bridgeId, optionId);
                            }
                        }
                    }
                    //ZD 104256 Ends

                    if (string.IsNullOrEmpty(locs[t]) || locs[t].Trim() == "")
                        continue;
                    int.TryParse(locs[t], out roomID);

                    //ZD 103216
                    Hashtable RmHostAttendee = new Hashtable();
                    if (Session["RoomHostDetails"] != null)
                        RmHostAttendee = (Hashtable)Session["RoomHostDetails"];


                    inxml.Append("<Location>");
                    inxml.Append("<ID>" + roomID + "</ID>");

                    if (!(lstConferenceType.SelectedValue.Equals(ns_MyVRMNet.vrmConfType.RoomOnly) || lstConferenceType.SelectedValue.Equals(ns_MyVRMNet.vrmConfType.HotDesking) || (lstVMR.SelectedIndex > 0 && lstVMR.SelectedValue != "2") || (chkPCConf.Checked)))
                    {
                        for (int i = 0; i < dtRooms.Rows.Count; i++)
                        {
                            if (roomID.ToString() != dtRooms.Rows[i]["ID"].ToString().Trim())
                                continue;
                            
                            inxml.Append("<UseDefault>0</UseDefault>");
                            inxml.Append("<IsLecturer>0</IsLecturer>");
                            inxml.Append("<EndpointID>" + dtRooms.Rows[i]["EndpointID"].ToString().Trim() + "</EndpointID>");
                            inxml.Append("<ProfileID>" + dtRooms.Rows[i]["ProfileID"].ToString().Trim() + "</ProfileID>");
                            inxml.Append("<BridgeID>" + dtRooms.Rows[i]["BridgeID"].ToString().Trim() + "</BridgeID>");

                            BID = dtRooms.Rows[i]["BridgeID"].ToString().Trim();
                            IDictionaryEnumerator iEnum = mculidt.GetEnumerator();
                            while (iEnum.MoveNext())
                            {
                                if (BID == iEnum.Key.ToString())
                                {
                                    optionId = iEnum.Value.ToString();
                                    break;
                                }
                                else
                                    optionId = "";
                            }
                          

                            inxml.Append("<BridgeProfileID>" + optionId + "</BridgeProfileID>");

                            //ZD 104256 Starts
                            iEnum = MCUPoolOrder.GetEnumerator();
                            while (iEnum.MoveNext())
                            {
                                if (BID == iEnum.Key.ToString())
                                {
                                    optionId = iEnum.Value.ToString();
                                    break;
                                }
                                else
                                    optionId = "";
                            }
                            inxml.Append("<BridgePoolOrder>" + optionId + "</BridgePoolOrder>");
                            //ZD 104256 Ends

                            inxml.Append("<AddressType></AddressType>");
                            inxml.Append("<Address></Address>");
                            inxml.Append("<VideoEquipment></VideoEquipment>");
                            inxml.Append("<connectionType></connectionType>");
                            inxml.Append("<Bandwidth></Bandwidth>");
                            inxml.Append("<IsOutside></IsOutside>");
                            inxml.Append("<GateKeeeperAddress></GateKeeeperAddress>");
                            inxml.Append("<DefaultProtocol></DefaultProtocol>");
                            inxml.Append("<Connection>2</Connection>"); //Default to AudioVideo
                            inxml.Append("<URL></URL>");
                            inxml.Append("<ExchangeID></ExchangeID>");
                            inxml.Append("<APIPortNo>23</APIPortNo>");
                            if (lstConferenceType.SelectedValue == ns_MyVRMNet.vrmConfType.P2P)
                            {
                                inxml.Append("<Connect2>" + dtRooms.Rows[i]["Connect2"].ToString().Trim() + "</Connect2>");
                            }
                            else
                                inxml.Append("<Connect2>-1</Connect2>");

                            //ZD 103216
                            if (RmHostAttendee.ContainsKey(dtRooms.Rows[i]["ID"].ToString().Trim()))
                            {
                                string strKey = "";
                                strKey = RmHostAttendee[dtRooms.Rows[i]["ID"].ToString().Trim()].ToString();
                                inxml.Append("<HostRoom>" + strKey.Split('|')[0] + "</HostRoom>");
                                inxml.Append("<NoofAttendee>" + strKey.Split('|')[1] + "</NoofAttendee>");
                            }
                            else
                            {
                                inxml.Append("<HostRoom>0</HostRoom>");
                                inxml.Append("<NoofAttendee>0</NoofAttendee>");
                            }
                        }
                    }
                    else
                    {
                        inxml.Append("<UseDefault>0</UseDefault>");
                        inxml.Append("<IsLecturer>0</IsLecturer>");
                        inxml.Append("<EndpointID></EndpointID>");
                        inxml.Append("<ProfileID></ProfileID>");
                        inxml.Append("<BridgeID></BridgeID>");
                        inxml.Append("<BridgeProfileID></BridgeProfileID>");
                        inxml.Append("<BridgePoolOrder></BridgePoolOrder>"); //ZD 104256
                        inxml.Append("<AddressType></AddressType>");
                        inxml.Append("<Address></Address>");
                        inxml.Append("<VideoEquipment></VideoEquipment>");
                        inxml.Append("<connectionType></connectionType>");
                        inxml.Append("<Bandwidth></Bandwidth>");
                        inxml.Append("<IsOutside></IsOutside>");
                        inxml.Append("<GateKeeeperAddress></GateKeeeperAddress>");
                        inxml.Append("<DefaultProtocol></DefaultProtocol>");
                        inxml.Append("<Connection>2</Connection>");
                        inxml.Append("<URL></URL>");
                        inxml.Append("<ExchangeID></ExchangeID>");
                        inxml.Append("<APIPortNo>23</APIPortNo>");
                        inxml.Append("<Connect2>-1</Connect2>");

                        //ZD 103216
                        if (RmHostAttendee.ContainsKey(roomID.ToString().Trim()))
                        {
                            string strKey = "";
                            strKey = RmHostAttendee[roomID.ToString().Trim()].ToString();
                            inxml.Append("<HostRoom>" + strKey.Split('|')[0] + "</HostRoom>");
                            inxml.Append("<NoofAttendee>" + strKey.Split('|')[1] + "</NoofAttendee>");
                        }
                        else
                        {
                            inxml.Append("<HostRoom>0</HostRoom>");
                            inxml.Append("<NoofAttendee>0</NoofAttendee>");
                        }
                    }
                    inxml.Append("</Location>");
                }
                inxml.Append("</locationList>");

                #endregion
                
                #region Load Guest Locations

                string Roomid = "";
                inxml.Append("<ConfGuestRooms>");
                inxml.Append("<Type>GL</Type>");
                GuestLocMCU = new List<int>();
                
                if (!lstConferenceType.SelectedValue.Equals(ns_MyVRMNet.vrmConfType.RoomOnly) && !lstConferenceType.SelectedValue.Equals(ns_MyVRMNet.vrmConfType.HotDesking))
                {
                    int j = 0, x = 0, isDeft = 0, GuestPfRow = 0;
                    foreach (DataGridItem item1 in dgOnflyGuestRoomlist.Items)
                    {
                        inxml.Append("<ConfGuestRoom>");
                        inxml.Append("<LoginUserID>" + Session["userID"].ToString() + "</LoginUserID>");
                        inxml.Append(obj.OrgXMLElement());
                        inxml.Append("<GuestRoomUID>" + item1.Cells[0].Text.ToString() + "</GuestRoomUID>");
                        int.TryParse(item1.Cells[0].Text.ToString(), out GuestPfRow);
                        Roomid = item1.Cells[1].Text.ToString().Replace("&nbsp;", "");
                        if (Roomid == "")
                            Roomid = "new";
                        inxml.Append("<GuestRoomID>" + Roomid + "</GuestRoomID>");
                        inxml.Append("<GuestRoomName>" + item1.Cells[2].Text.ToString() + "</GuestRoomName>");
                        inxml.Append("<ContactName>" + item1.Cells[3].Text.ToString().Replace("&nbsp;", "") + "</ContactName>");
                        inxml.Append("<ContactEmail>" + item1.Cells[4].Text.ToString().Replace("&nbsp;", "") + "</ContactEmail>");
                        inxml.Append("<ContactPhoneNo>" + item1.Cells[5].Text.ToString().Replace("&nbsp;", "") + "</ContactPhoneNo>");
                        inxml.Append("<GuestContactPhoneNo>" + item1.Cells[12].Text.ToString().Trim().Replace("&nbsp;", "") + "</GuestContactPhoneNo>");
                        inxml.Append("<GuestAdmin>" + item1.Cells[14].Text.ToString().Replace("&nbsp;", "") + "</GuestAdmin>");
                        inxml.Append("<GuestAdminID>" + item1.Cells[15].Text.ToString().Replace("&nbsp;", "") + "</GuestAdminID>");

                        if (item1.Cells[4].Text == item1.Cells[14].Text)
                            inxml.Append("<IsGuestAdmin>1</IsGuestAdmin>");
                        else
                            inxml.Append("<IsGuestAdmin>0</IsGuestAdmin>");
                        inxml.Append("<DftConnecttype>" + item1.Cells[13].Text.ToString().Replace("&nbsp;", "") + "</DftConnecttype>");
                        
                        inxml.Append("<RoomAddress>" + item1.Cells[6].Text.ToString().Replace("&nbsp;", "") + "</RoomAddress>");
                        inxml.Append("<State>" + item1.Cells[7].Text.ToString().Replace("&nbsp;", "") + "</State>");
                        inxml.Append("<City>" + item1.Cells[8].Text.ToString().Replace("&nbsp;", "") + "</City>");//To be done 6
                        inxml.Append("<ZipCode>" + item1.Cells[9].Text.ToString().Replace("&nbsp;", "") + "</ZipCode>");
                        inxml.Append("<Country>" + item1.Cells[10].Text.ToString().Replace("&nbsp;", "") + "</Country>");
                        inxml.Append("<Tier1>" + Session["OnflyTopTierID"].ToString().Replace("&nbsp;", "") + "</Tier1>");
                        inxml.Append("<Tier2>" + Session["OnflyMiddleTierID"].ToString().Replace("&nbsp;", "") + "</Tier2>");
                        inxml.Append("<GuestConfTimezone>" + lstConferenceTZ.SelectedValue + "</GuestConfTimezone>");
                        inxml.Append("<Profiles>");
                        
                        BindProfileOptionData(GuestPfRow);
                        TextBox txtTemp = null;
                        foreach (DataGridItem dpitem in dgProfiles.Items)
                        {
                            j++; isDeft = 0;
                            txtTemp = (TextBox)(dpitem.FindControl("txtGuestAddress"));
                            inxml.Append("<Profile>");
                            inxml.Append("<EndpointName>" + item1.Cells[2].Text.ToString() + "</EndpointName>");
                            inxml.Append("<ProfileName>" + item1.Cells[2].Text.ToString() + "_" + j + "</ProfileName>");
                            inxml.Append("<AddressType>" + ((DropDownList)dpitem.FindControl("lstGuestAddressType")).SelectedValue + "</AddressType>");
                            inxml.Append("<Address>" + txtTemp.Text + "</Address>");
                            inxml.Append("<MaxLineRate>" + ((DropDownList)dpitem.FindControl("lstGuestLineRate")).SelectedValue + "</MaxLineRate>");

                            if (((RadioButton)dpitem.FindControl("rdDefault")).Checked)
                                isDeft = 1;
                            inxml.Append("<isDefault>" + isDeft + "</isDefault>");

                            if (isDeft == 1)
                                inxml.Append("<ConnectionType>" + item1.Cells[13].Text.ToString() + "</ConnectionType>");
                            else
                                inxml.Append("<ConnectionType>" + ((DropDownList)dpitem.FindControl("lstGuestConnectionType1")).SelectedValue + "</ConnectionType>");

                            inxml.Append("<BridgeID>" + ((DropDownList)dpitem.FindControl("lstGuestBridges")).SelectedValue + "</BridgeID>");
                            int.TryParse(((DropDownList)dpitem.FindControl("lstGuestBridges")).SelectedValue, out GLMCU);
                            if (!GuestLocMCU.Contains(GLMCU))
                                GuestLocMCU.Add(GLMCU);
                            isDeft = 0;
                            if (((CheckBox)dpitem.FindControl("chkDelete")).Checked)
                                isDeft = 1;
                            inxml.Append("<Deleted>" + isDeft + "</Deleted>");
                            inxml.Append("</Profile>");

                        }
                        x++;
                        
                        inxml.Append("</Profiles>");
                        inxml.Append("</ConfGuestRoom>");
                    }
                }
                inxml.Append("</ConfGuestRooms>");

                #endregion

                #region Load AdvAvParams
                inxml.Append("<advAVParam>");
                inxml.Append("<maxAudioPart></maxAudioPart>");
                inxml.Append("<maxVideoPart></maxVideoPart>");
                inxml.Append("<restrictProtocol>3</restrictProtocol>"); //Restrict Network access to IP,ISDN,SIP
                inxml.Append("<restrictAV>2</restrictAV>"); //Restrict Usage default to AudioVideo
                inxml.Append("<videoLayout>" + txtSelectedImage.Text + "</videoLayout>");
                inxml.Append("<FamilyLayout>" + hdnFamilyLayout.Value + "</FamilyLayout>");
                inxml.Append("<maxLineRateID>" + lstLineRate.SelectedValue + "</maxLineRateID>");
                inxml.Append("<audioCodec>0</audioCodec>");
                inxml.Append("<videoCodec>0</videoCodec>");
                inxml.Append("<dualStream>1</dualStream>");
                inxml.Append("<confOnPort>0</confOnPort>");
                inxml.Append("<encryption>0</encryption>");
                inxml.Append("<lectureMode>0</lectureMode>");
                inxml.Append("<VideoMode>3</VideoMode>");
                inxml.Append("<SingleDialin>0</SingleDialin>");
                inxml.Append("<internalBridge>" + hdnintbridge.Value + "</internalBridge>");
                inxml.Append("<externalBridge>" + hdnextbridge.Value + "</externalBridge>");
                if (!(lstVMR.SelectedIndex > 0) && chkFECC.Checked && (lstConferenceType.SelectedValue.Equals(ns_MyVRMNet.vrmConfType.AudioVideo) || lstConferenceType.SelectedValue.Equals(ns_MyVRMNet.vrmConfType.OBTP) || lstConferenceType.SelectedValue.Equals(ns_MyVRMNet.vrmConfType.AudioVideo)))
                    inxml.Append("<FECCMode>1</FECCMode>");
                else
                    inxml.Append("<FECCMode>0</FECCMode>");
                inxml.Append("<PolycomSendMail>0</PolycomSendMail>");
                inxml.Append("<PolycomTemplate></PolycomTemplate>");
                inxml.Append("</advAVParam>");
                #endregion

                #region Load Participant

                string[] partyary = null;
                string sendemail = "1", partyNotify = "", survey = "";

                int partyid = 0;
                int partyInvite = 2; // Room Attendee
                int partyAudVid = 2; // Media type  - Video 

                inxml.Append("<ParticipantList>");
                inxml.Append("<Type>U</Type>");

                if (lstConferenceType.SelectedValue == ns_MyVRMNet.vrmConfType.AudioOnly)
                    partyAudVid = 1;// Media type  - Audio
                
                string[] partysary = txtPartysInfo.Text.Split(pipeDelim, StringSplitOptions.RemoveEmptyEntries);
                int partynum = partysary.Length;
                string[] strparty = hdnSelectedAudioDetails.Value.Split('|'); //ALLDEV-814
                
                if (lstConferenceType.SelectedValue.Equals(ns_MyVRMNet.vrmConfType.HotDesking))
                {
                    txtPartysInfo.Text = "";
                    partynum = 0;
                }

                for (int i = 0; i < partynum; i++) 
                {
                    partyary = partysary[i].Split(ExclamDelim, StringSplitOptions.None);

                    partyInvite = -1;
                    partyAudVid = -1;
                    if (partyary[4].Equals("1"))
                        partyInvite = 1;
                    if (partyary[5].Equals("1"))
                        partyInvite = 2;
                    if (partyary[6].Equals("1"))
                        partyInvite = 0;
                    if (partyary[18] != null)
                    {
                        if (partyary[18].Equals("1"))
                            partyInvite = 4;
                    }
                    partyNotify = partyary[7];
                    if (partyary[8].Equals("1"))
                        partyAudVid = 2;
                    else
                        partyAudVid = 1;

                    survey = partyary[16];
                    
                    if (partyary[17].Equals("1"))
                        partyInvite = 3;
                    
                    if (partyInvite == -1)
                        partyInvite = 0;

                    string UserID = partyary[0].ToString();
                    if (UserID.Trim().IndexOf("new") >= 0)
                        UserID = "new";

                 
                    if (enableDetailedExpressForm == 0)
                    {
                        partyNotify = "1";
                        partyInvite = 2;
                        survey = "0";
                        if (lstConferenceType.SelectedValue == ns_MyVRMNet.vrmConfType.AudioOnly)
                            partyAudVid = 1;// Media type  - Audio
                        else
                            partyAudVid = 2;
                    }

                    inxml.Append("<party>");
                    inxml.Append("<partyID>" + UserID + "</partyID>");
                    inxml.Append("<partyFirstName>" + obj.ControlConformityCheck(partyary[1].ToString().Replace("++", ",")) + "</partyFirstName>");
                    inxml.Append("<partyLastName>" + obj.ControlConformityCheck(partyary[2].ToString().Replace("++", ",")) + "</partyLastName>");
                    inxml.Append("<partyEmail>" + partyary[3].ToString() + "</partyEmail>");
                    inxml.Append("<partyInvite>" + partyInvite + "</partyInvite>");
                    inxml.Append("<partyNotify>" + partyNotify + "</partyNotify>");
                    inxml.Append("<partyAudVid>" + partyAudVid + "</partyAudVid>");
                    inxml.Append("<notifyOnEdit>" + sendemail + "</notifyOnEdit>"); 
                    inxml.Append("<survey>" + survey + "</survey>");
                    //ZD 102916 Start
                    string[] roomassigned = null;
                    roomassigned = partyary[19].ToString().Split('$');
                    if (roomassigned != null)
                    {
                        if (roomassigned.Count() > 1)
                        {
                            inxml.Append("<partyPublicVMR>" + roomassigned[0] + "</partyPublicVMR>");
                            inxml.Append("<partyAssignedRoom>" + roomassigned[1] + "</partyAssignedRoom>");
                        }
                        else
                        {
                            inxml.Append("<partyPublicVMR>" + roomassigned[0] + "</partyPublicVMR>");
                            inxml.Append("<partyAssignedRoom></partyAssignedRoom>");
                        }
                    }
                    else
                    {
                        inxml.Append("<partyPublicVMR>" + partyary[19].ToString() + "</partyPublicVMR>");
                        inxml.Append("<partyAssignedRoom></partyAssignedRoom>");
                    }
                    //ZD 102916 End

                    string[] ProfileList = hdnMcuProfileSelected.Value.Split('$');
                    string bridgeProfileID = "", BridgeValue = "";
                    DropDownList drpTemp = null; string[] attributeDt;
                    if (ProfileList.Length > 0)
                    {
                        for (int s = 0; s < ProfileList.Length - 1; s++)
                        {
                            BridgeValue = ProfileList[s];
                            attributeDt = BridgeValue.Split('?');
                            if (attributeDt.Length > 0)
                            {
                                bridgeId = attributeDt[0];
                                bridgeProfileID = attributeDt[1];
                                drpTemp = (DropDownList)tblMCUProfile.FindControl(bridgeProfileID);
                                if (drpTemp != null)
                                {
                                    optionId = drpTemp.SelectedValue;
                                    optionValue = drpTemp.SelectedItem.Text;
                                }
                                if (optionId == "")
                                    optionValue = "";

                                if (!mculidt.Contains(bridgeId))
                                    mculidt.Add(bridgeId, optionId);
                            }
                        }
                    }

                    //ZD 104256 Starts
                    string[] PoolOrderList = hdnMcuPoolOrder.Value.Split('$');
                    string bridgePoolOrder = "";
                    DropDownList drpPOtmp = null;
                    if (PoolOrderList.Length > 0)
                    {
                        for (int s = 0; s < PoolOrderList.Length - 1; s++)
                        {
                            BridgeValue = PoolOrderList[s];
                            attributeDt = BridgeValue.Split('?');
                            if (attributeDt.Length > 0)
                            {
                                bridgeId = attributeDt[0];
                                bridgePoolOrder = attributeDt[1];
                                drpPOtmp = (DropDownList)tblMCUProfile.FindControl(bridgePoolOrder);
                                if (drpPOtmp != null)
                                {
                                    optionId = drpPOtmp.SelectedValue;
                                    optionValue = drpPOtmp.SelectedItem.Text;
                                }
                                if (optionId == "")
                                    optionValue = "";

                                if (!MCUPoolOrder.Contains(bridgeId))
                                    MCUPoolOrder.Add(bridgeId, optionId);
                            }
                        }
                    }
                    //ZD 104256 Ends
                    if (partyInvite == 1 && !(lstConferenceType.SelectedValue.Equals(ns_MyVRMNet.vrmConfType.RoomOnly) || lstConferenceType.SelectedValue.Equals(ns_MyVRMNet.vrmConfType.HotDesking)))
                    {
                        int rowCnt = 0;
                        bool flag = false;
                        DataRow userRow;
                        DataTable userDT = (DataTable)ViewState["userGrid"];
                        string PartyAddressType = "-1", dftProtocol = "-1";
                        foreach (DataGridItem dgi in dgUsers.Items)
                        {
							//ZD 103402 - Start
                            flag = false;
                            if (((Label)dgi.FindControl("lblUserName")).Text.Trim().ToUpper().IndexOf(partyary[3].ToString().Trim().ToUpper()) >= 0)
                                flag = true;

                            if ((isEditMode == "1" || isClone || isTemplate) && !flag && ((TextBox)dgi.FindControl("lblUserID")).Text.Trim().IndexOf("new") < 0) //ZD 103830
                                if (((TextBox)dgi.FindControl("lblUserID")).Text.Trim().ToUpper().IndexOf(partyary[0].ToString().Trim().ToUpper()) >= 0)
                                    flag = true;

                            rowCnt = 0;
                            optionId = "";
                            userRow = userDT.Rows[rowCnt];
                            
                            if (flag.Equals(true))
                            {
                                inxml.Append("<UseDefault>0</UseDefault>");
                                inxml.Append("<IsLecturer>0</IsLecturer>");
                                inxml.Append("<EndpointID></EndpointID>");
                                inxml.Append("<ProfileID></ProfileID>");
                                if (lstVMR.SelectedValue == "2" && Session["VMRBridge"] != null && Session["VMRBridge"].ToString() != "0")
                                    inxml.Append("<BridgeID>" + Session["VMRBridge"].ToString() + "</BridgeID>");
                                else
                                    inxml.Append("<BridgeID>" + ((DropDownList)dgi.FindControl("lstBridges")).SelectedValue + "</BridgeID>");

                                BID = ((DropDownList)dgi.FindControl("lstBridges")).SelectedValue;
                                IDictionaryEnumerator iEnum = mculidt.GetEnumerator();
                                while (iEnum.MoveNext())
                                {
                                    if (BID == iEnum.Key.ToString())
                                    {
                                        optionId = iEnum.Value.ToString();
                                        break;
                                    }
                                    else
                                        optionId = "";
                                }

                                inxml.Append("<BridgeProfileID>" + optionId + "</BridgeProfileID>");

                                //ZD 104256 Starts
                                iEnum = MCUPoolOrder.GetEnumerator();
                                while (iEnum.MoveNext())
                                {
                                    if (BID == iEnum.Key.ToString())
                                    {
                                        optionId = iEnum.Value.ToString();
                                        break;
                                    }
                                    else
                                        optionId = "";
                                }
                                inxml.Append("<BridgePoolOrder>" + optionId + "</BridgePoolOrder>");
                                //ZD 104256 Ends

                                PartyAddressType = ((DropDownList)dgi.FindControl("lstAddressType")).SelectedValue;
                                inxml.Append("<AddressType>" + PartyAddressType + "</AddressType>");
                                inxml.Append("<Address>" + obj.ControlConformityCheck(((TextBox)dgi.FindControl("txtAddress")).Text.Trim()) + "</Address>");
                                inxml.Append("<VideoEquipment>" + ((DropDownList)dgi.FindControl("lstVideoEquipment")).SelectedValue + "</VideoEquipment>");
                                inxml.Append("<connectionType>" + ((DropDownList)dgi.FindControl("lstConnectionType")).SelectedValue + "</connectionType>");
                                inxml.Append("<Bandwidth>" + ((DropDownList)dgi.FindControl("lstLineRate")).SelectedValue + "</Bandwidth>");
                                inxml.Append("<IsOutside>" + userRow["IsOutside"].ToString() + "</IsOutside>");
                                dftProtocol = userRow["DefaultProtocol"].ToString();
                                if (PartyAddressType == "1" || PartyAddressType == "2")
                                    dftProtocol = "1";
                                else if (PartyAddressType == "3" || PartyAddressType == "4")
                                    dftProtocol = "2";
                                else if (PartyAddressType == "6")
                                    dftProtocol = "3";
                                else if (PartyAddressType == "5")
                                    dftProtocol = "4";
                                inxml.Append("<DefaultProtocol>" + dftProtocol + "</DefaultProtocol>");
                                inxml.Append("<Connection>" + ((DropDownList)dgi.FindControl("lstConnection")).SelectedValue + "</Connection>");
                                inxml.Append("<URL>" + userRow["URL"].ToString() + "</URL>");
                                inxml.Append("<ExchangeID>" + userRow["ExchangeID"].ToString() + "</ExchangeID>");
                                if (userRow["ApiPortno"].ToString() != "")
                                    inxml.Append("<APIPortNo>" + userRow["ApiPortno"].ToString() + "</APIPortNo>");
                                else
                                    inxml.Append("<APIPortNo>23</APIPortNo>");
                                if (lstConferenceType.SelectedValue.Equals(ns_MyVRMNet.vrmConfType.P2P))
                                    inxml.Append("<Connect2>" + ((DropDownList)dgi.FindControl("lstTelnetUsers")).SelectedValue + "</Connect2>");
                                else
                                    inxml.Append("<Connect2>-1</Connect2>");
                                inxml.Append("<participantCode></participantCode>");
                                inxml.Append("<ConferenceCode></ConferenceCode>"); //ALLDEV-814
                                inxml.Append("<LeaderPin></LeaderPin>");
                                //ALLDEV-814 - Start
                                if (hdnSelectedAudioDetails.Value != "")
                                {
                                    inxml.Append("<partyProfileID>" + strparty[8] + "</partyProfileID>");
                                    inxml.Append("<partyUID>0</partyUID>");
                                }
                                else
                                {
                                    inxml.Append("<partyProfileID>1</partyProfileID>");
                                    inxml.Append("<partyUID>0</partyUID>");
                                }
                                //ALLDEV-814 - End
                                break;
                            }
                            rowCnt++;
                        }
                    }
                    else
                    {
                        inxml.Append("<UseDefault>1</UseDefault>");
                        inxml.Append("<IsLecturer>0</IsLecturer>");
                        inxml.Append("<EndpointID></EndpointID>");
                        inxml.Append("<ProfileID></ProfileID>");
                        inxml.Append("<BridgeID></BridgeID>");
                        inxml.Append("<BridgeProfileID></BridgeProfileID>");
                        inxml.Append("<BridgePoolOrder></BridgePoolOrder>"); //ZD 104256
                        inxml.Append("<AddressType></AddressType>");
                        inxml.Append("<Address></Address>");
                        inxml.Append("<VideoEquipment>0</VideoEquipment>");
                        inxml.Append("<connectionType>0</connectionType>");
                        inxml.Append("<Bandwidth></Bandwidth>");
                        inxml.Append("<IsOutside>0</IsOutside>");
                        inxml.Append("<DefaultProtocol>0</DefaultProtocol>");
                        inxml.Append("<Connection></Connection>");
                        inxml.Append("<URL></URL>");
                        inxml.Append("<ExchangeID></ExchangeID>");
                        inxml.Append("<APIPortNo>23</APIPortNo>");
                        inxml.Append("<Connect2>0</Connect2>");
                        inxml.Append("<participantCode></participantCode>");
                        inxml.Append("<partyProfileID>1</partyProfileID>");//ALLDEV-814
                        inxml.Append("<partyUID>0</partyUID>");//ALLDEV-814
                        inxml.Append("<ConferenceCode></ConferenceCode>");//ALLDEV-814
                        inxml.Append("<LeaderPin></LeaderPin>");//ALLDEV-814
                    }
                    inxml.Append("</party>");
                }
                
                audioDialString = "";
                audioPartyId = 0;
                audioAddressType = 1; audioProtocol = 1; //ZD 103574 
                if (!lstConferenceType.SelectedValue.Equals(ns_MyVRMNet.vrmConfType.HotDesking) && lstAudioParty.SelectedValue != "-1" && lstAudioParty.SelectedValue != "")
                {
                    string[] audioIns = lstAudioParty.SelectedValue.Split('|');

                    if (audioIns.Length > 6) //ZD 103574 
                    {
                        int.TryParse(audioIns[0], out audioPartyId);
                        int.TryParse(audioIns[6], out audioAddressType); 
                        int.TryParse(audioIns[7], out audioProtocol);
                        if (audioPartyId > 0)
                        {
                            audioDialString = txtAudioDialNo.Text.Trim() + "D" + txtConfCode.Text.Trim() + "+" + txtLeaderPin.Text.Trim();

                            string[] partyName = lstAudioParty.SelectedItem.Text.Split(' ');
                            string lastName = "";
                            if (partyName.Length > 1)
                                lastName = partyName[1];

                            partyInvite = 1; // Default to External Attendee
                            partyAudVid = 1; // Default to Media type Audio

                            inxml.Append("<party>");
                            inxml.Append("<partyID>" + audioPartyId + "</partyID>");
                            inxml.Append("<partyFirstName>" + partyName[0] + "</partyFirstName>");
                            inxml.Append("<partyLastName>" + lastName + "</partyLastName>");
                            inxml.Append("<partyEmail>" + audioIns[1] + "</partyEmail>");
                            inxml.Append("<partyInvite>" + partyInvite + "</partyInvite>");
                            inxml.Append("<partyNotify>1</partyNotify>");
                            inxml.Append("<partyAudVid>" + partyAudVid + "</partyAudVid>");
                            inxml.Append("<notifyOnEdit>" + sendemail + "</notifyOnEdit>");
                           
                            if (Session["ConfEndPts"] != null)
                                usrEndpoints = (Hashtable)Session["ConfEndPts"];

                            if (audioPartyId > 0)
                            {
                                bridgeId = "-1";
                                if (usrEndpoints != null)
                                {
                                    if (usrEndpoints.Contains(audioPartyId))
                                    {
                                        bridgeId = usrEndpoints[audioPartyId].ToString().Trim();
                                    }
                                }
                            }
                            inxml.Append("<survey>0</survey>");
                            inxml.Append("<partyPublicVMR>0</partyPublicVMR>");
                            inxml.Append("<partyAssignedRoom></partyAssignedRoom>");//ZD 102916
                            inxml.Append("<UseDefault>1</UseDefault>"); //Default Profile
                            inxml.Append("<IsLecturer>0</IsLecturer>");
                            inxml.Append("<EndpointID></EndpointID>");
                            inxml.Append("<ProfileID></ProfileID>");
                            inxml.Append("<AddressType>" + audioAddressType + "</AddressType>"); //Set as ISDN //ZD 103574  - Fetched from Audio Add On Bridge
                            inxml.Append("<Address>" + audioDialString + "</Address>");
                            inxml.Append("<BridgeID>" + bridgeId + "</BridgeID>");
                            inxml.Append("<BridgeProfileID></BridgeProfileID>");
                            inxml.Append("<BridgePoolOrder></BridgePoolOrder>"); //ZD 104256
                            inxml.Append("<Connection>1</Connection>"); //Default to Audio
                            inxml.Append("<VideoEquipment>-1</VideoEquipment>");
                            inxml.Append("<connectionType>2</connectionType>"); //Default to Dial-out
                            inxml.Append("<Bandwidth>64</Bandwidth>"); //ZD 104235
                            //inxml.Append("<Bandwidth>" + lstLineRate.SelectedValue + "</Bandwidth>");
                            inxml.Append("<IsOutside>1</IsOutside>");
                            inxml.Append("<DefaultProtocol>" + audioProtocol + "</DefaultProtocol>");//Default to ISDN protocol //ZD 103574  - Fetched from Audio Add On Bridge
                            inxml.Append("<URL></URL>");
                            if (lstConferenceType.SelectedValue == ns_MyVRMNet.vrmConfType.P2P)
                                inxml.Append("<Connect2>1</Connect2>");
                            else
                                inxml.Append("<Connect2>-1</Connect2>");
                            inxml.Append("<APIPortNo>23</APIPortNo>");
                            inxml.Append("<ExchangeID></ExchangeID>");
                            if (enablePartCode == "0")
                                txtPartyCode.Text = "";
                            inxml.Append("<participantCode>" + txtPartyCode.Text + "</participantCode>");//ZD 101446
                            inxml.Append("<ConferenceCode>" + txtConfCode.Text + "</ConferenceCode>");//ALLDEV-814
                            inxml.Append("<LeaderPin>" + txtLeaderPin.Text + "</LeaderPin>");//ALLDEV-814
                            //ALLDEV-814 - Start
                            if (hdnSelectedAudioDetails.Value != "")
                            {
                                inxml.Append("<partyProfileID>" + strparty[8] + "</partyProfileID>");
                                inxml.Append("<partyUID>0</partyUID>");
                            }
                            else
                            {
                                inxml.Append("<partyProfileID>1</partyProfileID>");
                                inxml.Append("<partyUID>0</partyUID>");
                            }
                            //ALLDEV-814 - End
                            
                            inxml.Append("</party>");
                        }
                    }
                }
                inxml.Append("</ParticipantList>");
                #endregion

                if (Recur.Value.Trim() != "")
                    txtModifyType.Text = "1";
                else
                    txtModifyType.Text = "0";

                inxml.Append("<ModifyType>" + txtModifyType.Text + "</ModifyType>");
                inxml.Append("<fileUpload>");
                inxml.Append("<file>" + hdnUpload1.Text + "</file>");
                inxml.Append("<file>" + hdnUpload2.Text + "</file>");
                inxml.Append("<file>" + hdnUpload3.Text + "</file>");
                inxml.Append("</fileUpload>");

                #region Load Concierge Support

                inxml.Append("<ConciergeSupport>");

                if (chkOnSiteAVSupport.Checked)
                    inxml.Append("<OnSiteAVSupport>1</OnSiteAVSupport>");
                else
                    inxml.Append("<OnSiteAVSupport>0</OnSiteAVSupport>");

                if (chkMeetandGreet.Checked)
                    inxml.Append("<MeetandGreet>1</MeetandGreet>");
                else
                    inxml.Append("<MeetandGreet>0</MeetandGreet>");

                if (chkConciergeMonitoring.Checked)
                    inxml.Append("<ConciergeMonitoring>1</ConciergeMonitoring>");
                else
                    inxml.Append("<ConciergeMonitoring>0</ConciergeMonitoring>");

                if (hdnVNOCOperator.Text.Trim() != "" && txtVNOCOperator.Text.Trim() != "")
                {
                    string[] VNOCIDs = hdnVNOCOperator.Text.Split(',');
                    string[] VNOCName = txtVNOCOperator.Text.Split('\n');
                    inxml.Append("<DedicatedVNOCOperator>1</DedicatedVNOCOperator>");
                    inxml.Append("<VNOCAssignAdminID>" + Session["userID"].ToString() + "</VNOCAssignAdminID>");
                    inxml.Append("<ConfVNOCOperators>");
                    for (v = 0; v < VNOCIDs.Length; v++)
                        if (VNOCIDs[v] != "")
                            inxml.Append("<VNOCOperatorID>" + VNOCIDs[v] + "</VNOCOperatorID>");
                    inxml.Append("</ConfVNOCOperators>");
                }
                else
                {
                    if (chkDedicatedVNOCOperator.Checked)
                    {
                        Session["hdModalDiv"] = "none";
                        errLabel.Text = obj.GetTranslatedText("Please select Dedicated VNOC Operator.");
                        errLabel.Visible = true;
                        return false;
                    }
                    inxml.Append("<DedicatedVNOCOperator>0</DedicatedVNOCOperator>");
                    inxml.Append("<VNOCOperatorID></VNOCOperatorID>");
                    inxml.Append("<ConfVNOCOperators></ConfVNOCOperators>");
                }
                inxml.Append("</ConciergeSupport>");

                #endregion

                #region Load Custom Options

                inxml.Append("<CustomAttributesList>");

                if (CAObj == null)
                    CAObj = new myVRMNet.CustomAttributes();
                                
                //ZD 102909 - Start      
                //Session.Add("WrongEntityCode", "0"); //ZD 103265
                if (HttpContext.Current.Session["EntityCodesVal"] != null)
                {
                    if(HttpContext.Current.Session["EntityCodesVal"].ToString() != "")
                    {
                        dtEntityCode1 = (DataTable)HttpContext.Current.Session["EntityCodesVal"];

                        DataRow[] datrow = dtEntityCode1.Select("Caption like '" + hdnSelEntityCodeVal.Value + "'");

                        if (datrow.Count() > 0)
                        {
                            SelEntityCodeVal = datrow[0].ItemArray[0] + ":" + datrow[0].ItemArray[1];
                        }
                        else
                        {
                            //ZD 103265  start
                            if (hdnSelEntityCodeVal.Value != "")
                                SelEntityCodeVal = "new" + ":"  + hdnSelEntityCodeVal.Value;  
                               // Session.Add("WrongEntityCode", "1");
                        }
                    }
                }
                else
                {
                    if (HttpContext.Current.Session["EditEntityCodeVal"] != null)
                    {
                        if (HttpContext.Current.Session["EditEntityCodeVal"].ToString() != "")
                        {
                            dtEntityCode1 = obj.GetEntityCodes1(HttpContext.Current.Session["EditEntityCodeVal"].ToString());

                            DataRow[] datrow1 = dtEntityCode1.Select("Caption like '" + HttpContext.Current.Session["EditEntityCodeVal"].ToString() + "'");

                            if (datrow1.Count() > 0)
                                SelEntityCodeVal = datrow1[0].ItemArray[0] + ":" + datrow1[0].ItemArray[1];
                        }
                    }
                }

                string OrgEntityCodeVal = "";
                if (hdnSelEntityCodeVal.Value != "")
                {
                    OrgEntityCodeVal = hdnSelEntityCodeVal.Value;
                }
                else
                {
                    if (HttpContext.Current.Session["EditEntityCodeVal"] != null)
                        OrgEntityCodeVal = HttpContext.Current.Session["EditEntityCodeVal"].ToString();
                }

                //ZD 103265
                if (hdnRemoveEntityVal.Value == "" && SelEntityCodeVal == "")//ALLBUGS-138 When edit required message displayed even Entity code is filled.
                 {
                    HttpContext.Current.Session.Remove("EditEntityCodeVal");
                    SelEntityCodeVal = "";
                    OrgEntityCodeVal = "";
                }

                string custAtt = "";
                custAtt = CAObj.ExpressAttributesInxml(custControlIDs, tblHost, tblSpecial, tblCustomAttribute, SelEntityCodeVal, OrgEntityCodeVal);

                HttpContext.Current.Session.Remove("EditEntityCodeVal");
                //ZD 102909 - End

                if (custAtt.IndexOf("<error>") >= 0)
                {
                    XmlDocument xDoc = new XmlDocument();
                    xDoc.LoadXml(custAtt);
                    errLabel.Text = xDoc.SelectSingleNode("error").InnerText;
                    errLabel.Visible = true;
                    Session["hdModalDiv"] = "none";
                    return false;
                }
                else
                {
                    inxml.Append(custAtt);
                }
                inxml.Append("</CustomAttributesList>");

                #endregion

                inxml.Append("<ICALAttachment></ICALAttachment>");

                if (Application["Exchange"] != null && Application["Exchange"].ToString().ToUpper() == "YES")
                    inxml.Append("<isExchange>1</isExchange>");
                else if (Application["Exchange"] != null && Application["Exchange"].ToString().ToUpper() == "MOD")
                    inxml.Append("<isExchange>2</isExchange>");
                else
                    inxml.Append("<isExchange>0</isExchange>");
                
                if ((lstConferenceType.SelectedValue.Equals(ns_MyVRMNet.vrmConfType.AudioVideo) || lstConferenceType.SelectedValue.Equals(ns_MyVRMNet.vrmConfType.OBTP)) && lstVMR.SelectedIndex == 0 && !chkPCConf.Checked)
                {
                    inxml.Append("<MCUs>");
                    List<string> bridgeid = new List<string>();
                    int endpoint = 0;
                    for (int i = 0; i < dtRooms.Rows.Count; i++)
                    {
                        if (!bridgeid.Contains(dtRooms.Rows[i]["BridgeID"].ToString().Trim()))
                            bridgeid.Add(dtRooms.Rows[i]["BridgeID"].ToString().Trim());
                        endpoint++;
                    }
                    foreach (DataGridItem dgi in dgUsers.Items)
                    {
                        if (!bridgeid.Contains(((DropDownList)dgi.FindControl("lstBridges")).SelectedValue))
                            bridgeid.Add(((DropDownList)dgi.FindControl("lstBridges")).SelectedValue);
                        endpoint++;
                    }
                    for (int x = 0; x < GuestLocMCU.Count; x++)
                    {
                        if (!bridgeid.Contains(GuestLocMCU[x].ToString()))
                            bridgeid.Add(GuestLocMCU[x].ToString());
                        endpoint++;
                    }

                    for (int b = 0; b < bridgeid.Count; b++)
                        inxml.Append("<BridgeID>" + bridgeid[b] + "</BridgeID>");

                    inxml.Append("<EndpointCount>" + endpoint + "</EndpointCount>");

                    inxml.Append("</MCUs>");
                }
                inxml.Append("</confInfo>");
                inxml.Append("</conference>");

                if (inxml.ToString().IndexOf("<error>") >= 0)
                {
                    Session["hdModalDiv"] = "none";
                    errLabel.Text = obj.ShowErrorMessage(inxml.ToString());
                    errLabel.Visible = true;
                    return false;
                }
                string confInxml = inxml.ToString();
                if (isSetCustom)
                    confInxml = ChangeXML(confInxml);

                String outxml = "";
                if (confInxml.IndexOf("<error>") < 0)
                {
                    errLabel.Text = ""; //ALLBUGS-128
                    errLabel.Visible = false; //ALLBUGS-128
                    outxml = obj.CallCommand("SetConferenceDetails", confInxml);
                }
                else
                {
                    Session["hdModalDiv"] = "none";
                    errLabel.Text = obj.ShowErrorMessage(confInxml); //ALLBUGS-128
                    errLabel.Visible = true; //ALLBUGS-128
                    return false;
                }

                if (outxml.IndexOf("<error>") < 0)
                {
                    Session.Add("outxml", outxml);
                    XmlDocument xmlout = new XmlDocument();
                    xmlout.LoadXml(outxml);
                    String confID = xmlout.SelectSingleNode("//conferences/conference/confID").InnerText;
                    Session["confID"] = confID;
                    lblConfID.Text = confID;
                    //if (SetEndpoints(dtRooms))
                    //{
                    //ZD 102909 - Start
                    //ZD 103265 Start
                    //string EntityCodeText = "";
                    //if (Session["WrongEntityCode"] != null)
                    //{
                    //    if (Session["WrongEntityCode"].ToString() == "1")
                    //        EntityCodeText = "<br/>" + obj.GetTranslatedText("Mentioned entity code could not be linked with conference as it does not available.");

                    //}
                    // ZD 103265 End

                    /* commented for ZD 104482
                    //ZD 103569 start
                    string[] GuestroomID = null;
                    string roomIds = "";
                    if (xmlout.SelectSingleNode("//conferences/conference/GuestroomID") != null)
                        roomIds = xmlout.SelectSingleNode("//conferences/conference/GuestroomID").InnerText.ToString();
                    if (!string.IsNullOrEmpty(roomIds))
                    {
                        GuestroomID = roomIds.Split(',');
                    }

                    #region Memcache
                    DataTable dtTable = null;
                    DataRow[] RoomOldRows = null;
                    string disabled = "0";
                    DataSet dsCache = null;
                    DataView dv = null;
                    DataTable dtNewTable = null;
                    bool bRet = false;
                    if (memcacheEnabled == 1 && GuestroomID != null && GuestroomID.Length > 0)
                    {
                        using (memClient = new Enyim.Caching.MemcachedClient())
                        {
                            dtNewTable = new DataTable();
                            obj.GetTablefromCache(ref memClient, ref dsCache);

                            if (dsCache.Tables.Count > 0)
                                dtTable = dsCache.Tables[0];
                            for (int i = 0; i < GuestroomID.Length; i++)
                            {
                                bRet = obj.GetAllRoomsInfo(GuestroomID[i], disabled, ref dtNewTable);

                                if (!bRet || dtNewTable.Rows.Count <= 0)
                                {
                                    //dtTable = memClient.Get<DataTable>("myVRMRoomsDatatable");
                                    obj.GetAllRoomsInfo("", "0", ref dtTable, ref dsCache);
                                }
                                RoomOldRows = dtTable.Select("Roomid = '" + GuestroomID[i] + "'");

                                if (RoomOldRows != null && RoomOldRows.Count() > 0)
                                {
                                    dtTable.Rows.Remove(RoomOldRows[0]);
                                    dtTable.ImportRow(dtNewTable.Rows[0]);
                                }
                                else
                                    dtTable.ImportRow(dtNewTable.Rows[0]);


                                dv = new DataView(dtTable);
                                dv.Sort = "Tier1Name,Tier2Name,RoomName ASC";
                                dtTable = dv.ToTable();

                                obj.AddRoomTabletoCache(ref memClient, ref dsCache);
                                //memClient.Store(Enyim.Caching.Memcached.StoreMode.Set, "myVRMRoomsDatatable", dtTable);
                            }
                        }
                    }

                    #endregion
                    */

                    //ZD 103569 start
                    string LayoutAlertText = "";

                    int.TryParse(xmlout.SelectSingleNode("//conferences/AlertLOChange").InnerText, out LOAlertShow);

                    if (LOAlertShow == 1)
                        LayoutAlertText = "Note: The selected video display layout is not supported by the MCU in conference."; ;
                    string inDisXML = "<login>" + obj.OrgXMLElement() + "<userID>" + Session["userID"].ToString() + "</userID><selectType>1</selectType><selectID>" + Session["ConfID"].ToString() + "</selectID></login>";

                    string outDisXML = obj.CallMyVRMServer("GetOldConference", inDisXML, Application["MyVRMServer_ConfigPath"].ToString());

                    XmlDocument xmldoc = new XmlDocument();
                    xmldoc.LoadXml(outDisXML);

                    if (outDisXML.IndexOf("<error>") < 0)
                    {
                        outDisXML = obj.GetProperValue(xmldoc.SelectSingleNode("/conference/confInfo/Status").InnerText);

                        switch (outDisXML)
                        {
                            case ns_MyVRMNet.vrmConfStatus.Scheduled:
                                if (Session["SendConfirmationEmail"].ToString() == "3") //ALLDEV-841 start
                                {
                                    showConfMsg.Text = obj.GetTranslatedText("Your conference has been scheduled successfully.");
                                }
                                else //ALLDEV-841 End
                                    showConfMsg.Text = obj.GetTranslatedText("Your conference has been scheduled successfully. Email notifications have been sent to participants.") + "<br/>" + obj.GetTranslatedText(LayoutAlertText); //+ EntityCodeText;//FB 1830 - Translation //ZD 101971 103265
                                break;
                            case ns_MyVRMNet.vrmConfStatus.Pending:
                                showConfMsg.Text = obj.GetTranslatedText("Your conference has been submitted successfully and is currently in pending status awaiting administrative approval.") + "<br/>" + obj.GetTranslatedText(LayoutAlertText); // + EntityCodeText;//FB 1830 - Translation //ZD 101971 ZD 103265 
                                break;
                            case ns_MyVRMNet.vrmConfStatus.WaitList: //ZD 102532
                                showConfMsg.Text = obj.GetTranslatedText("Your conference has been submitted successfully and is currently in wait list status.") + "<br/>" + obj.GetTranslatedText(LayoutAlertText); /// + EntityCodeText;//FB 1830 - Translation //ZD 101971 ZD 103265 
                                break;
                            default: //FB 2341
                                showConfMsg.Text = obj.GetTranslatedText("Your conference has been scheduled successfully.") + "<br/>" + obj.GetTranslatedText(LayoutAlertText);  //+ EntityCodeText;//FB 1830 - Translation //ZD 101971 ZD 103265 
                                break;
                        }

                        if (hdnIsHDBusy != null && hdnIsHDBusy.Value == "1") //ALLDEV-807 
                            showConfMsg.Text = obj.GetTranslatedText("Your conference has been scheduled successfully.") + "<br/>";
                        //Session.Remove("WrongEntityCode"); //ZD 103265
                        //ZD 102909 - End

                        divSubModalPopup.Style.Add("display", "block");//ZD_101226
                        SubModalPopupExtender.Show();
                        Session["hdModalDiv"] = "none";//ZD 101500                            
                        Session["outxml"] = null;
                        Session["CalendarMonthly"] = null;//FB 1850

                        //FB 2363 - Start
                        if (Application["External"].ToString() != "")
                        {
                            String inEXML = "";
                            inEXML = "<SetExternalScheduling>";
                            inEXML += "<confID>" + confID + "</confID>";
                            inEXML += "</SetExternalScheduling>";

                            String outExml = obj.CallCommand("SetExternalScheduling", inEXML);
                        }
                        //FB 2363 - End
                        //FB 2426 Start

                        String eptxmlPath = Directory.GetParent(HttpContext.Current.Request.MapPath(".").ToString()) + "\\en" + "\\" + Session["EptXmlPath"].ToString();
                        if (File.Exists(eptxmlPath))
                            File.Delete(eptxmlPath);
                        String roomxmlPath = Directory.GetParent(HttpContext.Current.Request.MapPath(".").ToString()) + "\\en" + "\\" + Session["RoomXmlPath"].ToString();
                        if (File.Exists(roomxmlPath))
                        {
                            if (obj.WaitForFile(roomxmlPath))
                                File.Delete(roomxmlPath);
                        }

                        //FB 2426 End
                    }
                    else
                    {
                        Session["hdModalDiv"] = "none";//ZD 101500
                        errLabel.Text = obj.ShowErrorMessage(outxml);
                        errLabel.Visible = true;
                        return false;
                    }
                    //}

                    //ZD 102754
                    String inAXML = "";
                    inAXML = "<ConfAuditDetails>";
                    inAXML += "<confID>" + confID + "</confID>";
                    inAXML += "<userID>" + Session["userID"].ToString() + "</userID>";
                    inAXML += "</ConfAuditDetails>";
                    String outAxml = "";
                    outAxml = obj.CallCommand("InsertConfAuditDetails", inAXML);

                    return true;
                }
                else
                {
                    Session["hdModalDiv"] = "none";//ZD 101500                    
                    string errLevel = obj.GetErrorLevel(outxml);
                    if (errLevel.Equals("C"))
                    {
                        AddInstanceInfo(inxml.ToString(), outxml);
                    }
                    errLabel.Text = obj.ShowErrorMessage(outxml);
                    errLabel.Visible = true;

                    //ZD 100568 23-12-2013 Inncrewin
                    if (enableCloudInstallation == 1 && (errLabel.Text.Contains("698") || errLabel.Text.Contains("699")))
                    {
                        errLabel.Text = "";
                        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "update script", "fnShowPopupConfAlert();", true);
                    }
                    //ZD 100568 23-12-2013 Inncrewin  
                    hasVisited.Value = "1";
                    DisplayUserEndpoints();//ZD 103402
                    return false;
                }
            }
            catch (Exception ex)
            {
                Session["hdModalDiv"] = "none";//ZD 101500                
                log.Trace("Error occurred in SetConference. Please try later.\n" + ex.Message);
                errLabel.Visible = true;
                errLabel.Text = obj.ShowSystemMessage();
                return false;
            }
        }
        #endregion
		//ZD 102195 End
        #region SetEndpoints
        /// <summary>
        /// SetEndpoints
        /// </summary>
        /// <param name="dtrms"></param>
        /// <returns></returns>
        protected bool SetEndpoints(DataTable dtrms)
        {
            string bridgeId = "-1";
            StringBuilder inXML = new StringBuilder();
            try
            {
                //ZD 100704 Starts
                if (lstConferenceType.SelectedValue.Equals(ns_MyVRMNet.vrmConfType.RoomOnly) || lstConferenceType.SelectedValue.Equals(ns_MyVRMNet.vrmConfType.HotDesking))//ZD 101670
                {
                    inXML = new StringBuilder();
                    inXML.Append("<SetAdvancedAVSettings>");
                    if (Application["Exchange"] != null && Application["Exchange"].ToString().ToUpper() == "YES")
                        inXML.Append("<isExchange>1</isExchange>");
                    //ZD 100924 Start
                    else if (Application["Exchange"] != null && Application["Exchange"].ToString().ToUpper() == "MOD")
                        inXML.Append("<isExchange>2</isExchange>");
                    else
                        inXML.Append("<isExchange>0</isExchange>");
                    //ZD 100924 End
                    inXML.Append("<editFromWeb>1</editFromWeb>");
                    inXML.Append("<UserID>" + Session["userID"].ToString() + "</UserID>");
                    inXML.Append("<ConfID>" + lblConfID.Text + "</ConfID>");
                    inXML.Append(obj.OrgXMLElement());
                    inXML.Append("<language>" + language + "</language>");
                    inXML.Append("<AVParams>");
                    inXML.Append("<SingleDialin>0</SingleDialin>");
                    inXML.Append("</AVParams>");
                    inXML.Append("<Endpoints>");
                    inXML.Append("</Endpoints>");
                    inXML.Append("</SetAdvancedAVSettings>");
                }
                else
                {
                    XmlDocument xmldoc = new XmlDocument();
                    xmldoc.LoadXml(Session["outxml"].ToString());
                    XmlNodeList nodes = xmldoc.SelectNodes("//setConference/conferences/conference/invited/party");
                    inXML.Append("<SetAdvancedAVSettings>");
                    inXML.Append("<editFromWeb>1</editFromWeb>");//FB 2235
                    inXML.Append("<UserID>" + Session["userID"].ToString() + "</UserID>");
                    inXML.Append("<ConfID>" + lblConfID.Text + "</ConfID>");
                    inXML.Append(obj.OrgXMLElement());//Organization Module Fixes
                    inXML.Append("<AVParams>");
                    inXML.Append("<SingleDialin>0</SingleDialin>");
                    //FB 2870 Start
                    if (ChkEnableNumericID.Checked)
                    {
                        inXML.Append("<EnableNumericID>1</EnableNumericID>");
                        inXML.Append("<CTNumericID>" + txtNumeridID.Text + "</CTNumericID>");
                    }
                    else
                    {
                        inXML.Append("<EnableNumericID>0</EnableNumericID>");
                        inXML.Append("<CTNumericID></CTNumericID>");
                    }
                    //FB 2870 End

                    //ZD 100890 Start
                    if (chkStatic.Checked)
                    {
                        inXML.Append("<EnableStaticID>1</EnableStaticID>");
                        inXML.Append("<StaticID>" + txtStatic.Text + "</StaticID>");
                    }
                    else
                    {
                        inXML.Append("<EnableStaticID>0</EnableStaticID>");
                        inXML.Append("<StaticID></StaticID>");
                    }
                    //ZD 100890 End

                    inXML.Append("</AVParams>");
                    inXML.Append("<Endpoints>");
                    //ZD 100522
                    if (!(chkPCConf.Checked || (lstVMR.SelectedIndex > 0 && lstVMR.SelectedValue != "2") || lstConferenceType.SelectedValue == ns_MyVRMNet.vrmConfType.HotDesking)) //FB 2819 //FB 2833 - NO ENDPOINTS WILL BE FORMED FOR PC & VMR CONFERENCE
                    {
                        //FB 2426 start
                        string guestRoomStr = "";
                        XmlNode GuestRoom = xmldoc.SelectSingleNode("//setConference/conferences/conference/GuestRooms");
                        if (GuestRoom != null)
                        {
                            guestRoomStr = GuestRoom.InnerXml.Trim();
                        }
                        //FB 2426 end

                        //FB 2839
                        Hashtable mculidt = new Hashtable();
                        Hashtable MCUPoolOrder = new Hashtable();//ZD 104256

                        string optionId = "", optionValue = "", BID = "";
                        String[] ProfileList = hdnMcuProfileSelected.Value.Split('$');
                        string bridgeProfileID = "", BridgeValue = ""; String[] attributeDt;
                        if (ProfileList.Length > 0)
                        {
                            for (int s = 0; s < ProfileList.Length - 1; s++)
                            {
                                BridgeValue = ProfileList[s];
                                attributeDt = BridgeValue.Split('?');
                                if (attributeDt.Length > 0)
                                {
                                    bridgeId = attributeDt[0];
                                    bridgeProfileID = attributeDt[1];
                                    DropDownList drpTemp = (DropDownList)tblMCUProfile.FindControl(bridgeProfileID);
                                    if (drpTemp != null) //FB 3052
                                    {
                                        optionId = drpTemp.SelectedValue;
                                        optionValue = drpTemp.SelectedItem.Text;
                                    }
                                    if (optionId == "")
                                        optionValue = "";

                                    if (!mculidt.Contains(bridgeId))
                                        mculidt.Add(bridgeId, optionId);

                                }
                            }
                        }
                        //FB 2839 End
                        //ZD 104256 Starts
                        string[] PoolOrderList = hdnMcuPoolOrder.Value.Split('$');
                        string bridgePoolOrder = "";
                        DropDownList drpPOtmp = null;
                        if (PoolOrderList.Length > 0)
                        {
                            for (int s = 0; s < PoolOrderList.Length - 1; s++)
                            {
                                BridgeValue = PoolOrderList[s];
                                attributeDt = BridgeValue.Split('?');
                                if (attributeDt.Length > 0)
                                {
                                    bridgeId = attributeDt[0];
                                    bridgePoolOrder = attributeDt[1];
                                    drpPOtmp = (DropDownList)tblMCUProfile.FindControl(bridgePoolOrder);
                                    if (drpPOtmp != null)
                                    {
                                        optionId = drpPOtmp.SelectedValue;
                                        optionValue = drpPOtmp.SelectedItem.Text;
                                    }
                                    if (optionId == "")
                                        optionValue = "";

                                    if (!MCUPoolOrder.Contains(bridgeId))
                                        MCUPoolOrder.Add(bridgeId, optionId);
                                }
                            }
                        }
                        //ZD 104256 Ends

                        //Room Endpoints
                        if (lstConferenceType.SelectedValue == ns_MyVRMNet.vrmConfType.AudioVideo || lstConferenceType.SelectedValue.Equals(ns_MyVRMNet.vrmConfType.OBTP) || lstConferenceType.SelectedValue == ns_MyVRMNet.vrmConfType.AudioOnly || lstConferenceType.SelectedValue == ns_MyVRMNet.vrmConfType.P2P) //ZD 100513
                        {
                            foreach (DataRow dr in dtrms.Rows)
                            {
                                inXML.Append("<Endpoint>");
                                inXML.Append("<Type>R</Type>");
                                inXML.Append("<ID>" + dr["ID"].ToString() + "</ID>");
                                inXML.Append("<UseDefault>0</UseDefault>"); //ZD 100522
                                inXML.Append("<IsLecturer>0</IsLecturer>");
                                inXML.Append("<EndpointID>" + dr["EndpointID"].ToString() + "</EndpointID>");
                                inXML.Append("<ProfileID>" + dr["ProfileID"].ToString() + "</ProfileID>");
                                inXML.Append("<BridgeID>" + dr["BridgeID"].ToString() + "</BridgeID>");

                                //FB 2839 Start
                                BID = dr["BridgeID"].ToString();
                                IDictionaryEnumerator iEnum = mculidt.GetEnumerator();
                                while (iEnum.MoveNext())
                                {
                                    if (BID == iEnum.Key.ToString())
                                    {
                                        optionId = iEnum.Value.ToString();
                                        break;
                                    }
                                    else
                                        optionId = "";
                                }
                                //FB 2839 End

                                inXML.Append("<BridgeProfileID>" + optionId + "</BridgeProfileID>");
                                //ZD 104256 Starts
                                iEnum = MCUPoolOrder.GetEnumerator();
                                while (iEnum.MoveNext())
                                {
                                    if (BID == iEnum.Key.ToString())
                                    {
                                        optionId = iEnum.Value.ToString();
                                        break;
                                    }
                                    else
                                        optionId = "";
                                }

                                inXML.Append("<BridgePoolOrder>" + optionId + "</BridgePoolOrder>");
                                //ZD 104256 Ends
                                inXML.Append("<AddressType></AddressType>");
                                inXML.Append("<Address></Address>");
                                inXML.Append("<VideoEquipment></VideoEquipment>");
                                inXML.Append("<connectionType></connectionType>");
                                inXML.Append("<Bandwidth></Bandwidth>");
                                inXML.Append("<IsOutside></IsOutside>");
                                inXML.Append("<DefaultProtocol></DefaultProtocol>");
                                inXML.Append("<Connection>2</Connection>"); //Default to AudioVideo
                                inXML.Append("<URL></URL>");
                                if (lstConferenceType.SelectedValue == ns_MyVRMNet.vrmConfType.P2P) //ZD 100704
                                {
                                    inXML.Append("<Connect2>" + dr["Connect2"].ToString().Trim() + "</Connect2>");
                                }
                                else
                                    inXML.Append("<Connect2>-1</Connect2>");
                                inXML.Append("<APIPortNo>23</APIPortNo>");
                                inXML.Append("<ExchangeID></ExchangeID>");
                                inXML.Append("</Endpoint>");
                            }
                        }
                        inXML.Append(guestRoomStr); //FB 2426
                        //User Endpoints
                        if (audioDialString.Trim().Length > 2 && !lstConferenceType.SelectedValue.Equals(ns_MyVRMNet.vrmConfType.HotDesking))
                        {
                            if (Session["ConfEndPts"] != null)
                                usrEndpoints = (Hashtable)Session["ConfEndPts"];

                            if (audioPartyId > 0)
                            {
                                bridgeId = "-1";
                                if (usrEndpoints != null)
                                {
                                    if (usrEndpoints.Contains(audioPartyId))
                                    {
                                        bridgeId = usrEndpoints[audioPartyId].ToString().Trim();
                                    }
                                }

                                inXML.Append("<Endpoint>");
                                inXML.Append("<Type>U</Type>");
                                inXML.Append("<ID>" + audioPartyId + "</ID>");
                                inXML.Append("<UseDefault>1</UseDefault>"); //Default Profile
                                inXML.Append("<IsLecturer>0</IsLecturer>");
                                inXML.Append("<EndpointID></EndpointID>");
                                inXML.Append("<ProfileID></ProfileID>");
                                inXML.Append("<AddressType>" + audioAddressType + "</AddressType>"); //Set as ISDN //ZD 103574
                                inXML.Append("<Address>" + audioDialString + "</Address>");
                                inXML.Append("<BridgeID>" + bridgeId + "</BridgeID>");
                                inXML.Append("<Connection>1</Connection>"); //Default to Audio
                                inXML.Append("<VideoEquipment>-1</VideoEquipment>");
                                inXML.Append("<connectionType>2</connectionType>"); //Default to Dial-out
                                inXML.Append("<Bandwidth>" + lstLineRate.SelectedValue + "</Bandwidth>"); //FB 2394
                                inXML.Append("<IsOutside>1</IsOutside>");
                                inXML.Append("<DefaultProtocol>" + audioProtocol + "</DefaultProtocol>"); //Default to ISDN protocol //ZD 103574 
                                inXML.Append("<URL></URL>");
                                if (lstConferenceType.SelectedValue == ns_MyVRMNet.vrmConfType.P2P) //ZD 100704
                                    inXML.Append("<Connect2>1</Connect2>");
                                else
                                    inXML.Append("<Connect2>-1</Connect2>");
                                inXML.Append("<APIPortNo>23</APIPortNo>");
                                inXML.Append("<ExchangeID></ExchangeID>");
                                if (enablePartCode == "0") //ZD 101446
                                    txtPartyCode.Text = "";
                                inXML.Append("<participantCode>" + txtPartyCode.Text + "</participantCode>");//ZD 101446
                                inXML.Append("<ConferenceCode>" + txtConfCode.Text + "</ConferenceCode>"); //ALLDEV-814
                                inXML.Append("<LeaderPin>" + txtLeaderPin.Text + "</LeaderPin>");
                                inXML.Append("</Endpoint>");
                            }
                        }

                        //ZD 100834 Starts
                        int rowCnt = 0;
                        string epID = "";
                        bool flag = false;
                        DataRow userRow;
                        //ZD 100522
                        if ((lstConferenceType.SelectedValue.Equals(ns_MyVRMNet.vrmConfType.AudioOnly) || lstConferenceType.SelectedValue.Equals(ns_MyVRMNet.vrmConfType.OBTP) || lstConferenceType.SelectedValue.Equals(ns_MyVRMNet.vrmConfType.AudioVideo) || lstConferenceType.SelectedValue.Equals(ns_MyVRMNet.vrmConfType.P2P)) && (lstVMR.SelectedIndex == 0 || lstVMR.SelectedValue == "2") && !chkPCConf.Checked) //ZD 100513
                        {
                            DataTable userDT = (DataTable)ViewState["userGrid"];
                            string PartyAddressType = "-1", dftProtocol = "-1";//ZD 100834
                            foreach (DataGridItem dgi in dgUsers.Items)
                            {
                                rowCnt = 0;
                                optionId = "";
                                userRow = userDT.Rows[rowCnt];
                                epID = "";
                                if (dgi.Cells[0].Text.Trim().IndexOf("new") >= 0)
                                    foreach (XmlNode node in nodes)
                                    {
                                        if (((Label)dgi.FindControl("lblUserName")).Text.Trim().ToUpper().IndexOf(node.SelectSingleNode("partyEmail").InnerText.Trim().ToUpper()) >= 0)
                                        {
                                            epID = node.SelectSingleNode("partyID").InnerText.Trim();
                                        }
                                    }
                                else
                                    epID = dgi.Cells[0].Text;

                                flag = false;
                                string UserID = dgi.Cells[0].Text.ToString();
                                if (UserID.Trim().IndexOf("new") >= 0)
                                    UserID = "new";
                                for (int i = 0; i < txtPartysInfo.Text.Split(pipeDelim, StringSplitOptions.RemoveEmptyEntries).Length; i++)
                                {
                                    if (txtPartysInfo.Text.Split(pipeDelim, StringSplitOptions.RemoveEmptyEntries)[i].Split(ExclamDelim, StringSplitOptions.RemoveEmptyEntries)[4].Trim().Equals("1") && txtPartysInfo.Text.Split(pipeDelim, StringSplitOptions.RemoveEmptyEntries)[i].Split(ExclamDelim, StringSplitOptions.RemoveEmptyEntries)[0].Trim().Equals(UserID)) //FB 1888
                                        flag = true;
                                }
                                if (flag.Equals(true))
                                {
                                    inXML.Append("<Endpoint>");
                                    inXML.Append("<Type>U</Type>");
                                    inXML.Append("<ID>" + epID + "</ID>");
                                    inXML.Append("<UseDefault>0</UseDefault>");
                                    inXML.Append("<IsLecturer>0</IsLecturer>");
                                    inXML.Append("<EndpointID></EndpointID>");
                                    inXML.Append("<ProfileID></ProfileID>");
                                    //ZD 101561 Starts
                                    if (lstVMR.SelectedValue == "2" && Session["VMRBridge"] != null && Session["VMRBridge"].ToString() != "0")
                                        inXML.Append("<BridgeID>" + Session["VMRBridge"].ToString() + "</BridgeID>");
                                    else
                                        inXML.Append("<BridgeID>" + ((DropDownList)dgi.FindControl("lstBridges")).SelectedValue + "</BridgeID>");
                                    //ZD 101561 End
                                    BID = ((DropDownList)dgi.FindControl("lstBridges")).SelectedValue;
                                    IDictionaryEnumerator iEnum = mculidt.GetEnumerator();
                                    while (iEnum.MoveNext())
                                    {
                                        if (BID == iEnum.Key.ToString())
                                        {
                                            optionId = iEnum.Value.ToString();
                                            break;
                                        }
                                        else
                                            optionId = "";
                                    }

                                    inXML.Append("<BridgeProfileID>" + optionId + "</BridgeProfileID>");
                                    //ZD 104256 Starts
                                    iEnum = MCUPoolOrder.GetEnumerator();
                                    while (iEnum.MoveNext())
                                    {
                                        if (BID == iEnum.Key.ToString())
                                        {
                                            optionId = iEnum.Value.ToString();
                                            break;
                                        }
                                        else
                                            optionId = "";
                                    }

                                    inXML.Append("<BridgePoolOrder>" + optionId + "</BridgePoolOrder>");
                                    //ZD 104256 Ends

                                    PartyAddressType = ((DropDownList)dgi.FindControl("lstAddressType")).SelectedValue;
                                    inXML.Append("<AddressType>" + PartyAddressType + "</AddressType>");
                                    /*if (((DropDownList)dgi.FindControl("lstConnection")).SelectedValue == "1" && lstConferenceType.SelectedValue.Equals(ns_MyVRMNet.vrmConfType.AudioVideo))
                                    {
                                        string confCode = userRow["txtConfCode"].ToString().Trim();
                                        string leaderPin = userRow["txtleaderPin"].ToString().Trim();
                                        TextBox txtaddress = (TextBox)dgi.FindControl("txtAddress");
                                        string add = "";
                                        if (!string.IsNullOrWhiteSpace(confCode) || !string.IsNullOrWhiteSpace(leaderPin))
                                            add = txtaddress.Text.Trim() + "D" + confCode + "+" + leaderPin;
                                        else
                                            add = txtaddress.Text.Trim();

                                        inXML.Append("<Address>" + add + "</Address>");
                                    }
                                    else
                                    {
                                        inXML.Append("<Address>" + obj.ControlConformityCheck(((TextBox)dgi.FindControl("txtAddress")).Text.Trim()) + "</Address>");
                                    }*/
                                    inXML.Append("<Address>" + obj.ControlConformityCheck(((TextBox)dgi.FindControl("txtAddress")).Text.Trim()) + "</Address>");
                                    //ZD 100815 Starts
                                    //inXML.Append("<VideoEquipment>" + userRow["VideoEquipment"].ToString() + "</VideoEquipment>");
                                    inXML.Append("<VideoEquipment>" + ((DropDownList)dgi.FindControl("lstVideoEquipment")).SelectedValue + "</VideoEquipment>");
                                    //ZD 100815 End
                                    inXML.Append("<connectionType>" + ((DropDownList)dgi.FindControl("lstConnectionType")).SelectedValue + "</connectionType>");
                                    inXML.Append("<Bandwidth>" + ((DropDownList)dgi.FindControl("lstLineRate")).SelectedValue + "</Bandwidth>");
                                    inXML.Append("<IsOutside>" + userRow["IsOutside"].ToString() + "</IsOutside>");
                                    
                                    //ZD 100834 Starts
                                    dftProtocol = userRow["DefaultProtocol"].ToString();
                                    if (PartyAddressType == "1" || PartyAddressType == "2")
                                        dftProtocol = "1";
                                    else if (PartyAddressType == "3" || PartyAddressType == "4")
                                        dftProtocol = "2";
                                    else if (PartyAddressType == "6")
                                        dftProtocol = "3";
                                    else if (PartyAddressType == "5")
                                        dftProtocol = "4";
                                    inXML.Append("<DefaultProtocol>" + dftProtocol + "</DefaultProtocol>");
                                    //ZD 100834 End
                                    inXML.Append("<Connection>" + ((DropDownList)dgi.FindControl("lstConnection")).SelectedValue + "</Connection>");
                                    inXML.Append("<URL>" + userRow["URL"].ToString() + "</URL>");
                                    inXML.Append("<ExchangeID>" + userRow["ExchangeID"].ToString() + "</ExchangeID>");
                                    if (userRow["ApiPortno"].ToString() != "")
                                        inXML.Append("<APIPortNo>" + userRow["ApiPortno"].ToString() + "</APIPortNo>");
                                    else
                                        inXML.Append("<APIPortNo>23</APIPortNo>");
                                    if (lstConferenceType.SelectedValue.Equals(ns_MyVRMNet.vrmConfType.P2P))
                                        inXML.Append("<Connect2>" + ((DropDownList)dgi.FindControl("lstTelnetUsers")).SelectedValue + "</Connect2>");
                                    else
                                        inXML.Append("<Connect2>-1</Connect2>");
                                    inXML.Append("<participantCode></participantCode>"); ; //ZD 101446
                                    inXML.Append("<ConferenceCode></ConferenceCode>"); //ALLDEV-814
                                    inXML.Append("<LeaderPin></LeaderPin>");
                                    inXML.Append("</Endpoint>");
                                }
                                rowCnt++;
                            }
                        }
                        //ZD 100834 End
                    }
                    inXML.Append("</Endpoints>");
                    inXML.Append("</SetAdvancedAVSettings>");
                }
                //ZD 100704 End
                string outXML = obj.CallMyVRMServer("SetAdvancedAVSettings", inXML.ToString(), Application["MyVRMServer_ConfigPath"].ToString());
                if (outXML.IndexOf("<error>") >= 0)
                {
                    Session["hdModalDiv"] = "none";//ZD 101500
                    errLabel.Visible = true;
                    errLabel.Text = obj.ShowErrorMessage(outXML);
                    return false;
                }
                else
                {
                    //ZD 101971 Starts
                    string Successmsg = "";
                    XmlDocument xmldoc = new XmlDocument();
                    xmldoc.LoadXml(outXML);
                    XmlNode LOAlert = xmldoc.SelectSingleNode("//success");
                    LOAlertShow = 0;
                    if (LOAlert != null)
                        Successmsg = LOAlert.InnerText.Trim();
                    if (Successmsg.Contains(','))
                    {
                        Successmsg = Successmsg.Split(',')[1];
                        int.TryParse(Successmsg, out LOAlertShow);
                    }
                    //ZD 101971 Ends
                    return true;
                }
            }
            catch (Exception ex)
            {
                log.Trace(ex.Message);
                errLabel.Visible = true;
                return false;
            }
        }

        #endregion

        #region CreateDataTable
        protected DataTable CreateDataTable(DataTable dt)
        {
            try
            {
                if (!dt.Columns.Contains("ID"))
                    dt.Columns.Add("ID");
                if (!dt.Columns.Contains("Type"))
                    dt.Columns.Add("Type");
                if (!dt.Columns.Contains("Name"))
                    dt.Columns.Add("Name");
                if (!dt.Columns.Contains("EndpointID"))
                    dt.Columns.Add("EndpointID");
                if (!dt.Columns.Contains("EndpointName"))/***/
                    dt.Columns.Add("EndpointName");
                if (!dt.Columns.Contains("ProfileID"))
                    dt.Columns.Add("ProfileID");
                if (!dt.Columns.Contains("Bandwidth"))
                    dt.Columns.Add("Bandwidth");
                if (!dt.Columns.Contains("connectionType"))
                    dt.Columns.Add("connectionType");
                if (!dt.Columns.Contains("DefaultProtocol"))
                    dt.Columns.Add("DefaultProtocol");
                if (!dt.Columns.Contains("AddressType"))
                    dt.Columns.Add("AddressType");
                if (!dt.Columns.Contains("Address"))
                    dt.Columns.Add("Address");
                if (!dt.Columns.Contains("VideoEquipment"))
                    dt.Columns.Add("VideoEquipment");
                if (!dt.Columns.Contains("BridgeID"))
                    dt.Columns.Add("BridgeID");
                if (!dt.Columns.Contains("Connection"))
                    dt.Columns.Add("Connection");
                if (!dt.Columns.Contains("URL"))
                    dt.Columns.Add("URL");
                if (!dt.Columns.Contains("IsOutside"))
                    dt.Columns.Add("IsOutside");
                if (!dt.Columns.Contains("Connect2"))
                    dt.Columns.Add("Connect2");
                if (!dt.Columns.Contains("ConfCode"))
                    dt.Columns.Add("ConfCode");
                if (!dt.Columns.Contains("LPin"))
                    dt.Columns.Add("LPin");
				//ZD 100834 Starts
                if (!dt.Columns.Contains("APIPortNo"))
                    dt.Columns.Add("APIPortNo");
                if (!dt.Columns.Contains("ExchangeID"))
                    dt.Columns.Add("ExchangeID");
                if (!dt.Columns.Contains("isTelePresence"))
                    dt.Columns.Add("isTelePresence");
                if (!dt.Columns.Contains("BridgeProfileID"))
                    dt.Columns.Add("BridgeProfileID");
                if (!dt.Columns.Contains("AudioBridgeParty"))
                    dt.Columns.Add("AudioBridgeParty");
				//ZD 100834 End
                if (!dt.Columns.Contains("IsTestEquipment")) //ZD 101815
                    dt.Columns.Add("IsTestEquipment");
                if (!dt.Columns.Contains("PoolOrder")) //ZD 104256
                    dt.Columns.Add("PoolOrder");

                return dt;
            }
            catch (Exception ex)
            {
                log.Trace(ex.Message);
                Session["hdModalDiv"] = "none";//ZD 101500
                errLabel.Visible = true;
                return null;
            }
        }
        #endregion

        #region AddRoomEndpoint
        protected DataRow AddRoomEndpoint(String RoomID, DataTable dt, ref bool isError, bool isCaller)
        {
            DataRow dr = dt.NewRow();
            try
            {
                //ZD 101490 Starts
                int ttlPp = dgUsers.Items.Count; 
                string[] locs = selectedloc.Value.Split(',');

                if (locs != null)
                    ttlPp = locs.Count() + ttlPp;
                //ZD 101490 Ends

                log.Trace("In AddRoomEndpoint");

                dr["ID"] = RoomID;
                string inXML = "<login>" + obj.OrgXMLElement() + "<userID>" + Session["userID"].ToString() + "</userID><roomID>" + RoomID + "</roomID></login>";
                string outXML = obj.CallMyVRMServer("GetOldRoom", inXML, Application["MyVRMServer_ConfigPath"].ToString());//FB 2027(GetOldRoom)

                XmlDocument xmldoc = new XmlDocument();
                xmldoc.LoadXml(outXML);
                //ZD 100522 Starts
                String VMR = xmldoc.SelectSingleNode("//room/IsVMR").InnerText.Trim();
                if (VMR == "0")
                {
                    if (!dt.Columns.Contains("Name"))
                        dt.Columns.Add("Name");
                    if (!dt.Columns.Contains("EndpointName"))
                        dt.Columns.Add("EndpointName");
                    dr["Name"] = xmldoc.SelectSingleNode("//room/roomName").InnerText.Trim();
                    dr["Name"] = "<a href='#' onclick='javascript:chkresource(\"" + dr["ID"].ToString() + "\")'>" + dr["Name"] + "</a>";
                    dr["EndpointID"] = xmldoc.SelectSingleNode("//room/endpoint").InnerText.Trim();


                    if (dr["EndpointID"].ToString().Equals("") || dr["EndpointID"].ToString().Equals("0") || dr["EndpointID"].ToString().Equals("-1"))
                    {
                        if (!(lstConferenceType.SelectedValue == ns_MyVRMNet.vrmConfType.RoomOnly || lstConferenceType.SelectedValue == ns_MyVRMNet.vrmConfType.HotDesking))
                        {
                            dr["EndpointName"] = obj.GetTranslatedText("No Endpoint(s) associated with this room");
                            errLabel.Visible = true;
                            errLabel.Text = obj.GetTranslatedText("No Endpoint(s) associated with room: ") + dr["Name"].ToString();
                            isError = true;
                            return dr;
                        }
                    }
                    else
                    {
                        inXML = "";
                        inXML += "<EndpointDetails>";
                        inXML += obj.OrgXMLElement();//Organization Module Fixes
                        inXML += "  <UserID>" + Session["userID"].ToString() + "</UserID>";
                        inXML += "  <EndpointID>" + dr["EndpointID"].ToString() + "</EndpointID>";
                        inXML += "</EndpointDetails>";
                        outXML = obj.CallMyVRMServer("GetEndpointDetails", inXML, Application["MyVRMServer_ConfigPath"].ToString());
                        XmlDocument xmlEP = new XmlDocument();
                        xmlEP.LoadXml(outXML);
                        if (xmlEP.SelectNodes("//EndpointDetails/Endpoint/Profiles/Profile").Count > 0)
                        {
                            XmlNodeList nodesEP = xmlEP.SelectNodes("//EndpointDetails/Endpoint/Profiles/Profile");
                            dr["ProfileID"] = "0";
                            dr["EndpointName"] = xmlEP.SelectSingleNode("//EndpointDetails/Endpoint/Name").InnerText;
                            //ZD 100815 Starts
                            if (lstConferenceType.SelectedValue.Equals(ns_MyVRMNet.vrmConfType.P2P) || (ttlPp <= 2&& hdnCrossEnableSmartP2P.Value == "1"))//ZD 101490
                            {
                                foreach (XmlNode node in nodesEP)
                                {
                                    if (xmlEP.SelectSingleNode("//EndpointDetails/Endpoint/Profiles/Profile/isTelePresence") != null && xmlEP.SelectSingleNode("//EndpointDetails/Endpoint/Profiles/Profile/isTelePresence").InnerText == "1" && lstConferenceType.SelectedValue.Equals(ns_MyVRMNet.vrmConfType.P2P))
                                    {
                                        errLabel.Text = obj.GetTranslatedText("Point to Point Conference should not have Telepresence endpoint.. Telepresence Room Name:")
                                                  + dr["Name"].ToString();
                                        errLabel.Visible = true;
                                        isError = true;
                                        return dr;
                                    }
                                    if (node.SelectSingleNode("IsP2PDefault").InnerText.Equals("1")) //P2P Profile Type
                                    {
                                        dr["BridgeID"] = node.SelectSingleNode("Bridge").InnerText;
                                        if (dr["BridgeID"].Equals("0"))
                                            dr["BridgeID"] = "-1";
                                        dr["ProfileID"] = node.SelectSingleNode("ProfileID").InnerText;
                                        dr["IsTestEquipment"] = node.SelectSingleNode("IsTestEquipment").InnerText;
                                        dr["AddressType"] = node.SelectSingleNode("AddressType").InnerText;
                                        break;
                                    }
                                    if (node.SelectSingleNode("ProfileType").InnerText.Equals("2")) //P2P Profile Type
                                    {
                                        dr["BridgeID"] = node.SelectSingleNode("Bridge").InnerText;
                                        if (dr["BridgeID"].Equals("0"))
                                            dr["BridgeID"] = "-1";
                                        dr["ProfileID"] = node.SelectSingleNode("ProfileID").InnerText;
                                        dr["IsTestEquipment"] = node.SelectSingleNode("IsTestEquipment").InnerText;
                                        dr["AddressType"] = node.SelectSingleNode("AddressType").InnerText;
                                        break;
                                    }
                                    if (xmlEP.SelectSingleNode("//EndpointDetails/Endpoint/DefaultProfileID").InnerText.Equals(node.SelectSingleNode("ProfileID").InnerText))
                                    {
                                        dr["BridgeID"] = node.SelectSingleNode("Bridge").InnerText;
                                        if (dr["BridgeID"].Equals("0"))
                                            dr["BridgeID"] = "-1";
                                        dr["ProfileID"] = node.SelectSingleNode("ProfileID").InnerText;
                                        dr["IsTestEquipment"] = node.SelectSingleNode("IsTestEquipment").InnerText;
                                        dr["AddressType"] = node.SelectSingleNode("AddressType").InnerText;
                                    }

                                }
                            }//ZD 100815 Ends
                            else
                            {
                                foreach (XmlNode node in nodesEP)
                                {
                                    //ZD 100704 Starts
                                    if (xmlEP.SelectSingleNode("//EndpointDetails/Endpoint/Profiles/Profile/isTelePresence") != null && xmlEP.SelectSingleNode("//EndpointDetails/Endpoint/Profiles/Profile/isTelePresence").InnerText == "1" && lstConferenceType.SelectedValue.Equals(ns_MyVRMNet.vrmConfType.P2P))
                                    {
                                        errLabel.Text = obj.GetTranslatedText("Point to Point Conference should not have Telepresence endpoint.. Telepresence Room Name:")
                                                  + dr["Name"].ToString();
                                        errLabel.Visible = true;
                                        isError = true;
                                        return dr;
                                    }
                                    //ZD 100704 End
                                    if (xmlEP.SelectSingleNode("//EndpointDetails/Endpoint/DefaultProfileID").InnerText.Equals(node.SelectSingleNode("ProfileID").InnerText))
                                    {
                                        dr["BridgeID"] = node.SelectSingleNode("Bridge").InnerText;
                                        if (dr["BridgeID"].Equals("0"))
                                            dr["BridgeID"] = "-1";
                                        dr["ProfileID"] = node.SelectSingleNode("ProfileID").InnerText;
                                        dr["IsTestEquipment"] = node.SelectSingleNode("IsTestEquipment").InnerText;
                                        dr["AddressType"] = node.SelectSingleNode("AddressType").InnerText;
                                        if (lstVMR.SelectedValue != "2") //ZD 100522
                                            break;
                                    }
                                    //ZD 100522 Starts
                                    if (lstVMR.SelectedValue == "2")
                                    {
                                        if (xmlEP.SelectSingleNode("//EndpointDetails/Endpoint/DefaultProfileID").InnerText.Equals(node.SelectSingleNode("ProfileID").InnerText))
                                        {
                                            if (node.SelectSingleNode("ConnectionType").InnerText.Equals("2"))
                                            {
                                                dr["BridgeID"] = node.SelectSingleNode("Bridge").InnerText;
                                                if (dr["BridgeID"].Equals("0"))
                                                    dr["BridgeID"] = "-1";
                                                dr["ProfileID"] = node.SelectSingleNode("ProfileID").InnerText;
                                                dr["IsTestEquipment"] = node.SelectSingleNode("IsTestEquipment").InnerText;
                                                dr["AddressType"] = node.SelectSingleNode("AddressType").InnerText;
                                                break;
                                            }
                                            else
                                                continue;
                                        }

                                        if (node.SelectSingleNode("ConnectionType").InnerText.Equals("2"))
                                        {
                                            dr["BridgeID"] = node.SelectSingleNode("Bridge").InnerText;
                                            if (dr["BridgeID"].Equals("0"))
                                                dr["BridgeID"] = "-1";
                                            dr["ProfileID"] = node.SelectSingleNode("ProfileID").InnerText;
                                            dr["IsTestEquipment"] = node.SelectSingleNode("IsTestEquipment").InnerText;
                                            dr["AddressType"] = node.SelectSingleNode("AddressType").InnerText;
                                            break;
                                        }
                                        else
                                            continue;
                                    }
                                }
                            }
                        }
                        else
                        {
                            dr["EndpointID"] = "0";
                            dr["EndpointName"] = obj.GetTranslatedText("No Endpoint(s) associated with this room");
                            isError = true;
                        }

                    }
                }
                //ZD 101641 Starts
                if (!dt.Columns.Contains("IsVMR"))
                    dt.Columns.Add("IsVMR");
                dr["IsVMR"] = VMR;
                //ZD 100522 Starts

                if (VMR == "1")
                {
                    dr["BridgeID"] = xmldoc.SelectSingleNode("//room/VMRBridgeID").InnerText.Trim();
                    if (dr["BridgeID"].Equals("0"))
                        dr["BridgeID"] = "-1";
                    VMRBridge = xmldoc.SelectSingleNode("//room/VMRBridgeID").InnerText.Trim();
                    Session["VMRBridge"] = xmldoc.SelectSingleNode("//room/VMRBridgeID").InnerText.Trim();
                }
                //ZD 100522 Ends
                //ZD 101641 Ends
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return dr;
        }
        #endregion

        #region AddInstanceInfo
        //MEthod changed for - FB 2027 SetConference
        protected void AddInstanceInfo(string inXMLSetConference, string outxml)
        {
            try
            {
                XmlDocument xmldoc = new XmlDocument();
                xmldoc.LoadXml(inXMLSetConference);

                #region Commented during FB 2027 SetConference
                //StringBuilder inxml = new StringBuilder();

                //inxml.Append("<getRecurDateList>");
                //inxml.Append(obj.OrgXMLElement());//Organization Module Fixes
                //inxml.Append("<userID>" + Session["userid"].ToString() + "</userID>");
                //inxml.Append("<confID>" + xmldoc.SelectSingleNode("//conference/confInfo/confID").InnerXml + "</confID>");
                //inxml.Append("<appointmentTime>" + xmldoc.SelectSingleNode("//conference/confInfo/appointmentTime").InnerXml + "</appointmentTime>");
                //inxml.Append("<recurrencePattern>" + xmldoc.SelectSingleNode("//conference/confInfo/recurrencePattern").InnerXml);
                //inxml.Append("</recurrencePattern>");

                //inxml.Append("<rooms>");
                //XmlNodeList nodes = xmldoc.SelectNodes("//conference/confInfo/locationList/selected/level1ID");
                //for (int i = 0; i < nodes.Count; i++)
                //    inxml.Append("<roomID>" + nodes[i].InnerText + "</roomID>");
                //inxml.Append("</rooms>");
                //inxml.Append("</getRecurDateList>");

                //string outxml = obj.CallCOM("GetRecurDateList", inxml.ToString(), Application["COM_configPath"].ToString());
                #endregion

                string setupDur = "0";
                string tearDur = "0";
                double setupDuration = double.MinValue;
                double teardownDuration = double.MinValue;
                if (xmldoc.SelectSingleNode("//conference/confInfo/setupDuration") != null)
                {
                    if (xmldoc.SelectSingleNode("//conference/confInfo/setupDuration").InnerText != "")
                    {
                        setupDur = xmldoc.SelectSingleNode("//conference/confInfo/setupDuration").InnerText;
                    }
                }
                if (xmldoc.SelectSingleNode("//conference/confInfo/teardownDuration") != null)
                {
                    if (xmldoc.SelectSingleNode("//conference/confInfo/teardownDuration").InnerText != "")
                    {
                        tearDur = xmldoc.SelectSingleNode("//conference/confInfo/teardownDuration").InnerText;
                    }
                }

                Double.TryParse(setupDur, out setupDuration);
                Double.TryParse(tearDur, out teardownDuration);

                #region Commented during FB 2027 SetConference
                //if (outxml.IndexOf("<error>") >= 0)
                //{
                //    errLabel.Text = obj.ShowErrorMessage(outxml);
                //    errLabel.Visible = true;
                //}
                //else
                //{
                #endregion

                xmldoc = null;
                xmldoc = new XmlDocument(); //FB 2027 SetConference
                xmldoc.LoadXml(outxml);
                XmlNodeList nodes = xmldoc.SelectNodes("//error/dateList/dateTime");  //FB 2027 SetConference
                XmlTextReader xtr;
                DataSet ds = new DataSet();
                foreach (XmlNode node in nodes)
                {
                    xtr = new XmlTextReader(node.OuterXml, XmlNodeType.Element, new XmlParserContext(null, null, null, XmlSpace.None));
                    ds.ReadXml(xtr, XmlReadMode.InferSchema);
                }
                DataView dv;
                DataTable dt;
                if (ds.Tables.Count > 0)
                    dv = new DataView(ds.Tables[0]);
                else
                    dv = new DataView();
                dt = dv.Table;

                if (!dt.Columns.Contains("formatDate"))
                    dt.Columns.Add("formatDate");
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    switch (dt.Rows[i]["conflict"].ToString())
                    {
                        case "0":
                            dt.Rows[i]["conflict"] = obj.GetTranslatedText("No conflict");
                            break;
                        case "1":
                            dt.Rows[i]["conflict"] = obj.GetTranslatedText("Room(s) not available.");
                            break;
                        case "2":
                            dt.Rows[i]["conflict"] = obj.GetTranslatedText("Outside office hours.");
                            break;
                        default:
                            dt.Rows[i]["conflict"] = "";
                            break;
                    }
                    dt.Rows[i]["formatDate"] = myVRMNet.NETFunctions.GetFormattedDate(dt.Rows[i]["startDate"].ToString());
                }
                dgConflict.Visible = true;
                tblConflict.Attributes.Add("style", "display:block");
                btnConfSubmit.Visible = false;
                dgConflict.DataSource = dv;
                dgConflict.DataBind();

                MetaBuilders.WebControls.ComboBox mb = new MetaBuilders.WebControls.ComboBox();
                MetaBuilders.WebControls.ComboBox mb1 = new MetaBuilders.WebControls.ComboBox();
                MetaBuilders.WebControls.ComboBox mb2 = new MetaBuilders.WebControls.ComboBox();
                Button btnConflict = new Button();

                foreach (DataGridItem dgi in dgConflict.Items)
                {
                    mb = (MetaBuilders.WebControls.ComboBox)dgi.FindControl("conflictStartTime");
                    mb.Items.Clear();
                    obj.BindTimeToListBox(mb, true, true);
                    int sHour = Convert.ToInt16(dgi.Cells[7].Text);
                    int sMin = Convert.ToInt16(dgi.Cells[8].Text);
                    DateTime cStartDate = Convert.ToDateTime(dgi.Cells[11].Text + " " + sHour + ":" + sMin + " " + dgi.Cells[9].Text);

                    mb.Text = cStartDate.ToString(tformat);
                    sMin = Convert.ToInt16(dgi.Cells[10].Text);
                    DateTime cEndDate = cStartDate.AddMinutes(sMin); //new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, sHour, sMin, 0);
                    mb1 = (MetaBuilders.WebControls.ComboBox)dgi.FindControl("conflictEndTime");
                    mb2 = (MetaBuilders.WebControls.ComboBox)dgi.FindControl("conflictSetupTime");
                    //ZD 100085 Starts
                    DateTime setupTime = cStartDate.AddMinutes(-setupDuration);
                    DateTime teardownTime = cEndDate.AddMinutes(teardownDuration);
                    //ZD 100085 End
                    mb2.Items.Clear();
                    obj.BindTimeToListBox(mb2, true, true);
                    mb2.Text = setupTime.ToString(tformat);

                    mb1.Items.Clear();

                    obj.BindTimeToListBox(mb1, true, true);
                    mb1.Text = cEndDate.ToString(tformat);
                    btnConflict = (Button)dgi.FindControl("btnViewConflict");
                    btnConflict.Text = obj.GetTranslatedText("View Conflict"); //FB 2272
                    btnConflict.UseSubmitBehavior = false;
                    btnConflict.CssClass = "altMedium0BlueButtonFormat";//FB 3030
                    btnConflict.Attributes.Add("Style", "Width:100pt");//FB 3030
                    btnConflict.OnClientClick = "javascript:return roomconflict('" + dgi.Cells[0].Text + "')"; //FB 2027 SetConference

                    if (dgi.Cells[1].Text == obj.GetTranslatedText("No conflict"))
                    {
                        btnConflict.Visible = false;
                    }
                    else
                    {
                        //FB 2027 SetConference ... start
                        if (!dgi.Cells[1].Text.Contains(obj.GetTranslatedText("Outside")))
                            btnConflict.Visible = true;
                        else
                            btnConflict.Visible = false;
                        //FB 2027 SetConference ... end
                    }
                }
                dgConflict.Focus();
            }
            catch (Exception ex)
            {
                log.Trace(ex.Message);//"Error reading individual conflict for this conference. Please contact your VRM Administrator.";
                errLabel.Visible = true;
            }
        }

        #endregion

        #region InitializeConflict
        protected void InitializeConflict(Object sender, DataGridItemEventArgs e)
        {
            try
            {
                if (e.Item.ItemType.Equals(ListItemType.Item) || e.Item.ItemType.Equals(ListItemType.AlternatingItem))
                {
                    if (Session["timeFormat"] != null)
                    {
                        if (Session["timeFormat"].ToString() == "0")
                        {
                            RegularExpressionValidator valid = (RegularExpressionValidator)e.Item.FindControl("RegConflictTime");
                            if (valid != null)
                            {
                                valid.ValidationExpression = "[0-2][0-9]:[0-5][0-9]";
                                valid.ErrorMessage = obj.GetTranslatedText("Invalid Time (HH:MM)");//ZD 104163
                            }
                            RegularExpressionValidator validend = (RegularExpressionValidator)e.Item.FindControl("RegConflictEndTime");
                            if (validend != null)
                            {
                                validend.ValidationExpression = "[0-2][0-9]:[0-5][0-9]";
                                validend.ErrorMessage = obj.GetTranslatedText("Invalid Time (HH:MM)");//ZD 104163
                            }

                            if (EnableBufferZone == 1)
                            {
                                RegularExpressionValidator validsetup = (RegularExpressionValidator)e.Item.FindControl("RegConflictTeardownTime");
                                if (validsetup != null)
                                {
                                    validsetup.ValidationExpression = "[0-2][0-9]:[0-5][0-9]";
                                    validsetup.ErrorMessage = obj.GetTranslatedText("Invalid Time (HH:MM)");//ZD 104163

                                }
                                RegularExpressionValidator validtear = (RegularExpressionValidator)e.Item.FindControl("RegConflictSetupTime");
                                if (validtear != null)
                                {
                                    validtear.ValidationExpression = "[0-2][0-9]:[0-5][0-9]";
                                    validtear.ErrorMessage = obj.GetTranslatedText("Invalid Time (HH:MM)");//ZD 104163

                                }
                            }
                        }
                    }

                    //ZD 100085 Starts
                    TemplateColumn lblSetup = (TemplateColumn)dgConflict.Columns[2];
                    TemplateColumn lblTear = (TemplateColumn)dgConflict.Columns[5];
                    //ZD 100085 End
                    MetaBuilders.WebControls.ComboBox setup = (MetaBuilders.WebControls.ComboBox)e.Item.FindControl("conflictSetupTime");
                    MetaBuilders.WebControls.ComboBox tear = (MetaBuilders.WebControls.ComboBox)e.Item.FindControl("conflictTeardownTime");

                    if (EnableBufferZone == 0)
                    {
                        setup.Visible = false;

                        lblSetup.Visible = false;

                    }
                    tear.Visible = false;
                    lblTear.Visible = false;
                }
            }
            catch (Exception ex)
            {
                errLabel.Visible = true;
                log.Trace(ex.Message);
            }
        }
        #endregion

        #region ChangeXML
        protected string ChangeXML(string inxml)
        {
            String recurringXPath = "";
            String customPatternXml = "";
            string timeZone = "";
            if (obj == null)
                obj = new myVRMNet.NETFunctions();

            try
            {
                XmlDocument xmldoc = new XmlDocument();
                xmldoc.LoadXml(inxml);

                XmlNode recurNode = null;

                //ZD 100405 Starts
                int MCUPreStartDur = 0, MCUPreEndDur = 0;
                int.TryParse(txtMCUConnect.Text, out MCUPreStartDur);
                int.TryParse(txtMCUDisConnect.Text, out MCUPreEndDur);
                //ZD 100405 End

                recurringXPath = "//conference/confInfo/recurrencePattern";

                customPatternXml += "<recurType>5</recurType>";
                customPatternXml += "<startDates></startDates>";

                recurNode = xmldoc.SelectSingleNode(recurringXPath);
                if (recurNode != null)
                {
                    recurNode.InnerXml = "";
                    recurNode.InnerXml = customPatternXml;

                    recurNode = xmldoc.SelectSingleNode("//conference/confInfo/recurrencePattern/startDates");
                }

                customPatternXml = "";
                XmlNode node;
                StringBuilder appointXml = new StringBuilder();
                node = xmldoc.SelectSingleNode("//conference/confInfo/appointmentTime/timeZone");
                timeZone = node.InnerXml;

                node = xmldoc.SelectSingleNode("//conference/confInfo/appointmentTime");
                StringBuilder cusXML = new StringBuilder();
                cusXML.Append("<customInstance>1</customInstance>");
                cusXML.Append("<instances>");

                CheckBox del;
                MetaBuilders.WebControls.ComboBox tb;
                DateTime conflictSD, conflictED;
                int cntDelete = 0;
                foreach (DataGridItem dgi in dgConflict.Items)
                {
                    del = (CheckBox)dgi.FindControl("chkConflictDelete");
                    if (!del.Checked)
                    {
                        string confStDate = myVRMNet.NETFunctions.GetDefaultDate(dgi.Cells[0].Text);
                        tb = (MetaBuilders.WebControls.ComboBox)dgi.FindControl("conflictStartTime");
                        confStDate = confStDate + " " + tb.Text;

                        if (!DateTime.TryParse(confStDate, out conflictSD))
                            throw new Exception(obj.GetTranslatedText("Invalid conflict date"));

                        string confEDDate = conflictSD.ToShortDateString();
                        tb = (MetaBuilders.WebControls.ComboBox)dgi.FindControl("conflictEndTime");
                        confEDDate = confEDDate + " " + tb.Text;

                        if (!DateTime.TryParse(confEDDate, out conflictED))
                            throw new Exception(obj.GetTranslatedText("Invalid conflict date"));

                        if (conflictED <= conflictSD)
                        {
                            conflictED = conflictSD.AddDays(1);
                            confEDDate = conflictED.ToShortDateString() + " " + tb.Text;

                            if (!DateTime.TryParse(confEDDate, out conflictED))
                                throw new Exception(obj.GetTranslatedText("Invalid conflict date"));
                        }

                        DateTime conflictSetupDate, conflictTearDate;
                        string setupDate = "";
                        string teardownDate = "";

                        if (dgi.Cells[0].Text != "")
                            setupDate = myVRMNet.NETFunctions.GetDefaultDate(dgi.Cells[0].Text);

                        tb = (MetaBuilders.WebControls.ComboBox)dgi.FindControl("conflictSetupTime");
                        setupDate = setupDate + " " + tb.Text;

                        DateTime.TryParse(setupDate, out conflictSetupDate);

                        if (conflictSetupDate < conflictSD)
                        {
                            setupDate = conflictED.ToShortDateString() + " " + tb.Text;
                            DateTime.TryParse(setupDate, out conflictSetupDate);
                        }

                        teardownDate = conflictED.ToShortDateString();

                        tb = (MetaBuilders.WebControls.ComboBox)dgi.FindControl("conflictEndTime");
                        teardownDate = teardownDate + " " + tb.Text;
                        DateTime.TryParse(teardownDate, out conflictTearDate);

                        if (conflictED < conflictTearDate)
                        {
                            teardownDate = conflictSetupDate.ToShortDateString() + " " + tb.Text;
                            DateTime.TryParse(teardownDate, out conflictTearDate);
                        }

                        //ZD 100085 Starts
                        /*TimeSpan conflictDur = conflictED.Subtract(conflictSD);
                        TimeSpan setupDuration = conflictSetupDate.Subtract(conflictSD);
                        TimeSpan tearDuration = conflictED.Subtract(conflictTearDate);
                        TimeSpan bufferDur = conflictTearDate.Subtract(conflictSetupDate);*/

                        TimeSpan conflictDur = conflictED.Subtract(conflictSD);
                        TimeSpan setupDuration = conflictSD.Subtract(conflictSetupDate);
                        TimeSpan tearDuration = conflictTearDate.Subtract(conflictED);

                        //ZD 100433 Starts
                        int actualDur = 0, setupDur = 0, teardownDur = 0;
                        int totalDuration = 0, buffDur = 0, mcuPreEndPostive = 0, minDiff = 0;

                        int.TryParse(conflictDur.TotalMinutes.ToString(), out actualDur);
                        int.TryParse(setupDuration.TotalMinutes.ToString(), out  setupDur);
                        int.TryParse(tearDuration.TotalMinutes.ToString(), out teardownDur);

                        if (MCUPreEndDur < 0)
                            mcuPreEndPostive = MCUPreEndDur * -1;

                        if (MCUPreEndDur > 0 || mcuPreEndPostive <= teardownDur)
                            buffDur = teardownDur;
                        else if (MCUPreEndDur < 0 && mcuPreEndPostive > teardownDur)
                            buffDur = mcuPreEndPostive;

                        if (setupDur > MCUPreStartDur)
                            totalDuration = actualDur + setupDur + buffDur;
                        else
                            totalDuration = actualDur + MCUPreStartDur + buffDur;

                        minDiff = actualDur - MCUPreEndDur;

                        if (setupDur > 0 && MCUPreStartDur > setupDur)
                            throw new Exception(obj.GetTranslatedText("Setup duration should be greater than or equal to MCU Connect duration."));

                        if (teardownDur > 0 && MCUPreEndDur > teardownDur)
                            throw new Exception(obj.GetTranslatedText("Tear-down duration should be greater than or equal to MCU Disconnect duration."));

                        if (actualDur <= 0)
                            throw new Exception(obj.GetTranslatedText("Please check the start and end time for the conference."));

                        if (actualDur < 15 || minDiff < 15)
                            throw new Exception(obj.GetTranslatedText("Invalid Duration. Conference duration should be minimum of 15 mins."));

                        //if (totalDuration > (24 * 60)) ///103925 
                            //throw new Exception(obj.GetTranslatedText("Invalid Duration. Conference duration should be maximum of 24 hours."));
                        //ZD 100433 End

                        if (EnableBufferZone == 1)
                        {
                            if (conflictSetupDate > conflictSD)
                                throw new Exception(obj.GetTranslatedText("Invalid Setup Time."));

                            if (conflictED > conflictTearDate)
                                throw new Exception(obj.GetTranslatedText("Invalid Teardown Time."));

                            if (conflictSetupDate > conflictTearDate)
                                throw new Exception(obj.GetTranslatedText("Invalid Setup/Teardown Time."));

                            //if (bufferDur.TotalMinutes < 15)
                            //    throw new Exception("Invalid Duration. Conference duration should be minimum of 15 mins.");
                            //ZD 100085 End
                        }

                        if (cntDelete == 0)
                        {
                            appointXml.Append("<appointmentTime>");
                            appointXml.Append("<timeZone>" + timeZone + "</timeZone>");
                            appointXml.Append("<startHour>" + conflictSD.ToString("hh") + "</startHour>");
                            appointXml.Append("<startMin>" + conflictSD.Minute + "</startMin>");
                            appointXml.Append("<startSet>" + conflictSD.ToString("tt") + "</startSet>");
                            appointXml.Append("<durationMin>" + conflictDur.TotalMinutes + "</durationMin>");
                            appointXml.Append("<setupDuration>" + setupDuration.TotalMinutes + "</setupDuration>");
                            appointXml.Append("<teardownDuration>" + tearDuration.TotalMinutes + "</teardownDuration>");
                            appointXml.Append("<PreStartDuration>" + MCUPreStartDur + "</PreStartDuration>");//ZD 100405
                            appointXml.Append("<PreEndDuration>" + MCUPreEndDur + "</PreEndDuration>"); //ZD 100405
                            appointXml.Append("</appointmentTime>");

                            cusXML.Append(appointXml.ToString());
                        }
                        cntDelete++;

                        recurNode.InnerXml += "<startDate>" + conflictSD.ToString("MM/dd/yyyy") + "</startDate>";

                        if (customPatternXml == "")
                            customPatternXml = conflictSD.ToString("MM/dd/yyyy");
                        else
                            customPatternXml += ", " + conflictSD.ToString("MM/dd/yyyy");

                        cusXML.Append("<instance>");
                        cusXML.Append("<startDate>" + conflictSD.ToString("MM/dd/yyyy") + "</startDate>");
                        cusXML.Append("<startHour>" + conflictSD.ToString("hh") + "</startHour>");
                        cusXML.Append("<startMin>" + conflictSD.Minute + "</startMin>");
                        cusXML.Append("<startSet>" + conflictSD.ToString("tt") + "</startSet>");
                        cusXML.Append("<durationMin>" + conflictDur.TotalMinutes + "</durationMin>");
                        cusXML.Append("<setupDuration>" + setupDuration.TotalMinutes + "</setupDuration>");
                        cusXML.Append("<teardownDuration>" + tearDuration.TotalMinutes + "</teardownDuration>");
                        cusXML.Append("<PreStartDuration>" + MCUPreStartDur + "</PreStartDuration>"); //ZD 100405
                        cusXML.Append("<PreEndDuration>" + MCUPreEndDur + "</PreEndDuration>"); //ZD 100405
                        cusXML.Append("</instance>");
                    }
                }
                cusXML.Append("</instances>");
                node.InnerXml = cusXML.ToString();

                recurNode = xmldoc.SelectSingleNode("//conference/confInfo/recurringText");
                if (recurNode != null)
                    recurNode.InnerXml = "Custom Date Selection: " + customPatternXml;


                if (cntDelete < 2)
                    return "<error><message>" + obj.GetErrorMessage(432) + "</message></error>";//FB 1881 //FB 2164 //ALLBUGS-128
                //return "<error><message>A recurring conference must contain at least (2) instances. Please modify recurrence pattern before submitting conference.</message><level>E</level><errorCode></errorCode></error>";
                else
                    return xmldoc.InnerXml;
            }
            catch (Exception ex)
            {
                log.Trace(ex.Message);
                errLabel.Visible = true;
                //ZD 100263
                errLabel.Text = obj.ShowSystemMessage();
                //errLabel.Text = ex.Message; //FB 2164
                return "<error><message>" + ex.Message + "</message></error>"; //ZD 100263 //ZD 100085

            }
        }
        #endregion

        #region RefreshRoom

        protected void RefreshRoom(object sender, EventArgs e)
        {
            try
            {
                StringBuilder inxml = new StringBuilder();
                inxml.Append("<conferenceTime>");
                inxml.Append("<userID>" + Session["userID"].ToString() + "</userID>");
                inxml.Append("<confID>" + lblConfID.Text + "</confID>");
                inxml.Append(obj.OrgXMLElement());
                inxml.Append("<confType>" + lstConferenceType.SelectedValue.ToString() + "</confType>");//ZD 100704
                //FB 2023
                if (chkStartNow.Checked)
                    inxml.Append("<immediate>1</immediate>");
                else
                    inxml.Append("<immediate>0</immediate>");

                inxml.Append("<recurring>0</recurring>");
                DateTime dStart, dEnd;
                //FB 2634
                dStart = DateTime.Parse(myVRMNet.NETFunctions.GetDefaultDate(confStartDate.Text) + " " + myVRMNet.NETFunctions.ChangeTimeFormat(confStartTime.Text)); //new DateTime(Convert.ToInt16(confStartDate.Text.Split('/')[2]), Convert.ToInt16(confStartDate.Text.Split('/')[0]), Convert.ToInt16(confStartDate.Text.Split('/')[1]), Convert.ToInt16(confStartTime.Text.Split(' ')[0].Split(':')[0]), Convert.ToInt16(confStartTime.Text.Split(' ')[0].Split(':')[1]), 0, DateTimeKind.Local); //FB 2588
                //dEnd = DateTime.Parse(myVRMNet.NETFunctions.GetDefaultDate(TearDownDate.Text) + " " + TeardownTime.Text); //new DateTime(Convert.ToInt16(confEndDate.Text.Split('/')[2]), Convert.ToInt16(confEndDate.Text.Split('/')[0]), Convert.ToInt16(confEndDate.Text.Split('/')[1]), Convert.ToInt16(confEndTime.Text.Split(' ')[0].Split(':')[0]), Convert.ToInt16(confEndTime.Text.Split(' ')[0].Split(':')[1]), 0, DateTimeKind.Local);
                dEnd = DateTime.Parse(myVRMNet.NETFunctions.GetDefaultDate(confEndDate.Text) + " " + myVRMNet.NETFunctions.ChangeTimeFormat(confEndTime.Text)); //new DateTime(Convert.ToInt16(confEndDate.Text.Split('/')[2]), Convert.ToInt16(confEndDate.Text.Split('/')[0]), Convert.ToInt16(confEndDate.Text.Split('/')[1]), Convert.ToInt16(confEndTime.Text.Split(' ')[0].Split(':')[0]), Convert.ToInt16(confEndTime.Text.Split(' ')[0].Split(':')[1]), 0, DateTimeKind.Local);

                TimeSpan ts = dEnd.Subtract(dStart);
                inxml.Append("<startDate>" + myVRMNet.NETFunctions.GetDefaultDate(confStartDate.Text) + "</startDate>");
                inxml.Append("<startHour>" + dStart.Hour + "</startHour>");
                inxml.Append("<startMin>" + dStart.Minute + "</startMin>");
                inxml.Append("<startSet>" + dStart.ToString("tt") + "</startSet>");
                inxml.Append("<timeZone>" + lstConferenceTZ.SelectedValue + "</timeZone>");
                //ZD 102008
                if (selectedloc.Value != "" && selectedloc.Value.Trim().EndsWith(","))
                    selectedloc.Value = selectedloc.Value.Substring(0, (selectedloc.Value.Trim().Length));
                inxml.Append("<selected>" + selectedloc.Value + "</selected>");

                Double durationMin = ts.TotalMinutes; // ts.Days * 24 * 60 + ts.Hours * 60 + ts.Minutes;
                inxml.Append("<durationMin>" + durationMin.ToString() + "</durationMin>");
                //inxml.Append("<availableOnly>0</availableOnly>");

                inxml.Append("</conferenceTime>");
                //string outxml = obj.CallCOM("GetAvailableRoom", inxml.ToString(), Application["COM_Configpath"].ToString());
                string outxml = obj.CallMyVRMServer("GetAvailableRoom", inxml.ToString(), Application["MyVRMServer_ConfigPath"].ToString());
                if (outxml.IndexOf("<error>") < 0)
                {
                    XmlDocument xmldoc = new XmlDocument();
                    xmldoc.LoadXml(outxml);
                    XmlNodeList nodes = xmldoc.DocumentElement.SelectNodes("//locationList/level3List/level3");
                    lstRoomSelection.Items.Clear();
                    treeRoomSelection.Nodes.Clear();
                    TreeNode tn = new TreeNode("All");
                    tn.Expanded = true;
                    treeRoomSelection.Nodes.Add(tn);
                    GetLocationList(nodes);
                }
                else
                {
                    errLabel.Visible = true;
                    errLabel.Text = obj.ShowErrorMessage(outxml);
                }
                int countLeaf = 0;
                int countMid = 0;
                int countTop = 0;
                if (!Application["roomExpandLevel"].ToString().Equals("list"))
                {
                    foreach (TreeNode tnTop in treeRoomSelection.Nodes[0].ChildNodes)
                    {
                        countMid = 0;
                        if (Application["roomExpandLevel"] != null)
                        {
                            if (Application["roomExpandLevel"].ToString() != "")
                            {
                                if (Int32.Parse(Application["roomExpandLevel"].ToString()) >= 2)
                                    tnTop.Expanded = true;
                            }
                        }
                        else
                            tnTop.Expanded = false;

                        foreach (TreeNode tnMid in tnTop.ChildNodes)
                        {
                            if (Application["roomExpandLevel"] != null)
                            {
                                if (Application["roomExpandLevel"].ToString() != "")
                                {
                                    if (Int32.Parse(Application["roomExpandLevel"].ToString()) >= 3)
                                        tnMid.Expanded = true;
                                }
                            }
                            else
                                tnMid.Expanded = false;
                            countLeaf = 0;
                            foreach (TreeNode tn in tnMid.ChildNodes)
                            {
                                for (int i = 0; i < selectedloc.Value.Trim().Split(',').Length; i++)
                                    if (tn.Depth.Equals(3) && tn.Value.Equals(selectedloc.Value.Split(',')[i].Trim()))
                                    {
                                        tn.Checked = true;
                                        countLeaf++;
                                    }
                            }
                            if (countLeaf.Equals(tnMid.ChildNodes.Count))
                            {
                                tnMid.Checked = true;
                                countMid++;
                            }
                        }
                        if (countMid.Equals(tnTop.ChildNodes.Count))
                        {
                            tnTop.Checked = true;
                            countTop++;
                        }
                    }
                    if (countTop.Equals(treeRoomSelection.Nodes[0].ChildNodes.Count))
                        treeRoomSelection.Nodes[0].Checked = true;
                }
                else
                {
                    rdSelView.Items.FindByValue("2").Selected = true;
                    rdSelView.SelectedIndex = 1;
                    rdSelView_SelectedIndexChanged(new object(), new EventArgs());
                }
                if (lstRoomSelection != null)
                {
                    if (lstRoomSelection.Items.Count == 0)
                    {
                        rdSelView.Enabled = false;
                        pnlListView.Visible = false;
                        pnlLevelView.Visible = false;
                        btnCompare.Enabled = false;
                        pnlNoData.Visible = true;
                    }
                    else if (lstRoomSelection.Items.Count > 0)
                    {
                        rdSelView.Enabled = true;
                        rdSelView_SelectedIndexChanged(new object(), new EventArgs());
                        btnCompare.Enabled = true;
                        pnlNoData.Visible = false;

                    }
                }
            }
            catch (Exception ex)
            {
                log.Trace("RefreshRoom : " + ex.Message);
                //errLabel.Text = "Error in getting Available Room(s). Please contact your VRM Administrator.";
                errLabel.Text = obj.ShowSystemMessage();//FB 1881
                errLabel.Visible = true;
            }
        }

        #endregion

        #region SyncRoomSelection

        protected void SyncRoomSelection()
        {
            try
            {
                if (rdSelView.SelectedIndex.Equals(1))
                {
                    foreach (TreeNode tnTop in treeRoomSelection.Nodes[0].ChildNodes)
                        foreach (TreeNode tnMid in tnTop.ChildNodes)
                            foreach (TreeNode tn in tnMid.ChildNodes)
                            {
                                tn.Checked = false;
                                foreach (ListItem lstItem in lstRoomSelection.Items)
                                    if ((lstItem.Selected == true) && (tn.Value.Equals(lstItem.Value)))
                                    {
                                        tn.Checked = true;
                                        selRooms += tn.Value + ",";
                                    }
                            }
                }
                else
                {
                    foreach (ListItem lstItem in lstRoomSelection.Items)
                        foreach (TreeNode tn in treeRoomSelection.CheckedNodes)
                        {
                            if (tn.Value.Equals(lstItem.Value) && (tn.Depth.Equals(3)))
                            {
                                lstItem.Selected = true;
                                selRooms += lstItem.Value + ", ";
                            }
                        }
                }
            }
            catch (Exception ex)
            {
                log.Trace(ex.Message);
                errLabel.Visible = true;
            }
        }

        #endregion

        #region GetAdvancedAVSettings
        /// <summary>
        /// GetAdvancedAVSettings
        /// </summary>
        /// <returns></returns>
        private void GetAdvancedAVSettings()
        {
            string bridgeID = "-1";
            string usrId = "0"; //ALLDEV-814
            string epaddress = "", PartyCode = "", IsUserAudio = ""; //ZD 101446 //ALLDEV-814
            try
            {
                usrEndpoints = new Hashtable();
                String inXML = "<GetAdvancedAVSettings><UserID>" + Session["userID"].ToString() + "</UserID>" + obj.OrgXMLElement() + "<ConfID>" + lblConfID.Text + "</ConfID></GetAdvancedAVSettings>";
                String outXML = obj.CallMyVRMServer("GetAdvancedAVSettings", inXML, Application["MyVRMServer_Configpath"].ToString());
                if (outXML.IndexOf("<error>") < 0)
                {
                    XmlDocument xmldoc = new XmlDocument();
                    xmldoc.LoadXml(outXML);
                    XmlNodeList nodes = xmldoc.SelectNodes("//GetAdvancedAVSettings/Endpoints/Endpoint");

                    //FB 2839 Start
                    Session.Remove("Confbridge");
                    Session.Add("Confbridge", outXML);
                    //FB 2839 End

                    XmlNodeList endpointNodes = xmldoc.SelectNodes("descendant::GetAdvancedAVSettings/Endpoints/Endpoint[Type='U']");
                    int AudioBridgeParty = 0;
                    if (endpointNodes != null)
                    {
                        foreach (XmlNode nd in endpointNodes)
                        {
                            //ZD 100834
                            AudioBridgeParty = 0;
                            int.TryParse(nd.SelectSingleNode("AudioBridgeParty").InnerText.Trim(), out AudioBridgeParty);
                            if (AudioBridgeParty >= 1)
                            {
                                bridgeID = nd.SelectSingleNode("BridgeID").InnerText.Trim();
                                //int.TryParse(nd.SelectSingleNode("ID").InnerText.Trim(), out usrId);
                                usrId = nd.SelectSingleNode("ID").InnerText.Trim() + "" + nd.SelectSingleNode("ProfileID").InnerText.Trim(); //ALLDEV-814
                                epaddress = nd.SelectSingleNode("Address").InnerText.Trim();
                                PartyCode = nd.SelectSingleNode("participantCode").InnerText.Trim(); //ZD 101446
                                IsUserAudio = nd.SelectSingleNode("IsUserAudio").InnerText.Trim(); //ALLDEV-814
                                
                                if (usrId !=  "0") 
                                {
                                    if (!usrEndpoints.Contains(usrId))
                                    {
                                        usrEndpoints.Add(usrId, bridgeID);
                                    }
                                }
                                txtPartyCode.Text = PartyCode; //ZD 101446

                            }
                        }

                        if (epaddress != "")
                        {
                            string[] spiltAdd = SpiltAddress(epaddress);

                            if (spiltAdd != null)
                            {
                                if (spiltAdd.Length > 2)
                                {
                                    txtAudioDialNo.Text = spiltAdd[0];
                                    txtConfCode.Text = spiltAdd[1];
                                    txtLeaderPin.Text = spiltAdd[2];

                                    txtAudioDialNo.Attributes.Remove("readonly"); //ALLDEV-814
                                    if (IsUserAudio == "0")
                                    {
                                        txtPartyCode.Attributes.Add("ReadOnly", "true");
                                        txtConfCode.Attributes.Add("ReadOnly", "true");
                                        txtLeaderPin.Attributes.Add("ReadOnly", "true");
                                    }
                                }
                            }
                        }
                    }
                }
                else
                {
                    log.Trace(obj.ShowErrorMessage(outXML));
                }
                if (Session["ConfEndPts"] == null)
                {
                    Session.Add("ConfEndPts", usrEndpoints);
                }
                else
                {
                    Session.Remove("ConfEndPts");
                    Session.Add("ConfEndPts", usrEndpoints);
                }
            }
            catch (Exception ex)
            {
                log.Trace(ex.Message);
                //ZD 100263
                errLabel.Text = obj.ShowSystemMessage();
                //errLabel.Text = ex.Message;
                errLabel.Visible = true;
            }
        }
        #endregion

        #region MethodToSplitAddress
        /// <summary>
        /// MethodToSplitAddress
        /// </summary>
        /// <param name="address"></param>
        /// <returns></returns>
        protected String[] SpiltAddress(String address)
        {
            String[] add = null;
            try
            {
                add = new String[3];
                if (address.Contains("D") && address.Contains("+"))
                {
                    add[0] = address.Split('D')[0];//Address
                    if (address.IndexOf('+') > 0)
                    {
                        add[1] = address.Split('D')[1].Split('+')[0]; //Conference Code
                        add[2] = address.Split('D')[1].Split('+')[1]; // Leader Pin
                    }
                    else
                    {
                        add[1] = address.Split('D')[1]; //Conference Code
                        add[2] = ""; // Leader Pin
                    }
                }
                else if (address.Contains("D") && !address.Contains("+"))
                {
                    add[0] = address.Split('D')[0];//Address
                    add[1] = address.Split('D')[1]; //Conference Code
                    add[2] = ""; // Leader Pin
                }
                else if (!address.Contains("D") && !address.Contains("+"))
                {
                    add[0] = address;//Address
                    add[1] = ""; //Conference Code
                    add[2] = ""; // Leader Pin
                }
                return add;

            }
            catch (Exception ex)
            {
                log.Trace(ex.Message);
                throw ex;
            }
        }

        #endregion

        #region Get File List

        private void GetFileList()
        {
            try
            {
                lblFileList.Text = "";

                if (lblUpload1.Text.Trim() != "")
                    lblFileList.Text = lblUpload1.Text.Trim() + "; ";

                if (lblUpload2.Text.Trim() != "")
                    lblFileList.Text += lblUpload2.Text.Trim() + "; ";

                if (lblUpload3.Text.Trim() != "")
                    lblFileList.Text += lblUpload3.Text.Trim() + "; ";

            }
            catch (Exception ex)
            {
                log.Trace("GetFileList : " + ex.Message);
            }
        }

        #endregion

        #region Validate Conferencing Duration
        public bool ValidateConferenceDur()
        {
            try
            {
                //FB 2634
                DateTime startDate = Convert.ToDateTime(myVRMNet.NETFunctions.GetDefaultDate(confStartDate.Text) + " " + myVRMNet.NETFunctions.ChangeTimeFormat(confStartTime.Text)); //FB 2588
                DateTime endDate = Convert.ToDateTime(myVRMNet.NETFunctions.GetDefaultDate(confEndDate.Text) + " " + myVRMNet.NETFunctions.ChangeTimeFormat(confEndTime.Text));

                TimeSpan confduration = endDate.Subtract(startDate);

                //ZD 100405 Starts
                double MCUPreStartDur = 0, MCUPreEndDur = 0;
                //ZD 100704
                if ((lstConferenceType.SelectedValue == ns_MyVRMNet.vrmConfType.AudioVideo || lstConferenceType.SelectedValue.Equals(ns_MyVRMNet.vrmConfType.OBTP) || lstConferenceType.SelectedValue == ns_MyVRMNet.vrmConfType.AudioOnly) && !chkStartNow.Checked && lstVMR.SelectedIndex == 0 && !chkPCConf.Checked) //ZD 100513
                {
                    double.TryParse(txtMCUConnect.Text, out MCUPreStartDur);
                    double.TryParse(txtMCUDisConnect.Text, out MCUPreEndDur);
                }

                if (confduration.TotalMinutes < 15 || (confduration.TotalMinutes - MCUPreEndDur) < 15)
                {
                    Session["hdModalDiv"] = "none";//ZD 101500
                    errLabel.Text = obj.GetTranslatedText("Invalid Duration.Conference duration should be minimum of 15 mins.");//FB 1830 - Translation
                    errLabel.Visible = true;
                    return false;
                }
                //ZD 100405 End

                return true;

            }
            catch (Exception ex)
            {
                log.Trace("ValidateConferenceDur : " + ex.Message);
                //ZD 100263
                errLabel.Text = obj.ShowSystemMessage();
                //errLabel.Text = ex.Message;
                errLabel.Visible = true;
                return false;

            }
        }
        #endregion
		//ALLDEV-814
        protected void GetConfTimeforAudbridge(ref string inxml)
        {
            try
            {
                DateTime dStart, dEnd;
                int immediate = 0;
                Double durationMin = Double.MinValue;
                int durationMins = 0;
                TimeSpan ts;

                inxml = "<login>";
                inxml += obj.OrgXMLElement();
                inxml += "<userID> " + Session["userID"].ToString() + "</userID>";
                inxml += "<confID>" + lblConfID.Text + "</confID>";
                if (chkRecurrence.Checked)
                {
                    if (StartDate.Text.Trim() != "")
                    {
                        confStartDate.Text = obj.ControlConformityCheck(StartDate.Text.Trim());

                        DateTime.TryParse(StartDate.Text.Trim(), out dStart);
                        confEndDate.Text = obj.ControlConformityCheck(StartDate.Text.Trim());
                    }
                }

                if (chkStartNow.Checked)
                {
                    durationMins = Convert.ToInt32(lstDuration.Text.Split(':')[0]) * 60 + Convert.ToInt32(lstDuration.Text.Split(':')[1]);
                    dStart = DateTime.Now;
                    dEnd = dStart.AddMinutes(Convert.ToDouble(durationMins));
                    immediate = 1;
                    ts = dEnd.Subtract(dStart);
                    durationMin = ts.TotalMinutes;
                }
                else
                {
                    dStart = DateTime.Parse(myVRMNet.NETFunctions.GetDefaultDate(confStartDate.Text) + " " + myVRMNet.NETFunctions.ChangeTimeFormat(confStartTime.Text));
                    if (chkRecurrence.Checked || (Session["isSpecialRecur"].ToString().Equals("1") && hdnSpecRec.Value == "1" && isEditMode == "0"))
                        dEnd = dStart.AddHours(Convert.ToDouble(RecurDurationhr.Text)).AddMinutes(Convert.ToDouble(RecurDurationmi.Text));
                    else
                        dEnd = DateTime.Parse(myVRMNet.NETFunctions.GetDefaultDate(confEndDate.Text) + " " + myVRMNet.NETFunctions.ChangeTimeFormat(confEndTime.Text));
                    ts = dEnd.Subtract(dStart);
                    durationMin = ts.TotalMinutes;
                }

                inxml += "<immediate>" + immediate + "</immediate>";                
                inxml += "		<startDate>" + dStart.ToShortDateString() + "</startDate>"; 
                inxml += "		<startHour>" + dStart.Hour + "</startHour>";
                inxml += "		<startMin>" + dStart.Minute + "</startMin>";
                inxml += "		<startSet>" + dStart.ToString("tt") + "</startSet>";
                inxml += "		<timeZone>" + lstConferenceTZ.SelectedValue + "</timeZone>";
                inxml += "		<durationMin>" + durationMin.ToString() + "</durationMin>";
                inxml += "		<FetchFrom>Express</FetchFrom>";
                inxml += "</login>";                
            }

            catch (Exception ex)
            {
                log.Trace("GetConfdetails" + ex.Message); 
                errLabel.Text = obj.ShowSystemMessage();
                errLabel.Visible = true;
            }
        }

        //ALLDEV-814 - End
        #region FetchAvailableAudioParticipants
        protected void FetchAvailableAudioParticipants(Object server, EventArgs e)
        {
            string mode = Request.QueryString["t"];
            if (mode == null && mode == "")
                mode = "e";
            FetchAudioParticipants(mode);

            if (hdnSelectedAudioDetails.Value == "")
                lstAudioParty.SelectedValue = "-1";
            else
            {
                if (lstAudioParty.Items.FindByValue(hdnSelectedAudioDetails.Value) != null)
                    lstAudioParty.SelectedValue = hdnSelectedAudioDetails.Value;
                else
                    lstAudioParty.SelectedValue = "-1";
            }
            AudioPartySelectedProperty();

            UpdatePanel3.Update();
        }

        private void AudioPartySelectedProperty()
        {
            //ALLDEV-814 - Start - To maintain the Audio Bridge text box values Readonly property
            if (lstAudioParty.SelectedValue.Equals("-1"))
            {
                txtAudioDialNo.Text = "";
                txtPartyCode.Text = "";
                txtConfCode.Text = "";
                txtLeaderPin.Text = "";

                txtAudioDialNo.Attributes.Add("ReadOnly", "true");
                txtPartyCode.Attributes.Add("ReadOnly", "true");
                txtConfCode.Attributes.Add("ReadOnly", "true");
                txtLeaderPin.Attributes.Add("ReadOnly", "true");
            }
            else
            {
                string lstselValue = lstAudioParty.SelectedValue;
                string[] strlstselValue;
                if (lstselValue != "")
                {
                    strlstselValue = lstselValue.Split('|');
                    txtAudioDialNo.Attributes.Remove("readonly");
                    if (strlstselValue[10] == "0")
                    {
                        txtPartyCode.Attributes.Add("ReadOnly", "true");
                        txtConfCode.Attributes.Add("ReadOnly", "true");
                        txtLeaderPin.Attributes.Add("ReadOnly", "true");
                    }
                    else
                    {
                        txtPartyCode.Attributes.Remove("readonly");
                        txtConfCode.Attributes.Remove("readonly");
                        txtLeaderPin.Attributes.Remove("readonly");
                    }
                }
            }
        }
        #endregion

        #region  Fetch Audio Participant details

        private void FetchAudioParticipants(string mode)
        {
			//ALLDEV-814
            string inXML1 = "";
            string outXML1 = "";
            try
            {
                //inXML1 = "<login>" + obj.OrgXMLElement() + "<userID>" + Session["userID"].ToString() + "</userID></login>";
                GetConfTimeforAudbridge(ref inXML1); //ALLDEV-814
                outXML1 = obj.CallMyVRMServer("GetAudioUserList", inXML1, Application["COM_ConfigPath"].ToString());
                XmlDocument xmldoc = new XmlDocument();

                if (outXML1.IndexOf("<error>") < 0)
                {
                    xmldoc.LoadXml(outXML1);
                    XmlNodeList nodes = xmldoc.SelectNodes("//users/user");

                    lstAudioParty.Items.Clear();
                    if (nodes != null)
                    {
                        lstAudioParty.Items.Add(new ListItem(obj.GetTranslatedText("None"), "-1"));//FB 2341
                        if (lstAudioParty.Items.FindByValue("-1") != null)
                            lstAudioParty.Items.FindByValue("-1").Selected = true;

                    }
                    else
                    {
                        lstAudioParty.Items.Add(new ListItem(obj.GetTranslatedText("No Items..."), "-1"));
                    }

                    string valueFld = "";
                    string textFld = "";
                    string adpartyid = "-1"; //ALLDEV-814

                    XmlNode node = null;
                    for (int i = 0; i < nodes.Count; i++)
                    {
                        if (i == 2)//FB 2341
                            break;

                        node = nodes[i];
                        //Int32.TryParse(node.SelectSingleNode("userID").InnerText.Trim(), out adpartyid);
                        adpartyid = node.SelectSingleNode("userID").InnerText.Trim() + "" + node.SelectSingleNode("profileId").InnerText.Trim(); //ALLDEV-814
                        valueFld = node.SelectSingleNode("userID").InnerText.Trim() +
                            "|" + node.SelectSingleNode("userEmail").InnerText.Trim() +
                            "|" + node.SelectSingleNode("audioDialIn").InnerText.Trim() +
                            "|" + node.SelectSingleNode("confCode").InnerText.Trim() +
                            "|" + node.SelectSingleNode("leaderPin").InnerText.Trim() +
                        "|" + node.SelectSingleNode("PartyCode").InnerText.Trim() +
                        "|" + node.SelectSingleNode("AddressType").InnerText.Trim() +
                        "|" + node.SelectSingleNode("Protocol").InnerText.Trim() +
                        "|" + node.SelectSingleNode("profileId").InnerText.Trim() +     //ALLDEV -814
                        //"|" + node.SelectSingleNode("uId").InnerText.Trim() +
                        "|0" +
                        "|" + node.SelectSingleNode("IsUserAudio").InnerText.Trim();                         

                        textFld = node.SelectSingleNode("firstName").InnerText.Trim() +
                            " " + node.SelectSingleNode("lastName").InnerText.Trim();

                        lstAudioParty.Items.Add(new ListItem(textFld, valueFld));
                        try
                        {
                            if (adpartyid == audioPartyId.ToString())//FB 2341 //ALLDEV-814
                               lstAudioParty.SelectedValue = valueFld;

                            if (mode == "e")
                            {
                                if (usrEndpoints != null)
                                    if (usrEndpoints.Count > 0)
                                        if (usrEndpoints.Contains(adpartyid))
                                        {
                                            lstAudioParty.ClearSelection();
                                            lstAudioParty.SelectedValue = valueFld;
                                            //bridgeId = usrEndpoints[audioPartyId].ToString().Trim();
                                            //ZD 104357
                                            hdnSelectedAudioDetails.Value = valueFld; //ALLDEV-814
                                            if (EnableAudioBridges > 0)
                                                isAVSettingsSelected = true;
                                        }
                            }
                        }
                        catch (Exception e) { }

                    }
                    
                    if (lstAudioParty.SelectedValue.Equals("-1"))//FB 2341//ALLDEV-814
                    {
                        txtAudioDialNo.Attributes.Add("ReadOnly", "true");
                        txtPartyCode.Attributes.Add("ReadOnly", "true");
                        txtConfCode.Attributes.Add("ReadOnly", "true");
                        txtLeaderPin.Attributes.Add("ReadOnly", "true");//ZD 101446
                    }
                }
                else
                {
                    errLabel.Text = obj.ShowErrorMessage(outXML1);
                    errLabel.Visible = true;
                }
            }
            catch (Exception e)
            {
                log.Trace("FetchAudioParticipants: " + e.Message);
            }
        }
        #endregion

        #region CheckTime

        protected Boolean CheckTime()
        {
            string result = "false";
            string passwordResult = "";// FB 1865
            string tmzone = "26";
            string timezonedisplay = "";
            string confid = "";// FB 1865
            bool rtn = true;
            string skipCheck = "0";
            //ZD 100405 Starts
            int setuptime = 0, MCUPreStartDur = 0;
            DateTime startDateTime = DateTime.MinValue;
            //ZD 100405 End
            String CheckWebEx = "0", WebExResult = "0"; // ZD 100221 WebEx
            /** FB 2440 **/
            //ZD 100085 Starts
            //DateTime strtDateTime = DateTime.MinValue;
            //DateTime sDateTime = DateTime.MinValue;
            //DateTime tDateTime = DateTime.MinValue;
            //DateTime dEnd = DateTime.MinValue;
            //Int32 iSDur = -1, iTDur = 16, iMCUResult = 0;
            //String sMcuResult = "-1";
            //ZD 100085 End
            /** FB 2440 **/


            try
            {
                //FB 1911 - Start
                string date = "";
                date = ((Recur.Value == "") ? confStartDate.Text : StartDate.Text);

                if (RecurSpec.Value != "")
                    date = RecurSpec.Value.Split('#')[2].Split('&')[0];
                //FB 1911 - End

                timezonedisplay = timeZone;//((Request.QueryString["t"].ToString().Equals("o") && flagClone.Equals(false)) ? timeZone : "0");

                if (Recur.Value != "" || chkStartNow.Checked || RecurSpec.Value != "") //FB 2266 //if (Recur.Value != "" && RecurSpec.Value != "") //FB 2546
                    skipCheck = "1";


                if (obj == null)
                    obj = new myVRMNet.NETFunctions();

                confid = lblConfID.Text; //FB 1865

                if (Request.QueryString["t"] != null)//FB 1865
                {

                    if (Request.QueryString["t"].ToString().Equals("o"))
                        confid = "new";
                }

                //ZD 100433 Starts
                if (EnableBufferZone == 1)
                {
                    if (RecurSpec.Value != "")
                    {
                        if (hdnSetupTime.Value == "" || hdnSetupTime.Value == null)
                            hdnSetupTime.Value = "0";

                        int.TryParse(hdnSetupTime.Value, out setuptime);
                    }
                    else
                        int.TryParse(SetupDuration.Text, out setuptime);
                }
                DateTime.TryParse(myVRMNet.NETFunctions.GetDefaultDate(date) + " " + myVRMNet.NETFunctions.ChangeTimeFormat(confStartTime.Text), out startDateTime);
                int.TryParse(txtMCUConnect.Text, out MCUPreStartDur);

                if (setuptime > MCUPreStartDur)
                    startDateTime = startDateTime.AddMinutes(-setuptime);
                else
                    startDateTime = startDateTime.AddMinutes(-MCUPreStartDur); //Need to Check this datetime with current datetime.
                //ZD 100433 End

                // FB 2440
                //ZD 100085 Starts
                /*if (EnableBufferZone == 1)
                {
                    //FB 2634
                    if (Recur.Value.Trim() == "")
                    {
                        Int32.TryParse(SetupDuration.Text, out iSDur);
                        Int32.TryParse(TearDownDuration.Text, out iTDur);
                    }
                    else
                    {
                        if (hdnSetupTime.Value == "" || hdnSetupTime.Value == null)
                            hdnSetupTime.Value = "0";

                        if (hdnTeardownTime.Value == "" || hdnTeardownTime.Value == null)
                            hdnTeardownTime.Value = "0";

                        Int32.TryParse(hdnSetupTime.Value, out iSDur);
                        Int32.TryParse(hdnSetupTime.Value, out iTDur);

                        //sDateTime = DateTime.Parse(myVRMNet.NETFunctions.GetDefaultDate(SetupDate.Text) + " " + SetupTime.Text);
                        //tDateTime = DateTime.Parse(myVRMNet.NETFunctions.GetDefaultDate(TearDownDate.Text) + " " + TeardownTime.Text);
                        //strtDateTime = DateTime.Parse(myVRMNet.NETFunctions.GetDefaultDate(date) + " " + confStartTime.Text);
                        //dEnd = DateTime.Parse(myVRMNet.NETFunctions.GetDefaultDate(confEndDate.Text) + " " + confEndTime.Text);
                        //iSDur = (int)sDateTime.Subtract(strtDateTime).TotalMinutes;
                    }
                }*/
                //ZD 100085 End
                // FB 2440
                if (chkWebex.Checked == true) // ZD 100221 WebEx
                    CheckWebEx = "1";
                string resXML;
                //ZD 101672
                if (lstConferenceType.SelectedValue == ns_MyVRMNet.vrmConfType.RoomOnly || lstConferenceType.SelectedValue == ns_MyVRMNet.vrmConfType.HotDesking)//FB 1865
                {
                    confPassword.Value = "";
                    txtVMRPIN1.Text = "";
                }
                string inputXML = "<login>" + obj.OrgXMLElement() + "<userID>" + Session["userID"].ToString() + "</userID><hostID>" + hdnApprover4.Text + "</hostID><systemDate>" + Session["systemDate"].ToString() + "</systemDate><systemTime>" + Session["systemTime"].ToString() + "</systemTime><confDate>" + myVRMNet.NETFunctions.GetDefaultDate(startDateTime.ToString(format)) + "</confDate><confTime>" + myVRMNet.NETFunctions.ChangeTimeFormat(startDateTime.ToString(tformat)) + "</confTime><timeZone>" + lstConferenceTZ.SelectedValue + "</timeZone><timezoneDisplay>" + timezonedisplay + "</timezoneDisplay><password>" + confPassword.Value + "</password><hostPassword>" + hdnHostPassword.Value + "</hostPassword><confID>" + confid + "</confID><skipCheck>" + skipCheck + "</skipCheck><chkWebEx> " + CheckWebEx + " </chkWebEx></login>";//FB 1865 FB 2440 //FB 2588 //ZD 100793 //ZD 100221//ALLDEV-826
                //string inputXML = "<login>" + obj.OrgXMLElement() + "<userID>" + Session["userID"].ToString() + "</userID><hostID>" + hdnApprover4.Text + "</hostID><systemDate>" + Session["systemDate"].ToString() + "</systemDate><systemTime>" + Session["systemTime"].ToString() + "</systemTime><confDate>" + myVRMNet.NETFunctions.GetDefaultDate(startDateTime.ToString(format)) + "</confDate><confTime>" + myVRMNet.NETFunctions.ChangeTimeFormat(startDateTime.ToString(tformat)) + "</confTime><timeZone>" + lstConferenceTZ.SelectedValue + "</timeZone><timezoneDisplay>" + timezonedisplay + "</timezoneDisplay><password>" + confPassword.Value + "</password><confID>" + confid + "</confID><skipCheck>" + skipCheck + "</skipCheck><chkWebEx> " + CheckWebEx + " </chkWebEx></login>";//FB 1865 FB 2440 //FB 2588 //ZD 100793 //ZD 100221//ALLDEV-826
                //string inputXML = "<login>" + obj.OrgXMLElement() + "<userID>" + Session["userID"].ToString() + "</userID><hostID>" + hdnApprover4.Text + "</hostID><systemDate>" + Session["systemDate"].ToString() + "</systemDate><systemTime>" + Session["systemTime"].ToString() + "</systemTime><confDate>" + myVRMNet.NETFunctions.GetDefaultDate(date) + "</confDate><confTime>" + myVRMNet.NETFunctions.ChangeTimeFormat(confStartTime.Text) + "</confTime><timeZone>" + lstConferenceTZ.SelectedValue + "</timeZone><timezoneDisplay>" + timezonedisplay + "</timezoneDisplay><password>" + confPassword.Value + "</password><confID>" + confid + "</confID><skipCheck>" + skipCheck + "</skipCheck><mcuSetup>" + iSDur.ToString() + "</mcuSetup><mcuTeardown>" + iTDur.ToString() + "</mcuTeardown></login>";//FB 1865 2440

                resXML = obj.CallMyVRMServer("Isconferenceschedulable", inputXML, Application["MyVRMServer_ConfigPath"].ToString());
                XmlDocument xmlDOC = new XmlDocument();
                xmlDOC.LoadXml(resXML);


                if (timeZone == "0")
                {
                    if (xmlDOC.SelectSingleNode("/login/hosttimezone") != null)
                        tmzone = xmlDOC.SelectSingleNode("/login/hosttimezone").InnerText;

                    if (lstConferenceTZ.Items.FindByValue(tmzone) != null)// && (Request.QueryString["t"].ToString().Equals("o") && flagClone.Equals(false)))
                    {
                        lstConferenceTZ.ClearSelection();
                        lstConferenceTZ.Items.FindByValue(tmzone).Selected = true;
                    }

                }
                if (xmlDOC.SelectSingleNode("/login/result") != null)
                    result = xmlDOC.SelectSingleNode("/login/result").InnerText;

                if (xmlDOC.SelectSingleNode("/login/passwordCheck") != null)
                    passwordResult = xmlDOC.SelectSingleNode("/login/passwordCheck").InnerText;

                if (xmlDOC.SelectSingleNode("/login/WebExEligStat") != null) // ZD 100221 WebEx
                    WebExResult = xmlDOC.SelectSingleNode("/login/WebExEligStat").InnerText;

                if (WebExResult == "1")
                {
                    Session["hdModalDiv"] = "none";//ZD 101500
                    errLabel.Visible = true;
                    errLabel.Text = obj.GetTranslatedText("WebEx not enabled for this user");
                    //isCheckTimeError = true;
                }
                /** FB 2440 **/
                //if (xmlDOC.SelectSingleNode("/login/mcuresult") != null)
                //    sMcuResult = xmlDOC.SelectSingleNode("/login/mcuresult").InnerText;

                //Int32.TryParse(sMcuResult.Trim(), out iMCUResult);
                /** FB 2440 **/

                if (result.ToUpper().Equals("FALSE"))
                {
                    //coded and commented for ZD 104265 START
                    //string tmezne = "";
                    //if (timeZone == "0")
                    //    tmezne = " host timezone.(" + lstConferenceTZ.SelectedItem.Text + ")";
                    //else
                    //    //tmezne = " selected conference timezone.";
                    
                    errLabel.Visible = true;
                    if (timeZone == "0")
                        errLabel.Text = obj.GetTranslatedText("Invalid Start Date or Time. It should be greater than current time in host time zone.") + lstConferenceTZ.SelectedItem.Text; //104169
                    else
                        errLabel.Text = obj.GetTranslatedText("Invalid Start Date or Time. It should be greater than current time in selected conference time zone."); //104169
                    Session["hdModalDiv"] = "none";//ZD 101500
                    
                    //coded and commented for ZD 104265 End
                    /** FB 2440 **/
                    //if (iMCUResult == 1)
                    //    errLabel.Text = obj.GetTranslatedText("Setup time cannot be less than the MCU pre start time.");
                    //else if (iMCUResult == 2)
                    //    errLabel.Text = obj.GetTranslatedText("Teardown time cannot be less than the MCU pre end time.");
                    /** FB 2440 **/

                    rtn = false;
                }
                // FB 1865 //ZD 101672
                else if (passwordResult == "0" || passwordResult == "00" && lstConferenceType.SelectedValue != ns_MyVRMNet.vrmConfType.RoomOnly && lstConferenceType.SelectedValue != ns_MyVRMNet.vrmConfType.HotDesking) //ALLDEV-826
                {
                    Session["hdModalDiv"] = "none";//ZD 101500
                    errLabel.Visible = true;
                    //ALLDEV-826 Starts
                    if (Session["EnableHostGuestPwd"] != null && Session["EnableHostGuestPwd"].ToString() == "1")
                    {
                        if (passwordResult == "0")
                            errLabel.Text = obj.GetTranslatedText("Please check the guest password. It must be unique");
                        else if (passwordResult == "00")
                            errLabel.Text = obj.GetTranslatedText("Please check the host password. It must be unique");
                    }
                    else
                    //ALLDEV-826 Ends
                        errLabel.Text = obj.GetTranslatedText("Please check the password. It must be unique");//FB 1830 - Translation

                    rtn = false;
                }
                // FB 1865
                else
                {
                    errLabel.Text = "";
                }


            }
            catch (Exception ex)
            {
                errLabel.Visible = true;
                log.Trace(ex.StackTrace + " : " + ex.Message);

            }

            return rtn;
        }
        #endregion

        //FB 2426 - Start

        #region Video Guest Location Submit
        /// <summary>
        /// Video Guest Location Submit
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void fnGuestLocationSubmit(object sender, EventArgs e)
        {
            DataRow dr = null;
            //ZD 100619 Starts
            DataTable dt = new DataTable();
            DataGridItem dgi;
            DataRow drp, onflyp;
            int isDeft = 0;
            //ZD 100619 Ends

            onflyGrid = (DataTable)Session["OnlyFlyRoom"];
            onflyGridProfiles = (DataTable)Session["onflyGridProfiles"]; //ZD 100619 
            if (onflyGrid == null || onflyGrid.Rows.Count.Equals(0))
            {
                CreateDtColumnNames();
                onflyGrid = obj.LoadDataTable(null, colNames);

                if (!onflyGrid.Columns.Contains("RowUID"))
                    onflyGrid.Columns.Add("RowUID");
            }
            //ZD 100619 Ends
            if (onflyGridProfiles == null || onflyGridProfiles.Rows.Count.Equals(0))
            {
                CreateProfileDtColumnNames();
                onflyGridProfiles = obj.LoadDataTable(null, colNames);
            }
            CreateProfileDtColumnNames();
            dt = obj.LoadDataTable(null, colNames);
            //ZD 100619 Ends

            for (int i = 0; i < onflyGrid.Rows.Count; i++)
            {
                if (onflyGrid.Rows[i]["RowUID"].ToString().Trim() != hdnGuestloc.Value.Trim()
                    && onflyGrid.Rows[i]["RoomName"].ToString().Trim() == txtsiteName.Text.Trim())
                {
                    // FB 2525 Starts
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "errMessage", "<script>alert('" + obj.GetTranslatedText("Room Name has already been used") + "');</script>", false);
                    //errLabel.Text = obj.GetTranslatedText("Room Name has already been used");
                    //errLabel.Visible = true; FB 2525 Ends
                    return;
                }
            }

            dr = onflyGrid.NewRow();
            string rowUID = "";
            if (hdnGuestloc.Value != "")// && hdnGuestloc.Value != "0")
                rowUID = hdnGuestloc.Value;
            else
                rowUID = hdnROWID.Value;

            dr["RowUID"] = rowUID;
            
            dr["RoomID"] = hdnGuestRoomID.Value;
            dr["RoomName"] = txtsiteName.Text.Trim();
            dr["ContactName"] = txtApprover5.Text.Trim();
            dr["ContactEmail"] = txtEmailId.Text.Trim();
            dr["ContactPhoneNo"] = txtPhone.Text.Trim();
            dr["ContactAddress"] = txtAddress.Text.Trim();
            dr["State"] = txtState.Text.Trim();
            dr["City"] = txtCity.Text.Trim();
            dr["ZIP"] = txtZipcode.Text.Trim();
            dr["Country"] = lstCountries.SelectedValue.ToString();
            //ZD 100619 Starts
            dr["GuestContactPhoneNo"] = obj.ControlConformityCheck(txtContactPhone.Text.Trim()); //ZD 100619 
            dr["DftConnecttype"] = lstGuestConnectionType.SelectedValue.ToString(); //ZD 100619
            dr["GuestAdmin"] = hdnApprover5.Text; //ZD 100619
            dr["GuestAdminID"] = hdnGusetAdmin.Text; //ZD 100619

            for (int i = 0; i < dgProfiles.Items.Count; i++)
            {

                isDeft = 0;
                dgi = dgProfiles.Items[i];
                drp = dt.NewRow();
                onflyp = onflyGridProfiles.NewRow();
                onflyp["ProfileCnt"] = drp["ProfileCnt"] = i.ToString();
                onflyp["RowUID"] = drp["RowUID"] = rowUID; //onflyGrid.Rows.Count; //ZD 100619
                onflyp["Address"] = drp["Address"] = ((TextBox)dgi.FindControl("txtGuestAddress")).Text;
                onflyp["AddressType"] = drp["AddressType"] = ((DropDownList)dgi.FindControl("lstGuestAddressType")).SelectedValue;
                onflyp["ConnectionType"] = drp["ConnectionType"] = ((DropDownList)dgi.FindControl("lstGuestConnectionType1")).SelectedValue;
                onflyp["MaxLineRate"] = drp["MaxLineRate"] = ((DropDownList)dgi.FindControl("lstGuestLineRate")).SelectedValue;
                if (((RadioButton)dgi.FindControl("rdDefault")).Checked)
                    isDeft = 1;
                onflyp["DefaultProfile"] = drp["DefaultProfile"] = isDeft;
                onflyp["BridgeID"] = drp["BridgeID"] = ((DropDownList)dgi.FindControl("lstGuestBridges")).SelectedValue;
                isDeft = 0;
                if (((CheckBox)dgi.FindControl("chkDelete")).Checked)
                    isDeft = 1;
                onflyp["Deleted"] = drp["Deleted"] = isDeft;
                dt.Rows.Add(drp);

                if (hdnGuestRoom.Value != "")
                {
                    if (Convert.ToInt32(hdnGuestRoom.Value) == -1)
                    {
                        onflyGridProfiles.Rows.Add(onflyp);
                    }
                    else
                    {
                        Int32 proCnt = onflyGridProfiles.Rows.Count;
                        Boolean isExists = false;
                        for (int x = 0; x < proCnt; x++)
                        {
                            if (onflyGridProfiles.Rows[x]["RowUID"].Equals(rowUID))
                            {
                                if (onflyGridProfiles.Rows[x]["ProfileCnt"].Equals(i.ToString()))
                                {
                                    onflyGridProfiles.Rows.RemoveAt(x);
                                    onflyGridProfiles.Rows.InsertAt(onflyp, x);

                                    isExists = true;
                                }
                            }
                        }

                        if (!isExists)
                            onflyGridProfiles.Rows.Add(onflyp);
                    }
                }
            }

            dgProfiles.DataSource = dt;
            dgProfiles.DataBind();
            //ZD 100619 Ends

            if (hdnGuestRoom.Value != "")
            {
                if (Convert.ToInt32(hdnGuestRoom.Value) == -1)
                {
                    onflyGrid.Rows.Add(dr);
                }
                else
                {
                    onflyGrid.Rows.RemoveAt(Convert.ToInt32(hdnGuestRoom.Value));
                    onflyGrid.Rows.InsertAt(dr, Convert.ToInt32(hdnGuestRoom.Value));
                }
            }
            BindOptionData();
            dgOnflyGuestRoomlist.Visible = true;
            //ZD 100619
            if (hdnGuestloc.Value == "" && hdnROWID.Value != "")
                hdnROWID.Value = (Convert.ToInt32(hdnROWID.Value) + 1).ToString();

            hdnGuestloc.Value = "";
            hdnGuestRoomID.Value = "";
            LoadGuestEndpointProfile(-1);
            updtguest.Update();//ZD100619
            guestLocationPopup.Hide();
        }
        #endregion video guest location submit

        #region video guest location Cancel
        /// <summary>
        /// video guest location Cancel
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void fnGuestLocationCancel(object sender, EventArgs e)
        {
            try
            {
                LoadGuestEndpointProfile(-1); //ZD 100619
                //guestLocationPopup.Hide();
            }
            catch (Exception ex)
            {
                log.Trace("fnGuestLocationCancel" + ex.Message);
            }
        }
        #endregion video guest location Cancel

        #region Bind Option Data
        /// <summary>
        /// Bind Option Data
        /// </summary>
        private void BindOptionData()
        {
            try
            {
                if (onflyGrid == null)
                    onflyGrid = new DataTable();

                if (Session["OnlyFlyRoom"] == null)
                    Session.Add("OnlyFlyRoom", onflyGrid);
                else
                    Session["OnlyFlyRoom"] = onflyGrid;
                //ZD 100619 Starts
                if (Session["onflyGridProfiles"] == null)
                    Session.Add("onflyGridProfiles", onflyGridProfiles);
                else
                    Session["onflyGridProfiles"] = onflyGridProfiles;
                //ZD 100619 Ends
                dgOnflyGuestRoomlist.DataSource = onflyGrid;
                dgOnflyGuestRoomlist.DataBind();

                if (onflyGrid.Rows.Count.Equals(0))
                {
                    dgOnflyGuestRoomlist.Visible = false;
                    hdnGusetLoc.Value = "0"; //ZD 101299
                }
                else
                {
                    dgOnflyGuestRoomlist.Visible = true;
                    hdnGusetLoc.Value = "1"; //ZD 101299
                }
            }
            catch (Exception ex)
            {
                errLabel.Visible = true;
                //ZD 100263
                log.Trace("Page_Load" + ex.Message);
                errLabel.Text = obj.ShowSystemMessage();
                //errLabel.Text = ex.Message;
            }
        }
        #endregion

        #region dgOnflyGuestRoomlist_Edit
        /// <summary>
        /// dgOnflyGuestRoomlist_Edit
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void dgOnflyGuestRoomlist_Edit(Object sender, DataGridCommandEventArgs e)
        {
            try
            {
                int n = e.Item.ItemIndex, GuestPfRowID = 0;
                hdnGuestRoom.Value = n.ToString();

                if (Session["OnlyFlyRoom"] != null)
                    onflyGrid = (DataTable)Session["OnlyFlyRoom"];
                //ZD 100619 
                if (Session["onflyGridProfiles"] != null)
                    onflyGridProfiles = (DataTable)Session["onflyGridProfiles"];

                DataRow dr = onflyGrid.Rows[n];
                hdnGuestloc.Value = dr["RowUID"].ToString();
                int.TryParse(hdnGuestloc.Value, out GuestPfRowID);
                hdnGuestRoomID.Value = dr["RoomID"].ToString();
                txtsiteName.Text = dr["RoomName"].ToString();
                txtApprover5.Text = dr["ContactName"].ToString();
                txtEmailId.Text = dr["ContactEmail"].ToString();
                txtPhone.Text = dr["ContactPhoneNo"].ToString();
                txtAddress.Text = dr["ContactAddress"].ToString();
                if (dr["State"].ToString().Trim() == "55")
                    dr["State"] = "AB";
                else if (dr["State"].ToString().Trim() == "68")
                    dr["State"] = "AGS";
                else
                    dr["State"] = "NY";
                //ZD 100619 Starts
                txtContactPhone.Text = dr["GuestContactPhoneNo"].ToString(); //ZD 100619
                lstGuestConnectionType.SelectedValue = dr["DftConnecttype"].ToString(); //ZD 100619
                hdnApprover5.Text = dr["GuestAdmin"].ToString();
                hdnGusetAdmin.Text = dr["GuestAdminID"].ToString();
                //ZD 100619 Ends
                txtState.Text = dr["State"].ToString();
                txtCity.Text = dr["City"].ToString();
                txtZipcode.Text = dr["ZIP"].ToString();
                lstCountries.ClearSelection();
                lstCountries.SelectedValue = dr["Country"].ToString();
                //ZD 100619 Starts
                BindOptionData();
                BindProfileOptionData(GuestPfRowID);
                updtGuestLocation.Update();//ZD100619
                guestLocationPopup.Show();
            }
            catch (Exception ex)
            {
                log.Trace("dgOnflyGuestRoomlist_Edit" + ex.Message);
            }
        }

        #endregion

        #region dgOnflyGuestRoomlist_DeleteCommand
        /// <summary>
        /// dgOnflyGuestRoomlist_DeleteCommand 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void dgOnflyGuestRoomlist_DeleteCommand(object sender, DataGridCommandEventArgs e)
        {
            try
            {
                onflyGrid = (DataTable)Session["OnlyFlyRoom"];
                onflyGridProfiles = (DataTable)Session["onflyGridProfiles"];
                DataTable onflyDelete = (DataTable)Session["onflyGridProfiles"];
                int i = e.Item.ItemIndex;
                onflyGrid.Rows[i].Delete();

                string filterExp = "RowUID='" + e.Item.ItemIndex.ToString() + "'";
                DataRow[] drArr = onflyGridProfiles.Select(filterExp);
                foreach (DataRow dr in drArr)
                {
                    onflyGridProfiles.Rows.Remove(dr);
                }

                BindOptionData();
            }
            catch (Exception ex)
            {
                //ZD 100263
                log.Trace("dgOnflyGuestRoomlist_DeleteCommand" + ex.Message);
                errLabel.Text = obj.ShowSystemMessage();
                //errLabel.Text = ex.Message;
            }
        }
        #endregion

        #region Create Column Names
        /// <summary>
        /// Create Column Names
        /// </summary>
        private void CreateDtColumnNames()
        {
            colNames = new ArrayList();
            colNames.Add("RoomID");
            colNames.Add("RoomName");
            colNames.Add("ContactName");
            colNames.Add("ContactEmail");
            colNames.Add("ContactPhoneNo");
            colNames.Add("ContactAddress");
            colNames.Add("State");
            colNames.Add("City");
            colNames.Add("ZIP");
            colNames.Add("Country");
            colNames.Add("GuestContactPhoneNo"); //ZD 100619
            colNames.Add("DftConnecttype"); //ZD 100619
            colNames.Add("GuestAdmin");//ZD 100619 
            colNames.Add("GuestAdminID");//ZD 100619 
           
        }
        #endregion

        //FB 2426 - End

        //FB 2659 - Starts
        #region CheckSeatsAvailability
        /// <summary>
        /// CheckSeatsAvailability
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void CheckSeatsAvailability(object sender, EventArgs e)
        {
            try
            {
                StringBuilder inXML = new StringBuilder();
                DateTime dStart, dEnd;
                int setupDuration = 0, tearDuration = 0;

                dStart = DateTime.Parse(myVRMNet.NETFunctions.GetDefaultDate(confStartDate.Text) + " " + confStartTime.Text);
                dEnd = DateTime.Parse(myVRMNet.NETFunctions.GetDefaultDate(confEndDate.Text) + " " + confEndTime.Text);

                if (EnableBufferZone == 1)
                {
                    int.TryParse(SetupDuration.Text, out setupDuration);
                    int.TryParse(TearDownDuration.Text, out tearDuration);
                    dStart = dStart.AddMinutes(-setupDuration);
                    dEnd = dEnd.AddMinutes(tearDuration);
                }

                inXML.Append("<TDGetSlotAvailableSeats>");
                inXML.Append("<userID>" + Session["userID"].ToString() + "</userID>");
                inXML.Append("<editConfId>" + lblConfID.Text + "</editConfId>");
                inXML.Append("<startDate>" + dStart + "</startDate>");
                inXML.Append("<endDate>" + dEnd + "</endDate>");
                inXML.Append("<timezone>" + lstConferenceTZ.SelectedValue + "</timezone>");
                inXML.Append("</TDGetSlotAvailableSeats>");

                string outXML = obj.CallMyVRMServer("TDGetSlotAvailableSeats", inXML.ToString(), HttpContext.Current.Application["MyVRMServer_ConfigPath"].ToString());

                XmlDocument xd = new XmlDocument();
                xd.LoadXml(outXML);

                int totalOrgSeats = 0;
                if (xd.SelectSingleNode("//TDGetSlotAvailableSeats/TotalOrgSeats") != null)
                    int.TryParse(xd.SelectSingleNode("//TDGetSlotAvailableSeats/TotalOrgSeats").InnerText.Trim(), out  totalOrgSeats);

                if (totalOrgSeats <= 0)  //FB 3062
                {
                    Session["hdModalDiv"] = "none";//ZD 101500
                    errLabel.Text = obj.GetTranslatedText("Insufficient Seats. Please Contact Your VRM Administrator.");
                    errLabel.Visible = true;
                }
                else
                {

                    XmlNodeList nodes = xd.SelectNodes("//TDGetSlotAvailableSeats/availableSeatsList/availableSeats");

                    XmlTextReader xtr;
                    DataSet ds = new DataSet();

                    if (nodes != null)
                    {
                        for (int i = 0; i < nodes.Count; i++)
                        {
                            xtr = new XmlTextReader(nodes[i].OuterXml, XmlNodeType.Element, new XmlParserContext(null, null, null, XmlSpace.None));
                            ds.ReadXml(xtr, XmlReadMode.InferSchema);
                        }
                    }
                    DataTable dt = new DataTable();

                    tblSeatsAvailability.BorderColor = "Black";
                    HtmlTableRow trRow;
                    HtmlTableCell tdCell;
                    string tdConfDateId = "";
                    DateTime startDate = DateTime.Now;
                    DateTime endDate = DateTime.Now;
                    if (ds.Tables.Count > 0)
                    {
                        dt = ds.Tables[0];
                        for (int i = 0; i < 2; i++)
                        {
                            trRow = new HtmlTableRow();
                            if (i == 0)
                            {
                                for (int j = 0; j < dt.Rows.Count; j++)
                                {
                                    tdCell = new HtmlTableCell();
                                    tdCell.NoWrap = false;
                                    tdCell.Align = "left";
                                    tdCell.ID = "tdConfDate" + j;

                                    startDate = Convert.ToDateTime(dt.Rows[j]["startDate"].ToString());
                                    endDate = Convert.ToDateTime(dt.Rows[j]["endDate"].ToString());

                                    tdCell.InnerHtml = obj.GetTranslatedText("Start Date/Time") + ": " + myVRMNet.NETFunctions.GetFormattedDate(startDate.ToString(), format, Session["EmailDateFormat"].ToString()) + " " + startDate.ToString(tformat);//ZD 100995
                                    tdCell.InnerHtml += "<br>" + obj.GetTranslatedText("End Date/Time") + ": " + myVRMNet.NETFunctions.GetFormattedDate(endDate.ToString(), format, Session["EmailDateFormat"].ToString()) + " " + endDate.ToString(tformat); //ZD 100995
                                    tdCell.InnerHtml += "<br>" + obj.GetTranslatedText("Available Seats") + ": " + dt.Rows[j]["seatsNumber"].ToString();
                                    trRow.Height = "100px";
                                    trRow.Cells.Add(tdCell);
                                }
                            }
                            else
                            {
                                for (int j = 0; j < dt.Rows.Count; j++)
                                {
                                    tdCell = new HtmlTableCell();
                                    tdConfDateId = "tdConfDate" + j;
                                    tdCell.Style.Add("cursor", "pointer");

                                    if (dt.Rows[j]["seatsNumber"].ToString() == totalOrgSeats.ToString())
                                        tdCell.BgColor = "#65FF65";//Green-Totally Free.
                                    else
                                        tdCell.BgColor = "#F8F075";//Yellow-Partially Free.

                                    tdCell.Attributes.Add("onclick", "javascript:SelectOneDefault('" + tdConfDateId + "');");
                                    trRow.Height = "100px";
                                    trRow.Cells.Add(tdCell);
                                }
                            }
                            tblSeatsAvailability.Rows.Add(trRow);
                        }
                    }
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "seats", "fnShowSeats();", true);
                }
            }
            catch (Exception ex)
            {
                log.Trace("CheckSeatsAvailability: " + ex.Message);
                //ZD 100263
                errLabel.Text = obj.ShowSystemMessage();
                //errLabel.Text = ex.Message;
            }
        }
        #endregion
        //FB 2659 - End


        //FB 2693 Start
        #region FetchPCDetails
        /// <summary>
        /// fnFetchPCDetails
        /// </summary>
        /// <param name="userID"></param>
        /// <param name="PcType"></param>
        /// <returns></returns>
        [WebMethod]
        public static string fnFetchPCDetails(string userID, string pctype)
        {
            myVRMNet.NETFunctions obj;
            obj = new myVRMNet.NETFunctions();
            ns_Logger.Logger log;
            log = new ns_Logger.Logger();
            string res = "";
            StringBuilder inXML = new StringBuilder();
            String outXML = "";
            int PCId = 0;
            string htmlcontent = "";
            string Description = "", SkypeURL = "", VCDialinIP = "", VCMeetingID = "", VCDialinSIP = "", VCDialinH323 = "", VCPin = "", PCDialinNum = "";
            string PCDialinFreeNum = "", PCMeetingID = "", PCPin = "", Intructions = "";
            try
            {
                inXML.Append("<FetchPCDetails>");
                inXML.Append(obj.OrgXMLElement());
                inXML.Append("<userID>" + userID + "</userID>");
                inXML.Append("<PCType>" + pctype + "</PCType>");
                inXML.Append("</FetchPCDetails>");
                outXML = obj.CallMyVRMServer("FetchSelectedPCDetails", inXML.ToString(), HttpContext.Current.Application["MyVRMServer_Configpath"].ToString());

                if (outXML.IndexOf("<error>") >= 0)
                    res = obj.GetTranslatedText("Operation UnSuccessful");
                else
                {
                    htmlcontent = "";
                    XmlDocument xmldoc = new XmlDocument();
                    xmldoc.LoadXml(outXML);
                    XmlNodeList nodes = xmldoc.SelectNodes("//PCDetails/PCDetail");
                    if (nodes.Count > 0)
                    {
                        for (int i = 0; i < nodes.Count; i++)
                        {
                            if (nodes[i].SelectSingleNode("PCId") != null)
                                int.TryParse(nodes[i].SelectSingleNode("PCId").InnerText.Trim(), out PCId);
                            if (nodes[i].SelectSingleNode("Description") != null)
                                Description = nodes[i].SelectSingleNode("Description").InnerText.Trim();
                            if (nodes[i].SelectSingleNode("SkypeURL") != null)
                                SkypeURL = nodes[i].SelectSingleNode("SkypeURL").InnerText.Trim();
                            if (nodes[i].SelectSingleNode("VCDialinIP") != null)
                                VCDialinIP = nodes[i].SelectSingleNode("VCDialinIP").InnerText.Trim();
                            if (nodes[i].SelectSingleNode("VCMeetingID") != null)
                                VCMeetingID = nodes[i].SelectSingleNode("VCMeetingID").InnerText.Trim();
                            if (nodes[i].SelectSingleNode("PCDialinNum") != null)
                                PCDialinNum = nodes[i].SelectSingleNode("PCDialinNum").InnerText.Trim();
                            if (nodes[i].SelectSingleNode("PCDialinFreeNum") != null)
                                PCDialinFreeNum = nodes[i].SelectSingleNode("PCDialinFreeNum").InnerText.Trim();
                            if (nodes[i].SelectSingleNode("PCMeetingID") != null)
                                PCMeetingID = nodes[i].SelectSingleNode("PCMeetingID").InnerText.Trim();
                            if (nodes[i].SelectSingleNode("Intructions") != null)
                                Intructions = nodes[i].SelectSingleNode("Intructions").InnerText.Trim();
                            if (nodes[i].SelectSingleNode("VCDialinSIP") != null)
                                VCDialinSIP = nodes[i].SelectSingleNode("VCDialinSIP").InnerText.Trim();
                            if (nodes[i].SelectSingleNode("VCDialinH323") != null)
                                VCDialinH323 = nodes[i].SelectSingleNode("VCDialinH323").InnerText.Trim();
                            if (nodes[i].SelectSingleNode("VCPin") != null)
                                VCPin = nodes[i].SelectSingleNode("VCPin").InnerText.Trim();
                            if (nodes[i].SelectSingleNode("PCPin") != null)
                                PCPin = nodes[i].SelectSingleNode("PCPin").InnerText.Trim();
                            switch (PCId)
                            {
                                case ns_MyVRMNet.vrmPC.BlueJeans:
                                    htmlcontent = "<table width=\"100%\"<tr><td>";
                                    htmlcontent += "---------------------------------------------------------------------------------------<br />";
                                    htmlcontent += obj.GetTranslatedText("To join or start the meeting,go to") + ": <br />" + Description + "<br />";
                                    htmlcontent += "<br /> " + obj.GetTranslatedText("Or join directly with the following options:") + "<br /><br /></td></tr>";
                                    htmlcontent += "<tr><td>Skype:<br />" + SkypeURL + "<br /><br /></td></tr>";
                                    htmlcontent += "<tr><td>" + obj.GetTranslatedText("Video Conferencing Systems") + ":</td></tr>";
                                    htmlcontent += "<tr><td>" + obj.GetTranslatedText("Dial-in IP") + ": " + VCDialinIP + "<br />" + obj.GetTranslatedText("Meeting ID") + ": " + VCMeetingID + "<br /><br /></td></tr>";
                                    htmlcontent += "<tr><td>" + obj.GetTranslatedText("Phone Conferencing") + ":<br />" + obj.GetTranslatedText("Dial in toll number") + ": " + PCDialinNum + "<br />" + obj.GetTranslatedText("Dial in toll free number") + ": " + PCDialinFreeNum + "<br />" + obj.GetTranslatedText("Meeting ID") + ": " + VCMeetingID + "<br />";
                                    htmlcontent += "<br /></td></tr><tr><td>";
                                    htmlcontent += "***********************************************************";
                                    htmlcontent += "<br /><br />" + obj.GetTranslatedText("First time joining a Blue Jeans Video Meeting") + "?<br />" + obj.GetTranslatedText("For detailed instructions") + ": " + Intructions + "<br /><br />";
                                    htmlcontent += "***********************************************************";
                                    htmlcontent += "<br /><br />";
                                    htmlcontent += "***********************************************************";
                                    htmlcontent += "<br /><br />" + obj.GetTranslatedText("Test your video connection,talk to our video tester by clicking here") + "<br />" + Intructions + "<br /><br />";
                                    htmlcontent += "***********************************************************";
                                    htmlcontent += "<br /><br />" + obj.GetTranslatedText("(c) Blue Jeans Network 2012") + "</td></tr></table>";
                                    break;
                                case ns_MyVRMNet.vrmPC.Jabber:
                                    htmlcontent = "<table width=\"100%\"<tr><td>";
                                    htmlcontent += "---------------------------------------------------------------------------------------<br />";
                                    htmlcontent += obj.GetTranslatedText("To join or start the meeting,go to") + ": <br />" + Description + "<br />";
                                    htmlcontent += "<br /> " + obj.GetTranslatedText("Or join directly with the following options:") + "<br /><br /></td></tr>";
                                    htmlcontent += "<tr><td>Skype:<br />" + SkypeURL + "<br /><br /></td></tr>";
                                    htmlcontent += "<tr><td>" + obj.GetTranslatedText("Video Conferencing Systems") + ":</td></tr>";
                                    htmlcontent += "<tr><td>" + obj.GetTranslatedText("Dial-in IP") + ": " + VCDialinIP + "<br />" + obj.GetTranslatedText("Meeting ID") + ": " + VCMeetingID + "<br /><br /></td></tr>";
                                    htmlcontent += "<tr><td>" + obj.GetTranslatedText("Phone Conferencing") + ":<br />" + obj.GetTranslatedText("Dial in toll number") + ": " + PCDialinNum + "<br />" + obj.GetTranslatedText("Dial in toll free number") + ": " + PCDialinFreeNum + "<br />" + obj.GetTranslatedText("Meeting ID") + ": " + VCMeetingID + "<br />";
                                    htmlcontent += "<br /></td></tr><tr><td>";
                                    htmlcontent += "***********************************************************";
                                    htmlcontent += "<br /><br />" + obj.GetTranslatedText("First time joining a Jabber Video Meeting") + "?<br />" + obj.GetTranslatedText("For detailed instructions") + ": " + Intructions + "<br /><br />";
                                    htmlcontent += "***********************************************************";
                                    htmlcontent += "<br /><br />";
                                    htmlcontent += "***********************************************************";
                                    htmlcontent += "<br /><br />" + obj.GetTranslatedText("Test your video connection,talk to our video tester by clicking here") + "<br />" + Intructions + "<br /><br />";
                                    htmlcontent += "***********************************************************";
                                    htmlcontent += "<br /><br />" + obj.GetTranslatedText("(c) Jabber Network 2012") + "</td></tr></table>";
                                    break;
                                case ns_MyVRMNet.vrmPC.Lync:
                                    htmlcontent = "<table width=\"100%\"<tr><td>";
                                    htmlcontent += "---------------------------------------------------------------------------------------<br />";
                                    htmlcontent += obj.GetTranslatedText("To join or start the meeting,go to") + ": <br />" + Description + "<br />";
                                    htmlcontent += "<br /> " + obj.GetTranslatedText("Or join directly with the following options:") + "<br /><br /></td></tr>";
                                    htmlcontent += "<tr><td>Skype:<br />" + SkypeURL + "<br /><br /></td></tr>";
                                    htmlcontent += "<tr><td>" + obj.GetTranslatedText("Video Conferencing Systems") + ":</td></tr>";
                                    htmlcontent += "<tr><td>" + obj.GetTranslatedText("Dial-in IP") + ": " + VCDialinIP + "<br />" + obj.GetTranslatedText("Meeting ID") + ": " + VCMeetingID + "<br /><br /></td></tr>";
                                    htmlcontent += "<tr><td>" + obj.GetTranslatedText("Phone Conferencing") + ":<br />" + obj.GetTranslatedText("Dial in toll number") + ": " + PCDialinNum + "<br />" + obj.GetTranslatedText("Dial in toll free number") + ": " + PCDialinFreeNum + "<br />" + obj.GetTranslatedText("Meeting ID") + ": " + VCMeetingID + "<br />";
                                    htmlcontent += "<br /></td></tr><tr><td>";
                                    htmlcontent += "***********************************************************";
                                    htmlcontent += "<br /><br />" + obj.GetTranslatedText("First time joining a Lync Video Meeting") + "?<br />" + obj.GetTranslatedText("For detailed instructions") + ": " + Intructions + "<br /><br />";
                                    htmlcontent += "***********************************************************";
                                    htmlcontent += "<br /><br />";
                                    htmlcontent += "***********************************************************";
                                    htmlcontent += "<br /><br />" + obj.GetTranslatedText("Test your video connection,talk to our video tester by clicking here") + "<br />" + Intructions + "<br /><br />";
                                    htmlcontent += "***********************************************************";
                                    htmlcontent += "<br /><br />" + obj.GetTranslatedText("(c) Lync Network 2012") + "</td></tr></table>";
                                    break;
                                case ns_MyVRMNet.vrmPC.Vidtel:
                                    htmlcontent = "<table width=\"100%\"<tr><td>";
                                    htmlcontent += "---------------------------------------------------------------------------------------<br />";
                                    htmlcontent += obj.GetTranslatedText("To join or start the meeting,go to") + ": <br />" + Description + "<br />";
                                    htmlcontent += "<br /> " + obj.GetTranslatedText("Or join directly with the following options:") + "<br /><br /></td></tr>";
                                    htmlcontent += "<tr><td>Skype:<br />" + SkypeURL + "<br /><br /></td></tr>";
                                    htmlcontent += "<tr><td>" + obj.GetTranslatedText("Video Conferencing Systems") + ":</td></tr>";
                                    htmlcontent += "<tr><td>" + obj.GetTranslatedText("Dial-in SIP") + ": " + VCDialinSIP + "<br />" + obj.GetTranslatedText("Dial-in H.323") + ": " + VCDialinH323 + "<br />" + obj.GetTranslatedText("PIN") + ": " + VCPin + "<br /><br /></td></tr>";
                                    htmlcontent += "<tr><td>" + obj.GetTranslatedText("Phone Conferencing") + ":<br />" + obj.GetTranslatedText("Dial-in Toll Number") + ": " + PCDialinNum + "<br />" + obj.GetTranslatedText("Dial-in Toll Free Number") + ": " + PCDialinFreeNum + "<br />" + obj.GetTranslatedText("PIN") + ": " + PCPin + "<br />";
                                    //htmlcontent += "<br /></td></tr><tr><td>"; //FB 2723
                                    //htmlcontent += "***********************************************************";
                                    //htmlcontent += "<br /><br />Test your video connection,talk to our video tester by clicking here<br />" + Intructions + "<br /><br />";
                                    //htmlcontent += "***********************************************************";
                                    htmlcontent += "<br /><br />" + obj.GetTranslatedText("(c) Vidtel Network 2012") + "</td></tr></table>";
                                    break;
                                default:

                                    break;

                            }
                        }
                    }
                    else
                    {
                        htmlcontent = "<table border='0' cellpadding='0' cellspacing='0' width='100%'><tr><td colspan='3' height='20px'></td></tr>";
                        htmlcontent += "<tr><td colspan='3' align='center'><b style='font-family:Verdana;' >" + obj.GetTranslatedText("No Information") + "</b><br><br><br><br></td></tr>";
                    }
                    res = htmlcontent.ToString();
                    //res = "text";
                }
            }
            catch (Exception ex)
            {
                log.Trace("PacketDetails" + ex.Message);
            }
            return res;
        }
        #endregion

        private void CheckPCVendor()
        {
            try
            {
                if (Session["EnableBlueJeans"] != null)
                {
                    if (Session["EnableBlueJeans"].ToString().Equals("1"))
                        PCVendorType = 1;
                }
                if (Session["EnableJabber"] != null)
                {
                    if (Session["EnableJabber"].ToString().Equals("1"))
                    {
                        if (PCVendorType <= 0)
                            PCVendorType = 2;
                    }
                }
                if (Session["EnableLync"] != null)
                {
                    if (Session["EnableLync"].ToString().Equals("1"))
                    {
                        if (PCVendorType <= 0)
                            PCVendorType = 3;
                    }
                }
                if (Session["EnableVidtel"] != null)
                {
                    if (Session["EnableVidtel"].ToString().Equals("1"))
                    {
                        if (PCVendorType <= 0)
                            PCVendorType = 4;
                    }
                }
                switch (PCVendorType)
                {
                    //ZD 104021
                    //case ns_MyVRMNet.vrmPC.BlueJeans:
                    //    rdBJ.Checked = true;
                    //    break;
                    case ns_MyVRMNet.vrmPC.Jabber:
                        rdJB.Checked = true;
                        break;
                    case ns_MyVRMNet.vrmPC.Lync:
                        rdLync.Checked = true;
                        break;
                    //ZD 102004
                    //case ns_MyVRMNet.vrmPC.Vidtel:
                    //    rdVidtel.Checked = true;
                    //    break;
                    default:
                        rdJB.Checked = true;
                        break;
                }
            }
            catch (Exception ex)
            {
                log.Trace("PacketDetails" + ex.Message);
            }
        }
        //FB 2693 End
        //ZD 100221 Starts
        protected string Password(string PW)
        {
            string Encrypted = "";
            XmlDocument docs = null;
            try
            {
                string inxmls = "<System><Cipher>" + PW + "</Cipher></System>";

                string outXML = obj.CallMyVRMServer("GetEncrpytedText", inxmls, Application["MyVRMServer_ConfigPath"].ToString());

                if (outXML.IndexOf("<error>") < 0)
                {
                    docs = new XmlDocument();
                    docs.LoadXml(outXML);
                    XmlNode nde = docs.SelectSingleNode("System/Cipher");
                    if (nde != null)
                        Encrypted = nde.InnerXml;
                }
                else
                {
                    Session["hdModalDiv"] = "none";//ZD 101500
                    errLabel.Text = obj.ShowErrorMessage(outXML);
                }
            }
            catch (Exception ex)
            {
            }
            return Encrypted;
        }
        //ZD 100221 Starts

        //ZD 100834 Starts

        #region ValidateIPISDNAddress
        /// <summary>
        /// ValidateIPISDNAddress
        /// </summary>
        /// <returns></returns>
        protected bool ValidateIPISDNAddress()
        {
            try
            {
                //FB 3012 Starts
                string errMsg = "";
                string patternIP = @"^([1-9]|[1-9][0-9]|1[0-9][0-9]|2[0-4][0-9]|25[0-5])(\.([0-9]|[1-9][0-9]|1[0-9][0-9]|2[0-4][0-9]|25[0-5])){3}$";
                Regex checkIP = new Regex(patternIP);
                string patternISDN = @"^[0-9]+$";
                Regex checkISDN = new Regex(patternISDN);

                foreach (DataGridItem dgi in dgUsers.Items)
                {
                    DropDownList lstAddressType = (DropDownList)dgi.FindControl("lstAddressType");
                    DropDownList lstConnectionType = (DropDownList)dgi.FindControl("lstConnectionType");
                    TextBox txtAddress = (TextBox)dgi.FindControl("txtAddress");

                    //DropDownList lstProtocol = (DropDownList)dgi.FindControl("lstProtocol");
                    string pattern = "";
                    if (((DropDownList)dgi.FindControl("lstConnection")).SelectedValue == "2")
                        pattern = @"^([1-9]|[1-9][0-9]|1[0-9][0-9]|2[0-4][0-9]|25[0-5])(\.([0-9]|[1-9][0-9]|1[0-9][0-9]|2[0-4][0-9]|25[0-5])){3}$";
                    else
                        pattern = @"^[^<>&+]*$";//ZD 103540 - Allowing all characters in Conference,Particpant code & Leader pin except &<>+

                    Regex check = new Regex(pattern);
                    //if (lstConnectionType.SelectedValue.Equals("3"))
                    //{
                    //    if ((lstProtocol.SelectedValue.Equals(ns_MyVRMNet.vrmVideoProtocol.IP) && (lstAddressType.SelectedValue.Equals(ns_MyVRMNet.vrmAddressType.ISDN) || lstConnectionType.SelectedValue.Equals(ns_MyVRMNet.vrmConnectionTypes.Direct)))
                    //        || ((lstProtocol.SelectedValue.Equals(ns_MyVRMNet.vrmVideoProtocol.ISDN) && (!lstAddressType.SelectedValue.Equals(ns_MyVRMNet.vrmAddressType.ISDN) || lstConnectionType.SelectedValue.Equals(ns_MyVRMNet.vrmConnectionTypes.Direct)))))
                    //    {
                    //        errMsg += "<br>" + obj.GetTranslatedText("Invalid Protocol, Connection Type and Address Type selected for user: ") + ((Label)dgi.FindControl("lblUserName")).Text;
                    //    }
                    //}
                    if (lstConnectionType.SelectedValue.Equals("2"))
                    {
                        
                        switch (lstAddressType.SelectedValue)
                        {
                            case ns_MyVRMNet.vrmAddressType.IP:
                                {
                                    if (txtAddress.Text == "")
                                        errMsg += obj.GetTranslatedText("Invalid IP Address for user") + " : " + ((Label)dgi.FindControl("lblUserName")).Text;
                                    else if (!checkIP.IsMatch(txtAddress.Text.Trim(), 0))
                                        errMsg += "<br>" + obj.GetTranslatedText("Invalid IP Address for user") + " : " + ((Label)dgi.FindControl("lblUserName")).Text;
                                    break;
                                }
                            case ns_MyVRMNet.vrmAddressType.ISDN:
                                {
                                    if (txtAddress.Text == "")
                                        errMsg += obj.GetTranslatedText("Invalid ISDN Address for user") + " : " + ((Label)dgi.FindControl("lblUserName")).Text;
                                    else if (!checkISDN.IsMatch(txtAddress.Text.Trim(), 0))
                                    {
                                        errMsg += "<br>" + obj.GetTranslatedText("Invalid ISDN Address for user") + " : " + ((Label)dgi.FindControl("lblUserName")).Text;
                                    }
                                    break;
                                }
                        }
                    }
                    //if (((DropDownList)dgi.FindControl("lstConnection")).SelectedValue == "1") //Code changed for FB 1744
                    //{
                    //    if (txtConfCode.Text.Trim() != "")
                    //    {
                    //        if (!check.IsMatch(txtConfCode.Text.Trim(), 0))
                    //        {
                    //            errMsg += "<br>" + obj.GetTranslatedText("Invalid Conference Code for user") + " : " + ((Label)dgi.FindControl("lblUserName")).Text;
                    //        }
                    //    }
                    //    if (txtleaderPin.Text.Trim() != "")
                    //    {
                    //        if (!check.IsMatch(txtleaderPin.Text.Trim(), 0))
                    //        {
                    //            errMsg += "<br>" + obj.GetTranslatedText("Invalid Leader Pin for user") + ": " + ((Label)dgi.FindControl("lblUserName")).Text;
                    //        }
                    //    }

                    //}
                }

                if (errMsg.Trim().Equals(""))
                    return true;
                else
                {
                    Session["hdModalDiv"] = "none";//ZD 101500
                    errLabel.Visible = true;
                    errLabel.Text = errMsg;
                    return false;
                }
            }
            catch (Exception ex)
            {
                log.Trace(ex.StackTrace);
                return false;
            }
        }

        #endregion

        #region IsValidBridge
        /// <summary>
        /// IsValidBridge
        /// </summary>
        /// <param name="BridgeID"></param>
        /// <returns></returns>
        protected bool IsValidBridge(string BridgeID)
        {
            try
            {
                //ZD 101869 Starts
                if (Convert.ToInt16(BridgeID) <= 0)
                    return true;

                int interfaceType = 0;
                string inXML = "";
                inXML += "<GetBridgeDetails>";
                inXML += obj.OrgXMLElement();
                inXML += "<userID>" + Session["userID"].ToString() + "</userID>";
                inXML += "<BridgeId>" + BridgeID + "</BridgeId>";
                inXML += "</GetBridgeDetails>";

                //string outXML = obj.CallMyVRMServer("GetOldBridge", inXML, Application["MyVRMServer_ConfigPath"].ToString());
                string outXML = obj.CallMyVRMServer("GetBridgeDetails", inXML, Application["MyVRMServer_ConfigPath"].ToString());
                if (outXML.IndexOf("<error>") < 0)
                {
                    XmlDocument xmldoc = new XmlDocument();
                    xmldoc.LoadXml(outXML);

                    if (xmldoc.SelectSingleNode("//GetBridgeDetails/BridgeTypeId") != null)
                        int.TryParse(xmldoc.SelectSingleNode("//GetBridgeDetails/BridgeTypeId").InnerText, out bridgeType); //FB 2839
                   
                    if (xmldoc.SelectSingleNode("//GetBridgeDetails/BridgeTypeId") != null)
                        int.TryParse(xmldoc.SelectSingleNode("//GetBridgeDetails/interfaceType").InnerText, out interfaceType); //FB 2839

                    if (interfaceType.Equals(ns_MyVRMNet.vrmMCUInterfaceType.Polycom))
                        return false;

                    //bridgeType = Int32.Parse(xmldoc.SelectSingleNode("//bridge/bridgeType").InnerText); //FB 2839
                    //XmlNodeList nodes = xmldoc.SelectNodes("//bridge/bridgeTypes/type");

                    //if (xmldoc.SelectSingleNode("//bridge/bridgeDetails/ConferenceServiceID") != null)
                    //    txtConfServiceID = xmldoc.SelectSingleNode("//bridge/bridgeDetails/ConferenceServiceID").InnerText;
                    //Doubt
                    /*if (nodes[bridgeType - 1].SelectSingleNode("interfaceType").InnerText.Equals(ns_MyVRMNet.vrmMCUInterfaceType.Polycom))
                        return true;*/
                    //ZD 101869 End
                }
                return true;
            }
            catch (Exception ex)
            {
                log.Trace(ex.Message + " : " + ex.StackTrace);
                return false;
            }
        }
        #endregion

        #region AddUserEndpoint
        protected DataRow AddUserEndpoint(string UserID, DataTable dt, string connection, string stringUSer) //ZD 101254
        {
            try
            {
                DataRow dr = dt.NewRow();
                dr["ID"] = UserID;
                string userType = "";   //Guest is not display in Audio Setting Tab - end

                if (UserID.IndexOf("new") < 0)
                {
                    string inXML = "<login>" + obj.OrgXMLElement() + "<userID>" + Session["userID"].ToString() + "</userID><user><userID>" + UserID + "</userID></user></login>";
                    string outXML = obj.CallMyVRMServer("GetOldUser", inXML, Application["MyVRMServer_ConfigPath"].ToString());
                    if (outXML.IndexOf("<userName>") < 0)
                    {
                        userType = "G";
                        inXML = "<login>" + obj.OrgXMLElement() + "<userID>" + UserID + "</userID><alphabet></alphabet><pageNo>0</pageNo><sortBy>1</sortBy><EntityType>2</EntityType><userType>G</userType></login>";
                        outXML = obj.CallMyVRMServer("GetUserList", inXML, Application["MyVRMServer_ConfigPath"].ToString());
                    }
                    else
                        userType = "";

                    XmlDocument xmldoc = new XmlDocument();
                    xmldoc.LoadXml(outXML);
                    if (!dt.Columns.Contains("Name"))
                        dt.Columns.Add("Name");

                    if (userType == "G")
                    {
                        dr["ID"] = UserID;
                        dr["Name"] = "<a href='mailto:" + xmldoc.SelectSingleNode("//users/user/email").InnerText.Trim() + "'>" + xmldoc.SelectSingleNode("//users/user/firstName").InnerText.Trim() + " " + xmldoc.SelectSingleNode("//users/user/lastName").InnerText.Trim() + "</a>";  //ZD 103970
                        dr["BridgeID"] = "-1";
                        dr["VideoEquipment"] = "-1";
                        dr["AddressType"] = xmldoc.SelectSingleNode("//users/user/addresstype").InnerText;
                        dr["Address"] = xmldoc.SelectSingleNode("//users/user/address").InnerText;
                        dr["Bandwidth"] = "384";
                        if (stringUSer.Split(ExclamDelim, StringSplitOptions.RemoveEmptyEntries)[8].Trim() != "1") // ZD 101253
                        {
                            dr["Bandwidth"] = "64";
                            dr["AddressType"] = "4";  //ZD 101318
                        }
                        //ZD 101254
                        //dr["connectionType"] = xmldoc.SelectSingleNode("//users/user/connectiontype").InnerText;
                        dr["connectionType"] = "2";
                        dr["Connection"] = "2";//By Default 
                        dr["IsOutside"] = xmldoc.SelectSingleNode("//users/user/outsidenetwork").InnerText;
                        dr["URL"] = "";
                        dr["DefaultProtocol"] = xmldoc.SelectSingleNode("//users/user/protocol").InnerText;

                    }
                    else if (xmldoc.SelectNodes("//oldUser/userName").Count > 0)
                    {
                        dr["Name"] = xmldoc.SelectSingleNode("//oldUser/userName/firstName").InnerText.Trim() + " " + xmldoc.SelectSingleNode("//oldUser/userName/lastName").InnerText.Trim();
                        dr["Name"] = "<a href='mailto:" + xmldoc.SelectSingleNode("//oldUser/userEmail").InnerText.Trim() + "'>" + dr["Name"] + "</a>";
                        dr["BridgeID"] = xmldoc.SelectSingleNode("//oldUser/bridgeID").InnerText.Trim();
                        if (dr["BridgeID"].Equals("") || dr["BridgeID"].Equals("0"))
                            dr["BridgeID"] = "-1";
                        dr["VideoEquipment"] = xmldoc.SelectSingleNode("//oldUser/videoEquipmentID").InnerText.Trim();
                        if (dr["VideoEquipment"].Equals("0"))
                            dr["VideoEquipment"] = "-1";
                        dr["AddressType"] = xmldoc.SelectSingleNode("//oldUser/addressTypeID").InnerText.Trim();
                        if (dr["AddressType"].Equals("0"))
                            dr["AddressType"] = "1";
                        dr["Address"] = xmldoc.SelectSingleNode("//oldUser/IPISDNAddress").InnerText.Trim();
                        dr["Bandwidth"] = xmldoc.SelectSingleNode("//oldUser/lineRateID").InnerText.Trim();
                        if (stringUSer.Split(ExclamDelim, StringSplitOptions.RemoveEmptyEntries)[8].Trim() != "1") // ZD 101253
                        {
                            dr["Bandwidth"] = "64";
                            dr["AddressType"] = "4"; //ZD 101318
                        }
                        //dr["ConnectionType"] = xmldoc.SelectSingleNode("//oldUser/connectionType").InnerText.Trim();
                        //if (dr["ConnectionType"].Equals("-1") || dr["connectionType"].Equals("0"))
                        dr["ConnectionType"] = "2"; // ZD 101254
                        dr["Connection"] = connection;
                        dr["IsOutside"] = xmldoc.SelectSingleNode("//oldUser/isOutside").InnerText.Trim();
                        dr["URL"] = xmldoc.SelectSingleNode("//oldUser/URL").InnerText.Trim();
                        dr["ExchangeID"] = xmldoc.SelectSingleNode("//oldUser/ExchangeID").InnerText.Trim();
                        dr["APIPortNo"] = xmldoc.SelectSingleNode("//oldUser/APIPortNo").InnerText.Trim();
                        dr["ConfCode"] = xmldoc.SelectSingleNode("//oldUser/conferenceCode").InnerText.Trim();
                        dr["LPin"] = xmldoc.SelectSingleNode("//oldUser/leaderPin").InnerText.Trim();
                        if (!dt.Columns.Contains("BridgeProfileID"))
                            dt.Columns.Add("BridgeProfileID");
                        dr["BridgeProfileID"] = "";
                        if (xmldoc.SelectSingleNode("//oldUser/videoProtocol").InnerText.ToUpper().Equals("IP"))
                            dr["DefaultProtocol"] = "1";
                        else
                            dr["DefaultProtocol"] = "2";
                        string epID = xmldoc.SelectSingleNode("//oldUser/EndpointID").InnerText;
                        if (!epID.Trim().Equals("") || !epID.Trim().Equals("0") || !epID.Trim().Equals("-1"))
                        {
                            inXML = "";
                            inXML += "<EndpointDetails>";
                            inXML += obj.OrgXMLElement();
                            inXML += "  <UserID>" + Session["userID"].ToString() + "</UserID>";
                            inXML += "  <EndpointID>" + epID + "</EndpointID>";
                            inXML += "</EndpointDetails>";
                            outXML = obj.CallMyVRMServer("GetEndpointDetails", inXML, Application["MyVRMServer_ConfigPath"].ToString());
                            if (outXML.IndexOf("<error>") < 0)
                            {
                                xmldoc = new XmlDocument();
                                xmldoc.LoadXml(outXML);
                                try
                                {
                                    dr["BridgeID"] = xmldoc.SelectSingleNode("//EndpointDetails/Endpoint/Profiles/Profile/Bridge").InnerText;
                                    dr["VideoEquipment"] = xmldoc.SelectSingleNode("//EndpointDetails/Endpoint/Profiles/Profile/VideoEquipment").InnerText;
                                    if (dr["VideoEquipment"].ToString().Equals("0"))
                                        dr["VideoEquipment"] = "-1";
                                    dr["AddressType"] = xmldoc.SelectSingleNode("//EndpointDetails/Endpoint/Profiles/Profile/AddressType").InnerText;
                                    if (dr["AddressType"].ToString().Equals("0")) 
                                        dr["AddressType"] = "-1";
                                    dr["Address"] = xmldoc.SelectSingleNode("//EndpointDetails/Endpoint/Profiles/Profile/Address").InnerText.Trim();
                                    dr["Bandwidth"] = xmldoc.SelectSingleNode("//EndpointDetails/Endpoint/Profiles/Profile/LineRate").InnerText;
                                    if (stringUSer.Split(ExclamDelim, StringSplitOptions.RemoveEmptyEntries)[8].Trim() != "1") // ZD 101253
                                    {
                                        dr["Bandwidth"] = "64";
                                        dr["AddressType"] = "4"; //ZD 101318
                                    }
                                    //dr["ConnectionType"] = xmldoc.SelectSingleNode("//EndpointDetails/Endpoint/Profiles/Profile/ConnectionType").InnerText;
                                    dr["ConnectionType"] = "2"; // ZD 101254
                                    dr["IsOutside"] = xmldoc.SelectSingleNode("//EndpointDetails/Endpoint/Profiles/Profile/IsOutside").InnerText;
                                    dr["URL"] = xmldoc.SelectSingleNode("//EndpointDetails/Endpoint/Profiles/Profile/URL").InnerText;
                                    dr["ExchangeID"] = xmldoc.SelectSingleNode("//EndpointDetails/Endpoint/Profiles/Profile/ExchangeID").InnerText; //Cisco Fix
                                    dr["DefaultProtocol"] = xmldoc.SelectSingleNode("//EndpointDetails/Endpoint/Profiles/Profile/DefaultProtocol").InnerText;
                                    dr["APIPortNo"] = xmldoc.SelectSingleNode("//EndpointDetails/Endpoint/Profiles/Profile/ApiPortno").InnerText;//API Port...
                                    if (dr["connection"].ToString() == "1")
                                    {
                                        if (enableConferenceCode == "1")
                                            dr["ConfCode"] = xmldoc.SelectSingleNode("//EndpointDetails/Endpoint/Profiles/Profile/conferenceCode").InnerText;
                                        else
                                            dr["ConfCode"] = "";
                                        if (enableLeaderPin == "1")
                                            dr["LPin"] = xmldoc.SelectSingleNode("//EndpointDetails/Endpoint/Profiles/Profile/leaderPin").InnerText;
                                        else
                                            dr["LPin"] = "";
                                    }
                                    else
                                    {
                                        dr["ConfCode"] = "";
                                        dr["LPin"] = "";
                                    }
                                    dr["BridgeProfileID"] = xmldoc.SelectSingleNode("//EndpointDetails/Endpoint/Profiles/Profile/BridgeProfileID").InnerText;//FB 2839
                                    //if (ProfileID.Count > 0)
                                    //{
                                    //    int.TryParse(dr["BridgeID"].ToString(), out ConfBridge);
                                    //    if (ProfileID.ContainsKey(ConfBridge))
                                    //        dr["BridgeProfileID"] = ProfileID[ConfBridge].ToString();
                                    //}
                                }
                                catch (Exception ex1) { log.Trace(ex1.Message); }
                            }
                        }
                    }
                    else
                    {
                        inXML = "<GetConferenceEndpoint>";
                        inXML += obj.OrgXMLElement();
                        inXML += "  <UserID>" + Session["userID"].ToString() + "</UserID>";
                        inXML += "  <ConfID>" + lblConfID.Text + "</ConfID>";
                        inXML += "  <EndpointID>" + UserID + "</EndpointID>";
                        inXML += "  <Type>U</Type>";
                        inXML += "</GetConferenceEndpoint>";
                        outXML = obj.CallMyVRMServer("GetConferenceEndpoint", inXML, Application["MyVRMServer_ConfigPath"].ToString());
                        if (outXML.IndexOf("<error>") < 0)
                        {
                            xmldoc = new XmlDocument();
                            xmldoc.LoadXml(outXML);
                            try
                            {
                                dr["ID"] = UserID;
                                dr["Name"] = xmldoc.SelectSingleNode("//GetConferenceEndpoint/Endpoint/Name").InnerText;
                                dr["BridgeID"] = xmldoc.SelectSingleNode("//GetConferenceEndpoint/Endpoint/BridgeID").InnerText;
                                dr["VideoEquipment"] = xmldoc.SelectSingleNode("//GetConferenceEndpoint/Endpoint/VideoEquipment").InnerText;
                                if (dr["VideoEquipment"].ToString().Equals("0"))
                                    dr["VideoEquipment"] = "-1";
                                dr["AddressType"] = xmldoc.SelectSingleNode("//GetConferenceEndpoint/Endpoint/AddressType").InnerText;
                                if (dr["AddressType"].ToString().Equals("0"))
                                    dr["AddressType"] = "-1";
                                dr["Address"] = xmldoc.SelectSingleNode("//GetConferenceEndpoint/Endpoint/Address").InnerText.Trim();
                                dr["Bandwidth"] = xmldoc.SelectSingleNode("//GetConferenceEndpoint/Endpoint/Bandwidth").InnerText;
                                dr["connectionType"] = xmldoc.SelectSingleNode("//GetConferenceEndpoint/Endpoint/connectionType").InnerText;
                                dr["Connection"] = xmldoc.SelectSingleNode("//GetConferenceEndpoint/Endpoint/Connection").InnerText;
                                dr["IsOutside"] = xmldoc.SelectSingleNode("//GetConferenceEndpoint/Endpoint/IsOutside").InnerText;
                                dr["URL"] = xmldoc.SelectSingleNode("//GetConferenceEndpoint/Endpoint/URL").InnerText;
                                dr["ExchangeID"] = xmldoc.SelectSingleNode("//GetConferenceEndpoint/Endpoint/ExchangeID").InnerText;
                                dr["DefaultProtocol"] = xmldoc.SelectSingleNode("//GetConferenceEndpoint/Endpoint/DefaultProtocol").InnerText;
                                dr["APIPortNo"] = xmldoc.SelectSingleNode("//EndpointDetails/Endpoint/Profiles/Profile/APIPortNo").InnerText;
                            }
                            catch (Exception ex)
                            {
                                dr["Name"] = "";
                                log.Trace(ex.Message);
                            }
                        }
                        else
                        {
                            dr["ID"] = UserID;
                            dr["Name"] = "User not found.";
                            dr["BridgeID"] = "-1";
                            dr["VideoEquipment"] = "-1";
                            dr["AddressType"] = "-1";
                            dr["Address"] = "";
                            dr["VideoEquipment"] = "-1";
                            dr["Bandwidth"] = "384";
                            dr["connectionType"] = "2";//ZD 10253
                            dr["Connection"] = "2";
                            dr["IsOutside"] = "0";
                            dr["URL"] = "";
                            dr["ExchangeID"] = "";
                            dr["DefaultProtocol"] = "1";
                            dr["APIPortNo"] = "";
                        }
                    }
                }
                else
                {
                    if (stringUSer.Trim() != "")//ZD 101254
                    {
                        dr["ID"] = UserID;
                        dr["Name"] = "<a href='mailto:" + stringUSer.Split(ExclamDelim, StringSplitOptions.RemoveEmptyEntries)[3] + "'>" + stringUSer.Split(ExclamDelim, StringSplitOptions.RemoveEmptyEntries)[1] + " " + stringUSer.Split(ExclamDelim, StringSplitOptions.RemoveEmptyEntries)[2] + "</a>";  //ZD 101254
                        dr["Bandwidth"] = "384";
                        dr["AddressType"] = "-1"; //ZD 101318
                        if (stringUSer.Split(ExclamDelim, StringSplitOptions.RemoveEmptyEntries)[8].Trim() != "1") // ZD 101253
                        {
                            dr["Bandwidth"] = "64";
                            dr["AddressType"] = "4"; //ZD 101318
                        }
                        dr["BridgeID"] = "-1";
                        dr["VideoEquipment"] = "-1";                       
                        dr["Address"] = "";
                        dr["VideoEquipment"] = "-1";
                        dr["connectionType"] = "2";
                        dr["Connection"] = "2";
                        dr["IsOutside"] = "0";
                        dr["URL"] = "";
                        dr["ExchangeID"] = "";
                        dr["DefaultProtocol"] = "1";
                        dr["APIPortNo"] = "23";
                    }
                }
                return dr;
            }
            catch (Exception ex)
            {
                log.Trace("AddUserEndpoint: " + ex.StackTrace + " : " + ex.Message);
                return null;
            }
        }
        #endregion

        #region DisplayUserEndpoints

        protected void DisplayUserEndpoints()
        {
            try
            {
                DataTable dt = new DataTable();
                dt = CreateDataTable(dt);
                //enableAV == "0" //ZD 100522
                if ((lstVMR.SelectedIndex > 0 && lstVMR.SelectedValue != "2") || chkPCConf.Checked || lstConferenceType.SelectedValue.Equals(ns_MyVRMNet.vrmConfType.RoomOnly)
                   || lstConferenceType.SelectedValue.Equals(ns_MyVRMNet.vrmConfType.HotDesking) || (isCloudEnabled == 1 && chkCloudConferencing.Checked))
                {
                    lblNoUsers.Visible = true;
                    dgUsers.DataSource = null;
                    dgUsers.DataBind();
                    dgUsers.Visible = false;
                    ViewState.Remove("userGrid");
                }
                else
                {
                    if (!lblConfID.Text.ToLower().Equals("new"))
                    {
                        if(hasVisited.Value != "1") //ALLDEV-814
                            GetAdvancedAVSettings();
                        XmlTextReader xtr;
                        DataSet dsMain = new DataSet();
                        XmlDocument xd = new XmlDocument();
                        if (Session["Confbridge"] != null && !string.IsNullOrEmpty(Session["Confbridge"].ToString()))
                        {
                            xd.LoadXml(Session["Confbridge"].ToString());
                            XmlNodeList nodes = xd.SelectNodes("//GetAdvancedAVSettings/Endpoints/Endpoint[Type='U']");
                            foreach (XmlNode node in nodes)
                            {
                                xtr = new XmlTextReader(node.OuterXml, XmlNodeType.Element, new XmlParserContext(null, null, null, XmlSpace.None));
                                dsMain.ReadXml(xtr, XmlReadMode.InferSchema);
                            }
                        }
                        if (dsMain.Tables.Count > 0)
                        {
                            dt = dsMain.Tables[0];
                            string filterExp = "AudioBridgeParty='1'";
                            DataRow[] drArr = dt.Select(filterExp);
                            foreach (DataRow dr in drArr)
                            {
                                dt.Rows.Remove(dr);
                            }
                        }
                        if (!dt.Columns.Contains("ConfCode"))
                            dt.Columns.Add("ConfCode");
                        if (!dt.Columns.Contains("LPin"))
                            dt.Columns.Add("LPin");
                        if (!dt.Columns.Contains("isTelePresence"))
                            dt.Columns.Add("isTelePresence");
                        if (!dt.Columns.Contains("BridgeProfileID"))
                            dt.Columns.Add("BridgeProfileID");
                        if (!dt.Columns.Contains("IsTestEquipment"))
                            dt.Columns.Add("IsTestEquipment");
                        foreach (DataRow dr in dt.Rows)
                        {
                            if (dr["VideoEquipment"].ToString().Equals("0"))
                                dr["VideoEquipment"] = "-1";
                            if (dr["AddressType"].ToString().Equals("0"))
                                dr["AddressType"] = "-1";

                            if (dr["Name"] != null)
                            {
                                if (dr["Name"].ToString().IndexOf("++") >= 0)
                                {
                                    dr["Name"] = dr["Name"].ToString().Replace("++", " "); ;
                                }
                            }
                        }
                        //dgUsers.DataSource = dt;//ZD 101254
                        //dgUsers.DataBind();
                    }
                    //PreserveUsersState(); // Preload the datagrid with users input and rest works the same way it always does
                    UsersChanged(ref dt);
                    if (dt.Rows.Count.Equals(0))
                    {
                        lblNoUsers.Visible = true;
                        dgUsers.DataSource = null;
                        dgUsers.DataBind();
                        dgUsers.Visible = false;
                        ViewState.Remove("userGrid");
                    }
                    else
                    {
                        dgUsers.Visible = true;
                        lblNoUsers.Visible = false;
                        int cnt = 0;
                        foreach (DataRow dr in dt.Rows)
                        {
                            if (dr["BridgeId"].ToString().Equals("0"))
                                dr["BridgeId"] = "-1";

                            if (dr["Connect2"].ToString() == "")
                            {
                                if (cnt == 0)
                                    dr["Connect2"] = "1";
                                else
                                    dr["Connect2"] = "0";
                            }
                            cnt++;
                        }
                        dgUsers.DataSource = dt;
                        dgUsers.DataBind();

                        ViewState.Remove("userGrid");
                        ViewState.Add("userGrid", dt);

                        hdnextusrcnt.Value = "";//ALLBUGS-26 
                        hdnextusrcnt.Value = dgUsers.Items.Count.ToString();//ALLBUGS-26
                        
                        //ZD 101561 Starts
                        if (lstVMR.SelectedValue == "2" || lstConferenceType.SelectedValue.Equals(ns_MyVRMNet.vrmConfType.P2P))
                        {
                            foreach (DataGridItem dgi in dgUsers.Items)
                            {
                                if (lstVMR.SelectedValue == "2")
                                {
                                    HtmlTable tbMCUandConn = (HtmlTable)dgi.FindControl("tbMCUandConn");
                                    if (tbMCUandConn != null)
                                        tbMCUandConn.Attributes.Add("style", "display:none;");
                                    ((DropDownList)dgi.FindControl("lstBridges")).Visible = false;
                                    ((DropDownList)dgi.FindControl("lstTelnetUsers")).Enabled = false;
                                    ((DropDownList)dgi.FindControl("lstTelnetUsers")).Visible = false;
                                    
                                }
                                if (lstConferenceType.SelectedValue.Equals(ns_MyVRMNet.vrmConfType.P2P))
                                    ((DropDownList)dgi.FindControl("lstBridges")).Enabled = false;
                            }
                        }
                        //ZD 101561 End
                    }

                    hdnAudioId.Value = ""; // ZD 101253
                }

                // ZD 999999
                if (lstConferenceType.SelectedValue.Equals("4"))
                {
                    DataGrid dgs = (DataGrid)Page.FindControl("dgProfiles");
                    foreach (DataGridItem d in dgs.Items)
                    {
                        ((Literal)d.FindControl("LblGstEP")).Text = obj.GetTranslatedText("Select Caller/Callee");
                        d.FindControl("lstGuestBridges").Visible = false;
                        d.FindControl("lstTelnetUsers").Visible = true;
                        ((DropDownList)d.FindControl("lstTelnetUsers")).ClearSelection();
                        ((DropDownList)d.FindControl("lstTelnetUsers")).SelectedValue = "0";
                        ((DropDownList)d.FindControl("lstTelnetUsers")).Enabled = false;
                    }
                    btnGuestAddProfile.Disabled = true;
                    btnGuestAddProfile.Attributes.Add("Class", "btndisable");


                    DataGrid dgiuser = (DataGrid)Page.FindControl("dgUsers");
                    foreach (DataGridItem d in dgiuser.Items)
                    {
                        ((Label)d.FindControl("LblEPUsers")).Text = obj.GetTranslatedText("Select Caller/Callee");
                        ((DropDownList)d.FindControl("lstBridges")).Visible = false;
                        DropDownList lstTelnet = new DropDownList();
                        lstTelnet = d.FindControl("lstTelnetUsers") as DropDownList;
                        lstTelnet.Visible = true;
                        lstTelnet.Enabled = true;
                    }
                }
                else
                {
                    DataGrid dgs = (DataGrid)Page.FindControl("dgProfiles");
                    foreach (DataGridItem d in dgs.Items)
                    {
                        ((Literal)d.FindControl("LblGstEP")).Text = obj.GetTranslatedText("Assigned to MCU");
                        d.FindControl("lstGuestBridges").Visible = true;
                        d.FindControl("lstTelnetUsers").Visible = false;
                    }
                    if (dgs.Items.Count == 5)
                    {
                        btnGuestAddProfile.Disabled = true;
                        btnGuestAddProfile.Attributes.Add("Class", "btndisable");
                    }
                    else
                    {
                        btnGuestAddProfile.Disabled = false;
                        btnGuestAddProfile.Attributes.Add("Class", "altMedium0BlueButtonFormat");
                    }
                }
                updtGuestLocation.Update();
                // ZD 999999
            }
            catch (Exception ex)
            {
                log.Trace("Display: " + ex.StackTrace + " : " + ex.Message);
                errLabel.Visible = true;
            }
        }

        #endregion

        #region UsersChanged

        protected bool UsersChanged(ref DataTable dt)
        {
            try
            {
                bool flagUser = true;
                string connectn = "";
                log.Trace("txtPartysInfo.Text = " + txtPartysInfo.Text);
                for (int i = 0; i < txtPartysInfo.Text.Split(pipeDelim, StringSplitOptions.RemoveEmptyEntries).Length; i++)
                {
                    if (txtPartysInfo.Text.Split(pipeDelim, StringSplitOptions.RemoveEmptyEntries)[i].Split(ExclamDelim, StringSplitOptions.RemoveEmptyEntries)[4].Trim().Equals("1"))
                    {
                        flagUser = false;
                        foreach (DataRow dr in dt.Rows)
                        {
                            if (dr["ID"].ToString().Equals(txtPartysInfo.Text.Split(pipeDelim, StringSplitOptions.RemoveEmptyEntries)[i].Split(ExclamDelim, StringSplitOptions.RemoveEmptyEntries)[0].Trim()) && (txtPartysInfo.Text.Split(pipeDelim, StringSplitOptions.RemoveEmptyEntries)[i].Split(ExclamDelim, StringSplitOptions.RemoveEmptyEntries)[4].Trim().Equals("1")))
                                flagUser = true;
                        }

                        if (flagUser.Equals(false))
                        {
                            //ZD 101254 Starts
                            string CheckuserId = txtPartysInfo.Text.Split(pipeDelim, StringSplitOptions.RemoveEmptyEntries)[i].Split(ExclamDelim, StringSplitOptions.None)[0].Trim();
                            if (CheckuserId == "")
                                continue;
                            //ZD 101254 End

                            string UserID = txtPartysInfo.Text.Split(pipeDelim, StringSplitOptions.RemoveEmptyEntries)[i].Split(ExclamDelim, StringSplitOptions.RemoveEmptyEntries)[0].Trim();
                            if (UserID.Equals("new"))
                                UserID += "_" + txtPartysInfo.Text.Split(pipeDelim, StringSplitOptions.RemoveEmptyEntries)[i].Split(ExclamDelim, StringSplitOptions.RemoveEmptyEntries)[3].Trim(); ; //ZD 101254

                            string videocnn = txtPartysInfo.Text.Split(pipeDelim, StringSplitOptions.RemoveEmptyEntries)[i].Split(ExclamDelim, StringSplitOptions.RemoveEmptyEntries)[8].Trim();
                            string audiocnn = txtPartysInfo.Text.Split(pipeDelim, StringSplitOptions.RemoveEmptyEntries)[i].Split(ExclamDelim, StringSplitOptions.RemoveEmptyEntries)[9].Trim();

                            connectn = "1";
                            if (videocnn == "1")
                            {
                                connectn = "2"; //AudioVideo
                            }
                            else
                            {
                                connectn = "1"; //Audio only                          
                            }

                            dt.Rows.Add(AddUserEndpoint(UserID, dt, connectn, txtPartysInfo.Text.Split(pipeDelim, StringSplitOptions.RemoveEmptyEntries)[i])); //ZD 101254
                        }
                    }
                }
                for (int r = 0; r < dt.Rows.Count; r++)
                {
                    DataRow dr = dt.Rows[r];
                    flagUser = false;
                    for (int i = 0; i < txtPartysInfo.Text.Split(pipeDelim, StringSplitOptions.RemoveEmptyEntries).Length; i++)
                    {
                        string UserID = txtPartysInfo.Text.Split(pipeDelim, StringSplitOptions.RemoveEmptyEntries)[i].Split(ExclamDelim, StringSplitOptions.RemoveEmptyEntries)[0].Trim();
                        if (txtPartysInfo.Text.Split(pipeDelim, StringSplitOptions.RemoveEmptyEntries)[i].Split(ExclamDelim, StringSplitOptions.RemoveEmptyEntries)[0].Trim().Equals("new"))
                            UserID += "_" + txtPartysInfo.Text.Split(pipeDelim, StringSplitOptions.RemoveEmptyEntries)[i].Split(ExclamDelim, StringSplitOptions.RemoveEmptyEntries)[3].Trim(); ; //ZD 101254

                        string videocnn = txtPartysInfo.Text.Split(pipeDelim, StringSplitOptions.RemoveEmptyEntries)[i].Split(ExclamDelim, StringSplitOptions.RemoveEmptyEntries)[8].Trim();
                        string audiocnn = txtPartysInfo.Text.Split(pipeDelim, StringSplitOptions.RemoveEmptyEntries)[i].Split(ExclamDelim, StringSplitOptions.RemoveEmptyEntries)[9].Trim();
                        
                        connectn = "1";
                        if (videocnn == "1")
                        {
                            connectn = "2"; //AudioVideo
                        }
                        else
                        {
                            connectn = "1"; //Audio only
                        }

                        if (dr["ID"].ToString().Equals(UserID) && (txtPartysInfo.Text.Split(pipeDelim, StringSplitOptions.RemoveEmptyEntries)[i].Split(ExclamDelim, StringSplitOptions.RemoveEmptyEntries)[4].Trim().Equals("1")))
                        {
                            dr["Connection"] = connectn;
                            flagUser = true;
                        }
                    }
                    if (flagUser.Equals(false))
                    {
                        dt.Rows.RemoveAt(r);
                        r--;
                    }
                    else
                    {
                        foreach (DataGridItem dgi in dgUsers.Items)
                        {
                            if (dgi.Cells[0].Text.Equals(dr["ID"].ToString()))
                            {
                                dr["BridgeID"] = ((DropDownList)dgi.FindControl("lstBridges")).SelectedValue;
                                dr["Bandwidth"] = ((DropDownList)dgi.FindControl("lstLineRate")).SelectedValue;
                                dr["AddressType"] = ((DropDownList)dgi.FindControl("lstAddressType")).SelectedValue; //ZD 101318
                                // ZD 101253 Starts
                                if (dr["Connection"].ToString() == "1" && hdnAudioId.Value.Trim() != "" && hdnAudioId.Value.Trim() != "0")
                                {
                                    if (hdnAudioId.Value.Trim().Equals(dgi.Cells[0].Text) || dgi.Cells[0].Text.IndexOf(hdnAudioId.Value.Trim()) >= 0)
                                    {
                                        dr["Bandwidth"] = "64";
                                        dr["AddressType"] = "4"; //ZD 101318
                                    }
                                }
                                // ZD 101253 End
                                dr["connectionType"] = ((DropDownList)dgi.FindControl("lstConnectionType")).SelectedValue;                                
                                dr["Address"] = ((TextBox)dgi.FindControl("txtAddress")).Text.Trim();
                                dr["Connect2"] = ((DropDownList)dgi.FindControl("lstTelnetUsers")).SelectedValue;
                                //if (dr["Connection"].ToString() == "1" && lstConferenceType.SelectedValue == "2")
                                //{
                                //    dr["Address"] = ((TextBox)dgi.FindControl("txtAddress")).Text.Trim() + "D" + dr["ConfCode"].ToString().Trim() + "+" + dr["LPin"].ToString().Trim();
                                //}
                                //else
                                dr["Address"] = ((TextBox)dgi.FindControl("txtAddress")).Text.Trim();
                                dr["VideoEquipment"] = ((DropDownList)dgi.FindControl("lstVideoEquipment")).SelectedValue;//ZD 100815
                                //dr["BridgeProfileID"] = ((Label)dgi.FindControl("lstMCUProfSelected")).Text.Trim();
                            }
                        }
                    }
                }
                return flagUser;
            }
            catch (Exception ex)
            {
                log.Trace(ex.StackTrace + " : " + ex.Message);
                return false;
            }
        }

        #endregion

        #region InitializeUsers
        protected void InitializeUsers(Object sender, DataGridItemEventArgs e)
        {
            try
            {
                if (e.Item.ItemType.Equals(ListItemType.Item) || e.Item.ItemType.Equals(ListItemType.AlternatingItem))
                {
                    DataRowView dr = e.Item.DataItem as DataRowView;
                    obj.BindBridges((DropDownList)e.Item.FindControl("lstBridges"));
                    obj.BindLineRate((DropDownList)e.Item.FindControl("lstLineRate"));
                    obj.BindAddressType((DropDownList)e.Item.FindControl("lstAddressType"));
                    obj.BindMediaTypes((DropDownList)e.Item.FindControl("lstConnection"));
                    obj.BindDialingOptions((DropDownList)e.Item.FindControl("lstConnectionType"));
                    obj.BindVideoEquipment((DropDownList)e.Item.FindControl("lstVideoEquipment"));//ZD 100815

                    if (lstConferenceType.SelectedValue.Equals("4"))
                    {
                        ((DropDownList)e.Item.FindControl("lstBridges")).Visible = false;
                        DropDownList lstTelnet = new DropDownList();
                        lstTelnet = e.Item.FindControl("lstTelnetUsers") as DropDownList;
                        lstTelnet.Visible = true;

                        if (dr != null)
                        {
                            if (dr["Connect2"].ToString() == "-1")
                                dr["Connect2"] = "1";

                            lstTelnet.SelectedValue = dr["Connect2"].ToString();
                        }
                        ((Label)e.Item.FindControl("LblEPUsers")).Text = obj.GetTranslatedText("Select Caller/Callee");
                    }
                    else
                    {
                        DropDownList lstMCU = new DropDownList();
                        lstMCU = e.Item.FindControl("lstBridges") as DropDownList;
                        lstMCU.Visible = true;

                        if (dr != null)
                        {
                            if (dr["BridgeID"].ToString() != "" && lstMCU.SelectedValue == "-1")
                                lstMCU.SelectedValue = dr["BridgeID"].ToString();
                        }
                        ((DropDownList)e.Item.FindControl("lstTelnetUsers")).Visible = false;
                        ((Label)e.Item.FindControl("LblEPUsers")).Text = obj.GetTranslatedText("MCU");
                    }
                }
            }
            catch (Exception ex)
            {
                errLabel.Visible = true;
                log.Trace(ex.StackTrace + " : " + ex.Message);
            }
        }
        #endregion

        #region btnClickSample 
        /// <summary>
        /// btnClickSample
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnClickSample(object sender, EventArgs e)
        {
            DisplayUserEndpoints();
        }
        #endregion

        //ZD 100834 End

        //ZD 100619 Starts
        #region LoadGuestEndpointProfile
        /// <summary>
        /// LoadGuestEndpointProfile
        /// </summary>
        /// <param name="changedHistoryNodes"></param>
        /// <returns></returns>
        private bool LoadGuestEndpointProfile(int row)
        {
            try
            {
                lstGuestConnectionType.SelectedValue = "2";

                DataTable dtable = new DataTable();
                DataRow dr;
                DropDownList lnRate;
                if (row == 0)
                {
                    CreateProfileDtColumnNames();
                    onflyGridProfiles = obj.LoadDataTable(null, colNames);
                    dtable = obj.LoadDataTable(null, colNames);
                }
                else if (row == -1)
                {
                    row = 0;
                    CreateProfileDtColumnNames();
                    dtable = obj.LoadDataTable(null, colNames);
                }
                dr = dtable.NewRow();

                dr["RowUID"] = row.ToString();
                dr["ProfileCnt"] = "0";
                dr["Address"] = "";
                dr["AddressType"] = "1";
                dr["ConnectionType"] = "2";
                dr["MaxLineRate"] = OrgLineRate.ToString();
                dr["DefaultProfile"] = "1";
                dr["BridgeID"] = "-1";
                dr["Deleted"] = "0";
                dtable.Rows.Add(dr);

                dgProfiles.DataSource = dtable;
                dgProfiles.DataBind();
                for (int i = 0; i < dgProfiles.Items.Count; i++)
                {
                    lnRate = (DropDownList)(dgProfiles.Items[i].FindControl("lstGuestLineRate"));
                    ((DropDownList)dgProfiles.Items[i].FindControl("lstGuestConnectionType1")).SelectedValue = "2";
                    ((DropDownList)dgProfiles.Items[i].FindControl("lstGuestConnectionType1")).Enabled = false;
                    ((CheckBox)dgProfiles.Items[i].FindControl("chkDelete")).Enabled = false;
                }
                btnGuestAddProfile.Disabled = false;
                btnGuestAddProfile.Attributes.Add("Class", "altMedium0BlueButtonFormat");

                // ZD 999999
                if (lstConferenceType.SelectedValue.Equals("4"))
                {
                    DataGrid dgs = (DataGrid)Page.FindControl("dgProfiles");
                    foreach (DataGridItem d in dgs.Items)
                    {
                        ((Literal)d.FindControl("LblGstEP")).Text = obj.GetTranslatedText("Select Caller/Callee");
                        d.FindControl("lstGuestBridges").Visible = false;
                        d.FindControl("lstTelnetUsers").Visible = true;
                        ((DropDownList)d.FindControl("lstTelnetUsers")).ClearSelection();
                        ((DropDownList)d.FindControl("lstTelnetUsers")).SelectedValue = "0";
                        ((DropDownList)d.FindControl("lstTelnetUsers")).Enabled = false;
                    }
                    btnGuestAddProfile.Disabled = true;
                    btnGuestAddProfile.Attributes.Add("Class", "btndisable");


                    DataGrid dgiuser = (DataGrid)Page.FindControl("dgUsers");
                    foreach (DataGridItem d in dgiuser.Items)
                    {
                        ((Label)d.FindControl("LblEPUsers")).Text = obj.GetTranslatedText("Select Caller/Callee");
                        ((DropDownList)d.FindControl("lstBridges")).Visible = false;
                        DropDownList lstTelnet = new DropDownList();
                        lstTelnet = d.FindControl("lstTelnetUsers") as DropDownList;
                        lstTelnet.Visible = true;
                        lstTelnet.Enabled = true;
                    }
                }
                else
                {
                    DataGrid dgs = (DataGrid)Page.FindControl("dgProfiles");
                    foreach (DataGridItem d in dgs.Items)
                    {
                        ((Literal)d.FindControl("LblGstEP")).Text = obj.GetTranslatedText("Assigned to MCU");
                        d.FindControl("lstGuestBridges").Visible = true;
                        d.FindControl("lstTelnetUsers").Visible = false;
                    }
                    if (dgs.Items.Count == 5)
                    {
                        btnGuestAddProfile.Disabled = true;
                        btnGuestAddProfile.Attributes.Add("Class", "btndisable");
                    }
                    else
                    {
                        btnGuestAddProfile.Disabled = false;
                        btnGuestAddProfile.Attributes.Add("Class", "altMedium0BlueButtonFormat");
                    }
                }
                updtGuestLocation.Update();
                // ZD 999999
            }
            catch (Exception ex)
            {
                log.Trace("LoadGuestEndpointProfile" + ex.Message);
                return false;
            }
            return true;
        }
        #endregion

        # region fnGuestAddProfile
        /// <summary>
        /// fnGuestAddProfile
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void fnGuestAddProfile(Object sender, EventArgs e)
        {
            try
            {
                int isDeft = 0;
                string RowUID = "0";
                if (dgProfiles.Items.Count < 5)
                {

                    DataTable dt = new DataTable();
                    DataGridItem dgi;
                    DataRow dr;

                    for (int i = 0; i < dgProfiles.Items.Count; i++)
                    {
                        if (i == 0)
                        {
                            CreateProfileDtColumnNames();
                            dt = obj.LoadDataTable(null, colNames);
                        }
                        isDeft = 0;
                        dgi = dgProfiles.Items[i];
                        dr = dt.NewRow();
                        dr["RowUID"] = ((TextBox)dgi.FindControl("txtRowID")).Text;
                        dr["ProfileCnt"] = i.ToString();
                        RowUID = ((TextBox)dgi.FindControl("txtRowID")).Text;
                        dr["Address"] = ((TextBox)dgi.FindControl("txtGuestAddress")).Text;
                        dr["AddressType"] = ((DropDownList)dgi.FindControl("lstGuestAddressType")).SelectedValue;
                        dr["ConnectionType"] = ((DropDownList)dgi.FindControl("lstGuestConnectionType1")).SelectedValue;
                        dr["MaxLineRate"] = ((DropDownList)dgi.FindControl("lstGuestLineRate")).SelectedValue;
                        if (((RadioButton)dgi.FindControl("rdDefault")).Checked)
                            isDeft = 1;
                        dr["DefaultProfile"] = isDeft;
                        dr["BridgeID"] = ((DropDownList)dgi.FindControl("lstGuestBridges")).SelectedValue;
                        isDeft = 0;
                        if (((CheckBox)dgi.FindControl("chkDelete")).Checked)
                            isDeft = 1;
                        dr["Deleted"] = isDeft;
                        dt.Rows.Add(dr);

                        if (i == 3)
                        {
                            btnGuestAddProfile.Disabled = true;
                            btnGuestAddProfile.Attributes.Add("Class", "btndisable"); 
                        }
                    }
                    dr = dt.NewRow();
                    if (dgProfiles.Items.Count == 0)
                        RowUID = hdnROWID.Value;
                    dr["RowUID"] = RowUID;
                    dr["ProfileCnt"] = (dgProfiles.Items.Count + 1).ToString();
                    dr["Address"] = "";
                    dr["AddressType"] = "1";
                    dr["ConnectionType"] = "2";
                    dr["MaxLineRate"] = OrgLineRate.ToString();
                    dr["DefaultProfile"] = "0";
                    dr["BridgeID"] = "-1";
                    dr["Deleted"] = "0";
                    dt.Rows.Add(dr);
                    dgProfiles.DataSource = dt;
                    dgProfiles.DataBind();
                    dgProfiles.Visible = true;


                    for (int i = 0; i < dgProfiles.Items.Count; i++)
                    {
                        isDeft = 0;
                        if (((RadioButton)dgProfiles.Items[i].FindControl("rdDefault")).Checked)
                            isDeft = 1;
                        ((CheckBox)dgProfiles.Items[i].FindControl("chkDelete")).Enabled = true;
                        ((DropDownList)dgProfiles.Items[i].FindControl("lstGuestConnectionType1")).Enabled = true;
                        if (isDeft == 1)
                        {
                            ((DropDownList)dgProfiles.Items[i].FindControl("lstGuestConnectionType1")).Enabled = false;
                            ((CheckBox)dgProfiles.Items[i].FindControl("chkDelete")).Enabled = false;
                        }

                    }

                }

            }
            catch (Exception ex)
            {
                log.Trace(ex.StackTrace + " : " + ex.Message);
            }
        }
        #endregion

        protected void LoadProfiles(XmlNodeList nodes, String defaultProfile, int rowID)
        {
            try
            {
                int cnt = 0;
                XmlTextReader xtr;
                DataSet ds = new DataSet();
                foreach (XmlNode node in nodes)
                {
                    xtr = new XmlTextReader(node.OuterXml, XmlNodeType.Element, new XmlParserContext(null, null, null, XmlSpace.None));
                    ds.ReadXml(xtr, XmlReadMode.InferSchema);
                }

                if (onflyGridProfiles == null)
                {
                    CreateProfileDtColumnNames();
                    onflyGridProfiles = obj.LoadDataTable(null, colNames);
                }
                DataRow onflyRow;
                DataTable dt = new DataTable();
                if (ds.Tables.Count > 0)
                {
                    for (int i = 0; i < ds.Tables.Count; i++)
                    {
                        cnt = 0;
                        dt = ds.Tables[i];

                        if (!dt.Columns.Contains("RowUID"))
                            dt.Columns.Add("RowUID");

                        if (!dt.Columns.Contains("DefaultProfile"))
                            dt.Columns.Add("DefaultProfile");

                        if (!dt.Columns.Contains("ProfileCnt"))
                            dt.Columns.Add("ProfileCnt");

                        foreach (DataRow dr in dt.Rows)
                        {
                            onflyRow = onflyGridProfiles.NewRow();

                            dr["RowUID"] = rowID.ToString();
                            dr["ProfileCnt"] = cnt.ToString(); ;

                            if (dr["ProfileID"].ToString().Equals(defaultProfile))
                                dr["DefaultProfile"] = "1";
                            else
                                dr["DefaultProfile"] = "0";
                            if (dr["ConnectionType"].ToString().Equals("0"))
                                dr["ConnectionType"] = "2";

                            onflyRow["RowUID"] = dr["RowUID"];
                            onflyRow["Address"] = dr["Address"];
                            onflyRow["AddressType"] = dr["AddressType"];
                            onflyRow["ConnectionType"] = dr["ConnectionType"];
                            onflyRow["MaxLineRate"] = dr["MaxLineRate"];
                            onflyRow["DefaultProfile"] = dr["DefaultProfile"];
                            onflyRow["BridgeID"] = dr["BridgeID"];
                            onflyRow["Deleted"] = dr["Deleted"];
                            onflyRow["ProfileCnt"] = dr["ProfileCnt"];

                            cnt++;
                            onflyGridProfiles.Rows.Add(onflyRow);
                        }
                    }
                    LoadGuestEndpointProfile(-1);
                }
                else
                    LoadGuestEndpointProfile(rowID);



            }
            catch (Exception ex)
            {
                log.Trace("LoadProfiles: " + ex.StackTrace + " : " + ex.Message);
            }

        }

        protected void AddNewRow(DataRow dr)
        {
            try
            {
                dr["RowUID"] = "";
                dr["Address"] = "";
                dr["AddressType"] = "1";
                dr["ConnectionType"] = "2";
                dr["MaxLineRate"] = OrgLineRate.ToString();
                dr["DefaultProfile"] = "0";
                dr["BridgeID"] = "-1";
                dr["Deleted"] = "0";
            }
            catch (Exception ex)
            {
                log.Trace("AddNewRow: " + ex.StackTrace + " : " + ex.Message);
            }
        }

        #region GuestDftConnctType
        protected void GuestDftConnctType(Object sender, EventArgs e)
        {
            try
            {
                int isDeft = 0;
                if (dgProfiles.Items.Count <= 5)
                {
                    for (int i = 0; i < dgProfiles.Items.Count; i++)
                    {
                        isDeft = 0;
                        if (((RadioButton)dgProfiles.Items[i].FindControl("rdDefault")).Checked)
                            isDeft = 1;
                        ((DropDownList)dgProfiles.Items[i].FindControl("lstGuestConnectionType1")).Enabled = true;
                        //((RequiredFieldValidator)dgProfiles.Items[i].FindControl("reqGuestAddress")).Enabled = true;
                        ((CheckBox)dgProfiles.Items[i].FindControl("chkDelete")).Enabled = true;

                        if (isDeft == 1)
                        {
                            ((CheckBox)dgProfiles.Items[i].FindControl("chkDelete")).Enabled = false;

                            ((DropDownList)dgProfiles.Items[i].FindControl("lstGuestConnectionType1")).Enabled = false;
                            if (lstGuestConnectionType.SelectedValue.Equals("2"))
                                ((DropDownList)dgProfiles.Items[i].FindControl("lstGuestConnectionType1")).SelectedValue = "2";
                            else if (lstGuestConnectionType.SelectedValue.Equals("1"))
                            {
                                ((DropDownList)dgProfiles.Items[i].FindControl("lstGuestConnectionType1")).SelectedValue = "1";
                                //((RequiredFieldValidator)dgProfiles.Items[i].FindControl("reqGuestAddress")).Enabled = false;
                                //int.TryParse(((DropDownList)dgProfiles.Items[i].FindControl("lstGuestConnectionType1")).SelectedValue, out MCU);
                                //if (MCU > 0)
                                //{
                                //    IsValidBridge(MCU.ToString());
                                //    if ((bridgeType == ns_MyVRMNet.MCUType.PolycomRMX2000) || (bridgeType == ns_MyVRMNet.MCUType.PolycomRPRM) || (bridgeType == ns_MyVRMNet.MCUType.PolycomMGC100) || (bridgeType == ns_MyVRMNet.MCUType.PolycomMGC25) || (bridgeType == ns_MyVRMNet.MCUType.PolycomMGC50))
                                //        ((RequiredFieldValidator)dgProfiles.Items[i].FindControl("reqGuestAddress")).Enabled = true;
                                //}
                            }
                        }
                    }
                }
                ScriptManager.RegisterStartupScript(this, this.GetType(), "roomExpColStat", "fnSetExpColStatus();", true);

            }
            catch (System.Threading.ThreadAbortException)
            { }
            catch (Exception ex)
            {
                log.Trace("GuestDftConnctType: " + ex.StackTrace + " : " + ex.Message);
            }
        }
        #endregion

        #region SetDftProfile

        protected void SetDftProfile(Object sender, EventArgs e)
        {
            try
            {
                DataGridItem dgi;
                RadioButton chkTemp = (RadioButton)sender;
                string DftConnection = lstGuestConnectionType.SelectedValue;
                for (int x = 0; x < dgProfiles.Items.Count; x++)
                {
                    dgi = dgProfiles.Items[x];
                    ((RadioButton)dgi.FindControl("rdDefault")).Checked = false;
                    ((DropDownList)dgi.FindControl("lstGuestConnectionType1")).Enabled = true;
                    ((CheckBox)dgi.FindControl("chkDelete")).Enabled = true;
                    if (((RadioButton)dgi.FindControl("rdDefault")).Equals(chkTemp))
                    {
                        ((RadioButton)dgi.FindControl("rdDefault")).Checked = true;
                        ((DropDownList)dgi.FindControl("lstGuestConnectionType1")).SelectedValue = DftConnection;
                        ((DropDownList)dgi.FindControl("lstGuestConnectionType1")).Enabled = false;
                        ((CheckBox)dgi.FindControl("chkDelete")).Enabled = false;
                    }
                }
                ScriptManager.RegisterStartupScript(this, this.GetType(), "roomExpColStat", "fnSetExpColStatus();", true);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "DataLoadingDft", "DataLoading(0);", true);
            }
            catch (Exception ex)
            {
                log.Trace(ex.StackTrace + " : " + ex.Message);
            }
        }

        #endregion

        #region Create Column Names
        /// <summary>
        /// Create Column Names
        /// </summary>
        private void CreateProfileDtColumnNames()
        {

            colNames = new ArrayList();
            colNames.Add("RowUID");
            colNames.Add("Address");
            colNames.Add("AddressType");
            colNames.Add("ConnectionType");
            colNames.Add("MaxLineRate");
            colNames.Add("DefaultProfile");
            colNames.Add("BridgeID");
            colNames.Add("Deleted");
            colNames.Add("ProfileCnt");
        }
        #endregion

        #region BindProfileOptionData
        /// <summary>
        /// BindProfileOptionData
        /// </summary>
        private void BindProfileOptionData(int row)
        {
            try
            {
                int isDeft = 0;
                if (Session["onflyGridProfiles"] == null && onflyGridProfiles != null)
                    Session.Add("onflyGridProfiles", onflyGridProfiles);

                if (onflyGridProfiles == null)
                    onflyGridProfiles = (DataTable)Session["onflyGridProfiles"];

                DataTable dt = new DataTable();
                CreateProfileDtColumnNames();
                dt = obj.LoadDataTable(null, colNames);
                DataRow dr;
                for (int i = 0; i < onflyGridProfiles.Rows.Count; i++)
                {
                    if (onflyGridProfiles.Rows[i]["RowUID"].Equals(row.ToString()))
                    {
                        dr = dt.NewRow();
                        dr["RowUID"] = onflyGridProfiles.Rows[i]["RowUID"];
                        dr["Address"] = onflyGridProfiles.Rows[i]["Address"];
                        dr["AddressType"] = onflyGridProfiles.Rows[i]["AddressType"];
                        dr["ConnectionType"] = onflyGridProfiles.Rows[i]["ConnectionType"];
                        dr["MaxLineRate"] = onflyGridProfiles.Rows[i]["MaxLineRate"];
                        dr["DefaultProfile"] = onflyGridProfiles.Rows[i]["DefaultProfile"];
                        dr["BridgeID"] = onflyGridProfiles.Rows[i]["BridgeID"];
                        dr["Deleted"] = onflyGridProfiles.Rows[i]["Deleted"];
                        dr["ProfileCnt"] = onflyGridProfiles.Rows[i]["ProfileCnt"];
                        dt.Rows.Add(dr);
                    }
                }
                dgProfiles.DataSource = dt;
                dgProfiles.DataBind();

                for (int i = 0; i < dgProfiles.Items.Count; i++)
                {
                    isDeft = 0;
                    if (((RadioButton)dgProfiles.Items[i].FindControl("rdDefault")).Checked)
                        isDeft = 1;
                    ((CheckBox)dgProfiles.Items[i].FindControl("chkDelete")).Enabled = true;
                    ((DropDownList)dgProfiles.Items[i].FindControl("lstGuestConnectionType1")).Enabled = true;
                    if (isDeft == 1)
                    {
                        ((DropDownList)dgProfiles.Items[i].FindControl("lstGuestConnectionType1")).Enabled = false;
                        ((CheckBox)dgProfiles.Items[i].FindControl("chkDelete")).Enabled = false;
                    }

                    if (dgProfiles.Items.Count == 5)
                    {
                        btnGuestAddProfile.Disabled = true;
                        btnGuestAddProfile.Attributes.Add("Class", "btndisable");
                    }
                }

            }
            catch (Exception ex)
            {
                errLabel.Visible = true;
                log.Trace("BindProfileOptionData" + ex.Message);
                errLabel.Text = obj.ShowSystemMessage();
            }
        }
        #endregion

        #region ChangeMCU
        protected void ChangeMCU(Object sender, EventArgs e)
        {
            try
            {
                int BridgeType = 0, ConnectionType = 0;
                DataGridItem clickedRow = ((DropDownList)sender).NamingContainer as DataGridItem;
                DropDownList lstBridge = (DropDownList)clickedRow.FindControl("lstGuestBridges");
                DropDownList lstBridgeType = (DropDownList)clickedRow.FindControl("lstBridgeType");
                DropDownList lstConnectionType = (DropDownList)clickedRow.FindControl("lstGuestConnectionType1");

                if (!lstBridge.SelectedValue.Equals("-1"))
                {
                    lstBridgeType.SelectedValue = lstBridge.SelectedValue;
                    int.TryParse(lstBridgeType.SelectedItem.Text, out BridgeType);
                    int.TryParse(lstConnectionType.SelectedValue.ToString(), out ConnectionType);

                }
                //((RequiredFieldValidator)clickedRow.FindControl("reqGuestAddress")).Enabled = false;
                //if (ConnectionType == 1)
                //{
                //    if (BridgeType == 8)
                //        ((RequiredFieldValidator)clickedRow.FindControl("reqGuestAddress")).Enabled = true;
                //}
                //else
                //    ((RequiredFieldValidator)clickedRow.FindControl("reqGuestAddress")).Enabled = true;

                ScriptManager.RegisterStartupScript(this, this.GetType(), "roomExpColStat", "fnSetExpColStatus();", true);
            }
            catch (Exception ex)
            {
                log.Trace("ChangeMCU: " + ex.StackTrace + " : " + ex.Message);
            }
        }
        #endregion

        #region GusetLocClose

        public void GusetLocClose(object sender, EventArgs e)
        {
            try
            {
                hdnGuestloc.Value = "";
                hdnGuestRoomID.Value = "";
                LoadGuestEndpointProfile(-1);
                guestLocationPopup.Hide();//ZD100619
            }
            catch (Exception ex)
            {
                log.Trace(ex.StackTrace);
            }
        }

        #endregion

        //ZD 100619 Starts
        #region InitializeGuest
        protected void InitializeGuest(Object sender, DataGridItemEventArgs e)
        {
            try
            {
                if (e.Item.ItemType.Equals(ListItemType.Item) || e.Item.ItemType.Equals(ListItemType.AlternatingItem))// || (e.Item.ItemType.Equals(ListItemType.Footer)))
                {
                    if ((DropDownList)e.Item.FindControl("lstGuestLineRate") != null)
                        obj.BindLineRate((DropDownList)e.Item.FindControl("lstGuestLineRate"));
                    if ((DropDownList)e.Item.FindControl("lstGuestAddressType") != null)
                        obj.BindAddressType((DropDownList)e.Item.FindControl("lstGuestAddressType"));
                    if ((DropDownList)e.Item.FindControl("lstGuestConnectionType1") != null)
                        obj.BindDialingOptions((DropDownList)e.Item.FindControl("lstGuestConnectionType1"));
                    if ((DropDownList)e.Item.FindControl("lstGuestBridges") != null)
                        obj.BindBridges((DropDownList)e.Item.FindControl("lstGuestBridges"));
                    if ((DropDownList)e.Item.FindControl("lstGuestBridges") != null)
                        obj.BindBridgesType((DropDownList)e.Item.FindControl("lstBridgeType"));
                    if (((Label)e.Item.FindControl("lblProfileCount")) != null)
                        ((Label)e.Item.FindControl("lblProfileCount")).Text = (e.Item.ItemIndex + 1).ToString();

                    //ZD 100815
                    if (lstConferenceType.SelectedValue.Equals("4"))
                    {
                        ((Literal)e.Item.FindControl("LblGstEP")).Text = obj.GetTranslatedText("Select Caller/Callee");
                        ((DropDownList)e.Item.FindControl("lstGuestBridges")).Visible = false;
                        ((DropDownList)e.Item.FindControl("lstTelnetUsers")).Visible = true;
                        ((DropDownList)e.Item.FindControl("lstTelnetUsers")).Enabled = false; //ZD 101345
                    }
                    //ZD 100815
                }
            }
            catch (Exception ex)
            {
                errLabel.Visible = true;
                log.Trace(ex.StackTrace + " : " + ex.Message);
            }

        }
        #endregion
        //ZD 100619 Ends

        //ZD 101233 START
        #region CrossSilo
        private void CrossSilo()
        {
            try
            {
                string xml = "";
                int multiOrgId = 11, EnableSmartP2P = 0;
                string multisiloOrgID = Session["organizationID"].ToString();
                if (Session["multisiloOrganizationID"] != null)
                    multisiloOrgID = Session["multisiloOrganizationID"].ToString();
                Int32.TryParse(multisiloOrgID, out multiOrgId);

                XmlDocument xmldoc = null;
                hdnCrossSetupTime.Value = "-1"; 
                hdnCrossTearDownTime.Value = "-1"; 
                hdnCrossEnableSmartP2P.Value = "-1";

                if (multiOrgId >= 11)
                {
                    xml = obj.CallMyVRMServer("GetAllOrgSettings", "<GetAllOrgSettings><organizationID>" + multisiloOrgID + "</organizationID></GetAllOrgSettings>", HttpContext.Current.Application["MyVRMServer_ConfigPath"].ToString());
                    if (xml.IndexOf("<error>") <= 0)
                    {
                        xmldoc = new XmlDocument();
                        xmldoc.LoadXml(xml);

                        hdnCrossrecurEnable.Value = xmldoc.SelectSingleNode("//GetAllOrgSettings/EnableRecurringConference").InnerText;
                        hdnCrossdynInvite.Value = xmldoc.SelectSingleNode("//GetAllOrgSettings/EnableDynamicInvite").InnerText;
                        hdnCrossroomModule.Value = xmldoc.SelectSingleNode("//GetAllOrgSettings/EnableFacilites").InnerText;
                        hdnCrossfoodModule.Value = xmldoc.SelectSingleNode("//GetAllOrgSettings/EnableCatering").InnerText;
                        hdnCrosshkModule.Value = xmldoc.SelectSingleNode("//GetAllOrgSettings/EnableHouseKeeping").InnerText;
                        hdnCrossisVIP.Value = xmldoc.SelectSingleNode("//GetAllOrgSettings/isVIP").InnerText;
                        hdnCrossEnableRoomServiceType.Value = xmldoc.SelectSingleNode("//GetAllOrgSettings/EnableRoomServiceType").InnerText;
                        hdnCrossisSpecialRecur.Value = xmldoc.SelectSingleNode("//GetAllOrgSettings/isSpecialRecur").InnerText;
                        hdnCrossConferenceCode.Value = xmldoc.SelectSingleNode("//GetAllOrgSettings/ConferenceCode").InnerText;
                        hdnCrossLeaderPin.Value = xmldoc.SelectSingleNode("//GetAllOrgSettings/LeaderPin").InnerText;
                        hdnCrossPartyCode.Value = xmldoc.SelectSingleNode("//GetAllOrgSettings/EnablePartyCode").InnerText; //ZD 101446
                        hdnCrossAdvAvParams.Value = xmldoc.SelectSingleNode("//GetAllOrgSettings/AdvAvParams").InnerText;
                        hdnCrossEnableBufferZone.Value = xmldoc.SelectSingleNode("//GetAllOrgSettings/EnableBufferZone").InnerText;
                        hdnCrossEnableEntity.Value = xmldoc.SelectSingleNode("//GetAllOrgSettings/EnableCustomOption").InnerText;
                        hdnCrossAudioParams.Value = xmldoc.SelectSingleNode("//GetAllOrgSettings/AudioParams").InnerText;
                        hdnCrossdefaultPublic.Value = xmldoc.SelectSingleNode("//GetAllOrgSettings/DefaultConferencesAsPublic").InnerText;
                        hdnCrossP2PEnable.Value = xmldoc.SelectSingleNode("//GetAllOrgSettings/EnableP2PConference").InnerText;
                        hdnCrossEnableRoomConfType.Value = xmldoc.SelectSingleNode("//GetAllOrgSettings/EnableRoomConference").InnerText;
                        hdnCrossEnableHotdeskingConfType.Value = xmldoc.SelectSingleNode("//GetAllOrgSettings/EnableHotdeskingConference").InnerText;
                        hdnCrossEnableAudioVideoConfType.Value = xmldoc.SelectSingleNode("//GetAllOrgSettings/EnableAudioVideoConference").InnerText;
                        hdnCrossDefaultConferenceType.Value = xmldoc.SelectSingleNode("//GetAllOrgSettings/DefaultConferenceType").InnerText;
                        hdnCrossEnableAudioOnlyConfType.Value = xmldoc.SelectSingleNode("//GetAllOrgSettings/EnableAudioOnlyConference").InnerText;
                        hdnCrossDefaultConferenceType.Value = xmldoc.SelectSingleNode("//GetAllOrgSettings/DefaultConferenceType").InnerText;
                        hdnCrossisMultiLingual.Value = xmldoc.SelectSingleNode("//GetAllOrgSettings/isMultiLingual").InnerText;
                        hdnCrossroomExpandLevel.Value = xmldoc.SelectSingleNode("//GetAllOrgSettings/RoomTreeExpandLevel").InnerText;
                        hdnCrossEnableImmConf.Value = xmldoc.SelectSingleNode("//GetAllOrgSettings/EnableImmConf").InnerText;
                        hdnCrossEnablePublicConf.Value = xmldoc.SelectSingleNode("//GetAllOrgSettings/EnablePublicConference").InnerText;
                        hdnCrossEnableConfPassword.Value = xmldoc.SelectSingleNode("//GetAllOrgSettings/EnableConferencePassword").InnerText;
                        hdnCrossEnableHostGuestPwd.Value = xmldoc.SelectSingleNode("//GetAllOrgSettings/EnableHostGuestPwd").InnerText;   //ALLDEV-826
                        hdnCrossEnableRoomParam.Value = xmldoc.SelectSingleNode("//GetAllOrgSettings/EnableRoomParam").InnerText;
                        hdnCrossEnableSurvey.Value = xmldoc.SelectSingleNode("//GetAllOrgSettings/EnableSurvey").InnerText;
                        hdnCrossSetupTime.Value = xmldoc.SelectSingleNode("//GetAllOrgSettings/SetupTime").InnerText;
                        hdnCrossTearDownTime.Value = xmldoc.SelectSingleNode("//GetAllOrgSettings/TearDownTime").InnerText;
                        hdnCrossEnableAudioBridges.Value = xmldoc.SelectSingleNode("//GetAllOrgSettings/EnableAudioBridges").InnerText;
                        hdnTxtMsg.Value = xmldoc.SelectSingleNode("//GetAllOrgSettings/MCUEnchancedLimit").InnerText;
                        hdnCrossMeetGreetBufferTime.Value = xmldoc.SelectSingleNode("//GetAllOrgSettings/MeetandGreetBuffer").InnerText;
                        
                        if (xmldoc.SelectSingleNode("//GetAllOrgSettings/EnableSmartP2P") != null) 
                            hdnCrossEnableSmartP2P.Value = xmldoc.SelectSingleNode("//GetAllOrgSettings/EnableSmartP2P").InnerText;
                        
                        if (xmldoc.SelectSingleNode("//GetAllOrgSettings/EnableLinerate") != null)
                            hdnCrossEnableLinerate.Value = xmldoc.SelectSingleNode("//GetAllOrgSettings/EnableLinerate").InnerText;
                        
                        if (xmldoc.SelectSingleNode("//GetAllOrgSettings/EnableStartMode") != null)
                            hdnCrossEnableStartMode.Value = xmldoc.SelectSingleNode("//GetAllOrgSettings/EnableStartMode").InnerText;
                        
                        hdnEnableNumericID.Value = xmldoc.SelectSingleNode("//GetAllOrgSettings/EnableNumericID").InnerText;
                        hdnEnableProfileSelection.Value = xmldoc.SelectSingleNode("//GetAllOrgSettings/EnableProfileSelection").InnerText;
                        hdnEnablePoolOrderSelection.Value = xmldoc.SelectSingleNode("//GetAllOrgSettings/EnablePoolOrderSelection").InnerText; //ZD 104256
                        hdnEnableFECC.Value = xmldoc.SelectSingleNode("//GetAllOrgSettings/EnableFECC").InnerText;//ZD 101931
                        hdnMCUSetupTime.Value = xmldoc.SelectSingleNode("//GetAllOrgSettings/McuSetupTime").InnerText;
                        hdnMCUTeardownTime.Value = xmldoc.SelectSingleNode("//GetAllOrgSettings/MCUTeardonwnTime").InnerText;
                        hdnMCUConnectDisplay.Value = xmldoc.SelectSingleNode("//GetAllOrgSettings/MCUSetupDisplay").InnerText;
                        hdnMCUDisconnectDisplay.Value = xmldoc.SelectSingleNode("//GetAllOrgSettings/MCUTearDisplay").InnerText;
                        hdnNetworkSwitching.Value = xmldoc.SelectSingleNode("//GetAllOrgSettings/NetworkSwitching").InnerText;
                        hdnWebExCOnf.Value = xmldoc.SelectSingleNode("//GetAllOrgSettings/WebexUserLimit").InnerText;
                        hdnEnableWebExIngt.Value = xmldoc.SelectSingleNode("//GetAllOrgSettings/EnableWebExIntg").InnerText;
                        hdnGLWarningPopUp.Value = xmldoc.SelectSingleNode("//GetAllOrgSettings/EnableGuestLocWarningMsg").InnerText;
                        hdnGLApprTime.Value = xmldoc.SelectSingleNode("//GetAllOrgSettings/GuestLocApprovalTime").InnerText;
                        hdnVMRPINChange.Value = xmldoc.SelectSingleNode("//GetAllOrgSettings/VMRPINChange").InnerText;
                        hdnPINlen.Value = xmldoc.SelectSingleNode("//GetAllOrgSettings/PasswordCharLength").InnerText;
                        hnDetailedExpForm.Value = xmldoc.SelectSingleNode("//GetAllOrgSettings/EnableDetailedExpressForm").InnerText;
						hdnEnablEOBTP.Value = xmldoc.SelectSingleNode("//GetAllOrgSettings/EnableWETConference").InnerText;//ZD 100513
                        hdnWebExLaunch.Value = xmldoc.SelectSingleNode("//GetAllOrgSettings/WebExLaunch").InnerText;//ZD 100513
						hdnEnableExpressConfType.Value = xmldoc.SelectSingleNode("//GetAllOrgSettings/EnableDetailedExpressForm").InnerText; //ZD 101490
                        hdntemplatebooking.Value = xmldoc.SelectSingleNode("//GetAllOrgSettings/EnableTemplateBooking").InnerText;//ZD 101562
                        //ZD 101755 start
                        if (xmldoc.SelectSingleNode("//GetAllOrgSettings/EnableSetupTimeDisplay") != null)
                            hdnSetuptimeDisplay.Value = xmldoc.SelectSingleNode("//GetAllOrgSettings/EnableSetupTimeDisplay").InnerText;
                        if (xmldoc.SelectSingleNode("//GetAllOrgSettings/EnableTeardownTimeDisplay") != null)
                            hdnTeardowntimeDisplay.Value = xmldoc.SelectSingleNode("//GetAllOrgSettings/EnableTeardownTimeDisplay").InnerText;
                        //ZD 101755 End
                        //ZD 103550 - start                        
                        if (xmldoc.SelectSingleNode("//GetAllOrgSettings/BJNSelectOption") != null)
                            hdnCrossBJNSelectOption.Value = xmldoc.SelectSingleNode("//GetAllOrgSettings/BJNSelectOption").InnerText;
                        if (xmldoc.SelectSingleNode("//GetAllOrgSettings/EnableBlueJeans") != null)
                            hdnCrossEnableBlueJeans.Value = xmldoc.SelectSingleNode("//GetAllOrgSettings/EnableBlueJeans").InnerText;
                        if (xmldoc.SelectSingleNode("//GetAllOrgSettings/EnableBJNIntegration") != null)
                            hdnCrossBJNIntegration.Value = xmldoc.SelectSingleNode("//GetAllOrgSettings/EnableBJNIntegration").InnerText;
                        //ZD 103550 - End
                        //ZD 104116 - Start
                        if (xmldoc.SelectSingleNode("//GetAllOrgSettings/BJNDisplay") != null)
                            hdnCrossBJNDisplay.Value = xmldoc.SelectSingleNode("//GetAllOrgSettings/BJNDisplay").InnerText;
                        //ZD 104116 - End
                        //ALLDEV-782 Starts
                        if (xmldoc.SelectSingleNode("//GetAllOrgSettings/PexipSelectOption") != null)
                            hdnCrossPexipSelectOption.Value = xmldoc.SelectSingleNode("//GetAllOrgSettings/PexipSelectOption").InnerText;
                        //if (xmldoc.SelectSingleNode("//GetAllOrgSettings/EnableBlueJeans") != null)
                        //    hdnCrossEnableBlueJeans.Value = xmldoc.SelectSingleNode("//GetAllOrgSettings/EnableBlueJeans").InnerText;
                        if (xmldoc.SelectSingleNode("//GetAllOrgSettings/EnablePexipIntegration") != null)
                            hdnCrossPexipIntegration.Value = xmldoc.SelectSingleNode("//GetAllOrgSettings/EnablePexipIntegration").InnerText;
                        if (xmldoc.SelectSingleNode("//GetAllOrgSettings/PexipDisplay") != null)
                            hdnCrossPexipDisplay.Value = xmldoc.SelectSingleNode("//GetAllOrgSettings/PexipDisplay").InnerText;
                        //ALLDEV-782 Ends
                    }
                }

                if (hdnMCUConnectDisplay.Value != "")
                    mcuSetupDisplay = hdnMCUConnectDisplay.Value;

                if (hdnMCUDisconnectDisplay.Value != "")
                    mcuTearDisplay = hdnMCUDisconnectDisplay.Value;

                if (hdnCrossDefaultConferenceType != null && hdnCrossDefaultConferenceType.Value != "")
                    DefaultConferenceType = hdnCrossDefaultConferenceType.Value;
                else if (Session["DefaultConferenceType"] != null)
                    DefaultConferenceType = Session["DefaultConferenceType"].ToString();

                if (hdnCrossroomModule != null && hdnCrossroomModule.Value != "")
                    roomModule = hdnCrossroomModule.Value;
                else if (Session["roomModule"] != null)
                    roomModule = Session["roomModule"].ToString();

                if (hdnCrosshkModule != null && hdnCrosshkModule.Value != "")
                    hkModule = hdnCrosshkModule.Value;
                else if (Session["hkModule"] != null)
                    hkModule = Session["hkModule"].ToString();

                if (hdnCrossfoodModule != null && hdnCrossfoodModule.Value != "")
                    foodModule = hdnCrossfoodModule.Value;
                else if (Session["foodModule"] != null)
                    foodModule = Session["foodModule"].ToString();

                if (hdnCrossP2PEnable != null && hdnCrossP2PEnable.Value != "")
                    P2PEnableOrgOption = hdnCrossP2PEnable.Value;
                else if (Session["P2PEnable"] != null)
                    P2PEnableOrgOption = Session["P2PEnable"].ToString();

                if (hdnCrossEnableRoomConfType != null && hdnCrossEnableRoomConfType.Value != "")
                    EnableRoomConfTypeOrgOption = hdnCrossEnableRoomConfType.Value;
                else if (Session["EnableRoomConfType"] != null)
                    EnableRoomConfTypeOrgOption = Session["EnableRoomConfType"].ToString();

                
                if (hdnCrossEnableHotdeskingConfType != null && hdnCrossEnableHotdeskingConfType.Value != "")
                    EnableHotdeskingConfTypeOrgOption = hdnCrossEnableHotdeskingConfType.Value;
                
                else if (Session["EnableHotdeskingConference"] != null)
                    EnableHotdeskingConfTypeOrgOption = Session["EnableHotdeskingConference"].ToString();
               

                if (hdnCrossEnableAudioVideoConfType != null && hdnCrossEnableAudioVideoConfType.Value != "")
                    EnableAudioVideoConfTypeOrgOption = hdnCrossEnableAudioVideoConfType.Value;
                else if (Session["EnableAudioVideoConfType"] != null)
                    EnableAudioVideoConfTypeOrgOption = Session["EnableAudioVideoConfType"].ToString();

                if (hdnCrossEnableAudioOnlyConfType != null && hdnCrossEnableAudioOnlyConfType.Value != "")
                    EnableAudioOnlyConfTypeOrgOption = hdnCrossEnableAudioOnlyConfType.Value;
                else if (Session["EnableAudioOnlyConfType"] != null)
                    EnableAudioOnlyConfTypeOrgOption = Session["EnableAudioOnlyConfType"].ToString();

                if (hdnCrossisVIP != null && hdnCrossisVIP.Value != "")
                    EnableIsVip = hdnCrossisVIP.Value;
                else if (Session["isVIP"] != null)
                    EnableIsVip = Session["isVIP"].ToString();

                if (hdnCrossEnableRoomServiceType != null && hdnCrossEnableRoomServiceType.Value != "")
                    EnableServiceType = hdnCrossEnableRoomServiceType.Value;
                else if (Session["EnableRoomServiceType"] != null)
                    EnableServiceType = Session["EnableRoomServiceType"].ToString();

                if (hdnCrossEnablePublicConf != null && hdnCrossEnablePublicConf.Value != "")
                    EnablePublicConference = hdnCrossEnablePublicConf.Value;
                else if (Session["EnablePublicConf"] != null)
                    EnablePublicConference = Session["EnablePublicConf"].ToString();

               
                if (hdnCrossdefaultPublic != null && hdnCrossdefaultPublic.Value != "")
                    defaultPublic = hdnCrossdefaultPublic.Value;
                else if (Session["defaultpublic"] != null)
                    defaultPublic = Session["defaultpublic"].ToString();
               

                if (hdnCrossEnableConfPassword != null && hdnCrossEnableConfPassword.Value != "")
                    EnableConferencePasswordOrgOption = hdnCrossEnableConfPassword.Value;
                else if (Session["EnableConfPassword"] != null)
                    EnableConferencePasswordOrgOption = Session["EnableConfPassword"].ToString();

                //ALLDEV-826 Starts
                if (hdnCrossEnableHostGuestPwd != null && hdnCrossEnableHostGuestPwd.Value != "")
                    EnableHostGuestPwdOrgOption = hdnCrossEnableHostGuestPwd.Value;
                else if (Session["EnableHostGuestPwd"] != null)
                    EnableHostGuestPwdOrgOption = Session["EnableHostGuestPwd"].ToString();
                //ALLDEV-826 Ends

                if (hdnCrossdynInvite != null && hdnCrossdynInvite.Value != "")
                    Enableopenforregistration = hdnCrossdynInvite.Value;
                else if (Session["dynamicInviteEnabled"] != null)
                    Enableopenforregistration = Session["dynamicInviteEnabled"].ToString();
                

                if (hdnCrossEnableRoomParam != null && hdnCrossEnableRoomParam.Value != "")
                    EnableRoomParam = hdnCrossEnableRoomParam.Value;
                else if (Session["EnableRoomParam"] != null)
                    EnableRoomParam = Session["EnableRoomParam"].ToString();

                if (hdnCrossEnableBufferZone != null && hdnCrossEnableBufferZone.Value != "")
                    enableBufferZone = hdnCrossEnableBufferZone.Value;
                else
                    enableBufferZone = Session["EnableBufferZone"].ToString();

                if (hdnCrossEnableSurvey != null && hdnCrossEnableSurvey.Value != "")
                    if (Session["EnableSurveySilo"] == null)
                        Session.Add("EnableSurveySilo", hdnCrossEnableSurvey.Value);
                    else
                    {
                        EnableSurvey = hdnCrossEnableSurvey.Value;
                        Session["EnableSurveySilo"] = hdnCrossEnableSurvey.Value;
                    }
                
                Int32.TryParse(hdnCrossSetupTime.Value, out OrgSetupTime);  
                if (OrgSetupTime < 0 && Session["OrgSetupTime"] != null)
                    Int32.TryParse(Session["OrgSetupTime"].ToString(), out OrgSetupTime);

                Int32.TryParse(hdnCrossTearDownTime.Value, out OrgTearDownTime);
                if (OrgTearDownTime < 0 && Session["OrgTearDownTime"] != null)
                    Int32.TryParse(Session["OrgTearDownTime"].ToString(), out OrgTearDownTime); 
                Int32.TryParse(hdnCrossEnableSmartP2P.Value, out EnableSmartP2P);
                if (EnableSmartP2P < 0 && Session["EnableSmartP2P"] != null)
                {
                    Int32.TryParse(Session["EnableSmartP2P"].ToString(), out EnableSmartP2P);
                    hdnCrossEnableSmartP2P.Value = EnableSmartP2P.ToString();
                }
                
                int.TryParse(hdnCrossMeetGreetBufferTime.Value, out OrgMeetGrettBuffer);
                if (OrgMeetGrettBuffer < 0 && Session["MeetandGreetBuffer"] != null) 
                {
                    int.TryParse(Session["MeetandGreetBuffer"].ToString(), out OrgMeetGrettBuffer);
                    hdnCrossMeetGreetBufferTime.Value = OrgMeetGrettBuffer.ToString();
                }

               
                if (hdnCrossEnableLinerate != null && hdnCrossEnableLinerate.Value != "")
                    int.TryParse(hdnCrossEnableLinerate.Value, out EnableLinerate);
                else if (Session["EnableLinerate"] != null)
                    int.TryParse(Session["EnableLinerate"].ToString(), out EnableLinerate);

                if (hdnCrossEnableStartMode != null && hdnCrossEnableStartMode.Value != "")
                    int.TryParse(hdnCrossEnableStartMode.Value, out EnableStartMode);
                else if (Session["EnableStartMode"] != null)
                    int.TryParse(Session["EnableStartMode"].ToString(), out EnableStartMode);
                
                if (hdnNetworkSwitching != null && hdnNetworkSwitching.Value != "") 
                    int.TryParse(hdnNetworkSwitching.Value, out NetworkSwitching);
                else if (Session["NetworkSwitching"] != null)
                    int.TryParse(Session["NetworkSwitching"].ToString(), out NetworkSwitching);

                if (hdnEnableWebExIngt != null && hdnEnableWebExIngt.Value != "")
                    int.TryParse(hdnEnableWebExIngt.Value, out EnableWebExIntg);
                else if (Session["EnableWebExIntg"] != null)
                    int.TryParse(Session["EnableWebExIntg"].ToString(), out EnableWebExIntg);
               
                if (hdnGLApprTime != null && hdnGLApprTime.Value != "")
                    int.TryParse(hdnGLApprTime.Value, out GLApprlTime);
                else if (Session["GuestLocApprovalTime"] != null)
                    int.TryParse(Session["GuestLocApprovalTime"].ToString(), out GLApprlTime);

                if (hdnGLWarningPopUp != null && hdnGLWarningPopUp.Value != "")
                    int.TryParse(hdnGLWarningPopUp.Value, out GLWaringPopUp);
                else if (Session["EnableGuestLocWarningMsg"] != null)
                    int.TryParse(Session["EnableGuestLocWarningMsg"].ToString(), out GLWaringPopUp);
                
                if (hdnVMRPINChange != null && hdnVMRPINChange.Value != "")
                    int.TryParse(hdnVMRPINChange.Value, out VMRPINChange);
                else if (Session["VMRPINChange"] != null)
                    int.TryParse(Session["VMRPINChange"].ToString(), out VMRPINChange);

                if (hdnPINlen != null && hdnPINlen.Value != "")
                    int.TryParse(hdnPINlen.Value, out PINlength);
                else if (Session["PasswordCharLength"] != null)
                    int.TryParse(Session["PasswordCharLength"].ToString(), out PINlength);

                if (hnDetailedExpForm != null && hnDetailedExpForm.Value != "")
                    int.TryParse(hnDetailedExpForm.Value, out isDetaileDExpressForm);
                else if (Session["EnableDetailedExpressForm"] != null)
                    int.TryParse(Session["EnableDetailedExpressForm"].ToString(), out isDetaileDExpressForm);

				//ZD 100513 Starts
                if (hdnEnablEOBTP != null && hdnEnablEOBTP.Value != "")
                    int.TryParse(hdnEnablEOBTP.Value, out EnableOBTP);
                else if (Session["EnableWETConference"] != null)
                    int.TryParse(Session["EnableWETConference"].ToString(), out EnableOBTP);

                if (hdnWebExLaunch != null && hdnWebExLaunch.Value != "")
                    int.TryParse(hdnWebExLaunch.Value, out WebExLaunchMode);
                else if (Session["WebExLaunch"] != null)
                    int.TryParse(Session["WebExLaunch"].ToString(), out WebExLaunchMode);
                //ZD 100513 Ends

                //ZD 101755 start
                if (hdnSetuptimeDisplay != null && hdnSetuptimeDisplay.Value != "")
                    int.TryParse(hdnSetuptimeDisplay.Value, out EnableSetupTimeDisplay);
                else if (Session["EnableSetupTimeDisplay"] != null)
                    int.TryParse(Session["EnableSetupTimeDisplay"].ToString(), out EnableSetupTimeDisplay);

                if (hdnTeardowntimeDisplay != null && hdnTeardowntimeDisplay.Value != "")
                    int.TryParse(hdnTeardowntimeDisplay.Value, out EnableTeardownTimeDisplay);
                else if (Session["EnableTeardownTimeDisplay"] != null)
                    int.TryParse(Session["EnableTeardownTimeDisplay"].ToString(), out EnableTeardownTimeDisplay);
                //ZD 101755  End

                //ZD 103550 - Start                

                if (hdnCrossBJNSelectOption != null && hdnCrossBJNSelectOption.Value != "")
                    EnableBJNSelectOption = hdnCrossBJNSelectOption.Value;
                else if (Session["BJNSelectOption"] != null)
                    EnableBJNSelectOption = Session["BJNSelectOption"].ToString();

                if (hdnCrossBJNIntegration != null && hdnCrossBJNIntegration.Value != "")
                    EnableBJNIntegration = hdnCrossBJNIntegration.Value;
                else if (Session["EnableBJNIntegration"] != null)
                    EnableBJNIntegration = Session["EnableBJNIntegration"].ToString();

                if (hdnCrossEnableBlueJeans != null && hdnCrossEnableBlueJeans.Value != "")
                    EnableBlueJeans = hdnCrossEnableBlueJeans.Value;
                else if (Session["EnableBlueJeans"] != null)
                    EnableBlueJeans = Session["EnableBlueJeans"].ToString();
                //ZD 103550 - End

                //ZD 104116 - Start
                if (hdnCrossBJNDisplay != null && hdnCrossBJNDisplay.Value != "")
                    EnableBJNDisplay = hdnCrossBJNDisplay.Value;
                else if (Session["BJNDisplay"] != null)
                    EnableBJNDisplay = Session["BJNDisplay"].ToString();
                //ZD 104116 - End
                //ALLDEV-782 Starts
                if (hdnCrossPexipSelectOption != null && hdnCrossPexipSelectOption.Value != "")
                    EnablePexipSelectOption = hdnCrossPexipSelectOption.Value;
                else if (Session["PexipSelectOption"] != null)
                    EnablePexipSelectOption = Session["PexipSelectOption"].ToString();

                if (hdnCrossPexipIntegration != null && hdnCrossPexipIntegration.Value != "")
                    EnablePexipIntegration = hdnCrossPexipIntegration.Value;
                else if (Session["EnablePexipIntegration"] != null)
                    EnablePexipIntegration = Session["EnablePexipIntegration"].ToString();

                if (hdnCrossPexipDisplay != null && hdnCrossPexipDisplay.Value != "")
                    EnablePexipDisplay = hdnCrossPexipDisplay.Value;
                else if (Session["PexipDisplay"] != null)
                    EnablePexipDisplay = Session["PexipDisplay"].ToString();

                if (hdnChangePexipVal != null && hdnChangePexipVal.Value != "")
                    EnablePexipMeetingID = hdnCrossPexipDisplay.Value;
                else if (Session["PexipMeetingType"] != null)
                    EnablePexipMeetingID = Session["PexipMeetingType"].ToString();
                //ALLDEV-782 Ends
            }
            catch (Exception ex)
            {
                errLabel.Visible = true;
                log.Trace("CrossSilo: " + ex.StackTrace);
            }
        }
        #endregion
        //ZD 101233 END

        //ZD 101233 START
        #region CrossSiloSession
        /// <summary>
        /// CrossSiloSession
        /// </summary>
        private void CrossSiloSession()
        {
            if (hdnCrossConferenceCode.Value != null && hdnCrossConferenceCode.Value != "")
                enableConferenceCode = hdnCrossConferenceCode.Value;
            else if (Session["ConferenceCode"] != null)
                enableConferenceCode = Session["ConferenceCode"].ToString();

            if (hdnCrossLeaderPin.Value != null && hdnCrossLeaderPin.Value != "")
                enableLeaderPin = hdnCrossLeaderPin.Value;
            else if (Session["LeaderPin"] != null)
                enableLeaderPin = Session["LeaderPin"].ToString();


            if (hdnCrossPartyCode.Value != null && hdnCrossPartyCode.Value != "") //ZD 101446
                enablePartCode = hdnCrossPartyCode.Value;
            else if (Session["EnablePartyCode"] != null)
                enablePartCode = Session["EnablePartyCode"].ToString();

            if (hdnCrossisMultiLingual != null && hdnCrossisMultiLingual.Value != "")
                isMulti = hdnCrossisMultiLingual.Value;
            else if (Session["isMultiLingual"] != null)
                isMulti = Session["isMultiLingual"].ToString();

            if (hdnCrossroomExpandLevel != null && hdnCrossroomExpandLevel.Value != "")
                roomExpand = hdnCrossroomExpandLevel.Value;
            else
                roomExpand = Session["roomExpandLevel"].ToString();
            
            if (hdnCrossEnableBufferZone != null && hdnCrossEnableBufferZone.Value != "")
                enableBufferZone = hdnCrossEnableBufferZone.Value;
            else
                enableBufferZone = Session["EnableBufferZone"].ToString();
            
            if (hdnCrossEnableEntity != null && hdnCrossEnableEntity.Value != "")
                enableEntity = hdnCrossEnableEntity.Value;

            if (hdnCrossdefaultPublic != null && hdnCrossdefaultPublic.Value != "")
                defaultPublic = hdnCrossdefaultPublic.Value;
            else if (Session["defaultpublic"] != null)
                defaultPublic = Session["defaultpublic"].ToString();
            if ((defaultPublic == "1" && chkPublic.Checked == true) || (defaultPublic == "0" && chkPublic.Checked == true))
                chkPublic.Checked = true;
            else if ((defaultPublic == "1" && chkPublic.Checked == false) || (defaultPublic == "0" && chkPublic.Checked == false))
                chkPublic.Checked = false;


            if (hdnCrossSetupTime.Value.Trim() != "" && hdnCrossSetupTime.Value.Trim() != "-1")
                Int32.TryParse(hdnCrossSetupTime.Value, out OrgSetupTime);
            else if (Session["OrgSetupTime"] != null)
            {
                if (Session["OrgSetupTime"].ToString() != null)
                    Int32.TryParse(Session["OrgSetupTime"].ToString(), out OrgSetupTime);
            }

            if (hdnCrossTearDownTime.Value.Trim() != "" && hdnCrossTearDownTime.Value.Trim() != "-1") 
                Int32.TryParse(hdnCrossTearDownTime.Value.Trim(), out OrgTearDownTime);
            else if (Session["OrgTearDownTime"] != null)
            {
                if (Session["OrgTearDownTime"].ToString() != "")
                    Int32.TryParse(Session["OrgTearDownTime"].ToString(), out OrgTearDownTime);
            }

            if (hdnNetworkSwitching != null && hdnNetworkSwitching.Value != "") 
                int.TryParse(hdnNetworkSwitching.Value, out NetworkSwitching);
            else if (Session["NetworkSwitching"] != null)
                int.TryParse(Session["NetworkSwitching"].ToString(), out NetworkSwitching);



            if (hdnCrossEnableLinerate != null && hdnCrossEnableLinerate.Value != "")
                int.TryParse(hdnCrossEnableLinerate.Value, out EnableLinerate);
            else if (Session["EnableLinerate"] != null)
                int.TryParse(Session["EnableLinerate"].ToString(), out EnableLinerate);

            if (hdnCrossEnableStartMode != null && hdnCrossEnableStartMode.Value != "")
                int.TryParse(hdnCrossEnableStartMode.Value, out EnableStartMode);
            else if (Session["EnableStartMode"] != null)
                int.TryParse(Session["EnableStartMode"].ToString(), out EnableStartMode);

            //ZD 103550 - Start           

            if (hdnCrossBJNSelectOption != null && hdnCrossBJNSelectOption.Value != "")
                EnableBJNSelectOption = hdnCrossBJNSelectOption.Value;
            else if (Session["BJNSelectOption"] != null)
                EnableBJNSelectOption = Session["BJNSelectOption"].ToString();

            if (hdnCrossEnableBlueJeans != null && hdnCrossEnableBlueJeans.Value != "")
                EnableBlueJeans = hdnCrossEnableBlueJeans.Value;
            else if (Session["EnableBlueJeans"] != null)
                EnableBlueJeans = Session["EnableBlueJeans"].ToString();  

            if (hdnCrossBJNIntegration != null && hdnCrossBJNIntegration.Value != "")
                EnableBJNIntegration = hdnCrossBJNIntegration.Value;
            else if (Session["EnableBJNIntegration"] != null)
                EnableBJNIntegration = Session["EnableBJNIntegration"].ToString();
			//ZD 103550 - End

            //ZD 104116 - Start
            if (hdnCrossBJNDisplay != null && hdnCrossBJNDisplay.Value != "")
                EnableBJNDisplay = hdnCrossBJNDisplay.Value;
            else if (Session["BJNDisplay"] != null)
                EnableBJNDisplay = Session["BJNDisplay"].ToString();
            //ZD 104116 - End

            if (hdnCrossisVIP != null && hdnCrossisVIP.Value != "")
                EnableIsVip = hdnCrossisVIP.Value;
            else if (Session["isVIP"] != null)
                EnableIsVip = Session["isVIP"].ToString();

            if (hdnCrossEnableRoomServiceType != null && hdnCrossEnableRoomServiceType.Value != "")
                EnableServiceType = hdnCrossEnableRoomServiceType.Value;
            else if (Session["EnableRoomServiceType"] != null)
                EnableServiceType = Session["EnableRoomServiceType"].ToString();


            if (hdnEnableWebExIngt != null && hdnEnableWebExIngt.Value != "") 
                int.TryParse(hdnEnableWebExIngt.Value, out EnableWebExIntg);
            else if (Session["EnableWebExIntg"] != null)
                int.TryParse(Session["EnableWebExIntg"].ToString(), out EnableWebExIntg);


            if (hdnGLApprTime != null && hdnGLApprTime.Value != "")
                int.TryParse(hdnGLApprTime.Value, out GLApprlTime);
            else if (Session["GuestLocApprovalTime"] != null)
                int.TryParse(Session["GuestLocApprovalTime"].ToString(), out GLApprlTime);

            if (hdnGLWarningPopUp != null && hdnGLWarningPopUp.Value != "")
                int.TryParse(hdnGLWarningPopUp.Value, out GLWaringPopUp);
            else if (Session["EnableGuestLocWarningMsg"] != null)
                int.TryParse(Session["EnableGuestLocWarningMsg"].ToString(), out GLWaringPopUp);

            if (hdnVMRPINChange != null && hdnVMRPINChange.Value != "")
                int.TryParse(hdnVMRPINChange.Value, out VMRPINChange);
            else if (Session["VMRPINChange"] != null)
                int.TryParse(Session["VMRPINChange"].ToString(), out VMRPINChange);

            if (hdnPINlen != null && hdnPINlen.Value != "")
                int.TryParse(hdnPINlen.Value, out PINlength);
            else if (Session["PasswordCharLength"] != null)
                int.TryParse(Session["PasswordCharLength"].ToString(), out PINlength);

            if (hnDetailedExpForm != null && hnDetailedExpForm.Value != "")
                int.TryParse(hnDetailedExpForm.Value, out isDetaileDExpressForm);
            else if (Session["EnableDetailedExpressForm"] != null)
                int.TryParse(Session["EnableDetailedExpressForm"].ToString(), out isDetaileDExpressForm);
			//ZD 100513 Starts
            if (hdnEnablEOBTP != null && hdnEnablEOBTP.Value != "")
                int.TryParse(hdnEnablEOBTP.Value, out EnableOBTP);
            else if (Session["EnableWETConference"] != null)
                int.TryParse(Session["EnableWETConference"].ToString(), out EnableOBTP);

            if (hdnWebExLaunch != null && hdnWebExLaunch.Value != "")
                int.TryParse(hdnWebExLaunch.Value, out WebExLaunchMode);
            else if (Session["WebExLaunch"] != null)
                int.TryParse(Session["WebExLaunch"].ToString(), out WebExLaunchMode);
            //ZD 100513 Ends

            //ZD 101755 start
            if (hdnSetuptimeDisplay != null && hdnSetuptimeDisplay.Value != "")
                int.TryParse(hdnSetuptimeDisplay.Value, out EnableSetupTimeDisplay);
            else if (Session["EnableSetupTimeDisplay"] != null)
                int.TryParse(Session["EnableSetupTimeDisplay"].ToString(), out EnableSetupTimeDisplay);

            if (hdnTeardowntimeDisplay != null && hdnTeardowntimeDisplay.Value != "")
                int.TryParse(hdnTeardowntimeDisplay.Value, out EnableTeardownTimeDisplay);
            else if (Session["EnableTeardownTimeDisplay"] != null)
                int.TryParse(Session["EnableTeardownTimeDisplay"].ToString(), out EnableTeardownTimeDisplay);
            //ZD 101755  End

            //ALLDEV-782 Starts
            if (hdnCrossPexipSelectOption != null && hdnCrossPexipSelectOption.Value != "")
                EnablePexipSelectOption = hdnCrossPexipSelectOption.Value;
            else if (Session["PexipSelectOption"] != null)
                EnablePexipSelectOption = Session["PexipSelectOption"].ToString();

            if (hdnCrossPexipIntegration != null && hdnCrossPexipIntegration.Value != "")
                EnablePexipIntegration = hdnCrossPexipIntegration.Value;
            else if (Session["EnablePexipIntegration"] != null)
                EnablePexipIntegration = Session["EnablePexipIntegration"].ToString();

            if (hdnCrossPexipDisplay != null && hdnCrossPexipDisplay.Value != "")
                EnablePexipDisplay = hdnCrossPexipDisplay.Value;
            else if (Session["PexipDisplay"] != null)
                EnablePexipDisplay = Session["PexipDisplay"].ToString();

            if (hdnChangePexipVal != null && hdnChangePexipVal.Value != "")
                EnablePexipMeetingID = hdnCrossPexipDisplay.Value;
            else if (Session["PexipMeetingType"] != null)
                EnablePexipMeetingID = Session["PexipMeetingType"].ToString();
            //ALLDEV-782 Ends
			
        }
        
        #endregion
        //ZD 101233 END

        //ZD 100815 Starts
        public void ValidatP2PSelection()
        {
            try
            {
                DataTable dtRooms = new DataTable();
                XmlDocument xmlDOC = new XmlDocument();
                XmlNode node = null;
                CreateDataTable(dtRooms);
                bool isError = false; int roomID; //ZD 100522
                string[] locs = selectedloc.Value.Split(',');
                List<int> SelRooms = new List<int>();
                string GuserID = "0", GUinXML = "", GoutXML = "";
                int UserType = 0;
                int GuestPfRow = 0, smartP2P = 0, gusetUser = 0; bool isCaller = false;

                isP2PGL.Value = "0"; usrCnt = 0; isIPAddress.Value = "0"; isTestedModel.Value = "0";

                smartP2P = dgUsers.Items.Count + dgOnflyGuestRoomlist.Items.Count; //ZD 100815
                hdnextusrcnt.Value = smartP2P.ToString();
                if (dgOnflyGuestRoomlist.Items.Count <= 2)
                {
                    foreach (DataGridItem item in dgOnflyGuestRoomlist.Items)
                    {
                        if (usrCnt == 0)
                        {
                            usrCnt = usrCnt + 1;
                            gusetUser = gusetUser + 1;
                            int.TryParse(item.Cells[0].Text.ToString(), out GuestPfRow);
                            BindProfileOptionData(GuestPfRow);
                            foreach (DataGridItem dpitem in dgProfiles.Items)
                            {
                                ((DropDownList)dpitem.FindControl("lstTelnetUsers")).ClearSelection();
                                ((DropDownList)dpitem.FindControl("lstTelnetUsers")).SelectedValue = "0";
                            }
                        }
                        else
                        {
                            int.TryParse(item.Cells[0].Text.ToString(), out GuestPfRow);
                            BindProfileOptionData(GuestPfRow);
                            foreach (DataGridItem dpitem in dgProfiles.Items)
                            {
                                ((DropDownList)dpitem.FindControl("lstTelnetUsers")).ClearSelection();
                                ((DropDownList)dpitem.FindControl("lstTelnetUsers")).SelectedValue = "0";
                            }
                            if (lstConferenceType.SelectedValue == ns_MyVRMNet.vrmConfType.P2P)
                                isP2PGL.Value = "1";

                            hdnSmartP2PTotalEps.Value = "0";
                            gusetUser = gusetUser + 1;
                        }

                    }
                }
                GuserID = "0";
                if (dgUsers.Items.Count <= 2)
                {
                    foreach (DataGridItem dgi in dgUsers.Items)
                    {
                        GuserID = "0";
                        if (hdnChgCallerCallee.Value != "1")//ZD 103402
                        {
                            ((DropDownList)dgi.FindControl("lstTelnetUsers")).ClearSelection();
                            if (usrCnt == 0)
                            {
                                usrCnt = usrCnt + 1;

                                if (dgi.Cells[0].Text.ToString().Trim().IndexOf("new") >= 0)
                                {
                                    gusetUser = gusetUser + 1;
                                    isCaller = false;
                                    ((DropDownList)dgi.FindControl("lstTelnetUsers")).Items.FindByValue("0").Selected = true;
                                    continue;
                                }
                                else
                                {
                                    GuserID = dgi.Cells[0].Text.ToString();
                                    GUinXML = "<GetUserType><UserID>" + GuserID + "</UserID></GetUserType>";
                                    GoutXML = obj.CallMyVRMServer("GetUserType", GUinXML, Application["MyVRMServer_ConfigPath"].ToString());
                                    if (GoutXML.IndexOf("<error>") < 0)
                                    {
                                        xmlDOC.LoadXml(GoutXML);
                                        node = null;
                                        node = (XmlNode)xmlDOC.DocumentElement;

                                        if (node.SelectSingleNode("//GetUserType/UserType") != null)
                                            int.TryParse(node.SelectSingleNode("//GetUserType/UserType").InnerText, out UserType);

                                    }
                                    if (UserType != 1)
                                    {
                                        gusetUser = gusetUser + 1;
                                        isCaller = false;
                                        ((DropDownList)dgi.FindControl("lstTelnetUsers")).Items.FindByValue("0").Selected = true;
                                        continue;
                                    }
                                }
                                if ((((DropDownList)dgi.FindControl("lstVideoEquipment")).SelectedItem.Text.Contains("*")) && (((DropDownList)dgi.FindControl("lstAddressType")).SelectedValue == "1"))
                                {
                                    isCaller = true;
                                    ((DropDownList)dgi.FindControl("lstTelnetUsers")).Items.FindByValue("1").Selected = true;

                                }
                                else
                                {
                                    isCaller = false;
                                    ((DropDownList)dgi.FindControl("lstTelnetUsers")).Items.FindByValue("0").Selected = true;
                                }
                            }
                            else if (usrCnt == 1)
                            {
                                usrCnt = usrCnt + 1;
                                if (!isCaller)
                                {

                                    if (dgi.Cells[0].Text.ToString().Trim().IndexOf("new") >= 0)
                                    {
                                        gusetUser = gusetUser + 1;
                                        ((DropDownList)dgi.FindControl("lstTelnetUsers")).Items.FindByValue("0").Selected = true;
                                        if (gusetUser == 2)
                                        {
                                            isP2PGL.Value = "1";
                                            hdnSmartP2PTotalEps.Value = "0";
                                            continue;
                                        }
                                    }
                                    else
                                    {
                                        GuserID = dgi.Cells[0].Text.ToString();
                                        GUinXML = "<GetUserType><UserID>" + GuserID + "</UserID></GetUserType>";
                                        GoutXML = obj.CallMyVRMServer("GetUserType", GUinXML, Application["MyVRMServer_ConfigPath"].ToString());
                                        if (GoutXML.IndexOf("<error>") < 0)
                                        {
                                            xmlDOC.LoadXml(GoutXML);
                                            node = null;
                                            node = (XmlNode)xmlDOC.DocumentElement;

                                            if (node.SelectSingleNode("//GetUserType/UserType") != null)
                                                int.TryParse(node.SelectSingleNode("//GetUserType/UserType").InnerText, out UserType);

                                        }
                                        if (UserType != 1)
                                        {
                                            isP2PGL.Value = "1";
                                            hdnSmartP2PTotalEps.Value = "0";
                                            continue;
                                        }
                                    }

                                    if ((((DropDownList)dgi.FindControl("lstVideoEquipment")).SelectedItem.Text.Contains("*")) && (((DropDownList)dgi.FindControl("lstAddressType")).SelectedValue == "1"))
                                        ((DropDownList)dgi.FindControl("lstTelnetUsers")).Items.FindByValue("1").Selected = true;
                                    else
                                    {
                                        if (!(((DropDownList)dgi.FindControl("lstVideoEquipment")).SelectedItem.Text.Contains("*")))
                                            isTestedModel.Value = "1";
                                        if (((DropDownList)dgi.FindControl("lstAddressType")).SelectedValue != "1")
                                            isIPAddress.Value = "1";
                                        ((DropDownList)dgi.FindControl("lstTelnetUsers")).Items.FindByValue("1").Selected = true;
                                        hdnSmartP2PTotalEps.Value = "0";
                                    }

                                }
                                else
                                    ((DropDownList)dgi.FindControl("lstTelnetUsers")).Items.FindByValue("0").Selected = true;
                            }
                        }
                    }
                }

                if (locs.Length <= 3)
                {
                    for (int i = 0; i < locs.Length; i++)
                    {
                        if (string.IsNullOrEmpty(locs[i]) || locs[i].Trim() == "")
                            continue;
                        int.TryParse(locs[i], out roomID);
                        if (!SelRooms.Contains(roomID))
                            SelRooms.Add(roomID);
                        else
                            continue;
                        if (lstVMR.SelectedIndex == 0 || lstVMR.SelectedValue == "2")
                            dtRooms.Rows.Add(AddRoomEndpoint(locs[i], dtRooms, ref isError, isCaller));
                    }

                    for (int i = 0; i < dtRooms.Rows.Count; i++)
                    {
                        if (usrCnt == 0)
                        {
                            usrCnt = usrCnt + 1;
                            if ((dtRooms.Rows[i]["AddressType"].ToString().Trim() == "1") && (dtRooms.Rows[i]["IsTestEquipment"].ToString().Trim() == "1"))
                            {
                                isCaller = true;
                                dtRooms.Rows[i]["Connect2"] = "1";
                            }
                            else
                            {
                                isCaller = false;
                                dtRooms.Rows[i]["Connect2"] = "0";
                            }
                        }
                        else if (usrCnt == 1)
                        {
                            usrCnt = usrCnt + 1;
                            if (!isCaller)
                            {
                                if ((dtRooms.Rows[i]["AddressType"].ToString().Trim() == "1") && (dtRooms.Rows[i]["IsTestEquipment"].ToString().Trim() == "1"))
                                    dtRooms.Rows[i]["Connect2"] = "1";
                                else
                                {
                                    if (dtRooms.Rows[i]["IsTestEquipment"].ToString().Trim() != "1")
                                        isTestedModel.Value = "1";
                                    if (dtRooms.Rows[i]["AddressType"].ToString().Trim() != "1")
                                        isIPAddress.Value = "1";
                                    hdnSmartP2PTotalEps.Value = "0";
                                }

                            }
                            else
                                dtRooms.Rows[i]["Connect2"] = "1";
                        }
                    }
                }
                smartP2P += SelRooms.Count;
                //hdnextusrcnt.Value = smartP2P.ToString();
                errLabel.Text = "";
                if (lstConferenceType.SelectedValue.Equals(ns_MyVRMNet.vrmConfType.P2P))
                {
                    if (isIPAddress.Value == "1" && isTestedModel.Value == "1")
                        errLabel.Text = obj.GetTranslatedText("The caller endpoint must use an IP address as the address type. The caller endpoint must be a certified endpoint.");
                    else if (isIPAddress.Value == "1")
                        errLabel.Text = obj.GetTranslatedText("The caller endpoint must use an IP address as the address type.");
                    else if (isTestedModel.Value == "1")
                        errLabel.Text = obj.GetTranslatedText("The caller endpoint must be a certified endpoint.");

                    if (isP2PGL.Value == "1")
                        errLabel.Text = obj.GetTranslatedText("The caller endpoint cannot be a Guest Attendee."); //ZD 101345
                    //errLabel.Text = obj.GetTranslatedText("The caller endpoint must use an IP address as the address type. The caller endpoint must be a certified endpoint.");
                    errLabel.Visible = true;
                    if (errLabel.Text != "") //ZD 101345
                    {
                        DisplayUserEndpoints();//ZD 103402
                        Session["hdModalDiv"] = "none";//ZD 101500  
                        topExpDiv.Style.Add("display", "none");
                        return;
                    }
                }
                //ZD 100815 End
                hdnSmartP2PTotalEps.Value = smartP2P.ToString(); 
                if ( !(lstConferenceType.SelectedValue.Equals(ns_MyVRMNet.vrmConfType.AudioVideo) && lstVMR.SelectedValue == "0" && chkPCConf.Checked == false)  || lstConferenceType.SelectedValue.Equals(ns_MyVRMNet.vrmConfType.AudioOnly) || lstConferenceType.SelectedValue.Equals(ns_MyVRMNet.vrmConfType.HotDesking) || lstConferenceType.SelectedValue.Equals(ns_MyVRMNet.vrmConfType.RoomOnly))
                    hdnSmartP2PTotalEps.Value = "0";

                if (hdnEnableExpressConfType.Value =="1" && EnableSmartP2P == 1 && isP2PConfirm.Value == "0" && hdnSmartP2PTotalEps.Value == "2" && (isIPAddress.Value == "0" && isTestedModel.Value == "0") && lstConferenceType.SelectedValue.Equals(ns_MyVRMNet.vrmConfType.AudioVideo))
                {
                    //ScriptManager.RegisterStartupScript(this, this.GetType(), "SmartP2PConfirm", "InitSetConference();", true);
                    if (hdnSmartP2PNotify.Value == "1")
                        p2pConfirmPopup.Show();
                    else if (hdnSmartP2PNotify.Value == "0")
                    {
                        lstConferenceType.SelectedValue = "4";
                        if (ValidateConferenceDur() && CheckTime())
                            NotificationAlert();
                    }
                    else if (hdnSmartP2PNotify.Value == "2")
                    {
                        if (isEditMode == "0")
                        {
                            lstConferenceType.SelectedValue = "4";
                            if (ValidateConferenceDur() && CheckTime())
                                NotificationAlert();
                        }
                        else
                            p2pConfirmPopup.Show();
                    }
                }
                else if (lstConferenceType.SelectedValue.Equals(ns_MyVRMNet.vrmConfType.P2P))
                {
                    if (isIPAddress.Value == "0" && isTestedModel.Value == "0" && isP2PGL.Value == "0")
                    {
                        if (ValidateConferenceDur() && CheckTime())
                            NotificationAlert();
                    }
                }
                else
                {
                    if (ValidateConferenceDur() && CheckTime())
                        NotificationAlert();
                }

            }

            catch (Exception ex)
            {
            }
        }
        //ZD 100815 Ends
		//ZD 101869 Starts
        #region GetVideoLayouts
        protected void GetVideoLayouts()
        {
            ImagesPath.Attributes.Add("style", "display:none");
            txtSelectedImage.Attributes.Add("style", "display:none");
            ImageFiles.Attributes.Add("style", "display:none");
            ImageFilesBT.Attributes.Add("style", "display:none");
            ImagesPath.Text = "image/displaylayout/";
            if (client.ToString().ToUpper().Equals("BTBOCES"))
                ImagesPath.Text += "BTBoces/";
            ImageFiles.Text = "";
            foreach (string file in Directory.GetFiles(Server.MapPath(ImagesPath.Text)))
            {
                if (file.ToLower().LastIndexOf(".gif") > 1)
                    ImageFiles.Text += file.ToLower().Replace(Server.MapPath(ImagesPath.Text).ToLower(), "").Replace(".gif", "") + ":";
            }
        }

        #endregion
        //ZD 101869 End

        //ZD 102909 - Start        

        #region GetEntityCodes
        [System.Web.Script.Services.ScriptMethod()]
        [System.Web.Services.WebMethod]
        public static string[] GetEntityCodes(string prefixText)
        {
            myVRMNet.NETFunctions obj = new myVRMNet.NETFunctions();
            ns_Logger.Logger log = new ns_Logger.Logger();
            List<string> EntityCodeList = new List<string>();
            DataTable dt = new DataTable();            
            try
            {
                if (HttpContext.Current.Session["EditEntityCodeVal"] != null)
                    HttpContext.Current.Session.Remove("EditEntityCodeVal");

                dt = obj.GetEntityCodes1(prefixText);
                
                // ZD 103265
                if (prefixText.Length >= 3)
                {
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        DataRow dr = dt.Rows[i];
                        EntityCodeList.Add(AjaxControlToolkit.AutoCompleteExtender.CreateAutoCompleteItem(dr["Caption"].ToString(), dr["OptionID"].ToString()));
                    }
                }

                //ZD 103265 start
                //if ((EntityCodeList == null) || (EntityCodeList != null && EntityCodeList.Count == 0))
                //{
                //    EntityCodeList.Add(obj.GetTranslatedText("No items found"));
                //}
                //ZD 103265 End
            }
            catch (Exception ex)
            {
                log.Trace("GetEntityCodes" + ex.Message);
            }
            return EntityCodeList.ToArray();
        }
        #endregion

        #region page_unload
        protected void Page_UnLoad(object sender, EventArgs e)
        {
            try
            {
                if (!IsPostBack)
                {
                    if (Session["EntityCodesVal"] != null)
                    {
                        Session.Remove("EntityCodesVal");
                    }
                }
            }
            catch (Exception ex)
            {
                log.Trace("RemoveEntityCodes" + ex.Message);
            }
        }       
        #endregion
        //ZD 102909 - End
    	//ALLDEV-833 - Start
        #region getPartyInfo
        /// <summary>
        /// getPartyInfo
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>btnSchedulingAssistant
        /// 
        protected void getPartyInfo(object sender, EventArgs e)
        {
            string Confparty = "";
            try
            {
                if (!string.IsNullOrEmpty(txtPartysInfo.Text))
                {
                    string[] partys = txtPartysInfo.Text.Split(pipeDelim, StringSplitOptions.RemoveEmptyEntries);

                    for (int i = 0; i < partys.Length; i++)
                    {
                        if (partys[i] != null && partys[i] != "")
                        {
                            string[] temp = partys[i].Split(ExclamDelim, StringSplitOptions.None);

                            if (Confparty != null && Confparty != "")
                                Confparty += ";" + temp[1] + " " + temp[2] + "|" + temp[3];
                            else
                                Confparty = temp[1] + " " + temp[2] + "|" + temp[3];
                        }
                    }
                }
                Session.Add("PartyList", Confparty);
                
                string confStDateTime = myVRMNet.NETFunctions.GetDefaultDate(confStartDate.Text) + " " + myVRMNet.NETFunctions.ChangeTimeFormat(confStartTime.Text);
                string confEndDateTime = myVRMNet.NETFunctions.GetDefaultDate(confEndDate.Text) + " " + myVRMNet.NETFunctions.ChangeTimeFormat(confEndTime.Text);

                string url = "ScheduleAssistantMain.aspx?stDate=" + confStDateTime + "&enDate=" + confEndDateTime;

                string xWidth = "920";
                if (hdngetScreenWidth.Value != "" && hdngetScreenWidth.Value != "0")
                    xWidth = hdngetScreenWidth.Value;

                string message = "window.open('" + url + "','SchedulingAssistant','resizable=yes,scrollbars=yes,toolbar=no, width=" + xWidth + ",height=550,top=0,left=0,status=no')";

                ScriptManager.RegisterClientScriptBlock(UpdateSchedulingAssistant, this.GetType(), "alert", message, true);                   

            }
            catch (Exception ex)
            {
                log.Trace(ex.StackTrace + " : " + ex.Message);
            }
        }
        #endregion
        //ALLDEV-833 - End
    }
}
