/* Copyright (C) 2015 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
//ZD 100147 //ZD 100886
using System;
using System.Data;
using System.Xml;
using System.Text;
using System.IO;
using System.Collections;
using System.ComponentModel;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Configuration;
using web_com_v18_Net;
using System.Xml.Schema;
using System.Globalization;
using System.Threading;
using System.Text.RegularExpressions;
/// <summary>
/// Summary description for SuperAdministrator.
/// </summary>

namespace ns_MYVRM
{
    public partial class MainAdministrator : System.Web.UI.Page
    {
        #region protected Members
        protected Label errLabel;
        protected DropDownList StartTimehr;
        protected DropDownList StartTimemi;
        protected DropDownList StartTimeap;
        protected DropDownList EndTimehr;
        protected DropDownList EndTimemi;
        protected DropDownList EndTimeap;
        protected DropDownList TimezoneSystems;
        protected DropDownList AutoAcpModConf;
        protected DropDownList RecurEnabled;
        protected DropDownList p2pConfEnabled;
        protected DropDownList DialoutEnabled;
        protected DropDownList DefaultPublic;
        protected DropDownList RealtimeType;
        protected DropDownList lstDefaultConferenceType;
        protected DropDownList lstRFIDValue;//FB 2724
        protected DropDownList lstEnableRoomConference;
        protected DropDownList lstEnableAudioVideoConference;
        protected DropDownList lstEnableAudioOnlyConference;
        protected DropDownList lstEnableHotdeskingConference;//ZD 100719
        protected DropDownList lstEnableNumericID;//FB 2870
        protected DropDownList IsDefaultOfficeHours;
        protected DropDownList lstRoomTreeLevel;
        protected DropDownList lstDefaultOfficeHours;
        protected DropDownList DynamicInviteEnabled;
        //protected DropDownList drpenablesecuritybadge;//FB 2136 start
        //protected DropDownList drpsecuritybadgetype;
        //protected System.Web.UI.HtmlControls.HtmlTableCell tdSecurityType;
        //protected TextBox txtsecdeskemailid;
        //protected System.Web.UI.HtmlControls.HtmlTableRow tdsecdeskemailid;//FB 2136 end
        protected MetaBuilders.WebControls.ComboBox systemStartTime;
        protected MetaBuilders.WebControls.ComboBox systemEndTime;
        //ZD 100381
        protected System.Web.UI.WebControls.RegularExpressionValidator regSysStartTime;
        protected System.Web.UI.WebControls.RegularExpressionValidator regSysEndTime;
        
        protected TextBox ContactName;
        protected TextBox ContactEmail;
        protected TextBox ContactPhone;
        protected TextBox ContactAdditionInfo;
        protected TextBox txtiControlTimeout; //FB 2724
        protected CheckBox Open24;
        protected CheckBoxList DayClosed;
        //Audio Add On Starts..
        protected System.Web.UI.WebControls.DropDownList DrpConfcode;
        protected System.Web.UI.WebControls.DropDownList DrpLedpin;
        protected System.Web.UI.WebControls.DropDownList Drpavprm;
        protected System.Web.UI.WebControls.DropDownList DrpAudprm;
        //Audio Add On Ends..
        protected System.Web.UI.WebControls.TextBox txtiCalEmailId;//FB 1786
		
	
        protected DropDownList EnableBufferZone; //Merging Recurence
        protected DropDownList DrpListIcal; //FB 1782
        protected DropDownList DrpAppIcal; //FB 1782
        protected DropDownList DrpVIP;
        protected DropDownList DrpUniquePassword;

        protected DropDownList DrpDwnListAssignedMCU; // FB 1901

        //ZD 101527 - Start
        protected DropDownList DrpRPRMRoomSync;
        protected DropDownList DrpRoomSyncAuto;
        protected System.Web.UI.WebControls.TextBox TxtPollHrs;
        protected System.Web.UI.HtmlControls.HtmlInputButton btnPoll;
        protected System.Web.UI.HtmlControls.HtmlInputButton btnPendingRoomImport;
        protected System.Web.UI.HtmlControls.HtmlInputButton btnPendingEndpointImport;
        protected System.Web.UI.HtmlControls.HtmlTableCell trSyncPoll; 
        //ZD 101527 - End

        //FB 1926 start
        protected CheckBox WklyChk;
        protected CheckBox DlyChk;
        protected CheckBox HourlyChk;
        protected CheckBox MinChk;
        //FB 1926 end
        protected DropDownList DrpDwnListMultiLingual; //FB 1830 - Translation
        protected DropDownList DrpPluginConfirm; // FB 2141
        protected DropDownList DrpDwnAttachmnts; // FB 2154
        protected DropDownList DrpDwnFilterTelepresence; // FB 2170
        protected DropDownList lstEnableRoomServiceType;//FB 2219
        protected DropDownList DrpDwnListSpRecur; // FB 2052         
        protected DropDownList DrpDwnListDeptUser; //FB 2269
		protected DropDownList lstEnablePIMServiceType; // FB 2038
        protected DropDownList lstEnableImmediateConference;//FB 2036
		protected DropDownList DrpDwnPasswordRule;//FB 2339
        protected DropDownList DrpDwnDedicatedVideo; // FB 2334
        //FB 2598 Starts
        protected DropDownList DrpDwnEnableCallmonitor;
        protected DropDownList DrpDwnEnableEM7;
        protected DropDownList DrpDwnEnableCDR;
        //FB 2598 Ends
        protected DropDownList DrpEnableE164DialPlan; // FB 2636
        protected DropDownList DropDownZuLu;//FB 2588 
        protected DropDownList lstEnableProfileSelection;//FB 2839
        protected System.Web.UI.HtmlControls.HtmlInputHidden hdnZuluChange; //FB 2588
        // FB 2335 Starts
        protected System.Web.UI.HtmlControls.HtmlInputHidden Poly2MGC;
        protected System.Web.UI.HtmlControls.HtmlInputHidden Poly2RMX;
        protected System.Web.UI.HtmlControls.HtmlInputHidden CTMS2Cisco;
        protected System.Web.UI.HtmlControls.HtmlInputHidden CTMS2Poly;

        protected System.Web.UI.WebControls.Image imgLayoutMapping1;
        protected System.Web.UI.WebControls.Image imgLayoutMapping2;
        protected System.Web.UI.WebControls.Image imgLayoutMapping3;
        protected System.Web.UI.WebControls.Image imgLayoutMapping4;
        // FB 2335 Ends
        protected DropDownList lstEnableAudioBridges, drpPartyCode;//FB 2023 //ZD 101446
        //FB 2359 Start
        protected DropDownList lstEnableConfPassword;
        protected DropDownList lstEnablePublicConf;
        protected DropDownList lstRoomprm;
        //FB 2359 End
        //FB 2348
        protected System.Web.UI.WebControls.DropDownList drpenablesurvey;
       // protected System.Web.UI.WebControls.DropDownList drpsurveyoption; // ZD 103416
        protected System.Web.UI.HtmlControls.HtmlContainerControl divSurveytimedur;
        protected System.Web.UI.HtmlControls.HtmlContainerControl divSurveyURL;
        protected System.Web.UI.WebControls.TextBox txtSurWebsiteURL;
        protected System.Web.UI.WebControls.TextBox txtTimeDur;
       // protected System.Web.UI.HtmlControls.HtmlTableCell tdSurveyengine; // ZD 103416
       // protected System.Web.UI.HtmlControls.HtmlTableCell tdsurveyoption; // ZD 103416
        //FB 2565 Start
        protected System.Web.UI.HtmlControls.HtmlTableRow trUSROPT;
        protected System.Web.UI.HtmlControls.HtmlTableRow trROOM;
        protected System.Web.UI.HtmlControls.HtmlTableRow trCONFOPT;
        protected System.Web.UI.HtmlControls.HtmlTableRow trAUD;
        protected System.Web.UI.HtmlControls.HtmlTableRow trADMOPT;
        protected System.Web.UI.HtmlControls.HtmlTableRow trPIM;
        protected System.Web.UI.HtmlControls.HtmlTableRow trSYS;
        protected System.Web.UI.HtmlControls.HtmlTableRow trSUR;
        protected System.Web.UI.HtmlControls.HtmlTableRow trCONFMAIL;
        //protected System.Web.UI.HtmlControls.HtmlTableRow trCONFSECDESK; 
        protected System.Web.UI.HtmlControls.HtmlTableRow trEPT;
        protected System.Web.UI.HtmlControls.HtmlTableRow trRoomSync;//ZD 101527
        protected System.Web.UI.HtmlControls.HtmlTableRow trFLY;
        protected System.Web.UI.HtmlControls.HtmlTableRow trOnfly;//FB 2426
        protected System.Web.UI.HtmlControls.HtmlTableRow trCONFDEF;
        protected System.Web.UI.HtmlControls.HtmlTableRow trCONFTYPE;
        protected System.Web.UI.HtmlControls.HtmlTableRow trFEAT;
        protected System.Web.UI.HtmlControls.HtmlTableRow trAUTO;
        protected System.Web.UI.HtmlControls.HtmlTableRow trNETSWT; //FB 2594
        protected System.Web.UI.HtmlControls.HtmlTableRow trICP; //FB 2724
        protected System.Web.UI.HtmlControls.HtmlTableRow trWO; //ZD 101228
        //FB 2565 End
        protected System.Web.UI.HtmlControls.HtmlTableRow trWebCre; //ZD 100221
        protected System.Web.UI.WebControls.DropDownList lstTopTier;//FB 2426
        protected System.Web.UI.WebControls.DropDownList lstMiddleTier;//FB 2426

        //FB 2565 Start
        protected System.Web.UI.WebControls.ImageButton img_USROPT;
        protected System.Web.UI.WebControls.ImageButton img_ROOM;
        protected System.Web.UI.WebControls.ImageButton img_CONFOPT;
        protected System.Web.UI.WebControls.ImageButton img_AUD;
        protected System.Web.UI.WebControls.ImageButton img_EPT;
        protected System.Web.UI.WebControls.ImageButton img_RoomSync;//ZD 101527        
        protected System.Web.UI.WebControls.ImageButton img_CONFMAIL; 
        protected System.Web.UI.WebControls.ImageButton img_FLY;
        protected System.Web.UI.WebControls.ImageButton img_SYS;
        //protected System.Web.UI.WebControls.ImageButton img_CONFSECDESK;
        protected System.Web.UI.WebControls.ImageButton img_PIM;
        protected System.Web.UI.WebControls.ImageButton img_SUR;
        protected System.Web.UI.WebControls.ImageButton img_ADMOPT;
        protected System.Web.UI.WebControls.ImageButton img_CONFDEF;
        protected System.Web.UI.WebControls.ImageButton img_CONFTYPE;
        protected System.Web.UI.WebControls.ImageButton img_FEAT;
        protected System.Web.UI.WebControls.ImageButton img_AUTO;
        protected System.Web.UI.WebControls.ImageButton img_NETSWT; //FB 2595
        protected System.Web.UI.WebControls.ImageButton img_ICP;//FB 2724
        //FB 2565 End
        protected System.Web.UI.WebControls.ImageButton img_WEBCONF; //ZD 100221
        protected System.Web.UI.WebControls.ImageButton img_WO; //ZD 101228

        protected System.Web.UI.HtmlControls.HtmlInputHidden hdnSurURL;
        protected System.Web.UI.HtmlControls.HtmlInputHidden hdnTimeDur;

        //FB 2348
		protected DropDownList lstEnableVMR;
        protected System.Web.UI.WebControls.Button btnSubmit; //FB 2347 //ZD 100420
        //protected System.Web.UI.HtmlControls.HtmlButton btnSubmit; //FB 2347 //ZD 100420
        protected System.Web.UI.WebControls.DropDownList lstEPinMail;//FB 2401

        protected System.Web.UI.WebControls.TextBox txtTearDownTime; //FB 2398
        protected System.Web.UI.WebControls.TextBox txtSetupTime;
        protected System.Web.UI.WebControls.TextBox txtDefaultConfDuration;//FB 2501
        protected System.Web.UI.HtmlControls.HtmlTableRow trBufferOptions;//FB 2398
        protected System.Web.UI.WebControls.DropDownList lstAcceptDecline; //FB 2419
        protected System.Web.UI.WebControls.DropDownList lstLineRate; //FB 2429

        protected System.Web.UI.WebControls.TextBox txtMCUSetupTime; //FB 2440
        protected System.Web.UI.WebControls.TextBox txtMCUTearDownTime;//FB 2440
        protected System.Web.UI.HtmlControls.HtmlTableRow trMCUBufferOptions;//FB 2440
        protected System.Web.UI.HtmlControls.HtmlTableRow trForceMCUBuffer;//FB 2440
        protected System.Web.UI.HtmlControls.HtmlTable tblForceMCUBuffer;//FB 2440
        protected DropDownList DrpForceMCUBuffer;//FB 2440
        protected System.Web.UI.WebControls.DropDownList MCUSetupDisplay; //FB 2998
        protected System.Web.UI.WebControls.DropDownList MCUTearDisplay; //FB 2998

		//FB 2469 - Starts
        protected System.Web.UI.WebControls.DropDownList lstEnableConfTZinLoc; //FB 2419
        protected System.Web.UI.WebControls.DropDownList lstSendConfirmationEmail; //FB 2419
        protected System.Web.UI.WebControls.DropDownList lstEnableSmartP2P; //FB 2430
        protected DropDownList lstMcuAlert;//FB 2472
		//FB 2486 Starts
        protected System.Web.UI.ScriptManager OrgOptionScriptManager;
        protected AjaxControlToolkit.ModalPopupExtender MessagePopup;
        protected System.Web.UI.WebControls.CheckBox chkmsg1;
        protected System.Web.UI.WebControls.CheckBox chkmsg2;
        protected System.Web.UI.WebControls.CheckBox chkmsg3;
        protected System.Web.UI.WebControls.CheckBox chkmsg4;
        protected System.Web.UI.WebControls.CheckBox chkmsg5;
        protected System.Web.UI.WebControls.CheckBox chkmsg6;
        protected System.Web.UI.WebControls.CheckBox chkmsg7;
        protected System.Web.UI.WebControls.CheckBox chkmsg8;
        protected System.Web.UI.WebControls.CheckBox chkmsg9;
        protected System.Web.UI.WebControls.DropDownList drpdownmsgduration1;
        protected System.Web.UI.WebControls.DropDownList drpdownmsgduration2;
        protected System.Web.UI.WebControls.DropDownList drpdownmsgduration3;
        protected System.Web.UI.WebControls.DropDownList drpdownmsgduration4;
        protected System.Web.UI.WebControls.DropDownList drpdownmsgduration5;
        protected System.Web.UI.WebControls.DropDownList drpdownmsgduration6;
        protected System.Web.UI.WebControls.DropDownList drpdownmsgduration7;
        protected System.Web.UI.WebControls.DropDownList drpdownmsgduration8;
        protected System.Web.UI.WebControls.DropDownList drpdownmsgduration9;
        protected System.Web.UI.WebControls.DropDownList drpdownconfmsg1;
        protected System.Web.UI.WebControls.DropDownList drpdownconfmsg2;
        protected System.Web.UI.WebControls.DropDownList drpdownconfmsg3;
        protected System.Web.UI.WebControls.DropDownList drpdownconfmsg4;
        protected System.Web.UI.WebControls.DropDownList drpdownconfmsg5;
        protected System.Web.UI.WebControls.DropDownList drpdownconfmsg6;
        protected System.Web.UI.WebControls.DropDownList drpdownconfmsg7;
        protected System.Web.UI.WebControls.DropDownList drpdownconfmsg8;
        protected System.Web.UI.WebControls.DropDownList drpdownconfmsg9;
        protected System.Web.UI.HtmlControls.HtmlTableCell tdTxtMsgDetails;
        protected System.Web.UI.HtmlControls.HtmlTableCell tdTxtMsg;
        //protected System.Web.UI.WebControls.Button btnMsgSubmit;
        protected System.Web.UI.HtmlControls.HtmlButton btnMsgSubmit;//ZD 100420
        //protected System.Web.UI.WebControls.Button btnReset;//ZD 100263//ZD 100420
        protected System.Web.UI.HtmlControls.HtmlInputButton btnReset;//ZD 100263//ZD 100420
        //FB 2486 Ends

        //FB 2550 Starts
        protected System.Web.UI.WebControls.TextBox txtMaxVMRParty;
        protected System.Web.UI.HtmlControls.HtmlContainerControl tdMaxParty;
        protected System.Web.UI.HtmlControls.HtmlContainerControl tdMaxPartyCount;
        //FB 2550 Ends
        //FB 2571 Start
        protected System.Web.UI.WebControls.DropDownList lstenableFECC;
        protected System.Web.UI.WebControls.DropDownList lstdefaultFECC;
        protected System.Web.UI.HtmlControls.HtmlTableCell DefaultFECC;//FB 2571
        protected System.Web.UI.HtmlControls.HtmlTableCell DefaultFECCoptions;//FB 2571
        //FB 2571 End
		protected System.Web.UI.WebControls.DropDownList drpBrdgeExt; //FB 2610
        protected System.Web.UI.WebControls.TextBox txtMeetandGreetBuffer;//FB 2609

        //FB 2632 Starts
        protected System.Web.UI.WebControls.DropDownList drpCngSupport; 
        protected System.Web.UI.HtmlControls.HtmlInputCheckBox chkOnSiteAVSupport;
        protected System.Web.UI.HtmlControls.HtmlInputCheckBox chkMeetandGreet;
        protected System.Web.UI.HtmlControls.HtmlInputCheckBox chkConciergeMonitoring;
        protected System.Web.UI.HtmlControls.HtmlInputCheckBox chkDedicatedVNOCOperator;
        //FB 2632 Ends
		protected int Cloud = 0;//FB 2262 //FB 2599
		protected System.Web.UI.WebControls.DropDownList lstEnableRoomAdminDetails;//FB 2631
		//FB 2637 Starts
        protected System.Web.UI.WebControls.ListBox lstTier1;
        protected System.Web.UI.HtmlControls.HtmlInputHidden hdnTierIDs;
        //FB 2637 Ends
        //FB 2595 Starts
        protected System.Web.UI.WebControls.DropDownList drpSecureSwitch;
        protected System.Web.UI.WebControls.TextBox txtHardwareAdminEmail;
        protected System.Web.UI.HtmlControls.HtmlTableCell tdsecureadminaddress;
        protected System.Web.UI.HtmlControls.HtmlTableRow trNwtSwtiching;
        protected System.Web.UI.WebControls.DropDownList drpNwtSwtiching;
        protected System.Web.UI.WebControls.DropDownList drpNwtCallLaunch;
        protected System.Web.UI.WebControls.TextBox txtSecureLaunch;
        protected System.Web.UI.HtmlControls.HtmlTableRow trNwtCallBuffer;
        //FB 2595 Ends
        //FB 2670 Starts
        protected System.Web.UI.HtmlControls.HtmlInputCheckBox chkOnsiteAV;
        protected System.Web.UI.HtmlControls.HtmlInputCheckBox chkMeetandGret;
        protected System.Web.UI.HtmlControls.HtmlInputCheckBox chkConciergeMonitor;
        protected System.Web.UI.HtmlControls.HtmlInputCheckBox chkDedicatedVNOC;
        //FB 2670 Ends
		// FB 2641 Start
        protected System.Web.UI.WebControls.DropDownList drpEnableLineRate;
        protected System.Web.UI.WebControls.DropDownList drpEnableStartMode;
        // FB 2641 End
        protected System.Web.UI.WebControls.DropDownList lstSigRoomConf;//FB 2817
        protected System.Web.UI.WebControls.TextBox txtResponseTimeout; //FB 2993
        protected System.Web.UI.HtmlControls.HtmlTableRow trNetwork;//FB 2993
        //ZD 100263
        protected System.Web.UI.WebControls.TextBox txtWhiteList;
        protected System.Web.UI.WebControls.Button btnWhiteList;// ZD 100420
        //protected System.Web.UI.HtmlControls.HtmlButton btnWhiteList;
        protected System.Web.UI.WebControls.ListBox lstWhiteList;//ZD 100420
        //protected System.Web.UI.HtmlControls.HtmlSelect lstWhiteList;//ZD 100420
        protected System.Web.UI.HtmlControls.HtmlInputHidden hdnFileWhiteList;
        protected System.Web.UI.WebControls.CheckBox chkWhiteList;
        protected System.Web.UI.HtmlControls.HtmlTableRow trWhiteList;
		protected System.Web.UI.WebControls.DropDownList lstVMRTopTier;//ZD 100068
        protected System.Web.UI.WebControls.DropDownList lstVMRMiddleTier;//ZD 100068

		protected System.Web.UI.WebControls.DropDownList DrpDwnEnableCA; //ZD 100151
        //protected System.Web.UI.HtmlControls.HtmlTableRow trShwCusAttr; //ZD 100151 //ZD 101549 
        protected System.Web.UI.HtmlControls.HtmlTableCell tdlblCustOpt;//ZD 101549
        protected System.Web.UI.HtmlControls.HtmlTableCell tdDrpCustOpt; //ZD 101549 
		//ZD 100164 START
        protected DropDownList DrpDwnAdvUserOption;
        protected int enableCloudInstallation = 0;
        protected System.Web.UI.HtmlControls.HtmlTableCell tdEnableDisableadvanceduser;
        protected System.Web.UI.HtmlControls.HtmlTableCell tdDrpDwnAdvUserOption;
        //ZD 100164 END
        protected System.Web.UI.HtmlControls.HtmlTableRow trRecurrence;//ZD 100518
        protected System.Web.UI.HtmlControls.HtmlTableCell trBuffer;//ZD 100518
        //ZD 100707 Start
        protected DropDownList drpEnableVMR;
        protected DropDownList drpEnableRoomVMR;
        protected DropDownList drpEnableExternalVMR;
        protected DropDownList drpEnablePersonaVMR;
        protected System.Web.UI.HtmlControls.HtmlTableCell tdPersonalVMR;
        protected System.Web.UI.HtmlControls.HtmlTableCell tddrpEnablePersonaVMR;
        protected System.Web.UI.HtmlControls.HtmlTableRow trEnableRoomExternalVRM;
        //ZD 100707 End
		//ZD 100221 Starts
        protected System.Web.UI.WebControls.TextBox txtWebURL;
        protected System.Web.UI.WebControls.TextBox txtSiteID;
        protected System.Web.UI.WebControls.TextBox txtPartnrID;
        protected System.Web.UI.HtmlControls.HtmlTableRow trWebConf; 
        //ZD 100221 Ends

        protected System.Web.UI.WebControls.DropDownList drpEnableExpressConfType;//ZD 100704

		 //ZD 100935 STarts
        protected System.Web.UI.HtmlControls.HtmlTableCell tdWebExIntg;
        protected System.Web.UI.WebControls.DropDownList drpWebExIng;
        protected System.Web.UI.HtmlControls.HtmlTableRow trEnbWebIntg; //ZD 103782

        //ZD 100935 Ends
		protected System.Web.UI.WebControls.TextBox txtScheduleLimit;//ZD 100899        
		//ZD 100781 Starts
        protected System.Web.UI.WebControls.DropDownList DrpEnablePwdExp;
        protected System.Web.UI.WebControls.TextBox txtPwdExpDays;
        protected System.Web.UI.HtmlControls.HtmlTableCell lblPwdExp;
        protected System.Web.UI.HtmlControls.HtmlTableCell tdDrpPwdExp;
        //ZD 100781 Ends
		protected System.Web.UI.WebControls.DropDownList drpRoomCalView; //ZD 100963
        
        //ZD 101019 START
        protected System.Web.UI.WebControls.TextBox txtVideoSourceURL1;
        protected System.Web.UI.WebControls.TextBox txtVideoSourceURL2;
        protected System.Web.UI.WebControls.TextBox txtVideoSourceURL3;
        protected System.Web.UI.WebControls.TextBox txtVideoSourceURL4;

        protected System.Web.UI.HtmlControls.HtmlInputCheckBox chkPosA1;
        protected System.Web.UI.HtmlControls.HtmlInputCheckBox chkPosB1;
        protected System.Web.UI.HtmlControls.HtmlInputCheckBox chkPosC1;
        protected System.Web.UI.HtmlControls.HtmlInputCheckBox chkPosD1;

        protected System.Web.UI.HtmlControls.HtmlInputCheckBox chkPosA2;
        protected System.Web.UI.HtmlControls.HtmlInputCheckBox chkPosB2;
        protected System.Web.UI.HtmlControls.HtmlInputCheckBox chkPosC2;
        protected System.Web.UI.HtmlControls.HtmlInputCheckBox chkPosD2;

        protected System.Web.UI.HtmlControls.HtmlInputCheckBox chkPosA3;
        protected System.Web.UI.HtmlControls.HtmlInputCheckBox chkPosB3;
        protected System.Web.UI.HtmlControls.HtmlInputCheckBox chkPosC3;
        protected System.Web.UI.HtmlControls.HtmlInputCheckBox chkPosD3;

        protected System.Web.UI.HtmlControls.HtmlInputCheckBox chkPosA4;
        protected System.Web.UI.HtmlControls.HtmlInputCheckBox chkPosB4;
        protected System.Web.UI.HtmlControls.HtmlInputCheckBox chkPosC4;
        protected System.Web.UI.HtmlControls.HtmlInputCheckBox chkPosD4;

        protected System.Web.UI.HtmlControls.HtmlInputHidden chkPosAVert;
        protected System.Web.UI.HtmlControls.HtmlInputHidden chkPosBVert;
        protected System.Web.UI.HtmlControls.HtmlInputHidden chkPosCVert;
        protected System.Web.UI.HtmlControls.HtmlInputHidden chkPosDVert;

        protected System.Web.UI.HtmlControls.HtmlInputHidden chkPosAHor;
        protected System.Web.UI.HtmlControls.HtmlInputHidden chkPosBHor;
        protected System.Web.UI.HtmlControls.HtmlInputHidden chkPosCHor;
        protected System.Web.UI.HtmlControls.HtmlInputHidden chkPosDHor;
        //ZD 101019 END
        protected System.Web.UI.WebControls.DropDownList drpDetailedExpressForm; //ZD 100834
        //ZD 101120 Starts
        protected System.Web.UI.WebControls.DropDownList drpGLAppTime;
        protected System.Web.UI.WebControls.DropDownList drpWarningMsg;
        protected System.Web.UI.WebControls.DropDownList drpSmartP2PNotify;//ZD 100815
        protected System.Web.UI.HtmlControls.HtmlGenericControl lblNotify; //ZD 100815
        //ZD 101120 Ends
        protected System.Web.UI.WebControls.DropDownList drpVMRPIN;//ZD 100522
        protected System.Web.UI.WebControls.DropDownList DrpRoomDenial;//ZD 101445
        protected System.Web.UI.WebControls.TextBox txtVMRPW; //ZD 100522
        //ZD 101228 Starts
        protected System.Web.UI.WebControls.TextBox txtFacWOAlert;
        protected System.Web.UI.WebControls.TextBox txtCatWOAlert;
        protected System.Web.UI.WebControls.TextBox txtAVWOAlert;
        protected System.Web.UI.HtmlControls.HtmlTableRow trAVWOAlert;
        protected System.Web.UI.HtmlControls.HtmlTableRow trCatWOALert;
        protected System.Web.UI.HtmlControls.HtmlTableRow trFacWOALert;
        protected System.Web.UI.HtmlControls.HtmlTableRow trWOOptions;
        //ZD 101228 Ends

        //ZD 100513 Starts
        protected System.Web.UI.WebControls.DropDownList drpWebExMode;
        protected System.Web.UI.WebControls.DropDownList drpOBTPConf;
        protected System.Web.UI.HtmlControls.HtmlTableCell tdWebEX;
        //ZD 100513 Ends

        protected System.Web.UI.WebControls.DropDownList drptempbook;//ZD 101562
        //ZD 101755 start
        protected System.Web.UI.WebControls.DropDownList DrpSetupTimeDisplay;
        protected System.Web.UI.WebControls.DropDownList DrpTeardownTimeDisplay;
        //ZD 101755 End
		//ZD 101757
        protected System.Web.UI.WebControls.DropDownList drpActiveMsgDel;
        protected System.Web.UI.HtmlControls.HtmlTableCell tdTextmsg;
        protected System.Web.UI.HtmlControls.HtmlTableCell tdMsgPopup;
        protected System.Web.UI.HtmlControls.HtmlTableCell tdActMsgDel;
        protected System.Web.UI.HtmlControls.HtmlTableCell tdCtlActMsgDel;
		//ZD 101869 start
        protected System.Web.UI.WebControls.Image imgLayoutMapping5;
        protected System.Web.UI.WebControls.Image imgLayoutMapping6;
        protected System.Web.UI.WebControls.Image imgLayoutMapping7;
        protected System.Web.UI.WebControls.Image imgLayoutMapping8;
        protected System.Web.UI.HtmlControls.HtmlInputHidden hdnCodian;
        protected System.Web.UI.WebControls.Label lblCodianLO;
        //ZD 101869 End
        
        protected System.Web.UI.WebControls.DropDownList DrpShowvideoLayout;  //ZD 101931
        protected System.Web.UI.WebControls.TextBox txtMultipleAssistant; //ZD 102085
        protected System.Web.UI.WebControls.DropDownList drpCalDefaultDisplay;  //ZD 102356
        protected System.Web.UI.WebControls.CheckBox chkEnableWaitList;//ZD 102532
        protected System.Web.UI.WebControls.ListBox lstPersonnelAlert;//ZD 103046
        protected System.Web.UI.HtmlControls.HtmlInputHidden hdnPersonnelAlert;//ZD 103046
        protected System.Web.UI.WebControls.TextBox txtVideoRefreshTimer;//ZD 103398
        //ZD 102514 Start
        protected System.Web.UI.WebControls.TextBox txtOnSiteAVSupportBuffer;
        protected System.Web.UI.WebControls.TextBox txtCallMonitoringBuffer;
        protected System.Web.UI.WebControls.TextBox txtDedicatedVNOCOperatorBuffer;
        //ZD 102514 End
        protected System.Web.UI.WebControls.DropDownList TravelAvodTrck; // ZD 103216
        // ZD 103550 - Start
        protected System.Web.UI.WebControls.DropDownList DrpEnableBJNIntegration;        
        protected System.Web.UI.WebControls.RadioButtonList RdBJNMeetingID;        
        protected System.Web.UI.WebControls.CheckBox ChkSelectable;
        protected System.Web.UI.HtmlControls.HtmlTableRow trBJNMeetingID;
        protected System.Web.UI.HtmlControls.HtmlTableCell tdEnableBJNInt;
        // ZD 103550 - End
        protected System.Web.UI.WebControls.DropDownList DrpBJNDisplay;//ZD 104116
        protected System.Web.UI.HtmlControls.HtmlGenericControl spnBJNDisplay, lblPxipDisplay;//ZD 104116 //ALLDEV-782
        //ZD 104151 Start
        protected System.Web.UI.WebControls.DropDownList lstEmptyconf; 
        // ZD 104151 End
		protected System.Web.UI.WebControls.DropDownList lstEnablePoolOrderSelection;//ZD 104221
        protected System.Web.UI.WebControls.DropDownList DrpAssignParttoRoom; //ZD 102916
        protected System.Web.UI.WebControls.DropDownList drpEnableAudbridgefreebusy; //ZD 104854-Disney 
        protected System.Web.UI.WebControls.DropDownList DrpEnBlkusrDataImport;//ZD 104862
        protected System.Web.UI.WebControls.DropDownList DrpDwnSendRoomResourceiCal;//ALLDEV-828
        protected DropDownList DrpEnableiControlLogin; //ALLDEV-503
        protected DropDownList DrpEnableFindAvailRm; //ALLDEV-503
		protected DropDownList drpEnableHostandGuestPwd;    //ALLDEV-826
        //ALLDEV-837 Starts
        protected DropDownList drpDwnEnableSyncLaunchBuffer;
        protected System.Web.UI.WebControls.TextBox txtLaunchBuffer;
        protected System.Web.UI.HtmlControls.HtmlTableCell tdLaunchBuffer;
        protected System.Web.UI.HtmlControls.HtmlTableCell tdTxtLaunchBuffer;
        //ALLDEV-837 Ends
		protected System.Web.UI.WebControls.TextBox txtConfAdmin;   //ALLDEV-498
        protected System.Web.UI.WebControls.TextBox txtAdminID;   //ALLDEV-498
		protected System.Web.UI.WebControls.DropDownList DrpAllowReqtoEdit;//ALLDEV-839
		protected System.Web.UI.WebControls.DropDownList DrpDwnEnableRoomSelection; //ALLDEV-834 
		protected System.Web.UI.WebControls.DropDownList DrpEWSPartyFreeBusy; //ALLDEV-833       
        //ALLDEV-782 - Start
        protected System.Web.UI.WebControls.DropDownList drpPexipInt, drpPexipDisplay;
        protected System.Web.UI.WebControls.RadioButtonList rdPexipMtg;
        protected System.Web.UI.WebControls.CheckBox chkPexipSelect;
        protected System.Web.UI.HtmlControls.HtmlTableCell trPexipSel, lblPexipMtgt, PexipAtt;
        //ALLDEV-782 - End
        protected System.Web.UI.WebControls.DropDownList drpEnableS4BEWS; //ALLDEV-862
        #endregion

        #region Private Members
        //string companyinfofile = "C:\\VRMSchemas_v18\\company.ifo";
        ns_Logger.Logger log;
        String tformat = "hh:mm tt";
        Int32 orgId = 11;
        //FB 1926 start
        Int32 wkly = 8;
        Int32 dly = 4;
        Int32 hourly = 2;
        Int32 minutely = 1;
        //FB 1926 end
        protected string language = ""; //FB 2335
        protected int languageid = 1; //FB 2486
        string isZuluChanged = ""; //FB 2588
        //protected int enableCloudInstallation = 0; //ZD 100166
		int WebExUserLimit = 0; //ZD 100221
        MyVRMNet.Util utilObj; //ZD 104391
        #endregion

        myVRMNet.NETFunctions obj;
        public MainAdministrator()
        {
            obj = new myVRMNet.NETFunctions();
            log = new ns_Logger.Logger();
            utilObj = new MyVRMNet.Util();  //ZD 104391
        }

        //ZD 101022
        #region InitializeCulture
        protected override void InitializeCulture()
        {
            if (Session["UserCulture"] != null)
            {
                UICulture = Session["UserCulture"].ToString();
                Culture = Session["UserCulture"].ToString();
                //ZD 101714
                if (Session["UserCulture"].ToString() == "fr-CA")
                    Culture = "en-US";
                base.InitializeCulture();
            }
        }
        #endregion

        #region Methods Executed on Page Load

        private void Page_Load(object sender, System.EventArgs e)
        {
            try
            {
                if (obj == null)
                    obj = new myVRMNet.NETFunctions();
                obj.AccessandURLConformityCheck("mainadministrator.aspx", Request.Url.AbsoluteUri.ToLower()); // ZD 100263
                Session["multisiloOrganizationID"] = null; //FB 2274 
                if (Application["client"].ToString().ToUpper() == "DISNEY")//FB 1985
                {
                    lstDefaultConferenceType.Enabled = false;
                    lstEnableAudioOnlyConference.Enabled = false;
                    //p2pConfEnabled.Enabled = false; //p2p for exchange
                    tblForceMCUBuffer.Attributes.Add("Style", "Display:block");//FB 2440
                }

                //FB 2426 Start //ZD 103522
                //if (Session["GuestRooms"].ToString() == "0")
                //    trOnfly.Attributes.Add("style", "display:none");
                //FB 2426 End

                //FB 2335
                if (Session["language"] == null)
                    Session["language"] = "en";
                if (Session["language"].ToString() != "")
                    language = Session["language"].ToString();

                if (Session["languageID"] == null)
                    Session["languageID"] = "1";
                if (Session["languageID"].ToString() != "")
                    Int32.TryParse(Session["languageID"].ToString(), out languageid);

                //FB 2262,//FB 2599 - Starts
                if (Session["Cloud"] != null)
                {
                    if (Session["Cloud"].ToString() != "")
                        Int32.TryParse(Session["Cloud"].ToString(), out Cloud);
                }

                //ZD 100151 //ZD 101549 - Start
                if (Session["EnableEntity"].ToString() == "1")
                {
                    //trShwCusAttr.Visible = true;
                    tdlblCustOpt.Visible = true;
                    tdDrpCustOpt.Visible = true;
                }
                else
                {
                    //trShwCusAttr.Visible = false;
                    tdlblCustOpt.Visible = false;
                    tdDrpCustOpt.Visible = false;
                }
                //ZD 101549 - End

                //FB 2717 Starts
                if (Cloud == 1)
                {
                    lstEnableVMR.Items.Remove(lstEnableVMR.Items.FindByValue("0"));
                    lstEnableAudioVideoConference.Enabled = false;
					DrpDwnListAssignedMCU.Enabled = false;
                    //lstEnableAudioBridges.SelectedValue = "0";
                    //lstEnableAudioBridges.Enabled = false;
                    //lstDefaultConferenceType.Enabled = false;
                    //lstEnableVMR.Enabled = false;
                    //lstEnableVMR.SelectedValue = "3";
                    //ZD 100753 Starts
                    drpEnableVMR.Enabled = false;
                    drpEnableVMR.SelectedValue = "1";
                    drpEnableExternalVMR.Enabled = false;
                    drpEnableExternalVMR.SelectedValue = "1";
                    //ZD 100753 Ends
                }
                //FB 2262,//FB 2599 - Ends
                //FB 2717 End

				 //ZD 100221 Starts
                if (Session["WebexUserLimit"] != null)
                {
                    if (Session["WebexUserLimit"].ToString() != "")
                        int.TryParse(Session["WebexUserLimit"].ToString(), out WebExUserLimit);
                }
                if (WebExUserLimit > 0)
                {
                    trWebConf.Attributes.Add("style", "display:");
                    trEnbWebIntg.Attributes.Add("style", "display:");
                    drpWebExIng.Visible = true; //ZD 100935
                    //tdWebEX.Attributes.Add("style", "display:"); //ZD 100513
                    drpWebExMode.Visible = true; //ZD 100513

                }
                else
                {
                    trWebConf.Attributes.Add("style", "display:none");
                    trEnbWebIntg.Attributes.Add("style", "display:none");
                    drpWebExIng.Visible = false; //ZD 100935
                    tdWebEX.Attributes.Add("style", "display:none"); //ZD 100513
                    drpWebExMode.Visible = false; //ZD 100513
                }
                //ZD 100221 Ends

                //ZD 100166 Starts
                if (Session["EnableCloudInstallation"] != null)
                    int.TryParse(Session["EnableCloudInstallation"].ToString(), out enableCloudInstallation);

                //ZD 100518 Starts
                if (enableCloudInstallation == 1)
                {
                    //ZD 101543
                    /*txtMCUSetupTime.Text = "0";
                    txtMCUTearDownTime.Text = "0";
                    trMCUBufferOptions.Visible = false;
                    EnableBufferZone.SelectedValue = "0";
                    EnableBufferZone.Visible = false;
                    trBuffer.Visible = false;
                    MCUSetupDisplay.Enabled = false;
                    MCUTearDisplay.Enabled = false;*/
                    trRecurrence.Visible = false; //ZD 100518
                }
                //ZD 100518 End
                //ZD 100166 End

                Session["timeFormat"] = ((Session["timeFormat"] == null) ? "1" : Session["timeFormat"]);
                Application["interval"] = ((Application["interval"] == null) ? "60" : Application["interval"]);
                tformat = (Session["timeFormat"].ToString().Equals("0") ? "HH:mm" : "hh:mm tt");

                //FB 2588 Starts
                if (Session["timeFormat"].ToString().Equals("2"))
                    tformat = "HHmmZ"; 
                //FB 2588 Ends

                //ZD 100381
                if (Session["timeFormat"] != null)
                    if (Session["timeFormat"].ToString().Equals("0"))
                    {
                        regSysStartTime.ValidationExpression = "[0-2][0-9]:[0-5][0-9]";
                        regSysStartTime.ErrorMessage = obj.GetTranslatedText("Invalid Time (HH:mm)");
                        regSysEndTime.ValidationExpression = "[0-2][0-9]:[0-5][0-9]";
                        regSysEndTime.ErrorMessage = obj.GetTranslatedText("Invalid Time (HH:mm)");                        
                    }
                    else if (Session["timeFormat"].ToString().Equals("2"))
                    {
                        regSysStartTime.ValidationExpression = "[0-2][0-9][0-5][0-9][Z|z]";
                        regSysStartTime.ErrorMessage = obj.GetTranslatedText("Invalid Time (HHmmZ)");
                        regSysEndTime.ValidationExpression = "[0-2][0-9][0-5][0-9][Z|z]";
                        regSysEndTime.ErrorMessage = obj.GetTranslatedText("Invalid Time (HHmmZ)");
                    }

                systemStartTime.Items.Clear();
                systemEndTime.Items.Clear();
                obj.BindTimeToListBox(systemStartTime, false, false);
                obj.BindTimeToListBox(systemEndTime, false, false);
                if (lstLineRate.Items.Count <= 0)
                    obj.BindLineRate(lstLineRate); //FB 2429
                //FB 2348 start FB 2842 Start  ZD 100419 start
                img_USROPT.Attributes.Add("onclick", "javascript:ExpandCollapse(this,'" + trUSROPT.ClientID + "', this.className);return false;");
                img_ROOM.Attributes.Add("onclick", "javascript:ExpandCollapse(this,'" + trROOM.ClientID + "', this.className);return false;");
                img_CONFOPT.Attributes.Add("onclick", "javascript:ExpandCollapse(this,'" + trCONFOPT.ClientID + "', this.className);return false;");
                img_AUD.Attributes.Add("onclick", "javascript:ExpandCollapse(this,'" + trAUD.ClientID + "', this.className);return false;");
                img_ADMOPT.Attributes.Add("onclick", "javascript:ExpandCollapse(this,'" + trADMOPT.ClientID + "', this.className);return false;");
                img_EPT.Attributes.Add("onclick", "javascript:ExpandCollapse(this,'" + trEPT.ClientID + "', this.className);return false;");
                img_RoomSync.Attributes.Add("onclick", "javascript:ExpandCollapse(this,'" + trRoomSync.ClientID + "', this.className);return false;");                
                img_PIM.Attributes.Add("onclick", "javascript:ExpandCollapse(this,'" + trPIM.ClientID + "', this.className);return false;");
                img_SUR.Attributes.Add("onclick", "javascript:ExpandCollapse(this,'" + trSUR.ClientID + "', this.className);return false;");
                img_SYS.Attributes.Add("onclick", "javascript:ExpandCollapse(this,'" + trSYS.ClientID + "', this.className);return false;");
                img_FLY.Attributes.Add("onclick", "javascript:ExpandCollapse(this,'" + trFLY.ClientID + "', this.className);return false;");//FB 2426
                img_CONFMAIL.Attributes.Add("onclick", "javascript:ExpandCollapse(this,'" + trCONFMAIL.ClientID + "', this.className);return false;");
                //img_CONFSECDESK.Attributes.Add("onclick", "javascript:ExpandCollapse(this,'" + trCONFSECDESK.ClientID + "', this.className);return false;");
                img_CONFDEF.Attributes.Add("onclick", "javascript:ExpandCollapse(this,'" + trCONFDEF.ClientID + "', this.className);return false;");
                img_CONFTYPE.Attributes.Add("onclick", "javascript:ExpandCollapse(this,'" + trCONFTYPE.ClientID + "', this.className);return false;");
                img_FEAT.Attributes.Add("onclick", "javascript:ExpandCollapse(this,'" + trFEAT.ClientID + "', this.className);return false;");
                img_AUTO.Attributes.Add("onclick", "javascript:ExpandCollapse(this,'" + trAUTO.ClientID + "', this.className);return false;");
                img_NETSWT.Attributes.Add("onclick", "javascript:ExpandCollapse(this,'" + trNETSWT.ClientID + "', this.className);return false;");
                img_ICP.Attributes.Add("onclick", "javascript:ExpandCollapse(this,'" + trICP.ClientID + "', this.className);return false;");//FB 2724
                //FB 2348 End FB 2842  End ZD 100419 End
                img_WEBCONF.Attributes.Add("onclick", "javascript:ExpandCollapse(this,'" + trWebCre.ClientID + "', this.className);return false;"); //ZD 100221
                img_WO.Attributes.Add("onclick", "javascript:ExpandCollapse(this,'" + trWO.ClientID + "', this.className);return false;"); //ZD 101228
                //FB 2486 Starts
                #region DropdownMsg

                if (!IsPostBack)
                {

                    String inxml = "<GetConfMsg>" + obj.OrgXMLElement() + "<UserID>" + Session["userID"].ToString() + "</UserID><language>1</language></GetConfMsg>";
                    String outxml = obj.CallMyVRMServer("GetConfMsg", inxml, HttpContext.Current.Application["MyVRMServer_ConfigPath"].ToString());

                    XmlDocument xmldoc = new XmlDocument();
                    xmldoc.LoadXml(outxml);
                    XmlNodeList nodes = xmldoc.SelectNodes("//GetConfMsgList/GetConfMsg");

                    obj.LoadOrgList(drpdownconfmsg1, nodes, "ConfMsgID", "ConfMsg");
                    obj.LoadOrgList(drpdownconfmsg2, nodes, "ConfMsgID", "ConfMsg");
                    obj.LoadOrgList(drpdownconfmsg3, nodes, "ConfMsgID", "ConfMsg");
                    obj.LoadOrgList(drpdownconfmsg4, nodes, "ConfMsgID", "ConfMsg");
                    obj.LoadOrgList(drpdownconfmsg5, nodes, "ConfMsgID", "ConfMsg");
                    obj.LoadOrgList(drpdownconfmsg6, nodes, "ConfMsgID", "ConfMsg");
                    obj.LoadOrgList(drpdownconfmsg7, nodes, "ConfMsgID", "ConfMsg");
                    obj.LoadOrgList(drpdownconfmsg8, nodes, "ConfMsgID", "ConfMsg");
                    obj.LoadOrgList(drpdownconfmsg9, nodes, "ConfMsgID", "ConfMsg");

                    obj.SetToolTip(drpdownconfmsg1);
                    obj.SetToolTip(drpdownconfmsg2);
                    obj.SetToolTip(drpdownconfmsg3);
                    obj.SetToolTip(drpdownconfmsg4);
                    obj.SetToolTip(drpdownconfmsg5);
                    obj.SetToolTip(drpdownconfmsg6);
                    obj.SetToolTip(drpdownconfmsg7);
                    obj.SetToolTip(drpdownconfmsg8);
                    obj.SetToolTip(drpdownconfmsg9);

                    ArrayList min5 = new ArrayList();
                    ArrayList min2 = new ArrayList();
                    ArrayList minS = new ArrayList();
                    Int32 ii = 0;
                    for (Int32 m = 5; m <= 30; m = m + 5)
                        min5.Insert(ii++, m + "M");

                    drpdownmsgduration1.DataSource = min5;
                    drpdownmsgduration1.DataBind();

                    drpdownmsgduration4.DataSource = min5;
                    drpdownmsgduration4.DataBind();

                    drpdownmsgduration7.DataSource = min5;
                    drpdownmsgduration7.DataBind();

                    ii = 0;
                    for (Int32 m = 2; m <= 20; m = m + 2)
                        min2.Insert(ii++, m + "M");

                    drpdownmsgduration2.DataSource = min2;
                    drpdownmsgduration2.DataBind();

                    drpdownmsgduration5.DataSource = min2;
                    drpdownmsgduration5.DataBind();

                    drpdownmsgduration8.DataSource = min2;
                    drpdownmsgduration8.DataBind();
                    
                    ii = 0;
                    for (Int32 m = 30; m <= 120; m = m + 30)
                        minS.Insert(ii++, m + "s");

                    drpdownmsgduration3.DataSource = minS;
                    drpdownmsgduration3.DataBind();

                    drpdownmsgduration6.DataSource = minS;
                    drpdownmsgduration6.DataBind();

                    drpdownmsgduration9.DataSource = minS;
                    drpdownmsgduration9.DataBind();
                }

                #endregion
                //FB 2486 Ends
                //FB 2670
                if (Session["admin"].ToString() == "3")
                {
                    
                    //btnSubmit.ForeColor = System.Drawing.Color.Gray; // FB 2796
                    
                    //btnMsgSubmit.ForeColor = System.Drawing.Color.Gray; // FB 2796
                    //btnSubmit.Attributes.Add("Class", "btndisable");// FB 2796
                    //btnMsgSubmit.Attributes.Add("Class", "btndisable");// FB 2796
                    //ZD 100263 start
                    btnReset.Visible = false;
                    btnSubmit.Visible = false;
                    btnMsgSubmit.Visible = false;
                    //ZD 100263 End
                    //ZD 101527 Starts
                    btnPoll.Visible = false;
                    trSyncPoll.Visible = false;
                    //ZD 101527 Ends
                }
                // FB 2796 Start
                else
                {
                    btnSubmit.Attributes.Add("Class", "altMedium0BlueButtonFormat");
                    btnMsgSubmit.Attributes.Add("Class", "altMedium0BlueButtonFormat");
                }
                // FB 2796 End
                //ZD 101869 start
                    imgLayoutMapping5.Attributes.Add("style", "display:none");
                    imgLayoutMapping6.Attributes.Add("style", "display:none");
                    imgLayoutMapping7.Attributes.Add("style", "display:none");
                    imgLayoutMapping8.Attributes.Add("style", "display:none");
                //ZD 101869 End
                txtConfAdmin.Attributes.Add("readonly", "");   //ALLDEV-498
                if (!IsPostBack)
                {
                    
                   // btnSubmit.Attributes.Add("onclick", "javascript:ChangeValidator();");//FB 2347//ZD 100420// ZD 103416

                    string inXML = "<GetOrgOptions><UserID>" + Session["userID"] + "</UserID>"+ obj.OrgXMLElement() +"</GetOrgOptions>";
                    string outXML = obj.CallMyVRMServer("GetOrgOptions", inXML, Application["MyVRMServer_ConfigPath"].ToString());
                    BindData(outXML);
                }
                if (Request.QueryString["m"] != null) 
                    if (Request.QueryString["m"].ToString().Equals("2"))
                    {
                        errLabel.Text = obj.GetTranslatedText("Operation Successful! Please logout and re-login to see the changes.");
                        errLabel.Visible = true;
                    }
                    else  if (Request.QueryString["m"].ToString().Equals("1"))
                    {
                        errLabel.Text = obj.GetTranslatedText("Operation Successful!");
                        errLabel.Visible = true;
                    }
                //FB 2993 Start
                if (Session["EnableNetworkFeatures"].ToString() != null)
                {
                    if (Session["EnableNetworkFeatures"].ToString().Equals("1"))
                    {
                        trNetwork.Attributes.Add("style", "display:");
                    }
                    else
                    {
                        trNETSWT.Attributes.Add("style", "display:none");
                        trNetwork.Attributes.Add("style", "display:none");
                        
                    }
                }
                //FB 2993 End

                //ZD 100164 START
                if (Session["EnableCloudInstallation"] != null)
                    int.TryParse(Session["EnableCloudInstallation"].ToString(), out enableCloudInstallation);
                if (enableCloudInstallation == 1)
                {
                    tdEnableDisableadvanceduser.Attributes.Add("style", "display:");
                    tdDrpDwnAdvUserOption.Attributes.Add("style", "display:");
                }
                else
                {
                    tdEnableDisableadvanceduser.Attributes.Add("style", "display:none");
                    tdDrpDwnAdvUserOption.Attributes.Add("style", "display:none");

                }
                //ZD 100164 END
                //ZD 100815 Starts
                if (lstEnableSmartP2P.SelectedValue == "0")
                {
                    drpSmartP2PNotify.Attributes.Add("style", "display:none");
                    lblNotify.Attributes.Add("style", "display:none");
                }
                //ZD 100815 Ends
                //ZD 101228 Starts
                if (Session["foodModule"].ToString() != null || Session["hkModule"].ToString() != null || Session["roomModule"].ToString() != null)
                {
                    if (Session["foodModule"].ToString() == "0" && Session["hkModule"].ToString() == "0" && Session["roomModule"].ToString() == "0")
                    {
                        trWOOptions.Attributes.Add("style", "display:none");
                    }
                    if (Session["roomModule"].ToString() == "0")
                        trAVWOAlert.Attributes.Add("style", "display:none");
                    if (Session["foodModule"].ToString() == "0")
                        trCatWOALert.Attributes.Add("style", "display:none");
                    if (Session["hkModule"].ToString() == "0")
                        trFacWOALert.Attributes.Add("style", "display:none");
                }
                //ZD 101228 Starts

                //ZD 103550
                if (Session["EnableBlueJeans"] != null && Session["EnableBlueJeans"].ToString() == "1")
                {
                    tdEnableBJNInt.Attributes.Add("style", "display:''");
                    DrpEnableBJNIntegration.Attributes.Add("style", "display:''");
                }
                else
                {
                    tdEnableBJNInt.Attributes.Add("style", "display:none");
                    DrpEnableBJNIntegration.Attributes.Add("style", "display:none");
                    DrpEnableBJNIntegration.SelectedValue = "0";
                }
				//ZD 104116 - Start
                if (DrpEnableBJNIntegration.SelectedValue == "1")
                {
                    trBJNMeetingID.Attributes.Add("style", "display:''");
                    spnBJNDisplay.Attributes.Add("style", "display:''");
                    DrpBJNDisplay.Attributes.Add("style", "display:''");
                }
                else
                {
                    trBJNMeetingID.Attributes.Add("style", "display:none");
                    spnBJNDisplay.Attributes.Add("style", "display:none");
                    DrpBJNDisplay.Attributes.Add("style", "display:none");
                }
				//ZD 104116 - End

                //ALLDEV-782 Starts
                if (drpPexipInt.SelectedValue == "1")
                {
                    drpPexipDisplay.Attributes.Add("style", "display:''");
                    lblPexipMtgt.Attributes.Add("style", "display:''");
                    PexipAtt.Attributes.Add("style", "display:''");
                    lblPxipDisplay.Attributes.Add("style", "display:''");
                }
                else
                {
                    drpPexipDisplay.Attributes.Add("style", "display:none");
                    lblPexipMtgt.Attributes.Add("style", "display:none");
                    PexipAtt.Attributes.Add("style", "display:none");
                    lblPxipDisplay.Attributes.Add("style", "display:none");
                }
                //ALLDEV-782 Ends


            }
            catch (Exception ex)
            {
                log.Trace("PageLoad: " + ex.Message + " : " + ex.StackTrace);
            }
        }


        private void BindData(String outXML)
        {
            string tmpstr = "";
            string[] tmpstrs;
            int i, tmpint;
            XmlDocument xmldoc = new XmlDocument();
            XmlNodeList nodelist = null;
            XmlNode node = (XmlNode)xmldoc.DocumentElement;

            try
            {
                xmldoc.LoadXml(outXML);
                log.Trace("GetOrgOptions: " + outXML);
                tmpstr = xmldoc.SelectSingleNode("//GetOrgOptions/TimezoneSystemID").InnerText;
                nodelist = xmldoc.SelectNodes("//GetOrgOptions/TimezoneSystems/TimezoneSystem");

                TimezoneSystems.Items.Clear();
                for (i = 0; i < nodelist.Count; i++)
                    TimezoneSystems.Items.Add(new ListItem(obj.GetTranslatedText(nodelist.Item(i).SelectSingleNode("Name").InnerText), nodelist.Item(i).SelectSingleNode("ID").InnerText));
                try
                {
                    TimezoneSystems.ClearSelection();
                    TimezoneSystems.Items.FindByValue(tmpstr).Selected = true;
                }
                catch (Exception ex)
                {
                    log.Trace("Timezone Type: " + ex.Message);
                }

                //Auto Accept modified conferences
                try
                {
                    AutoAcpModConf.ClearSelection();
                    AutoAcpModConf.Items.FindByValue(xmldoc.SelectSingleNode("//GetOrgOptions/AutoAcceptModifiedConference").InnerText).Selected = true;
                }
                catch (Exception ex) { log.Trace("Autoacceptmodifiedconference: " + ex.Message); }

                //Recur Enabled
                try
                {
                    RecurEnabled.ClearSelection();
                    RecurEnabled.Items.FindByValue(xmldoc.SelectSingleNode("//GetOrgOptions/EnableRecurringConference").InnerText).Selected = true;
                }
                catch (Exception ex) { log.Trace("RecurEnabled: " + ex.Message); }

                // Dialout enabled
                try
                {
                    DialoutEnabled.ClearSelection();
                    DialoutEnabled.Items.FindByValue(xmldoc.SelectSingleNode("//GetOrgOptions/EnableDialout").InnerText).Selected = true;
                }
                catch (Exception ex) { log.Trace("DialoutEnabled: " + ex.Message + " : " + ex.StackTrace); }

                //EnableDynamicInvite
                try
                {
                    DynamicInviteEnabled.ClearSelection();
                    DynamicInviteEnabled.Items.FindByValue(xmldoc.SelectSingleNode("//GetOrgOptions/EnableDynamicInvite").InnerText).Selected = true;
                }
                catch (Exception ex) { log.Trace("EnableDynamicInvite: " + ex.Message); }

                // EnableP2PConference
                try
                {
                    p2pConfEnabled.ClearSelection();
                    //if (Application["client"].ToString().ToUpper() != "DISNEY")//FB 1985 //p2p for exchange
                    if(p2pConfEnabled.Items.FindByValue(xmldoc.SelectSingleNode("//GetOrgOptions/EnableP2PConference").InnerText) != null)
                        p2pConfEnabled.Items.FindByValue(xmldoc.SelectSingleNode("//GetOrgOptions/EnableP2PConference").InnerText).Selected = true;
                    //else //p2p for exchange
                    //   p2pConfEnabled.SelectedIndex = 2; //p2p for exchange
                }
                catch (Exception ex) { log.Trace("EnableP2PConference: " + ex.Message); }
                // EnableRealtimeDisplay
                try
                {
                    RealtimeType.ClearSelection();
                    RealtimeType.Items.FindByValue(xmldoc.SelectSingleNode("//GetOrgOptions/EnableRealtimeDisplay").InnerText).Selected = true;
                }
                catch (Exception ex) { log.Trace("EnableRealtimeDisplay: " + ex.Message); }
                // DefaultConferencesAsPublic
                try
                {
                    DefaultPublic.ClearSelection();
                    DefaultPublic.Items.FindByValue(xmldoc.SelectSingleNode("//GetOrgOptions/DefaultConferencesAsPublic").InnerText).Selected = true;
                }
                catch (Exception ex) { log.Trace("DefaultConferencesAsPublic: " + ex.Message); }
                // DefaultConferenceType
                try
                {
                    lstDefaultConferenceType.ClearSelection();
                    if (Application["client"].ToString().ToUpper() != "DISNEY")//FB 1985
                        lstDefaultConferenceType.Items.FindByValue(xmldoc.SelectSingleNode("//GetOrgOptions/DefaultConferenceType").InnerText).Selected = true;
                    else
                        lstDefaultConferenceType.SelectedIndex = 1;
                }
                catch (Exception ex) { log.Trace("DefaultConferenceType: " + ex.Message); }
                // RFIC Tag Value //FB 2724
                try
                {
                    lstRFIDValue.ClearSelection();
                    lstRFIDValue.Items.FindByValue(xmldoc.SelectSingleNode("//GetOrgOptions/RFIDTagValue").InnerText).Selected = true;
                }
                catch (Exception ex) { log.Trace("RFIDTagValue: " + ex.Message); }

                if (xmldoc.SelectSingleNode("//GetOrgOptions/iControlTimeout") != null)
                    txtiControlTimeout.Text = xmldoc.SelectSingleNode("//GetOrgOptions/iControlTimeout").InnerText;
                //ZD 103398 start
                if (xmldoc.SelectSingleNode("//GetOrgOptions/VideoRefreshTimer") != null)
                    txtVideoRefreshTimer.Text = xmldoc.SelectSingleNode("//GetOrgOptions/VideoRefreshTimer").InnerText;
                //ZD 103398 end
                //ALLDEV-503 Start
                    DrpEnableiControlLogin.ClearSelection();
                    if (xmldoc.SelectSingleNode("//GetOrgOptions/EnableiControlLogin") != null)
                        DrpEnableiControlLogin.Items.FindByValue(xmldoc.SelectSingleNode("//GetOrgOptions/EnableiControlLogin").InnerText).Selected = true;
                    
                    DrpEnableFindAvailRm.ClearSelection();
                    if (xmldoc.SelectSingleNode("//GetOrgOptions/EnableFindAvailRm") != null)
                        DrpEnableFindAvailRm.Items.FindByValue(xmldoc.SelectSingleNode("//GetOrgOptions/EnableFindAvailRm").InnerText).Selected = true;
                //ALLDEV-503 End
                // EnableRoomConference
                try
                {
                    lstEnableRoomConference.ClearSelection();
                    lstEnableRoomConference.Items.FindByValue(xmldoc.SelectSingleNode("//GetOrgOptions/EnableRoomConference").InnerText).Selected = true;
                }
                catch (Exception ex) { log.Trace("EnableRoomConference: " + ex.Message); }
                // EnableAudioVideoConference
                try
                {
                    lstEnableAudioVideoConference.ClearSelection();
                    lstEnableAudioVideoConference.Items.FindByValue(xmldoc.SelectSingleNode("//GetOrgOptions/EnableAudioVideoConference").InnerText).Selected = true;
                }
                catch (Exception ex) { log.Trace("EnableAudioVideoConference: " + ex.Message); }
                // lstEnableHotdeskingConference
                //ZD 100719 - Start
                try
                {
                    lstEnableHotdeskingConference.ClearSelection();
                    lstEnableHotdeskingConference.Items.FindByValue(xmldoc.SelectSingleNode("//GetOrgOptions/EnableHotdeskingConference").InnerText).Selected = true;
                }
                catch (Exception ex) { log.Trace("EnableHotdeskingConference: " + ex.Message); }
                //ZD 100719 - End
                //FB 2870 Start
                try
                {
                    lstEnableNumericID.ClearSelection();
                    lstEnableNumericID.Items.FindByValue(xmldoc.SelectSingleNode("//GetOrgOptions/EnableNumericID").InnerText).Selected = true;
                }
                catch (Exception ex) { log.Trace("EnableNumericID: " + ex.Message); }
                //FB 2870 End
                //ZD 100513 starts
                drpOBTPConf.ClearSelection();
                if (drpOBTPConf.Items.FindByValue(xmldoc.SelectSingleNode("//GetOrgOptions/EnableWETConference").InnerText) != null)
                    drpOBTPConf.Items.FindByValue(xmldoc.SelectSingleNode("//GetOrgOptions/EnableWETConference").InnerText).Selected = true;

                drpWebExMode.ClearSelection();
                if (drpWebExMode.Items.FindByValue(xmldoc.SelectSingleNode("//GetOrgOptions/WebExLaunch").InnerText) != null)
                    drpWebExMode.Items.FindByValue(xmldoc.SelectSingleNode("//GetOrgOptions/WebExLaunch").InnerText).Selected = true;

                //ZD 100513 Ends
                try
                {
                    lstEnableAudioOnlyConference.ClearSelection();
                    if (Application["client"].ToString().ToUpper() != "DISNEY")//FB 1985
                        lstEnableAudioOnlyConference.Items.FindByValue(xmldoc.SelectSingleNode("//GetOrgOptions/EnableAudioOnlyConference").InnerText).Selected = true;
                    else
                        lstEnableAudioOnlyConference.SelectedIndex = 2;
                }
                catch (Exception ex) { log.Trace("EnableAudioOnlyConference: " + ex.Message); }
                // DefaultCalendarToOfficeHours
                try
                {
                    lstDefaultOfficeHours.ClearSelection();
                    lstDefaultOfficeHours.Items.FindByValue(xmldoc.SelectSingleNode("//GetOrgOptions/DefaultCalendarToOfficeHours").InnerText).Selected = true;
                }
                catch (Exception ex) { log.Trace("DefaultCalendarToOfficeHours: " + ex.Message); }
                // RoomTreeExpandLevel
                try
                {
                    lstRoomTreeLevel.ClearSelection();
                    lstRoomTreeLevel.Items.FindByValue(xmldoc.SelectSingleNode("//GetOrgOptions/RoomTreeExpandLevel").InnerText).Selected = true;
                }
                catch (Exception ex) { log.Trace("RoomtreeExpandLevel " + ex.Message); }
				

                //Merging Recurrence
                 try
                {
                    EnableBufferZone.ClearSelection();
                    EnableBufferZone.Items.FindByValue(xmldoc.SelectSingleNode("//GetOrgOptions/EnableBufferZone").InnerText).Selected = true;
                }
                catch (Exception ex) { log.Trace("EnableBufferZone " + ex.Message); }

                //FB 2440
                 if (xmldoc.SelectSingleNode("//GetOrgOptions/MCUTeardonwnTime") != null)
                     txtMCUTearDownTime.Text = xmldoc.SelectSingleNode("//GetOrgOptions/MCUTeardonwnTime").InnerText;
                if (xmldoc.SelectSingleNode("//GetOrgOptions/McuSetupTime") != null)
                    txtMCUSetupTime.Text = xmldoc.SelectSingleNode("//GetOrgOptions/McuSetupTime").InnerText;
                try
                {
                    DrpForceMCUBuffer.ClearSelection();
                    DrpForceMCUBuffer.Items.FindByValue(xmldoc.SelectSingleNode("//GetOrgOptions/MCUBufferPriority").InnerText).Selected = true;
                }
                catch (Exception ex) { log.Trace("DrpForceMCUBuffer " + ex.Message); }

                 //FB 2440

                //FB 2998
                try
                {
                    MCUSetupDisplay.ClearSelection();
                    MCUSetupDisplay.Items.FindByValue(xmldoc.SelectSingleNode("//GetOrgOptions/MCUSetupDisplay").InnerText).Selected = true;
                }
                catch (Exception ex) { log.Trace("MCUSetupDisplay " + ex.Message); }
                try
                {
                    MCUTearDisplay.ClearSelection();
                    MCUTearDisplay.Items.FindByValue(xmldoc.SelectSingleNode("//GetOrgOptions/MCUTearDisplay").InnerText).Selected = true;
                }
                catch (Exception ex) { log.Trace("MCUTearDisplay " + ex.Message); }

                //FB 2398 Start
                 txtSetupTime.Text = xmldoc.SelectSingleNode("//GetOrgOptions/SetupTime").InnerText;
                 txtTearDownTime.Text = xmldoc.SelectSingleNode("//GetOrgOptions/TearDownTime").InnerText;

                 if (EnableBufferZone.SelectedValue == "1")
                 {
                     //trBufferOptions.Attributes.Remove("style");
                     trBufferOptions.Attributes.Add("style", "display:");//TCK  #100154
                     //trMCUBufferOptions.Attributes.Add("style", "display:");//FB 2440 //TCK  #100154//ZD 100085
                     //trForceMCUBuffer.Attributes.Add("style", "display:");//FB 2440 // TCK  #100154
                 }
                 else
                 {
                     //trBufferOptions.Attributes.Remove("style");
                     trBufferOptions.Attributes.Add("style", "display:none");//TCK #100154
                     //trMCUBufferOptions.Attributes.Add("style", "display:none");//FB 2440 //TCK  #100154 //ZD 100085
                     //trForceMCUBuffer.Attributes.Add("style", "display:none");//FB 2440 //TCK  #100154
                 }
                 //FB 2398 End
                //ZD 104391 - Start
                ContactName.Text = utilObj.ReplaceOutXMLSpecialCharacters(xmldoc.SelectSingleNode("//GetOrgOptions/ContactDetails/Name").InnerText, 1);
                ContactEmail.Text = utilObj.ReplaceOutXMLSpecialCharacters(xmldoc.SelectSingleNode("//GetOrgOptions/ContactDetails/Email").InnerText, 1);
                ContactPhone.Text = utilObj.ReplaceOutXMLSpecialCharacters(xmldoc.SelectSingleNode("//GetOrgOptions/ContactDetails/Phone").InnerText, 1);
                ContactAdditionInfo.Text = utilObj.ReplaceOutXMLSpecialCharacters(xmldoc.SelectSingleNode("//GetOrgOptions/ContactDetails/AdditionInfo").InnerText,1);
                //ZD 104391 - End

                //MYVRM.systemavailtime systemAvail = new MYVRM.systemavailtime(root.SelectSingleNode("systemAvail").OuterXml);
                Open24.Checked = xmldoc.SelectSingleNode("//GetOrgOptions/SystemAvailableTime/IsOpen24Hours").InnerText.Equals("1");

                if (!Open24.Checked)
                {
                    systemStartTime.Text = xmldoc.SelectSingleNode("//GetOrgOptions/SystemAvailableTime/StartTime").InnerText;
                    systemEndTime.Text = xmldoc.SelectSingleNode("//GetOrgOptions/SystemAvailableTime/EndTime").InnerText;
                    try
                    {
                        systemStartTime.Text = DateTime.Parse(myVRMNet.NETFunctions.ChangeTimeFormat(systemStartTime.Text)).ToString(tformat); //FB 2588
                        systemStartTime.ClearSelection();
                        systemStartTime.Items.FindByValue(systemStartTime.Text).Selected = true;
                    }
                    catch (Exception ex) { log.Trace(ex.Message + ":" + ex.StackTrace); }
                    try
                    {
                        systemEndTime.Text = DateTime.Parse(myVRMNet.NETFunctions.ChangeTimeFormat(systemEndTime.Text)).ToString(tformat); //FB 2588
                        systemEndTime.ClearSelection();
                        systemEndTime.Items.FindByValue(systemEndTime.Text).Selected = true;
                    }
                    catch (Exception ex) { log.Trace(ex.Message + ":" + ex.StackTrace); }
                }

                else
                {
                    systemStartTime.Text = DateTime.Parse("12:00 AM").ToString(tformat);
                    systemEndTime.Text = DateTime.Parse("11:59 PM").ToString(tformat); //FB 2588 //ZD 100284
                }
                //FB-1642 Audio Add On Starts..
                try
                {
                    DrpConfcode.ClearSelection();
                    DrpConfcode.Items.FindByValue(xmldoc.SelectSingleNode("//GetOrgOptions/ConferenceCode").InnerText).Selected = true;
                }
                catch (Exception ex) { log.Trace("ConferenceCode: " + ex.Message + " : " + ex.StackTrace); }

                try
                {
                    DrpLedpin.ClearSelection();
                    DrpLedpin.Items.FindByValue(xmldoc.SelectSingleNode("//GetOrgOptions/LeaderPin").InnerText).Selected = true;
                }
                catch (Exception ex) { log.Trace("LeaderPin: " + ex.Message + " : " + ex.StackTrace); }

                try
                {
                    Drpavprm.ClearSelection();
                    Drpavprm.Items.FindByValue(xmldoc.SelectSingleNode("//GetOrgOptions/AdvAvParams").InnerText).Selected = true;
                }
                catch (Exception ex) { log.Trace("AdvAvParams: " + ex.Message + " : " + ex.StackTrace); }

                try
                {
                    DrpAudprm.ClearSelection();
                    DrpAudprm.Items.FindByValue(xmldoc.SelectSingleNode("//GetOrgOptions/AudioParams").InnerText).Selected = true;
                }
                catch (Exception ex) { log.Trace("AudioParams: " + ex.Message + " : " + ex.StackTrace); }
                //FB-1642 Audio Add On Ends..

                //FB 1786 - Start
                if (xmldoc.SelectSingleNode("//GetOrgOptions/iCalReqEmailId") != null)
                    txtiCalEmailId.Text = xmldoc.SelectSingleNode("//GetOrgOptions/iCalReqEmailId").InnerText;
                //FB 1786 - End

                //FB 2610 Starts
                drpBrdgeExt.ClearSelection();
                if (drpBrdgeExt.Items.FindByValue(xmldoc.SelectSingleNode("//GetOrgOptions/showBridgeExt").InnerText) != null)
                    drpBrdgeExt.Items.FindByValue(xmldoc.SelectSingleNode("//GetOrgOptions/showBridgeExt").InnerText).Selected = true;
                //FB 2610 Ends

                tmpstrs = ((tmpstr = xmldoc.SelectSingleNode("//GetOrgOptions/SystemAvailableTime/DaysClosed").InnerText)).Split(',');
                tmpint = ((tmpstr.Trim()).Equals("")) ? -1 : tmpstrs.Length;
                for (i = 0; i < tmpint; i++)
                    DayClosed.Items[Convert.ToInt16(tmpstrs[i]) - 1].Selected = true;
				//FB 1782 start
                DrpListIcal.ClearSelection();
                if (DrpListIcal.Items.FindByValue(xmldoc.SelectSingleNode("//GetOrgOptions/sendIcal").InnerText) != null)
                    DrpListIcal.Items.FindByValue(xmldoc.SelectSingleNode("//GetOrgOptions/sendIcal").InnerText).Selected = true;
                DrpAppIcal.ClearSelection();
                if (DrpAppIcal.Items.FindByValue(xmldoc.SelectSingleNode("//GetOrgOptions/sendApprovalIcal").InnerText) != null)
                    DrpAppIcal.Items.FindByValue(xmldoc.SelectSingleNode("//GetOrgOptions/sendApprovalIcal").InnerText).Selected = true;
                
                //FB 1782 end

                DrpVIP.ClearSelection();
                if (DrpVIP.Items.FindByValue(xmldoc.SelectSingleNode("//GetOrgOptions/isVIP").InnerText) != null)
                    DrpVIP.Items.FindByValue(xmldoc.SelectSingleNode("//GetOrgOptions/isVIP").InnerText).Selected = true;

                DrpUniquePassword.ClearSelection();
                if (DrpUniquePassword.Items.FindByValue(xmldoc.SelectSingleNode("//GetOrgOptions/isUniquePassword").InnerText) != null)
                    DrpUniquePassword.Items.FindByValue(xmldoc.SelectSingleNode("//GetOrgOptions/isUniquePassword").InnerText).Selected = true;

                //FB 1901
                DrpDwnListAssignedMCU.ClearSelection();
                if (DrpDwnListAssignedMCU.Items.FindByValue(xmldoc.SelectSingleNode("//GetOrgOptions/isAssignedMCU").InnerText) != null)
                    DrpDwnListAssignedMCU.Items.FindByValue(xmldoc.SelectSingleNode("//GetOrgOptions/isAssignedMCU").InnerText).Selected = true;

                //ZD 101527 - Start
                DrpRPRMRoomSync.ClearSelection();
                if (DrpRPRMRoomSync.Items.FindByValue(xmldoc.SelectSingleNode("//GetOrgOptions/EnableRPRMRoomSync").InnerText) != null)
                    DrpRPRMRoomSync.Items.FindByValue(xmldoc.SelectSingleNode("//GetOrgOptions/EnableRPRMRoomSync").InnerText).Selected = true;

                DrpRoomSyncAuto.ClearSelection();
                if (DrpRoomSyncAuto.Items.FindByValue(xmldoc.SelectSingleNode("//GetOrgOptions/RoomSyncAuto").InnerText) != null)
                    DrpRoomSyncAuto.Items.FindByValue(xmldoc.SelectSingleNode("//GetOrgOptions/RoomSyncAuto").InnerText).Selected = true;

                if (xmldoc.SelectSingleNode("//GetOrgOptions/RoomSyncPollTime") != null)
                    TxtPollHrs.Text = xmldoc.SelectSingleNode("//GetOrgOptions/RoomSyncPollTime").InnerText;

                //ZD 101527 - End

                //FB 1830 - Translation
                DrpDwnListMultiLingual.ClearSelection();
                if (DrpDwnListMultiLingual.Items.FindByValue(xmldoc.SelectSingleNode("//GetOrgOptions/isMultiLingual").InnerText) != null)
                    DrpDwnListMultiLingual.Items.FindByValue(xmldoc.SelectSingleNode("//GetOrgOptions/isMultiLingual").InnerText).Selected = true;

                //FB 2141
                DrpPluginConfirm.ClearSelection();
                if (DrpPluginConfirm.Items.FindByValue(xmldoc.SelectSingleNode("//GetOrgOptions/PIMNotifications").InnerText) != null)
                    DrpPluginConfirm.Items.FindByValue(xmldoc.SelectSingleNode("//GetOrgOptions/PIMNotifications").InnerText).Selected = true;
                //FB 2154
                DrpDwnAttachmnts.ClearSelection();
                if (DrpDwnAttachmnts.Items.FindByValue(xmldoc.SelectSingleNode("//GetOrgOptions/ExternalAttachments").InnerText) != null)
                    DrpDwnAttachmnts.Items.FindByValue(xmldoc.SelectSingleNode("//GetOrgOptions/ExternalAttachments").InnerText).Selected = true;

                //FB 2170
                DrpDwnFilterTelepresence.ClearSelection();
                if (DrpDwnFilterTelepresence.Items.FindByValue(xmldoc.SelectSingleNode("//GetOrgOptions/filterTelepresence").InnerText) != null)
                    DrpDwnFilterTelepresence.Items.FindByValue(xmldoc.SelectSingleNode("//GetOrgOptions/filterTelepresence").InnerText).Selected = true;

                //FB 2219 //ZD 102159
                //lstEnableRoomServiceType.ClearSelection();
                //if (lstEnableRoomServiceType.Items.FindByValue(xmldoc.SelectSingleNode("//GetOrgOptions/EnableRoomServiceType").InnerText) != null)
                //    lstEnableRoomServiceType.Items.FindByValue(xmldoc.SelectSingleNode("//GetOrgOptions/EnableRoomServiceType").InnerText).Selected = true;

                //FB 1926
                if (xmldoc.SelectSingleNode("//GetOrgOptions/ReminderMask") != null)
                    SetRemainderfromMask(xmldoc.SelectSingleNode("//GetOrgOptions/ReminderMask").InnerText);

                 //FB 2052
                DrpDwnListSpRecur.ClearSelection();
                if (DrpDwnListSpRecur.Items.FindByValue(xmldoc.SelectSingleNode("//GetOrgOptions/isSpecialRecur").InnerText) != null)
                    DrpDwnListSpRecur.Items.FindByValue(xmldoc.SelectSingleNode("//GetOrgOptions/isSpecialRecur").InnerText).Selected = true;

                //FB 2269
                DrpDwnListDeptUser.ClearSelection();
                if (DrpDwnListDeptUser.Items.FindByValue(xmldoc.SelectSingleNode("//GetOrgOptions/isDeptUser").InnerText.Trim()) != null)
                    DrpDwnListDeptUser.Items.FindByValue(xmldoc.SelectSingleNode("//GetOrgOptions/isDeptUser").InnerText.Trim()).Selected = true;

				//FB 2038
                lstEnablePIMServiceType.ClearSelection();
                if (lstEnablePIMServiceType.Items.FindByValue(xmldoc.SelectSingleNode("//GetOrgOptions/EnablePIMServiceType").InnerText) != null)
                    lstEnablePIMServiceType.Items.FindByValue(xmldoc.SelectSingleNode("//GetOrgOptions/EnablePIMServiceType").InnerText).Selected = true;
                
                //FB 2036
                lstEnableImmediateConference.ClearSelection();
                if (lstEnableImmediateConference.Items.FindByValue(xmldoc.SelectSingleNode("//GetOrgOptions/EnableImmConf").InnerText) != null)
                    lstEnableImmediateConference.Items.FindByValue(xmldoc.SelectSingleNode("//GetOrgOptions/EnableImmConf").InnerText).Selected = true;

                lstEnableAudioBridges.ClearSelection(); //FB 2023
                if (lstEnableAudioBridges.Items.FindByValue(xmldoc.SelectSingleNode("//GetOrgOptions/EnableAudioBridges").InnerText) != null)
                    lstEnableAudioBridges.Items.FindByValue(xmldoc.SelectSingleNode("//GetOrgOptions/EnableAudioBridges").InnerText).Selected = true;
                //FB 2359 Start
                //ZD 101446 Starts
                drpPartyCode.ClearSelection();
                if (drpPartyCode.Items.FindByValue(xmldoc.SelectSingleNode("//GetOrgOptions/EnablePartyCode").InnerText) != null)
                    drpPartyCode.Items.FindByValue(xmldoc.SelectSingleNode("//GetOrgOptions/EnablePartyCode").InnerText).Selected = true;
                //ZD 101446 Ends
                lstEnableConfPassword.ClearSelection(); 
                if (lstEnableConfPassword.Items.FindByValue(xmldoc.SelectSingleNode("//GetOrgOptions/EnableConferencePassword").InnerText) != null)
                    lstEnableConfPassword.Items.FindByValue(xmldoc.SelectSingleNode("//GetOrgOptions/EnableConferencePassword").InnerText).Selected = true;

                lstEnablePublicConf.ClearSelection(); 
                if (lstEnablePublicConf.Items.FindByValue(xmldoc.SelectSingleNode("//GetOrgOptions/EnablePublicConference").InnerText) != null)
                    lstEnablePublicConf.Items.FindByValue(xmldoc.SelectSingleNode("//GetOrgOptions/EnablePublicConference").InnerText).Selected = true;

                lstRoomprm.ClearSelection(); 
                if (lstRoomprm.Items.FindByValue(xmldoc.SelectSingleNode("//GetOrgOptions/EnableRoomParam").InnerText) != null)
                    lstRoomprm.Items.FindByValue(xmldoc.SelectSingleNode("//GetOrgOptions/EnableRoomParam").InnerText).Selected = true;
                //FB 2359 End
                //FB 2136 start
                /*drpenablesecuritybadge.ClearSelection();
                if (drpenablesecuritybadge.Items.FindByValue(xmldoc.SelectSingleNode("//GetOrgOptions/EnableSecurityBadge").InnerText) != null)
                    drpenablesecuritybadge.Items.FindByValue(xmldoc.SelectSingleNode("//GetOrgOptions/EnableSecurityBadge").InnerText).Selected = true;

                drpsecuritybadgetype.Style.Add("display","block");
                tdSecurityType.Attributes.Add("style", "display:block");
                if ((xmldoc.SelectSingleNode("//GetOrgOptions/EnableSecurityBadge").InnerText) == "0")
                {
                    drpsecuritybadgetype.Style.Add("display","none");
                    tdSecurityType.Attributes.Add("style", "display:none");
                    tdsecdeskemailid.Attributes.Add("style", "visibility:hidden");
                }

                obj.BindSecurityBadgeType(drpsecuritybadgetype); //FB 2136
                drpsecuritybadgetype.ClearSelection();
                if (drpsecuritybadgetype.Items.FindByValue(xmldoc.SelectSingleNode("//GetOrgOptions/SecurityBadgeType").InnerText) != null)
                    drpsecuritybadgetype.Items.FindByValue(xmldoc.SelectSingleNode("//GetOrgOptions/SecurityBadgeType").InnerText).Selected = true;
                if ((xmldoc.SelectSingleNode("//GetOrgOptions/SecurityBadgeType").InnerText) == "1")
                {
                    tdsecdeskemailid.Attributes.Add("style", "visibility:hidden");
                }

                if (xmldoc.SelectSingleNode("//GetOrgOptions/SecurityDeskEmailId") != null)
                    txtsecdeskemailid.Text = xmldoc.SelectSingleNode("//GetOrgOptions/SecurityDeskEmailId").InnerText;
                */
                //FB 2136 end
                //FB 2339 Start
                DrpDwnPasswordRule.ClearSelection();
                if (DrpDwnPasswordRule.Items.FindByValue(xmldoc.SelectSingleNode("//GetOrgOptions/EnablePasswordRule").InnerText) != null)
                    DrpDwnPasswordRule.Items.FindByValue(xmldoc.SelectSingleNode("//GetOrgOptions/EnablePasswordRule").InnerText).Selected = true;
                //FB 2339 End

                //FB 2334
                DrpDwnDedicatedVideo.ClearSelection();
                if (DrpDwnDedicatedVideo.Items.FindByValue(xmldoc.SelectSingleNode("//GetOrgOptions/DedicatedVideo").InnerText) != null)
                    DrpDwnDedicatedVideo.Items.FindByValue(xmldoc.SelectSingleNode("//GetOrgOptions/DedicatedVideo").InnerText).Selected = true;

                //FB 2335
                //VMR Start
                lstEnableVMR.ClearSelection();
                if (lstEnableVMR.Items.FindByValue(xmldoc.SelectSingleNode("//GetOrgOptions/EnableVMR").InnerText) != null)
                    lstEnableVMR.Items.FindByValue(xmldoc.SelectSingleNode("//GetOrgOptions/EnableVMR").InnerText).Selected = true;
                Session.Add("EnableVMR", xmldoc.SelectSingleNode("//GetOrgOptions/EnableVMR").InnerText); // FB 2620-Doubt
                //VMR End

                //FB 2401 Start
                lstEPinMail.ClearSelection();
                if (lstEPinMail.Items.FindByValue(xmldoc.SelectSingleNode("//GetOrgOptions/EnableEPDetails").InnerText) != null)
                    lstEPinMail.Items.FindByValue(xmldoc.SelectSingleNode("//GetOrgOptions/EnableEPDetails").InnerText).Selected = true;
                //FB 2401 End

                //FB 2472 Start
                lstMcuAlert.ClearSelection();
                if (lstMcuAlert.Items.FindByValue(xmldoc.SelectSingleNode("//GetOrgOptions/MCUAlert").InnerText) != null)
                    lstMcuAlert.Items.FindByValue(xmldoc.SelectSingleNode("//GetOrgOptions/MCUAlert").InnerText).Selected = true;
                //FB 2472 End

                try
                {
                    fnSetLayoutImage(xmldoc.SelectSingleNode("//GetOrgOptions/defPolycomMGCLO").InnerText, xmldoc.SelectSingleNode("//GetOrgOptions/defPolycomRMXLO").InnerText, xmldoc.SelectSingleNode("//GetOrgOptions/defCTMSLO").InnerText, xmldoc.SelectSingleNode("//GetOrgOptions/defCiscoTPLO").InnerText, xmldoc.SelectSingleNode("//GetOrgOptions/DefCodianLO").InnerText); //ZD 101869
                }
                catch (Exception ex) { log.Trace("Def Layout: " + ex.Message + " : " + ex.StackTrace); }

                //FB 2348 Start
                drpenablesurvey.SelectedValue = xmldoc.SelectSingleNode("//GetOrgOptions/EnableSurvey").InnerText;
                // ZD 103416 Start
                //if (drpenablesurvey.SelectedValue == "1")
                //{
                     //tdSurveyengine.Attributes.Add("style", "visibility: visible");
                    //tdsurveyoption.Attributes.Add("style", "visibility: visible");
               // }
                //drpsurveyoption.SelectedValue = xmldoc.SelectSingleNode("//GetOrgOptions/SurveyOption").InnerText;
               // if (drpsurveyoption.SelectedValue == "2" && drpenablesurvey.SelectedValue == "1")
                if (drpenablesurvey.SelectedValue == "1")
                {
                    divSurveytimedur.Attributes.Add("Style", "Display:block");
                    divSurveyURL.Attributes.Add("Style", "Display:block");
                }
                else
                {
                    divSurveytimedur.Attributes.Add("Style", "Display:none");
                    divSurveyURL.Attributes.Add("Style", "Display:none");
                }
                txtSurWebsiteURL.Text = xmldoc.SelectSingleNode("//GetOrgOptions/SurveyURL").InnerText;
                txtTimeDur.Text = xmldoc.SelectSingleNode("//GetOrgOptions/TimeDuration").InnerText;

                hdnSurURL.Value = txtSurWebsiteURL.Text;
                hdnTimeDur.Value = txtTimeDur.Text;
                // ZD 103416 End
                //FB 2348 End
                //FB 2419 - Starts
                lstAcceptDecline.ClearSelection();
                if (lstAcceptDecline.Items.FindByValue(xmldoc.SelectSingleNode("//GetOrgOptions/EnableAcceptDecline").InnerText) != null)
                    lstAcceptDecline.Items.FindByValue(xmldoc.SelectSingleNode("//GetOrgOptions/EnableAcceptDecline").InnerText).Selected = true;
                //FB 2419 - End
                //FB 2429 - Starts
                lstLineRate.ClearSelection();
                if (lstLineRate.Items.FindByValue(xmldoc.SelectSingleNode("//GetOrgOptions/DefaultLineRate").InnerText) != null)
                    lstLineRate.Items.FindByValue(xmldoc.SelectSingleNode("//GetOrgOptions/DefaultLineRate").InnerText).Selected = true;
                //FB 2429 - End

                string TxtMsg = "";
                node = xmldoc.SelectSingleNode("//GetOrgOptions/OrgMessage"); //FB 2486
                if (node != null)
                {
                    TxtMsg = xmldoc.SelectSingleNode("//GetOrgOptions/OrgMessage").InnerText.Trim();
                    GetOrgMessages(TxtMsg);
                }
                
                //FB 2426 Start
                lstEnableSmartP2P.Attributes.Add("onchange", "javascript:fnUpdateSmartNotify()");
                lstEnableSmartP2P.ClearSelection();//FB 2430
                if (lstEnableSmartP2P.Items.FindByValue(xmldoc.SelectSingleNode("//GetOrgOptions/EnableSmartP2P").InnerText) != null)
                    lstEnableSmartP2P.Items.FindByValue(xmldoc.SelectSingleNode("//GetOrgOptions/EnableSmartP2P").InnerText).Selected = true;


               //FB 2571 - START
                try
                {
                    lstenableFECC.ClearSelection();
                    if (lstenableFECC.Items.FindByValue(xmldoc.SelectSingleNode("//GetOrgOptions/EnableFECC").InnerText) != null)
                        lstenableFECC.Items.FindByValue(xmldoc.SelectSingleNode("//GetOrgOptions/EnableFECC").InnerText).Selected = true;
                }
                catch (Exception ex) { log.Trace("EnableFECC " + ex.Message); }

                if (lstenableFECC.SelectedValue == "2")
                {
                    DefaultFECC.Attributes.Add("style", "visibility: hidden");
                    DefaultFECCoptions.Attributes.Add("style", "visibility: hidden");
                }
                else
                {
                    DefaultFECC.Attributes.Add("style", "visibility:visible");
                    DefaultFECCoptions.Attributes.Add("style", "visibility:visible");
                }
                if (lstenableFECC.SelectedValue == "0")
                {
                    DefaultFECC.Attributes.Add("style", "visibility:visible");
                    DefaultFECCoptions.Attributes.Add("style", "value:1");
                    lstdefaultFECC.Attributes.Add("disabled", "disabled");
                }
                //FB 2571 - Starts
                lstdefaultFECC.ClearSelection();
                if (lstdefaultFECC.Items.FindByValue(xmldoc.SelectSingleNode("//GetOrgOptions/DefaultFECC").InnerText) != null)
                    lstdefaultFECC.Items.FindByValue(xmldoc.SelectSingleNode("//GetOrgOptions/DefaultFECC").InnerText).Selected = true;
                //FB 2571 End
                //FB 2588 Start
                try
                {
                    DropDownZuLu.Attributes.Add("onchange", "javascript:fnChangeZulu()");
                    DropDownZuLu.ClearSelection();
                    if (DropDownZuLu.Items.FindByValue(xmldoc.SelectSingleNode("//GetOrgOptions/EnableZulu").InnerText) != null)
                        DropDownZuLu.Items.FindByValue(xmldoc.SelectSingleNode("//GetOrgOptions/EnableZulu").InnerText).Selected = true;
                }
                catch (Exception ex) { log.Trace("EnableZulu " + ex.Message); }
                if (DropDownZuLu.SelectedValue == "1")
                {
                    TimezoneSystems.Attributes.Add("style", "visibility:visible");
                    TimezoneSystems.Attributes.Add("style", "value:0");
                    TimezoneSystems.Attributes.Add("disabled", "disabled");
                }
                else
                {
                    TimezoneSystems.Attributes.Add("style", "visibility:visible");
                    
                }
                //FB 2588 End
                
                //FB 2598 Starts 
                DrpDwnEnableCallmonitor.ClearSelection();
                if (DrpDwnEnableCallmonitor.Items.FindByValue(xmldoc.SelectSingleNode("//GetOrgOptions/EnableCallmonitor").InnerText) != null)
                    DrpDwnEnableCallmonitor.Items.FindByValue(xmldoc.SelectSingleNode("//GetOrgOptions/EnableCallmonitor").InnerText).Selected = true;
                Session.Add("EnableCallmonitor", xmldoc.SelectSingleNode("//GetOrgOptions/EnableCallmonitor").InnerText);

                DrpDwnEnableEM7.ClearSelection();
                if (DrpDwnEnableEM7.Items.FindByValue(xmldoc.SelectSingleNode("//GetOrgOptions/EnableEM7").InnerText) != null)
                    DrpDwnEnableEM7.Items.FindByValue(xmldoc.SelectSingleNode("//GetOrgOptions/EnableEM7").InnerText).Selected = true;
                Session.Add("EnableEM7", xmldoc.SelectSingleNode("//GetOrgOptions/EnableEM7").InnerText);

                DrpDwnEnableCDR.ClearSelection();
                if (DrpDwnEnableCDR.Items.FindByValue(xmldoc.SelectSingleNode("//GetOrgOptions/EnableCDR").InnerText) != null)
                    DrpDwnEnableCDR.Items.FindByValue(xmldoc.SelectSingleNode("//GetOrgOptions/EnableCDR").InnerText).Selected = true;
                Session.Add("EnableCDR", xmldoc.SelectSingleNode("//GetOrgOptions/EnableCDR").InnerText);
                //FB 2598 Ends

                if (xmldoc.SelectSingleNode("//GetOrgOptions/AlertforTier1") != null) //FB 2637
                    hdnTierIDs.Value = xmldoc.SelectSingleNode("//GetOrgOptions/AlertforTier1").InnerText + "|";

                string alert = hdnTierIDs.Value;
				
				// FB 2636 Starts
                DrpEnableE164DialPlan.ClearSelection();
                if (DrpEnableE164DialPlan.Items.FindByValue(xmldoc.SelectSingleNode("//GetOrgOptions/EnableDialPlan").InnerText) != null)
                    DrpEnableE164DialPlan.Items.FindByValue(xmldoc.SelectSingleNode("//GetOrgOptions/EnableDialPlan").InnerText).Selected = true;
                // FB 2636 End


                //FB 2839 Start
                lstEnableProfileSelection.ClearSelection();
                if (lstEnableProfileSelection.Items.FindByValue(xmldoc.SelectSingleNode("//GetOrgOptions/EnableProfileSelection").InnerText) != null)
                    lstEnableProfileSelection.Items.FindByValue(xmldoc.SelectSingleNode("//GetOrgOptions/EnableProfileSelection").InnerText).Selected = true;
                //FB 2839 End

                StringBuilder inXML = new StringBuilder();  //Used StringBuilder instead of string for inXML during ZD 100068 start
                string toptier = "", middletier = "";
                inXML.Append("<GetLocations>");
                inXML.Append(obj.OrgXMLElement());//Organization Module Fixes
                inXML.Append("<UserID>" + Session["userID"].ToString() + "</UserID>");
                inXML.Append("</GetLocations>");
                log.Trace("getLocations InXML: " + inXML);
                string outXML1 = obj.CallMyVRMServer("GetLocations", inXML.ToString(), Application["MyVRMServer_ConfigPath"].ToString());
                //Used StringBuilder instead of string for inXML during ZD 100068 end
                log.Trace("getLocations OutXML: " + outXML);
                if (outXML1.IndexOf("<error>") < 0)
                {
                    XmlDocument xmldoc1 = new XmlDocument();
                    xmldoc1.LoadXml(outXML1);
                    XmlNodeList nodes = xmldoc1.SelectNodes("//GetLocations/Location");
                    if (nodes != null)
                    {
                        obj.LoadList(lstTopTier, nodes, "ID", "Name");
                        //FB 2637 Starts
                        obj.LoadList(lstVMRTopTier, nodes, "ID", "Name");//ZD 100068
                        obj.LoadList(lstTier1, nodes, "ID", "Name");
                    }
                    foreach (ListItem li in lstTier1.Items)
                    {
                        if (alert.IndexOf(li.Value) > -1)
                        {
                            li.Selected = true;
                        }
                    }
                    //FB 2637 Ends
                }
                //Used StringBuilder instead of string for inXML during ZD 100068 start
                if (xmldoc.SelectSingleNode("//GetOrgOptions/OnflyTier/DefTopTier") != null)
                    toptier = xmldoc.SelectSingleNode("//GetOrgOptions/OnflyTier/DefTopTier").InnerText.Trim();
                if (xmldoc.SelectSingleNode("//GetOrgOptions/OnflyTier/DefMiddleTier") != null)
                    middletier = xmldoc.SelectSingleNode("//GetOrgOptions/OnflyTier/DefMiddleTier").InnerText.Trim();
                Session.Add("OnflyTopTierName", toptier);
                Session.Add("OnflyMiddleTierName", middletier);
                
                if (xmldoc.SelectSingleNode("//GetOrgOptions/OnflyTier/OnflyTopTierID") != null)
                    lstTopTier.SelectedValue = xmldoc.SelectSingleNode("//GetOrgOptions/OnflyTier/OnflyTopTierID").InnerText.Trim();
                Session.Add("OnflyTopTierID", lstTopTier.SelectedValue);
                UpdateMiddleTiers(new Object(), new EventArgs());
                
                if (xmldoc.SelectSingleNode("//GetOrgOptions/OnflyTier/OnflyMiddleTierID") != null)
                    lstMiddleTier.SelectedValue = xmldoc.SelectSingleNode("//GetOrgOptions/OnflyTier/OnflyMiddleTierID").InnerText.Trim();
                Session.Add("OnflyMiddleTierID", lstMiddleTier.SelectedValue);
                //Used StringBuilder instead of string for inXML during ZD 100068 end
                try
                {
                    lstTopTier.ClearSelection();
                    lstTopTier.Items.FindByValue(xmldoc.SelectSingleNode("//GetOrgOptions/OnflyTier/OnflyTopTierID").InnerText).Selected = true;
                    if ((lstTopTier.Items.Count > 1) && (lstTopTier.SelectedIndex > 0))
                    {
                        UpdateMiddleTiers(new Object(), new EventArgs());
                        if (lstMiddleTier.Items.Count > 0)
                        {
                            lstMiddleTier.ClearSelection();
                            lstMiddleTier.Items.FindByValue(xmldoc.SelectSingleNode("//GetOrgOptions/OnflyTier/OnflyMiddleTierID").InnerText).Selected = true;
                        }
                    }
                     }
                catch (Exception ex1) { }
                //FB 2426 End
                //ZD 100068 start
                try
                {
                    if (xmldoc.SelectSingleNode("//GetOrgOptions/VMRTier/VMRTopTierID") != null)
                    {
                        lstVMRTopTier.ClearSelection();
                        lstVMRTopTier.Items.FindByValue(xmldoc.SelectSingleNode("//GetOrgOptions/VMRTier/VMRTopTierID").InnerText).Selected = true;
                        Session.Add("VMRTopTierID", lstVMRTopTier.SelectedValue);
                    }
                    if ((lstVMRTopTier.Items.Count > 1) && (lstVMRTopTier.SelectedIndex > 0))
                    {
                        UpdateVMRMiddleTiers(new Object(), new EventArgs());
                        if (lstVMRMiddleTier.Items.Count > 0)
                        {
                            if (xmldoc.SelectSingleNode("//GetOrgOptions/VMRTier/VMRMiddleTierID") != null)
                            {
                                lstVMRMiddleTier.ClearSelection();
                                lstVMRMiddleTier.Items.FindByValue(xmldoc.SelectSingleNode("//GetOrgOptions/VMRTier/VMRMiddleTierID").InnerText).Selected = true;
                                Session.Add("VMRMiddleTierID", lstVMRMiddleTier.SelectedValue);
                            }
                        }
                    }
                }
                catch (Exception ex1) { }
                //ZD 100068 end

                //FB 2469 - Starts
                lstEnableConfTZinLoc.ClearSelection();
                if (lstEnableConfTZinLoc.Items.FindByValue(xmldoc.SelectSingleNode("//GetOrgOptions/EnableConfTZinLoc").InnerText) != null)
                    lstEnableConfTZinLoc.Items.FindByValue(xmldoc.SelectSingleNode("//GetOrgOptions/EnableConfTZinLoc").InnerText).Selected = true;
                lstSendConfirmationEmail.ClearSelection();
                if (lstSendConfirmationEmail.Items.FindByValue(xmldoc.SelectSingleNode("//GetOrgOptions/SendConfirmationEmail").InnerText) != null)
                    lstSendConfirmationEmail.Items.FindByValue(xmldoc.SelectSingleNode("//GetOrgOptions/SendConfirmationEmail").InnerText).Selected = true;
                //FB 2469 - End
                //FB 2501 - Start
                txtDefaultConfDuration.Text = xmldoc.SelectSingleNode("//GetOrgOptions/DefaultConfDuration").InnerText;
                if (txtDefaultConfDuration.Text == "")
                {
                    txtDefaultConfDuration.Text = "60";
                }
                //FB 2501 - End
                //FB 2550 Starts
                int VMRtype=0;
                Int32.TryParse(lstEnableVMR.SelectedValue.ToString(),out VMRtype);
                tdMaxParty.Attributes.Add("style", "display:none");
                tdMaxPartyCount.Attributes.Add("style", "display:none");
                if ((DynamicInviteEnabled.SelectedValue.ToString().Equals("1")) && VMRtype > 0)
                {
                    tdMaxParty.Attributes.Add("style", "display:block");
                    tdMaxPartyCount.Attributes.Add("style", "display:block");
                    txtMaxVMRParty.Text = xmldoc.SelectSingleNode("//GetOrgOptions/MaxPublicVMRParty").InnerText;
                }
                else
                    txtMaxVMRParty.Text = "0";
                //FB 2550 Ends
                txtMeetandGreetBuffer.Text = xmldoc.SelectSingleNode("//GetOrgOptions/MeetandGreetBuffer").InnerText;//FB 2609

                //FB 2632 Starts
                drpCngSupport.Attributes.Add("onchange", "javascript:fnUpdateCngSupport()");
                drpCngSupport.ClearSelection();
                if (xmldoc.SelectSingleNode("//GetOrgOptions/EnableCongSupport") != null)
                {
                    if (drpCngSupport.Items.FindByValue(xmldoc.SelectSingleNode("//GetOrgOptions/EnableCongSupport").InnerText) != null)
                    {
                        drpCngSupport.Items.FindByValue(xmldoc.SelectSingleNode("//GetOrgOptions/EnableCongSupport").InnerText).Selected = true;
                        if (xmldoc.SelectSingleNode("//GetOrgOptions/EnableCongSupport").InnerText == "0")
                        {
                            chkMeetandGreet.Disabled = true;
                            chkOnSiteAVSupport.Disabled = true;
                            chkDedicatedVNOCOperator.Disabled = true;
                            chkConciergeMonitoring.Disabled = true;
                        }
                    }
                }

                if (node.SelectSingleNode("//GetOrgOptions/MeetandGreetinEmail") != null)
                {
                    if (node.SelectSingleNode("//GetOrgOptions/MeetandGreetinEmail").InnerText.Equals("1"))
                        chkMeetandGreet.Checked = true;
                    else
                        chkMeetandGreet.Checked = false;
                }
                if (node.SelectSingleNode("//GetOrgOptions/OnSiteAVSupportinEmail") != null)
                {
                    if (node.SelectSingleNode("//GetOrgOptions/OnSiteAVSupportinEmail").InnerText.Equals("1"))
                        chkOnSiteAVSupport.Checked = true;
                    else
                        chkOnSiteAVSupport.Checked = false;
                }
                if (node.SelectSingleNode("//GetOrgOptions/ConciergeMonitoringinEmail") != null)
                {
                    if (node.SelectSingleNode("//GetOrgOptions/ConciergeMonitoringinEmail").InnerText.Equals("1"))
                        chkConciergeMonitoring.Checked = true;
                    else
                        chkConciergeMonitoring.Checked = false;
                }
                if (node.SelectSingleNode("//GetOrgOptions/DedicatedVNOCOperatorinEmail") != null)
                {
                    if (node.SelectSingleNode("//GetOrgOptions/DedicatedVNOCOperatorinEmail").InnerText.Equals("1"))
                        chkDedicatedVNOCOperator.Checked = true;
                    else
                        chkDedicatedVNOCOperator.Checked = false;
                }


                //FB 2632 Ends
                //FB 2631
                lstEnableRoomAdminDetails.ClearSelection();
                if (lstEnableRoomAdminDetails.Items.FindByValue(xmldoc.SelectSingleNode("//GetOrgOptions/EnableRoomAdminDetails").InnerText) != null)
                    lstEnableRoomAdminDetails.Items.FindByValue(xmldoc.SelectSingleNode("//GetOrgOptions/EnableRoomAdminDetails").InnerText).Selected = true;

                //FB 2595 Start
                if (xmldoc.SelectSingleNode("//GetOrgOptions/SecureSwitch") != null)
                    if (drpSecureSwitch.Items.FindByValue(xmldoc.SelectSingleNode("//GetOrgOptions/SecureSwitch").InnerText) != null)
                        drpSecureSwitch.Items.FindByValue(xmldoc.SelectSingleNode("//GetOrgOptions/SecureSwitch").InnerText).Selected = true;

                if (xmldoc.SelectSingleNode("//GetOrgOptions/HardwareAdminEmail") != null)
                    if (xmldoc.SelectSingleNode("//GetOrgOptions/HardwareAdminEmail").InnerText != null)
                        txtHardwareAdminEmail.Text = xmldoc.SelectSingleNode("//GetOrgOptions/HardwareAdminEmail").InnerText.Trim();

                drpNwtSwtiching.ClearSelection();
                if (xmldoc.SelectSingleNode("//GetOrgOptions/NetworkSwitching") != null)
                    if (drpNwtSwtiching.Items.FindByValue(xmldoc.SelectSingleNode("//GetOrgOptions/NetworkSwitching").InnerText) != null)
                        drpNwtSwtiching.Items.FindByValue(xmldoc.SelectSingleNode("//GetOrgOptions/NetworkSwitching").InnerText).Selected = true;
                
                drpNwtCallLaunch.ClearSelection();
                if (xmldoc.SelectSingleNode("//GetOrgOptions/NetworkCallLaunch") != null)
                    if (drpNwtCallLaunch.Items.FindByValue(xmldoc.SelectSingleNode("//GetOrgOptions/NetworkCallLaunch").InnerText) != null)
                        drpNwtCallLaunch.Items.FindByValue(xmldoc.SelectSingleNode("//GetOrgOptions/NetworkCallLaunch").InnerText).Selected = true;

                if (xmldoc.SelectSingleNode("//GetOrgOptions/SecureLaunchBuffer") != null)
                    txtSecureLaunch.Text = xmldoc.SelectSingleNode("//GetOrgOptions/SecureLaunchBuffer").InnerText.Trim();

                //FB 2993 starts
                if (xmldoc.SelectSingleNode("//GetOrgOptions/ResponseTimeout") != null)
                    txtResponseTimeout.Text = xmldoc.SelectSingleNode("//GetOrgOptions/ResponseTimeout").InnerText.Trim();
                //FB 2993 End


                if (drpSecureSwitch.SelectedValue == "0")
                {
                    tdsecureadminaddress.Attributes.Add("style", "visibility: hidden");
                    txtHardwareAdminEmail.Attributes.Add("style", "visibility: hidden");
                    trNwtSwtiching.Attributes.Add("style", "visibility: hidden");
                    trNwtCallBuffer.Attributes.Add("style", "visibility: hidden");
                }
                else
                {
                    tdsecureadminaddress.Attributes.Add("style", "visibility:visible");
                    txtHardwareAdminEmail.Attributes.Add("style", "visibility:visible");
                    trNwtSwtiching.Attributes.Add("style", "visibility: visible");
                    trNwtCallBuffer.Attributes.Add("style", "visibility: visible");
                }

                //FB 2595 End
                // FB 2641 start
                drpEnableLineRate.ClearSelection();
                if (xmldoc.SelectSingleNode("//GetOrgOptions/EnableLinerate") != null)
                    if (drpEnableLineRate.Items.FindByValue(xmldoc.SelectSingleNode("//GetOrgOptions/EnableLinerate").InnerText) != null)
                        drpEnableLineRate.Items.FindByValue(xmldoc.SelectSingleNode("//GetOrgOptions/EnableLinerate").InnerText).Selected = true;

                drpEnableStartMode.ClearSelection();
                if(xmldoc.SelectSingleNode("//GetOrgOptions/EnableStartMode")!=null)
                    if (drpEnableStartMode.Items.FindByValue(xmldoc.SelectSingleNode("//GetOrgOptions/EnableStartMode").InnerText) != null)
                        drpEnableStartMode.Items.FindByValue(xmldoc.SelectSingleNode("//GetOrgOptions/EnableStartMode").InnerText).Selected = true;

                // FB 2641 End
                //FB 2670 START
                if (node.SelectSingleNode("//GetOrgOptions/EnableOnsiteAV") != null)
                {
                    if (node.SelectSingleNode("//GetOrgOptions/EnableOnsiteAV").InnerText.Equals("1"))
                        chkOnsiteAV.Checked = true;
                    else
                        chkOnsiteAV.Checked = false;
                }
                if (node.SelectSingleNode("//GetOrgOptions/EnableMeetandGreet") != null)
                {
                    if (node.SelectSingleNode("//GetOrgOptions/EnableMeetandGreet").InnerText.Equals("1"))
                        chkMeetandGret.Checked = true;
                    else
                        chkMeetandGret.Checked = false;
                }
                if (node.SelectSingleNode("//GetOrgOptions/EnableConciergeMonitoring") != null)
                {
                    if (node.SelectSingleNode("//GetOrgOptions/EnableConciergeMonitoring").InnerText.Equals("1"))
                        chkConciergeMonitor.Checked = true;
                    else
                        chkConciergeMonitor.Checked = false;
                }
                if (node.SelectSingleNode("//GetOrgOptions/EnableDedicatedVNOC") != null)
                {
                    if (node.SelectSingleNode("//GetOrgOptions/EnableDedicatedVNOC").InnerText.Equals("1"))
                        chkDedicatedVNOC.Checked = true;
                    else
                        chkDedicatedVNOC.Checked = false;
                }
                //FB 2670 END
                //FB 2817
                lstSigRoomConf.ClearSelection();
                if (lstSigRoomConf.Items.FindByValue(xmldoc.SelectSingleNode("//GetOrgOptions/EnableSingleRoomConfEmails").InnerText) != null)
                    lstSigRoomConf.Items.FindByValue(xmldoc.SelectSingleNode("//GetOrgOptions/EnableSingleRoomConfEmails").InnerText).Selected = true;

                //ZD 100151
                DrpDwnEnableCA.ClearSelection();
                if (DrpDwnEnableCA.Items.FindByValue(xmldoc.SelectSingleNode("//GetOrgOptions/ShowCusAttInCalendar").InnerText) != null)
                    DrpDwnEnableCA.Items.FindByValue(xmldoc.SelectSingleNode("//GetOrgOptions/ShowCusAttInCalendar").InnerText).Selected = true;
                Session.Add("ShowCusAttInCalendar", xmldoc.SelectSingleNode("//GetOrgOptions/ShowCusAttInCalendar").InnerText);

                //ZD 100164 START
                DrpDwnAdvUserOption.ClearSelection();
                if (DrpDwnAdvUserOption.Items.FindByValue(xmldoc.SelectSingleNode("//GetOrgOptions/EnableAdvancedUserOption").InnerText) != null)
                    DrpDwnAdvUserOption.Items.FindByValue(xmldoc.SelectSingleNode("//GetOrgOptions/EnableAdvancedUserOption").InnerText).Selected = true;
               //ZD 100164 END


                //ZD 100707 START
                drpEnableVMR.ClearSelection();
                if (drpEnableVMR.Items.FindByValue(xmldoc.SelectSingleNode("//GetOrgOptions/ShowHideVMR").InnerText) != null)
                    drpEnableVMR.Items.FindByValue(xmldoc.SelectSingleNode("//GetOrgOptions/ShowHideVMR").InnerText).Selected = true;

                drpEnablePersonaVMR.ClearSelection();
                if (drpEnablePersonaVMR.Items.FindByValue(xmldoc.SelectSingleNode("//GetOrgOptions/EnablePersonaVMR").InnerText) != null)
                    drpEnablePersonaVMR.Items.FindByValue(xmldoc.SelectSingleNode("//GetOrgOptions/EnablePersonaVMR").InnerText).Selected = true;

                drpEnableRoomVMR.ClearSelection();
                if (drpEnableRoomVMR.Items.FindByValue(xmldoc.SelectSingleNode("//GetOrgOptions/EnableRoomVMR").InnerText) != null)
                    drpEnableRoomVMR.Items.FindByValue(xmldoc.SelectSingleNode("//GetOrgOptions/EnableRoomVMR").InnerText).Selected = true;

                drpEnableExternalVMR.ClearSelection();
                if (drpEnableExternalVMR.Items.FindByValue(xmldoc.SelectSingleNode("//GetOrgOptions/EnableExternalVMR").InnerText) != null)
                    drpEnableExternalVMR.Items.FindByValue(xmldoc.SelectSingleNode("//GetOrgOptions/EnableExternalVMR").InnerText).Selected = true;

                if (xmldoc.SelectSingleNode("//GetOrgOptions/ShowHideVMR").InnerText.Trim() == "1")
                {
                    tdPersonalVMR.Attributes.Add("style", "display:");
                    tddrpEnablePersonaVMR.Attributes.Add("style", "display:"); 
                    trEnableRoomExternalVRM.Attributes.Add("style", "display:");
                }
                else
                {
                    tdPersonalVMR.Attributes.Add("style", "display:none");
                    tddrpEnablePersonaVMR.Attributes.Add("style", "display:none"); 
                    trEnableRoomExternalVRM.Attributes.Add("style", "display:none");
                }

                //ZD 100707 END

                //ZD 100263 Starts
                if (xmldoc.SelectSingleNode("//GetOrgOptions/EnableFileWhiteList") != null)
                {
                    if (xmldoc.SelectSingleNode("//GetOrgOptions/EnableFileWhiteList").InnerText.Trim() == "1")
                        chkWhiteList.Checked = true;
                    else
                        chkWhiteList.Checked = false;
                }

                if (xmldoc.SelectSingleNode("//GetOrgOptions/FileWhiteList") != null)
                {
                    ListItem item = null;
                    hdnFileWhiteList.Value = xmldoc.SelectSingleNode("//GetOrgOptions/FileWhiteList").InnerText.Trim();
                    string[] FileList = xmldoc.SelectSingleNode("//GetOrgOptions/FileWhiteList").InnerText.Trim().Split(';');
                    for (int k = 0; k < FileList.Length; k++)
                    {
                        if (FileList[k].Trim() != "")
                        {
                            item = new ListItem();
                            item.Text = FileList[k];
                            item.Attributes.Add("title", FileList[k]);
                            lstWhiteList.Items.Add(item);
                        }
                    }
                }

                if (chkWhiteList.Checked)
                {
                    txtWhiteList.Attributes.Add("style", "display:");
                    btnWhiteList.Attributes.Add("style", "display:"); //ZD 100420
                    trWhiteList.Attributes.Add("style", "display:");
                }
                else
                {
                    txtWhiteList.Attributes.Add("style", "display:none");
                    btnWhiteList.Attributes.Add("style", "display:none");//ZD 100420
                    trWhiteList.Attributes.Add("style", "display:none");
                }
                //ZD 100263 End
				 //ZD 100221 Starts
                if (xmldoc.SelectSingleNode("//GetOrgOptions/WebExURL") != null)
                    txtWebURL.Text = xmldoc.SelectSingleNode("//GetOrgOptions/WebExURL").InnerText.Trim();
                if (xmldoc.SelectSingleNode("//GetOrgOptions/WebExSiteID") != null)
                    txtSiteID.Text = xmldoc.SelectSingleNode("//GetOrgOptions/WebExSiteID").InnerText.Trim();
                if (xmldoc.SelectSingleNode("//GetOrgOptions/WebExPartnerID") != null)
                    txtPartnrID.Text = xmldoc.SelectSingleNode("//GetOrgOptions/WebExPartnerID").InnerText.Trim();
                //ZD 100221 Ends
                //ZD 100935 Starts
                drpWebExIng.ClearSelection();
                if (drpWebExIng.Items.FindByValue(xmldoc.SelectSingleNode("//GetOrgOptions/EnableWebExIntg").InnerText) != null)
                    drpWebExIng.Items.FindByValue(xmldoc.SelectSingleNode("//GetOrgOptions/EnableWebExIntg").InnerText).Selected = true;
                Session.Add("EnableWebExIntg", xmldoc.SelectSingleNode("//GetOrgOptions/EnableWebExIntg").InnerText);

                //ZD 100704 Starts
                if (xmldoc.SelectSingleNode("//GetOrgOptions/EnableExpressConfType") != null)
                {
                    drpEnableExpressConfType.ClearSelection();
                    drpEnableExpressConfType.Items.FindByValue(xmldoc.SelectSingleNode("//GetOrgOptions/EnableExpressConfType").InnerText.Trim()).Selected = true;
                }
                //ZD 100704 End
                //ZD 100834 Start
                if (xmldoc.SelectSingleNode("//GetOrgOptions/EnableDetailedExpressForm") != null)
                {
                    drpDetailedExpressForm.ClearSelection();
                    drpDetailedExpressForm.Items.FindByValue(xmldoc.SelectSingleNode("//GetOrgOptions/EnableDetailedExpressForm").InnerText.Trim()).Selected = true;
                }
                // ZD 100834 End
                //ZD 101120 Start
                if (xmldoc.SelectSingleNode("//GetOrgOptions/GuestLocApprovalTime") != null)
                {
                    drpGLAppTime.ClearSelection();
                    drpGLAppTime.Items.FindByValue(xmldoc.SelectSingleNode("//GetOrgOptions/GuestLocApprovalTime").InnerText.Trim()).Selected = true;
                }
                if (xmldoc.SelectSingleNode("//GetOrgOptions/EnableGuestLocWarningMsg") != null)
                {
                    drpWarningMsg.ClearSelection();
                    drpWarningMsg.Items.FindByValue(xmldoc.SelectSingleNode("//GetOrgOptions/EnableGuestLocWarningMsg").InnerText.Trim()).Selected = true;
                }
                // ZD 101120 End
				//ZD 100899 start
                if (xmldoc.SelectSingleNode("//GetOrgOptions/ScheduleLimit") != null)
                    txtScheduleLimit.Text = xmldoc.SelectSingleNode("//GetOrgOptions/ScheduleLimit").InnerText.Trim();
                //ZD 100899 End
                //ZD 100781 Starts 
                if (xmldoc.SelectSingleNode("//GetOrgOptions/EnablePasswordExp") != null)
                {
                    DrpEnablePwdExp.ClearSelection();
                    DrpEnablePwdExp.Items.FindByValue(xmldoc.SelectSingleNode("//GetOrgOptions/EnablePasswordExp").InnerText.Trim()).Selected = true;

                    if (DrpEnablePwdExp.SelectedValue == "1")
                    {
                        lblPwdExp.Attributes.Add("style", "display:");
                        tdDrpPwdExp.Attributes.Add("style", "display:");
                    }
                    else
                    {
                        lblPwdExp.Attributes.Add("style", "display:none");
                        tdDrpPwdExp.Attributes.Add("style", "display:none");
                    }
                }
                txtPwdExpDays.Text = xmldoc.SelectSingleNode("//GetOrgOptions/PasswordExpDays").InnerText;
                if (txtPwdExpDays.Text == "")
                    txtPwdExpDays.Text = "90";
                //ZD 100781 Ends

				//ZD 100963- START
                if (xmldoc.SelectSingleNode("//GetOrgOptions/EnableRoomCalendarView") != null)
                {
                    drpRoomCalView.ClearSelection();
                    drpRoomCalView.Items.FindByValue(xmldoc.SelectSingleNode("//GetOrgOptions/EnableRoomCalendarView").InnerText.Trim()).Selected = true;
                }
                //ZD 100963- END

                //ZD 101019 START
                if (xmldoc.SelectSingleNode("//GetOrgOptions/VideoSourceURL1") != null)
                    txtVideoSourceURL1.Text = xmldoc.SelectSingleNode("//GetOrgOptions/VideoSourceURL1").InnerText.Trim();

                if (xmldoc.SelectSingleNode("//GetOrgOptions/VideoSourceURL2") != null)
                    txtVideoSourceURL2.Text = xmldoc.SelectSingleNode("//GetOrgOptions/VideoSourceURL2").InnerText.Trim();

                if (xmldoc.SelectSingleNode("//GetOrgOptions/VideoSourceURL3") != null)
                    txtVideoSourceURL3.Text = xmldoc.SelectSingleNode("//GetOrgOptions/VideoSourceURL3").InnerText.Trim();

                if (xmldoc.SelectSingleNode("//GetOrgOptions/VideoSourceURL4") != null)
                    txtVideoSourceURL4.Text = xmldoc.SelectSingleNode("//GetOrgOptions/VideoSourceURL4").InnerText.Trim();

                string scrPos1="";
                if (node.SelectSingleNode("//GetOrgOptions/ScreenPosition1") != null)
                {
                    if (node.SelectSingleNode("//GetOrgOptions/ScreenPosition1").InnerText !="")
                        scrPos1 = node.SelectSingleNode("//GetOrgOptions/ScreenPosition1").InnerText.ToString();

                    if (scrPos1.IndexOf("1") > -1)
                        chkPosA1.Checked = true;
                    
                    if (scrPos1.IndexOf("2") > -1)
                        chkPosB1.Checked = true;
                    
                    if (scrPos1.IndexOf("3") > -1)
                        chkPosC1.Checked = true;
                    
                    if (scrPos1.IndexOf("4") > -1)
                        chkPosD1.Checked = true;
                                      
                }

                string scrPos2 = "";
                if (node.SelectSingleNode("//GetOrgOptions/ScreenPosition2") != null)
                {
                    if (node.SelectSingleNode("//GetOrgOptions/ScreenPosition2").InnerText != "")
                        scrPos2 = node.SelectSingleNode("//GetOrgOptions/ScreenPosition2").InnerText.ToString();

                    if (scrPos2.IndexOf("1") > -1)
                        chkPosA2.Checked = true;
                    
                    if (scrPos2.IndexOf("2") > -1)
                        chkPosB2.Checked = true;
                    
                    if (scrPos2.IndexOf("3") > -1)
                        chkPosC2.Checked = true;
                    
                    if (scrPos2.IndexOf("4") > -1)
                        chkPosD2.Checked = true;
                    
                }

                string scrPos3 = "";
                if (node.SelectSingleNode("//GetOrgOptions/ScreenPosition3") != null)
                {
                    if (node.SelectSingleNode("//GetOrgOptions/ScreenPosition3").InnerText != "")
                        scrPos3 = node.SelectSingleNode("//GetOrgOptions/ScreenPosition3").InnerText.ToString();

                    if (scrPos3.IndexOf("1") > -1)
                        chkPosA3.Checked = true;
                    
                    if (scrPos3.IndexOf("2") > -1)
                        chkPosB3.Checked = true;
                    
                    if (scrPos3.IndexOf("3") > -1)
                        chkPosC3.Checked = true;
                    
                    if (scrPos3.IndexOf("4") > -1)
                        chkPosD3.Checked = true;
                    
                }

                string scrPos4 = "";
                if (node.SelectSingleNode("//GetOrgOptions/ScreenPosition4") != null)
                {
                    if (node.SelectSingleNode("//GetOrgOptions/ScreenPosition1").InnerText != "")
                        scrPos4 = node.SelectSingleNode("//GetOrgOptions/ScreenPosition4").InnerText.ToString();

                    if (scrPos4.IndexOf("1") > -1)
                        chkPosA4.Checked = true;
                    
                    if (scrPos4.IndexOf("2") > -1)
                        chkPosB4.Checked = true;
                    
                    if (scrPos4.IndexOf("3") > -1)
                        chkPosC4.Checked = true;
                    
                    if (scrPos4.IndexOf("4") > -1)
                        chkPosD4.Checked = true;
                    
                }
                //ZD 101019 END

                //ZD 100522 Starts
                txtVMRPW.Text = xmldoc.SelectSingleNode("//GetOrgOptions/PasswordCharLength").InnerText;
                if (txtVMRPW.Text == "")
                    txtVMRPW.Text = "4";

                if (xmldoc.SelectSingleNode("//GetOrgOptions/VMRPINChange") != null)
                {
                    drpVMRPIN.ClearSelection();
                    drpVMRPIN.Items.FindByValue(xmldoc.SelectSingleNode("//GetOrgOptions/VMRPINChange").InnerText.Trim()).Selected = true;
                }
                //ZD 100522 Ends
                //ZD 101445 - Start
                if (xmldoc.SelectSingleNode("//GetOrgOptions/RoomDenialCommt") != null)
                {
                    DrpRoomDenial.ClearSelection();
                    DrpRoomDenial.Items.FindByValue(xmldoc.SelectSingleNode("//GetOrgOptions/RoomDenialCommt").InnerText.Trim()).Selected = true;
                }
                //ZD 101445 - End
                //ZD 100815 Start
                drpSmartP2PNotify.ClearSelection();
                if (drpSmartP2PNotify.Items.FindByValue(xmldoc.SelectSingleNode("//GetOrgOptions/SmartP2PNotify").InnerText) != null)
                    drpSmartP2PNotify.Items.FindByValue(xmldoc.SelectSingleNode("//GetOrgOptions/SmartP2PNotify").InnerText).Selected = true;
                //ZD 100815 End
                
                //ZD 101562 START
                if (xmldoc.SelectSingleNode("//GetOrgOptions/EnableTemplateBooking") != null)
                {
                    drptempbook.ClearSelection();
                    drptempbook.Items.FindByValue(xmldoc.SelectSingleNode("//GetOrgOptions/EnableTemplateBooking").InnerText.Trim()).Selected = true;
                }
                //ZD 101562 END

                //ZD 101757
                if (xmldoc.SelectSingleNode("//GetOrgOptions/EnableActMsgDelivery") != null)
                {
                    drpActiveMsgDel.ClearSelection();
                    drpActiveMsgDel.Items.FindByValue(xmldoc.SelectSingleNode("//GetOrgOptions/EnableActMsgDelivery").InnerText.Trim()).Selected = true;
                }
				//ZD 101757
                String McuEnhanceLimit = "0";
                if (Session["McuEnchancedLimit"] != null)
                   McuEnhanceLimit = Session["McuEnchancedLimit"].ToString();

                if (Int32.Parse(McuEnhanceLimit) <= 0)
                {
                    drpActiveMsgDel.SelectedValue = "0";
                    tdActMsgDel.Attributes.Add("style", "display:None");
                    tdCtlActMsgDel.Attributes.Add("style", "display:None");
                }
                else
                {
                    tdActMsgDel.Attributes.Add("style", "display:");
                    tdCtlActMsgDel.Attributes.Add("style", "display:");
                }

                if (drpActiveMsgDel.SelectedValue == "0")
                {
                    tdTextmsg.Attributes.Add("style", "display:None");
                    tdMsgPopup.Attributes.Add("style", "display:None");
                }
                else
                {
                    tdTextmsg.Attributes.Add("style", "display:");
                    tdMsgPopup.Attributes.Add("style", "display:");
                }
                //ZD 101228 Starts
                txtAVWOAlert.Text = xmldoc.SelectSingleNode("//GetOrgOptions/AVWOAlertTime").InnerText;
                if (txtAVWOAlert.Text == "")
                    txtAVWOAlert.Text = "0";
                txtCatWOAlert.Text = xmldoc.SelectSingleNode("//GetOrgOptions/CatWOAlertTime").InnerText;
                if (txtCatWOAlert.Text == "")
                    txtCatWOAlert.Text = "0";
                txtFacWOAlert.Text = xmldoc.SelectSingleNode("//GetOrgOptions/FacilityWOAlertTime").InnerText;
                if (txtFacWOAlert.Text == "")
                    txtFacWOAlert.Text = "0";
                //ZD 101228 Ends
                //ZD 101755 start
                DrpSetupTimeDisplay.ClearSelection();
                if (xmldoc.SelectSingleNode("//GetOrgOptions/EnableSetupTimeDisplay") != null)
                    DrpSetupTimeDisplay.Items.FindByValue(xmldoc.SelectSingleNode("//GetOrgOptions/EnableSetupTimeDisplay").InnerText.Trim()).Selected = true;

                DrpTeardownTimeDisplay.ClearSelection();
                if (xmldoc.SelectSingleNode("//GetOrgOptions/EnableTeardownTimeDisplay") != null)
                   DrpTeardownTimeDisplay.Items.FindByValue(xmldoc.SelectSingleNode("//GetOrgOptions/EnableTeardownTimeDisplay").InnerText.Trim()).Selected = true;
                //ZD 101755 End

                //ZD 101931 start
                DrpShowvideoLayout.ClearSelection();
                if (xmldoc.SelectSingleNode("//GetOrgOptions/ShowVideoLayout") != null)
                    DrpShowvideoLayout.Items.FindByValue(xmldoc.SelectSingleNode("//GetOrgOptions/ShowVideoLayout").InnerText.Trim()).Selected = true;
                //ZD 101931 End
                txtMultipleAssistant.Text = ""; //ZD 102085
                if (xmldoc.SelectSingleNode("//GetOrgOptions/EWSConfAdmins") != null)
                    txtMultipleAssistant.Text = xmldoc.SelectSingleNode("//GetOrgOptions/EWSConfAdmins").InnerText;

                //ZD 102356
                drpCalDefaultDisplay.ClearSelection();
                if (xmldoc.SelectSingleNode("//GetOrgOptions/EnableCalDefaultDisplay") != null)
                    drpCalDefaultDisplay.Items.FindByValue(xmldoc.SelectSingleNode("//GetOrgOptions/EnableCalDefaultDisplay").InnerText.Trim()).Selected = true;
                

                //ZD 102532 START
                if (xmldoc.SelectSingleNode("//GetOrgOptions/EnableWaitList") != null && xmldoc.SelectSingleNode("//GetOrgOptions/EnableWaitList").InnerText.Trim() == "1")
                {
                    chkEnableWaitList.Checked = true;
                }
                else
                    chkEnableWaitList.Checked = false;
                //ZD 102532 END

                //ZD 103046 - Start				
				node = xmldoc.SelectSingleNode("//GetOrgOptions/PersonnelAlert");
                if (node != null)
                {
                    ListItem item = null;
                    hdnPersonnelAlert.Value = node.InnerText.Trim();
                    string[] PersonnelList = node.InnerText.Trim().Split(';');
                    for (int k = 0; k < PersonnelList.Length; k++)
                    {
                        if (PersonnelList[k].Trim() != "")
                        {
                            item = new ListItem();
                            item.Text = PersonnelList[k];
                            item.Attributes.Add("title", PersonnelList[k]);
                            lstPersonnelAlert.Items.Add(item);                            
                        }
                    }
                }
                //ZD 103046 - End
                //ZD 102514 Start 
                if (xmldoc.SelectSingleNode("//GetOrgOptions/DedicatedVNOCOperatorBuffer") != null)
                    txtDedicatedVNOCOperatorBuffer.Text = xmldoc.SelectSingleNode("//GetOrgOptions/DedicatedVNOCOperatorBuffer").InnerText;
                if (xmldoc.SelectSingleNode("//GetOrgOptions/OnSiteAVSupportBuffer") != null)
                    txtOnSiteAVSupportBuffer.Text = xmldoc.SelectSingleNode("//GetOrgOptions/OnSiteAVSupportBuffer").InnerText;
                if (xmldoc.SelectSingleNode("//GetOrgOptions/CallMonitoringBuffer") != null)
                    txtCallMonitoringBuffer.Text = xmldoc.SelectSingleNode("//GetOrgOptions/CallMonitoringBuffer").InnerText;
                //ZD 102514 End
                // ZD 103216 Start
                TravelAvodTrck.ClearSelection();
                if (xmldoc.SelectSingleNode("//GetOrgOptions/EnableTravelAvoidTrack") != null)
                    TravelAvodTrck.Items.FindByValue(xmldoc.SelectSingleNode("//GetOrgOptions/EnableTravelAvoidTrack").InnerText.Trim()).Selected = true;
                // ZD 103216 End

                //ZD 103550 - Start
                DrpEnableBJNIntegration.ClearSelection();
                if (xmldoc.SelectSingleNode("//GetOrgOptions/EnableBJNIntegration") != null)
                    DrpEnableBJNIntegration.Items.FindByValue(xmldoc.SelectSingleNode("//GetOrgOptions/EnableBJNIntegration").InnerText.Trim()).Selected = true;

                string BJNMeetingType = "1";
                RdBJNMeetingID.ClearSelection();
                if (xmldoc.SelectSingleNode("//GetOrgOptions/BJNMeetingType") != null)
                    BJNMeetingType = xmldoc.SelectSingleNode("//GetOrgOptions/BJNMeetingType").InnerText.Trim();

                RdBJNMeetingID.Items.FindByValue(BJNMeetingType).Selected = true;

                if (xmldoc.SelectSingleNode("//GetOrgOptions/BJNSelectOption") != null && xmldoc.SelectSingleNode("//GetOrgOptions/BJNSelectOption").InnerText.Trim() == "1")                
                    ChkSelectable.Checked = true;                
                else
                    ChkSelectable.Checked = false;
                //ZD 103550 - End
                //ALLDEV-782 Starts
                drpPexipInt.ClearSelection();
                if (xmldoc.SelectSingleNode("//GetOrgOptions/EnablePexipIntegration") != null)
                    drpPexipInt.Items.FindByValue(xmldoc.SelectSingleNode("//GetOrgOptions/EnablePexipIntegration").InnerText.Trim()).Selected = true;

                BJNMeetingType = "1";
                rdPexipMtg.ClearSelection();
                if (xmldoc.SelectSingleNode("//GetOrgOptions/PexipMeetingType") != null)
                    BJNMeetingType = xmldoc.SelectSingleNode("//GetOrgOptions/PexipMeetingType").InnerText.Trim();

                rdPexipMtg.Items.FindByValue(BJNMeetingType).Selected = true;

                if (xmldoc.SelectSingleNode("//GetOrgOptions/PexipSelectOption") != null && xmldoc.SelectSingleNode("//GetOrgOptions/PexipSelectOption").InnerText.Trim() == "1")
                    chkPexipSelect.Checked = true;                
                else
                    chkPexipSelect.Checked = false;

                drpPexipDisplay.ClearSelection();
                if (xmldoc.SelectSingleNode("//GetOrgOptions/PexipDisplay") != null)
                    drpPexipDisplay.Items.FindByValue(xmldoc.SelectSingleNode("//GetOrgOptions/PexipDisplay").InnerText.Trim()).Selected = true;

                //ALLDEV-782 Ends
                // ZD 104151 Start
                lstEmptyconf.Items.Clear();
                ListItem item1 = null;
                if (xmldoc.SelectSingleNode("//GetOrgOptions/EmptyConferencePush/MCU") != null)
                {
                    nodelist = xmldoc.SelectNodes("//GetOrgOptions/EmptyConferencePush/MCU");
                    XmlNodeList iCALNodes = xmldoc.SelectNodes("descendant::MCU[Selected='1']");
                    try
                    {
                        item1 = new ListItem(obj.GetTranslatedText("None"), "-1");
                        if (iCALNodes.Count == 0)
                            item1.Selected = true;
                        lstEmptyconf.Items.Add(item1);

                        for (i = 0; i < nodelist.Count; i++)
                        {
                            item1 = null;

                            item1 = new ListItem(nodelist.Item(i).SelectSingleNode("Name").InnerText, nodelist.Item(i).SelectSingleNode("ID").InnerText);
                            if (nodelist.Item(i).SelectSingleNode("Selected").InnerText == "1")
                                item1.Selected = true;
                            lstEmptyconf.Items.Add(item1);
                        }
                    }
                    catch (Exception)
                    {

                    }
                }
                else
                {
                    item1 = new ListItem(obj.GetTranslatedText("None"), "-1");
                    item1.Selected = true;
                    lstEmptyconf.Items.Add(item1);
                }
                // ZD 104151 End

                //ZD 104116 - Start
                DrpBJNDisplay.ClearSelection();
                if (xmldoc.SelectSingleNode("//GetOrgOptions/BJNDisplay") != null)
                    DrpBJNDisplay.Items.FindByValue(xmldoc.SelectSingleNode("//GetOrgOptions/BJNDisplay").InnerText.Trim()).Selected = true;
                //ZD 104116 - End

                //ZD 104221 start
                lstEnablePoolOrderSelection.ClearSelection();
                if (lstEnablePoolOrderSelection.Items.FindByValue(xmldoc.SelectSingleNode("//GetOrgOptions/EnablePoolOrderSelection").InnerText) != null)
                    lstEnablePoolOrderSelection.Items.FindByValue(xmldoc.SelectSingleNode("//GetOrgOptions/EnablePoolOrderSelection").InnerText).Selected = true;
                //ZD 104221 End

                //ZD 102916 Start
                  DrpAssignParttoRoom.ClearSelection();
                  if (xmldoc.SelectSingleNode("//GetOrgOptions/AssignParticipantsToRoom") != null)
                  DrpAssignParttoRoom.Items.FindByValue(xmldoc.SelectSingleNode("//GetOrgOptions/AssignParticipantsToRoom").InnerText).Selected = true;
                //ZD 102916 End

                  //ZD 104854-Disney Start
                  drpEnableAudbridgefreebusy.ClearSelection();
                  if (xmldoc.SelectSingleNode("//GetOrgOptions/EnableAudbridgefreebusy") != null)
                      drpEnableAudbridgefreebusy.Items.FindByValue(xmldoc.SelectSingleNode("//GetOrgOptions/EnableAudbridgefreebusy").InnerText).Selected = true;
                //ZD 104854-Disney End
				//ZD 104862 Start
                  DrpEnBlkusrDataImport.ClearSelection();
                  if (xmldoc.SelectSingleNode("//GetOrgOptions/EnableBlockUserDI") != null)
                      DrpEnBlkusrDataImport.Items.FindByValue(xmldoc.SelectSingleNode("//GetOrgOptions/EnableBlockUserDI").InnerText).Selected = true;
                //ZD 104862 End
                //ALLDEV-828 START
                  DrpDwnSendRoomResourceiCal.ClearSelection();
                  if (xmldoc.SelectSingleNode("//GetOrgOptions/EnableSendRoomResourceiCal") != null)
                      DrpDwnSendRoomResourceiCal.Items.FindByValue(xmldoc.SelectSingleNode("//GetOrgOptions/EnableSendRoomResourceiCal").InnerText).Selected = true;
                //ALLDEV-828 END

                //ALLDEV-826 Starts
                drpEnableHostandGuestPwd.ClearSelection();
                if (xmldoc.SelectSingleNode("//GetOrgOptions/EnableHostGuestPwd") != null)
                    drpEnableHostandGuestPwd.Items.FindByValue(xmldoc.SelectSingleNode("//GetOrgOptions/EnableHostGuestPwd").InnerText).Selected = true;
                //ALLDEV-826 Ends
                //ALLDEV-837 Starts
                drpDwnEnableSyncLaunchBuffer.ClearSelection();
                if (xmldoc.SelectSingleNode("//GetOrgOptions/EnableSyncLaunchBuffer") != null)
                    drpDwnEnableSyncLaunchBuffer.Items.FindByValue(xmldoc.SelectSingleNode("//GetOrgOptions/EnableSyncLaunchBuffer").InnerText).Selected = true;
                
                if (xmldoc.SelectSingleNode("//GetOrgOptions/SyncLaunchBuffer") != null)
                    txtLaunchBuffer.Text = xmldoc.SelectSingleNode("//GetOrgOptions/SyncLaunchBuffer").InnerText;

                if (drpDwnEnableSyncLaunchBuffer.SelectedValue == "1")
                {
                    tdLaunchBuffer.Attributes.Add("style", "display:");
                    tdTxtLaunchBuffer.Attributes.Add("style", "display:");
                }
                else
                {
                    tdLaunchBuffer.Attributes.Add("style", "display:none");
                    tdTxtLaunchBuffer.Attributes.Add("style", "display:none");
                }
                //ALLDEV-837 Ends
                //ALLDEV-498 Starts
                if (xmldoc.SelectSingleNode("//GetOrgOptions/ConfAdministrator") != null)
                    txtAdminID.Text = xmldoc.SelectSingleNode("//GetOrgOptions/ConfAdministrator").InnerText;
                if (xmldoc.SelectSingleNode("//GetOrgOptions/DefinedAdminName") != null)
                    txtConfAdmin.Text = xmldoc.SelectSingleNode("//GetOrgOptions/DefinedAdminName").InnerText;
                //ALLDEV-498 Ends

				//ALLDEV-839 start
               	DrpAllowReqtoEdit.ClearSelection();
                if (xmldoc.SelectSingleNode("//GetOrgOptions/AllowRequestortoEdit") != null)
                    DrpAllowReqtoEdit.Items.FindByValue(xmldoc.SelectSingleNode("//GetOrgOptions/AllowRequestortoEdit").InnerText).Selected = true;
                //ALLDEV-839 End
                //ALLDEV-834 Starts
                DrpDwnEnableRoomSelection.ClearSelection();
                if (DrpDwnEnableRoomSelection.Items.FindByValue(xmldoc.SelectSingleNode("//GetOrgOptions/EnableRoomSelection").InnerText) != null)
                    DrpDwnEnableRoomSelection.Items.FindByValue(xmldoc.SelectSingleNode("//GetOrgOptions/EnableRoomSelection").InnerText).Selected = true;
                //ALLDEV-834 Ends
				//ALLDEV-833 START
                  DrpEWSPartyFreeBusy.ClearSelection();
                  if (xmldoc.SelectSingleNode("//GetOrgOptions/EnableEWSPartyFreeBusy") != null)
                      DrpEWSPartyFreeBusy.Items.FindByValue(xmldoc.SelectSingleNode("//GetOrgOptions/EnableEWSPartyFreeBusy").InnerText).Selected = true;
                //ALLDEV-833 END
                //ALLDEV-862 START
                drpEnableS4BEWS.ClearSelection();
                if (xmldoc.SelectSingleNode("//GetOrgOptions/EnableS4BEWS") != null)
                    drpEnableS4BEWS.Items.FindByValue(xmldoc.SelectSingleNode("//GetOrgOptions/EnableS4BEWS").InnerText).Selected = true;
                //ALLDEV-862 END
            }
            catch (Exception ex)
            {
                log.Trace("BindData: " + ex.Message + " : " + ex.StackTrace);
            }
        }

        #endregion

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            InitializeUIComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            //            this.Load += new System.EventHandler(this.Page_Load);

        }

        private void InitializeUIComponent()
        {

        }
        #endregion

        protected void SetOrgOptions()
        {
            try
            {
                //ZD 104391 - Start
                string ctName = "", ctEmail = "", ctPhone = "", ctAddInfo = "";

                ctName = RemoveEndofBackslash(ContactName.Text);
                ctEmail = RemoveEndofBackslash(ContactEmail.Text);
                ctPhone = RemoveEndofBackslash(ContactPhone.Text);
                ctAddInfo = RemoveEndofBackslash(ContactAdditionInfo.Text);
                //ZD 104391 - End

                string inXML = "", errorAddress = string.Empty; //ZD 102085
                inXML += "<SetOrgOptions>";
                inXML += obj.OrgXMLElement(); //Organization Module
                inXML += "  <ContactDetails>";
                inXML += "      <Name>" + utilObj.ReplaceInXMLSpecialCharacters(ctName) + "</Name>"; //ZD 104391
                inXML += "      <Email>" + utilObj.ReplaceInXMLSpecialCharacters(ctEmail) + "</Email>"; //ZD 104391
                inXML += "      <Phone>" + utilObj.ReplaceInXMLSpecialCharacters(ctPhone) + "</Phone>"; //ZD 104391
                inXML += "      <AdditionInfo>" + utilObj.ReplaceInXMLSpecialCharacters(ctAddInfo) + "</AdditionInfo>"; //ZD 104391
                inXML += "  </ContactDetails>";
                //inXML += "  <SystemTimeZoneID>" + PreferTimezone.SelectedValue + "</SystemTimeZoneID>";
                inXML += "  <TimezoneSystemID>" + TimezoneSystems.SelectedValue + "</TimezoneSystemID>";
                inXML += "  <AutoAcceptModifiedConference>" + AutoAcpModConf.SelectedValue + "</AutoAcceptModifiedConference>";
                inXML += "  <EnableRecurringConference>" + RecurEnabled.SelectedValue + "</EnableRecurringConference>";
                inXML += "  <EnableDynamicInvite>" + DynamicInviteEnabled.SelectedValue + "</EnableDynamicInvite>";
                inXML += "  <EnableP2PConference>" + p2pConfEnabled.SelectedValue + "</EnableP2PConference>";
                inXML += "  <iCalReqEmailId>" + txtiCalEmailId.Text.Trim() + "</iCalReqEmailId>";//FB 1786
                inXML += "  <showBridgeExt>" + drpBrdgeExt.SelectedValue + "</showBridgeExt>";//FB 2610
                inXML += "  <EnableRealtimeDisplay>" + RealtimeType.SelectedValue + "</EnableRealtimeDisplay>";
                inXML += "  <EnableDialout>" + DialoutEnabled.SelectedValue + "</EnableDialout>";
                inXML += "  <DefaultConferencesAsPublic>" + DefaultPublic.SelectedValue + "</DefaultConferencesAsPublic>";
                inXML += "  <DefaultConferenceType>" + lstDefaultConferenceType.SelectedValue + "</DefaultConferenceType>";
                inXML += "  <RFIDTagValue>" + lstRFIDValue.SelectedValue + "</RFIDTagValue>";//FB 2724
                inXML += "  <iControlTimeout>" + txtiControlTimeout.Text.Trim() + "</iControlTimeout>";//FB 2724
                inXML += "  <VideoRefreshTimer>" + txtVideoRefreshTimer.Text.Trim() + "</VideoRefreshTimer>";//ZD 103398
                inXML += "  <EnableRoomConference>" + lstEnableRoomConference.SelectedValue + "</EnableRoomConference>";
                inXML += "  <EnableAudioVideoConference>" + lstEnableAudioVideoConference.SelectedValue + "</EnableAudioVideoConference>";
                inXML += "  <EnableAudioOnlyConference>" + lstEnableAudioOnlyConference.SelectedValue + "</EnableAudioOnlyConference>";
                inXML += "  <EnableHotdeskingConference>" + lstEnableHotdeskingConference.SelectedValue + "</EnableHotdeskingConference>";//ZD 100719
                inXML += "  <EnableWETConference>" + drpOBTPConf.SelectedValue + "</EnableWETConference>";//ZD 100513
                inXML += "  <WebExLaunch>" + drpWebExMode.SelectedValue + "</WebExLaunch>";//ZD 100513
                inXML += "  <EnableNumericID>" + lstEnableNumericID.SelectedValue + "</EnableNumericID>";//FB 2870
                inXML += "  <DefaultCalendarToOfficeHours>" + lstDefaultOfficeHours.SelectedValue + "</DefaultCalendarToOfficeHours>";
                inXML += "  <RoomTreeExpandLevel>" + lstRoomTreeLevel.SelectedValue + "</RoomTreeExpandLevel>";
                //Code added for FB 1470
                inXML += "  <EnableEntity></EnableEntity>";
                inXML += "  <EnableBufferZone>" + EnableBufferZone.SelectedValue + "</EnableBufferZone>";
                inXML += "  <SetupTime>" + txtSetupTime.Text.Trim() + "</SetupTime>"; //FB 2398
                inXML += "  <TearDownTime>" + txtTearDownTime.Text.Trim() + "</TearDownTime>";
                //FB-1642 Audio Add On Starts..
                inXML += "<ConferenceCode>" + DrpConfcode.SelectedValue + "</ConferenceCode>";
                inXML += "<LeaderPin>" + DrpLedpin.SelectedValue + "</LeaderPin>";
                inXML += "<AdvAvParams>" + Drpavprm.SelectedValue + "</AdvAvParams>";
                inXML += "<AudioParams >" + DrpAudprm.SelectedValue + "</AudioParams >";
                //FB-1642 Audio Add On Ends..
                inXML += "<sendIcal >" + DrpListIcal.SelectedValue + "</sendIcal>";//FB 1782
                inXML += "<sendApprovalIcal>" + DrpAppIcal.SelectedValue + "</sendApprovalIcal>";//FB 1782
                inXML += "<isVIP >" + DrpVIP.SelectedValue + "</isVIP>";
                inXML += "<isUniquePassword>" + DrpUniquePassword.SelectedValue + "</isUniquePassword>";
                inXML += "<isAssignedMCU>" + DrpDwnListAssignedMCU.SelectedValue + "</isAssignedMCU>";// FB 1901
                inXML += "<ReminderMask>" + buildRemainderMask() + "</ReminderMask>";// FB 1926
                inXML += "<isMultiLingual>" + DrpDwnListMultiLingual.SelectedValue + "</isMultiLingual>";//FB 1830 - Translation
                inXML += "<PIMNotifications>" + DrpPluginConfirm.SelectedValue + "</PIMNotifications>";// FB 2141
                inXML += "<ExternalAttachments>" + DrpDwnAttachmnts.SelectedValue + "</ExternalAttachments>";// FB 2154
                inXML += "<filterTelepresence>" + DrpDwnFilterTelepresence.SelectedValue + "</filterTelepresence>";// FB 2170
                //ZD 102159
                //inXML += "<EnableRoomServiceType>" + lstEnableRoomServiceType.SelectedValue + "</EnableRoomServiceType>";//FB 2219
                inXML += "<EnableRoomServiceType>0</EnableRoomServiceType>";//FB 2219
                inXML += "<isSpecialRecur>" + DrpDwnListSpRecur.SelectedValue + "</isSpecialRecur>";// FB 2052                                
				inXML += "<isDeptUser>" + DrpDwnListDeptUser.SelectedValue + "</isDeptUser>";// FB 2269                                
				inXML += "<EnablePIMServiceType>" + lstEnablePIMServiceType.SelectedValue + "</EnablePIMServiceType>";//FB 2038
                inXML += "<EnableImmConf>" + lstEnableImmediateConference.SelectedValue + "</EnableImmConf>";//FB 2036                
                inXML += "<EnablePasswordRule>" + DrpDwnPasswordRule.SelectedValue + "</EnablePasswordRule>";//FB 2339                               
                inXML += "<EnableAudioBridges>" + lstEnableAudioBridges.SelectedValue + "</EnableAudioBridges>";//FB 2023 
                inXML += "<EnablePartyCode>" + drpPartyCode.SelectedValue + "</EnablePartyCode>";//ZD 101446
                inXML += "<DedicatedVideo>" + DrpDwnDedicatedVideo.SelectedValue + "</DedicatedVideo>";// FB 2334	
                //FB 2359 Start
                inXML += "<EnableConferencePassword>" + lstEnableConfPassword.SelectedValue + "</EnableConferencePassword>";
                inXML += "<EnablePublicConference>" + lstEnablePublicConf.SelectedValue + "</EnablePublicConference>";
                inXML += "<EnableRoomParam>" + lstRoomprm.SelectedValue + "</EnableRoomParam>";
                //FB 2359 End
                inXML += "<EnableVMR>" + lstEnableVMR.SelectedValue + "</EnableVMR>";//VMR 
                inXML += "<EnableEPDetails>" + lstEPinMail.SelectedValue + "</EnableEPDetails>";//FB 2401
                inXML += "<MCUAlert>" + lstMcuAlert.SelectedValue + "</MCUAlert>";//FB 2472
                //FB 2136 start
                //inXML += "<EnableSecurityBadge>" + drpenablesecuritybadge.SelectedValue + "</EnableSecurityBadge>";
                //inXML += "<SecurityBadgeType>" + drpsecuritybadgetype.SelectedValue + "</SecurityBadgeType>";
                //inXML += "<SecurityDeskEmailId>" + txtsecdeskemailid.Text + "</SecurityDeskEmailId>";
                //FB 2136 end
                //FB 2348 Start
                inXML += "<EnableSurvey>" + drpenablesurvey.SelectedValue + "</EnableSurvey>";
               // inXML += "<SurveyOption>" + drpsurveyoption.SelectedValue + "</SurveyOption>";
                // ZD 103416 Start
                inXML += "<SurveyOption>2</SurveyOption>";
               // if (drpsurveyoption.SelectedValue == "2" && drpenablesurvey.SelectedValue == "1")
                if (drpenablesurvey.SelectedValue == "1")
                {
                    inXML += "<SurveyURL>" + txtSurWebsiteURL.Text.Trim() + "</SurveyURL>";
                    inXML += "<TimeDuration>" + txtTimeDur.Text.Trim() + "</TimeDuration>";
                }
                else
                {
                    inXML += "<SurveyURL>" + hdnSurURL.Value + "</SurveyURL>";
                    inXML += "<TimeDuration>" + hdnTimeDur.Value + "</TimeDuration>";
                }
                // ZD 103416 End
                //FB 2348 end

                //FB 2335 start
                inXML += "<defPolycomRMXLO>" + Poly2RMX.Value + "</defPolycomRMXLO>";
                inXML += "<defPolycomMGCLO>" + Poly2MGC.Value + "</defPolycomMGCLO>";
                inXML += "<defCiscoTPLO>" + CTMS2Poly.Value + "</defCiscoTPLO>";
                inXML += "<defCTMSLO>" + CTMS2Cisco.Value + "</defCTMSLO>";
                //FB 2335 end
                inXML += "<EnableAcceptDecline>" + lstAcceptDecline.SelectedValue + "</EnableAcceptDecline>";//FB 2419
                inXML += "<DefaultLineRate>" + lstLineRate.SelectedValue + "</DefaultLineRate>";//FB 2429
                inXML += "<McuSetupTime>" + txtMCUSetupTime.Text + "</McuSetupTime>";//FB 2440
                inXML += "<MCUTeardonwnTime>" + txtMCUTearDownTime.Text + "</MCUTeardonwnTime>";//FB 2440
				inXML += "<MCUBufferPriority>" + DrpForceMCUBuffer.SelectedValue.Trim() + "</MCUBufferPriority>";//FB 2440
                inXML += "<MCUSetupDisplay>" + MCUSetupDisplay.SelectedValue.Trim() + "</MCUSetupDisplay>";//FB 2998
                inXML += "<MCUTearDisplay>" + MCUTearDisplay.SelectedValue.Trim() + "</MCUTearDisplay>";//FB 2998

                //FB 2426 Start
                inXML += "<OnflyTier>";
                inXML += "<DefTopTier>" + lstTopTier.SelectedItem.Text.Trim() + "</DefTopTier>";
                inXML += "<DefMiddleTier>" + lstMiddleTier.SelectedItem.Text.Trim() + "</DefMiddleTier>";
                inXML += "<OnflyTopTierID>" + lstTopTier.SelectedValue + "</OnflyTopTierID>";
                inXML += "<OnflyMiddleTierID>" + lstMiddleTier.SelectedValue + "</OnflyMiddleTierID>";
                inXML += "</OnflyTier>";
                //FB 2426 End

                inXML += "<AssignParticipantsToRoom>" + DrpAssignParttoRoom.SelectedValue + "</AssignParticipantsToRoom>"; //ZD 102916
                inXML += "<EnableAudbridgefreebusy>" + drpEnableAudbridgefreebusy.SelectedValue + "</EnableAudbridgefreebusy>"; //ZD 104854-Disney

                //ZD 100068 start
                inXML += "<VMRTier>";
                inXML += "<VMRTopTierID>" + lstVMRTopTier.SelectedValue + "</VMRTopTierID>";
                inXML += "<VMRMiddleTierID>" + lstVMRMiddleTier.SelectedValue + "</VMRMiddleTierID>";
                inXML += "</VMRTier>";

                //ZD 100068 end

                inXML += "<EnableSmartP2P>" + lstEnableSmartP2P.SelectedValue + "</EnableSmartP2P>";//FB 2430
                //FB 2469 Starts
                inXML += "<EnableConfTZinLoc>" + lstEnableConfTZinLoc.SelectedValue + "</EnableConfTZinLoc>";
                inXML += "<SendConfirmationEmail>" + lstSendConfirmationEmail.SelectedValue + "</SendConfirmationEmail>";
                //FB 2469 End
                inXML += "<DefaultConfDuration>" + txtDefaultConfDuration.Text.Trim() + "</DefaultConfDuration>";//FB 2501
                //FB 2571 Start
                inXML += "<EnableFECC>" + lstenableFECC.SelectedValue + "</EnableFECC>";
                inXML += "<DefaultFECC>" + lstdefaultFECC.SelectedValue + "</DefaultFECC>";
                //FB 2571 End

                //FB 2598 Starts
                inXML += "<EnableCallmonitor>" + DrpDwnEnableCallmonitor.SelectedValue + "</EnableCallmonitor>";// DD Feature                                
                inXML += "<EnableEM7>" + DrpDwnEnableEM7.SelectedValue + "</EnableEM7>";
                inXML += "<EnableCDR>" + DrpDwnEnableCDR.SelectedValue + "</EnableCDR>";                                
                //FB 2598 Ends
                inXML += "<EnableDialPlan>" + DrpEnableE164DialPlan.SelectedValue + "</EnableDialPlan>"; // FB 2636                                
                //FB 2550 Starts
                int VMRtype=0;
                Int32.TryParse(lstEnableVMR.SelectedValue.ToString(),out VMRtype);
                if (!((DynamicInviteEnabled.SelectedValue.ToString().Equals("1")) && VMRtype > 0))
                    txtMaxVMRParty.Text = "0";
            
                inXML += "<MaxPublicVMRParty>" + txtMaxVMRParty.Text.Trim() + "</MaxPublicVMRParty>";
                //FB 2550 Ends
                inXML += "<MeetandGreetBuffer>" + txtMeetandGreetBuffer.Text.Trim() + "</MeetandGreetBuffer>";//FB 2609

                //FB 2632 Starts
                inXML += "<EnableCongSupport>" + drpCngSupport.SelectedValue + "</EnableCongSupport>";
                
                if (chkMeetandGreet.Checked)
                    inXML += "<MeetandGreetinEmail>1</MeetandGreetinEmail>";
                else
                    inXML += "<MeetandGreetinEmail>0</MeetandGreetinEmail>";
                
                if (chkOnSiteAVSupport.Checked)
                    inXML += "<OnSiteAVSupportinEmail>1</OnSiteAVSupportinEmail>";
                else
                    inXML += "<OnSiteAVSupportinEmail>0</OnSiteAVSupportinEmail>";

                if (chkConciergeMonitoring.Checked)
                    inXML += "<ConciergeMonitoringinEmail>1</ConciergeMonitoringinEmail>";
                else
                    inXML += "<ConciergeMonitoringinEmail>0</ConciergeMonitoringinEmail>";

                if (chkDedicatedVNOCOperator.Checked)
                    inXML += "<DedicatedVNOCOperatorinEmail>1</DedicatedVNOCOperatorinEmail>";
                else
                    inXML += "<DedicatedVNOCOperatorinEmail>0</DedicatedVNOCOperatorinEmail>";
                //FB 2632 Ends
				inXML += "<EnableRoomAdminDetails>" + lstEnableRoomAdminDetails.SelectedValue + "</EnableRoomAdminDetails>";//FB 2631

                //FB 2637 Starts
                string tierIds = "";
                hdnTierIDs.Value = "";
                foreach (ListItem li in lstTier1.Items)
                {
                    if (li.Selected.Equals(true))
                        hdnTierIDs.Value += li.Value + "|";
                }
               
                if (hdnTierIDs.Value.Trim().Length > 0)
                    tierIds = hdnTierIDs.Value.Substring(0, hdnTierIDs.Value.Length - 1);

                inXML += "<AlertforTier1>" + tierIds + "</AlertforTier1>"; //FB 2637
                inXML += "<EnableZulu>" + DropDownZuLu.SelectedValue + "</EnableZulu>";//FB 2588
                //FB 2637 Ends
                //FB 2595 Start
                inXML += "<SecureSwitch>" + drpSecureSwitch.SelectedValue + "</SecureSwitch>";
                inXML += "<HardwareAdminEmail>" + txtHardwareAdminEmail.Text.Trim() + "</HardwareAdminEmail>";
                inXML += "<NetworkSwitching>" + drpNwtSwtiching.SelectedValue + "</NetworkSwitching>";
                inXML += "<NetworkCallLaunch>" + drpNwtCallLaunch.SelectedValue + "</NetworkCallLaunch>";
                inXML += "<SecureLaunchBuffer>" + txtSecureLaunch.Text.Trim() + "</SecureLaunchBuffer>";
                //FB 2595 End
                inXML += "<EnableLinerate>" + drpEnableLineRate.SelectedValue + "</EnableLinerate>";//FB 2641
                inXML += "<EnableStartMode>" + drpEnableStartMode.SelectedValue + "</EnableStartMode>";//FB 2641
                //FB 2670 START
                if (chkOnsiteAV.Checked)
                    inXML += "<EnableOnsiteAV>1</EnableOnsiteAV>";
                else
                    inXML += "<EnableOnsiteAV>0</EnableOnsiteAV>";

                if (chkMeetandGret.Checked)
                    inXML += "<EnableMeetandGreet>1</EnableMeetandGreet>";
                else
                    inXML += "<EnableMeetandGreet>0</EnableMeetandGreet>";

                if (chkConciergeMonitor.Checked)
                    inXML += "<EnableConciergeMonitoring>1</EnableConciergeMonitoring>";
                else
                    inXML += "<EnableConciergeMonitoring>0</EnableConciergeMonitoring>";

                if (chkDedicatedVNOC.Checked)
                    inXML += "<EnableDedicatedVNOC>1</EnableDedicatedVNOC>";
                else
                    inXML += "<EnableDedicatedVNOC>0</EnableDedicatedVNOC>";
                //FB 2670 END
                inXML += "<EnableSingleRoomConfEmails>" + lstSigRoomConf.SelectedValue + "</EnableSingleRoomConfEmails>";//FB 2817

                inXML += "<EnableProfileSelection>" + lstEnableProfileSelection.SelectedValue + "</EnableProfileSelection>";//FB 2839                

                inXML += "<ResponseTimeout>" + txtResponseTimeout.Text.Trim() + "</ResponseTimeout>";//FB 2993
				inXML += "<ShowCusAttInCalendar>" + DrpDwnEnableCA.SelectedValue + "</ShowCusAttInCalendar>";//ZD 100151

                //ZD 100263 Starts
                if (chkWhiteList.Checked)
                {
                    inXML += "<EnableFileWhiteList>1</EnableFileWhiteList>";
                    inXML += "<FileWhiteList>" + hdnFileWhiteList.Value + "</FileWhiteList>";
                }
                else
                {
                    inXML += "<EnableFileWhiteList>0</EnableFileWhiteList>";
                    inXML += "<FileWhiteList></FileWhiteList>";
                }
                //ZD 100263 End
				//ZD 100164 START
                inXML += "<EnableAdvancedUserOption>" + DrpDwnAdvUserOption.SelectedValue + "</EnableAdvancedUserOption>";

                //ZD 100164 END

                //ZD 100707 Start
                inXML += "<ShowHideVMR>" + drpEnableVMR.SelectedValue + "</ShowHideVMR>";
                inXML += "<EnablePersonaVMR>" + ((drpEnableVMR.SelectedValue == "1") ? drpEnablePersonaVMR.SelectedValue : "0") + "</EnablePersonaVMR>";
                inXML += "<EnableRoomVMR>" + ((drpEnableVMR.SelectedValue == "1") ? drpEnableRoomVMR.SelectedValue : "0") + "</EnableRoomVMR>";
                inXML += "<EnableExternalVMR>" + ((drpEnableVMR.SelectedValue == "1") ? drpEnableExternalVMR.SelectedValue : "0") + "</EnableExternalVMR>";
                //ZD 100707 End

				//ZD 100221 Starts
                inXML += "<WebExURL>" + txtWebURL.Text.Trim() + "</WebExURL>";
                inXML += "<WebExSiteID>" + txtSiteID.Text.Trim() + "</WebExSiteID>";
                inXML += "<WebExPartnerID>" + txtPartnrID.Text.Trim() + "</WebExPartnerID>";
                //ZD 100221 Ends
				inXML += "<ScheduleLimit>" + txtScheduleLimit.Text.Trim() + "</ScheduleLimit>";//ZD 100899
                //ZD 100704
                inXML += "<EnableExpressConfType>" + drpEnableExpressConfType.SelectedValue + "</EnableExpressConfType>";
                inXML += "<EnableDetailedExpressForm>" + drpDetailedExpressForm.SelectedValue + "</EnableDetailedExpressForm>"; // ZD 100834
                inXML += "<EnableGuestLocWarningMsg>" + drpWarningMsg.SelectedValue + "</EnableGuestLocWarningMsg>"; // ZD 101120
                inXML += "<GuestLocApprovalTime>" + drpGLAppTime.SelectedValue + "</GuestLocApprovalTime>"; // ZD 101120
                inXML += "<SmartP2PNotify>" + drpSmartP2PNotify.SelectedValue + "</SmartP2PNotify>";//ZD 100815
				if(WebExUserLimit >0)
                inXML += "<EnableWebExIntg>" + drpWebExIng.SelectedValue+ "</EnableWebExIntg>";//ZD 100935
                else
                    inXML += "<EnableWebExIntg>0</EnableWebExIntg>"; //ZD 100935
                //ZD 100781 Starts
                inXML += "<EnablePasswordExp>" + DrpEnablePwdExp.Text + "</EnablePasswordExp>"; 
                if(txtPwdExpDays.Text != "")
                inXML += "<PasswordExpDays>" + txtPwdExpDays.Text + "</PasswordExpDays>"; 
                else
                    inXML += "<PasswordExpDays>90</PasswordExpDays>"; 
                //ZD 100781 Ends
				inXML += "<EnableRoomCalendarView>" + drpRoomCalView.SelectedValue + "</EnableRoomCalendarView>";//ZD 100963
                
                //ZD 101019 START
                if (txtVideoSourceURL1.Text != "")
                    inXML += "<VideoSourceURL1>" + txtVideoSourceURL1.Text + "</VideoSourceURL1>";

                if (txtVideoSourceURL2.Text != "")
                    inXML += "<VideoSourceURL2>" + txtVideoSourceURL2.Text + "</VideoSourceURL2>";

                if (txtVideoSourceURL3.Text != "")
                    inXML += "<VideoSourceURL3>" + txtVideoSourceURL3.Text + "</VideoSourceURL3>";

                if (txtVideoSourceURL4.Text != "")
                    inXML += "<VideoSourceURL4>" + txtVideoSourceURL4.Text + "</VideoSourceURL4>";


                if (chkPosAVert.Value == "1") // Vertical position any one chkbox checked
                    inXML += "<ScreenPosition1>" + chkPosAHor.Value + "</ScreenPosition1>";
                else
                    inXML += "<ScreenPosition1></ScreenPosition1>";

                if (chkPosBVert.Value == "2")
                    inXML += "<ScreenPosition2>" + chkPosBHor.Value + "</ScreenPosition2>";
                else
                    inXML += "<ScreenPosition2></ScreenPosition2>";

                if (chkPosCVert.Value == "3")
                    inXML += "<ScreenPosition3>" + chkPosCHor.Value + "</ScreenPosition3>";
                else
                    inXML += "<ScreenPosition3></ScreenPosition3>";

                if (chkPosDVert.Value == "4")
                    inXML += "<ScreenPosition4>" + chkPosDHor.Value + "</ScreenPosition4>";
                else
                    inXML += "<ScreenPosition4></ScreenPosition4>";
                //ZD 101019 END
                inXML += "  <EnableiControlLogin>" + DrpEnableiControlLogin.SelectedValue + "</EnableiControlLogin>";//ALLDEV-503
                inXML += "  <EnableFindAvailRm>" + DrpEnableFindAvailRm.SelectedValue + "</EnableFindAvailRm>";//ALLDEV-503
                //ZD 100522 Starts
                if (!string.IsNullOrEmpty(txtVMRPW.Text))
                    inXML += "<PasswordCharLength>" + txtVMRPW.Text + "</PasswordCharLength>";
                else
                    inXML += "<PasswordCharLength>4</PasswordCharLength>";

                inXML += "<VMRPINChange>" + drpVMRPIN.SelectedValue + "</VMRPINChange>";
                //ZD 100522 Ends
                inXML += "<RoomDenialCommt>" + DrpRoomDenial.SelectedValue + "</RoomDenialCommt>";//ZD 101445
                inXML += "<EnableTemplateBooking>" + drptempbook.SelectedValue + "</EnableTemplateBooking>";//ZD 101562
                inXML += "<EnableActMsgDelivery>" + drpActiveMsgDel.SelectedValue + "</EnableActMsgDelivery>";//ZD 101757
                //ZD 101228 Strats                
                inXML += "<AVWOAlertTime>" + txtAVWOAlert.Text + "</AVWOAlertTime>";
                inXML += "<CatWOAlertTime>" + txtCatWOAlert.Text + "</CatWOAlertTime>";
                inXML += "<FacilityWOAlertTime>" + txtFacWOAlert.Text + "</FacilityWOAlertTime>";                
                //ZD 101228 Ends
                //ZD 101755 start
                inXML +="<EnableSetupTimeDisplay>" + DrpSetupTimeDisplay.SelectedValue +"</EnableSetupTimeDisplay>";
                inXML +="<EnableTeardownTimeDisplay>" + DrpTeardownTimeDisplay.SelectedValue +"</EnableTeardownTimeDisplay>";
                //ZD 101755  End
				 //ZD 101527 - Start
                inXML += "<EnableRPRMRoomSync>" + DrpRPRMRoomSync.SelectedValue + "</EnableRPRMRoomSync>";
                inXML += "<RoomSyncAuto>" + DrpRoomSyncAuto.SelectedValue + "</RoomSyncAuto>";
                inXML += "<RoomSyncPollTime>" + TxtPollHrs.Text + "</RoomSyncPollTime>";
                //ZD 101527 - End
               
                inXML += "<DefCodianLO>" + hdnCodian.Value +"</DefCodianLO>";//ZD 101869
                inXML += "<ShowVideoLayout>" + DrpShowvideoLayout.SelectedValue + "</ShowVideoLayout>";   //ZD 101931
                inXML += "<EnableCalDefaultDisplay>" + drpCalDefaultDisplay.SelectedValue + "</EnableCalDefaultDisplay>";   //ZD 102356                
                
                if (!CheckUserEmail(txtMultipleAssistant.Text, ref errorAddress)) //ZD 102085
                    if (!string.IsNullOrEmpty(errorAddress)) //104480 
                    {
                        throw new Exception(obj.GetTranslatedText("Please enter a valid email address") + ": " + errorAddress);
                    }
                inXML += "<EWSConfAdmins>" + txtMultipleAssistant.Text + "</EWSConfAdmins>";
                if (chkEnableWaitList.Checked)
                    inXML += "<EnableWaitList>1</EnableWaitList>";//ZD 102532
                else
                    inXML += "<EnableWaitList>0</EnableWaitList>";//ZD 102532

                inXML += "<PersonnelAlert>" + hdnPersonnelAlert.Value + "</PersonnelAlert>"; //ZD 103046
                //ZD 102514 Start
                inXML += "<OnSiteAVSupportBuffer>" + txtOnSiteAVSupportBuffer.Text.Trim() + "</OnSiteAVSupportBuffer>";
                inXML += "<CallMonitoringBuffer>" + txtCallMonitoringBuffer.Text.Trim() + "</CallMonitoringBuffer>";
                inXML += "<DedicatedVNOCOperatorBuffer>" + txtDedicatedVNOCOperatorBuffer.Text.Trim() + "</DedicatedVNOCOperatorBuffer>";
                //ZD 102514 End
                inXML += "<EnableTravelAvoidTrack>" + TravelAvodTrck.SelectedValue + "</EnableTravelAvoidTrack>"; // ZD 103216
                // ZD 103550 - Start
                inXML += "<EnableBJNIntegration>" + DrpEnableBJNIntegration.SelectedValue + "</EnableBJNIntegration>";
                inXML += "<BJNMeetingType>" + RdBJNMeetingID.SelectedValue + "</BJNMeetingType>";
                if (ChkSelectable.Checked)
                    inXML += "<BJNSelectOption>1</BJNSelectOption>";
                else
                    inXML += "<BJNSelectOption>0</BJNSelectOption>";               
                // ZD 103550 - End
                //ALLDEV-782 Starts
                inXML += "<EnablePexipIntegration>" + drpPexipInt.SelectedValue + "</EnablePexipIntegration>";
                inXML += "<PexipMeetingType>" + rdPexipMtg.SelectedValue + "</PexipMeetingType>";
                if (chkPexipSelect.Checked)
                    inXML += "<PexipSelectOption>1</PexipSelectOption>";
                else
                    inXML += "<PexipSelectOption>0</PexipSelectOption>";
                inXML += "<PexipDisplay>" + drpPexipDisplay.SelectedValue + "</PexipDisplay>";
                //ALLDEV-782 Ends
                 inXML += "<EmptyConferencePush>" +  lstEmptyconf.SelectedValue  + "</EmptyConferencePush>"; // ZD 104151
                 //ZD 104116 - Start
                if(DrpEnableBJNIntegration.SelectedValue == "0")
                    inXML += "<BJNDisplay>0</BJNDisplay>";
                else
                    inXML += "<BJNDisplay>" + DrpBJNDisplay.SelectedValue + "</BJNDisplay>";
                //ZD 104116 - End
                inXML += "<EnablePoolOrderSelection>" + lstEnablePoolOrderSelection.SelectedValue + "</EnablePoolOrderSelection>"; //ZD 104221
                inXML += "<EnableBlockUserDI>" + DrpEnBlkusrDataImport.SelectedValue +"</EnableBlockUserDI>";//ZD 104862
                inXML += "<EnableSendRoomResourceiCal>" + DrpDwnSendRoomResourceiCal.SelectedValue + "</EnableSendRoomResourceiCal>";//ALLDEV-828
				inXML += "<AllowRequestortoEdit>" + DrpAllowReqtoEdit.SelectedValue + "</AllowRequestortoEdit>";//ALLDEV-839 
                inXML += "<EnableEWSPartyFreeBusy>" + DrpEWSPartyFreeBusy.SelectedValue + "</EnableEWSPartyFreeBusy>";//ALLDEV-833
                inXML += "<EnableS4BEWS>" + drpEnableS4BEWS.SelectedValue + "</EnableS4BEWS>";//ALLDEV-862
                inXML += "  <SystemAvailableTime>";
                string tmpStr = "0";
                if (Open24.Checked.Equals(true))
                    tmpStr = "1";
                inXML += "      <IsOpen24Hours>" + tmpStr + "</IsOpen24Hours>";
                if (tmpStr.Equals("1"))
                {
                    inXML += "      <StartTime>12:00 AM</StartTime>";
                    inXML += "      <EndTime>11:59 PM</EndTime>";
                }
                else
                {
                    inXML += "      <StartTime>" + DateTime.Parse(myVRMNet.NETFunctions.ChangeTimeFormat(systemStartTime.Text)).ToShortTimeString() + "</StartTime>"; //FB 2588
                    inXML += "      <EndTime>" + DateTime.Parse(myVRMNet.NETFunctions.ChangeTimeFormat(systemEndTime.Text)).ToShortTimeString() + "</EndTime>"; //FB 2588
                }
                inXML += "      <DaysClosed>";
                tmpStr = "";
                foreach (ListItem li in DayClosed.Items)
                    if (li.Selected)
                        tmpStr += li.Value + ",";
                if (tmpStr.Length > 0)
                    tmpStr = tmpStr.Substring(0, tmpStr.Length - 1);
                inXML += tmpStr;
                inXML += "</DaysClosed>";
                inXML += "  </SystemAvailableTime>";
				inXML += "<EnableHostGuestPwd>" + drpEnableHostandGuestPwd.SelectedValue + "</EnableHostGuestPwd>"; //ALLDEV-826
                //ALLDEV-837 Starts
                inXML += "<EnableSyncLaunchBuffer>" + drpDwnEnableSyncLaunchBuffer.SelectedValue + "</EnableSyncLaunchBuffer>";
                if (drpDwnEnableSyncLaunchBuffer.SelectedValue == "1")
                {
                    inXML += "<SyncLaunchBuffer>" + txtLaunchBuffer.Text + "</SyncLaunchBuffer>";
                }
                else
                {
                    inXML += "<SyncLaunchBuffer>0</SyncLaunchBuffer>";
                }
                //ALLDEV-837 Ends
                inXML += "<ConfAdministrator>" + txtAdminID.Text + "</ConfAdministrator>";   //ALLDEV-498                
                inXML += "<EnableRoomSelection>" + DrpDwnEnableRoomSelection.SelectedValue + "</EnableRoomSelection>";  //ALLDEV-834
                inXML += "</SetOrgOptions>";
                log.Trace("SetOrgOptions Inxml: " + inXML);
                String outXML = obj.CallMyVRMServer("SetOrgOptions", inXML, Application["MyVRMServer_ConfigPath"].ToString());
                log.Trace("SetOrgOptions Outxml: " + outXML);
                if (outXML.IndexOf("<error>") >= 0)
                {
                    errLabel.Text = obj.ShowErrorMessage(outXML);
                    errLabel.Visible = true;
                }
                else
                {
                    if (Session["organizationID"] != null)
                    {
                        if (Session["organizationID"].ToString() != "")
                            Int32.TryParse(Session["organizationID"].ToString(), out orgId);
                    }

                    obj.SetOrgSession(orgId);
                    Session["CalendarMonthly"] = null;//FB 1850
                    Session.Add("AlertforTier1", tierIds); //FB 2637
                    if (hdnZuluChange != null)
                        isZuluChanged = hdnZuluChange.Value;
                    if (isZuluChanged == "1")
                        Response.Redirect("mainadministrator.aspx?m=2");
                    else
                        Response.Redirect("mainadministrator.aspx?m=1");
                }
            }
            catch (Exception ex)
            {
                //log.Trace("SetOrgOptions: " + ex.Message + " : " + ex.StackTrace);
                errLabel.Visible = true;
                errLabel.Text = ex.Message;
                log.Trace(ex.StackTrace + " : " + ex.Message);
                return;
            }
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            if(Validate())//FB 2440            
                SetOrgOptions();
        }

        #region Set reminder mask from selection
        /// <summary>
        /// Set reminder mask from selection (FB 1926)
        /// </summary>
        /// <returns>string remindermsk</returns>
        private string buildRemainderMask()
        {
            Int32 remindermsk = 0;
            try
            {

                if (WklyChk.Checked)
                    remindermsk += wkly;
                if (DlyChk.Checked)
                    remindermsk += dly;
                if (HourlyChk.Checked)
                    remindermsk += hourly;
                if (MinChk.Checked)
                    remindermsk += minutely;

            }
            catch (Exception ex)
            {

                log.Trace("SetOrgOptions: " + ex.Message + " : " + ex.StackTrace);
            }

            return remindermsk.ToString();
        }
        #endregion

        #region Set reminder from mask
        /// <summary>
        /// To set Reminder from mask ( FB 1926)
        /// </summary>
        /// <returns></returns>
        private void SetRemainderfromMask(String mask)
        {
            Int32 remindermsk = 0;
            try
            {
                Int32.TryParse(mask,out remindermsk);

                if(remindermsk > 0)
                {
                    if (Convert.ToBoolean(remindermsk & wkly))
                        WklyChk.Checked = true;
                    if (Convert.ToBoolean(remindermsk & dly))
                        DlyChk.Checked = true;
                    if (Convert.ToBoolean(remindermsk & hourly))
                        HourlyChk.Checked = true;
                    if (Convert.ToBoolean(remindermsk & minutely))
                        MinChk.Checked = true;

                }

            }
            catch (Exception ex)
            {

                log.Trace("SetOrgOptions: " + ex.Message + " : " + ex.StackTrace);
            }
        }
        #endregion

        #region Auto Screen Layout Mapping - Load Id and Image
        // FB 2335 Starts
        protected void fnSetLayoutImage(string xPoly2MGC, string xPoly2RMX, string xCTMS2Cisco, string xCTMS2Poly,string  xCodian)//ZD 101869
        {
            String fullPath = "";
            if (xPoly2MGC == "00") //FB 2384
                xPoly2MGC = "01";
            //fullPath = Request.Url.ToString().Substring(0, Request.Url.ToString().IndexOf("/" + language + "/") + 3);
            fullPath = Request.Url.ToString().Substring(0, Request.Url.ToString().IndexOf("/en/") + 3);
            Poly2MGC.Value = xPoly2MGC;
            imgLayoutMapping1.ImageUrl =  fullPath + "/image/displaylayout/" + xPoly2MGC + ".gif";
            if (xPoly2RMX == "00") //FB 2384
                xPoly2RMX = "01";
            Poly2RMX.Value = xPoly2RMX;
            imgLayoutMapping2.ImageUrl = fullPath + "/image/displaylayout/" + xPoly2RMX + ".gif";
            if (xCTMS2Cisco == "00") //FB 2384
                xCTMS2Cisco = "01";
            CTMS2Cisco.Value = xCTMS2Cisco;
            imgLayoutMapping3.ImageUrl = fullPath + "/image/displaylayout/" + xCTMS2Cisco + ".gif";
            if (xCTMS2Poly == "00") //FB 2384
                xCTMS2Poly = "01";
            CTMS2Poly.Value = xCTMS2Poly;
            imgLayoutMapping4.ImageUrl = fullPath + "/image/displaylayout/" + xCTMS2Poly + ".gif";
            //ZD 101869 start
            if (xCodian == "00") //FB 2384
                xCodian = "01";
            if (xCodian == "101")
            {
                imgLayoutMapping5.Attributes.Add("style", "display:");
                imgLayoutMapping5.ImageUrl = fullPath + "/image/displaylayout/" + "05" + ".gif";
                imgLayoutMapping6.Attributes.Add("style", "display:");
                imgLayoutMapping6.ImageUrl = fullPath + "/image/displaylayout/" + "06" + ".gif";
                imgLayoutMapping7.Attributes.Add("style", "display:");
                imgLayoutMapping7.ImageUrl = fullPath + "/image/displaylayout/" + "07" + ".gif";
                imgLayoutMapping8.Attributes.Add("style", "display:");
                imgLayoutMapping8.ImageUrl = fullPath + "/image/displaylayout/" + "44" + ".gif";
                lblCodianLO.Text = obj.GetTranslatedText("Family 1");
            }
            else if (xCodian == "102")
            {
                imgLayoutMapping5.ImageUrl = fullPath + "/image/displaylayout/" + "01" + ".gif";
                lblCodianLO.Text = obj.GetTranslatedText("Family 2");
                imgLayoutMapping5.Attributes.Add("style", "display:");
            }
            else if (xCodian == "103")
            {
                imgLayoutMapping5.ImageUrl = fullPath + "/image/displaylayout/" + "02" + ".gif";
                lblCodianLO.Text = obj.GetTranslatedText("Family 3");
                imgLayoutMapping5.Attributes.Add("style", "display:");
            }
            else if (xCodian == "104")
            {
                imgLayoutMapping5.Attributes.Add("style", "display:");
                imgLayoutMapping5.ImageUrl = fullPath + "/image/displaylayout/" + "02" + ".gif";
                imgLayoutMapping6.Attributes.Add("style", "display:");
                imgLayoutMapping6.ImageUrl = fullPath + "/image/displaylayout/" + "03" + ".gif";
                imgLayoutMapping7.Attributes.Add("style", "display:");
                imgLayoutMapping7.ImageUrl = fullPath + "/image/displaylayout/" + "04" + ".gif";
                imgLayoutMapping8.Attributes.Add("style", "display:");
                imgLayoutMapping8.ImageUrl = fullPath + "/image/displaylayout/" + "43" + ".gif";
                lblCodianLO.Text = obj.GetTranslatedText("Family 4");
            }
            else if (xCodian == "105")
            {
                imgLayoutMapping5.ImageUrl = fullPath + "/image/displaylayout/" + "25" + ".gif";
                lblCodianLO.Text = obj.GetTranslatedText("Family 5");
                imgLayoutMapping5.Attributes.Add("style", "display:");
            }
            else if (xCodian == "100")
            {
                imgLayoutMapping5.Attributes.Add("style", "display:None");
                imgLayoutMapping6.Attributes.Add("style", "display:None");
                imgLayoutMapping7.Attributes.Add("style", "display:None");
                imgLayoutMapping8.Attributes.Add("style", "display:None");
                lblCodianLO.Text = obj.GetTranslatedText("Default Family");
            }
            else
            {
                imgLayoutMapping5.ImageUrl = fullPath + "/image/displaylayout/" + xCodian + ".gif";
                imgLayoutMapping5.Attributes.Add("style", "display:");
                lblCodianLO.Text = "";
            }

            hdnCodian.Value = xCodian;
            //ZD 101869 End

        }
        // FB 2335 Ends
        #endregion
		//ZD 100085 Starts
        #region Validate times
        /// <summary>
        /// FB 2440
        /// Validate
        /// </summary>
        /// <returns></returns>
        private Boolean Validate()
        {
            bool bRet = true;
            int mcuSetup = 0, roomSetup = 0,mcuTear = 0,roomTear = 0;//100433
            try
            {
                //FB 2634
                if (EnableBufferZone.SelectedValue.Trim() == "0")
                {
                    txtSetupTime.Text = "0";
                    txtTearDownTime.Text = "0";
                    DrpSetupTimeDisplay.SelectedValue = "0"; //ZD 101755 
                    DrpSetupTimeDisplay.SelectedValue = "0"; //ZD 101755 
                    //txtMCUSetupTime.Text = "0";
                    //txtMCUTearDownTime.Text = "0";

                }//100433 start
                //else if (EnableBufferZone.SelectedValue.Trim() == "1" && DrpForceMCUBuffer.SelectedValue.Trim() == "0")
                else
                {

                    int.TryParse(txtMCUSetupTime.Text.Trim(), out mcuSetup);
                    int.TryParse(txtSetupTime.Text.Trim(), out roomSetup);
                    int.TryParse(txtMCUTearDownTime.Text.Trim(), out mcuTear);
                    int.TryParse(txtTearDownTime.Text.Trim(), out roomTear);

                    if (mcuSetup != 0 && roomSetup != 0)
                    {
                        if (mcuSetup > roomSetup)
                        {
                            errLabel.Text = obj.GetTranslatedText("Setup time cannot be less than the MCU pre start time.");
                            errLabel.Visible = true;
                            bRet = false;
                        }
                    }
                    if (mcuTear != 0 && roomTear != 0)
                    {
                        if (mcuTear > roomTear)
                        {
                            errLabel.Text = obj.GetTranslatedText("Teardown time cannot be less than the MCU pre end time.");
                            errLabel.Visible = true;
                            bRet = false;
                        }
                    }
                }//100433 End
				//ZD 100085 End


            }
            catch (Exception ex)
            {
                log.Trace("Validate: " + ex.Message + " : " + ex.StackTrace);
                
            }

            return bRet;
        }
#endregion

        //FB 2426 Start
        #region UpdateMiddleTiers
        /// <summary>
        /// UpdateMiddleTiers
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void UpdateMiddleTiers(Object sender, EventArgs e)
        {
            try
            {
                StringBuilder inXML = new StringBuilder(); //Used StringBuilder instead of string for inXML during ZD 100068 start
                inXML.Append("<GetLocations2>");
                inXML.Append(obj.OrgXMLElement());//Organization Module Fixes
                inXML.Append("<UserID>" + Session["userID"].ToString() + "</UserID>");
                inXML.Append("<Tier1ID>" + lstTopTier.SelectedValue + "</Tier1ID>");
                inXML.Append("</GetLocations2>");
                string outXML = obj.CallMyVRMServer("GetLocations2", inXML.ToString(), Application["MyVRMServer_ConfigPath"].ToString());
                log.Trace("GetLocations2 outxml: " + outXML);

                if (outXML.IndexOf("<error>") < 0)
                {
                    XmlDocument xmldoc = new XmlDocument();
                    xmldoc.LoadXml(outXML);
                    XmlNodeList nodes = null;
                    nodes = xmldoc.SelectNodes("//GetLocations2/Location");
                    if (nodes != null)
                        obj.LoadList(lstMiddleTier, nodes, "ID", "Name");
                }
                if(IsPostBack)
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "addorremove1", "<script>var img_FLY_obj = document.getElementById('img_FLY'); ExpandCollapse(img_FLY_obj,'trFLY', false);</script>", false);
            }
            catch (Exception ex)
            {
                log.Trace("UpdateMiddleTiers: " + ex.StackTrace + " : " + ex.Message);
            }
        }
        #endregion
        //FB 2426 End
        
        //ZD 100068 start
        #region UpdateVMRMiddleTiers
        /// <summary>
        /// UpdateVMR MiddleTiers
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void UpdateVMRMiddleTiers(Object sender, EventArgs e)
        {
            try
            {
                StringBuilder inXML = new StringBuilder();

                inXML.Append("<GetLocations2>");
                inXML.Append(obj.OrgXMLElement());
                inXML.Append("<UserID>" + Session["userID"].ToString() + "</UserID>");
                inXML.Append("<Tier1ID>" + lstVMRTopTier.SelectedValue + "</Tier1ID>");
                inXML.Append("</GetLocations2>");
                string outXML = obj.CallMyVRMServer("GetLocations2", inXML.ToString(), Application["MyVRMServer_ConfigPath"].ToString());

                if (outXML.IndexOf("<error>") < 0)
                {
                    XmlDocument xmldoc1 = new XmlDocument();
                    xmldoc1.LoadXml(outXML);
                    XmlNodeList nodes = null;
                    nodes = xmldoc1.SelectNodes("//GetLocations2/Location");
                    if (nodes != null)
                        obj.LoadList(lstVMRMiddleTier, nodes, "ID", "Name");
                }


                if (IsPostBack)
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "addorremove2", "<script> ExpandCollapse(document.getElementById('img_PIM'),'trPIM', false);</script>", false);
                 
                }
            }
            catch (Exception ex)
            {
                log.Trace("UpdateVMRMiddleTiers Error: " + ex.StackTrace + " : " + ex.Message);
            }
        }
        #endregion
        //ZD 100068 end
        
        //FB 2486
        #region SetOrgTextMessage
        /// <summary>
        /// SetMessage
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void fnOrgTxtMsgSubmit(object sender, EventArgs e)
        {
            try
            {
                StringBuilder inxml = new StringBuilder();

                inxml.Append("<SetOrgTextMsg><UserID>" + Session["userID"] + "</UserID>" + obj.OrgXMLElement());
                #region OrgMessageList
                inxml.Append("<OrgMessage>");

                if (chkmsg1.Checked)
                    inxml.Append("1#" + drpdownconfmsg1.SelectedItem.Value + "#" + drpdownmsgduration1.SelectedItem.Text.Trim()+";");

                if (chkmsg2.Checked)
                    inxml.Append("2#" + drpdownconfmsg2.SelectedItem.Value + "#" + drpdownmsgduration2.SelectedItem.Text.Trim() + ";");

                if (chkmsg3.Checked)
                    inxml.Append("3#" + drpdownconfmsg3.SelectedItem.Value + "#" + drpdownmsgduration3.SelectedItem.Text.Trim() + ";");

                if (chkmsg4.Checked)
                    inxml.Append("4#" + drpdownconfmsg4.SelectedItem.Value + "#" + drpdownmsgduration4.SelectedItem.Text.Trim() + ";");

                if (chkmsg5.Checked)
                    inxml.Append("5#" + drpdownconfmsg5.SelectedItem.Value + "#" + drpdownmsgduration5.SelectedItem.Text.Trim() + ";");

                if (chkmsg6.Checked)
                    inxml.Append("6#" + drpdownconfmsg6.SelectedItem.Value + "#" + drpdownmsgduration6.SelectedItem.Text.Trim() + ";");

                if (chkmsg7.Checked)
                    inxml.Append("7#" + drpdownconfmsg7.SelectedItem.Value + "#" + drpdownmsgduration7.SelectedItem.Text.Trim() + ";");

                if (chkmsg8.Checked)
                    inxml.Append("8#" + drpdownconfmsg8.SelectedItem.Value + "#" + drpdownmsgduration8.SelectedItem.Text.Trim() + ";");

                if (chkmsg9.Checked)
                    inxml.Append("9#" + drpdownconfmsg9.SelectedItem.Value + "#" + drpdownmsgduration9.SelectedItem.Text.Trim() + ";");

                inxml.Append("</OrgMessage>");
                #endregion
                inxml.Append("</SetOrgTextMsg>");

                String outXML = obj.CallMyVRMServer("SetOrgTextMsg", inxml.ToString(), Application["MyVRMServer_ConfigPath"].ToString());
                
                if (outXML.IndexOf("<error>") >= 0)
                {
                    errLabel.Text = obj.ShowErrorMessage(outXML);
                    errLabel.Visible = true;
                }
            }
            catch (Exception ex)
            {
                log.Trace("fnOrgTxtMsgSubmit" + ex.Message);
            }
        }
        #endregion

        #region GetOrgMessages
        public void GetOrgMessages(string TxtMsgList)
        {
            try
            {
                string controlID = "0", duration = "0", textMessage = "0";
                String[] TxtMsgID;

                String[] TxtMsg = TxtMsgList.Split(';');
                if (TxtMsg.Length > 0)
                {
                    for (int i = 0; i < TxtMsg.Length -1; i++)
                    {
                        controlID = "0";
                        duration = "0";
                        textMessage = "0";

                        TxtMsgID = TxtMsg[i].Split('#');

                        controlID = TxtMsgID[0];
                        textMessage = TxtMsgID[1];
                        duration = TxtMsgID[2];
                       
                        if (controlID == "1")
                        {
                            chkmsg1.Checked = true;

                            drpdownconfmsg1.ClearSelection();
                            if (drpdownconfmsg1.Items.FindByValue(textMessage) != null)
                                drpdownconfmsg1.Items.FindByValue(textMessage).Selected = true;

                            drpdownmsgduration1.ClearSelection();
                            if (drpdownmsgduration1.Items.FindByText(duration) != null)
                            {
                                drpdownmsgduration1.Items.FindByText(duration).Selected = true;
                                drpdownmsgduration1.Attributes["PreValue"] = duration;
                            }
                        }
                        else if (controlID == "2")
                        {
                            chkmsg2.Checked = true;

                            drpdownconfmsg2.ClearSelection();
                            if (drpdownconfmsg2.Items.FindByValue(textMessage) != null)
                                drpdownconfmsg2.Items.FindByValue(textMessage).Selected = true;

                            drpdownmsgduration2.ClearSelection();
                            if (drpdownmsgduration2.Items.FindByText(duration) != null)
                            {
                                drpdownmsgduration2.Items.FindByText(duration).Selected = true;
                                drpdownmsgduration2.Attributes["PreValue"] = duration;
                            }
                        }
                        else if (controlID == "3")
                        {
                            chkmsg3.Checked = true;

                            drpdownconfmsg3.ClearSelection();
                            if (drpdownconfmsg3.Items.FindByValue(textMessage) != null)
                                drpdownconfmsg3.Items.FindByValue(textMessage).Selected = true;

                            drpdownmsgduration3.ClearSelection();
                            if (drpdownmsgduration3.Items.FindByText(duration) != null)
                            {
                                drpdownmsgduration3.Items.FindByText(duration).Selected = true;
                                drpdownmsgduration3.Attributes["PreValue"] = duration;
                            }
                        }
                        else if (controlID == "4")
                        {
                            chkmsg4.Checked = true;

                            drpdownconfmsg4.ClearSelection();
                            if (drpdownconfmsg4.Items.FindByValue(textMessage) != null)
                                drpdownconfmsg4.Items.FindByValue(textMessage).Selected = true;

                            drpdownmsgduration4.ClearSelection();
                            if (drpdownmsgduration4.Items.FindByText(duration) != null)
                            {
                                drpdownmsgduration4.Items.FindByText(duration).Selected = true;
                                drpdownmsgduration4.Attributes["PreValue"] = duration;
                            }
                        }
                        else if (controlID == "5")
                        {
                            chkmsg5.Checked = true;

                            drpdownconfmsg5.ClearSelection();
                            if (drpdownconfmsg5.Items.FindByValue(textMessage) != null)
                                drpdownconfmsg5.Items.FindByValue(textMessage).Selected = true;

                            drpdownmsgduration5.ClearSelection();
                            if (drpdownmsgduration5.Items.FindByText(duration) != null)
                            {
                                drpdownmsgduration5.Items.FindByText(duration).Selected = true;
                                drpdownmsgduration5.Attributes["PreValue"] = duration;
                            }
                        }
                        else if (controlID == "6")
                        {
                            chkmsg6.Checked = true;

                            drpdownconfmsg6.ClearSelection();
                            if (drpdownconfmsg6.Items.FindByValue(textMessage) != null)
                                drpdownconfmsg6.Items.FindByValue(textMessage).Selected = true;

                            drpdownmsgduration6.ClearSelection();
                            if (drpdownmsgduration6.Items.FindByText(duration) != null)
                            {
                                drpdownmsgduration6.Items.FindByText(duration).Selected = true;
                                drpdownmsgduration6.Attributes["PreValue"] = duration;
                            }
                        }
                        else if (controlID == "7")
                        {
                            chkmsg7.Checked = true;

                            drpdownconfmsg7.ClearSelection();
                            if (drpdownconfmsg7.Items.FindByValue(textMessage) != null)
                                drpdownconfmsg7.Items.FindByValue(textMessage).Selected = true;

                            drpdownmsgduration7.ClearSelection();
                            if (drpdownmsgduration7.Items.FindByText(duration) != null)
                            {
                                drpdownmsgduration7.Items.FindByText(duration).Selected = true;
                                drpdownmsgduration7.Attributes["PreValue"] = duration;
                            }
                        }
                        else if (controlID == "8")
                        {
                            chkmsg8.Checked = true;

                            drpdownconfmsg8.ClearSelection();
                            if (drpdownconfmsg8.Items.FindByValue(textMessage) != null)
                                drpdownconfmsg8.Items.FindByValue(textMessage).Selected = true;

                            drpdownmsgduration8.ClearSelection();
                            if (drpdownmsgduration8.Items.FindByText(duration) != null)
                            {
                                drpdownmsgduration8.Items.FindByText(duration).Selected = true;
                                drpdownmsgduration8.Attributes["PreValue"] = duration;
                            }
                        }
                        else if (controlID == "9")
                        {
                            chkmsg9.Checked = true;

                            drpdownconfmsg9.ClearSelection();
                            if (drpdownconfmsg9.Items.FindByValue(textMessage) != null)
                                drpdownconfmsg9.Items.FindByValue(textMessage).Selected = true;

                            drpdownmsgduration9.ClearSelection();
                            if (drpdownmsgduration9.Items.FindByText(duration) != null)
                            {
                                drpdownmsgduration9.Items.FindByText(duration).Selected = true;
                                drpdownmsgduration9.Attributes["PreValue"] = duration;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                log.Trace("GetOrgMessages" + ex.Message);//ZD 100263
                errLabel.Text = obj.ShowSystemMessage();//ZD 100263
                errLabel.Visible = true;
                
            }
        }

        #endregion
        //FB 2486 Ends

        protected void RoomImportPage(object sender, EventArgs e)
        {
            Session.Add("SelectedOptions", "1");
            Response.Redirect("RoomEndpointImport.aspx");            
        }
        protected void EndpointImportPage(object sender, EventArgs e)
        {
            Session.Add("SelectedOptions", "2");
            Response.Redirect("RoomEndpointImport.aspx");
        }

        protected void ManualSync(object sender, EventArgs e)
        {
            try
            {
                string inXML = String.Empty;
                string crossEPxml = "", eptxmlPath = "";
                inXML = "<login>" + obj.OrgXMLElement() + "</login>";
                string outXML = String.Empty;

                outXML = obj.CallCOM2("ManualSyncRoom", inXML, Application["RTC_ConfigPath"].ToString());

                if (outXML.IndexOf("<error>") >= 0)
                {
                    errLabel.Visible = true;
                    errLabel.Text = obj.ShowErrorMessage(outXML);
                    if (errLabel.Text.Contains("No RPRM MCUs to Poll Rooms/Endpoints"))
                        errLabel.Text = obj.GetTranslatedText("No RPRM MCUs to Poll Rooms/Endpoints.");
                }
                else
                {
                    errLabel.Text = obj.GetTranslatedText("Operation Successful!");
                    errLabel.Visible = true;
                    eptxmlPath = Directory.GetParent(HttpContext.Current.Request.MapPath(".").ToString()) + "\\en" + "\\" + Session["EptXmlPath"].ToString();
                    if (File.Exists(eptxmlPath))
                        File.Delete(eptxmlPath);
                }
               
            }
            catch (Exception ex)
            {
                errLabel.Visible = true;
                errLabel.Text = obj.ShowSystemMessage();//ZD 100263

            }
        }

        #region CheckUserEmail
        /// <summary>
        /// CheckUserEmail
        /// </summary>
        /// <param name="strEmailAddress"></param>
        /// <param name="errorEmail"></param>
        /// <returns></returns>
        protected bool CheckUserEmail(string strEmailAddress, ref string errorEmail)
        {
            string[] emails = null;
            string emailaddress = "";//ZD 103856 start
            string EmailaddressValidation = @"^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}" +
         @"\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\" +
         @".)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$";
            Regex Checkemailaddress = new Regex(EmailaddressValidation);
            try
            {
                if (string.IsNullOrEmpty(strEmailAddress))
                    return true;
                
                emails = strEmailAddress.Split(';');
                for (int cnt = 0; cnt < emails.Length; cnt++)
                {
                    emailaddress = emails[cnt].Trim();
                    if (string.IsNullOrEmpty(emailaddress.Trim()))
                        continue;
                    try
                    {
                        if (Checkemailaddress.IsMatch(emailaddress))
                            continue;
                        else
                        {
                            if (errorEmail == "")
                                errorEmail = emailaddress.Trim();
                            else
                                errorEmail = errorEmail + ";" + emailaddress.Trim();
                        }
                    }
                           
                    catch (Exception e)
                    {
                        return false;
                         
                    }
                }

                if (!Checkemailaddress.IsMatch(strEmailAddress))
                    return false;


            }
            catch (Exception ex)
            {                
                return false;
            }//ZD 103856 End

            return true;
        }
        #endregion

        //ZD 104391
        #region RemoveEndofBackslash
        private string RemoveEndofBackslash(string strInput)
        {
            string tempINput = "";
            try
            {
                tempINput = strInput;
                for (Int32 c = strInput.Length; c > 0; c--)
                {
                    if (strInput[c-1].ToString() == @"\")
                    {
                        tempINput = tempINput.Remove(tempINput.Length - 1);
                    }
                    else
                        break;
                }

                return tempINput;
              
            }
            catch (Exception ex)
            {
                log.Trace("Util_InXML :" + ex.StackTrace);
            }
            return tempINput;
        }
        #endregion
        
    }
}