/* Copyright (C) 2015 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
//ZD 100147 ZD 100886
using System;
using System.Data;
using System.Xml;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text.RegularExpressions;
using System.Text;
using System.Collections;
using System.Globalization;
using System.Threading;
/// <summary>
/// Summary description for ConferenceOrders
/// </summary>
/// 
namespace ns_Bridges
{
    public partial class BridgeDetails : System.Web.UI.Page
    {
        myVRMNet.NETFunctions obj;
        ns_Logger.Logger log;
        
        protected System.Web.UI.WebControls.Label lblHeader;
        protected System.Web.UI.WebControls.Label lblHeader1;
        protected System.Web.UI.WebControls.Label errLabel;
        protected System.Web.UI.WebControls.Label lblNoIPServices;
        protected System.Web.UI.WebControls.Label lblNoISDNServices;
        //protected System.Web.UI.WebControls.Label lblNoMPIServices;
        protected System.Web.UI.WebControls.Label lblIsPublic; // FB 1920

        protected System.Web.UI.WebControls.TextBox txtPassword1;
        protected System.Web.UI.WebControls.TextBox txtPassword2;
        protected System.Web.UI.WebControls.TextBox txtApprover1;
        protected System.Web.UI.WebControls.TextBox txtApprover2;
        protected System.Web.UI.WebControls.TextBox txtApprover3;
        protected System.Web.UI.WebControls.TextBox txtApprover4;
        protected System.Web.UI.HtmlControls.HtmlInputControl txtApprover1_1;
        protected System.Web.UI.HtmlControls.HtmlInputControl txtApprover2_1;
        protected System.Web.UI.HtmlControls.HtmlInputControl txtApprover3_1;
        protected System.Web.UI.HtmlControls.HtmlInputControl txtApprover4_1;
        protected System.Web.UI.WebControls.TextBox hdnApprover1;
        protected System.Web.UI.WebControls.TextBox hdnApprover2;
        protected System.Web.UI.WebControls.TextBox hdnApprover3;
        protected System.Web.UI.WebControls.TextBox hdnApprover4;
        protected System.Web.UI.HtmlControls.HtmlInputControl hdnApprover1_1;
        protected System.Web.UI.HtmlControls.HtmlInputControl hdnApprover2_1;
        protected System.Web.UI.HtmlControls.HtmlInputControl hdnApprover3_1;
        protected System.Web.UI.HtmlControls.HtmlInputControl hdnApprover4_1;
        protected System.Web.UI.WebControls.TextBox txtMCUID;
        protected System.Web.UI.WebControls.TextBox txtMCUName;
        protected System.Web.UI.WebControls.TextBox txtMaxVideoCalls;
        protected System.Web.UI.WebControls.TextBox txtMaxAudioCalls;
        protected System.Web.UI.WebControls.TextBox txtMCULogin;
        protected System.Web.UI.WebControls.TextBox txtPortA;
        protected System.Web.UI.WebControls.TextBox txtPortB;
        protected System.Web.UI.WebControls.TextBox txtPortP;
        protected System.Web.UI.WebControls.TextBox txtPortT;
        protected System.Web.UI.WebControls.TextBox txtMCUISDNPortCharge;
        protected System.Web.UI.WebControls.TextBox txtISDNLineCost;
        protected System.Web.UI.WebControls.TextBox txtISDNMaxCost;
        protected System.Web.UI.WebControls.TextBox txtISDNThresholdPercentage;
        protected System.Web.UI.WebControls.TextBox txtIVRName;//FB 1766
        protected System.Web.UI.WebControls.TextBox txtISDNAudioPref;//FB 2003
        protected System.Web.UI.WebControls.TextBox txtISDNVideoPref;//FB 2003
        protected System.Web.UI.WebControls.TextBox txtConfServiceID;//FB 2016

        protected System.Web.UI.WebControls.RadioButtonList rdISDNThresholdTimeframe;

        protected System.Web.UI.WebControls.DropDownList lstMCUType;
        protected System.Web.UI.WebControls.DropDownList lstAddressType;
        protected System.Web.UI.WebControls.DropDownList lstInterfaceType;
        protected System.Web.UI.WebControls.DropDownList lstStatus;
        protected System.Web.UI.WebControls.DropDownList lstTimezone;
        protected System.Web.UI.WebControls.DropDownList lstFirmwareVersion;
        protected System.Web.UI.WebControls.DropDownList drpURLAccess; //ZD 100113
        //protected System.Web.UI.WebControls.DropDownList lstTimezone;

        protected System.Web.UI.WebControls.DataGrid dgIPServices;
        protected System.Web.UI.WebControls.DataGrid dgISDNServices;
        protected System.Web.UI.WebControls.DataGrid dgMPIServices;
        protected System.Web.UI.WebControls.DataGrid dgMCUCards;

        // FB 2636 Starts
        protected System.Web.UI.WebControls.DataGrid dgE164Services;
        protected System.Web.UI.HtmlControls.HtmlTableRow trE164Services;
        protected System.Web.UI.WebControls.Label lblNoE164Services;
        //protected System.Web.UI.WebControls.Button btnAddNewE164Service;//ZD 100420
        protected System.Web.UI.HtmlControls.HtmlButton btnAddNewE164Service;
        protected System.Web.UI.WebControls.CheckBox chkE164;
        protected System.Web.UI.WebControls.Label lblE164;
        protected System.Web.UI.WebControls.CheckBox chkH323;
        protected System.Web.UI.WebControls.Label lblEH323;
        // Fb 2636 Ends
		//ZD 100420
        //protected System.Web.UI.WebControls.Button btnAddISDNService;
        protected System.Web.UI.HtmlControls.HtmlButton btnAddISDNService;
        //protected System.Web.UI.WebControls.Button btnAddNewIPService;
        protected System.Web.UI.HtmlControls.HtmlButton btnAddNewIPService;
		//ZD 100420
        //protected System.Web.UI.WebControls.Button btnAddNewMPIService;//ZD 103595

        protected System.Web.UI.WebControls.CheckBox chkIsVirtual;
        protected System.Web.UI.WebControls.CheckBox chkMalfunctionAlert;
        protected System.Web.UI.WebControls.CheckBox chkISDNThresholdAlert;
        protected System.Web.UI.WebControls.CheckBox chkEnableIVR;//FB 1766
        protected System.Web.UI.WebControls.CheckBox chkbxIsPublic; //FB 1920
        protected System.Web.UI.WebControls.CheckBox chkSetFavourite; // FB 2501 CallMonitoring

        protected System.Web.UI.HtmlControls.HtmlTableRow trISDN;
        protected System.Web.UI.HtmlControls.HtmlTableRow tr1;
        //FB 1937 - Start
        protected System.Web.UI.HtmlControls.HtmlTableRow tr2; 
        protected System.Web.UI.HtmlControls.HtmlTableRow tr5;
        protected System.Web.UI.HtmlControls.HtmlInputCheckBox chkExpandCollapse;
        //FB 1937 - End
        
        protected System.Web.UI.HtmlControls.HtmlTableRow tr3;
        protected System.Web.UI.HtmlControls.HtmlTableRow tr4;
        protected System.Web.UI.HtmlControls.HtmlTableRow trIPServices;
        protected System.Web.UI.HtmlControls.HtmlTableRow trMPIServices;
        protected System.Web.UI.HtmlControls.HtmlTableRow trISDN1; //FB 2636
        protected System.Web.UI.HtmlControls.HtmlTableRow trMCUCards;
        protected System.Web.UI.HtmlControls.HtmlTableRow trCodianMessage; //FB Case 698
        protected System.Web.UI.HtmlControls.HtmlInputHidden confPassword;

        protected System.Web.UI.WebControls.RequiredFieldValidator reqPortP;
        protected System.Web.UI.WebControls.RegularExpressionValidator regPortP;
        protected System.Web.UI.WebControls.CustomValidator customvalidation;

        /* *** Code added for FB 1425 QA Bug -Start *** */
        protected System.Web.UI.HtmlControls.HtmlTableCell TzTD1;
        protected System.Web.UI.HtmlControls.HtmlTableCell TzTD2;
        protected System.Web.UI.HtmlControls.HtmlInputHidden hdntzone;
        /* *** Code added for FB 1425 QA Bug -End *** */
        protected System.Web.UI.WebControls.TextBox txtapiportno;//API Port...

        //FB 1642 - DTMF Commented for FB 2655   
        //protected System.Web.UI.WebControls.TextBox PreConfCode;
        //protected System.Web.UI.WebControls.TextBox PreLeaderPin;
        //protected System.Web.UI.WebControls.TextBox PostLeaderPin;
        protected System.Web.UI.HtmlControls.HtmlTableRow trEnableIVR;//FB 1766
        protected System.Web.UI.HtmlControls.HtmlTableRow trIVRName;//FB 1766
        protected System.Web.UI.HtmlControls.HtmlTableRow trEnableRecord;//FB 1907
        protected System.Web.UI.WebControls.CheckBox chkEnableRecord;//FB 1907  
        protected System.Web.UI.HtmlControls.HtmlTableRow trConfServiceID;//FB 2016

        //FB 1938 - Start
        protected String format = "MM/dd/yyyy";
        String tformat = "hh:mm tt";
        protected MetaBuilders.WebControls.ComboBox ReportTime;
        protected System.Web.UI.WebControls.TextBox ReportDate;
        protected System.Web.UI.HtmlControls.HtmlTableRow ReportRow;
        protected System.Web.UI.WebControls.DataGrid dgUsageReport;
        protected System.Web.UI.WebControls.Label lblDetails;
		//FB 1938 - Start
        protected System.Web.UI.WebControls.Label LblVideoused;
        protected System.Web.UI.WebControls.Label LblVideoavail;
        protected System.Web.UI.WebControls.Label LblAudioused;
        protected System.Web.UI.WebControls.Label LblAudioavail;
        protected System.Web.UI.WebControls.RegularExpressionValidator regStartTime;

        
        
        //FB 1938 - End

        protected System.Web.UI.HtmlControls.HtmlTableCell tdAudio;//FB 1937  //FB 2660
        protected System.Web.UI.HtmlControls.HtmlTableCell tdtxtAudio;//FB 2660
        protected System.Web.UI.HtmlControls.HtmlTableRow tr6; // FB 2008
        protected System.Web.UI.WebControls.TextBox txtISDNGateway;//FB 2008
        protected System.Web.UI.HtmlControls.HtmlTableRow trEnableLPR;//FB 1907
        protected System.Web.UI.HtmlControls.HtmlTableRow trEnableMessage;//FB 2486
        protected System.Web.UI.WebControls.CheckBox chkMessage;
        protected System.Web.UI.WebControls.CheckBox chkLPR;//FB 1907  

        protected System.Web.UI.HtmlControls.HtmlTableRow trProfileID; //FB 2427
        protected System.Web.UI.WebControls.DropDownList txtProfileID, drpPoolOrder; //FB 2427//FB 2591 //ZD 104256
        
        protected System.Web.UI.HtmlControls.HtmlInputHidden hdnMcuCount; //FB 2486

        //FB 2610 Starts
        protected System.Web.UI.HtmlControls.HtmlTableRow trExtensionNumber;
        protected System.Web.UI.WebControls.TextBox txtExtNumber;
        protected System.Web.UI.WebControls.TextBox txtTempExtNumber;
        //FB 2610 Ends

        //FB 2441 Starts  
        protected System.Web.UI.HtmlControls.HtmlTableRow trEhancedMCU;
        protected System.Web.UI.WebControls.CheckBox ChkEhancedMCU;
        protected System.Web.UI.WebControls.CheckBox chkSendmail;
        protected System.Web.UI.WebControls.TextBox txtTemplate;
        protected System.Web.UI.HtmlControls.HtmlTableRow trTemplate;
        protected System.Web.UI.WebControls.TextBox txtDomain;
        protected System.Web.UI.HtmlControls.HtmlTableRow trDMAName;
        protected System.Web.UI.HtmlControls.HtmlTableRow trDMAPassword;
        protected System.Web.UI.HtmlControls.HtmlTableRow trDMAURL;
		
        protected System.Web.UI.HtmlControls.HtmlTableRow trDMATestCon;//FB 2441 II 
        protected System.Web.UI.HtmlControls.HtmlTableRow trDMATemplate;//FB 2441 II 
       
        protected System.Web.UI.WebControls.TextBox txtDMAName;
        protected System.Web.UI.WebControls.TextBox txtDMALogin;
        protected System.Web.UI.WebControls.TextBox txtDMAPassword;
        protected System.Web.UI.WebControls.TextBox txtDMARetypePwd;
        protected System.Web.UI.WebControls.TextBox txtDMAURL;
        protected System.Web.UI.WebControls.TextBox txtDMAPort;
        protected System.Web.UI.HtmlControls.HtmlInputCheckBox chkSynchronous; 
        protected System.Web.UI.HtmlControls.HtmlTableRow trSynchronous;
        protected System.Web.UI.HtmlControls.HtmlTableCell tdSynchronous;//FB 2556
        protected System.Web.UI.HtmlControls.HtmlTableCell tdSendMail;//FB 2556
        
        
        protected System.Web.UI.HtmlControls.HtmlButton btn_DMATestConnection;//ZD 100420
		 protected System.Web.UI.WebControls.DropDownList lstTemplate; // FB 2441 II
        protected System.Web.UI.WebControls.TextBox txtDMADomain;// FB 2441 II
        protected System.Web.UI.WebControls.TextBox txtDialinprefix; //FB 2689
        protected System.Web.UI.HtmlControls.HtmlTableRow trDMADialinprefix; // FB 2689
       // FB 2441 Ends

        //FB 2660 Starts
        protected System.Web.UI.HtmlControls.HtmlGenericControl spEnableCDR;
        protected System.Web.UI.HtmlControls.HtmlGenericControl spCDREvent;
        protected System.Web.UI.HtmlControls.HtmlContainerControl divtxtCDR;
        protected System.Web.UI.WebControls.TextBox txtdltCDR;
        protected System.Web.UI.WebControls.CheckBox chkEnableCDR;
        protected int enableCDR = 0;
        //FB 2660 Ends
		 //FB 2709 
        protected System.Web.UI.HtmlControls.HtmlTableRow trRPRMLogin;
        protected System.Web.UI.HtmlControls.HtmlTableRow trRPRMemail;
        protected System.Web.UI.WebControls.TextBox txtRPRMLogin;
        protected System.Web.UI.WebControls.TextBox txtCallsCount;
        protected System.Web.UI.HtmlControls.HtmlInputHidden hdnRPRMUserid;
        protected System.Web.UI.HtmlControls.HtmlInputHidden hdnRPRMCallCount;
        protected System.Web.UI.WebControls.TextBox txtRPRMEmail;
        int Existingcallscount = 0;
        //FB 2709 
		//FB 2556 - Starts
        protected System.Web.UI.HtmlControls.HtmlTableRow iViewOrgDetails1;
        protected System.Web.UI.HtmlControls.HtmlTableRow iViewOrgDetails2;
        protected System.Web.UI.HtmlControls.HtmlTableRow trMultiTenant;
        protected System.Web.UI.WebControls.TextBox txtScopiaURL;
        protected System.Web.UI.WebControls.TextBox txtScopiaOrgID;
        protected System.Web.UI.WebControls.TextBox txtScopiaServiceID;
        protected System.Web.UI.WebControls.TextBox txtScopiaOrgLogin;
        protected System.Web.UI.WebControls.DropDownList lstScopiaOrg;
        protected System.Web.UI.WebControls.DropDownList lstScopiaService;
        protected System.Web.UI.WebControls.CheckBox chkMultiTenant;
        protected System.Web.UI.HtmlControls.HtmlGenericControl lblAVPorts;
        protected System.Web.UI.HtmlControls.HtmlGenericControl lblAudioPorts;
        protected System.Web.UI.HtmlControls.HtmlAnchor imgAdminsearch;
        protected System.Web.UI.HtmlControls.HtmlAnchor imgApprover1;
        protected System.Web.UI.HtmlControls.HtmlAnchor imgApprover2;
        protected System.Web.UI.HtmlControls.HtmlAnchor imgApprover3;
        protected System.Web.UI.HtmlControls.HtmlImage imgDelApprover1;
        protected System.Web.UI.HtmlControls.HtmlImage imgDelApprover2;
        protected System.Web.UI.HtmlControls.HtmlImage imgDelApprover3;
        protected System.Web.UI.HtmlControls.HtmlTableRow trAlert;
        protected System.Web.UI.HtmlControls.HtmlTableRow trAlertchks;       
        //FB 2556 - End
		protected int enableCloudInstallation = 0;

        protected System.Web.UI.WebControls.Button btnSubmit; //FB 2670
        protected System.Web.UI.HtmlControls.HtmlInputHidden hdnPasschange; //FB 3054
        protected System.Web.UI.HtmlControls.HtmlInputHidden hdnDMSPW; //FB 3054
        //ZD 100369_MCU Start
        protected System.Web.UI.WebControls.CheckBox chkFailover;
        protected System.Web.UI.WebControls.TextBox txtMultipleAssistant;
        protected System.Web.UI.HtmlControls.HtmlContainerControl divFailover;
        protected System.Web.UI.HtmlControls.HtmlGenericControl lblFailover;
        protected System.Web.UI.HtmlControls.HtmlGenericControl lblPollStatus;
        //ZD 100369_MCU End
        bool hasReports = false;//ZD 100664
        protected System.Web.UI.WebControls.DataGrid dgChangedHistory;//ZD 100664
        protected System.Web.UI.HtmlControls.HtmlTableRow trHistory; //ZD 100664
        protected System.Web.UI.HtmlControls.HtmlTableRow trURL;//ZD 101078
        protected System.Web.UI.WebControls.TextBox txtURL;//ZD 101078
        //ZD 100522 Starts
        protected System.Web.UI.WebControls.TextBox txtTempDomain;
        protected System.Web.UI.WebControls.TextBox txtMCUDomain;
        //ZD 100522 Ends
        protected System.Web.UI.WebControls.DropDownList lstSystemLocation;//ZD 101522
        protected System.Web.UI.HtmlControls.HtmlTableRow trsyslocs;//ZD 101522
        protected System.Web.UI.WebControls.TextBox txtDialinInfo;//C20 S4B
        //ZD 103550
        protected System.Web.UI.WebControls.TextBox txtBJNAppKey;
        protected System.Web.UI.WebControls.TextBox txtBJNAppSecret;
        protected System.Web.UI.WebControls.TextBox txtBJNAddress;        
        protected System.Web.UI.HtmlControls.HtmlInputHidden hdnBJNAppKey1;
        protected System.Web.UI.HtmlControls.HtmlInputHidden hdnBJNAppSecret1;
        protected System.Web.UI.HtmlControls.HtmlTableRow tr7;
        protected System.Web.UI.HtmlControls.HtmlTableRow tr8;
        protected System.Web.UI.HtmlControls.HtmlTableRow tr9;
        protected System.Web.UI.HtmlControls.HtmlTableRow trAVPorts;
        protected System.Web.UI.HtmlControls.HtmlTableRow trURLandPoll;
        protected System.Web.UI.HtmlControls.HtmlTableRow trVirMCUandPublic;
        protected System.Web.UI.HtmlControls.HtmlTableRow trCDR;
        protected System.Web.UI.HtmlControls.HtmlTableRow ReportRow1;
        protected System.Web.UI.HtmlControls.HtmlTableRow trPwd;
        protected System.Web.UI.HtmlControls.HtmlTableCell tdLogin1;
        protected System.Web.UI.HtmlControls.HtmlTableCell tdLogin2;
        protected System.Web.UI.HtmlControls.HtmlTableCell tdFirm1;
        protected System.Web.UI.HtmlControls.HtmlTableCell tdFirm2;
        protected System.Web.UI.WebControls.DataGrid dgVideoPort;//ZD 100040
        protected System.Web.UI.WebControls.DataGrid dgloadBalance;//ZD 100040
        protected System.Web.UI.HtmlControls.HtmlTableCell tdImgLoadBalance;//ZD 100040

        //ALLDEV-854 Start
        protected System.Web.UI.HtmlControls.HtmlTableRow trAlias;
        protected System.Web.UI.WebControls.DropDownList DrpDwnAlias;
        protected System.Web.UI.WebControls.TextBox txtAlias2;
        protected System.Web.UI.WebControls.TextBox txtAlias3;
        //ALLDEV-854 End
                               
        public BridgeDetails()
        {
            obj = new myVRMNet.NETFunctions();
            log = new ns_Logger.Logger();
            //
            // TODO: Add constructor logic here
            //
        }

        //ZD 101022
        #region InitializeCulture
        protected override void InitializeCulture()
        {
            if (Session["UserCulture"] != null)
            {
                UICulture = Session["UserCulture"].ToString();
                Culture = Session["UserCulture"].ToString();
                //ZD 101714
                if (Session["UserCulture"].ToString() == "fr-CA")
                    Culture = "en-US";
                base.InitializeCulture();
            }
        }
        #endregion

        #region Methods Executed on Page Load

        private void Page_Load(object sender, System.EventArgs e)
        {
            try
            {
                if (obj == null)
                    obj = new myVRMNet.NETFunctions();
                obj.AccessandURLConformityCheck("bridgedetails.aspx", Request.Url.AbsoluteUri.ToLower()); // ZD 100263
                
                /* *** Code added for FB 1425 QA Bug -Start *** */
                //FB 1938 - Start
                if (Session["FormatDateType"] != null)
                {
                    if (Session["FormatDateType"].ToString() != "")
                        format = Session["FormatDateType"].ToString();
                }
                tformat = (Session["timeFormat"].ToString().Equals("0") ? "HH:mm" : "hh:mm tt");

                //FB 2588 Starts
                if (Session["timeFormat"].ToString().Equals("2"))
                    tformat = "HHmmZ";
				
				if(Session["EnableCDR"] != null) //FB 2660
                {
                    int.TryParse(Session["EnableCDR"].ToString(), out enableCDR);
                }
                
                if (Session["systemDate"] == null) 
                {
                    Session.Add("systemDate", DateTime.Now.ToString("MM/dd/yyyy"));
                    Session.Add("systemTime", DateTime.Now.ToString(tformat));
                }

                //FB 2659
                if (Session["EnableCloudInstallation"] != null)
                    int.TryParse(Session["EnableCloudInstallation"].ToString().Trim(), out enableCloudInstallation);

                if (enableCloudInstallation == 1)
                {
                    lblAVPorts.InnerText = obj.GetTranslatedText("Total Audio/Video Seats");
                    lblAudioPorts.InnerText = obj.GetTranslatedText("Total Audio Only Seats");
                }

                //txtMultipleAssistant.Attributes.Add("readonly", "readonly"); //100369_MCU

                if (!IsPostBack)
                {
                    ReportTime.Items.Clear();
                    obj.BindTimeToListBox(ReportTime, true, true);

                    DateTime curDateTime = DateTime.Parse(Session["systemDate"].ToString() + " " + Session["systemTime"].ToString());

                    if (curDateTime.Minute < 45)
                        curDateTime = curDateTime.AddMinutes(60 - curDateTime.Minute);
                    else
                        curDateTime = curDateTime.AddMinutes(120 - curDateTime.Minute);

                    ReportDate.Text = curDateTime.ToString(format);
                    ReportTime.Text = curDateTime.ToString(tformat);
                    ReportTime.SelectedValue = ReportTime.Text;

                    if (Session["timeFormat"] != null)
                        if (Session["timeFormat"].ToString().Equals("0"))
                        {
                            regStartTime.ValidationExpression = "[0-2][0-9]:[0-5][0-9]";
                            regStartTime.ErrorMessage = obj.GetTranslatedText("Invalid Time (HH:mm)");                           
                        }
                        else if (Session["timeFormat"].ToString().Equals("2")) //FB 2588
                        {
                            regStartTime.ValidationExpression = "[0-2][0-9][0-5][0-9][Z|z]";
                            regStartTime.ErrorMessage = obj.GetTranslatedText("Invalid Time (HHmmZ)");
                        }
                }
                //FB 1938 - End

                if (Application["Client"].ToString().ToUpper().Equals(ns_MyVRMNet.vrmClient.MOJ))
                {

                    TzTD1.Attributes.Add("style", "display:none");
                    TzTD2.Attributes.Add("style", "display:none");

                    if (hdntzone.Value == "")
                    {
                        lstTimezone.SelectedValue = "31";
                        //String timezne = obj.GetSystemTimeZone(Application["COM_ConfigPath"].ToString());
                        String timezne = obj.GetSystemTimeZone(Application["MyVRMServer_ConfigPath"].ToString()); //FB 2027
                        if (timezne != "")
                        {
                            lstTimezone.ClearSelection();
                            lstTimezone.SelectedValue = timezne;
                        }
                    }
                }               

                /* *** Code added for FB 1425 QA Bug -End *** */

                if (!IsPostBack)
                {
                    //FB 1920 Starts
                    if( Session ["organizationID"].ToString() != "11")
                    {
                        lblIsPublic.Attributes.Add("style", "display:none");
                        chkbxIsPublic.Attributes.Add("style", "display:none");
                    }
                    //FB 1920 Ends
                    if (Request.QueryString["hf"] != null)
                    {
                        if (Request.QueryString["hf"].ToString().Equals("1"))
                        {
                            Session.Add("BridgeID", Request.QueryString["bid"]);
                            DisableAllControls(Page.Controls[1]);
                        }
                    }
                    txtPassword1.Attributes.Add("onchange", "javascript:SavePassword()");
                    txtApprover1.Attributes.Add("onBlur", "javascript:SavePassword()");
                    txtApprover2.Attributes.Add("onBlur", "javascript:SavePassword()");
                    txtApprover3.Attributes.Add("onBlur", "javascript:SavePassword()");
                    txtApprover4.Attributes.Add("onBlur", "javascript:SavePassword()");
                    obj.BindAddressType(lstAddressType);
                    //ZD 100664 Start
                    string[] mary = Session["sMenuMask"].ToString().Split('-');
                    string[] mmary = mary[1].Split('+');
                    string[] ccary = mary[0].Split('*');
                    int topMenu = Convert.ToInt32(ccary[1]);
                    int adminMenu = Convert.ToInt32(mmary[10].Split('*')[1]); // Convert.ToInt32(mmary[1].Split('*')[1]); ZD 101233 ZD 103095

                    if (Convert.ToBoolean(topMenu & 16))
                    {
                        hasReports = Convert.ToBoolean(adminMenu & 4);
                    }

                    if (Session["admin"] != null)
                    {
                        if (Session["admin"].ToString().Equals("2") && (hasReports == true))
                            trHistory.Attributes.Add("Style", "visibility:visible;");
                        else
                            trHistory.Attributes.Add("Style", "visibility:hidden;");
                    }
                    //ZD 100664 End
                    if (Session["BridgeID"].ToString().Equals("new"))
                    {
                        ReportRow.Attributes.Add("Style", "display:None;"); //FB 1938
                        ReportRow1.Attributes.Add("Style", "display:None;"); //ZD 103550
                        lblHeader.Text = obj.GetTranslatedText("Create New MCU");//FB 1830 - Translation
                        trHistory.Attributes.Add("Style", "visibility:hidden;"); //ZD 100664
                        DrpDwnAlias.SelectedValue = "0";//ALLDEV-854
                        trAlias.Attributes.Add("Style", "visibility:hidden;");//ALLDEV-854
                        BindDataNew();
                    }
                    else
                    {
                        //ReportRow.Attributes.Add("Style", "display:Block;"); //FB 1938 //Commented for FB 1982
                        lblHeader.Text = obj.GetTranslatedText("Edit Existing MCU");//FB 1830 - Translation
                        BindDataOld();
                    }


                }
                if (Request.QueryString["m"] != null)
                    if (Request.QueryString["m"].ToString().Equals("1"))
                    {
                        errLabel.Text = obj.GetTranslatedText("Operation Successful!");//FB 1830 - Translation
                        errLabel.Visible = true;
                    }
                //txtPassword1.Attributes.Add("value", confPassword.Value);
                //txtPassword2.Attributes.Add("value", confPassword.Value);
                txtApprover1.Attributes.Add("value", txtApprover1_1.Value);
                txtApprover2.Attributes.Add("value", txtApprover2_1.Value);
                txtApprover3.Attributes.Add("value", txtApprover3_1.Value);
                txtApprover4.Attributes.Add("value", txtApprover4_1.Value);
                hdnApprover1.Attributes.Add("value", hdnApprover1_1.Value);
                hdnApprover2.Attributes.Add("value", hdnApprover2_1.Value);
                hdnApprover3.Attributes.Add("value", hdnApprover3_1.Value);
                hdnApprover4.Attributes.Add("value", hdnApprover4_1.Value);
                if (chkEnableIVR.Checked != true) //FB 1766
                {
                    txtIVRName.Enabled = false; 
                }
                else
                {
                    txtIVRName.Enabled = true;
                }
                //FB 2660 Starts
                spCDREvent.Attributes.Add("style", "display:none");
                divtxtCDR.Attributes.Add("style", "display:none");
                if (chkEnableCDR.Checked && enableCDR == 1)
                {
                    spCDREvent.Attributes.Add("style", "display:block");
                    divtxtCDR.Attributes.Add("style", "display:block");
                }
                //FB 2660 Ends
				//FB 2670
                if (Session["admin"].ToString() == "3")
                {
                    //btnSubmit.Attributes.Add("Class", "btndisable");// FB 2796
                    btnSubmit.Visible = false;//ZD 100263
                    //btnSubmit.ForeColor = System.Drawing.Color.Gray;// FB 2796                    
                    lblHeader.Text = obj.GetTranslatedText("View MCU Details");
                }
                else
                    btnSubmit.Attributes.Add("Class", "altLongBlueButtonFormat");// FB 2796
            }
            catch (Exception ex)
            {
                log.Trace(ex.StackTrace + " : " + ex.Message);
            }
        }

        private void BindDataNew()
        {
            try
            {
                String inXML = "";
                inXML += "<login>";
                inXML += obj.OrgXMLElement();//Organization Module Fixes
                inXML += "  <userID>" + Session["userID"].ToString() + "</userID>";
                inXML += "  <bridgeID>" + Session["BridgeID"].ToString() + "</bridgeID>";
                inXML += "</login>";
                String outXML = obj.CallMyVRMServer("GetNewBridge", inXML, Application["MyVRMServer_ConfigPath"].ToString());
                if (outXML.IndexOf("<error>") < 0)
                {
                    XmlDocument xmldoc = new XmlDocument();
                    xmldoc.LoadXml(outXML);
                    XmlNodeList nodesTemp = xmldoc.SelectNodes("//bridge/bridgeTypes/type");
                    LoadList(lstMCUType, nodesTemp);
                    LoadList(lstInterfaceType, nodesTemp);
                    nodesTemp = xmldoc.SelectNodes("//bridge/bridgeStatuses/status");
                    LoadList(lstStatus, nodesTemp);
                    nodesTemp = xmldoc.SelectNodes("//bridge/timezones/timezone");
                    //FB 2486
                    hdnMcuCount.Value = xmldoc.SelectSingleNode("//bridge/EnhancedMCUCnt").InnerText;
                    
                    /* Code Modified For FB 1453 - Start */
                    //LoadList(lstTimezone, nodesTemp);
                    String selTZ = "-1";
                    lstTimezone.ClearSelection();
                    obj.GetTimezones(lstTimezone, ref selTZ);
                    /* Code Modified For FB 1453 - End */
                    try
                    {
                        lstTimezone.ClearSelection();//Code added for FB 1425 QA Bug
                        lstTimezone.Items.FindByValue(Session["systemTimezoneID"].ToString()).Selected = true;
                    }
                    catch (Exception ex)
                    {
                        log.Trace(ex.Message + " : " + ex.StackTrace);
                    }
                    //FB 2660 Starts
                    spEnableCDR.Attributes.Add("style", "display:none");
                    chkEnableCDR.Attributes.Add("style", "display:none");
                    spCDREvent.Attributes.Add("style", "display:none");
                    divtxtCDR.Attributes.Add("style", "display:none");
                    if (enableCDR == 1)
                    {
                        spEnableCDR.Attributes.Add("style", "display:block");
                        chkEnableCDR.Attributes.Add("style", "display:block");
                        txtdltCDR.Text = "1"; //FB 2714
                    }
                    //FB 2660 Ends
                    ChangeFirmwareVersion(new object(), new EventArgs());
                    txtMCUID.Text = "new";
                    lblNoISDNServices.Visible = true;
                    btnAddISDNService.Visible = true;
                    txtMaxVideoCalls.Text = "1";
                    txtMaxAudioCalls.Text = "1";
                    lblNoIPServices.Visible = true;
                    btnAddNewIPService.Visible = true;
                    //lblNoMPIServices.Visible = true;
                    //btnAddNewMPIService.Visible = true;//ZD 103595
                    LoadMCUCards("1", null); //FB Case 460 Saima
                    txtProfileID.Items.Clear(); //FB 2876
                    txtProfileID.Items.Insert(0, new ListItem(obj.GetTranslatedText("None"), "0")); //FB 2876
                    //ZD 100040 - Start
                    nodesTemp = xmldoc.SelectNodes("//bridge/videoPorts/port");
                    if (nodesTemp.Count > 0)
                    {
                        LoadVideoPortGrid(nodesTemp, 1);
                        dgVideoPort.Visible = true;
                    }
                    //ZD 100040 - End
					drpPoolOrder.Items.Clear();//ZD 104256
                    drpPoolOrder.Items.Insert(0, new ListItem(obj.GetTranslatedText("None"), "0")); //ZD 104256
                }
            }
            catch (Exception ex)
            {
                log.Trace(ex.StackTrace + " : " + ex.Message);
            }
        }

        private void BindDataOld()
        {
            int ProfileID = 0;//FB 2591
            bool isE164H323 = false;//FB 2636
            Session.Add("MCUPW", ""); //FB 3054
            Session.Add("DMAPW", ""); //FB 3054
            Session.Add("BJNAppkey", "");
            Session.Add("BJNAppSecret", "");
            try
            {
                String inXML = ""; //<login><userID>11</userID><bridgeID>1</bridgeID></login>
                inXML += "<login>";
                inXML += obj.OrgXMLElement();//Organization Module Fixes
                inXML += "  <userID>" + Session["userID"].ToString() + "</userID>";
                inXML += "  <bridgeID>" + Session["BridgeID"].ToString() + "</bridgeID>";
                inXML += "</login>";
                 // String outXML = obj.CallCOM("GetOldBridge", inXML, Application["MyVRMServer_ConfigPath"].ToString());
                String outXML = obj.CallMyVRMServer("GetOldBridge", inXML, Application["MyVRMServer_ConfigPath"].ToString());
                //String outXML = "<bridge><bridgeID>1</bridgeID><name>Test Bridge</name><login>login</login><password>pwd</password><bridgeType>3</bridgeType><bridgeStatus>1</bridgeStatus><virtualBridge>0</virtualBridge><malfunctionAlert>0</malfunctionAlert><timeZone>26</timeZone><firmwareVersion>7.0</firmwareVersion><percentReservedPort>0</percentReservedPort><bridgeAdmin><ID>11</ID><firstName>VRM</firstName><lastName>Administrator</lastName><maxAudioCalls>1</maxAudioCalls><maxVideoCalls>1</maxVideoCalls></bridgeAdmin><bridgeDetails><controlPortIPAddress>1.1.1.1</controlPortIPAddress><IPServices><IPService><ID>1</ID><name>Service 1</name><addressType>1</addressType><address>2.2.2.2</address><networkAccess>1</networkAccess><usage>1</usage></IPService></IPServices><ISDNServices><ISDNService><ID>1</ID><name>Service2</name><prefix>2</prefix><startRange>1234</startRange><endRange>8765</endRange><networkAccess>1</networkAccess><usage>1</usage></ISDNService></ISDNServices></bridgeDetails><approvers><responseMsg></responseMsg><responseTime>60</responseTime></approvers><timezones><timezone><timezoneID>35</timezoneID><timezoneName>(GMT-10:00) Hawaii</timezoneName></timezone><timezone><timezoneID>2</timezoneID><timezoneName>(GMT-09:00) Alaska</timezoneName></timezone><timezone><timezoneID>51</timezoneID><timezoneName>(GMT-08:00) Pacific Time</timezoneName></timezone><timezone><timezoneID>67</timezoneID><timezoneName>(GMT-07:00) Mountain - Arizona</timezoneName></timezone><timezone><timezoneID>42</timezoneID><timezoneName>(GMT-07:00) Mountain Time</timezoneName></timezone><timezone><timezoneID>66</timezoneID><timezoneName>(GMT-05:00) Indiana</timezoneName></timezone><timezone><timezoneID>19</timezoneID><timezoneName>(GMT-06:00) Central Time</timezoneName></timezone><timezone><timezoneID>26</timezoneID><timezoneName>(GMT-05:00) Eastern Time </timezoneName></timezone><timezone><timezoneID>73</timezoneID><timezoneName>(GMT+10:00) Guam</timezoneName></timezone><timezone><timezoneID>18</timezoneID><timezoneName>(GMT+11:00) Magadan</timezoneName></timezone></timezones><bridgeTypes><type><ID>1</ID><name>Polycom MGC 25</name><interfaceType>1</interfaceType></type><type><ID>2</ID><name>Polycom MGC 50</name><interfaceType>1</interfaceType></type><type><ID>3</ID><name>Polycom MGC 100</name><interfaceType>1</interfaceType></type><type><ID>4</ID><name>Codian MCU 4200</name><interfaceType>3</interfaceType></type><type><ID>5</ID><name>Codian MCU 4500</name><interfaceType>3</interfaceType></type><type><ID>6</ID><name> MSE 8000 Series</name><interfaceType>3</interfaceType></type><type><ID>7</ID><name>Tandberg MPS 800 Series</name><interfaceType>4</interfaceType></type></bridgeTypes><bridgeStatuses><status><ID>1</ID><name>Active</name></status><status><ID>2</ID><name>Maintenance</name></status><status><ID>3</ID><name>Disabled</name></status></bridgeStatuses><addressType><type><ID>1</ID><name>IP Address</name></type><type><ID>2</ID><name>H323 ID</name></type><type><ID>3</ID><name>E.164</name></type><type><ID>4</ID><name>ISDN Phone number</name></type></addressType><ISDNThresholdAlert>0</ISDNThresholdAlert></bridge>";
                //Response.Write("<br>" + obj.Transfer(outXML));
                log.Trace("GetOldBridge: " + outXML);
                if (outXML.IndexOf("<error>") < 0)
                {
                    XmlDocument xmldoc = new XmlDocument();
                    xmldoc.LoadXml(outXML);
                    XmlNodeList nodesTemp = xmldoc.SelectNodes("//bridge/bridgeTypes/type");
                    LoadList(lstMCUType, nodesTemp);
                    LoadList(lstInterfaceType, nodesTemp);
                    //ZD 100664 Start
                    XmlNodeList changedHistoryNodes = xmldoc.SelectNodes("//bridge/LastModifiedDetails/LastModified");
                    LoadHistory(changedHistoryNodes);
                    //ZD 100664 End

                    nodesTemp = xmldoc.SelectNodes("//bridge/bridgeStatuses/status");
                    LoadList(lstStatus, nodesTemp);
                    nodesTemp = xmldoc.SelectNodes("//bridge/timezones/timezone");
                    //FB 2486
                    hdnMcuCount.Value = xmldoc.SelectSingleNode("//bridge/EnhancedMCUCnt").InnerText;
                    int EnableMessage = Int32.Parse(xmldoc.SelectSingleNode("//bridge/EnableMessage").InnerText);
                    /* Code Modified For FB 1453 - Start */
                    //LoadList(lstTimezone, nodesTemp);
                    String selTZ = "-1";
                    lstTimezone.ClearSelection();
                    obj.GetTimezones(lstTimezone, ref selTZ);
                    /* Code Modified For FB 1453 - End */
                    //Response.Write(Session["systemTimeZone"].ToString()); 
                    //lstTimezone.Items.FindByText(Session["systemTimeZone"].ToString()).Selected = true;
                    txtMCUName.Text = xmldoc.SelectSingleNode("//bridge/name").InnerText;
                    txtMCUID.Text = xmldoc.SelectSingleNode("//bridge/bridgeID").InnerText;
                    txtMCULogin.Text = xmldoc.SelectSingleNode("//bridge/login").InnerText;
                    txtURL.Text = xmldoc.SelectSingleNode("//bridge/AccessURL").InnerText; //ZD 101078
                    //FB 3054 Starts
                    //txtPassword1.Attributes.Add("value", ns_MyVRMNet.vrmPassword.MCUProfile);
                    //txtPassword2.Attributes.Add("value", ns_MyVRMNet.vrmPassword.MCUProfile);
                    //txtPassword1.Attributes.Add("value", xmldoc.SelectSingleNode("//bridge/password").InnerText);
                    //txtPassword2.Attributes.Add("value", xmldoc.SelectSingleNode("//bridge/password").InnerText);
                    //FB 3054 Ends
                    lstTimezone.ClearSelection();//Code added for FB 1425 QA Bug
                    lstTimezone.Items.FindByValue(xmldoc.SelectSingleNode("//bridge/timeZone").InnerText).Selected = true;
                    lstStatus.Items.FindByValue(xmldoc.SelectSingleNode("//bridge/bridgeStatus").InnerText).Selected = true;
                    lstMCUType.Items.FindByValue(xmldoc.SelectSingleNode("//bridge/bridgeType").InnerText).Selected = true;
                    drpURLAccess.Items.FindByValue(xmldoc.SelectSingleNode("//bridge/URLAccess").InnerText).Selected = true;//ZD 100113
                    //ZD 100369_MCU Start
                    if (xmldoc.SelectSingleNode("//bridge/MultipleAssistant") != null)
                        txtMultipleAssistant.Text = xmldoc.SelectSingleNode("//bridge/MultipleAssistant").InnerText;
                    //ZD 100369_MCU End
                    //lstInterfaceType.Items.FindByValue(lstMCUType.SelectedValue).Selected = true;
                    //confPassword.Value = "dummy"; //FB 3054
                    Session.Remove("MCUPW"); //FB 3054
                    if (xmldoc.SelectSingleNode("//bridge/password") != null)
                        Session["MCUPW"] = xmldoc.SelectSingleNode("//bridge/password").InnerText;
                    //confPassword.Value = xmldoc.SelectSingleNode("//bridge/password").InnerText;
                    //API Port Starts...
                    if (xmldoc.SelectSingleNode("//bridge/ApiPortNo").InnerText != "")
                        txtapiportno.Text = xmldoc.SelectSingleNode("//bridge/ApiPortNo").InnerText;
                    //API Port Ends...
                    /* Code Modified For FB 1462 - Start */
                    if (xmldoc.SelectSingleNode("//bridge/bridgeAdmin/userstate").InnerText.Trim().Equals("A")) // || !xmldoc.SelectSingleNode("//bridge/bridgeAdmin/userstate").InnerText.Trim().Equals("D"))
                    {
                        txtApprover4.Text = xmldoc.SelectSingleNode("//bridge/bridgeAdmin/firstName").InnerText + " " + xmldoc.SelectSingleNode("//bridge/bridgeAdmin/lastName").InnerText;
                        hdnApprover4.Text = xmldoc.SelectSingleNode("//bridge/bridgeAdmin/ID").InnerText;
                        txtApprover4_1.Value = txtApprover4.Text;
                        hdnApprover4_1.Value = hdnApprover4.Text;
                    }
                    /* Code Modified For FB 1462 - End */                  
                    if (xmldoc.SelectSingleNode("//bridge/virtualBridge").InnerText.Equals("1"))
                        chkIsVirtual.Checked = true;
                    //FB 1920 starts
                    if (xmldoc.SelectSingleNode("//bridge/isPublic").InnerText.Equals("1")) 
                        chkbxIsPublic.Checked = true;
                    // FB 1920 Ends

                    // FB 2501 Starts
                    if (xmldoc.SelectSingleNode("//bridge/setFavourite").InnerText.Equals("1"))
                        chkSetFavourite.Checked = true;
                    // FB 2501 Ends

                    //FB 2660 Starts
                    spCDREvent.Attributes.Add("style", "display:none");
                    divtxtCDR.Attributes.Add("style", "display:none");
                    spEnableCDR.Attributes.Add("style", "display:none");
                    chkEnableCDR.Attributes.Add("style", "display:none");
                    if (enableCDR == 1)
                    {
                        spEnableCDR.Attributes.Add("style", "display:block");
                        chkEnableCDR.Attributes.Add("style", "display:block");
                    }

                    if (xmldoc.SelectSingleNode("//bridge/enableCDR").InnerText.Equals("1"))
                        chkEnableCDR.Checked = true;

                    if (xmldoc.SelectSingleNode("//bridge/deleteCDRDays") != null)
                        txtdltCDR.Text = xmldoc.SelectSingleNode("//bridge/deleteCDRDays").InnerText.Trim();

                    if (chkEnableCDR.Checked && enableCDR == 1)
                    {
                        spCDREvent.Attributes.Add("style", "display:block");
                        divtxtCDR.Attributes.Add("style", "display:block");
                    }

                    //FB 2660 Ends
                    ChangeFirmwareVersion(new object(), new EventArgs());
                    //FB 2486 - Start
                    if (EnableMessage == 1)
                    {
                        trEnableMessage.Disabled = false;
                        chkMessage.Checked = true;
                        chkMessage.Enabled = true;
                        chkMessage.Visible = true; //ZD 100263
                    }
                    //FB 2486 - End
                    txtMaxAudioCalls.Text = xmldoc.SelectSingleNode("//bridge/bridgeAdmin/maxAudioCalls").InnerText;
                    txtMaxVideoCalls.Text = xmldoc.SelectSingleNode("//bridge/bridgeAdmin/maxVideoCalls").InnerText;
                    //Code Modified For FB 1222 - Start
                    //if (lstInterfaceType.SelectedItem.Text.Equals("1") || lstInterfaceType.SelectedItem.Text.Equals("4"))
                 
                    log.Trace(lstInterfaceType.SelectedItem.Text);
                    if (lstInterfaceType.SelectedItem.Text.Equals("1") || lstInterfaceType.SelectedItem.Text.Equals("4") || lstInterfaceType.SelectedItem.Text.Equals("5"))
                    //Code Modified For FB 1222 - End
                    {
                        txtPortP.Text = xmldoc.SelectSingleNode("//bridge/bridgeDetails/controlPortIPAddress").InnerText;
                        //FB 2016 -Starts
                        if (lstInterfaceType.SelectedItem.Text.Equals("5") || lstInterfaceType.SelectedItem.Text.Equals("1"))//FB 2427
                        {
                            if (xmldoc.SelectSingleNode("//bridge/bridgeDetails/ConferenceServiceID").InnerText.Trim() != null)
                                txtConfServiceID.Text = xmldoc.SelectSingleNode("//bridge/bridgeDetails/ConferenceServiceID").InnerText;
							
							 //FB 2610 Start
                            if (xmldoc.SelectSingleNode("//bridge/bridgeDetails/BridgeExtNo") != null)
                            {
                                txtExtNumber.Text = xmldoc.SelectSingleNode("//bridge/bridgeDetails/BridgeExtNo").InnerText;
                                txtTempExtNumber.Text = xmldoc.SelectSingleNode("//bridge/bridgeDetails/BridgeExtNo").InnerText;
                            }
                            //FB 2610 End
                            //ZD 100522 Start
                            if (xmldoc.SelectSingleNode("//bridge/bridgeDetails/BridgeDomain") != null)
                            {
                                txtTempDomain.Text = xmldoc.SelectSingleNode("//bridge/bridgeDetails/BridgeDomain").InnerText;
                                txtMCUDomain.Text = xmldoc.SelectSingleNode("//bridge/bridgeDetails/BridgeDomain").InnerText;
                            }
                            //ZD 100522 End
                            //C20 S4B Start
                            if (xmldoc.SelectSingleNode("//bridge/bridgeDetails/ConferenceDialinName").InnerText.Trim() != null)
                                txtDialinInfo.Text = xmldoc.SelectSingleNode("//bridge/bridgeDetails/ConferenceDialinName").InnerText;
                            //C20 S4B End
                            //FB 2591 Starts
                            Int32.TryParse(txtConfServiceID.Text, out ProfileID);
                            //FB 2441 II Starts ZD 103787
                            if (lstMCUType.SelectedValue.Equals("8") || lstMCUType.SelectedValue.Equals("13") || lstMCUType.SelectedValue.Equals("17") || lstMCUType.SelectedValue.Equals("18") || lstMCUType.SelectedValue.Equals("19")) 
                            {
                                XmlNodeList nodesTemp1 = xmldoc.SelectNodes("//bridge/bridgeDetails/Profiles/Profile");
                                if (lstMCUType.SelectedValue.Equals("8") || lstMCUType.SelectedValue.Equals("17") || lstMCUType.SelectedValue.Equals("18") || lstMCUType.SelectedValue.Equals("19")) 
                                {
                                    txtProfileID.Items.Clear(); //FB 2876
                                    if (nodesTemp1.Count > 0)
                                    {
                                        if (nodesTemp1.Count > 0)
                                            obj.LoadList(txtProfileID, nodesTemp1, "ID", "Name");
                                    }
                                    else
                                        txtProfileID.Items.Insert(0, new ListItem(obj.GetTranslatedText("None"),"0"));
                                   txtProfileID.Items.FindByValue(xmldoc.SelectSingleNode("//bridge/bridgeDetails/ConferenceServiceID").InnerText).Selected = true;//FB 2696
                                }
                                if (lstMCUType.SelectedValue.Equals("13"))
                                {
                                    LoadList(lstTemplate, nodesTemp1);
                                    lstTemplate.Items.Insert(0, new ListItem(obj.GetTranslatedText("None"), "0")); //FB 2876 
                                    if (lstTemplate.Items.FindByValue(ProfileID.ToString()) != null)
                                        lstTemplate.Items.FindByValue(ProfileID.ToString()).Selected = true;//FB 2441-II
                                }
								//FB 2441 II Ends
                                //txtProfileID.Items.FindByValue("0").Selected = true; //FB 2636
                            }
                            //FB 2591 Ends
                                       
                        }

                        //FB 2441 Starts
                        if (lstMCUType.SelectedValue.Equals("13")) //Polycom RPRM
                        {
                            int DMAMonitor = 0;
							// FB 2441 II Starts
                            if (xmldoc.SelectSingleNode("//bridge/bridgeDetails/PolycomRPRM/RPRMDomain") != null)
                            {
                                if (xmldoc.SelectSingleNode("//bridge/bridgeDetails/PolycomRPRM/RPRMDomain").InnerText != null)
                                    txtDomain.Text = xmldoc.SelectSingleNode("//bridge/bridgeDetails/PolycomRPRM/RPRMDomain").InnerText;
                            }
							// FB 2441 II Starts
                            if (xmldoc.SelectSingleNode("//bridge/bridgeDetails/PolycomRPRM/Sendmail") != null)
                            {
                                chkSendmail.Checked = (xmldoc.SelectSingleNode("//bridge/bridgeDetails/PolycomRPRM/Sendmail").InnerText.Trim().Equals("1"));
                            }
                            //FB 2709
                            if (xmldoc.SelectSingleNode("//bridge/bridgeDetails/PolycomRPRM/LoginAccount") != null)
                            {
                                if (xmldoc.SelectSingleNode("//bridge/bridgeDetails/PolycomRPRM/LoginAccount").InnerText != null)
                                    txtRPRMLogin.Text = xmldoc.SelectSingleNode("//bridge/bridgeDetails/PolycomRPRM/LoginAccount").InnerText;

                                hdnRPRMUserid.Value = txtRPRMLogin.Text.ToLower().Trim();
                            }
                            if (xmldoc.SelectSingleNode("//bridge/bridgeDetails/PolycomRPRM/LoginCount") != null)
                            {
                                if (xmldoc.SelectSingleNode("//bridge/bridgeDetails/PolycomRPRM/LoginCount").InnerText != null)
                                    txtCallsCount.Text = xmldoc.SelectSingleNode("//bridge/bridgeDetails/PolycomRPRM/LoginCount").InnerText;

                                hdnRPRMCallCount.Value = txtCallsCount.Text.Trim();
                            }
                            if (xmldoc.SelectSingleNode("//bridge/bridgeDetails/PolycomRPRM/RPRMEmail") != null)
                            {
                                if (xmldoc.SelectSingleNode("//bridge/bridgeDetails/PolycomRPRM/RPRMEmail").InnerText != null)
                                    txtRPRMEmail.Text = xmldoc.SelectSingleNode("//bridge/bridgeDetails/PolycomRPRM/RPRMEmail").InnerText;
                            }
                            //FB 2709
                            if (xmldoc.SelectSingleNode("//bridge/bridgeDetails/PolycomRPRM/MonitorMCU") != null)
                                int.TryParse(xmldoc.SelectSingleNode("//bridge/bridgeDetails/PolycomRPRM/MonitorMCU").InnerText.Trim(), out DMAMonitor);

                            ChkEhancedMCU.Checked = false;
                            if (DMAMonitor == 1)
                            {
                                trDMAName.Attributes.Add("style", "display:");
                                trDMAPassword.Attributes.Add("style", "display:");
                                trDMAURL.Attributes.Add("style", "display:");
                                trDMADialinprefix.Attributes.Add("style", "display:"); // FB 2689
                                ChkEhancedMCU.Checked = true;
                                ChkEhancedMCU.Enabled = true;
                                ChkEhancedMCU.Visible = true; //ZD 100263
                                trEhancedMCU.Visible = true; //ZD 100263
                                if (xmldoc.SelectSingleNode("//bridge/bridgeDetails/PolycomRPRM/DMAName") != null)
                                {
                                    if (xmldoc.SelectSingleNode("//bridge/bridgeDetails/PolycomRPRM/DMAName").InnerText != null)
                                        txtDMAName.Text = xmldoc.SelectSingleNode("//bridge/bridgeDetails/PolycomRPRM/DMAName").InnerText;
                                }
                                if (xmldoc.SelectSingleNode("//bridge/bridgeDetails/PolycomRPRM/DMALogin") != null)
                                {
                                    if (xmldoc.SelectSingleNode("//bridge/bridgeDetails/PolycomRPRM/DMALogin").InnerText != null)
                                        txtDMALogin.Text = xmldoc.SelectSingleNode("//bridge/bridgeDetails/PolycomRPRM/DMALogin").InnerText;
                                }

                                if (xmldoc.SelectSingleNode("//bridge/bridgeDetails/PolycomRPRM/DMAPassword") != null)
                                {
                                    //FB 3054 Starts
                                    //txtDMAPassword.Attributes.Add("value", ns_MyVRMNet.vrmPassword.DMAPW);
                                    //txtDMARetypePwd.Attributes.Add("value", ns_MyVRMNet.vrmPassword.DMAPW);

                                    if (xmldoc.SelectSingleNode("//bridge/bridgeDetails/PolycomRPRM/DMAPassword").InnerText != null)
                                    {
                                        //txtDMAPassword.Attributes.Add("value", xmldoc.SelectSingleNode("//bridge/bridgeDetails/PolycomRPRM/DMAPassword").InnerText);
                                        //txtDMARetypePwd.Attributes.Add("value", xmldoc.SelectSingleNode("//bridge/bridgeDetails/PolycomRPRM/DMAPassword").InnerText);
                                        Session.Remove("DMAPW"); //FB 3054
                                        Session["DMAPW"] = xmldoc.SelectSingleNode("//bridge/bridgeDetails/PolycomRPRM/DMAPassword").InnerText;
                                    }
                                    //FB 3054 Ends
                                }

                                if (xmldoc.SelectSingleNode("//bridge/bridgeDetails/PolycomRPRM/DMAURL") != null)
                                {
                                    if (xmldoc.SelectSingleNode("//bridge/bridgeDetails/PolycomRPRM/DMAURL").InnerText != null)
                                        txtDMAURL.Text = xmldoc.SelectSingleNode("//bridge/bridgeDetails/PolycomRPRM/DMAURL").InnerText;
                                }
                                if (xmldoc.SelectSingleNode("//bridge/bridgeDetails/PolycomRPRM/DMAPort") != null)
                                {
                                    if (xmldoc.SelectSingleNode("//bridge/bridgeDetails/PolycomRPRM/DMAPort").InnerText != null)
                                        txtDMAPort.Text = xmldoc.SelectSingleNode("//bridge/bridgeDetails/PolycomRPRM/DMAPort").InnerText;
                                }
                                //FB 2689 start
                                if (xmldoc.SelectSingleNode("//bridge/bridgeDetails/PolycomRPRM/DMADialinPrefix") != null)
                                {
                                    if (xmldoc.SelectSingleNode("//bridge/bridgeDetails/PolycomRPRM/DMADialinPrefix").InnerText != null)
                                        txtDialinprefix.Text = xmldoc.SelectSingleNode("//bridge/bridgeDetails/PolycomRPRM/DMADialinPrefix").InnerText.Trim();
                                }
                                //FB 2683 Start
                                if (xmldoc.SelectSingleNode("//bridge/bridgeDetails/PolycomRPRM/DMADialinPrefix") != null)
                                {
                                    if (xmldoc.SelectSingleNode("//bridge/bridgeDetails/PolycomRPRM/DMADomain").InnerText != null)
                                        txtDMADomain.Text = xmldoc.SelectSingleNode("//bridge/bridgeDetails/PolycomRPRM/DMADomain").InnerText.Trim();
                                }
                                //FB 2683 End
                                
                            }
                            if (xmldoc.SelectSingleNode("//bridge/bridgeDetails/PolycomRPRM/Synchronous") != null)
                            {
                                if (xmldoc.SelectSingleNode("//bridge/bridgeDetails/PolycomRPRM/Synchronous").InnerText.Trim() == "1")
                                    chkSynchronous.Checked = true;
                                else
                                    chkSynchronous.Checked = false;
                            }
                            //FB 2689 end

                            //ZD 104256 Starts
                            XmlNodeList nodesTemp1 = xmldoc.SelectNodes("//bridge/bridgeDetails/PoolOrders/PoolOrder");
                            drpPoolOrder.Items.Clear();
                            if (nodesTemp1.Count > 0)
                                obj.LoadList(drpPoolOrder, nodesTemp1, "Id", "Name");
                            else
                                drpPoolOrder.Items.Insert(0, new ListItem(obj.GetTranslatedText("None"), "0"));
                            drpPoolOrder.Items.FindByValue(xmldoc.SelectSingleNode("//bridge/bridgeDetails/ConferencePoolOrderID").InnerText).Selected = true;//FB 2696
                            //ZD 104256 Ends

                        }
                        //FB 2441 Ends

                        //FB 1766 - Start
                        if (!lstInterfaceType.SelectedItem.Text.Equals("5"))//NGC Fix & Radvision elite fixes
                        {
                            if (lstInterfaceType.SelectedItem.Text.Equals("1"))
                            {
                                if (xmldoc.SelectSingleNode("//bridge/bridgeDetails/EnableIVR").InnerText.Trim().Equals("1"))
                                {
                                    chkEnableIVR.Checked = true;
                                    txtIVRName.Enabled = true;
                                }
                                txtIVRName.Text = xmldoc.SelectSingleNode("//bridge/bridgeDetails/IVRServiceName").InnerText;
                            }
                            //FB 1766 - End
                            //FB 1907 - Start
                            if (xmldoc.SelectSingleNode("//bridge/bridgeDetails/enableRecord") != null)
                            {
                                if (xmldoc.SelectSingleNode("//bridge/bridgeDetails/enableRecord").InnerText.Trim().Equals("1"))
                                {
                                    chkEnableRecord.Checked = true;
                                }
                                else
                                {
                                    chkEnableRecord.Checked = false;

                                }
                            }

                            if (xmldoc.SelectSingleNode("//bridge/bridgeDetails/enableLPR") != null)
                            {
                                if (xmldoc.SelectSingleNode("//bridge/bridgeDetails/enableLPR").InnerText.Trim().Equals("1"))
                                {
                                    chkLPR.Checked = true;
                                }
                                else
                                {
                                    chkLPR.Checked = false;

                                }
                            }

                            if (xmldoc.SelectSingleNode("//bridge/bridgeDetails/EnableMessage") != null)
                            {
                                if (xmldoc.SelectSingleNode("//bridge/bridgeDetails/EnableMessage").InnerText.Trim().Equals("1"))//FB 2486
                                {
                                    chkMessage.Checked = true;
                                    trEnableMessage.Visible = true; //ZD 100263
                                }
                                else
                                {
                                    chkMessage.Checked = false;
                                }
                            }
                         
                        }
                        //FB 1907 - End
                        XmlNodeList nodes = xmldoc.SelectNodes("//bridge/bridgeDetails/IPServices/IPService");
                        if (nodes.Count > 0)
                        {
                            GetServices(nodes, dgIPServices);
                            lblNoIPServices.Visible = false;
                            btnAddNewIPService.Visible = false;
                            dgIPServices.Visible = true;
                        }
                        else
                        {
                            lblNoIPServices.Visible = true;
                            btnAddNewIPService.Visible = true;
                            dgIPServices.Visible = false;
                        }
                        nodes = xmldoc.SelectNodes("//bridge/bridgeDetails/ISDNServices/ISDNService");
                        if (nodes.Count > 0)
                        {
                            GetServices(nodes, dgISDNServices);
                            lblNoISDNServices.Visible = false;
                            btnAddISDNService.Visible = false;
                            dgISDNServices.Visible = true;
                        }
                        else
                        {
                            lblNoISDNServices.Visible = true;
                            btnAddISDNService.Visible = true;
                            dgISDNServices.Visible = false;
                        }
                        nodes = xmldoc.SelectNodes("//bridge/bridgeDetails/MPIServices/MPIService");
                        if (nodes.Count > 0)
                        {
                            GetServices(nodes, dgMPIServices);
                            //lblNoMPIServices.Visible = false;
                            //btnAddNewMPIService.Visible = false;//ZD 103595
                            dgMPIServices.Visible = true;
                        }
                        else
                        {
                            //lblNoMPIServices.Visible = true;
                            //btnAddNewMPIService.Visible = true;//ZD 103595
                            dgMPIServices.Visible = false;
                        }
                    }
                    if (lstInterfaceType.SelectedItem.Text.Equals("3") || lstInterfaceType.SelectedItem.Text.Equals("6") || lstInterfaceType.SelectedItem.Text.Equals("8") || lstInterfaceType.SelectedItem.Text.Equals("10"))//FB 2008 //FB 2501 Call Monitoring //ZD 101217
                    {
                        //FB 2718 Starts
                        if (lstMCUType.SelectedValue.Equals("15"))
                        {
                            txtPortP.Text = xmldoc.SelectSingleNode("//bridge/bridgeDetails/portA").InnerText;
                            chkSynchronous.Checked = false;
                            if (xmldoc.SelectSingleNode("//bridge/bridgeDetails/Synchronous") != null)
                                if (xmldoc.SelectSingleNode("//bridge/bridgeDetails/Synchronous").InnerText.Trim().Equals("1"))
                                    chkSynchronous.Checked = true;
                                else
                                    chkSynchronous.Checked = false;
                            if (xmldoc.SelectSingleNode("//bridge/bridgeDetails/Domain") != null)
                                txtDomain.Text = xmldoc.SelectSingleNode("//bridge/bridgeDetails/Domain").InnerText;

                        }
                        //FB 2718 Ends
                        
                        txtPortA.Text = xmldoc.SelectSingleNode("//bridge/bridgeDetails/portA").InnerText;
                        txtPortB.Text = xmldoc.SelectSingleNode("//bridge/bridgeDetails/portB").InnerText;
                        
                        //FB 2610 Start
                        if (xmldoc.SelectSingleNode("//bridge/bridgeDetails/BridgeExtNo") != null)
                            txtExtNumber.Text = xmldoc.SelectSingleNode("//bridge/bridgeDetails/BridgeExtNo").InnerText;
                        //FB 2610 End

                        //ZD 100522 Start
                        if (xmldoc.SelectSingleNode("//bridge/bridgeDetails/BridgeDomain") != null)
                            txtTempDomain.Text = xmldoc.SelectSingleNode("//bridge/bridgeDetails/BridgeDomain").InnerText;
                        //ZD 100522 End

                        //C20 S4B Start
                        if (xmldoc.SelectSingleNode("//bridge/bridgeDetails/ConferenceDialinName").InnerText.Trim() != null)
                            txtDialinInfo.Text = xmldoc.SelectSingleNode("//bridge/bridgeDetails/ConferenceDialinName").InnerText;
                        //C20 S4B End

                        if (xmldoc.SelectSingleNode("//bridge/bridgeDetails/ISDNGateway") != null)//Fb 2008
                            txtISDNGateway.Text = xmldoc.SelectSingleNode("//bridge/bridgeDetails/ISDNGateway").InnerText;

						 //FB 2003 - Start
                        txtISDNAudioPref.Text = xmldoc.SelectSingleNode("//bridge/bridgeDetails/ISDNAudioPref").InnerText;
                        txtISDNVideoPref.Text = xmldoc.SelectSingleNode("//bridge/bridgeDetails/ISDNVideoPref").InnerText;
                        //FB 2003 - End

                        //FB 2636 Start
                        isE164H323 = false;
                        if (xmldoc.SelectSingleNode("//bridge/bridgeDetails/E164Dialing") != null)
                        {
                            if (xmldoc.SelectSingleNode("//bridge/bridgeDetails/E164Dialing").InnerText.Equals("1"))
                            {
                                isE164H323 = true;
                                chkE164.Checked = true;
                            }
                            else
                            {
                                chkE164.Checked = false;
                            }
                        }
                        if (xmldoc.SelectSingleNode("//bridge/bridgeDetails/H323Dialing") != null)
                        {
                            if (xmldoc.SelectSingleNode("//bridge/bridgeDetails/H323Dialing").InnerText.Equals("1"))
                            {
                                isE164H323 = true;
                                chkH323.Checked = true;
                            }
                            else
                            {
                                chkH323.Checked = false;
                            }
                        }
                        if (isE164H323)
                        {
                            chkH323.Enabled = true;
                            chkE164.Enabled = true;
                            chkE164.Visible = true; //ZD 100263
                            chkH323.Visible = true; //ZD 100263
                            lblE164.Visible = true;  //ZD 100263
                            trEnableMessage.Visible = true; //ZD 100263
                            lblEH323.Visible = true; //ZD 100263
                        }
                        XmlNodeList nodeList = xmldoc.SelectNodes("//bridge/bridgeDetails/E164Services/E164Service");
                        if (nodeList.Count > 0)
                        {
                            GetServices(nodeList, dgE164Services);
                            lblNoE164Services.Visible = false;
                            btnAddNewE164Service.Visible = false;
                            dgE164Services.Visible = true;
                        }
                        else
                        {
                            lblNoE164Services.Visible = true;
                            btnAddNewE164Service.Visible = true;
                            dgE164Services.Visible = false;
                        }
                        // FB 2636 Ends
                    }
                    else if (lstInterfaceType.SelectedItem.Text.Equals("11")) //ZD 103550 Blue Jeans
                    {
                        Session.Remove("BJNAppkey");
                        Session["BJNAppkey"] = xmldoc.SelectSingleNode("//bridge/bridgeDetails/BJNAppKey").InnerText;
                        Session.Remove("BJNAppSecret");
                        Session["BJNAppSecret"] = xmldoc.SelectSingleNode("//bridge/bridgeDetails/BJNAppSecret").InnerText;
                        txtBJNAddress.Text = xmldoc.SelectSingleNode("//bridge/bridgeDetails/controlPortIPAddress").InnerText;
                    }
                    if (lstInterfaceType.SelectedItem.Text.Equals("7"))//FB 2261
                    {
                        txtPortP.Text = xmldoc.SelectSingleNode("//bridge/bridgeDetails/controlPortIPAddress").InnerText;
                        //FB 2610 Start
                        if (xmldoc.SelectSingleNode("//bridge/bridgeDetails/BridgeExtNo") != null)
                            txtExtNumber.Text = xmldoc.SelectSingleNode("//bridge/bridgeDetails/BridgeExtNo").InnerText;
                        //FB 2610 End
                        //ZD 100522 Start
                        if (xmldoc.SelectSingleNode("//bridge/bridgeDetails/BridgeDomain") != null)
                            txtTempDomain.Text = xmldoc.SelectSingleNode("//bridge/bridgeDetails/BridgeDomain").InnerText;
                        //ZD 100522 End
                        //C20 S4B Start
                        if (xmldoc.SelectSingleNode("//bridge/bridgeDetails/ConferenceDialinName").InnerText.Trim() != null)
                            txtDialinInfo.Text = xmldoc.SelectSingleNode("//bridge/bridgeDetails/ConferenceDialinName").InnerText;
                        //C20 S4B End
                    }
                    //FB 2556 - Starts
                    if (lstInterfaceType.SelectedItem.Text.Equals("9"))
                    {
                        if (xmldoc.SelectSingleNode("//bridge/bridgeDetails/controlPortIPAddress") != null)
                            txtPortP.Text = xmldoc.SelectSingleNode("//bridge/bridgeDetails/controlPortIPAddress").InnerText.Trim();

                        if (xmldoc.SelectSingleNode("//bridge/bridgeDetails/orgSpecificDetails") != null)
                        {

                            if (xmldoc.SelectSingleNode("//bridge/bridgeDetails/orgSpecificDetails/ScopiaDesktopURL") != null)
                                txtScopiaURL.Text = xmldoc.SelectSingleNode("//bridge/bridgeDetails/orgSpecificDetails/ScopiaDesktopURL").InnerText;

                            lstScopiaOrg.ClearSelection();
                            XmlNodeList nodesExt = xmldoc.SelectNodes("//bridge/bridgeDetails/ExtSiloProfiles/Profile");
                            if (nodesExt != null)
                                LoadList(lstScopiaOrg, nodesExt);
                            lstScopiaOrg.Items.Insert(0, obj.GetTranslatedText("None"));

                            lstScopiaService.ClearSelection();
                            nodesExt = xmldoc.SelectNodes("//bridge/bridgeDetails/ExtServiceProfiles/Profile");
                            if (nodesExt != null)
                                LoadList(lstScopiaService, nodesExt);
                            lstScopiaService.Items.Insert(0, obj.GetTranslatedText("None"));

                            //if (xmldoc.SelectSingleNode("//bridge/bridgeDetails/orgSpecificDetails/ScopiaOrgID") != null)
                            //    txtScopiaOrgID.Text = xmldoc.SelectSingleNode("//bridge/bridgeDetails/orgSpecificDetails/ScopiaOrgID").InnerText;

                            if (xmldoc.SelectSingleNode("//bridge/bridgeDetails/orgSpecificDetails/ScopiaOrgLogin") != null)
                                txtScopiaOrgLogin.Text = xmldoc.SelectSingleNode("//bridge/bridgeDetails/orgSpecificDetails/ScopiaOrgLogin").InnerText;

                            //if (xmldoc.SelectSingleNode("//bridge/bridgeDetails/orgSpecificDetails/ScopiaServiceID") != null)
                            //    lstScopiaService.SelectedValue = xmldoc.SelectSingleNode("//bridge/bridgeDetails/orgSpecificDetails/ScopiaServiceID").InnerText;

                            if (lstScopiaService.Items.FindByValue(xmldoc.SelectSingleNode("//bridge/bridgeDetails/orgSpecificDetails/ScopiaServiceID").InnerText) != null)
                                lstScopiaService.Items.FindByValue(xmldoc.SelectSingleNode("//bridge/bridgeDetails/orgSpecificDetails/ScopiaServiceID").InnerText).Selected = true;

                            if (lstScopiaOrg.Items.FindByValue(xmldoc.SelectSingleNode("//bridge/bridgeDetails/orgSpecificDetails/ScopiaOrgID").InnerText) != null)
                                lstScopiaOrg.Items.FindByValue(xmldoc.SelectSingleNode("//bridge/bridgeDetails/orgSpecificDetails/ScopiaOrgID").InnerText).Selected = true;
                        }

                       

                        chkMultiTenant.Checked = false;
                        if (xmldoc.SelectSingleNode("//bridge/bridgeDetails/Multitenant") != null)
                            if (xmldoc.SelectSingleNode("//bridge/bridgeDetails/Multitenant").InnerText.Trim().Equals("1"))
                                chkMultiTenant.Checked = true;


                        chkSynchronous.Checked = false;
                        if (xmldoc.SelectSingleNode("//bridge/bridgeDetails/Synchronous") != null)
                            if (xmldoc.SelectSingleNode("//bridge/bridgeDetails/Synchronous").InnerText.Trim().Equals("1"))
                                chkSynchronous.Checked = true;
                    }
                    //FB 2556 - End

					//ZD 101522 Starts
                    if (lstInterfaceType.SelectedItem.Text.Equals("10"))
                    {
                        lstSystemLocation.ClearSelection();
                        XmlNodeList nodesExt = xmldoc.SelectNodes("//bridge/bridgeDetails/GetSystemLocations/GetSystemLocation");
                        if (nodesExt != null)
                            LoadList(lstSystemLocation, nodesExt);
                        if (lstSystemLocation.Items.Count == 0) //ZD 104821
                            lstSystemLocation.Items.Insert(0, obj.GetTranslatedText("None"));

                        if (xmldoc.SelectSingleNode("//bridge/bridgeDetails/SysLocationId") != null && lstSystemLocation.Items.FindByValue(xmldoc.SelectSingleNode("//bridge/bridgeDetails/SysLocationId").InnerText) != null)
                            lstSystemLocation.Items.FindByValue(xmldoc.SelectSingleNode("//bridge/bridgeDetails/SysLocationId").InnerText).Selected = true;
                        //ALLDEV-854 Start
                        if (xmldoc.SelectSingleNode("//bridge/bridgeDetails/EnableAlias") != null && DrpDwnAlias.Items.FindByValue(xmldoc.SelectSingleNode("//bridge/bridgeDetails/EnableAlias").InnerText) != null)
                            DrpDwnAlias.Items.FindByValue(xmldoc.SelectSingleNode("//bridge/bridgeDetails/EnableAlias").InnerText).Selected = true;

                        if (xmldoc.SelectSingleNode("//bridge/bridgeDetails/EnableAlias").InnerText.Trim().Equals("1"))
                            trAlias.Attributes.Add("Style", "visibility:visible;");
                        else
                            trAlias.Attributes.Add("Style", "visibility:hidden;");

                        txtAlias2.Text = xmldoc.SelectSingleNode("//bridge/bridgeDetails/Alias2").InnerText;
                        txtAlias3.Text = xmldoc.SelectSingleNode("//bridge/bridgeDetails/Alias3").InnerText;
                        //ALLDEV-854 End
                    }
					//ZD 101522 End
                    bool adminStatus = true;    //FB 1462
                    nodesTemp = xmldoc.SelectNodes("//bridge/approvers/approver");
                    foreach (XmlNode node in nodesTemp)
                    {
                        if (txtApprover1.Text.Equals(""))
                        {
                            txtApprover1.Text = node.SelectSingleNode("firstName").InnerText + " " + node.SelectSingleNode("lastName").InnerText;
                            hdnApprover1.Text = node.SelectSingleNode("ID").InnerText;
                            txtApprover1_1.Value = txtApprover1.Text;
                            hdnApprover1_1.Value = hdnApprover1.Text;
                        }
                        else if (txtApprover2.Text.Equals(""))
                        {
                            txtApprover2.Text = node.SelectSingleNode("firstName").InnerText + " " + node.SelectSingleNode("lastName").InnerText;
                            hdnApprover2.Text = node.SelectSingleNode("ID").InnerText;
                            txtApprover2_1.Value = txtApprover2.Text;
                            hdnApprover2_1.Value = hdnApprover2.Text;
                        }
                        else if (txtApprover3.Text.Equals(""))
                        {
                            txtApprover3.Text = node.SelectSingleNode("firstName").InnerText + " " + node.SelectSingleNode("lastName").InnerText;
                            hdnApprover3.Text = node.SelectSingleNode("ID").InnerText;
                            txtApprover3_1.Value = txtApprover3.Text;
                            hdnApprover3_1.Value = hdnApprover3.Text;
                        }
                    }
                    //ZD 100040 - Start
                    nodesTemp = xmldoc.SelectNodes("//bridge/videoPorts/port");
                    if (nodesTemp.Count > 0)
                    {
                        LoadVideoPortGrid(nodesTemp, 1);
                        dgVideoPort.Visible = true;
                    }
                    nodesTemp = xmldoc.SelectNodes("//bridge/mcuLoadBalanceGroup/mcu");
                    tdImgLoadBalance.Attributes.Add("style", "display:none");
                    if (nodesTemp.Count > 0)
                    {
                        LoadVideoPortGrid(nodesTemp, 2);
                        tdImgLoadBalance.Attributes.Add("style", "display:''");
                        chkIsVirtual.Attributes.Add("onclick", "javascript:ShowHideLoadbal();");
                        dgloadBalance.Visible = true;
                    }
                    //ZD 100040 - End                    

                    lstFirmwareVersion.Items.FindByValue(xmldoc.SelectSingleNode("//bridge/firmwareVersion").InnerText).Selected = true;
                    if (xmldoc.SelectSingleNode("//bridge/malfunctionAlert").InnerText.Trim().Equals("1"))
                        chkMalfunctionAlert.Checked = true;
                    if (xmldoc.SelectSingleNode("//bridge/ISDNThresholdAlert").InnerText.Trim().Equals("1"))
                    {
                        chkISDNThresholdAlert.Checked = true;
                        trISDN.Visible = true;
                        trISDN.Attributes.Add("style", "display:");
                        txtMCUISDNPortCharge.Text = xmldoc.SelectSingleNode("//bridge/ISDNPortCharge").InnerText;
                        txtISDNLineCost.Text = xmldoc.SelectSingleNode("//bridge/ISDNLineCharge").InnerText;
                        txtISDNMaxCost.Text = xmldoc.SelectSingleNode("//bridge/ISDNMaxCost").InnerText;
                        txtISDNThresholdPercentage.Text = xmldoc.SelectSingleNode("//bridge/ISDNThreshold").InnerText;
                        if (xmldoc.SelectSingleNode("//bridge/ISDNThresholdTimeframe").InnerText.Equals(""))
                            rdISDNThresholdTimeframe.Items.FindByValue("2").Selected = true;
                        else
                            rdISDNThresholdTimeframe.Items.FindByValue(xmldoc.SelectSingleNode("//bridge/ISDNThresholdTimeframe").InnerText).Selected = true;
                    }
                    nodesTemp = xmldoc.SelectNodes("//bridge/MCUCards/MCUCard");
                    LoadMCUCards("2", nodesTemp);//FB Case 460 Saima
                    /* *** Code Added For FB 1462 - Start *** */
                    if (xmldoc.SelectSingleNode("//bridge/bridgeAdmin/userstate").InnerText.Trim().Equals("I"))
                    {
                        if (errLabel.Text == "")
                            errLabel.Text += "Error:MCU (" + xmldoc.SelectSingleNode("//bridge/name").InnerText + ") Admin User - " + xmldoc.SelectSingleNode("//bridge/bridgeAdmin/firstName").InnerText.Trim() + ", " + xmldoc.SelectSingleNode("//bridge/bridgeAdmin/lastName").InnerText.Trim() + " is In-Active. Please make sure the user is Active.";
                        else
                            errLabel.Text += "<br>Error:MCU (" + xmldoc.SelectSingleNode("//bridge/name").InnerText + ") Admin User - " + xmldoc.SelectSingleNode("//bridge/bridgeAdmin/firstName").InnerText.Trim() + ", " + xmldoc.SelectSingleNode("//bridge/bridgeAdmin/lastName").InnerText.Trim() + " is In-Active. Please make sure the user is Active.";

                        adminStatus = false;
                    }
                    else if (xmldoc.SelectSingleNode("//bridge/bridgeAdmin/userstate").InnerText.Trim().Equals("D"))
                    {
                        if (errLabel.Text == "")
                            errLabel.Text += "Error:MCU (" + xmldoc.SelectSingleNode("//bridge/name").InnerText + ")" + obj.GetErrorMessage(430);//FB 1881
                            //errLabel.Text += "Error:MCU (" + xmldoc.SelectSingleNode("//bridge/name").InnerText + ") Admin User has been Deleted";
                        else
                            errLabel.Text += "<br>Error:MCU (" + xmldoc.SelectSingleNode("//bridge/name").InnerText + ")" + obj.GetErrorMessage(430);//FB 1881
                            //errLabel.Text += "<br>Error:MCU (" + xmldoc.SelectSingleNode("//bridge/name").InnerText + ") Admin User has been Deleted";

                        adminStatus = false;
                    }

                    if (!adminStatus)
                        errLabel.Visible = true;
                    /* *** Code Added For FB 1462 - End *** */
					hdntzone.Value = "1";//Code added for  FB 1425 QA Bug


                    //FB 1642 - DTMF Start Commented for FB 2655
                    //if (xmldoc.SelectSingleNode("//bridge/DTMF/PreConfCode") != null)
                    //    PreConfCode.Text = xmldoc.SelectSingleNode("//bridge/DTMF/PreConfCode").InnerText;

                    //if (xmldoc.SelectSingleNode("//bridge/DTMF/PreLeaderPin") != null)
                    //    PreLeaderPin.Text = xmldoc.SelectSingleNode("//bridge/DTMF/PreLeaderPin").InnerText;

                    //if (xmldoc.SelectSingleNode("//bridge/DTMF/PostLeaderPin") != null)
                    //    PostLeaderPin.Text = xmldoc.SelectSingleNode("//bridge/DTMF/PostLeaderPin").InnerText;

                    //FB 1642 DTMF End
                    //FB 2610 Start
                    if (xmldoc.SelectSingleNode("//bridge/bridgeDetails/BridgeExtNo") != null)
                    {
                        txtExtNumber.Text = xmldoc.SelectSingleNode("//bridge/bridgeDetails/BridgeExtNo").InnerText;
                        txtTempExtNumber.Text = xmldoc.SelectSingleNode("//bridge/bridgeDetails/BridgeExtNo").InnerText;
                    }
                    //FB 2610 End

                    //ZD 100522 Start
                    if (xmldoc.SelectSingleNode("//bridge/bridgeDetails/BridgeDomain") != null)
                    {
                        txtTempDomain.Text = xmldoc.SelectSingleNode("//bridge/bridgeDetails/BridgeDomain").InnerText;
                        txtMCUDomain.Text = xmldoc.SelectSingleNode("//bridge/bridgeDetails/BridgeDomain").InnerText;
                    }
                    //ZD 100522 End
                    //C20 S4B Start
                    if (xmldoc.SelectSingleNode("//bridge/bridgeDetails/ConferenceDialinName").InnerText.Trim() != null)
                        txtDialinInfo.Text = xmldoc.SelectSingleNode("//bridge/bridgeDetails/ConferenceDialinName").InnerText;
                    //C20 S4B End

                    //ZD 100369_MCU Start
                    int EnablePollFailure = Int32.Parse(xmldoc.SelectSingleNode("//bridge/EnablePollFailure").InnerText);
                    int PollCount = Int32.Parse(xmldoc.SelectSingleNode("//bridge/PollCount").InnerText);
                    if (EnablePollFailure == 1)
                    {
                        chkFailover.Checked = true;
                        chkFailover.Enabled = true;
                    }
                    else
                    {
                        chkFailover.Checked = false;
                    }

                    if (chkFailover.Checked)
                    {
                        lblPollStatus.Visible = true;
                        lblPollStatus.InnerText = obj.GetTranslatedText("PASS");
                        if (PollCount == -2)
                        {
                            lblPollStatus.Style.Add("color", "#FF0000");
                            lblPollStatus.InnerText = obj.GetTranslatedText("FAIL");
                        }
                        if (PollCount == -1)
                            lblPollStatus.Style.Add("color", "#F5B800");

                        if (PollCount == 0)
                            lblPollStatus.Style.Add("color", "#008000");
                    }
                    else
                    {
                        lblPollStatus.Visible = false;
                    }
                    //ZD 100369_MCU End


                }
                else
                {
                    errLabel.Text = outXML;
                    errLabel.Visible = true;
                }
            }
            catch (Exception ex)
            {
                log.Trace(ex.StackTrace + " : " + ex.Message);
            }
        }
        // FB 460 Virtual Bridge Cards for Polycom
        protected void LoadMCUCards(String frm, XmlNodeList nodesMCUCards)
        {
            try
            {
                DataTable dt = new DataTable();
                String outXML = obj.GetMCUCards();
                if (outXML.IndexOf("<error>") < 0)
                {
                    XmlDocument xmldoc = new XmlDocument();
                    xmldoc.LoadXml(outXML);
                    XmlNodeList nodes = xmldoc.SelectNodes("//GetMCUCards/MCUCard");
                    XmlTextReader xtr;
                    DataSet ds = new DataSet();

                    foreach (XmlNode node in nodes)
                    {
                        xtr = new XmlTextReader(node.OuterXml, XmlNodeType.Element, new XmlParserContext(null, null, null, XmlSpace.None));
                        ds.ReadXml(xtr, XmlReadMode.InferSchema);
                    }

                    if (ds.Tables.Count > 0)
                    {
                        dt = ds.Tables[0];
                        dt.Columns.Add("MaximumCalls");
                    }
                }
                if (frm.Equals("1"))
                {
                    foreach (DataRow dr in dt.Rows)
                        dr["MaximumCalls"] = "0";
                }
                else
                {
                    //Response.Write("in else: " + nodesMCUCards.Count);
                    foreach (XmlNode node in nodesMCUCards)
                        foreach (DataRow dr in dt.Rows)
                        {
                            //Response.Write(dr["ID"].ToString() + " : " + node.SelectSingleNode("MaximumCalls").InnerText);
                            if (dr["ID"].ToString().Equals(node.SelectSingleNode("ID").InnerText))
                            {
                                //Response.Write("in if");
                                dr["MaximumCalls"] = node.SelectSingleNode("MaximumCalls").InnerText;
                            }
                        }
                }
                dgMCUCards.DataSource = dt;
                dgMCUCards.DataBind();
            }
            catch (Exception ex)
            {
                log.Trace(ex.StackTrace + " : " + ex.Message);
            }

        }
        protected void GetServices(XmlNodeList nodes, DataGrid dgTemp)
        {
            try
            {
                XmlTextReader xtr;
                DataSet ds = new DataSet();

                foreach (XmlNode node in nodes)
                {
                    xtr = new XmlTextReader(node.OuterXml, XmlNodeType.Element, new XmlParserContext(null, null, null, XmlSpace.None));
                    ds.ReadXml(xtr, XmlReadMode.InferSchema);
                }
                DataTable dt = new DataTable();

                if (ds.Tables.Count > 0)
                {
                    dt = ds.Tables[0];

                    if (!dt.Columns.Contains("RangeSortOrder"))
                        dt.Columns.Add("RangeSortOrder");
                    dgTemp.DataSource = dt;
                    dgTemp.DataBind();
                }
            }
            catch (Exception ex)
            {
                log.Trace(ex.StackTrace + " : " + ex.Message);
            }
        }
        protected void ChangeFirmwareVersion(Object sender, EventArgs e)
        {
            try
            {
                lstInterfaceType.SelectedIndex = lstMCUType.SelectedIndex;
                DataTable dt = new DataTable();
                dt.Columns.Add("ID");
                dt.Columns.Add("Name");
                lstFirmwareVersion.Items.Clear();
                DataRow dr;
                tr6.Visible = false;// FB 2008
                trEnableLPR.Visible = false;//FB 1907
                trProfileID.Visible = false; //FB 2427
                trEnableMessage.Visible = false;//FB 2486
                // FB 2441 Starts
                trEhancedMCU.Visible = false;
                trTemplate.Visible = false;
				// FB 2441 II Starts
                trDMAName.Visible = false;
                trDMAPassword.Visible = false;
                trDMAURL.Visible = false;
                trDMATemplate.Visible = false;
                trDMATestCon.Visible = false;
				trDMADialinprefix.Visible = false;;//FB 2689
				// FB 2441 II Ends
                trE164Services.Visible = false;
                trSynchronous.Visible = false;
                btn_DMATestConnection.Visible = false;
				trRPRMLogin.Visible = false;//FB 2709 
                trRPRMemail.Visible = false;//FB 2709
                // FB 2441 Ends
				//FB 2556
                trMultiTenant.Visible = false;
                iViewOrgDetails1.Visible = false;
                iViewOrgDetails2.Visible = false;
                chkSynchronous.Disabled = false;//FB 2556
                trURL.Visible = false;//ZD 101078
                trsyslocs.Visible = false; //ZD 101522
                //ALLDEV-854 Start
                trAlias.Visible = false;
                //ALLDEV-854 End
                //ZD 103550
                trAVPorts.Visible = true;
                trURLandPoll.Visible = true;
                trVirMCUandPublic.Visible = true;
                trCDR.Visible = true;
                tr9.Visible = true;
                trAlert.Visible = true;
                trAlertchks.Visible = true;
                tr7.Visible = false;
                tr8.Visible = false;
                tdLogin1.Visible = true;
                tdLogin2.Visible = true;
                tdFirm1.Visible = true;
                tdFirm2.Visible = true;
                trPwd.Visible = true;

                #region InterfaceType Cases

               
               
                
                switch (lstInterfaceType.SelectedItem.Text)
                {
                    case "1": //Polycom
                        trCodianMessage.Visible = false; //FB Case 698
                        if (lstMCUType.SelectedValue.Equals("8"))//Polycom RMX 2000
                        {
                            //dr = dt.NewRow();
                            //dr["ID"] = "3.x"; dr["Name"] = "3.x";//ZD 100736
                            //dt.Rows.Add(dr);
                            //ZD 103787 START
                            dr = dt.NewRow();
                            dr["ID"] = "4.x"; dr["Name"] = "4.x";
                            dt.Rows.Add(dr);
                            dr = dt.NewRow();
                            dr["ID"] = "5.x"; dr["Name"] = "5.x";
                            dt.Rows.Add(dr);
                            dr = dt.NewRow();
                            dr["ID"] = "6.x"; dr["Name"] = "6.x";
                            dt.Rows.Add(dr);
                            dr = dt.NewRow();
                            dr["ID"] = "7.x"; dr["Name"] = "7.x";
                            dt.Rows.Add(dr);
                            dr = dt.NewRow();
                            dr["ID"] = "8.x"; dr["Name"] = "8.x";
                            dt.Rows.Add(dr);
                            //ZD 103787 END
                            lblHeader1.Text = obj.GetTranslatedText("Polycom RMX Configuration");//FB 1830 - Translation
                            trMCUCards.Visible = false;
                            trEnableIVR.Visible = true; //FB 1766
                            trIVRName.Visible = true;
                            trEnableRecord.Visible = true;//FB 1907
                            trEnableLPR.Visible = true;//FB 1907
                            trProfileID.Visible = true; //FB 2427
                            trEnableMessage.Visible = true;//FB 2486
                            // FB 2441 Starts
                            trEhancedMCU.Visible = false;
                            trTemplate.Visible = false;
                            trSynchronous.Visible = false;
							// FB 2441 II Starts
                            trTemplate.Visible = false;
                            trDMAName.Visible = false;
                            trDMAPassword.Visible = false;
                            trDMAURL.Visible = false;
                            trDMATemplate.Visible = false;
                            trDMATestCon.Visible = false;
							trDMADialinprefix.Visible = false;
							// FB 2441 II Ends
                            // FB 2441 Ends
							trRPRMLogin.Visible = false;//FB 2709 
                            trRPRMemail.Visible = false;//FB 2709 
                            // FB 2441 Ends

                            //FB 2797 Start
                            if (enableCDR == 1)
                            {
                                spEnableCDR.Attributes.Add("style", "display:block");
                                chkEnableCDR.Attributes.Add("style", "display:block");
                            }
                            spCDREvent.Attributes.Add("style", "display:none");
                            divtxtCDR.Attributes.Add("style", "display:none");
                            if (chkEnableCDR.Checked && enableCDR == 1)
                            {
                                spCDREvent.Attributes.Add("style", "display:block");
                                divtxtCDR.Attributes.Add("style", "display:block");
                            }
                            //FB 2797 Ends

                        }
                        else if (lstMCUType.SelectedValue.Equals("10"))
                        {
                            dr = dt.NewRow();
                            dr["ID"] = "3.x"; dr["Name"] = "3.x";//ZD 100736
                            dt.Rows.Add(dr);
                            lblHeader1.Text = obj.GetTranslatedText("Polycom CMA Configuration");
                            trMCUCards.Visible = false;
                            trEnableIVR.Visible = false;
                            trIVRName.Visible = false;
                            trEnableRecord.Visible = false;
                            trEnableLPR.Visible = false;
                            trProfileID.Visible = false;
                            trEnableMessage.Visible = false;
                            // FB 2441 Starts
                            // FB 2441 II Starts
                            trEhancedMCU.Visible = false;
                            trTemplate.Visible = false;
                            trSynchronous.Visible = false;
                            trDMAName.Visible = false;
                            trDMAPassword.Visible = false;
                            trDMAURL.Visible = false;
                            trDMATemplate.Visible = false;
                            trDMATestCon.Visible = false;
							trDMADialinprefix.Visible = false;
							// FB 2441 II Starts
                            // FB 2441 Ends
							trRPRMLogin.Visible = true;//FB 2709 
                            trRPRMemail.Visible = true;//FB 2709 
                            //FB 2797 Start
                            if (enableCDR == 1)
                            {
                                spEnableCDR.Attributes.Add("style", "display:block");
                                chkEnableCDR.Attributes.Add("style", "display:block");
                            }
                            spCDREvent.Attributes.Add("style", "display:none");
                            divtxtCDR.Attributes.Add("style", "display:none");
                            if (chkEnableCDR.Checked && enableCDR == 1)
                            {
                                spCDREvent.Attributes.Add("style", "display:block");
                                divtxtCDR.Attributes.Add("style", "display:block");
                            }
                            //FB 2797 Ends
                        }
                        // FB 2441 Starts
                        else if (lstMCUType.SelectedValue.Equals("13"))
                        {
                            //ZD 101884 Strats
                            //dr = dt.NewRow();
                            //dr["ID"] = "Wave 7.x"; dr["Name"] = "Wave 7.x";
                            //dt.Rows.Add(dr);
                            dr = dt.NewRow();
                            dr["ID"] = "8.0.x"; dr["Name"] = "8.0.x";
                            dt.Rows.Add(dr);
                            dr = dt.NewRow();
                            dr["ID"] = "8.1.x"; dr["Name"] = "8.1.x";
                            dt.Rows.Add(dr);
                            //ZD 101884 End
                            dr = dt.NewRow();
                            dr["ID"] = "8.2.x"; dr["Name"] = "8.2.x"; //ZD 102205
                            dt.Rows.Add(dr);
                            //ZD 103467 Starts
                            dr = dt.NewRow();
                            dr["ID"] = "8.3.x"; dr["Name"] = "8.3.x";
                            dt.Rows.Add(dr);
                            //ZD 103467 Ends
                            //ZD ZD 104465 Starts
                            dr = dt.NewRow();
                            dr["ID"] = "8.4.x"; dr["Name"] = "8.4.x";
                            dt.Rows.Add(dr);
                            //ZD ZD 104465 Ends
                            //ALLDEV-825 Starts
                            dr = dt.NewRow();
                            dr["ID"] = "8.4.1.x"; dr["Name"] = "8.4.1.x";
                            dt.Rows.Add(dr);
                            //ALLDEV-825 Ends
                            //ALLDEV-820 START
                            dr = dt.NewRow();
                            dr["ID"] = "9.0.x"; dr["Name"] = "9.0.x";
                            dt.Rows.Add(dr);
                            //ALLDEV-820 END
                            lblHeader1.Text = obj.GetTranslatedText("Polycom RPRM Configuration");
                            txtPortP.Visible = true;
                            trEhancedMCU.Visible = true;
                            trTemplate.Visible = true;
                            trMCUCards.Visible = false;
                            trEnableIVR.Visible = false;
                            trIVRName.Visible = false;
                            trEnableRecord.Visible = false;
                            trSynchronous.Visible = true;
                            btn_DMATestConnection.Visible = true;
							// FB 2441 II Starts
                            trDMAName.Visible = true;
                            trDMAPassword.Visible = true;
                            trDMAURL.Visible = true;
                            trDMATemplate.Visible = true;
                            trDMATestCon.Visible = true;
							trDMADialinprefix.Visible = true;
							// FB 2441 II Ends
							trRPRMLogin.Visible = true;//FB 2709 
                            trRPRMemail.Visible = true;//FB 2709 
                        }
                        // FB 2441 Ends
                        //ZD 103787 START
                        else if (lstMCUType.SelectedValue.Equals("17"))// Polycom RMX 1000
                        {
                            dr = dt.NewRow();
                            dr["ID"] = "1.x"; dr["Name"] = "1.x";
                            dt.Rows.Add(dr);
                            dr = dt.NewRow();
                            dr["ID"] = "2.x"; dr["Name"] = "2.x";
                            dt.Rows.Add(dr);
                        }
                        else if (lstMCUType.SelectedValue.Equals("18"))//Polycom RMX 1500
                        {
                            dr = dt.NewRow();
                            dr["ID"] = "7.x"; dr["Name"] = "7.x";
                            dt.Rows.Add(dr);
                            dr = dt.NewRow();
                            dr["ID"] = "8.x"; dr["Name"] = "8.x";
                            dt.Rows.Add(dr);
                        }
                        else if (lstMCUType.SelectedValue.Equals("19"))//Polycom RMX 4000
                        {
                            dr = dt.NewRow();
                            dr["ID"] = "5.x"; dr["Name"] = "5.x";
                            dt.Rows.Add(dr);
                            dr = dt.NewRow();
                            dr["ID"] = "6.x"; dr["Name"] = "6.x";
                            dt.Rows.Add(dr);
                            dr = dt.NewRow();
                            dr["ID"] = "7.x"; dr["Name"] = "7.x";
                            dt.Rows.Add(dr);
                            dr = dt.NewRow();
                            dr["ID"] = "8.x"; dr["Name"] = "8.x";
                            dt.Rows.Add(dr);
                        }
                        //ZD 103787 END
                        else
                        {
                            dr = dt.NewRow();
                            dr["ID"] = "7.0"; dr["Name"] = "7.x/8.x";
                            dt.Rows.Add(dr);
                            lblHeader1.Text = obj.GetTranslatedText("Polycom MGC Configuration");//FB 1830 - Translation
                            trMCUCards.Visible = true;
                            trEnableIVR.Visible = true; //FB 1766
                            trIVRName.Visible = true;
                            trEnableRecord.Visible = true;//FB 1907                           
                        }
                        //ZD 103787 START
                        if (lstMCUType.SelectedValue.Equals("17") || lstMCUType.SelectedValue.Equals("18") || lstMCUType.SelectedValue.Equals("19"))
                        {
                            lblHeader1.Text = obj.GetTranslatedText("Polycom RMX Configuration");
                            trMCUCards.Visible = false;
                            trEnableIVR.Visible = true; 
                            trIVRName.Visible = true;
                            trEnableRecord.Visible = true;
                            trEnableLPR.Visible = true;
                            trProfileID.Visible = true;
                            trEnableMessage.Visible = true;
                            trEhancedMCU.Visible = false;
                            trTemplate.Visible = false;
                            trSynchronous.Visible = false;
                            trTemplate.Visible = false;
                            trDMAName.Visible = false;
                            trDMAPassword.Visible = false;
                            trDMAURL.Visible = false;
                            trDMATemplate.Visible = false;
                            trDMATestCon.Visible = false;
                            trDMADialinprefix.Visible = false;
                            trRPRMLogin.Visible = false; 
                            trRPRMemail.Visible = false;
                            if (enableCDR == 1)
                            {
                                spEnableCDR.Attributes.Add("style", "display:block");
                                chkEnableCDR.Attributes.Add("style", "display:block");
                            }
                            spCDREvent.Attributes.Add("style", "display:none");
                            divtxtCDR.Attributes.Add("style", "display:none");
                            if (chkEnableCDR.Checked && enableCDR == 1)
                            {
                                spCDREvent.Attributes.Add("style", "display:block");
                                divtxtCDR.Attributes.Add("style", "display:block");
                            }
                        }
                        //ZD 103787 END
                        tr1.Visible = true;
                        //FB 1937 - Start
                        tr2.Visible = true; 
                        tr5.Visible = true; 
                        chkExpandCollapse.Checked = true;
                        ClientScript.RegisterStartupScript(this.GetType(), "DefaultKey", "<script>ShowHideRow()</script>");
                        //FB 1937 - End
                        tr3.Visible = false;
                        tr4.Visible = false;
                        reqPortP.Enabled = true;
                        regPortP.Enabled = true;
                        trIPServices.Visible = true;
                        //TzTD1.Attributes.Add("style", "display:none");
                        //TzTD2.Attributes.Add("style", "display:none");
                        tdAudio.Visible = false;//FB 1937 //FB 2660
                        tdtxtAudio.Visible = false;//FB 2660
                        trConfServiceID.Visible = false; //FB 2016
                        trExtensionNumber.Visible = true;//FB 2610
                        //FB 2636 Starts
                        trE164Services.Visible = false; 
                        trISDN1.Visible = true;
                        trMPIServices.Visible = false;
                        //FB 2636 Ends
                        //FB 2797 Start
                        if (enableCDR == 1)
                        {
                            spEnableCDR.Attributes.Add("style", "display:block");
                            chkEnableCDR.Attributes.Add("style", "display:block");
                        }
                        spCDREvent.Attributes.Add("style", "display:none");
                        divtxtCDR.Attributes.Add("style", "display:none");
                        if (chkEnableCDR.Checked && enableCDR == 1)
                        {
                            spCDREvent.Attributes.Add("style", "display:block");
                            divtxtCDR.Attributes.Add("style", "display:block");
                        }
                        //FB 2797 Ends
                        break;
                    case "3": // Codian
                        trCodianMessage.Visible = true; //FB Case 698
                        dr = dt.NewRow();
                        dr["ID"] = "2.x"; dr["Name"] = "2.x";
                        dt.Rows.Add(dr);
                        lblHeader1.Text = obj.GetTranslatedText("Codian MCU Configuration");//FB 1830 - Translation
                        //FB 2636 Starts
                        tr5.Visible = false;
                        tr1.Visible = false;
                        tr2.Visible = true;
                        tr3.Visible = true;
                        tr4.Visible = false;
                        trISDN1.Visible = false;
                        trE164Services.Visible = true; 
                        // FB 2636 Ends
                        if (txtPortA.Text.Equals(""))
                            txtPortA.Text = txtPortP.Text;
                        reqPortP.Enabled = false;
                        regPortP.Enabled = false;
                        trIPServices.Visible = false;
                        trMPIServices.Visible = false;
                        
                        trMCUCards.Visible = false;
                        trEnableIVR.Visible = false; //FB 1766
                        trIVRName.Visible = false;
                        trEnableRecord.Visible = false;//FB 1907  
                        tdAudio.Visible = true;//FB 1937 //FB 2660
                        tdtxtAudio.Visible = true;//FB 2660
                        trConfServiceID.Visible = false;//FB 2016
                        trExtensionNumber.Visible = true;//FB 2610
                        
                        //FB 2797 Start
                        spEnableCDR.Attributes.Add("style", "display:none");
                        chkEnableCDR.Attributes.Add("style", "display:none");
                        spCDREvent.Attributes.Add("style", "display:none");
                        divtxtCDR.Attributes.Add("style", "display:none");
                        //FB 2797 End
                        break;
                    case "4": //tandberg
                        trCodianMessage.Visible = false; //FB Case 698
                        dr = dt.NewRow(); //fogbugz case 401
                        dr["ID"] = "J4.2"; dr["Name"] = "J4.2";
                        dt.Rows.Add(dr);
                        dr = dt.NewRow();
                        dr["ID"] = "J4.1"; dr["Name"] = "J4.1";
                        dt.Rows.Add(dr);
                        dr = dt.NewRow();
                        dr["ID"] = "J4.0"; dr["Name"] = "J4.0";
                        dt.Rows.Add(dr);
                        dr = dt.NewRow();
                        dr["ID"] = "D3.9"; dr["Name"] = "D3.9";
                        dt.Rows.Add(dr);
                        lblHeader1.Text = obj.GetTranslatedText("Tandberg MCU Configuration");//FB 1830 - Translation
                        if (txtPortT.Text.Equals(""))
                            txtPortT.Text = txtPortP.Text;
                        tr1.Visible = true;
                        //FB 1937 - Start
                        tr2.Visible = true;
                        tr5.Visible = true;
                        chkExpandCollapse.Checked = true;
                        ClientScript.RegisterStartupScript(this.GetType(), "DefaultKey", "<script>ShowHideRow()</script>");
                        //FB 1937 - End
                        tr3.Visible = false;
                        tr4.Visible = false;
                        reqPortP.Enabled = true;
                        regPortP.Enabled = true;
                        trIPServices.Visible = false;
                        trMPIServices.Visible = false;
                        trMCUCards.Visible = false;
                        trEnableIVR.Visible = false; //FB 1766
                        trIVRName.Visible = false;
                        trEnableRecord.Visible = false;//FB 1907
                        tdAudio.Visible = false;//FB 1937 //FB 2660
                        tdtxtAudio.Visible = false;//FB 2660
                        trConfServiceID.Visible = false; // FB 2016
                        trExtensionNumber.Visible = true;//FB 2610
                        trE164Services.Visible = false; //FB 2636
                        trISDN1.Visible = true; //FB 2636
                        //FB 2797 Start
                        if (enableCDR == 1)
                        {
                            spEnableCDR.Attributes.Add("style", "display:block");
                            chkEnableCDR.Attributes.Add("style", "display:block");
                        }
                        spCDREvent.Attributes.Add("style", "display:none");
                        divtxtCDR.Attributes.Add("style", "display:none");
                        if (chkEnableCDR.Checked && enableCDR == 1)
                        {
                            spCDREvent.Attributes.Add("style", "display:block");
                            divtxtCDR.Attributes.Add("style", "display:block");
                        }
                        //FB 2797 Ends
                        break;
                    case "5": //Radvision Scopia
                        trCodianMessage.Visible = false; //FB Case 698
                        dr = dt.NewRow(); //fogbugz case 401
                        dr["ID"] = "5.x"; dr["Name"] = "5.x";
                        dt.Rows.Add(dr);
                        lblHeader1.Text = obj.GetTranslatedText("Radvision MCU Configuration");//FB 1830 - Translation
                        if (txtPortT.Text.Equals(""))
                            txtPortT.Text = txtPortP.Text;
                        trConfServiceID.Visible = true; // FB 2016
                        //FB 1937 - Start
                        tr1.Visible = true;
                        tr2.Visible = true; 
                        tr5.Visible = true; 
                        chkExpandCollapse.Checked = true;
                        ClientScript.RegisterStartupScript(this.GetType(), "DefaultKey", "<script>ShowHideRow()</script>");
                        //FB 1937 - End

                        tr3.Visible = false;
                        tr4.Visible = false;
                        reqPortP.Enabled = true;
                        regPortP.Enabled = false;
                        trIPServices.Visible = true;
                        trMPIServices.Visible = false;
                        trMCUCards.Visible = false;
                        trEnableIVR.Visible = false; //FB 1766
                        trIVRName.Visible = false;
                        trEnableRecord.Visible = false; //FB 1907 
                        tdAudio.Visible = false;//FB 1937 //FB 2660
                        tdtxtAudio.Visible = false;//FB 2660   
                        trExtensionNumber.Visible = true;//FB 2610
                        trE164Services.Visible = false; //FB 2636
                        //FB 2797 Start
                        if (enableCDR == 1)
                        {
                            spEnableCDR.Attributes.Add("style", "display:block");
                            chkEnableCDR.Attributes.Add("style", "display:block");
                        }
                        spCDREvent.Attributes.Add("style", "display:none");
                        divtxtCDR.Attributes.Add("style", "display:none");
                        if (chkEnableCDR.Checked && enableCDR == 1)
                        {
                            spCDREvent.Attributes.Add("style", "display:block");
                            divtxtCDR.Attributes.Add("style", "display:block");
                        }
                        //FB 2797 Ends
                        break;
                    case "6": // FB 2008
                        trCodianMessage.Visible = true; //FB Case 698
                        dr = dt.NewRow();
                        dr["ID"] = "2.x"; dr["Name"] = "2.x";
                        dt.Rows.Add(dr);
                        //ZD 103231 start
                        dr = dt.NewRow();
                        dr["ID"] = "4.1"; dr["Name"] = "4.1";
                        dt.Rows.Add(dr);

                        dr = dt.NewRow();
                        dr["ID"] = "4.2"; dr["Name"] = "4.2";
                        dt.Rows.Add(dr);

                        dr = dt.NewRow();
                        dr["ID"] = "4.3"; dr["Name"] = "4.3";
                        dt.Rows.Add(dr);

                        dr = dt.NewRow();
                        dr["ID"] = "4.4"; dr["Name"] = "4.4";
                        dt.Rows.Add(dr);

                        dr = dt.NewRow();
                        dr["ID"] = "4.5"; dr["Name"] = "4.5";
                        dt.Rows.Add(dr);
                        //ZD 103231  End
                        lblHeader1.Text = "MSE 8000 MCU Configuration";
                        tr1.Visible = false;
                        tr2.Visible = true;//FB 1937
                        tr5.Visible = false;//FB 1937
                        tr3.Visible = true;
                        tr4.Visible = false;
                        if (txtPortA.Text.Equals(""))
                            txtPortA.Text = txtPortP.Text;
                        reqPortP.Enabled = false;
                        regPortP.Enabled = false;
                        trIPServices.Visible = false;
                        trMPIServices.Visible = false;
                        trMCUCards.Visible = false;
                        trEnableIVR.Visible = false; //FB 1766
                        trIVRName.Visible = false;
                        trEnableRecord.Visible = false;//FB 1907  
                        tdAudio.Visible = true;//FB 1937 //FB 2660
                        tdtxtAudio.Visible = true;//FB 2660
                        tr6.Visible = true;
                        trConfServiceID.Visible = false; //FB 2016
                        trExtensionNumber.Visible = true;//FB 2610
                        //FB 2636 Starts
                        trISDN1.Visible = false;
                        trE164Services.Visible = true;
                        //FB 2636 Ends
                        //FB 2797 Start
                        if (enableCDR == 1)
                        {
                            spEnableCDR.Attributes.Add("style", "display:block");
                            chkEnableCDR.Attributes.Add("style", "display:block");
                        }
                        spCDREvent.Attributes.Add("style", "display:none");
                        divtxtCDR.Attributes.Add("style", "display:none");
                        if (chkEnableCDR.Checked && enableCDR == 1)
                        {
                            spCDREvent.Attributes.Add("style", "display:block");
                            divtxtCDR.Attributes.Add("style", "display:block");
                        }
                        //FB 2797 Ends
                        break;
                    case "7": //Life size FB 2261
                        trCodianMessage.Visible = false; 
                        dr = dt.NewRow();
                        dr["ID"] = "2.x"; dr["Name"] = "2.x";
                        dt.Rows.Add(dr);
                        lblHeader1.Text = obj.GetTranslatedText("Lifesize MCU Configuration");
                        if (txtPortT.Text.Equals(""))
                            txtPortT.Text = txtPortP.Text;
                        tr1.Visible = true;
                        tr2.Visible = false;
                        tr5.Visible = false;
                        tr3.Visible = false;
                        tr4.Visible = false;

                        reqPortP.Enabled = true;
                        regPortP.Enabled = true;
                        trIPServices.Visible = false;
                        trMPIServices.Visible = false;
                        trMCUCards.Visible = false;
                        trEnableIVR.Visible = false; 
                        trIVRName.Visible = false;
                        trEnableRecord.Visible = false;
                        tdAudio.Visible = false;//FB 1937 //FB 2660
                        tdtxtAudio.Visible = false;//FB 2660
                        trConfServiceID.Visible = false;
                        trExtensionNumber.Visible = true;//FB 2610
                        //FB 2797 Start
                        if (enableCDR == 1)
                        {
                            spEnableCDR.Attributes.Add("style", "display:block");
                            chkEnableCDR.Attributes.Add("style", "display:block");
                        }
                        spCDREvent.Attributes.Add("style", "display:none");
                        divtxtCDR.Attributes.Add("style", "display:none");
                        if (chkEnableCDR.Checked && enableCDR == 1)
                        {
                            spCDREvent.Attributes.Add("style", "display:block");
                            divtxtCDR.Attributes.Add("style", "display:block");
                        }
                        //FB 2797 Ends
                        break;
                    case "8": // FB 2510 Call Monitoring Cisco 8710
						//FB 2718 Starts
                        switch(lstMCUType.SelectedValue)
                        {
                            case "12":
                                trCodianMessage.Visible = true; 
                                //dr = dt.NewRow();
                                //dr["ID"] = "2.x"; dr["Name"] = "2.x";
                                //dt.Rows.Add(dr);
                                //ZD 103231 start
                                dr = dt.NewRow();
                                dr["ID"] = "2.1"; dr["Name"] = "2.1";
                                dt.Rows.Add(dr);

                                dr = dt.NewRow();
                                dr["ID"] = "2.2"; dr["Name"] = "2.2";
                                dt.Rows.Add(dr);

                                dr = dt.NewRow();
                                dr["ID"] = "2.3"; dr["Name"] = "2.3";
                                dt.Rows.Add(dr);

                                dr = dt.NewRow();
                                dr["ID"] = "3.0"; dr["Name"] = "3.0";
                                dt.Rows.Add(dr);

                                dr = dt.NewRow();
                                dr["ID"] = "3.1"; dr["Name"] = "3.1";
                                dt.Rows.Add(dr);

                                dr = dt.NewRow();
                                dr["ID"] = "4.0"; dr["Name"] = "4.0";
                                dt.Rows.Add(dr);

                                dr = dt.NewRow();
                                dr["ID"] = "4.1"; dr["Name"] = "4.1";
                                dt.Rows.Add(dr);
                                //ZD 103231 End
                                lblHeader1.Text = "CISCO 8710 MCU Configuration";
                                tr1.Visible = false;
                                //FB 2636 Starts
                                tr2.Visible = true;
                                trE164Services.Visible = true;
                                //FB 2636 Ends
                                tr5.Visible = false;
                                tr3.Visible = true;
                                tr4.Visible = false;
                                tr5.Visible = false;
                                if (txtPortA.Text.Equals(""))
                                    txtPortA.Text = txtPortP.Text;
                                reqPortP.Enabled = false;
                                regPortP.Enabled = false;
                                trIPServices.Visible = false;
                                trISDN.Visible = false;
                                trMPIServices.Visible = false;
                                trMCUCards.Visible = false;
                                trEnableIVR.Visible = false;
                                trIVRName.Visible = false;
                                trEnableRecord.Visible = false;
                                tdAudio.Visible = true;//FB 1937 //FB 2660
                                tdtxtAudio.Visible = true;//FB 2660
                                tr6.Visible = true;
                                trConfServiceID.Visible = false;
                                trExtensionNumber.Visible = true;//FB 2610
                                trISDN1.Visible = false; //FB 2635
                                //FB 2797 Start
                                if (enableCDR == 1)
                                {
                                    spEnableCDR.Attributes.Add("style", "display:block");
                                    chkEnableCDR.Attributes.Add("style", "display:block");
                                }
                                spCDREvent.Attributes.Add("style", "display:none");
                                divtxtCDR.Attributes.Add("style", "display:none");
                                if (chkEnableCDR.Checked && enableCDR == 1)
                                {
                                    spCDREvent.Attributes.Add("style", "display:block");
                                    divtxtCDR.Attributes.Add("style", "display:block");
                                }
                                //FB 2797 Ends
                                break;
                            case "15":
                                trCodianMessage.Visible = true;
                                dr = dt.NewRow();
                                dr["ID"] = "2.x"; dr["Name"] = "2.x";
                                dt.Rows.Add(dr);
                                lblHeader1.Text = obj.GetTranslatedText("CISCO TMS Configuration");
                                tr1.Visible = true;
                                trSynchronous.Visible = true;
                                tdSynchronous.Visible = true;
                                tdSendMail.Visible = false;
                                chkSendmail.Visible = false;
                                tr2.Visible = true;
                                trE164Services.Visible = true;
                                tr5.Visible = false;
                                tr3.Visible = false;
                                tr4.Visible = false;
                                tr5.Visible = false;
                                if (txtPortA.Text.Equals(""))
                                    txtPortA.Text = txtPortP.Text;
                                reqPortP.Enabled = true;
                                regPortP.Enabled = true;
                                trIPServices.Visible = false;
                                trISDN.Visible = false;
                                trMPIServices.Visible = false;
                                trMCUCards.Visible = false;
                                trEnableIVR.Visible = false;
                                trIVRName.Visible = false;
                                trEnableRecord.Visible = false;
                                tdAudio.Visible = true;
                                tdtxtAudio.Visible = true;
                                tr6.Visible = true;
                                trConfServiceID.Visible = false;
                                trExtensionNumber.Visible = true;
                                trISDN1.Visible = false; 
                                if (enableCDR == 1)
                                {
                                    spEnableCDR.Attributes.Add("style", "display:block");
                                    chkEnableCDR.Attributes.Add("style", "display:block");
                                }
                                spCDREvent.Attributes.Add("style", "display:none");
                                divtxtCDR.Attributes.Add("style", "display:none");
                                if (chkEnableCDR.Checked && enableCDR == 1)
                                {
                                    spCDREvent.Attributes.Add("style", "display:block");
                                    divtxtCDR.Attributes.Add("style", "display:block");
                                }
                                trTemplate.Visible = true;
                                trURL.Visible = true;//ZD 101078
                                break;
                               
                        }
						//FB 2718 Ends
                        break;
                    //FB 2556 - Starts
                    case "9":
                        trCodianMessage.Visible = false;
                        dr = dt.NewRow();
                        dr["ID"] = "7.x"; dr["Name"] = "7.x";//ZD 100736
                        dt.Rows.Add(dr);
                        dr = dt.NewRow();
                        dr["ID"] = "8.x"; dr["Name"] = "8.x";//ZD 100736
                        dt.Rows.Add(dr);
                        lblHeader1.Text = obj.GetTranslatedText("iView MCU Configuration");
                        if (txtPortT.Text.Equals(""))
                            txtPortT.Text = txtPortP.Text;
                        tr1.Visible = true;
                        tr2.Visible = false;
                        tr5.Visible = false;
                        tr3.Visible = false;
                        tr4.Visible = false;

                        reqPortP.Enabled = true;
                        regPortP.Enabled = true;
                        trIPServices.Visible = false;
                        trMPIServices.Visible = false;
                        trMCUCards.Visible = false;
                        trEnableIVR.Visible = false;
                        trIVRName.Visible = false;
                        trEnableRecord.Visible = false;
                        tdAudio.Visible = false;
                        tdtxtAudio.Visible = false;
                        trConfServiceID.Visible = false;
                        trExtensionNumber.Visible = true;
                        trMultiTenant.Visible = true;
                        iViewOrgDetails1.Visible = true;
                        iViewOrgDetails2.Visible = true;
                        trExtensionNumber.Visible = false;
                        trAlert.Visible = false;
                        trAlertchks.Visible = false;
                        trSynchronous.Visible = true;
                        tdSynchronous.Visible = true;
                        tdSendMail.Visible = false;
                        chkSendmail.Visible = false;
                        chkSynchronous.Visible = true;
						if (enableCloudInstallation == 1)
                        {
                            chkSynchronous.Checked = true;
                            chkSynchronous.Disabled = true;
                           
                        }
                        if (Session["organizationID"].ToString() != "11" && enableCloudInstallation == 1)
                        {
                            txtPortT.Enabled = false;
                            txtPortP.Enabled = false;
                            txtMCUName.Text = txtMCUName.Text.Replace("(*)","");
                            txtMCUName.Enabled = false;
                            txtMCULogin.Enabled = false;
							//FB 2659 Start
                            txtPassword1.Enabled = false;
                            txtPassword2.Enabled = false;
                            txtapiportno.Enabled = false;
                            txtMaxAudioCalls.Enabled = false;
                            txtMaxVideoCalls.Enabled = false;
							//FB 2659 End
                            lstTimezone.Enabled = false;
                            lstStatus.Enabled = false;
                            drpURLAccess.Enabled = false; //ZD 100113
                            lstMCUType.Enabled = false;
                            txtapiportno.Enabled = false;
							lstFirmwareVersion.Enabled = false;//FB 2659
                            chkIsVirtual.Enabled = false;
                            chkbxIsPublic.Enabled = false;
                            chkSetFavourite.Enabled = false;
                            txtMaxAudioCalls.Visible = false;
                            imgAdminsearch.Visible = false;
							imgApprover1.Visible = false;
                            imgApprover2.Visible = false;
                            imgApprover3.Visible = false;
                            imgDelApprover1.Visible = false;
                            imgDelApprover2.Visible = false;
                            imgDelApprover3.Visible = false;
                            txtMaxVideoCalls.Visible = false;
                            chkMultiTenant.Enabled = false;
                            chkSynchronous.Disabled = true;
                        }
                        //FB 2797 Start
                        if (enableCDR == 1)
                        {
                            spEnableCDR.Attributes.Add("style", "display:block");
                            chkEnableCDR.Attributes.Add("style", "display:block");
                        }
                        spCDREvent.Attributes.Add("style", "display:none");
                        divtxtCDR.Attributes.Add("style", "display:none");
                        if (chkEnableCDR.Checked && enableCDR == 1)
                        {
                            spCDREvent.Attributes.Add("style", "display:block");
                            divtxtCDR.Attributes.Add("style", "display:block");
                        }
                        //FB 2797 Ends
                        break;
                    //FB 2556 - End
                    case "10": // Pexip //ZD 101217 Start 101522
                        trCodianMessage.Visible = true; //FB Case 698
                        dr = dt.NewRow();
                        dr["ID"] = "5.x"; dr["Name"] = "5.x";//ZD 10128
                        dt.Rows.Add(dr);
                        //103087 start
                        dr = dt.NewRow();
                        dr["ID"] = "6.x"; dr["Name"] = "6.x";
                        dt.Rows.Add(dr);
                        dr = dt.NewRow();
                        dr["ID"] = "7.x"; dr["Name"] = "7.x";
                        dt.Rows.Add(dr);
                        dr = dt.NewRow();
                        dr["ID"] = "8.x"; dr["Name"] = "8.x";
                        dt.Rows.Add(dr);
                        //ZD 103087 End
                        //ZD 104134 Support of v9 & v10 of Pexip Software Start
                        dr = dt.NewRow();
                        dr["ID"] = "9.x"; dr["Name"] = "9.x";
                        dt.Rows.Add(dr);
                        dr = dt.NewRow();
                        dr["ID"] = "10.x"; dr["Name"] = "10.x";
                        dt.Rows.Add(dr);
                        dr = dt.NewRow(); //ALLDEV-710 start
                        dr["ID"] = "11.x"; dr["Name"] = "11.x";
                        dt.Rows.Add(dr); //ALLDEV-710 End
                        //ZD 104134 End
                        //Begin EN-9238 - ZD 105457 : KD
                        dr = dt.NewRow();
                        dr["ID"] = "12.x"; dr["Name"] = "12.x";
                        dt.Rows.Add(dr);
                        dr = dt.NewRow();
                        dr["ID"] = "13.x"; dr["Name"] = "13.x";
                        dt.Rows.Add(dr);
                        //End EN-9238 - ZD 105457 : KD
                        lblHeader1.Text = obj.GetTranslatedText("Pexip MCU Configuration");//FB 1830 - Translation //ZD 102032
                        //FB 2636 Starts
                        tr5.Visible = false;
                        tr1.Visible = false;
                        tr2.Visible = true;
                        tr3.Visible = true;
                        tr4.Visible = false;
                        trISDN1.Visible = false;
                        trE164Services.Visible = true;
                        // FB 2636 Ends
                        if (txtPortA.Text.Equals(""))
                            txtPortA.Text = txtPortP.Text;
                        reqPortP.Enabled = false;
                        regPortP.Enabled = false;
                        trIPServices.Visible = false;
                        trMPIServices.Visible = false;

                        trMCUCards.Visible = false;
                        trEnableIVR.Visible = false; //FB 1766
                        trIVRName.Visible = false;
                        trEnableRecord.Visible = false;//FB 1907  
                        tdAudio.Visible = true;//FB 1937 //FB 2660
                        tdtxtAudio.Visible = true;//FB 2660
                        trConfServiceID.Visible = false;//FB 2016
                        trExtensionNumber.Visible = true;//FB 2610
                        trsyslocs.Visible = true;//ZD 101522
                        //ALLDEV-854 Start
                        trAlias.Visible = true;
                        //ALLDEV-854 End
                        //FB 2797, ALLDEV-773 Start
                        spEnableCDR.Attributes.Add("style", "display:block");
                        chkEnableCDR.Attributes.Add("style", "display:block");
                        spCDREvent.Attributes.Add("style", "display:block");
                        divtxtCDR.Attributes.Add("style", "display:block");
                        //FB 2797, ALLDEV-773 End
                        break;
                    //ZD 101217 End
                    case "11": //ZD 103550
                        tr7.Visible = true;
                        tr8.Visible = true;
                        
                        tdLogin1.Visible = false;
                        tdLogin2.Visible = false;
                        tdFirm1.Visible = false;
                        tdFirm2.Visible = false;
                        trPwd.Visible = false;
                        tr9.Visible = false;
                        trAVPorts.Visible = false;
                        trURLandPoll.Visible = false;
                        trVirMCUandPublic.Visible = false;
                        trCDR.Visible = false;
                        trCodianMessage.Visible = false;
                        tr5.Visible = false;
                        tr1.Visible = false;
                        tr2.Visible = false;
                        tr3.Visible = false;
                        tr4.Visible = false;
                        trISDN1.Visible = false;
                        trE164Services.Visible = false;
                        if (txtPortA.Text.Equals(""))
                            txtPortA.Text = txtPortP.Text;
                        reqPortP.Enabled = false;
                        regPortP.Enabled = false;
                        trIPServices.Visible = false;
                        trMPIServices.Visible = false;
                        trMCUCards.Visible = false;
                        trEnableIVR.Visible = false;
                        trIVRName.Visible = false;
                        trEnableRecord.Visible = false;
                        tdAudio.Visible = false;
                        tdtxtAudio.Visible = false;
                        trConfServiceID.Visible = false;
                        trExtensionNumber.Visible = false;
                        trsyslocs.Visible = false;
                        trAlertchks.Visible = false;
                        trAlert.Visible = false;

                        spEnableCDR.Attributes.Add("style", "display:none");
                        chkEnableCDR.Attributes.Add("style", "display:none");
                        spCDREvent.Attributes.Add("style", "display:none");
                        divtxtCDR.Attributes.Add("style", "display:none");
                        break;
                    default:
                        dr = dt.NewRow();
                        dr["ID"] = "-1"; dr["Name"] = "None";
                        dt.Rows.Add(dr);
                        reqPortP.Enabled = false;
                        regPortP.Enabled = false;
                        trMCUCards.Visible = false;
                        trEnableIVR.Visible = false; //FB 1766
                        trIVRName.Visible = false;
                        trEnableRecord.Visible = false;  //FB 1907 
                        tdAudio.Visible = false;//FB 1937 //FB 2660
                        tdtxtAudio.Visible = false;//FB 2660
                        trConfServiceID.Visible = false; // FB 2016
                        trExtensionNumber.Visible = false;//FB 2610
                        break;
                }

                #endregion
                //FB 2486 - Start
                if (Session["McuEnchancedLimit"] != null)
                {
                    String McuEnhanceLimit = Session["McuEnchancedLimit"].ToString();
                    int McuCount = 0;
                    Int32.TryParse(hdnMcuCount.Value.ToString(), out McuCount);
                    if (Int32.Parse(McuEnhanceLimit) > McuCount)
                    {
                        trEnableMessage.Disabled = false;
                        // FB 2636 Starts
                        chkE164.Visible = true; 
                        lblE164.Visible = true; 
                        // FB 2636 Ends
                        ChkEhancedMCU.Enabled = true;//FB 2441
						chkFailover.Enabled = true;//FB 100369_MCU
                        //ZD 100263 Starts
                        trEnableMessage.Visible = true;
                        chkMessage.Visible = true;
                        ChkEhancedMCU.Visible = true;
                        chkFailover.Visible = true;
                        lblFailover.Visible = true;
                        //ZD 100263 Ends
                    }
                    else
                    {
                        trEnableMessage.Disabled = true;
                        chkMessage.Checked = false;
                        chkMessage.Enabled = false;
                        // FB 2636 Starts
                        chkE164.Checked = false; 
                        chkE164.Enabled = false; 
                        // FB 2636 Ends
                        ChkEhancedMCU.Enabled = false;//FB 2441
                        ChkEhancedMCU.Checked = false;//FB 2441
						chkFailover.Enabled = false;//FB 100369_MCU
                        chkFailover.Checked = false;//FB 100369_MCU
                        //ZD 100263 Starts
                        trEnableMessage.Visible = false;
                        chkMessage.Visible = false;
                        chkE164.Visible = false;
                        lblE164.Visible = false; 
                        ChkEhancedMCU.Visible = false;
                        trEhancedMCU.Visible = false;
                        chkFailover.Visible = false;
                        lblFailover.Visible = false;
                        //ZD 100263 Ends
                        chkIsVirtual.Checked = false; //ZD 100040
                        chkIsVirtual.Enabled = false;//ZD 100040
                    }
                    //FB 2636 Starts
                    if (chkE164.Enabled)
                    {
                        chkH323.Enabled = true;
                    }
                    else
                    {
                        chkH323.Checked = false;
                        chkH323.Enabled = false;
                        lblEH323.Visible = false; //ZD 100263
                        chkH323.Visible = false; //ZD 100263

                    }
                    //FB 2636 Ends
                }
                //FB 2486 - End
                lstFirmwareVersion.DataSource = dt;
                lstFirmwareVersion.DataBind();
            }
            catch (Exception ex)
            {
                log.Trace(ex.StackTrace + " : " + ex.Message);
            }
        }
        protected void LoadList(DropDownList lstTemp, XmlNodeList nodes)
        {
            try
            {
                XmlTextReader xtr;
                DataSet ds = new DataSet();

                foreach (XmlNode node in nodes)
                {
                    xtr = new XmlTextReader(node.OuterXml, XmlNodeType.Element, new XmlParserContext(null, null, null, XmlSpace.None));
                    ds.ReadXml(xtr, XmlReadMode.InferSchema);
                }
                DataTable dt = new DataTable();

                if (ds.Tables.Count > 0)
                    dt = ds.Tables[0];
                if (dt.Columns.Contains("name"))//FB 2272
                        foreach (DataRow dr in dt.Rows)
                            dr["name"] = obj.GetTranslatedText(dr["name"].ToString());
                if (dt.Columns.Contains("name"))
                    dt.DefaultView.Sort = "name ASC";//ZD 103787
                lstTemp.DataSource = dt;
                lstTemp.DataBind();
            }
            catch (Exception ex)
            {
                log.Trace(ex.StackTrace + " : " + ex.Message);
            }
        }

        #endregion
        protected void SubmitMCU(Object sender, EventArgs e)
        {
            try
            {
                if (Page.IsValid)
                {
                    // FB 2441 Starts
                    string ButtonID = "";
                    Button btn_sender = (Button)sender;
                    ButtonID = btn_sender.ID;
                    // FB 2441 Ends
                    //ZD 104021 Start
                    if (lstInterfaceType.SelectedItem.Text == "11" && Session["EnableBlueJeans"].ToString().Equals("0"))
                    {
                        errLabel.Text = obj.GetTranslatedText("Blue Jeans License does not permit this operation");
                        errLabel.Visible = true;
                        return;
                    }
                    //ZD 104021 End
                    //ALLDEV-854 Starts
                    if (DrpDwnAlias.SelectedValue == "1")
                    {
                        trAlias.Attributes.Add("Style", "visibility:visible;");
                        if (txtAlias3.Text == "{5}" || txtAlias2.Text == "{5}")
                        {
                            errLabel.Text = obj.GetTranslatedText("Alias cannot conatin {5} placeholder alone.");
                            errLabel.Visible = true;
                            return;
                        }
                    }
                    //ALLDEV-854 Ends
                    String inXML = GenerateInXML(ButtonID);
                    //                Response.Write(obj.Transfer(inXML));
                    //                Response.End();
                    String outXML = "";
                    hdnPasschange.Value = "false"; //FB 3054
                    if (!inXML.Equals(""))
                    {
                        log.Trace("Setbridge: " + inXML);
                        outXML = obj.CallMyVRMServer("SetBridge", inXML, Application["MyVRMServer_ConfigPath"].ToString());
                       // outXML = obj.CallCOM("SetBridge", inXML, Application["COM_ConfigPath"].ToString());
                        if (outXML.IndexOf("<error>") < 0)
						{
                            //FB 2709
                            if (lstMCUType.SelectedValue.Equals("13"))
                            {
                                XmlDocument xmlDoc = new XmlDocument();
                                xmlDoc.LoadXml(outXML);
                                XmlNode nodes = xmlDoc.SelectSingleNode("//SetBridge/MCUID");
                                string mcuid = nodes.InnerText.Trim();
                                int RPRMconcurrentcount = 0;
                                Int32.TryParse(txtCallsCount.Text, out RPRMconcurrentcount);
                                Int32.TryParse(hdnRPRMCallCount.Value, out Existingcallscount);
                                if (txtRPRMLogin.Text.ToLower().Trim() != hdnRPRMUserid.Value || RPRMconcurrentcount != Existingcallscount)
                                {
                                    if (txtRPRMLogin.Text.ToLower().Trim() == hdnRPRMUserid.Value && RPRMconcurrentcount < Existingcallscount)
                                    {
                                        //NO need to do any action
                                    }
                                    else
                                    {
                                        string RPRMINXML = "<CreateUser><UserID>" + Session["userID"].ToString() + "</UserID><mcuId>" + mcuid + "</mcuId><Existingcallscount>" + Existingcallscount + "</Existingcallscount></CreateUser>";
                                        outXML = obj.CallCOM2("CreateUserOnMcu", RPRMINXML, Application["RTC_ConfigPath"].ToString());
                                        if (outXML.IndexOf("<error>") < 0)
                                        {
                                            Response.Redirect("ManageBridge.aspx?m=1", false);
                                        }
                                        else
                                        {
                                            errLabel.Text = obj.ShowErrorMessage(outXML);
                                            errLabel.Visible = true;
                                        }
                                    }
                                }
                            }
                            Response.Redirect("ManageBridge.aspx?m=1", false);

                            //FB 2709
                        }
                        else
                        {
                            errLabel.Text = obj.ShowErrorMessage(outXML);
                            
                            if (errLabel.Text.IndexOf("MCU as Private") > 0) //FB 1920
                                chkbxIsPublic.Checked = true;

                            errLabel.Visible = true;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                log.Trace(ex.StackTrace + " : " + ex.Message);
            }
        }

        protected String GenerateInXML(string btn_sender)
        {
            String errorAddress = "";
            int iVideoPorts = 0;// ZD 104605
            int iVideoTotalPorts = 0;// ZD 104605
            try
            {
                DataGridItem E164dgi = null; //FB 2636
                StringBuilder inXML = new StringBuilder(); // Fb 2636

                int confserID = 0, PoolOrderID = 0; //FB 2016 //ZD 104256
                inXML.Append("<setBridge>");
                inXML.Append(obj.OrgXMLElement());//Organization Module Fixes
                inXML.Append("<userID>" + Session["userID"].ToString() + "</userID>");

                inXML.Append("<bridge>");
                inXML.Append("<bridgeID>" + txtMCUID.Text + "</bridgeID>");
                inXML.Append("<name>" + txtMCUName.Text + "</name>");
                inXML.Append("<login>" + txtMCULogin.Text + "</login>");
                //FB 3054 Starts
                if (confPassword.Value != "false" && hdnPasschange.Value == "true") //FB 3054
                    txtPassword1.Text = confPassword.Value;
                confPassword.Value = "";
                if (Session["MCUPW"] == null)
                    Session["MCUPW"] = "";
                if (hdnPasschange.Value != "" && hdnPasschange.Value == "true") //FB 3054
                    inXML.Append("<password>" + UserPassword(txtPassword1.Text) + "</password>");
                else
                    inXML.Append("<password>" + Session["MCUPW"].ToString() + "</password>"); //FB 3054
                //FB 3054 Ends
                inXML.Append("<timeZone>" + lstTimezone.SelectedValue + "</timeZone>");
                //inXML.Append("<maxAudioCalls>" + txtMaxAudioCalls.Text + "</maxAudioCalls>"); // ZD 104605
                // inXML.Append("<maxVideoCalls>" + txtMaxVideoCalls.Text + "</maxVideoCalls>"); // ZD 104605
                inXML.Append("<bridgeType>" + lstMCUType.SelectedValue + "</bridgeType>");
                inXML.Append("<bridgeStatus>" + lstStatus.SelectedValue + "</bridgeStatus>");
                inXML.Append("<URLAccess>" + drpURLAccess.SelectedValue + "</URLAccess>");//ZD 100113
                //ZD 100369_MCU Start
                if(!CheckUserEmail(txtMultipleAssistant.Text, ref errorAddress))
                    if (!string.IsNullOrEmpty(errorAddress)) //104480 
                    {
                        throw new Exception(obj.GetTranslatedText("Please enter a valid email address") + ": " + errorAddress);
                    }

                inXML.Append("<MultipleAssistant>" + txtMultipleAssistant.Text + "</MultipleAssistant>");
                if (chkFailover.Checked)
                    inXML.Append("<EnablePollFailure>1</EnablePollFailure>");
                else
                    inXML.Append("<EnablePollFailure>0</EnablePollFailure>");
                //ZD 100369_MCU End

                if (chkIsVirtual.Checked)
                    inXML.Append("<virtualBridge>1</virtualBridge>");
                else
                    inXML.Append("<virtualBridge>0</virtualBridge>");
                // FB 1920 Starts
                if(chkbxIsPublic.Checked)
                    inXML.Append("<isPublic>1</isPublic>");
                else
                    inXML.Append("<isPublic>0</isPublic>");
                // FB 1920 Ends

                // FB 2501 Starts
                if (chkSetFavourite.Checked)
                    inXML.Append("<setFavourite>1</setFavourite>");
                else
                    inXML.Append("<setFavourite>0</setFavourite>");
                // FB 2501 Ends

                //FB 2797 Start
                if (lstInterfaceType.SelectedItem.Text == "3")
                    chkEnableCDR.Checked = false;
                //FB 2797 End

                // FB 2660 Starts
                if (chkEnableCDR.Checked)
                {
                    inXML.Append("<enableCDR>1</enableCDR>");
                    inXML.Append("<deleteCDRDays>" + txtdltCDR.Text + "</deleteCDRDays>");//FB 2714
                }
                else
                {
                    inXML.Append("<enableCDR>0</enableCDR>");
                    inXML.Append("<deleteCDRDays>1</deleteCDRDays>");//FB 2714-DefaultValue is 1.
                }
                // FB 2660 Ends

                inXML.Append("<bridgeAdmin>");
                inXML.Append("<ID>" + hdnApprover4.Text + "</ID>");
                inXML.Append("</bridgeAdmin>");
                inXML.Append("<firmwareVersion>" + lstFirmwareVersion.SelectedValue + "</firmwareVersion>");
                inXML.Append("<percentReservedPort>20</percentReservedPort>");
                inXML.Append("<approvers>");
                inXML.Append("<approver>");
                inXML.Append("<ID>" + hdnApprover1.Text + "</ID>");
                inXML.Append("</approver>");
                inXML.Append("<approver>");
                inXML.Append("<ID>" + hdnApprover2.Text + "</ID>");
                inXML.Append("</approver>");
                inXML.Append("<approver>");
                inXML.Append("<ID>" + hdnApprover3.Text + "</ID>");
                inXML.Append("</approver>");
                inXML.Append("</approvers>");

                iVideoTotalPorts = 0; // ZD 104605
                //ZD 100040 - start     
                inXML.Append("<videoPorts>");
                foreach (DataGridItem dgi in dgVideoPort.Items)
                {
                    inXML.Append("<port>");
                    inXML.Append("<resolutionId>" + ((Label)dgi.FindControl("lblResolutionID")).Text + "</resolutionId>");
                    inXML.Append("<qty>" + ((TextBox)dgi.FindControl("txtVideoPort")).Text + "</qty>");
                    iVideoPorts = 0; // ZD 104605
                    Int32.TryParse(((TextBox)dgi.FindControl("txtVideoPort")).Text, out iVideoPorts);// ZD 104605
                    iVideoTotalPorts += iVideoPorts;// ZD 104605
                    inXML.Append("</port>");
                }
                inXML.Append("</videoPorts>");
                //ZD 100040 - End
                inXML.Append("<maxAudioCalls>" + iVideoTotalPorts.ToString() + "</maxAudioCalls>"); // ZD 104605
                inXML.Append("<maxVideoCalls>" + iVideoTotalPorts.ToString() + "</maxVideoCalls>"); // ZD 104605
                if(txtapiportno.Text != "")
                inXML.Append("<ApiPortNo>"+txtapiportno.Text+"</ApiPortNo>");//API Port...
                else
                    inXML.Append("<ApiPortNo>" + "80" + "</ApiPortNo>");//API Port...
                inXML.Append("<bridgeDetails>");
                switch (lstInterfaceType.SelectedItem.Text)
                {
                    case "1":
                        inXML.Append("<controlPortIPAddress>" + txtPortP.Text + "</controlPortIPAddress>");

                        //ZD 103787 - It is for RMX MCU
                        if (lstMCUType.SelectedValue.Equals("8") || lstMCUType.SelectedValue.Equals("17") || lstMCUType.SelectedValue.Equals("18") || lstMCUType.SelectedValue.Equals("19")) 
                        {
                            Int32.TryParse(txtProfileID.Text, out confserID); //FB 2427
                            inXML.Append("<ConferenceServiceID>" + confserID + "</ConferenceServiceID>");

                            //FB 2591 Starts
                            if (confserID > 0) 
                            {
                                inXML.Append("<Profile>");
                                inXML.Append("<Id>" + confserID + "</Id>");
                                inXML.Append("<Name>" + txtProfileID.SelectedItem + "</Name>");
                                inXML.Append("</Profile>");
                            }
                            //FB 2591 Ends
                        }
                        //FB 2441 II Starts
                        if (lstMCUType.SelectedValue.Equals("13"))
                        {
                            Int32.TryParse(lstTemplate.SelectedValue, out confserID);
                            inXML.Append("<ConferenceServiceID>" + confserID + "</ConferenceServiceID>");

                            if (confserID > 0)
                            {
                                inXML.Append("<Profile>");
                                inXML.Append("<Id>" + confserID + "</Id>");
                                inXML.Append("<Name>" + lstTemplate.SelectedItem + "</Name>");
                                inXML.Append("</Profile>");
                            }
                            
                            //ZD 104256 Starts
                            int.TryParse(drpPoolOrder.SelectedValue, out PoolOrderID);
                            inXML.Append("<ConferencePoolOrderID>" + PoolOrderID + "</ConferencePoolOrderID>");                            
                            //ZD 104256 Ends
                        }
                        //FB 2441 II End
                        
                        //FB 1766 - Start
                        String chkTem = "0";
                        if (chkEnableIVR.Checked) chkTem = "1";
                        inXML.Append("<EnableIVR>" + chkTem + "</EnableIVR>");
                        inXML.Append("<IVRServiceName>" + txtIVRName.Text + "</IVRServiceName>");
                        //FB 1766 - End
                        //FB 1907 - Start
                        if (chkEnableRecord.Checked)
                            inXML.Append("<enableRecord>1</enableRecord>");
                        else
                            inXML.Append("<enableRecord>0</enableRecord>");

                        if (chkLPR.Checked)
                            inXML.Append("<enableLPR>1</enableLPR>");
                        else
                            inXML.Append("<enableLPR>0</enableLPR>");

                        //FB 1907 - End

                        if (chkMessage.Checked)//FB 2486
                            inXML.Append("<EnableMessage>1</EnableMessage>");
                        else
                            inXML.Append("<EnableMessage>0</EnableMessage>");
                        // FB 2441 RPRM Starts
                        if(lstMCUType.SelectedValue.Equals("13"))
                        {
                            inXML.Append("<PolycomRPRM>");
                            if (btn_sender.ToString().Trim() == "btn_DMATestConnection")
                                inXML.Append("<DMATestConnection>1</DMATestConnection>");
                            else
                                inXML.Append("<DMATestConnection>0</DMATestConnection>");

                            inXML.Append("<RPRMDomain>" + txtDomain.Text + "</RPRMDomain>"); //FB 2441 II
                            
                            if (chkSynchronous.Checked || enableCloudInstallation == 1)//FB 2659
                                inXML.Append("<Synchronous>1</Synchronous>");
                            else
                                inXML.Append("<Synchronous>0</Synchronous>");
                            
                            if (chkSendmail.Checked)
                                inXML.Append("<Sendmail>1</Sendmail>");
                            else
                                inXML.Append("<Sendmail>0</Sendmail>");

                            //FB 2709
                            inXML.Append("<LoginAccount>" + txtRPRMLogin.Text + "</LoginAccount>");
                            inXML.Append("<LoginCount>" + txtCallsCount.Text + "</LoginCount>");
                            inXML.Append("<RPRMEmail>" + txtRPRMEmail.Text + "</RPRMEmail>");
                            //FB 2709

                            if (ChkEhancedMCU.Checked)
                               inXML.Append("<MonitorMCU>1</MonitorMCU>");
                            else
                               inXML.Append("<MonitorMCU>0</MonitorMCU>");
                            
                            inXML.Append("<DMAName>" + txtDMAName.Text + "</DMAName>");
                            inXML.Append("<DMALogin>" +  txtDMALogin.Text + "</DMALogin>");
                            //FB 3054 Starts
                            if (Session["DMAPW"] == null)
                                Session["DMAPW"] = "";
                            if (hdnDMSPW.Value != "" && hdnDMSPW.Value == "true")
                                inXML.Append("<DMAPassword>" + UserPassword(txtDMAPassword.Text) + "</DMAPassword>");
                            else
                                inXML.Append("<DMAPassword>" + Session["DMAPW"].ToString() + "</DMAPassword>"); 
                            //FB 3054 Ends
                            inXML.Append("<DMAURL>" + txtDMAURL.Text + "</DMAURL>"); 
                            inXML.Append("<DMAPort>" + txtDMAPort.Text + "</DMAPort>");
                            inXML.Append("<DMADialinPrefix>" + txtDialinprefix.Text.Trim() + "</DMADialinPrefix>"); //FB 2689
                            inXML.Append("<DMADomain>" + txtDMADomain.Text + "</DMADomain>");
                            inXML.Append("</PolycomRPRM>");
                       }

                        // FB 2441 RPRM Ends

                        inXML.Append("<IPServices>");
                        if (trIPServices.Visible.Equals(true))
                            foreach (DataGridItem dgi in dgIPServices.Items)
                            {
                                if (!((CheckBox)dgi.FindControl("chkDelete")).Checked)
                                {
                                    inXML.Append("<IPService>");
                                    //inXML.Append("              <ID>" + dgi.Cells[0].Text + "</ID>");
                                    inXML.Append("<SortID>" + ((DropDownList)dgi.FindControl("lstOrder")).SelectedValue + "</SortID>");
                                    inXML.Append("<name>" + ((TextBox)dgi.FindControl("txtName")).Text + "</name>");
                                    inXML.Append("<addressType>" + ((DropDownList)dgi.FindControl("lstAddressType")).SelectedValue + "</addressType>");
                                    inXML.Append("<address>" + ((TextBox)dgi.FindControl("txtAddress")).Text + "</address>");
                                    inXML.Append("<networkAccess>" + ((DropDownList)dgi.FindControl("lstNetworkAccess")).SelectedValue + "</networkAccess>");
                                    inXML.Append("<usage>" + ((DropDownList)dgi.FindControl("lstUsage")).SelectedValue + "</usage>");
                                    inXML.Append("</IPService>");
                                }
                            }
                        inXML.Append("</IPServices>");
                        inXML.Append("<ISDNServices>");
                        if (dgISDNServices.Visible.Equals(true))
                            foreach (DataGridItem dgi in dgISDNServices.Items)
                            {
                                if (!((CheckBox)dgi.FindControl("chkDelete")).Checked)
                                {
                                    inXML.Append("<ISDNService>");
                                    //inXML.Append("              <ID>" + dgi.Cells[0].Text + "</ID>");
                                    inXML.Append("<SortID>" + ((DropDownList)dgi.FindControl("lstOrder")).SelectedValue + "</SortID>");
                                    inXML.Append("<name>" + ((TextBox)dgi.FindControl("txtName")).Text + "</name>");
                                    inXML.Append("<prefix>" + ((TextBox)dgi.FindControl("txtPrefix")).Text + "</prefix>");
                                    inXML.Append("<startRange>" + ((TextBox)dgi.FindControl("txtStartRange")).Text + "</startRange>");
                                    inXML.Append("<endRange>" + ((TextBox)dgi.FindControl("txtEndRange")).Text + "</endRange>");
                                    inXML.Append("<RangeSortOrder>" + ((DropDownList)dgi.FindControl("lstRangeSortOrder")).SelectedValue + "</RangeSortOrder>");
                                    inXML.Append("<networkAccess>" + ((DropDownList)dgi.FindControl("lstNetworkAccess")).SelectedValue + "</networkAccess>");
                                    inXML.Append("<usage>" + ((DropDownList)dgi.FindControl("lstUsage")).SelectedValue + "</usage>");
                                    inXML.Append("</ISDNService>");
                                }
                            }
                        inXML.Append("</ISDNServices>");
                        inXML.Append("<MPIServices>");

                        if (trMPIServices.Visible.Equals(true))
                            foreach (DataGridItem dgi in dgMPIServices.Items)
                            {
                                if (!((CheckBox)dgi.FindControl("chkDelete")).Checked)
                                {
                                    inXML.Append("<MPIService>");
                                    //inXML.Append("              <ID>" + dgi.Cells[0].Text + "</ID>");
                                    inXML.Append("<SortID>" + ((DropDownList)dgi.FindControl("lstOrder")).SelectedValue + "</SortID>");
                                    inXML.Append("<name>" + ((TextBox)dgi.FindControl("txtName")).Text + "</name>");
                                    inXML.Append("<addressType>" + ((DropDownList)dgi.FindControl("lstAddressType")).SelectedValue + "</addressType>");
                                    inXML.Append("<address>" + ((TextBox)dgi.FindControl("txtAddress")).Text + "</address>");
                                    inXML.Append("<networkAccess>" + ((DropDownList)dgi.FindControl("lstNetworkAccess")).SelectedValue + "</networkAccess>");
                                    inXML.Append("<usage>" + ((DropDownList)dgi.FindControl("lstUsage")).SelectedValue + "</usage>");
                                    inXML.Append("</MPIService>");
                                }
                            }
                        inXML.Append("</MPIServices>");

                        inXML.Append("<MCUCards>");
                        if (trMCUCards.Visible.Equals(true))
                            foreach (DataGridItem dgi in dgMCUCards.Items)
                            {
                                inXML.Append("<MCUCard>");
                                inXML.Append("<ID>" + dgi.Cells[0].Text + "</ID>");
                                inXML.Append("<MaximumCalls>" + ((TextBox)dgi.FindControl("txtMaxCalls")).Text + "</MaximumCalls>");
                                inXML.Append("</MCUCard>");
                            }
                        inXML.Append("</MCUCards>");
                        inXML.Append("<BridgeExtNo>" + txtExtNumber.Text + "</BridgeExtNo>"); //FB 2610
                        inXML.Append("<BridgeDomain>" + txtTempDomain.Text + "</BridgeDomain>"); //ZD 100522
                        inXML.Append("<ConferenceDialinName>" + txtDialinInfo.Text+ "</ConferenceDialinName>"); //C20 S4B
                        break;
                    case "3":
                       
                        inXML.Append("<portA>" + txtPortA.Text + "</portA>");
                        inXML.Append("<portB>" + txtPortB.Text + "</portB>");
                        inXML.Append("<ISDNAudioPref>" + txtISDNAudioPref.Text + "</ISDNAudioPref>");//FB 2003
                        inXML.Append("<ISDNVideoPref>" + txtISDNVideoPref.Text + "</ISDNVideoPref>");
                        inXML.Append("<ISDNServices>");
                        foreach (DataGridItem dgi in dgISDNServices.Items)
                        {
                            if (!((CheckBox)dgi.FindControl("chkDelete")).Checked)
                            {
                                inXML.Append("<ISDNService>");
                                inXML.Append("<SortID>" + ((DropDownList)dgi.FindControl("lstOrder")).SelectedValue + "</SortID>");
                                inXML.Append("<name>" + ((TextBox)dgi.FindControl("txtName")).Text + "</name>");
                                inXML.Append("<prefix>" + ((TextBox)dgi.FindControl("txtPrefix")).Text + "</prefix>");
                                inXML.Append("<startRange>" + ((TextBox)dgi.FindControl("txtStartRange")).Text + "</startRange>");
                                inXML.Append("<endRange>" + ((TextBox)dgi.FindControl("txtEndRange")).Text + "</endRange>");
                                inXML.Append("<RangeSortOrder>" + ((DropDownList)dgi.FindControl("lstRangeSortOrder")).SelectedValue + "</RangeSortOrder>");
                                inXML.Append("<networkAccess>" + ((DropDownList)dgi.FindControl("lstNetworkAccess")).SelectedValue + "</networkAccess>");
                                inXML.Append("<usage>" + ((DropDownList)dgi.FindControl("lstUsage")).SelectedValue + "</usage>");
                                inXML.Append("</ISDNService>");
                            }
                        }
                        inXML.Append("</ISDNServices>");
                        inXML.Append("<BridgeExtNo>" + txtTempExtNumber.Text + "</BridgeExtNo>"); //FB 2610
                        inXML.Append("<BridgeDomain>" + txtMCUDomain.Text + "</BridgeDomain>"); //ZD 100522
                        inXML.Append("<ConferenceDialinName>" + txtDialinInfo.Text + "</ConferenceDialinName>"); //C20 S4B
                        // FB 2636 Starts
                        inXML.Append("<E164Services>");
                        if (trE164Services.Visible.Equals(true))
                            for(int i=0;i<dgE164Services.Items.Count;i++)
                            {
                                E164dgi = dgE164Services.Items[i];
                                if (!((CheckBox)E164dgi.FindControl("chkDelete")).Checked)
                                {
                                    inXML.Append("<E164Service>");
                                    inXML.Append("<SortID>" + (i+1).ToString() + "</SortID>");
                                    inXML.Append("<StartRange>" + ((TextBox)E164dgi.FindControl("txtStartRange")).Text + "</StartRange>");
                                    inXML.Append("<EndRange>" + ((TextBox)E164dgi.FindControl("txtEndRange")).Text + "</EndRange>");
                                    inXML.Append("</E164Service>");
                                }
                            }
                        inXML.Append("</E164Services>");
                       
                        if (chkE164.Checked) 
                            inXML.Append("<E164Dialing>1</E164Dialing>");
                        else
                            inXML.Append("<E164Dialing>0</E164Dialing>");

                        if (chkH323.Checked)
                            inXML.Append("<H323Dialing>1</H323Dialing>");
                        else
                            inXML.Append("<H323Dialing>0</H323Dialing>");

                        // FB 2636 End
                        break;
                    case "4":
                        inXML.Append("<controlPortIPAddress>" + txtPortP.Text + "</controlPortIPAddress>");
                        inXML.Append("<IPServices></IPServices>");
                        inXML.Append("<ISDNServices>");
                        foreach (DataGridItem dgi in dgISDNServices.Items)
                        {
                            if (!((CheckBox)dgi.FindControl("chkDelete")).Checked)
                            {
                                inXML.Append("<ISDNService>");
                                inXML.Append("<SortID>" + ((DropDownList)dgi.FindControl("lstOrder")).SelectedValue + "</SortID>");
                                inXML.Append("<name>" + ((TextBox)dgi.FindControl("txtName")).Text + "</name>");
                                inXML.Append("<prefix>" + ((TextBox)dgi.FindControl("txtPrefix")).Text + "</prefix>");
                                inXML.Append("<startRange>" + ((TextBox)dgi.FindControl("txtStartRange")).Text + "</startRange>");
                                inXML.Append("<endRange>" + ((TextBox)dgi.FindControl("txtEndRange")).Text + "</endRange>");
                                inXML.Append("<RangeSortOrder>" + ((DropDownList)dgi.FindControl("lstRangeSortOrder")).SelectedValue + "</RangeSortOrder>");
                                inXML.Append("<networkAccess>" + ((DropDownList)dgi.FindControl("lstNetworkAccess")).SelectedValue + "</networkAccess>");
                                inXML.Append("<usage>" + ((DropDownList)dgi.FindControl("lstUsage")).SelectedValue + "</usage>");
                                inXML.Append("</ISDNService>");
                            }
                        }
                        inXML.Append("</ISDNServices>");
                        inXML.Append("<BridgeExtNo>" + txtExtNumber.Text + "</BridgeExtNo>"); //FB 2610
                        inXML.Append("<BridgeDomain>" + txtTempDomain.Text + "</BridgeDomain>"); //ZD 100522
                        inXML.Append("<ConferenceDialinName>" + txtDialinInfo.Text+ "</ConferenceDialinName>"); //C20 S4B
                        break;
                    case "5":
                        inXML.Append("<controlPortIPAddress>" + txtPortP.Text + "</controlPortIPAddress>");
                        //FB 2016 - Start
                        Int32.TryParse(txtConfServiceID.Text, out confserID);
                            if(confserID >=0)
                            {
                                inXML.Append("<ConferenceServiceID>" + txtConfServiceID.Text + "</ConferenceServiceID>"); 
                            }
                            else
                            {
                                errLabel.Text = obj.GetTranslatedText("Invalid Conference Service ID");//FB 1830 - Translation
                                //return inXML = ""; // FB 2636
                                return "";
                                     
                            }
                         //FB 2016 - End
                        inXML.Append("<IPServices>");
                        if (trIPServices.Visible.Equals(true))
                            foreach (DataGridItem dgi in dgIPServices.Items)
                            {
                                if (!((CheckBox)dgi.FindControl("chkDelete")).Checked)
                                {
                                    inXML.Append("<IPService>");
                                    //inXML.Append("              <ID>" + dgi.Cells[0].Text + "</ID>");
                                    inXML.Append("<SortID>" + ((DropDownList)dgi.FindControl("lstOrder")).SelectedValue + "</SortID>");
                                    inXML.Append("<name>" + ((TextBox)dgi.FindControl("txtName")).Text + "</name>");
                                    inXML.Append("<addressType>" + ((DropDownList)dgi.FindControl("lstAddressType")).SelectedValue + "</addressType>");
                                    inXML.Append("<address>" + ((TextBox)dgi.FindControl("txtAddress")).Text + "</address>");
                                    inXML.Append("<networkAccess>" + ((DropDownList)dgi.FindControl("lstNetworkAccess")).SelectedValue + "</networkAccess>");
                                    inXML.Append("<usage>" + ((DropDownList)dgi.FindControl("lstUsage")).SelectedValue + "</usage>");
                                    inXML.Append("</IPService>");
                                }
                            }
                        inXML.Append("</IPServices>");
                        inXML.Append("<ISDNServices>");
                        foreach (DataGridItem dgi in dgISDNServices.Items)
                        {
                            if (!((CheckBox)dgi.FindControl("chkDelete")).Checked)
                            {
                                inXML.Append("<ISDNService>");
                                inXML.Append("<SortID>" + ((DropDownList)dgi.FindControl("lstOrder")).SelectedValue + "</SortID>");
                                inXML.Append("<name>" + ((TextBox)dgi.FindControl("txtName")).Text + "</name>");
                                inXML.Append("<prefix>" + ((TextBox)dgi.FindControl("txtPrefix")).Text + "</prefix>");
                                inXML.Append("<startRange>" + ((TextBox)dgi.FindControl("txtStartRange")).Text + "</startRange>");
                                inXML.Append("<endRange>" + ((TextBox)dgi.FindControl("txtEndRange")).Text + "</endRange>");
                                inXML.Append("<RangeSortOrder>" + ((DropDownList)dgi.FindControl("lstRangeSortOrder")).SelectedValue + "</RangeSortOrder>");
                                inXML.Append("<networkAccess>" + ((DropDownList)dgi.FindControl("lstNetworkAccess")).SelectedValue + "</networkAccess>");
                                inXML.Append("<usage>" + ((DropDownList)dgi.FindControl("lstUsage")).SelectedValue + "</usage>");
                                inXML.Append("</ISDNService>");
                            }
                        }
                        inXML.Append("</ISDNServices>");
                        inXML.Append("<BridgeExtNo>" + txtExtNumber.Text + "</BridgeExtNo>"); //FB 2610
                        inXML.Append("<BridgeDomain>" + txtTempDomain.Text + "</BridgeDomain>"); //ZD 100522
                        inXML.Append("<ConferenceDialinName>" + txtDialinInfo.Text+ "</ConferenceDialinName>"); //C20 S4B
                        break;
                    //FB 2008
                    case "6":
                        inXML.Append("<portA>" + txtPortA.Text + "</portA>");
                        inXML.Append("<portB>" + txtPortB.Text + "</portB>");
                        inXML.Append("<ISDNGateway>" + txtISDNGateway.Text + "</ISDNGateway>");

                        inXML.Append("<ISDNAudioPref>" + txtISDNAudioPref.Text + "</ISDNAudioPref>");//FB 2003
                        inXML.Append("<ISDNVideoPref>" + txtISDNVideoPref.Text + "</ISDNVideoPref>");
                        inXML.Append("<ISDNServices>");
                        foreach (DataGridItem dgi in dgISDNServices.Items)
                        {
                            if (!((CheckBox)dgi.FindControl("chkDelete")).Checked)
                            {
                                inXML.Append("<ISDNService>");
                                inXML.Append("<SortID>" + ((DropDownList)dgi.FindControl("lstOrder")).SelectedValue + "</SortID>");
                                inXML.Append("<name>" + ((TextBox)dgi.FindControl("txtName")).Text + "</name>");
                                inXML.Append("<prefix>" + ((TextBox)dgi.FindControl("txtPrefix")).Text + "</prefix>");
                                inXML.Append("<startRange>" + ((TextBox)dgi.FindControl("txtStartRange")).Text + "</startRange>");
                                inXML.Append("<endRange>" + ((TextBox)dgi.FindControl("txtEndRange")).Text + "</endRange>");
                                inXML.Append("<RangeSortOrder>" + ((DropDownList)dgi.FindControl("lstRangeSortOrder")).SelectedValue + "</RangeSortOrder>");
                                inXML.Append("<networkAccess>" + ((DropDownList)dgi.FindControl("lstNetworkAccess")).SelectedValue + "</networkAccess>");
                                inXML.Append("<usage>" + ((DropDownList)dgi.FindControl("lstUsage")).SelectedValue + "</usage>");
                                inXML.Append("</ISDNService>");
                            }
                        }
                        inXML.Append("</ISDNServices>");
                        inXML.Append("<BridgeExtNo>" + txtTempExtNumber.Text + "</BridgeExtNo>"); //FB 2610
                        inXML.Append("<BridgeDomain>" + txtMCUDomain.Text + "</BridgeDomain>"); //ZD 100522
                        inXML.Append("<ConferenceDialinName>" + txtDialinInfo.Text + "</ConferenceDialinName>"); //C20 S4B 
                        // FB 2636 Starts
                        inXML.Append("<E164Services>");
                        if (trE164Services.Visible.Equals(true))
                            for (int i = 0; i < dgE164Services.Items.Count; i++)
                            {
                                E164dgi = dgE164Services.Items[i];
                                if (!((CheckBox)E164dgi.FindControl("chkDelete")).Checked)
                                {
                                    inXML.Append("<E164Service>");
                                    inXML.Append("<SortID>" + (i + 1).ToString() + "</SortID>");
                                    inXML.Append("<StartRange>" + ((TextBox)E164dgi.FindControl("txtStartRange")).Text + "</StartRange>");
                                    inXML.Append("<EndRange>" + ((TextBox)E164dgi.FindControl("txtEndRange")).Text + "</EndRange>");
                                    inXML.Append("</E164Service>");
                                }
                            }
                        inXML.Append("</E164Services>");

                        if (chkE164.Checked)
                            inXML.Append("<E164Dialing>1</E164Dialing>");
                        else
                            inXML.Append("<E164Dialing>0</E164Dialing>");

                        if (chkH323.Checked)
                            inXML.Append("<H323Dialing>1</H323Dialing>");
                        else
                            inXML.Append("<H323Dialing>0</H323Dialing>");

                        // FB 2636 End
                        break;
                        //FB 2008
                    //FB 2261
                    case "7":
                        inXML.Append("<controlPortIPAddress>" + txtPortP.Text + "</controlPortIPAddress>");
                        inXML.Append("<IPServices></IPServices>");
                        inXML.Append("<ISDNServices></ISDNServices>");
                        inXML.Append("<BridgeExtNo>" + txtExtNumber.Text + "</BridgeExtNo>"); //FB 2610
                        inXML.Append("<BridgeDomain>" + txtTempDomain.Text + "</BridgeDomain>"); //ZD 100522
                        inXML.Append("<ConferenceDialinName>" + txtDialinInfo.Text+ "</ConferenceDialinName>"); //C20 S4B
                        break;
                    //FB 2501 Call Monitoring
                    case "8":
                        //FB 2718 Starts
                        if (lstMCUType.SelectedValue.Equals("15"))
                        {
                            inXML.Append("<portA>" + txtPortP.Text + "</portA>");
                            inXML.Append("<Domain>" + txtDomain.Text + "</Domain>");
                            if (chkSynchronous.Checked)
                                inXML.Append("<Synchronous>1</Synchronous>");
                            else
                                inXML.Append("<Synchronous>0</Synchronous>");
                            
                            inXML.Append("<BridgeExtNo>" + txtExtNumber.Text + "</BridgeExtNo>"); //ZD 101577
                            inXML.Append("<BridgeDomain>" + txtTempDomain.Text + "</BridgeDomain>"); //ZD 101577
                        }
                        //FB 2718 Ends
                        else if (lstMCUType.SelectedValue.Equals("12"))
                        {
                            inXML.Append("<portA>" + txtPortA.Text + "</portA>");
                            inXML.Append("<portB>" + txtPortB.Text + "</portB>");
                        }
                        inXML.Append("<ISDNAudioPref>" + txtISDNAudioPref.Text + "</ISDNAudioPref>");//FB 2003
                        inXML.Append("<ISDNVideoPref>" + txtISDNVideoPref.Text + "</ISDNVideoPref>");
                        inXML.Append("<ISDNServices>");
                        foreach (DataGridItem dgi in dgISDNServices.Items)
                        {
                            if (!((CheckBox)dgi.FindControl("chkDelete")).Checked)
                            {
                                inXML.Append("<ISDNService>");
                                inXML.Append("<SortID>" + ((DropDownList)dgi.FindControl("lstOrder")).SelectedValue + "</SortID>");
                                inXML.Append("<name>" + ((TextBox)dgi.FindControl("txtName")).Text + "</name>");
                                inXML.Append("<prefix>" + ((TextBox)dgi.FindControl("txtPrefix")).Text + "</prefix>");
                                inXML.Append("<startRange>" + ((TextBox)dgi.FindControl("txtStartRange")).Text + "</startRange>");
                                inXML.Append("<endRange>" + ((TextBox)dgi.FindControl("txtEndRange")).Text + "</endRange>");
                                inXML.Append("<RangeSortOrder>" + ((DropDownList)dgi.FindControl("lstRangeSortOrder")).SelectedValue + "</RangeSortOrder>");
                                inXML.Append("<networkAccess>" + ((DropDownList)dgi.FindControl("lstNetworkAccess")).SelectedValue + "</networkAccess>");
                                inXML.Append("<usage>" + ((DropDownList)dgi.FindControl("lstUsage")).SelectedValue + "</usage>");
                                inXML.Append("</ISDNService>");
                            }
                        }
                        inXML.Append("</ISDNServices>");
                        inXML.Append("<BridgeExtNo>" + txtTempExtNumber.Text + "</BridgeExtNo>"); //FB 2610
                        inXML.Append("<BridgeDomain>" + txtMCUDomain.Text + "</BridgeDomain>"); //ZD 100522
                        inXML.Append("<ConferenceDialinName>" + txtDialinInfo.Text + "</ConferenceDialinName>"); //C20 S4B 
                        // FB 2636 Starts
                        inXML.Append("<E164Services>");
                        if (trE164Services.Visible.Equals(true))
                            for (int i = 0; i < dgE164Services.Items.Count; i++)
                            {
                                E164dgi = dgE164Services.Items[i];
                                if (!((CheckBox)E164dgi.FindControl("chkDelete")).Checked)
                                {
                                    inXML.Append("<E164Service>");
                                    inXML.Append("<SortID>" + (i + 1).ToString() + "</SortID>");
                                    inXML.Append("<StartRange>" + ((TextBox)E164dgi.FindControl("txtStartRange")).Text + "</StartRange>");
                                    inXML.Append("<EndRange>" + ((TextBox)E164dgi.FindControl("txtEndRange")).Text + "</EndRange>");
                                    inXML.Append("</E164Service>");
                                }
                            }
                        inXML.Append("</E164Services>");

                        if (chkE164.Checked)
                            inXML.Append("<E164Dialing>1</E164Dialing>");
                        else
                            inXML.Append("<E164Dialing>0</E164Dialing>");

                        if (chkH323.Checked)
                            inXML.Append("<H323Dialing>1</H323Dialing>");
                        else
                            inXML.Append("<H323Dialing>0</H323Dialing>");

                        // FB 2636 End
                        break;
                    //FB 2556 - Starts
                    case "9":
                        inXML.Append("<controlPortIPAddress>" + txtPortP.Text + "</controlPortIPAddress>");
                        inXML.Append("<IPServices></IPServices>");
                        inXML.Append("<ISDNServices></ISDNServices>");
                        if (chkSynchronous.Checked)
                            inXML.Append("<Synchronous>1</Synchronous>");
                        else
                            inXML.Append("<Synchronous>0</Synchronous>");
                        inXML.Append("<BridgeExtNo>" + txtExtNumber.Text + "</BridgeExtNo>");
                        inXML.Append("<BridgeDomain>" + txtTempDomain.Text + "</BridgeDomain>"); //ZD 100522
                        inXML.Append("<ConferenceDialinName>" + txtDialinInfo.Text+ "</ConferenceDialinName>"); //C20 S4B
                        inXML.Append("<orgSpecificDetails>");
                        inXML.Append("<ScopiaDesktopURL>" + txtScopiaURL.Text + "</ScopiaDesktopURL>");
                        inXML.Append("<ScopiaOrgID>" + lstScopiaOrg.SelectedValue + "</ScopiaOrgID>");
                        inXML.Append("<ScopiaOrgLogin>" + txtScopiaOrgLogin.Text + "</ScopiaOrgLogin>");
                        inXML.Append("<ScopiaServiceID>" + lstScopiaService.SelectedValue + "</ScopiaServiceID>");
                        inXML.Append("</orgSpecificDetails>");
                        if (chkMultiTenant.Checked)
                            inXML.Append("<Multitenant>1</Multitenant>");
                        else
                            inXML.Append("<Multitenant>0</Multitenant>");

                        break;
                    //FB 2556 - End
                    //ZD 101217 Start
                    case "10":
                        inXML.Append("<SysLocationId>" + lstSystemLocation.SelectedValue + "</SysLocationId>");//ZD 101522
                        //ALLDEV-854 Starts
                        inXML.Append("<EnableAlias>" + DrpDwnAlias.SelectedValue + "</EnableAlias>");
                        if (DrpDwnAlias.SelectedValue == "1")
                        {
                            inXML.Append("<Alias2>" + txtAlias2.Text + "</Alias2>");
                            inXML.Append("<Alias3>" + txtAlias3.Text + "</Alias3>");
                        }
                        else
                        {
                            inXML.Append("<Alias2></Alias2>");
                            inXML.Append("<Alias3></Alias3>");
                        }
                        //ALLDEV-854 Ends
                        
                        inXML.Append("<portA>" + txtPortA.Text + "</portA>");
                        inXML.Append("<portB>" + txtPortB.Text + "</portB>");
                        inXML.Append("<ISDNAudioPref>" + txtISDNAudioPref.Text + "</ISDNAudioPref>");//FB 2003
                        inXML.Append("<ISDNVideoPref>" + txtISDNVideoPref.Text + "</ISDNVideoPref>");
                        inXML.Append("<ISDNServices>");
                        foreach (DataGridItem dgi in dgISDNServices.Items)
                        {
                            if (!((CheckBox)dgi.FindControl("chkDelete")).Checked)
                            {
                                inXML.Append("<ISDNService>");
                                inXML.Append("<SortID>" + ((DropDownList)dgi.FindControl("lstOrder")).SelectedValue + "</SortID>");
                                inXML.Append("<name>" + ((TextBox)dgi.FindControl("txtName")).Text + "</name>");
                                inXML.Append("<prefix>" + ((TextBox)dgi.FindControl("txtPrefix")).Text + "</prefix>");
                                inXML.Append("<startRange>" + ((TextBox)dgi.FindControl("txtStartRange")).Text + "</startRange>");
                                inXML.Append("<endRange>" + ((TextBox)dgi.FindControl("txtEndRange")).Text + "</endRange>");
                                inXML.Append("<RangeSortOrder>" + ((DropDownList)dgi.FindControl("lstRangeSortOrder")).SelectedValue + "</RangeSortOrder>");
                                inXML.Append("<networkAccess>" + ((DropDownList)dgi.FindControl("lstNetworkAccess")).SelectedValue + "</networkAccess>");
                                inXML.Append("<usage>" + ((DropDownList)dgi.FindControl("lstUsage")).SelectedValue + "</usage>");
                                inXML.Append("</ISDNService>");
                            }
                        }
                        inXML.Append("</ISDNServices>");
                        inXML.Append("<BridgeExtNo>" + txtTempExtNumber.Text + "</BridgeExtNo>"); //FB 2610
                        inXML.Append("<BridgeDomain>" + txtMCUDomain.Text + "</BridgeDomain>"); //ZD 100522
                        inXML.Append("<ConferenceDialinName>" + txtDialinInfo.Text + "</ConferenceDialinName>"); //C20 S4B 
                        // FB 2636 Starts
                        inXML.Append("<E164Services>");
                        if (trE164Services.Visible.Equals(true))
                            for (int i = 0; i < dgE164Services.Items.Count; i++)
                            {
                                E164dgi = dgE164Services.Items[i];
                                if (!((CheckBox)E164dgi.FindControl("chkDelete")).Checked)
                                {
                                    inXML.Append("<E164Service>");
                                    inXML.Append("<SortID>" + (i + 1).ToString() + "</SortID>");
                                    inXML.Append("<StartRange>" + ((TextBox)E164dgi.FindControl("txtStartRange")).Text + "</StartRange>");
                                    inXML.Append("<EndRange>" + ((TextBox)E164dgi.FindControl("txtEndRange")).Text + "</EndRange>");
                                    inXML.Append("</E164Service>");
                                }
                            }
                        inXML.Append("</E164Services>");

                        if (chkE164.Checked)
                            inXML.Append("<E164Dialing>1</E164Dialing>");
                        else
                            inXML.Append("<E164Dialing>0</E164Dialing>");

                        if (chkH323.Checked)
                            inXML.Append("<H323Dialing>1</H323Dialing>");
                        else
                            inXML.Append("<H323Dialing>0</H323Dialing>");

                        // FB 2636 End
                        break;
                    case "11": //ZD 103550
                        inXML.Append("<controlPortIPAddress>" + txtBJNAddress.Text + "</controlPortIPAddress>");

                        if (Session["BJNAppkey"] == null)
                            Session["BJNAppkey"] = "";
                        if (hdnBJNAppKey1.Value != "" && hdnBJNAppKey1.Value == "true")
                            inXML.Append("<BJNAppKey>" + txtBJNAppKey.Text.Trim() + "</BJNAppKey>");
                        else
                            inXML.Append("<BJNAppKey>" + Session["BJNAppkey"].ToString() + "</BJNAppKey>");

                        if (Session["BJNAppSecret"] == null)
                            Session["BJNAppSecret"] = "";
                        if (hdnBJNAppSecret1.Value != "" && hdnBJNAppSecret1.Value == "true")
                            inXML.Append("<BJNAppSecret>" + txtBJNAppSecret.Text.Trim() + "</BJNAppSecret>");
                        else
                            inXML.Append("<BJNAppSecret>" + Session["BJNAppSecret"].ToString() + "</BJNAppSecret>");                         
                        
                        break;
                    //ZD 101217 End
                }

                inXML.Append("</bridgeDetails>");
                String chkTemp = "0";
                if (chkISDNThresholdAlert.Checked) chkTemp = "1";
                inXML.Append("  <ISDNThresholdAlert>" + chkTemp + "</ISDNThresholdAlert>");
                if (chkTemp.Equals("1"))
                {
                    inXML.Append("<ISDNPortCharge>" + txtMCUISDNPortCharge.Text + "</ISDNPortCharge>");
                    inXML.Append("<ISDNLineCharge>" + txtISDNLineCost.Text + "</ISDNLineCharge>");
                    inXML.Append("<ISDNMaxCost>" + txtISDNMaxCost.Text + "</ISDNMaxCost>");
                    inXML.Append("<ISDNThresholdTimeframe>" + rdISDNThresholdTimeframe.SelectedValue + "</ISDNThresholdTimeframe>");
                    inXML.Append("<ISDNThreshold>" + txtISDNThresholdPercentage.Text + "</ISDNThreshold>");
                }
                chkTemp = "0";
                if (chkMalfunctionAlert.Checked) chkTemp = "1";
                inXML.Append("<malfunctionAlert>" + chkTemp + "</malfunctionAlert>");

                //FB 1642 - DTMF Start Commented for FB 2655
                //inXML.Append("<DTMF>");
                //inXML.Append("<PreConfCode>" + PreConfCode.Text.Trim() + "</PreConfCode>");
                //inXML.Append("<PreLeaderPin>" + PreLeaderPin.Text.Trim() + "</PreLeaderPin>");
                //inXML.Append("<PostLeaderPin>" + PostLeaderPin.Text.Trim() + "</PostLeaderPin>");
                //inXML.Append("</DTMF>");
                //FB 1642 - DTMF End
                //FB 2660 Starts
                if (chkMessage.Checked || ChkEhancedMCU.Checked || chkE164.Checked || chkH323.Checked || chkFailover.Checked || chkSynchronous.Checked || chkIsVirtual.Checked) //ZD 100369_MCU //ZD 100971 //ZD 100040
                    inXML.Append("<EnhancedMCU>1</EnhancedMCU>");
                else
                    inXML.Append("<EnhancedMCU>0</EnhancedMCU>");
                //FB 2660 Ends
                inXML.Append("<AccessURL>" + txtURL.Text + "</AccessURL>"); //ZD 101078
                
                inXML.Append("</bridge>");
                inXML.Append("</setBridge>");
                //Response.Write(obj.Transfer(inXML));
                //Response.End();
                return inXML.ToString();
            }
            catch (Exception ex)
            {
                errLabel.Visible = true;
                errLabel.Text = ex.Message;
                log.Trace(ex.StackTrace + " : " + ex.Message);
                return "";
            }
        }
        protected void TestConnection(Object sender, EventArgs e)
        {
            try
            {
                //Response.Write("in test connection");
                // FB 2441 Starts
                string ButtonID = "";
                //ZD 100369 Start
                System.Web.UI.HtmlControls.HtmlButton btn_sender = (System.Web.UI.HtmlControls.HtmlButton)sender;
                //Button btn_sender = (Button)sender;
                //ZD 100369
                ButtonID = btn_sender.ID;
                // FB 2441 Ends

                String inXML = GenerateInXML(ButtonID);

                if (confPassword.Value != "false" && hdnPasschange.Value == "true")
                    confPassword.Value = txtPassword1.Text;
                String outXML = "";
                errLabel.Visible = true;
                if (!inXML.Equals(""))
                {
                    outXML = obj.CallCOM2("TestMCUConnection", inXML, Application["RTC_ConfigPath"].ToString());
                    if (outXML.IndexOf("<error>") < 0)
                        //Response.Redirect("ManageBridge.aspx?m=1");
                        errLabel.Text = obj.GetTranslatedText("Operation Successful!");//FB 1830 - Translation
                    else
                        errLabel.Text = obj.ShowErrorMessage(outXML);
                }
            }
            catch (Exception ex)
            {
                log.Trace(ex.StackTrace + " : " + ex.Message);
            }
        }

        protected void EditService(Object sender, DataGridCommandEventArgs e)
        {
            try
            {
                //Response.Write(e.Item.ItemIndex);
                dgIPServices.EditItemIndex = e.Item.ItemIndex;
            }
            catch (Exception ex)
            {
                log.Trace(ex.StackTrace + " : " + ex.Message);
            }
        }

        protected void BindAddressType(Object sender, EventArgs e)
        {
            try
            {
                obj.BindAddressType((DropDownList)sender);
            }
            catch (Exception ex)
            {
                log.Trace(ex.StackTrace + " : " + ex.Message);
            }
        }

        protected void AddNewIPService(Object sender, EventArgs e)
        {
            try
            {
                //Response.Write(dgIPServices.Items.Count);
                if (dgIPServices.Items.Count < 10)
                {
                    DataTable dt = new DataTable();
                    DataRow dr;
                    //dt.Columns.Add("ID");
                    dt.Columns.Add("SortID");
                    dt.Columns.Add("name");
                    dt.Columns.Add("addressType");
                    dt.Columns.Add("address");
                    dt.Columns.Add("networkAccess");
                    dt.Columns.Add("usage");
                    foreach (DataGridItem dgi in dgIPServices.Items)
                    {
                        dr = dt.NewRow();
                        //dr["ID"] = dgi.Cells[0].Text;
                        dr["SortID"] = ((DropDownList)dgi.FindControl("lstOrder")).SelectedValue;
                        dr["name"] = ((TextBox)dgi.FindControl("txtName")).Text;
                        dr["addressType"] = ((DropDownList)dgi.FindControl("lstAddressType")).SelectedValue;
                        dr["address"] = ((TextBox)dgi.FindControl("txtAddress")).Text;
                        dr["networkAccess"] = ((DropDownList)dgi.FindControl("lstNetworkAccess")).SelectedValue;
                        dr["usage"] = ((DropDownList)dgi.FindControl("lstUsage")).SelectedValue;
                        dt.Rows.Add(dr);
                    }
                    dr = dt.NewRow();
                    //dr["ID"] = "new";
                    dr["SortID"] = dt.Rows.Count + 1;
                    dr["name"] = "";
                    dr["addressType"] = ns_MyVRMNet.vrmAddressType.IP;
                    dr["address"] = "";
                    dr["networkAccess"] = "3";
                    dr["usage"] = "3";
                    dt.Rows.Add(dr);
                    dgIPServices.DataSource = dt;
                    dgIPServices.DataBind();
                    dgIPServices.Visible = true;
                    btnAddNewIPService.Visible = false;
                    lblNoIPServices.Visible = false;
                }
                else
                    errLabel.Text = obj.GetTranslatedText("More than 10 IP Services can not be added.");//FB 1830 - Translation
                dgIPServices.Focus();
            }
            catch (Exception ex)
            {
                log.Trace(ex.StackTrace + " : " + ex.Message);
            }
        }
        protected void AddNewISDNService(Object sender, EventArgs e)
        {
            try
            {
                if (dgISDNServices.Items.Count < 10)
                {
                    DataTable dt = new DataTable();
                    DataRow dr;
                    //dt.Columns.Add("ID");
                    dt.Columns.Add("SortID");
                    dt.Columns.Add("RangeSortOrder");
                    dt.Columns.Add("name");
                    dt.Columns.Add("prefix");
                    dt.Columns.Add("startRange");
                    dt.Columns.Add("endRange");
                    dt.Columns.Add("networkAccess");
                    dt.Columns.Add("usage");
                    foreach (DataGridItem dgi in dgISDNServices.Items)
                    {
                        dr = dt.NewRow();
                        //dr["ID"] = dgi.Cells[0].Text;
                        dr["SortID"] = ((DropDownList)dgi.FindControl("lstOrder")).SelectedValue;
                        dr["name"] = ((TextBox)dgi.FindControl("txtName")).Text;
                        dr["prefix"] = ((TextBox)dgi.FindControl("txtPrefix")).Text;
                        dr["startRange"] = ((TextBox)dgi.FindControl("txtStartRange")).Text;
                        dr["endRange"] = ((TextBox)dgi.FindControl("txtEndRange")).Text;
                        dr["RangeSortOrder"] = ((DropDownList)dgi.FindControl("lstRangeSortOrder")).SelectedValue;
                        dr["networkAccess"] = ((DropDownList)dgi.FindControl("lstNetworkAccess")).SelectedValue;
                        dr["usage"] = ((DropDownList)dgi.FindControl("lstUsage")).SelectedValue;
                        dt.Rows.Add(dr);
                    }
                    dr = dt.NewRow();
                    //dr["ID"] = "new";
                    dr["SortID"] = dt.Rows.Count + 1;
                    dr["name"] = "";
                    dr["prefix"] = "";
                    dr["startRange"] = "";
                    dr["endRange"] = "";
                    dr["RangeSortOrder"] = "0";
                    dr["networkAccess"] = "3";
                    dr["usage"] = "3";
                    dt.Rows.Add(dr);
                    dgISDNServices.DataSource = dt;
                    dgISDNServices.Visible = true;
                    dgISDNServices.DataBind();
                    btnAddISDNService.Visible = false;
                    lblNoISDNServices.Visible = false;
                }
                else
                {
                    errLabel.Text = obj.GetTranslatedText("More than 10 ISDN Services can not be added.");//FB 1830 - Translation
                }
                dgISDNServices.Focus();
            }
            catch (Exception ex)
            {
                log.Trace(ex.StackTrace + " : " + ex.Message);
            }
        }

        /* commented for ZD 103595- Operations - Remove MPI Functionality and Verbiage START
        
        protected void AddNewMPIService(Object sender, EventArgs e)
        {
            try
            {
                //Response.Write(dgIPServices.Items.Count);
                if (dgMPIServices.Items.Count < 10)
                {
                    DataTable dt = new DataTable();
                    DataRow dr;
                    //dt.Columns.Add("ID");
                    dt.Columns.Add("SortID");
                    dt.Columns.Add("name");
                    dt.Columns.Add("addressType");
                    dt.Columns.Add("address");
                    dt.Columns.Add("networkAccess");
                    dt.Columns.Add("usage");
                    foreach (DataGridItem dgi in dgMPIServices.Items)
                    {
                        dr = dt.NewRow();
                        //dr["ID"] = dgi.Cells[0].Text;
                        dr["SortID"] = ((DropDownList)dgi.FindControl("lstOrder")).SelectedValue;
                        dr["name"] = ((TextBox)dgi.FindControl("txtName")).Text;
                        dr["addressType"] = ((DropDownList)dgi.FindControl("lstAddressType")).SelectedValue;
                        dr["address"] = ((TextBox)dgi.FindControl("txtAddress")).Text;
                        dr["networkAccess"] = ((DropDownList)dgi.FindControl("lstNetworkAccess")).SelectedValue;
                        dr["usage"] = ((DropDownList)dgi.FindControl("lstUsage")).SelectedValue;
                        dt.Rows.Add(dr);
                    }
                    dr = dt.NewRow();
                    //dr["ID"] = "new";
                    dr["SortID"] = dt.Rows.Count + 1;
                    dr["name"] = "";
                    dr["addressType"] = ns_MyVRMNet.vrmAddressType.MPI;
                    dr["address"] = "";
                    dr["networkAccess"] = "3";
                    dr["usage"] = "3";
                    dt.Rows.Add(dr);
                    dgMPIServices.DataSource = dt;
                    dgMPIServices.DataBind();
                    dgMPIServices.Visible = true;
                    btnAddNewMPIService.Visible = false;
                    lblNoMPIServices.Visible = false;
                    ((TextBox)dgMPIServices.Items[dgMPIServices.Items.Count - 1].FindControl("txtAddress")).Focus();

                }
                else
                    errLabel.Text = obj.GetTranslatedText("More than 10 IP Services can not be added.");//FB 1830 - Translation
                dgMPIServices.Focus();
            }
            catch (Exception ex)
            {
                log.Trace(ex.StackTrace + " : " + ex.Message);
            }
        }
         */
        //ZD commented for ZD 103595- Operations - Remove MPI Functionality and Verbiage END

        // FB 2636 Starts

        # region AddNewE164Service 
        /// <summary>
        /// AddNewE164Service
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void AddNewE164Service(Object sender, EventArgs e)
        {
            try
            {                
                if (dgE164Services.Items.Count < 10)
                {
                    int tempSortID = 0;
                    DataTable dt = new DataTable();
                    DataGridItem dgi;
                    DataRow dr;                    
                    dt.Columns.Add("SortID");
                    dt.Columns.Add("StartRange");
                    dt.Columns.Add("EndRange");
                    for(int i=0; i < dgE164Services.Items.Count; i++)
                    {
                        dgi = dgE164Services.Items[i];
                        dr = dt.NewRow();
                        dr["SortID"] = (i + 1).ToString();
                        dr["StartRange"] = ((TextBox)dgi.FindControl("txtStartRange")).Text;
                        dr["EndRange"] = ((TextBox)dgi.FindControl("txtEndRange")).Text;
                        int.TryParse(dr["SortID"].ToString(), out tempSortID);
                        dt.Rows.Add(dr);
                    }
                    dr = dt.NewRow();
                    dr["SortID"] = tempSortID + 1;
                    dr["StartRange"] = "";                    
                    dr["EndRange"] = "";
                    dt.Rows.Add(dr);
                    dgE164Services.DataSource = dt;
                    dgE164Services.DataBind();
                    dgE164Services.Visible = true;
                    btnAddNewE164Service.Visible = false;
                    lblNoE164Services.Visible = false;
                }
                else
                    errLabel.Text = obj.GetTranslatedText("More than 10 E.164 Services can not be added.");
                dgE164Services.Focus();
            }
            catch (Exception ex)
            {
                log.Trace(ex.StackTrace + " : " + ex.Message);
            }
        }
        #endregion

        // FB 2636 Ends

        protected void ChangeIPOrder(Object sender, EventArgs e)
        {
            try
            {
                DropDownList lstTemp = (DropDownList)sender;
                //Response.Write(Int32.Parse(lstTemp.SelectedValue) + " : " + dgIPServices.Items.Count);
                if (Int32.Parse(lstTemp.SelectedValue) > dgIPServices.Items.Count)
                {
                    errLabel.Text = obj.GetTranslatedText("Invalid Selection.");//FB 1830 - Translation
                    lstTemp.SelectedValue = "1";
                }
                else
                    foreach (DataGridItem dgi in dgIPServices.Items)
                        if (((DropDownList)dgi.FindControl("lstOrder")).SelectedValue.Equals(lstTemp.SelectedValue))
                        {
                            errLabel.Text = obj.GetTranslatedText("This value has already been taken.");//FB 1830 - Translation
                            lstTemp.SelectedValue = dgi.ItemIndex.ToString();
                            break;
                        }
            }
            catch (Exception ex)
            {
                log.Trace(ex.StackTrace + " : " + ex.Message);
            }
        }
        protected void ChangeISDNOrder(Object sender, EventArgs e)
        {
            try
            {
                DropDownList lstTemp = (DropDownList)sender;
                //Response.Write(Int32.Parse(lstTemp.SelectedValue) + " : " + dgIPServices.Items.Count);
                if (Int32.Parse(lstTemp.SelectedValue) > dgISDNServices.Items.Count)
                {
                    errLabel.Text = obj.GetTranslatedText("Invalid Selection.");//FB 1830 - Translation
                    lstTemp.SelectedValue = "1";
                }
                else
                    foreach (DataGridItem dgi in dgISDNServices.Items)
                        if (((DropDownList)dgi.FindControl("lstOrder")).SelectedValue.Equals(lstTemp.SelectedValue))
                        {
                            errLabel.Text = obj.GetTranslatedText("This value has already been taken.");//FB 1830 - Translation
                            lstTemp.SelectedValue = dgi.ItemIndex.ToString();
                            break;
                        }
            }
            catch (Exception ex)
            {
                log.Trace(ex.StackTrace + " : " + ex.Message);
            }
        }
        protected void ValidateIP(Object sender, ServerValidateEventArgs e)
        {
            try
            {
                customvalidation.Text = "";
                string pattern = @"^([1-9]|[1-9][0-9]|1[0-9][0-9]|2[0-4][0-9]|25[0-5])(\.([0-9]|[1-9][0-9]|1[0-9][0-9]|2[0-4][0-9]|25[0-5])){3}$";
                Regex check = new Regex(pattern);
                String IPToValidate = txtPortP.Text;
                bool valid = true;
                if (lstInterfaceType.SelectedItem.Text.Equals("1"))
                {
                    if (IPToValidate == "")
                    {
                        valid = false;
                    }
                    else
                    {
                        valid = check.IsMatch(IPToValidate, 0);
                    }
                    if (valid.Equals(true))
                        e.IsValid = true;
                    else
                        e.IsValid = false;
                    foreach (DataGridItem dgi in dgIPServices.Items)
                    {
                        if (((DropDownList)dgi.FindControl("lstAddressType")).SelectedValue.Equals(ns_MyVRMNet.vrmAddressType.IP))
                        {
                            String IPToValidate1 = ((TextBox)dgi.FindControl("txtAddress")).Text;
                            if (IPToValidate1 == "")
                                valid = false;
                            else
                                valid = check.IsMatch(IPToValidate1, 0);

                            if (valid.Equals(false))
                            {
                                customvalidation.Text = obj.GetTranslatedText(ns_MyVRMNet.ErrorList.InvalidIPAddress);//ZD 100288
                                break;
                            }
                        }
                        //ZD 103595 Removed MPI for ZD 103595- Operations - Remove MPI Functionality and Verbiage
                        else if (((DropDownList)dgi.FindControl("lstAddressType")).SelectedValue.Equals(ns_MyVRMNet.vrmAddressType.ISDN))
                        {
                            valid = false;
                            customvalidation.Text = obj.GetTranslatedText("Invalid Address Type selected for IP Service #" + ((DropDownList)dgi.FindControl("lstOrder")).SelectedValue);//FB 1830D - Translation
                            break;
                        }
                    }
                    if (valid.Equals(true))
                        foreach (DataGridItem dgi in dgISDNServices.Items)
                        {
                            if (!((CheckBox)dgi.FindControl("chkDelete")).Checked)
                            {
                                if (((TextBox)dgi.FindControl("txtEndRange")).Text.Length > 22 || ((TextBox)dgi.FindControl("txtStartRange")).Text.Length > 22)
                                {
                                    valid = false;
                                    customvalidation.Text = obj.GetTranslatedText("Max. length for start and end range is 22 characters.");//FB 1830 - Translation
                                    break;
                                }
                                else
                                {
                                    Double startRange = Double.Parse(((TextBox)dgi.FindControl("txtStartRange")).Text);
                                    Double endRange = Double.Parse(((TextBox)dgi.FindControl("txtEndRange")).Text);
                                    if (endRange <= startRange)
                                    {
                                        valid = false;
                                        customvalidation.Text = obj.GetTranslatedText("Invalid Start and End Range");//FB 1830 - Translation
                                        break;
                                    }
                                }
                            }
                        }
                }
                if (valid.Equals(true))
                    foreach (DataGridItem dgi in dgMPIServices.Items)
                    {
                        //Removed MPI check for ZD 103595- Operations - Remove MPI Functionality and Verbiage
                        if (!((CheckBox)dgi.FindControl("chkDelete")).Checked)
                        {
                            valid = false;
                            customvalidation.Text = obj.GetTranslatedText("Invalid Address Type selected for IP Service #" + ((DropDownList)dgi.FindControl("lstOrder")).SelectedValue);//FB 1830 - Translation
                            break;
                        }
                    }

                //Response.Write(valid);
                //Response.End();

                e.IsValid = valid;
            }
            catch (Exception ex)
            {
                log.Trace(ex.StackTrace + " : " + ex.Message);
                e.IsValid = false;
            }
        }

        protected void DisableAllControls(Control ctrl)
        {
            try
            {
                //Response.Write("<br>" + ctrl.ID);
                foreach (Control ctl in ctrl.Controls)
                {
                    if (ctl is Button)
                        ((Button)ctl).Visible = false;
                    if (ctl is DataGrid)
                        ((DataGrid)ctl).Enabled = false;
                    DisableAllControls(ctl);
                }
            }
            catch (Exception ex)
            {
                log.Trace(ex.StackTrace + " : " + ex.Message);
            }
        }

        #region MCUusageReport
        protected void MCUusageReport(Object sender, EventArgs e)
        {
            try
            {
                String inXML = "";
                inXML = "<GetCompleteMCUusageReport>";
                inXML += "<BridgeID>" + Session["BridgeID"].ToString() + "</BridgeID>";
                inXML += "<timezone>" + Session["systemTimezoneID"].ToString() + "</timezone>";
                inXML += "<StartDate>" + myVRMNet.NETFunctions.GetDefaultDate(ReportDate.Text) + " " + myVRMNet.NETFunctions.ChangeTimeFormat(ReportTime.Text) + "</StartDate>"; //FB 2588
                inXML += "</GetCompleteMCUusageReport>";

                String outXML = "";
                outXML = obj.CallMyVRMServer("GetCompleteMCUusageReport", inXML, Application["MyVRMServer_ConfigPath"].ToString());
                if (outXML.IndexOf("<error>") < 0)
                {
                    DataSet ds = new DataSet();
                    XmlDocument xmldoc = new XmlDocument();
                    xmldoc.LoadXml(outXML);
                    XmlNodeList nodes = xmldoc.SelectNodes("//UsageReport/UsageDetails");
                    XmlTextReader xtr;
					//FB 1938
                    LblVideoused.Text = xmldoc.SelectSingleNode("//UsageReport/TotalVideoUsed").InnerText;
                    Int32 totVideoAvail = 0;
                    if (xmldoc.SelectSingleNode("//UsageReport/TotalVideoAvailable").InnerText != "")
                        Int32.TryParse(xmldoc.SelectSingleNode("//UsageReport/TotalVideoAvailable").InnerText,out totVideoAvail);

                    if (totVideoAvail < 0)
                        totVideoAvail = 0;

                    LblVideoavail.Text = totVideoAvail.ToString();

                    LblAudioused.Text = xmldoc.SelectSingleNode("//UsageReport/TotalAudioUsed").InnerText;
                    //ZD 100288 Start
                    if (LblAudioused.Text.ToUpper() == "N/A")
                        LblAudioused.Text = obj.GetTranslatedText("N/A");
                    //ZD 100288 End

                    Int32 totAudioAvail = 0;
                    if (xmldoc.SelectSingleNode("//UsageReport/TotalAudioAvailable").InnerText.ToUpper() == "N/A")
                        LblAudioavail.Text = obj.GetTranslatedText("N/A");
                    else if (xmldoc.SelectSingleNode("//UsageReport/TotalAudioAvailable").InnerText != "")
                    {
                        Int32.TryParse(xmldoc.SelectSingleNode("//UsageReport/TotalAudioAvailable").InnerText, out totAudioAvail);
                        if (totAudioAvail < 0)
                            totAudioAvail = 0;

                        LblAudioavail.Text = totAudioAvail.ToString();
                    }

                    foreach (XmlNode node in nodes)
                    {
                        xtr = new XmlTextReader(node.OuterXml, XmlNodeType.Element, new XmlParserContext(null, null, null, XmlSpace.None));
                        ds.ReadXml(xtr, XmlReadMode.InferSchema);
                    }

                    DataTable dt = new DataTable();
                    if (ds.Tables.Count > 0)
                        dt = ds.Tables[0];

                    for (Int32 i = 0; i < dt.Rows.Count; i++)
                    {
                        DateTime startDate = Convert.ToDateTime(dt.Rows[i]["StartDate"].ToString());
                        DateTime endDate = Convert.ToDateTime(dt.Rows[i]["EndDate"].ToString());

                        dt.Rows[i]["StartDate"] =  myVRMNet.NETFunctions.GetFormattedDate(startDate.Date) + " "
                            + myVRMNet.NETFunctions.GetFormattedTime(startDate.TimeOfDay.ToString(), Session["timeFormat"].ToString());

                        dt.Rows[i]["EndDate"] = myVRMNet.NETFunctions.GetFormattedDate(endDate.Date) + " "
                            + myVRMNet.NETFunctions.GetFormattedTime(endDate.TimeOfDay.ToString(), Session["timeFormat"].ToString());
                    }

                    dgUsageReport.DataSource = dt;
                    dgUsageReport.DataBind();

                    if (dt.Rows.Count > 0)
                        lblDetails.Visible = false;
                    else
                        lblDetails.Visible = true;

                    ClientScript.RegisterStartupScript(this.GetType(), "DefaultKey", "<script>fnShowHide(2);</script>");
                    chkExpandCollapse.Checked = true;
                    
                }
                else
                {
                    errLabel.Text = obj.ShowErrorMessage(outXML);
                    errLabel.Visible = true;
                }
            }
            catch (Exception ex)
            {
                log.Trace(ex.StackTrace + " : " + ex.Message);
            }
        }

        #endregion

        //FB 2591 Starts -- GetProfiles click event
        #region
        protected void GetProfiles(object sender, EventArgs e)
        {
            BindProfiles();

        }
        #endregion


        //FB 2591 BindMCUProfiles to dropdwnlist txtProfileId
        #region
        private void BindProfiles()
        {
            String outXML = "";
            String inXML = "";
            XmlDocument xmlDoc = null;
            DataSet ds = null;
            try
            {
                inXML = "<GetMCUProfiles>";
                inXML += "  <userID>" + Session["userID"].ToString() + "</userID>";
                inXML += "  <organizationID>" + Session["OrganizationID"].ToString() + "</organizationID>";
                inXML += "  <MCUId>" + txtMCUID.Text + "</MCUId>";
                inXML += "</GetMCUProfiles>";

                outXML = obj.CallCOM2("GetMCUProfiles", inXML.ToString(), Application["RTC_ConfigPath"].ToString());
                if (outXML.IndexOf("<error>") >= 0)
                {
                    errLabel.Text = obj.ShowErrorMessage(outXML);
                    errLabel.Visible = true;
                }
                else
                {
                    outXML = obj.CallMyVRMServer("GetMCUProfiles", inXML.ToString(), Application["MyVRMServer_ConfigPath"].ToString());

                    if (outXML.IndexOf("<error>") >= 0)
                    {
                        errLabel.Text = obj.ShowErrorMessage(outXML);
                        errLabel.Visible = true;
                    }
                    else
                    {
                        xmlDoc = new XmlDocument();
                        xmlDoc.LoadXml(outXML);
                        ds = new DataSet();
                        ds.ReadXml(new XmlNodeReader(xmlDoc));
                        XmlNodeList nodes = xmlDoc.SelectNodes("//GetMCUProfiles/Profile");
                        // FB 2441 II Starts //ZD 103787 - It s for binding the profiles for RMX MCU
                        if (lstMCUType.SelectedValue.Equals("8") || lstMCUType.SelectedValue.Equals("17") || lstMCUType.SelectedValue.Equals("18") || lstMCUType.SelectedValue.Equals("19"))
                        {
                            txtProfileID.Items.Clear(); //FB 2876 Starts
                            if (nodes.Count > 0)
                            {
                                if (nodes.Count > 0)
                                    obj.LoadList(txtProfileID, nodes, "ID", "Name");
                            }
                            else
                                txtProfileID.Items.Insert(0, new ListItem(obj.GetTranslatedText("None"), "0")); //FB 2876 Ends
                           
                        }
                        if (lstMCUType.SelectedValue.Equals("13"))//FB 2441
                        {
                            lstTemplate.Items.Clear();
                            if (nodes.Count > 0)
                            {
                                LoadList(lstTemplate, nodes);
                                lstTemplate.Items.Insert(0, new ListItem(obj.GetTranslatedText("None"), "0")); //FB 2876 
                            }
                        }
						// FB 2441 II Ends
                    }
                }
            }
            catch (Exception ex)
            {
                log.Trace(ex.StackTrace + " : " + ex.Message);
            }
        }
        #endregion

        //FB 2591 Ends
		

        //FB 2556 - Starts
        #region GetMCUService
        /// <summary>
        /// GetMCUService
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void GetMCUService(object sender, EventArgs e)
        {
            BindExtMCUService();

        }
        #endregion

        #region GetMCUSilo
        /// <summary>
        /// GetMCUSilo
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void GetMCUSilo(object sender, EventArgs e)
        {
            BindExtMCUSilo();

        }
        #endregion

        #region BindExtMCUService
        /// <summary>
        /// BindExtMCUService
        /// </summary>
        private void BindExtMCUService()
        {
            String outXML = "";
            StringBuilder inXML = new StringBuilder();
            XmlDocument xmlDoc = null;
            DataSet ds = null;
            try
            {
                inXML.Append("<GetExtMCUService>");
                inXML.Append("<userID>" + Session["userID"].ToString() + "</userID>");
                inXML.Append("<organizationID>" + Session["OrganizationID"].ToString() + "</organizationID>");
                inXML.Append("<BridgeId>" + txtMCUID.Text + "</BridgeId>");
                inXML.Append("<BridgeTypeId>" + lstMCUType.SelectedValue + "</BridgeTypeId>");
                inXML.Append("<MemberId>" + lstScopiaOrg.SelectedValue + "</MemberId>");
                inXML.Append("</GetExtMCUService>");

                outXML = obj.CallCOM2("GetExtMCUServices", inXML.ToString(), Application["RTC_ConfigPath"].ToString());
                if (outXML.IndexOf("<error>") >= 0)
                {
                    errLabel.Text = obj.ShowErrorMessage(outXML);
                    errLabel.Visible = true;
                }
                else
                {
                    outXML = obj.CallMyVRMServer("GetExtMCUServices", inXML.ToString(), Application["MyVRMServer_ConfigPath"].ToString());

                    if (outXML.IndexOf("<error>") >= 0)
                    {
                        errLabel.Text = obj.ShowErrorMessage(outXML);
                        errLabel.Visible = true;
                    }
                    else
                    {
                        xmlDoc = new XmlDocument();
                        xmlDoc.LoadXml(outXML);
                        ds = new DataSet();
                        ds.ReadXml(new XmlNodeReader(xmlDoc));
                        XmlNodeList nodes = xmlDoc.SelectNodes("//GetExtMCUServices/GetExtMCUService");
                        lstScopiaService.Items.Clear();
                        if (nodes.Count > 0)
                        {
                            LoadList(lstScopiaService, nodes);
                            lstScopiaService.Items.Insert(0, obj.GetTranslatedText("None"));
                        }
                        errLabel.Text = obj.ShowSuccessMessage();//FB 2659
                        errLabel.Visible = true;//FB 2659
                    }
                }
            }
            catch (Exception ex)
            {
                log.Trace("BindExtMCUService" + ex.Message);
            }
        }
        #endregion

        #region BindExtMCUSilo
        /// <summary>
        /// BindExtMCUSilo
        /// </summary>
        private void BindExtMCUSilo()
        {
            String outXML = "";
            StringBuilder inXML = new StringBuilder();
            XmlDocument xmlDoc = null;
            DataSet ds = null;
            try
            {
                inXML.Append("<GetExtMCUSilo>");
                inXML.Append("<userID>" + Session["userID"].ToString() + "</userID>");
                inXML.Append("<organizationID>" + Session["OrganizationID"].ToString() + "</organizationID>");
                inXML.Append("<BridgeId>" + txtMCUID.Text + "</BridgeId>");
                inXML.Append("<BridgeTypeId>" + lstMCUType.SelectedValue + "</BridgeTypeId>");
                inXML.Append("</GetExtMCUSilo>");

                outXML = obj.CallCOM2("GetExtMCUSilo", inXML.ToString(), Application["RTC_ConfigPath"].ToString());
                if (outXML.IndexOf("<error>") >= 0)
                {
                    errLabel.Text = obj.ShowErrorMessage(outXML);
                    errLabel.Visible = true;
                }
                else
                {
                    outXML = obj.CallMyVRMServer("GetExtMCUSilo", inXML.ToString(), Application["MyVRMServer_ConfigPath"].ToString());

                    if (outXML.IndexOf("<error>") >= 0)
                    {
                        errLabel.Text = obj.ShowErrorMessage(outXML);
                        errLabel.Visible = true;
                    }
                    else
                    {
                        xmlDoc = new XmlDocument();
                        xmlDoc.LoadXml(outXML);
                        ds = new DataSet();
                        ds.ReadXml(new XmlNodeReader(xmlDoc));
                        XmlNodeList nodes = xmlDoc.SelectNodes("//GetExtMCUSilos/GetExtMCUSilo");
                        lstScopiaOrg.Items.Clear();
                        if (nodes.Count > 0)
                        {
                            LoadList(lstScopiaOrg, nodes);
                            lstScopiaOrg.Items.Insert(0, obj.GetTranslatedText("None"));
                        }
                        errLabel.Text = obj.ShowSuccessMessage();//FB 2659
                        errLabel.Visible = true;//FB 2659
                    }
                }
            }
            catch (Exception ex)
            {
                log.Trace("BindExtMCUSilo" + ex.Message);
            }
        }
        #endregion
        //FB 2556 - End
        protected string UserPassword(string PW)
        {
            string Encrypted = "";
            XmlDocument docs = null;
            try
            {
                string inxmls = "<System><Cipher>" + PW + "</Cipher></System>";

                string outXML = obj.CallMyVRMServer("GetEncrpytedText", inxmls, Application["MyVRMServer_ConfigPath"].ToString());

                if (outXML.IndexOf("<error>") < 0)
                {
                    docs = new XmlDocument();
                    docs.LoadXml(outXML);
                    XmlNode nde = docs.SelectSingleNode("System/Cipher");
                    if (nde != null)
                        Encrypted = nde.InnerXml;
                }
                else
                {
                    errLabel.Text = obj.ShowErrorMessage(outXML);
                }
            }
            catch (Exception ex)
            {
                log.Trace("UserPassword:" + ex.Message);//ZD 100263
            }
            return Encrypted;
        }

        #region CheckUserEmail
        /// <summary>
        /// CheckUserEmail
        /// </summary>
        /// <param name="strEmailAddress"></param>
        /// <param name="errorEmail"></param>
        /// <returns></returns>
        protected bool CheckUserEmail(string strEmailAddress, ref string errorEmail)
        {
            string[] emails = null; //104480 start
            string emailaddress = "";//ZD 103856
            string EmailaddressValidation = @"^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}" +
         @"\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\" +
         @".)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$";
            Regex Checkemailaddress = new Regex(EmailaddressValidation);
            try
            {
                if (string.IsNullOrEmpty(strEmailAddress))
                    return true;

                emails = strEmailAddress.Split(';');
                for (int cnt = 0; cnt < emails.Length; cnt++)
                {
                    emailaddress = emails[cnt].Trim();
                    if (string.IsNullOrEmpty(emailaddress.Trim()))
                        continue;
                    try
                    {
                        if (Checkemailaddress.IsMatch(emailaddress))
                            continue;
                        else
                        {
                            if (errorEmail == "")
                                errorEmail = emailaddress.Trim();
                            else
                                errorEmail = errorEmail + ";" + emailaddress.Trim();
                        }
                    }

                    catch (Exception e)
                    {
                        return false;

                    }
                }

                if (!Checkemailaddress.IsMatch(strEmailAddress))
                    return false;
            }
            catch (Exception ex)
            {
                return false;
            }

            return true; //104480 End
        }
        #endregion

        //ZD 101026 Starts
        #region LoadHistory
        /// <summary>
        /// LoadHistory
        /// </summary>
        /// <param name="changedHistoryNodes"></param>
        /// <returns></returns>
        private bool LoadHistory(XmlNodeList changedHistoryNodes)
        {
            if (obj == null)
                obj = new myVRMNet.NETFunctions();
            try
            {
                DataTable dtable = new DataTable();
                if (!dtable.Columns.Contains("ModifiedUserId"))
                    dtable.Columns.Add("ModifiedUserId");
                if (!dtable.Columns.Contains("ModifiedDateTime"))
                    dtable.Columns.Add("ModifiedDateTime");
                if (!dtable.Columns.Contains("ModifiedUserName"))
                    dtable.Columns.Add("ModifiedUserName");
                if (!dtable.Columns.Contains("Description"))
                    dtable.Columns.Add("Description");

                if (changedHistoryNodes != null && changedHistoryNodes.Count > 0)
                {
                    dtable = obj.LoadDataTable(changedHistoryNodes, null);
                    DateTime ModifiedDateTime = DateTime.Now;

                    for (Int32 i = 0; i < dtable.Rows.Count; i++)
                    {
                        DateTime.TryParse(dtable.Rows[i]["ModifiedDateTime"].ToString(), out ModifiedDateTime);
                        dtable.Rows[i]["ModifiedDateTime"] = myVRMNet.NETFunctions.GetFormattedDate(ModifiedDateTime.Date.ToString()) + " " + ModifiedDateTime.ToString(tformat);

                        dtable.Rows[i]["Description"] = obj.GetTranslatedText(dtable.Rows[i]["Description"].ToString());

                    }
                }
                dgChangedHistory.DataSource = dtable;
                dgChangedHistory.DataBind();

            }
            catch (Exception ex)
            {
                log.Trace("LoadHistory" + ex.Message);
                return false;
            }
            return true;
        }
        #endregion
        //ZD 101026 End
		//ZD 101522 Starts
        #region BindSystemLocation
        /// <summary>
        /// BindSystemLocation
        /// </summary>
        protected void BindSystemLocation(object sender, EventArgs e)
        {
            string outXML = "";
            StringBuilder inXML = new StringBuilder();
            XmlDocument xmlDoc = null;
            DataSet ds = null;
            try
            {
                inXML.Append("<GetSystemLocation>");
                inXML.Append("<userID>" + Session["userID"].ToString() + "</userID>");
                inXML.Append("<organizationID>" + Session["OrganizationID"].ToString() + "</organizationID>");
                inXML.Append("<BridgeId>" + txtMCUID.Text + "</BridgeId>");
                inXML.Append("<BridgeTypeId>" + lstMCUType.SelectedValue + "</BridgeTypeId>");
                inXML.Append("</GetSystemLocation>");

                outXML = obj.CallCOM2("FetchSystemLocation", inXML.ToString(), Application["RTC_ConfigPath"].ToString());
                if (outXML.IndexOf("<error>") >= 0)
                {
                    errLabel.Text = obj.ShowErrorMessage(outXML);
                    errLabel.Visible = true;
                }
                else
                {
                    outXML = obj.CallMyVRMServer("GetSystemLocation", inXML.ToString(), Application["MyVRMServer_ConfigPath"].ToString());

                    if (outXML.IndexOf("<error>") >= 0)
                    {
                        errLabel.Text = obj.ShowErrorMessage(outXML);
                        errLabel.Visible = true;
                    }
                    else
                    {
                        xmlDoc = new XmlDocument();
                        xmlDoc.LoadXml(outXML);
                        ds = new DataSet();
                        ds.ReadXml(new XmlNodeReader(xmlDoc));
                        XmlNodeList nodes = xmlDoc.SelectNodes("//GetSystemLocations/GetSystemLocation");
                        lstSystemLocation.Items.Clear();
                        lstSystemLocation.Items.Insert(0, new ListItem(obj.GetTranslatedText("None"), "0"));
                        if (nodes.Count > 0)
                            LoadList(lstSystemLocation, nodes);
                        errLabel.Text = obj.ShowSuccessMessage();
                        errLabel.Visible = true;
                    }
                }
            }
            catch (Exception ex)
            {
                log.Trace("BindSystemLocation" + ex.Message);
            }
        }
        #endregion
		//ZD 101522 End
       
        //ZD 100040 - Start
        protected void LoadVideoPortGrid(XmlNodeList nodes, int x)
        {
            try
            {
                XmlTextReader xtr;
                DataSet ds = new DataSet();

                foreach (XmlNode node in nodes)
                {
                    xtr = new XmlTextReader(node.OuterXml, XmlNodeType.Element, new XmlParserContext(null, null, null, XmlSpace.None));
                    ds.ReadXml(xtr, XmlReadMode.InferSchema);
                }
                DataTable dt = new DataTable();                
                
                if (ds.Tables.Count > 0)
                {
                    dt = ds.Tables[0];

                    if (x == 1)
                    {
                        if (!dt.Columns.Contains("resolutionId"))
                            dt.Columns.Add("resolutionId");
                        if (!dt.Columns.Contains("resolution"))
                            dt.Columns.Add("resolution");
                        if (!dt.Columns.Contains("qty"))
                            dt.Columns.Add("qty");

                        dgVideoPort.DataSource = dt;
                        dgVideoPort.DataBind();
                    }
                    else if(x == 2)
                    {
                        if (!dt.Columns.Contains("bridgeId"))
                            dt.Columns.Add("bridgeId");
                        if (!dt.Columns.Contains("bridgeName"))
                            dt.Columns.Add("bridgeName");
                        if (!dt.Columns.Contains("loadBalance"))
                            dt.Columns.Add("loadBalance");

                        dgloadBalance.DataSource = dt;
                        dgloadBalance.DataBind();
                    }
                }
            }
            catch (Exception ex)
            {
                log.Trace("LoadVideoPortGrid" + ex.Message);
                errLabel.Text = obj.ShowSystemMessage();
                errLabel.Attributes.Add("style", "display:block");
            }
        }       
        //ZD 100040 - End
		//ZD 104256 Starts

        protected void GetPoolOrders(object sender, EventArgs e)
        {
            BindPoolOrders();

        }

        #region BindPoolOrder
        /// <summary>
        /// BindPoolOrder
        /// </summary>
        private void BindPoolOrders()
        {
            string outXML = "", inXML = "";
            XmlDocument xmlDoc = null;
            DataSet ds = null;
            try
            {
                inXML = "<GetMCUPoolOrders>";
                inXML += "  <userID>" + Session["userID"].ToString() + "</userID>";
                inXML += "  <organizationID>" + Session["OrganizationID"].ToString() + "</organizationID>";
                inXML += "  <MCUId>" + txtMCUID.Text + "</MCUId>";
                inXML += "</GetMCUPoolOrders>";

                outXML = obj.CallCOM2("GetMCUPoolOrders", inXML.ToString(), Application["RTC_ConfigPath"].ToString());
                if (outXML.IndexOf("<error>") >= 0)
                {
                    errLabel.Text = obj.ShowErrorMessage(outXML);
                    errLabel.Visible = true;
                }
                else
                {
                    outXML = obj.CallMyVRMServer("GetMCUPoolOrders", inXML.ToString(), Application["MyVRMServer_ConfigPath"].ToString());

                    if (outXML.IndexOf("<error>") >= 0)
                    {
                        errLabel.Text = obj.ShowErrorMessage(outXML);
                        errLabel.Visible = true;
                    }
                    else
                    {
                        xmlDoc = new XmlDocument();
                        xmlDoc.LoadXml(outXML);
                        ds = new DataSet();
                        ds.ReadXml(new XmlNodeReader(xmlDoc));
                        XmlNodeList nodes = xmlDoc.SelectNodes("//GetMCUPoolOrders/PoolOrder");
                        if (lstMCUType.SelectedValue.Equals("13"))
                        {
                            drpPoolOrder.Items.Clear();
                            if (nodes.Count > 0)
                                obj.LoadList(drpPoolOrder, nodes, "Id", "Name");
                            else
                                drpPoolOrder.Items.Insert(0, new ListItem(obj.GetTranslatedText("None"), "0"));
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                log.Trace(ex.StackTrace + " : " + ex.Message);
            }
        }
        #endregion
        //ZD 104256 Ends
    }
}
