﻿/* Copyright (C) 2015 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml;
using DevExpress.Web;
using DevExpress.Web.ASPxGridView;
using DevExpress.Web.ASPxEditors;
using System.Text;

namespace ns_MyVRM
{
    public partial class ExchangeLookup : System.Web.UI.Page
    {
        #region private variable

        myVRMNet.NETFunctions obj;
        ns_Logger.Logger log;
        String partystring = "";
        bool bParty = false;

        #endregion

        #region Protected Variables

        protected System.Web.UI.WebControls.TextBox txtSearch;
        protected DevExpress.Web.ASPxGridView.ASPxGridView grid;
        protected System.Web.UI.HtmlControls.HtmlInputHidden nonEWSuser;

        #endregion

        #region InitializeCulture
        protected override void InitializeCulture()
        {
            if (Session["UserCulture"] != null)
            {
                UICulture = Session["UserCulture"].ToString();
                Culture = Session["UserCulture"].ToString();
                base.InitializeCulture();
            }
        }
        #endregion

        #region Constructor
        public ExchangeLookup()
        {
            obj = new myVRMNet.NETFunctions();
            log = new ns_Logger.Logger();
        }
        #endregion

        #region Page_Load
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (obj == null)
                    obj = new myVRMNet.NETFunctions();
                obj.AccessandURLConformityCheck("AudioparticipantList.aspx", Request.Url.AbsoluteUri.ToLower());
                log = new ns_Logger.Logger();

                if (Request.QueryString["partys"] != null)
                    partystring = Request.QueryString["partys"].ToString().Trim().Replace("@@", "++");

                bParty = false;

                if (ViewState["dirState"] != null)
                {
                    DataTable dt = new DataTable();                    
                    dt = (DataTable)ViewState["dirState"];
                    grid.DataSource = dt;
                    grid.DataBind();
                }


            }
            catch (Exception ex)
            {

                log.Trace(ex.ToString());
            }

        }
        #endregion

        #region SearchEWSUser
        protected void SearchEWSUser(Object sender, EventArgs e)
        {
            try
            {
                StringBuilder inXML = new StringBuilder();
                XmlDocument xmldoc = new XmlDocument();
                DataSet ds = new DataSet();
                DataTable dt = new DataTable();
                XmlNodeList nodes = null;
                XmlTextReader xtr;

                inXML.Append("<GetEWSUser>");
                inXML.Append(obj.OrgXMLElement());
                inXML.Append("<loginID>" + Session["userID"].ToString() + "</loginID>");
                inXML.Append("<Searchtext>" + txtSearch.Text + "</Searchtext>");
                inXML.Append("</GetEWSUser>");

                string outXML = obj.CallMyVRMServer("GetEWSUser", inXML.ToString(), Application["MyVRMServer_ConfigPath"].ToString());

                if (outXML.IndexOf("<error>") < 0 && outXML != "")
                {
                    xmldoc.LoadXml(outXML);
                    nodes = xmldoc.SelectNodes("//GetEWSUser/EWSUser");

                    foreach (XmlNode node in nodes)
                    {
                        xtr = new XmlTextReader(node.OuterXml, XmlNodeType.Element, new XmlParserContext(null, null, null, XmlSpace.None));
                        ds.ReadXml(xtr, XmlReadMode.InferSchema);
                    }
                }

                if (ds.Tables.Count == 0)
                {
                    if (!dt.Columns.Contains("LoginName")) dt.Columns.Add("Login");
                    if (!dt.Columns.Contains("Email")) dt.Columns.Add("Email");
                    if (!dt.Columns.Contains("Select")) dt.Columns.Add("Select");
                    if (!dt.Columns.Contains("ifrmDetails")) dt.Columns.Add("ifrmDetails");
                }
                else if (ds.Tables.Count > 0)
                {
                    dt = ds.Tables[0];
                    if (!dt.Columns.Contains("ifrmDetails")) dt.Columns.Add("ifrmDetails");

                    foreach (DataRow dr in dt.Rows)
                    {
                        dr["ifrmDetails"] = dr["LoginName"].ToString() + "|" + dr["Email"].ToString();
                    }
                }
                bParty = true;
                grid.DataSource = dt;
                grid.DataBind();

                ViewState["dirState"] = dt;
            }
            catch (Exception ex)
            {
                log.Trace(ex.StackTrace + " : " + ex.Message);
            }
        }
        #endregion

        #region ASPxGridView1_DataBound
        protected void ASPxGridView1_DataBound(object sender, EventArgs e)
        {
            bool isuserpresent = false;
            try
            {
                String[] delimiter = { "!!" };
                String[] delimiter1 = { "||" };
                if (bParty == true)
                {
                    if (partystring != "")
                    {
                        string[] partys = partystring.Split(delimiter1, StringSplitOptions.RemoveEmptyEntries);

                        for (int j = 0; j < partys.Length; j++)
                        {
                            isuserpresent = false;
                            string temp = partys[j].ToString();
                            string tmpEmail = "";
                            if (temp != "")
                            {
                                for (int i = 0; i < this.grid.VisibleRowCount; i++)
                                {
                                    tmpEmail = "";
                                    tmpEmail = temp.Split(delimiter, StringSplitOptions.RemoveEmptyEntries)[3];
                                    if (this.grid.GetRowValues(i, "Email") != null)
                                    {

                                        if (this.grid.GetRowValues(i, "Email").ToString() == tmpEmail)
                                        {
                                            this.grid.Selection.SelectRow(i);
                                            isuserpresent = true;
                                        }
                                    }
                                }

                                if (!isuserpresent)
                                {

                                    if (!nonEWSuser.Value.Contains(temp))
                                        nonEWSuser.Value += temp + "||";
                                }
                            }

                        }
                    }
                }
            }
            catch (Exception ex)
            {

                log.Trace(ex.ToString());
            }
        }
        #endregion
    }
}
