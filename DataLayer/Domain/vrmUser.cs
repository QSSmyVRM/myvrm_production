/* Copyright (C) 2015 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
//ZD 100147 ZD 100886
using System;
using System.Collections;
using System.Globalization;
using System.Collections.Generic;

namespace myVRM.DataLayer
{
    /// <summary>
    /// Summary description for user table Usr_List_D 
    /// guest Usr_Guest_D and inactive Usr_Inactive_D).
    /// </summary>

    public static class vrmUserConstant
    {
        // universal vrmAdmin user id
        public static int vrmAdminId = 11;
        
        // role constants from VRM[COM]
        public const int UNKNOWN_ROLE = 0;
        public const int SUPER_ADMIN = 2;
        public const int ADMIN = 1;
        public const int CA_SPECIALIST = 4; // food assistant;
        public const int AV_SPECIALIST = 5; // room assistant (AV);
        public const int HK_SPECIALIST = 6; // room assistant (HK);
        public const int CUSTOM = 7;
        public const int VNOCADMIN = 3; //FB 2670

        public const int TYPE_USER = 1;
        public const int TYPE_GUEST = 2;
        public const int TYPE_INACTIVE= 3;

    }
    public static class vrmUserStatus
    {
            /* Lock Status */
        public const int USER_ACTIVE = 0;
        public const int AUTO_LOCKED = 2;
        public const int MAN_LOCKED	 = 3;
        public const int DATAIMPORT_LOCKED = 4;//ZD 104862
        /* Deletion Status */
        public const int USER_INACTIVE = 1;
        public const int USER_DELETED  = 2;
    }
    public static class vrmUserAction
    {
        public const int USER_STATUS_ACTIVE  = 1;
        public const int USER_STATUS_DELETE  = -1;
        public const int USER_STATUS_LOCK    = -2;
        public const int USER_STATUS_UNLOCK  = 2;
        public const int USER_GUEST_DELETE   = -3;
        public const int USER_GUEST_UNDELETE = 3;
    }
    public static class vrmPCVendor
    {
        public const int BlueJeans = 1;
        public const int Jabber = 2;
        public const int Lync = 3;
        public const int Vidtel = 4;
        public const int Vidyo = 5;
    }
    public class vrmBaseUser
    {
        #region Private Internal Members

        private int m_userId, m_Id, m_enableExchange, m_enableDomino, m_enableMobile, m_enablePCUser, m_enableWebexUser;//Edited for License modification FB 1979 //FB 2693 //ZD 100221
        private string m_FirstName, m_LastName, m_Email, m_RFIDValue, m_WebexUserName, m_WebexPassword,m_RequestID, m_LandingPageTitle, m_LandingPageLink; //FB 2724 //ZD 100263 // ZD 100172
        private int m_Financial, m_Admin, m_LastModifiedUser;//ZD 100664
        private string m_Login, m_Password, m_Company;
        private int m_TimeZone, m_Language;//
        private String m_PreferedRoom;//Room Search
        private string m_AlternativeEmail;
        private int m_DoubleEmail, m_PreferedGroup, m_CCGroup;
        private string m_Telephone;
        //FB 2027 Start
        private int m_enableAV, m_enableParticipants, m_TickerStatus, m_TickerSpeed, m_TickerPosition;
        private int m_enableAVWorkOrder, m_enableCateringWO, m_enableFacilityWO; //ZD 101122
        private int m_TickerDisplay, m_TickerStatus1, m_TickerSpeed1, m_TickerPosition1, m_TickerDisplay1;
        private string m_TickerBackground, m_TickerBackground1, m_RSSFeedLink, m_RSSFeedLink1;
        //FB 2027 End
        private int m_RoomID, m_PreferedOperator, m_lockCntTrns, m_DefLineRate;
        private int m_DefVideoProtocol, m_Deleted, m_DefAudio, m_DefVideoSession;
        private string m_MenuMask;
        private int m_initialTime, m_EmailClient, m_newUser, m_PrefTZSID;
        private string m_IPISDNAddress;
        private int m_DefaultEquipmentId, m_connectionType, m_companyId;
        private string m_securityKey;
        private int m_LockStatus;
        private DateTime m_LastLogin = DateTime.Today; //FB 1939;
        private int m_outsidenetwork, m_emailmask, m_roleID, m_addresstype;
        private DateTime m_accountexpiry;
        private int m_approverCount, m_BridgeID;
        private string m_Title;
        private int m_LevelID;
        private int m_preferredISDN, m_portletId, m_userType, m_endpointId, m_searchId;
        private string m_Audioaddon, m_WorkPhone, m_CellPhone, m_ConferenceCode, m_LeaderPin; //FB 1642-Audio Add On
        private int m_DefaultTemplate; //FB 2027
        private int m_EmailLangId; //FB 1830
        private IList<vrmAccount> m_AccountList;
        private IList<vrmEndPoint> m_EndPointList;
        private string m_DateFormat,m_TimeFormat,m_TimezoneDisplay;
        private int m_mailBlocked;
        private DateTime m_mailBlockedDate = DateTime.Today; //FB 1922
        private int m_PluginConfirmations;//FB 2141
        private string m_InternalVideoNumber, m_ExternalVideoNumber;//FB 2227
        private string m_HelpReqEmailID, m_HelpReqPhone; //FB 2268
        private int m_SendSurveyEmail;//FB 2348
        private string m_PrivateVMR;//FB 2481
        private int m_VNOCoperator;//FB 2608
		private string m_VidyoURL, m_Extension, m_Pin, m_Type; //FB 2262 //FB 2599
        private int m_EntityID, m_MemberID, m_isLocked, m_allowPersonalMeeting, m_allowCallDirect;
        private int m_ParentReseller, m_CorpUser ; //FB 2392-Whygo
        private int m_Region, m_ESUserID; //FB 2392-Whygo
        private int m_Secured;//FB 2595
        //FB 2655 - DTMF
        private string m_PreConfCode, m_PreLeaderPin, m_PostLeaderPin, m_AudioDialInPrefix;
        private string m_BrokerRoomNum; //FB 3001 
        private DateTime m_RoomCalEndime, m_PerCalStartTime, m_PerCalEndTime, m_RoomCalStartTime; //ZD 100157
        int m_PerCalShowHours, m_RoomCalShowHours; //ZD 100157
        private string m_staticId, m_ExternalUserId; //ZD 100890
        private int m_isStaticIDEnabled = 0,m_IsPasswordReset = 0;//ZD 100890 //ZD 100781
        private DateTime m_PasswordTime; //ZD 100781
        private DateTime m_LastModifiedDate = DateTime.UtcNow;//ZD 100664
        private int m_RoomViewType, m_RoomRecordsView;//ZD 100621 //ZD 101175
        private int m_enableAdditionalOption;//ZD 101093 
        private int m_custAdminRights; // ZD 101233
        private int m_usrorgin, m_BJNUserId, m_EnableBJNConf;//ZD 101865 //ZD 104021
        private string m_ParticipantCode, m_UserRolename, m_BJNUserName, m_BJNMeetingId, m_BJNUserEmail, m_BJNPassCode, m_BJNPartyPassCode; //ZD 101446 //ZD 103405 //ZD 104021
		private string m_UserDomain; //ZD 103784
        private string m_PexipStaticVMR;//ALLDEV 782
        private string m_PexipAliasURI; //ALLDEV-782
        #endregion

        #region Public Properties
        public int id
        {
            get { return m_Id; }
            set { m_Id = value; }
        }
   
        public int userid
        {
            get { return m_userId; }
            set { m_userId = value; }
        }
        public string FirstName
        {
            get { return m_FirstName; }
            set { m_FirstName = value; }
        }
        public string LastName
        {
            get { return m_LastName; }
            set { m_LastName = value; }
        }
        public string Email
        {
            get { return m_Email; }
            set { m_Email = value; }
        }
        public int Financial
        {
            get { return m_Financial; }
            set { m_Financial = value; }
        }
        public int Admin
        {
            get { return m_Admin; }
            set { m_Admin = value; }
        }
        public string Login
        {
            get { return m_Login; }
            set { m_Login = value; }
        }
        public string Password
        {
            get { return m_Password; }
            set { m_Password = value; }
        }
        public string Company
        {
            get { return m_Company; }
            set { m_Company = value; }
        }
        public int TimeZone
        {
            get { return m_TimeZone; }
            set { m_TimeZone = value; }
        }
        public int Language
        {
            get { return m_Language; }
            set { m_Language = value; }
        }
        public String PreferedRoom//Room Search
        {
            get { return m_PreferedRoom; }
            set { m_PreferedRoom = value; }
        }
        public string AlternativeEmail
        {
            get { return m_AlternativeEmail; }
            set { m_AlternativeEmail = value; }
        }
        public int DoubleEmail
        {
            get { return m_DoubleEmail; }
            set { m_DoubleEmail = value; }
        }
        public int PreferedGroup
        {
            get { return m_PreferedGroup; }
            set { m_PreferedGroup = value; }
        }
        public int CCGroup
        {
            get { return m_CCGroup; }
            set { m_CCGroup = value; }
        }
        public string Telephone
        {
            get { return m_Telephone; }
            set { m_Telephone = value; }
        }
        public int RoomID
        {
            get { return m_RoomID; }
            set { m_RoomID = value; }
        }
        public int PreferedOperator
        {
            get { return m_PreferedOperator; }
            set { m_PreferedOperator = value; }
        }
        public int lockCntTrns
        {
            get { return m_lockCntTrns; }
            set { m_lockCntTrns = value; }
        }
        public int DefLineRate
        {
            get { return m_DefLineRate; }
            set { m_DefLineRate = value; }
        }
        public int DefVideoProtocol
        {
            get { return m_DefVideoProtocol; }
            set { m_DefVideoProtocol = value; }
        }
        public int Deleted
        {
            get { return m_Deleted; }
            set { m_Deleted = value; }
        }
        public int DefAudio
        {
            get { return m_DefAudio; }
            set { m_DefAudio = value; }
        }
        public int DefVideoSession
        {
            get { return m_DefVideoSession; }
            set { m_DefVideoSession = value; }
        }
        public string MenuMask
        {
            get { return m_MenuMask; }
            set { m_MenuMask = value; }
        }
        public int initialTime
        {
            get { return m_initialTime; }
            set { m_initialTime = value; }
        }
        public int EmailClient
        {
            get { return m_EmailClient; }
            set { m_EmailClient = value; }
        }
        public int newUser
        {
            get { return m_newUser; }
            set { m_newUser = value; }
        }
        public int PrefTZSID
        {
            get { return m_PrefTZSID; }
            set { m_PrefTZSID = value; }
        }
        public string IPISDNAddress
        {
            get { return m_IPISDNAddress; }
            set { m_IPISDNAddress = value; }
        }
        public int DefaultEquipmentId
        {
            get { return m_DefaultEquipmentId; }
            set { m_DefaultEquipmentId = value; }
        }
        public int connectionType
        {
            get { return m_connectionType; }
            set { m_connectionType = value; }
        }
        public int companyId
        {
            get { return m_companyId; }
            set { m_companyId = value; }
        }
        public string securityKey
        {
            get { return m_securityKey; }
            set { m_securityKey = value; }
        }
        public int LockStatus
        {
            get { return m_LockStatus; }
            set { m_LockStatus = value; }
        }
        public DateTime LastLogin
        {
            get { return m_LastLogin; }
            set
            {
                //FB 1939
                if (value.ToString().IndexOf("0001") > 0)
                    m_LastLogin = DateTime.Today;
                else
                    m_LastLogin = value;
            }
        }
        public int outsidenetwork
        {
            get { return m_outsidenetwork; }
            set { m_outsidenetwork = value; }
        }
        public int emailmask
        {
            get { return m_emailmask; }
            set { m_emailmask = value; }
        }
        public int roleID
        {
            get { return m_roleID; }
            set { m_roleID = value; }
        }
        public int addresstype
        {
            get { return m_addresstype; }
            set { m_addresstype = value; }
        }
        public DateTime accountexpiry
        {
            get { return m_accountexpiry; }
            //set { m_accountexpiry = value; } //ZD 100318
            set
            {
                if (value.ToString().IndexOf("0001") > 0)
                    m_accountexpiry = DateTime.Today;
                else
                    m_accountexpiry = value;
            }
        }
        public int approverCount
        {
            get { return m_approverCount; }
            set { m_approverCount = value; }
        }
        public int BridgeID
        {
            get { return m_BridgeID; }
            set { m_BridgeID = value; }
        }
        public string Title
        {
            get { return m_Title; }
            set { m_Title = value; }
        }
        public int LevelID
        {
            get { return m_LevelID; }
            set { m_LevelID = value; }
        }
        public int preferredISDN
        {
            get { return m_preferredISDN; }
            set { m_preferredISDN = value; }
        }

        public int portletId
        {
            get { return m_portletId; }
            set { m_portletId = value; }
        }
        public int userType
        {
            get { return m_userType; }
            set { m_userType = value; }
        }
        public int endpointId
        {
            get { return m_endpointId; }
            set { m_endpointId = value; }
        }
        public int searchId
        {
            get { return m_searchId; }
            set { m_searchId = value; }
        }
        //FB 1642-Audio Add On - Starts
        public string Audioaddon
        {
            get { return m_Audioaddon; }
            set { m_Audioaddon = value; }
        }
   
        public string WorkPhone
        {
            get { return m_WorkPhone; }
            set { m_WorkPhone = value; } 
        }

        public string CellPhone
        {
            get { return m_CellPhone; }
            set { m_CellPhone = value; }
        }
        //ZD 100621 start
        public int RoomViewType
        {
            get { return m_RoomViewType; }
            set { m_RoomViewType = value; }
        }
        //ZD 100621 End
        //ZD 101175 Start
        public int RoomRecordsView
        {
            get { return m_RoomRecordsView; }
            set { m_RoomRecordsView = value; }
        }
        //ZD 101175 end
        public string ConferenceCode
        {
            get { return m_ConferenceCode; }
            set { m_ConferenceCode = value; }
        }
        //ZD 100664
        public int LastModifiedUser
        {
            get { return m_LastModifiedUser; }
            set { m_LastModifiedUser = value; }
        }
        public DateTime LastModifiedDate
        {
            get { return m_LastModifiedDate; }
            set { m_LastModifiedDate = value; }
        }  
        
        public string LeaderPin
        {
            get { return m_LeaderPin; }
            set { m_LeaderPin = value; }
        }
        //FB 1642-Audio Add On - End

        public int DefaultTemplate //FB 2027
        {
            get { return m_DefaultTemplate; }
            set { m_DefaultTemplate = value; }
        }

        public int EmailLangId  //FB 1830
        {
            get { return m_EmailLangId; }
            set { m_EmailLangId = value; }
        }

        public int custAdminRights  //FB ZD 101233
        {
            get { return m_custAdminRights; }
            set { m_custAdminRights = value; }
        }

        public IList<vrmAccount> AccountList
        {
            get { return m_AccountList; }
            set { m_AccountList = value; }
        }
        public IList<vrmEndPoint> EndPointList
        {
            get { return m_EndPointList; }
            set { m_EndPointList = value; }
        }

        public vrmEndPoint EndPoint
        {
            get
            {
                if (m_EndPointList.Count < 1)
                {
                    vrmEndPoint ep = new vrmEndPoint();
                    m_EndPointList.Add(ep);
                }
                return m_EndPointList[0]; 
            }
            set
            {
                if (m_EndPointList.Count < 1)            
                    m_EndPointList.Add(value);
                 else
                    m_EndPointList[0] = value; 
            }
        }
        public string DateFormat    //PSU - Client Error code 200 
        {
            get 
            {
                if (m_DateFormat == null)
                    m_DateFormat = "MM/dd/yyyy";

                if (m_DateFormat.Trim() == "")
                    m_DateFormat = "MM/dd/yyyy";

                return m_DateFormat; 
            }
            set { m_DateFormat = value; }
        }
        public string TimeFormat    //PSU - Client Error code 200 
        {
            get 
            {
                if (m_TimeFormat == null)
                    m_TimeFormat = "1";

                if (m_TimeFormat.Trim() == "")
                    m_TimeFormat = "1";

                return m_TimeFormat;
            }
            set { m_TimeFormat = value; }
        }
        public string Timezonedisplay   //PSU - Client Error code 200 
        {
            get 
            {
                if (m_TimezoneDisplay == null)
                    m_TimezoneDisplay = "1";

                if (m_TimezoneDisplay.Trim() == "")
                    m_TimezoneDisplay = "1";

                return m_TimezoneDisplay; 
            }
            set { m_TimezoneDisplay = value; }
        }

        public bool isSuperAdmin() { if (m_Admin == vrmUserConstant.SUPER_ADMIN) return true; else return false; }
        public bool isAdmin() { if (m_Admin == vrmUserConstant.ADMIN)return true; else return false; }
        public bool isVNOCAdmin() { if (m_Admin == vrmUserConstant.VNOCADMIN) return true; else return false; }//ZD 101764

        //Added for License modification Fixes
        public int enableExchange
        {
            get { return m_enableExchange; }
            set { m_enableExchange = value; }
        }
        public int enableDomino
        {
            get { return m_enableDomino; }
            set { m_enableDomino = value; }
        }
        //FB 1979 - Start
        public int enableMobile
        {
            get { return m_enableMobile; }
            set { m_enableMobile = value; }
        }
        //FB 1979 - End
        //FB 2693 Starts
        public int enablePCUser
        {
            get { return m_enablePCUser; }
            set { m_enablePCUser = value; }
        }
        //FB 2693 Ends
        //FB 2724 Starts
        public string RFIDValue
        {
            get { return m_RFIDValue; }
            set { m_RFIDValue = value; }
        }
        //FB 2724 Ends
        //ZD 100221 Starts
        public int enableWebexUser
        {
            get { return m_enableWebexUser; }
            set { m_enableWebexUser = value; }
        }
        public string WebexUserName
        {
            get { return m_WebexUserName; }
            set { m_WebexUserName = value; }
        }
        public string WebexPassword
        {
            get { return m_WebexPassword; }
            set { m_WebexPassword = value; }
        }
        //ZD 100221 Ends
        public int MailBlocked
        {
            get { return m_mailBlocked; }
            set { m_mailBlocked = value; }
        }

        public DateTime MailBlockedDate
        {
            get { return m_mailBlockedDate; }
            //set { m_mailBlockedDate = value; } //ZD 100318
            set
            {
                //FB 1939
                if (value.ToString().IndexOf("0001") > 0)
                    m_mailBlockedDate = DateTime.Today;
                else
                    m_mailBlockedDate = value;
            }
        }

        public int PluginConfirmations//FB 2141
        {
            get { return m_PluginConfirmations; }
            set { m_PluginConfirmations = value; }
        }
        //ZD 100263 Starts
        public string RequestID
        {
            get { return m_RequestID; }
            set { m_RequestID = value; }
        }
        //ZD 100263 Ends

        // ZD 100172 Starts
        public string LandingPageTitle
        {
            get { return m_LandingPageTitle; }
            set { m_LandingPageTitle = value; }
        }
        public string LandingPageLink
        {
            get { return m_LandingPageLink; }
            set { m_LandingPageLink = value; }
        }
        // ZD 100172 Ends

        //FB 2027 - Start
        public int enableAV
        {
            get { return m_enableAV; }
            set { m_enableAV = value; }
        }
        public int enableParticipants
        {
            get { return m_enableParticipants; }
            set { m_enableParticipants = value; }
        }
        //ZD 101122 - Start
        public int EnableAVWO
        {
            get { return m_enableAVWorkOrder; }
            set { m_enableAVWorkOrder = value; }
        }
        public int EnableCateringWO
        {
            get { return m_enableCateringWO; }
            set { m_enableCateringWO = value; }
        }
        public int EnableFacilityWO
        {
            get { return m_enableFacilityWO; }
            set { m_enableFacilityWO = value; }
        }
        //ZD 101122 - End
        //ZD 101093 START
        public int enableAdditionalOption
        {
            get { return m_enableAdditionalOption; }
            set { m_enableAdditionalOption = value; }
        }
        //ZD 101093 END
        //ZD 101446 Starts
        public string ParticipantCode
        {
            get { return m_ParticipantCode; }
            set { m_ParticipantCode = value; }
        }
        //ZD 101446 Ends
        public string UserRolename //ZD 103405
        {
            get { return m_UserRolename; }
            set { m_UserRolename = value; }
        }
        public int TickerStatus
        {
            get { return m_TickerStatus; }
            set { m_TickerStatus = value; }
        }
        public int TickerSpeed
        {
            get { return m_TickerSpeed; }
            set { m_TickerSpeed = value; }
        }
        public int TickerPosition
        {
            get { return m_TickerPosition; }
            set { m_TickerPosition = value; }
        }
        public string TickerBackground
        {
            get { return m_TickerBackground; }
            set { m_TickerBackground = value; }
        }
        public int TickerDisplay
        {
            get { return m_TickerDisplay; }
            set { m_TickerDisplay = value; }
        }
        public string RSSFeedLink
        {
            get { return m_RSSFeedLink; }
            set { m_RSSFeedLink = value; }
        }
        public int TickerStatus1
        {
            get { return m_TickerStatus1; }
            set { m_TickerStatus1 = value; }
        }
        public int TickerSpeed1
        {
            get { return m_TickerSpeed1; }
            set { m_TickerSpeed1 = value; }
        }
        public int TickerPosition1
        {
            get { return m_TickerPosition1; }
            set { m_TickerPosition1 = value; }
        }
        public string TickerBackground1
        {
            get { return m_TickerBackground1; }
            set { m_TickerBackground1 = value; }
        }
        public int TickerDisplay1
        {
            get { return m_TickerDisplay1; }
            set { m_TickerDisplay1 = value; }
        }
        public string RSSFeedLink1
        {
            get { return m_RSSFeedLink1; }
            set { m_RSSFeedLink1 = value; }
        }
        //FB 2027 - End

        //FB 2227 - Start
       
        public string InternalVideoNumber
        {
            get { return m_InternalVideoNumber; }
            set { m_InternalVideoNumber = value; }
        }

        public string ExternalVideoNumber
        {
            get { return m_ExternalVideoNumber; }
            set { m_ExternalVideoNumber = value; }
        }

        //FB 2227 - End
        //FB 2268 start
        public string HelpReqEmailID
        {
            get { return m_HelpReqEmailID; }
            set { m_HelpReqEmailID = value; }
        }
        public string HelpReqPhone
        {
            get { return m_HelpReqPhone; }
            set { m_HelpReqPhone = value; }
        }
        //FB 2268 end
        //FB 2348 start
        public int SendSurveyEmail
        {
            get { return m_SendSurveyEmail; }
            set { m_SendSurveyEmail = value; }
        }
        //FB 2348 end


        //FB 2608 start
        public int EnableVNOCselection
        {
            get { return m_VNOCoperator; }
            set { m_VNOCoperator = value; }
        }
        //FB 2608 end


        //FB 2481
        public string PrivateVMR
        {
            get { return m_PrivateVMR; }
            set { m_PrivateVMR = value; }
        }

		//FB 2599 Start
        //FB 2262
        public int EntityID
        {
            get { return m_EntityID; }
            set { m_EntityID = value; }
        }
        public string VidyoURL
        {
            get { return m_VidyoURL; }
            set { m_VidyoURL = value; }
        }
        public string Extension
        {
            get { return m_Extension; }
            set { m_Extension = value; }
        }
        public string Pin
        {
            get { return m_Pin; }
            set { m_Pin = value; }
        }
        public string Type
        {
            get { return m_Type; }
            set { m_Type = value; }
        }
        public int MemberID
        {
            get { return m_MemberID; }
            set { m_MemberID = value; }
        }
        public int isLocked
        {
            get { return m_isLocked; }
            set { m_isLocked = value; }
        }
        public int allowPersonalMeeting
        {
            get { return m_allowPersonalMeeting; }
            set { m_allowPersonalMeeting = value; }
        }
        public int allowCallDirect
        {
            get { return m_allowCallDirect; }
            set { m_allowCallDirect = value; }
        }
        //FB 2599 End
        
        //FB 2392-Whygo Starts
        public int ParentReseller
        {
            get { return m_ParentReseller; }
            set { m_ParentReseller = value; }
        }

        public int CorpUser
        {
            get { return m_CorpUser; }
            set { m_CorpUser = value; }
        }

        public int Region
        {
            get { return m_Region; }
            set { m_Region = value; }
        }

        public int ESUserID
        {
            get { return m_ESUserID; }
            set { m_ESUserID = value; }
        }

        
        //FB 2392-Whygo End
        //FB 2595
        public int Secured
        {
            get { return m_Secured; }
            set { m_Secured = value; }
        }

        // FB 2655 - DTMF - Start
        public string PreConfCode
        {
            get { return m_PreConfCode; }
            set { m_PreConfCode = value; }
        }

        public string PreLeaderPin
        {
            get { return m_PreLeaderPin; }
            set { m_PreLeaderPin = value; }
        }

        public string PostLeaderPin
        {
            get { return m_PostLeaderPin; }
            set { m_PostLeaderPin = value; }
        }

        public string AudioDialInPrefix
        {
            get { return m_AudioDialInPrefix; }
            set { m_AudioDialInPrefix = value; }
        }
        //FB 2655 - DTMF - End
        public string BrokerRoomNum //FB 3001
        {
            get { return m_BrokerRoomNum; }
            set { m_BrokerRoomNum = value; }
        }
        //ZD 100157 Starts
        public DateTime PerCalStartTime
        {
            get { return m_PerCalStartTime; }
            //set { m_PerCalStartTime = value; } //ZD 100318
            set
            {
                if (value.ToString().IndexOf("0001") > 0)//LDAP IMPORT FIX
                    m_PerCalStartTime = DateTime.Today;
                else
                    m_PerCalStartTime = value;
            }
        }
        public DateTime PerCalEndTime
        {
            get { return m_PerCalEndTime; }
            //set { m_PerCalEndTime = value; } //ZD 100318
            set
            {
                if (value.ToString().IndexOf("0001") > 0)//LDAP IMPORT FIX
                    m_PerCalEndTime = DateTime.Today;
                else
                    m_PerCalEndTime = value;
            }
        }
        public DateTime RoomCalStartTime
        {
            get { return m_RoomCalStartTime; }
            //set { m_RoomCalStartTime = value; } //ZD 100318
            set
            {
                if (value.ToString().IndexOf("0001") > 0)//LDAP IMPORT FIX
                    m_RoomCalStartTime = DateTime.Today;
                else
                    m_RoomCalStartTime = value;
            }

        }
        public DateTime RoomCalEndime
        {
            get { return m_RoomCalEndime; }
            //set { m_RoomCalEndime = value; } //ZD 100318
            set
            {
                if (value.ToString().IndexOf("0001") > 0)//LDAP IMPORT FIX
                    m_RoomCalEndime = DateTime.Today;
                else
                    m_RoomCalEndime = value;
            }
        }
        public int PerCalShowHours
        {
            get { return m_PerCalShowHours; }
            set { m_PerCalShowHours = value; }
        }
        public int RoomCalShowHours
        {
            get { return m_RoomCalShowHours; }
            set { m_RoomCalShowHours = value; }
        }
        //ZD 100157 Ends
        //ZD 100890 Start
       //ZD 100168 inncrewin 23/12/2013
        public string StaticID 
        { 
            get { return m_staticId; } 
            set { m_staticId = value; } 
        }
        public int IsStaticIDEnabled 
        { 
            get { return m_isStaticIDEnabled; } 
            set { m_isStaticIDEnabled = value; } 
        }
        //ZD 100168 inncrewin 23/12/2013 Ends
        public string ExternalUserId 
        { 
            get { return m_ExternalUserId; } 
            set { m_ExternalUserId = value; } 
        }
        //ZD 100890 End
		//ZD 100781 Starts
        public DateTime PasswordTime
        {
            get { return m_PasswordTime; }
            set
            {
                if (value.ToString().IndexOf("0001") > 0)
                    m_PasswordTime = DateTime.Today;
                else
                    m_PasswordTime = value;
            }

        }
        public int IsPasswordReset
        {
            get { return m_IsPasswordReset; }
            set { m_IsPasswordReset = value; }
        }
        //ZD 100781 Ends
        public int UserOrgin//ZD 101865 starts
        {
            get { return m_usrorgin; }
            set { m_usrorgin = value; }
        }//ZD 101865 starts

        public string UserDomain//ZD 103874 starts
        {
            get { return m_UserDomain; }
            set { m_UserDomain = value; }
        }//ZD 103874 End
		//ZD 104021 Start
        public string BJNUserName 
        {
            get { return m_BJNUserName; }
            set { m_BJNUserName = value; }
        }
        public int BJNUserId 
        {
            get { return m_BJNUserId; }
            set { m_BJNUserId = value; }
        }
        public string BJNMeetingId 
        {
            get { return m_BJNMeetingId; }
            set { m_BJNMeetingId = value; }
        }
        public string BJNUserEmail  
        {
            get { return m_BJNUserEmail; }
            set { m_BJNUserEmail = value; }
        }
        public int EnableBJNConf  
        {
            get { return m_EnableBJNConf; }
            set { m_EnableBJNConf = value; }
        }
        public string BJNPassCode  
        {
            get { return m_BJNPassCode; }
            set { m_BJNPassCode = value; }
        }
        public string BJNPartyPassCode
        {
            get { return m_BJNPartyPassCode; }
            set { m_BJNPartyPassCode = value; }
        }
		//ZD 104021 End
        //ALLDEV 782-Start
        public string PexipStaticVMR
        {
            get { return m_PexipStaticVMR; }
            set { m_PexipStaticVMR = value; }
        }
        //ALLDEV 782-End

        //ALLDEV-782 Starts
        public string PexipAliasURI
        {
            get { return m_PexipAliasURI; }
            set { m_PexipAliasURI = value; }
        }
        //ALLDEV-782 Ends
        #endregion

        public vrmBaseUser()
        {

            m_LastLogin = DateTime.Today;
            m_accountexpiry = sysSettings.ExpiryDate;
            m_EndPointList = new List<vrmEndPoint>();
            m_AccountList = new List<vrmAccount>();
            // FB 381 set as default 
            m_searchId = -1;
     
        }
        // copy constructor
        public vrmBaseUser(vrmBaseUser obj)
        {
            userid = obj.userid;
            FirstName = obj.FirstName;
            LastName = obj.LastName;
            Email = obj.Email;
            Financial = obj.Financial;
            Admin = obj.Admin;
            Login = obj.Login;
            Password = obj.Password;
            Company = obj.Company;
            TimeZone = obj.TimeZone;
            Language = obj.Language;
            PreferedRoom = obj.PreferedRoom;
            AlternativeEmail = obj.AlternativeEmail;
            DoubleEmail = obj.DoubleEmail;
            PreferedGroup = obj.PreferedGroup;
            CCGroup = obj.CCGroup;
            Telephone = obj.Telephone;
            RoomID = obj.RoomID;
            PreferedOperator = obj.PreferedOperator;
            lockCntTrns = obj.lockCntTrns;
            DefLineRate = obj.DefLineRate;
            DefVideoProtocol = obj.DefVideoProtocol;
            Deleted = obj.Deleted;
            DefAudio = obj.DefAudio;
            DefVideoSession = obj.DefVideoSession;
            MenuMask = obj.MenuMask;
            initialTime = obj.initialTime;
            EmailClient = obj.EmailClient;
            newUser = obj.newUser;
            PrefTZSID = obj.PrefTZSID;
            IPISDNAddress = obj.IPISDNAddress;
            DefaultEquipmentId = obj.DefaultEquipmentId;
            connectionType = obj.connectionType;
            companyId = obj.companyId;
            securityKey = obj.securityKey;
            LockStatus = obj.LockStatus;
            LastLogin = obj.LastLogin;
            outsidenetwork = obj.outsidenetwork;
            emailmask = obj.emailmask;
            roleID = obj.roleID;
            addresstype = obj.addresstype;
            accountexpiry = obj.accountexpiry;
            approverCount = obj.approverCount;
            BridgeID = obj.BridgeID;
            Title = obj.Title;
            LevelID = obj.LevelID;
            preferredISDN = obj.preferredISDN;
            portletId = obj.portletId;
            userType = obj.userType;
            endpointId = obj.endpointId;
            searchId = obj.searchId;
            //Added  for License modification
            enableExchange = obj.enableExchange;
            enableDomino = obj.enableDomino;
            enableMobile = obj.enableMobile; //FB 1979
            //FB 1642 - Audio add on - Starts
            Audioaddon = obj.Audioaddon;
            WorkPhone = obj.WorkPhone;
            CellPhone = obj.CellPhone;
            ConferenceCode = obj.ConferenceCode;
            LeaderPin = obj.LeaderPin;
            //FB 1642 - Audio add on - End
            DefaultTemplate = obj.DefaultTemplate; //FB 2027
            EmailLangId = obj.EmailLangId;  //FB 1830
            //FB 2227 Start
            InternalVideoNumber = obj.InternalVideoNumber;
            ExternalVideoNumber = obj.ExternalVideoNumber;
            //FB 2227 End
            HelpReqEmailID = obj.HelpReqEmailID; //FB 2268
            HelpReqPhone = obj.HelpReqPhone;//FB 2268
            SendSurveyEmail = obj.SendSurveyEmail;//FB 2348
            PrivateVMR = obj.PrivateVMR;//FB 2481
			//FB 2599 - Start
            //FB 2262
            EntityID = obj.EntityID;//FB 2262
            VidyoURL = obj.VidyoURL;
            Extension = obj.Extension;
            Pin = obj.Pin;
            Type = obj.Type;
            MemberID = obj.MemberID;
            isLocked = obj.isLocked;
            allowCallDirect = obj.allowCallDirect;
            allowPersonalMeeting = obj.allowPersonalMeeting;
            //FB 2599 - End
			//FB 2392-Whygo
            ParentReseller = obj.ParentReseller;
            CorpUser = obj.CorpUser;
            Region = obj.Region;
            Secured = obj.Secured; //FB 2595
            //FB 2655 - DTMF
            PreLeaderPin = obj.PreLeaderPin;
            PreConfCode = obj.PreConfCode;
            PostLeaderPin = obj.PostLeaderPin;
            AudioDialInPrefix = obj.AudioDialInPrefix;
            enablePCUser = obj.enablePCUser; //FB 2693
            RFIDValue = obj.RFIDValue;//FB 2724
            BrokerRoomNum = obj.BrokerRoomNum; //FB 3001
            //ZD 100157 Starts
            PerCalStartTime = obj.PerCalStartTime;
            PerCalEndTime = obj.PerCalEndTime;
            RoomCalStartTime = obj.RoomCalStartTime;
            RoomCalEndime = obj.RoomCalEndime;
            PerCalShowHours = obj.PerCalShowHours;
            RoomCalShowHours = obj.RoomCalShowHours;
            //ZD 100157 Ends
            RequestID = obj.RequestID;//ZD 100263
            //ZD 100168 inncrewin 23/12/2013
            StaticID = obj.StaticID;
            IsStaticIDEnabled = obj.IsStaticIDEnabled;
            //ZD 100168 inncrewin 23/12/2013 Ends
			//ZD 100221 Starts
            enableWebexUser = obj.enableWebexUser;
            WebexPassword = obj.WebexPassword;
            WebexUserName = obj.WebexUserName;
            //ZD 100221 Ends
            LandingPageTitle = obj.LandingPageTitle; // ZD 100172
            LandingPageLink = obj.LandingPageLink; // ZD 100172
			ExternalUserId = obj.ExternalUserId; //ZD 100890
            PasswordTime = obj.PasswordTime;//ZD 100781
            IsPasswordReset = obj.IsPasswordReset; //ZD 100781
            LastModifiedDate = obj.LastModifiedDate; //ZD 100664
            LastModifiedUser = obj.LastModifiedUser; //ZD 100664
            RoomViewType = obj.RoomViewType;//ZD 100621
            EnableAVWO = obj.EnableAVWO;//ZD 101122
            EnableCateringWO = obj.EnableCateringWO;
            EnableFacilityWO = obj.EnableFacilityWO;
            UserOrgin = obj.UserOrgin;//ZD 101865
            RoomRecordsView = obj.RoomRecordsView;//ZD 101175
			custAdminRights = obj.custAdminRights; // ZD 101233
            ParticipantCode = obj.ParticipantCode; //ZD 101446
            UserRolename = obj.UserRolename;//ZD 103405
			UserDomain = obj.UserDomain; //ZD 103784
            //ZD 104021
            BJNUserName = obj.BJNUserName; 
            BJNUserId = obj.BJNUserId;
            BJNMeetingId = obj.BJNMeetingId;
            BJNUserEmail = obj.BJNUserEmail;
            EnableBJNConf = obj.EnableBJNConf;
            BJNPassCode = obj.BJNPassCode;
            BJNPartyPassCode = obj.BJNPartyPassCode;
            //ZD 104021
            PexipStaticVMR = obj.PexipStaticVMR;//ALLDEV 782
          }
        
      
    }
    public class vrmLDAPUser
    {
        #region Private Internal Members
        private int m_userid;
        private string m_firstname;
        private string m_lastname;
        private string m_email;
        private string m_telephone;
        private DateTime m_whencreated;
        private int m_newuser;
        private string m_login;

        #endregion

        #region Public Properties

        public int userid
        {
            get { return m_userid; }
            set { m_userid = value; }
        }
        public string FirstName
        {
            get { return m_firstname; }
            set { m_firstname = value; }
        }
        public string LastName
        {
            get { return m_lastname; }
            set { m_lastname = value; }
        }
        public string Email
        {
            get { return m_email; }
            set { m_email = value; }
        }
        public string Telephone
        {
            get { return m_telephone; }
            set { m_telephone = value; }
        }
        public DateTime whencreated
        {
            get { return m_whencreated; }
            //set { m_whencreated = value; } //ZD 100318
            set
            {
                if (value.ToString().IndexOf("0001") > 0)
                    m_whencreated = DateTime.Today;
                else
                    m_whencreated = value;
            }
        }
        public int newUser
        {
            get { return m_newuser; }
            set { m_newuser = value; }
        }
        public string Login
        {
            get { return m_login; }
            set { m_login = value; }
        }        
        #endregion
    }

    public class vrmUserTemplate
    {
        #region Private Internal Members
        private int m_id;
        private string m_name;
        private int m_initialtime;
        private int m_videoProtocol;
        private string m_ipisdnaddress;
        private int m_connectiontype;
        private int m_roleId;
        private int m_timezone;
        //private int m_location;
        private String m_location;// Code added for room search
        private int m_languageId;
        private int m_lineRateId;
        private int m_bridgeId;
        private string m_deptId;//ZD 102052
        private int m_groupId;
        private DateTime m_expirationDate;
        private int m_addressBook;
        private int m_emailNotification;
        private int m_outsideNetwork;
        private int m_equipmentId;
        private int m_deleted;
        private int m_companyId;
        //ZD 102052 START
        private int m_enableParticipants;
        private int m_enableAV;
        private int m_enableAdditionalOption;
        private int m_enableAVWO;
        private int m_enableCateringWO;
        private int m_enableFacilityWO;
        private int m_enableExchange;
        private int m_enableDomino;
        private int m_enableMobile;
        //ZD 102052 END
        private string m_DateFormat, m_TimeFormat;//ZD 103673

        #endregion

        #region Public Properties

        public int id
        {
            get { return m_id; }
            set { m_id = value; }
        }
        public string name
        {
            get { return m_name; }
            set { m_name = value; }
        }
        public int connectiontype
        {
            get { return m_connectiontype; }
            set { m_connectiontype = value; }
        }
        public int roleId
        {
            get { return m_roleId; }
            set { m_roleId = value; }
        }
        public string ipisdnaddress
        {
            get { return m_ipisdnaddress; }
            set { m_ipisdnaddress = value; }
        }
        public int initialtime
        {
            get { return m_initialtime; }
            set { m_initialtime = value; }
        }
        public int timezone
        {
            get { return m_timezone; }
            set { m_timezone = value; }
        }
        public int videoProtocol
        {
            get { return m_videoProtocol; }
            set { m_videoProtocol = value; }
        }
        public String location// Code added for room search
        {
            get { return m_location; }
            set { m_location = value; }
        }
        public int languageId
        {
            get { return m_languageId; }
            set { m_languageId = value; }
        }
        public int lineRateId
        {
            get { return m_lineRateId; }
            set { m_lineRateId = value; }
        }
        public int bridgeId
        {
            get { return m_bridgeId; }
            set { m_bridgeId = value; }
        }
        public string deptId//ZD 102052
        {
            get { return m_deptId; }
            set { m_deptId = value; }
        }
        public int groupId
        {
            get { return m_groupId; }
            set { m_groupId = value; }
        }
        public DateTime expirationDate
        {
            get { return m_expirationDate; }
            //set { m_expirationDate = value; } //ZD 100318
            set
            {
                if (value.ToString().IndexOf("0001") > 0)
                    m_expirationDate = DateTime.Today;
                else
                    m_expirationDate = value;
            }
        }

        public int addressBook
        {
            get { return m_addressBook; }
            set { m_addressBook = value; }
        }
        public int emailNotification
        {
            get { return m_emailNotification; }
            set { m_emailNotification = value; }
        }
        public int outsideNetwork
        {
            get { return m_outsideNetwork; }
            set { m_outsideNetwork = value; }
        }
        public int equipmentId
        {
            get { return m_equipmentId; }
            set { m_equipmentId = value; }
        }
        public int deleted
        {
            get { return m_deleted; }
            set { m_deleted = value; }
        }

        public int companyId//Code added for organization
        {
            get { return m_companyId; }
            set { m_companyId = value; }
        }
        //ZD 102052 START
        public int enableParticipants
        {
            get { return m_enableParticipants; }
            set { m_enableParticipants = value; }
        }
        public int enableAV
        {
            get { return m_enableAV; }
            set { m_enableAV = value; }
        }
        public int enableAdditionalOption
        {
            get { return m_enableAdditionalOption; }
            set { m_enableAdditionalOption = value; }
        }
        public int enableAVWO
        {
            get { return m_enableAVWO; }
            set { m_enableAVWO = value; }
        }
        public int enableCateringWO
        {
            get { return m_enableCateringWO; }
            set { m_enableCateringWO = value; }
        }
        public int enableFacilityWO
        {
            get { return m_enableFacilityWO; }
            set { m_enableFacilityWO = value; }
        }
        public int enableExchange
        {
            get { return m_enableExchange; }
            set { m_enableExchange = value; }
        }
        public int enableDomino
        {
            get { return m_enableDomino; }
            set { m_enableDomino = value; }
        }
        public int enableMobile
        {
            get { return m_enableMobile; }
            set { m_enableMobile = value; }
        }
        //ZD 102052 END
        //ZD 103673 - Start
        public string DateFormat
        {
            get
            {
                if (m_DateFormat == null)
                    m_DateFormat = "MM/dd/yyyy";

                if (m_DateFormat.Trim() == "")
                    m_DateFormat = "MM/dd/yyyy";

                return m_DateFormat;
            }
            set { m_DateFormat = value; }
        }
        public string TimeFormat
        {
            get
            {
                if (m_TimeFormat == null)
                    m_TimeFormat = "1";

                if (m_TimeFormat.Trim() == "")
                    m_TimeFormat = "1";

                return m_TimeFormat;
            }
            set { m_TimeFormat = value; }
        }
        //ZD 103673 - End

        #endregion


        public vrmUserTemplate()
        {
            id = 0;
            timezone = 0;
        }
    }
    public class vrmUserRoles
    {
        #region Private Internal Members
        private int m_roleID;
        private string m_roleName;
        private string m_roleMenuMask;
        private int m_locked;
        private int m_level;
        private int m_crossaccess;
        private int m_createType;//FB 1939
        private int m_DisplayOrder, m_orgID;//FB 2891 //ZD 100923

        #endregion

        #region Public Properties
        public int roleID
        {
            get { return m_roleID; }
            set { m_roleID = value; }
        }
        public string roleName
        {
            get { return m_roleName; }
            set { m_roleName = value; }
        }
        public string roleMenuMask
        {
            get { return m_roleMenuMask; }
            set { m_roleMenuMask = value; }
        }

        public int locked
        {
            get { return m_locked; }
            set { m_locked = value; }
        }
        public int level
        {
            get { return m_level; }
            set { m_level = value; }
        }
        public int crossaccess //organization module
        {
            get { return m_crossaccess; }
            set { m_crossaccess = value; }
        }
        public int createType//FB 1939
        {
            get { return m_createType; }
            set { m_createType = value; }
        }

        public bool isCrossAccessAllowed() { if (m_crossaccess == 1) return true; else return false; }

        public int DisplayOrder//FB 2891
        {
            get { return m_DisplayOrder; }
            set { m_DisplayOrder = value; }
        }
        public int orgID //ZD 100923
        {
            get { return m_orgID; }
            set { m_orgID = value; }
        }
        #endregion

        public vrmUserRoles()
        {
            //roleID = 0;//FB 2027

        }
    }
    //FB 2027 Start
    public class vrmUserLotusNotesPrefs
    {
        #region Private Internal Members

        private int m_userid;
        private string m_noteslogin;
        private string m_notespwd;
        private string m_notespath;
        #endregion

        #region Public Properties

        public int userid
        {
            get { return m_userid; }
            set { m_userid = value; }
        }
        public string noteslogin
        {
            get { return m_noteslogin; }
            set { m_noteslogin = value; }
        }
        public string notespwd
        {
            get { return m_notespwd; }
            set { m_notespwd = value; }
        }
        public string notespath
        {
            get { return m_notespath; }
            set { m_notespath = value; }
        }
        #endregion
    }
    //FB 2027 End
    public class vrmUser : vrmBaseUser
    {
        public vrmUser() { }
        public vrmUser(vrmBaseUser obj) : base(obj) { }
    }
    public class vrmGuestUser : vrmBaseUser
    {
    }
    public class vrmInactiveUser : vrmBaseUser
    {
        public vrmInactiveUser() { }
        public vrmInactiveUser(vrmBaseUser obj) : base(obj) { }
    }
    public class userConstant
    {
        #region Public Static Defines
        public const int LIST_TYPE_USER = 0;
        public const int LIST_TYPE_GUEST = 1;
        public const int LIST_TYPE_INACTIVE = 2;
        public const int LIST_TYPE_LDAP = 3;
        #endregion

    }

    public class vrmAccount
    {
        #region Private Internal Members

        private int m_userId, m_ID;//FB 2027
        private int m_TotalTime;
        private int m_TimeRemaining;
        private DateTime m_InitTime;
        private DateTime m_ExpirationTime;
        private int m_UsedMinutes, m_ScheduledMinutes; //ZD 102043
        #endregion

        #region Public Properties

        public int userId
        {
            get { return m_userId; }
            set { m_userId = value; }
        }
        public int TotalTime
        {
            get { return m_TotalTime; }
            set { m_TotalTime = value; }
        }
        public int TimeRemaining
        {
            get { return m_TimeRemaining; }
            set { m_TimeRemaining = value; }
        }
        public DateTime InitTime
        {
            get { return m_InitTime; }
            //set { m_InitTime = value; } //ZD 100318
            set
            {
                //FB 1939
                if (value.ToString().IndexOf("0001") > 0)
                    m_InitTime = DateTime.Today;
                else
                    m_InitTime = value;
            }
        }
        public DateTime ExpirationTime
        {
            get { return m_ExpirationTime; }
            //set { m_ExpirationTime = value; } //ZD 100318
            set
            {
                //FB 1939
                if (value.ToString().IndexOf("0001") > 0)
                    m_ExpirationTime = DateTime.Today;
                else
                    m_ExpirationTime = value;
            }
        }
		//ZD 102043 Starts
        public int UsedMinutes 
        {
            get { return m_UsedMinutes; }
            set { m_UsedMinutes = value; }
        }
        public int ScheduledMinutes
        {
            get { return m_ScheduledMinutes; }
            set { m_ScheduledMinutes = value; }
        }
		//ZD 102043 End
        public int ID //FB 2027
        {
            get { return m_ID; }
            set { m_ID = value; }
        }
        
        #endregion
    }
    public class vrmUserSearchTemplate
    {
        #region Private Internal Members
        private int m_id;
        private int m_userid;
        private string m_name;
        private string m_template;
        private int m_orgId;
        #endregion

        #region Public Properties

        public int id
        {
            get { return m_id; }
            set { m_id = value; }
        }
        public int userid
        {
            get { return m_userid; }
            set { m_userid = value; }
        }
        public string name
        {
            get { return m_name; }
            set { m_name = value; }
        }
        public string template
        {
            get { return m_template; }
            set { m_template = value; }
        }
        public int orgId
        {
            get { return m_orgId; }
            set { m_orgId = value; }
        }
        #endregion
    }
    public class vrmGroupParticipant
    {
        #region Private Internal Members
        private int m_GroupID, m_UserID, m_CC, m_ID;//FB 2027
        #endregion

        #region Public Properties
        public int GroupID
        {
            get { return m_GroupID; }
            set { m_GroupID = value; }
        }
        public int UserID
        {
            get { return m_UserID; }
            set { m_UserID = value; }
        }
        public int CC
        {
            get { return m_CC; }
            set { m_CC = value; }
        }
        public int ID //FB 2027
        {
            get { return m_ID; }
            set { m_ID = value; }
        }
        #endregion
    }
    public class vrmGroupDetails
    {
        #region Private Internal Members
        private int m_GroupId, m_CC, m_Owner, m_Private, m_orgId;//FB 2027
        private string m_Name, m_Description;
        #endregion

        #region Public Properties
        public int GroupID
        {
            get { return m_GroupId; }
            set { m_GroupId = value; }
        }
        public string Name
        {
            get { return m_Name; }
            set { m_Name = value; }
        }
        public int Owner
        {
            get { return m_Owner; }
            set { m_Owner = value; }
        }
        public int CC
        {
            get { return m_CC; }
            set { m_CC = value; }
        }
        public int Private
        {
            get { return m_Private; }
            set { m_Private = value; }
        }
        public string Description
        {
            get { return m_Description; }
            set { m_Description = value; }
        }
        public int orgId //FB 2027
        {
            get { return m_orgId; }
            set { m_orgId = value; }
        }
        
        #endregion
    }

    //FB 2027 - Starts
    public class vrmAccountGroupList
    {
        #region Private Internal Members
        private int m_ID, m_userid, m_acgroupid;
        #endregion

        #region Public Properties
        public int ID
        {
            get { return m_ID; }
            set { m_ID = value; }
        }
        public int userid
        {
            get { return m_userid; }
            set { m_userid = value; }
        }
        public int acgroupid
        {
            get { return m_acgroupid; }
            set { m_acgroupid = value; }
        }
        #endregion

    }
    //FB 2027 - End
    //FB 1830 - Translation
    public class vrmLanguageText
    {
        #region Private Internal Members
        private int m_uid, m_LanguageID, m_TextType; //ZD 102829
        private string m_Text;
        private string m_TranslatedText;
        #endregion

        #region Public Properties
        public int uId
        {
            get { return m_uid; }
            set { m_uid = value; }
        }
        public string Text
        {
            get { return m_Text; }
            set { m_Text = value; }
        }

        public string TranslatedText
        {
            get { return m_TranslatedText; }
            set { m_TranslatedText = value; }
        }

        public int LanguageID
        {
            get { return m_LanguageID; }
            set { m_LanguageID = value; }
        }

        public int TextType //ZD 102829
        {
            get { return m_TextType; }
            set { m_TextType = value; }
        }
        #endregion

    }

    //NewLobby start
    public class vrmUserLobby
    {
        #region Private Internal Members
        private int m_UserID, m_IconID, m_GridPosition, m_UId;
        private string m_Label, m_SelectedColor, m_SelectedImage; // ZD 101006
        #endregion

        #region Public Properties
        public int UId
        {
            get { return m_UId; }
            set { m_UId = value; }
        }
        public int UserID
        {
            get { return m_UserID; }
            set { m_UserID = value; }
        }
        public int IconID
        {
            get { return m_IconID; }
            set { m_IconID = value; }
        }
        public int GridPosition
        {
            get { return m_GridPosition; }
            set { m_GridPosition = value; }
        }
        public string Label
        {
            get { return m_Label; }
            set { m_Label = value; }
        }
        // ZD 101006 Starts
        public string SelectedColor
        {
            get { return m_SelectedColor; }
            set { m_SelectedColor = value; }
        }
        public string SelectedImage
        {
            get { return m_SelectedImage; }
            set { m_SelectedImage = value; }
        }
        // // ZD 101006 Ends
        #endregion
    }

    //FB 2693 - Starts
    #region vrmUserPC
    public class vrmUserPC
    {
        #region Private Internal Members
        private int m_id, m_userid, m_PCId;
        private string m_Description, m_SkypeURL, m_VCDialinIP, m_VCMeetingID, m_VCDialinSIP;
        private string m_VCDialinH323, m_VCPin, m_PCDialinNum, m_PCDialinFreeNum, m_PCMeetingID;
        private string m_PCPin, m_Intructions;
        #endregion

        #region Public Properties
        public int id
        {
            get { return m_id; }
            set { m_id = value; }
        }
        public int userid
        {
            get { return m_userid; }
            set { m_userid = value; }
        }
        public int PCId
        {
            get { return m_PCId; }
            set { m_PCId = value; }
        }
        public string Description
        {
            get { return m_Description; }
            set { m_Description = value; }
        }
        public string SkypeURL
        {
            get { return m_SkypeURL; }
            set { m_SkypeURL = value; }
        }
        public string VCDialinIP
        {
            get { return m_VCDialinIP; }
            set { m_VCDialinIP = value; }
        }
        public string VCMeetingID
        {
            get { return m_VCMeetingID; }
            set { m_VCMeetingID = value; }
        }
        public string VCDialinSIP
        {
            get { return m_VCDialinSIP; }
            set { m_VCDialinSIP = value; }
        }
        public string VCDialinH323
        {
            get { return m_VCDialinH323; }
            set { m_VCDialinH323 = value; }
        }
        public string VCPin
        {
            get { return m_VCPin; }
            set { m_VCPin = value; }
        }
        public string PCDialinNum
        {
            get { return m_PCDialinNum; }
            set { m_PCDialinNum = value; }
        }
        public string PCDialinFreeNum
        {
            get { return m_PCDialinFreeNum; }
            set { m_PCDialinFreeNum = value; }
        }
        public string PCMeetingID
        {
            get { return m_PCMeetingID; }
            set { m_PCMeetingID = value; }
        }
        public string PCPin
        {
            get { return m_PCPin; }
            set { m_PCPin = value; }
        }
        public string Intructions
        {
            get { return m_Intructions; }
            set { m_Intructions = value; }
        }
        #endregion
    }
    #endregion
    //FB 2693 - End

    //ZD 100152 Starts
    #region vrmUserToken
    public class vrmUserToken
    {
        #region Private Internal Members
        private int m_id, m_userid, m_orgid;
        private string m_AccessToken, m_refreshtoken;
        private string m_ChannelID,  m_ChannelEtag, m_ChannelResourceID, m_ChannelResourseURL, m_UserEmail;
        private DateTime m_ChannelExpiration;
        #endregion

        #region Public Properties
        public int id
        {
            get { return m_id; }
            set { m_id = value; }
        }
        public int userid
        {
            get { return m_userid; }
            set { m_userid = value; }
        }
        public int orgid
        {
            get { return m_orgid; }
            set { m_orgid = value; }
        }
        public string AccessToken
        {
            get { return m_AccessToken; }
            set { m_AccessToken = value; }
        }
        public string RefreshToken
        {
            get { return m_refreshtoken; }
            set { m_refreshtoken = value; }
        }
        public string ChannelID
        {
            get { return m_ChannelID; }
            set { m_ChannelID = value; }
        }
        public string ChannelEtag
        {
            get { return m_ChannelEtag; }
            set { m_ChannelEtag = value; }
        }

        public string ChannelResourceID
        {
            get { return m_ChannelResourceID; }
            set { m_ChannelResourceID = value; }
        }
        public string ChannelResourseURL
        {
            get { return m_ChannelResourseURL; }
            set { m_ChannelResourseURL = value; }
        }
        public DateTime ChannelExpiration
        {
            get { return m_ChannelExpiration; }
            set { m_ChannelExpiration = value; }
        }
        public string UserEmail
        {
            get { return m_UserEmail; }
            set { m_UserEmail = value; }

        }


        
        #endregion
    }
    #endregion
    //ZD 100152 Ends
    //ZD 103878
    public class vrmUserDeleteList : vrmBaseUser
    {
        public vrmUserDeleteList() { }
        public vrmUserDeleteList(vrmBaseUser obj) : base(obj) { }

        #region Private Internal Members
        private DateTime m_CreatedDate;
        #endregion

        #region Public Properties
        public DateTime CreatedDate
        {
            get { return m_CreatedDate; }
            set { m_CreatedDate = value; }
        }

        #endregion
    }    
}
