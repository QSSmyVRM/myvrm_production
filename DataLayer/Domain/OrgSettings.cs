﻿/* Copyright (C) 2015 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
//ZD 100147 //ZD 100886
using System;
using System.Text;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.ComponentModel;
using System.Xml;

using NHibernate;
using NHibernate.Criterion;
using log4net;

namespace myVRM.DataLayer
{

    public class PexipMeetingType
    {
        public static int PersonalMeeting = 1;
        public static int OneTimeMeeting = 2;
    }

    #region Class OrgData
    /// <summary>
    /// Summary description for organization settings.
    /// </summary>
    public class OrgData
    {
        #region Private Internal Members

        //private int m_uid;
        private DateTime m_LastModified;
        private int m_LastModifiedUser, m_tzsystemid;
        private string m_Logo, m_CompanyTel, m_CompanyEmail, m_CompanyURL;
        private int m_Connect2, m_DialOut, m_AccountingLogic, m_BillPoint2Point, m_AllLocation;
        private int m_RealtimeStatus, m_BillRealTime, m_MultipleDepartments;
        private int m_overAllocation, m_Threshold, m_autoApproveImmediate, m_adminEmail1, m_adminEmail2;
        private int m_adminEmail3, m_AutoAcceptModConf, m_recurEnabled, m_responsetime;
        private string m_responsemessage;
        private DateTime m_SystemStartTime, m_SystemEndTime;
        private int m_Open24hrs;
        private string m_Offdays;
        private float m_ISDNLineCost, m_ISDNPortCost, m_IPLineCost, m_IPPortCost;
        private int m_ISDNTimeFrame;
        private int m_dynamicinviteenabled, m_doublebookingenabled;
        private int m_IMEnabled;
        private float m_IMRefreshConn;
        private int m_IMMaxUnitConn, m_IMMaxSysConn, m_DefaultToPublic;
        private int m_RoomLimit, m_MCULimit, m_UserLimit, m_ExchangeUserLimit, m_DominoUserLimit, m_MobileUserLimit, m_GuestRoomLimit, m_GuestRoomPerUser, m_WebexUserLimit; //FB 1979 FB 2426 //ZD 100221
        private int m_BlueJeansUserLimit; //ZD 104021
        private int m_DefaultConferenceType, m_EnableRoomConference, m_EnableAudioVideoConference, m_EnableAudioOnlyConference,
            m_DefaultCalendarToOfficeHours, m_MCUEnchancedLimit, m_EnableHotdeskingConference;//FB 2486 //ZD 100719
        private String m_RoomTreeExpandLevel;
        private String m_EnableCustomOption;
        private String m_EnableBufferZone;
        private int m_OrgId;
        private int m_MaxNonVideoRooms;
		private int m_MaxVideoRooms;
        private int m_MaxVMRRooms;//FB 2586
		private int m_MaxEndpoints;
		private int m_MaxCDRs;
		private int m_EnableFacilities;
		private int m_EnableCatering;
		private int m_EnableHousekeeping;
		private int m_EnableAPI;
		private int m_Language;
        private int m_LogoImageId; //Image Project
        private int m_LobytopImageId; //Image Project
        private int m_LobytopHighImageId; //Image Project
        private string m_LogoImageName; //Image Project
        private string m_LobytopImageName;  //Image Project
        private string m_LobytopHighImageName;  //Image Project
        private int m_MailLogoID;
        //Css project Starts...
        private int m_DefaultCssId;
        private int m_MirrorCssId;
        private int m_ArtifactsCssId;
        private int m_TextchangeId;
        //Css project Ends...
        //Audio Add On ..
        private int m_ConferenceCode;
        private int m_LeaderPin;
        private int m_AdvAvParams;
        private int m_AudioParams;
 		private string m_FooterMessage; //FB 1710
        private int m_FooterImage; //FB 1710
        private string m_IcalReqEmailID; //iCal Req Email
        private int m_CustomAttributeLimit; //FB 1779
		private int m_sendIcal; //FB 1782
        private int m_sendApprovalIcal; //FB 1782
        private int m_EmailLangId;  //FB 1830
        private int m_isVIP;// FB 1864
        private int m_isUniquePassword;// FB 1865
        private int m_mailBlocked;// FB 1860
        private DateTime m_mailBlockedDate = DateTime.Today;// FB 1860, 1922

        private int m_isAssignedMCU;// FB 1901
        private int m_ReminderMask;// FB 1926
        private int m_isMultiLingual;// FB 1830
		private int m_PluginConfirmations;// FB 2141
        private int m_SendAttachmentsExternal;// FB 2154
        private int m_TelepresenceFilter;// FB 2170
        private int m_EnableRoomServiceType;//FB 2219
		private int m_SpecialRecur;// FB 2052
        private int m_isDeptUser; //FB 2269
		private int m_EnablePIMServiceType;//FB 2038
        private int m_EnableImmediateConference, m_EnableAudioBridges;//FB 2036 //FB 2023
        private int m_EnableConfPassword, m_EnablePublicConf, m_EnableRoomParam; //FB 2359
        private int m_EnableSecurityBadge;// FB 2136
        private int m_SecurityBadgeType;// FB 2136   
        private string m_SecurityDeskEmailId;// FB 2136  
		private int m_EnablePasswordRule;//FB 2339
        private int m_DefPolycomRMXLO;// FB 2335   
        private int m_DefPolycomMGCLO;// FB 2335   
        private int m_DefCiscoTPLO;// FB 2335   
        private int m_DefCTMSLO;// FB 2335 
        private int m_DedicatedVideo;// FB 2334
        private int m_WorkingHours, m_EnableVMR; //FB 2343
		//FB 2348
        private int m_EnableSurvey, m_SurveyOption, m_TimeDuration;
        private string m_SurveyURL;
        //private int m_EnablePCModule; //FB 2693
        private int m_EnableEPDetails, m_EnableCloud; //FB 2347 2401 FB 2262 - J //FB 2599
        private int m_SetupTime, m_TearDownTime;//FB 2398
        private string m_customerName, m_customerID; //FB 2363
		private int m_EnableAcceptDecline; //FB 2419
        private int m_DefLinerate, m_EnableSmartP2P; //FB 2429 //FB 2430
        private int m_McuSetupTime; //FB 2440
        private int m_MCUTeardonwnTime; //FB 2440
		private int m_MCUBufferPriority; //FB 2440
        private string m_TopTier, m_MiddleTier, m_DefaultTxtMsg; //FB 2426 //FB 2486
        private int m_OnflyTopTierID, m_OnflyMiddleTierID; //FB 2426
        private int m_EnableConfTZinLoc, m_SendConfirmationEmail, m_DefaultConfDuration;//FB 2469 //FB 2501
        private int m_MCUAlert;//FB 2472
        private int m_EM7OrgID, m_EmailDateFormat; //FB 2501 EM7 //FB 2555
        private string m_EM7UserName,m_EM7Password; //FB 2501 EM7
        private int m_MaxPublicVMRParty;//FB 2550
        private int m_EnableFECC, m_DefaultFECC;//FB 2571
        //FB 2598 Starts
        private int m_EnableCallmonitor;//DD Feature
        private int m_EnableEM7;
        private int m_EnableCDR;
        //FB 2598 Ends
        private int m_EnableDialPlan; // Fb 2636
        private int m_IsBridgeExtNo, m_MeetandGreetBuffer;//FB 2610 FB 2609
        private int m_EnableCncigSupport, m_AVOnsiteinEmail, m_MeetandGreetinEmail, m_CncigMoniteringinEmail;//FB 2632
        private int m_VNOCinEmail; //FB 2632
        public int m_EnablePublicRoomService,m_EnableRoomAdminDetails; //FB 2594//FB 2631
		private string m_AlertforTier1; //FB 2637
        private int m_SecureSwitch, m_NetworkSwitching, m_NetworkCallLaunch, m_SecureLaunchBuffer; //FB 2595
        private string m_HardwareAdminEmail; //FB 2595 
        private int m_EnableZulu, m_iControlTimeout;//FB 2588//FB 2724
		private int m_EnableOnsiteAV, m_EnableMeetandGreet, m_EnableConciergeMonitoring, m_EnableDedicatedVNOC;//FB 2670
        private int m_EnableLinerate, m_EnableStartMode;// FB 2641
		private int m_PCUserLimit, m_EnableBlueJeans,m_EnableJabber, m_EnableLync, m_EnableVidtel; //FB 2693 //ZD 104021
        private int m_MaxROHotdesking, m_MaxVCHotdesking, m_ThemeType,m_SingleRoomConfMail,m_RFIDTagValue;//FB 2694 // FB 2719 Theme//FB 2817//FB 2724
        private int m_Seats, m_MaxParticipants, m_MaxConcurrentCall;//FB 2659 //ZD 100518
        private string m_DefaultSubject, m_DefaultInvitation, m_FileWhiteList;//FB 2659 //ZD 100263
        private int m_EnableAdvancedReport, m_EnableProfileSelection, m_EnableNumericID;//FB 2593 //FB 2870 //FB 2839
        private int m_MCUSetupDisplay, m_MCUTearDisplay;//FB 2998
        private int m_ResponseTimeout, m_EnableFileWhiteList;//FB 2993 //ZD 100263
		private int m_VMRTopTierID, m_VMRMiddleTierID; //ZD 100068
		private int m_ShowCusAttInCalendar;//ZD 100151
        private int m_EnableAdvancedUserOption, m_ShowHideVMR, m_EnablePersonaVMR, m_EnableRoomVMR, m_EnableExternalVMR;//ZD 100164 //ZD 100707
		private string m_WebExPartnerID, m_WebExSiteID, m_WebExURL; //ZD 100221
        private int m_EnableExpressConfType, m_EnableDetailedExpressForm; //ZD 100704 // ZD 100834
        private int m_EnableWebExIntg, m_PasswordMonths, m_EnablePasswordExp; //ZD 100935 //ZD 100781
		private int m_ScheduleLimit; //ZD 100899
        private int m_EnableRoomCalendarView, m_PasswordCharLength, m_VMRPINChange, m_RoomDenialCommt;//ZD 100963 //ZD 100522 //ZD 101445
        private string m_VideoSourceURL1, m_VideoSourceURL2, m_VideoSourceURL3, m_VideoSourceURL4; //ZD 101019
        private string m_ScreenPosition1, m_ScreenPosition2, m_ScreenPosition3, m_ScreenPosition4; //ZD 101019
        private int m_GuestLocApprovalTime, m_EnableGuestLocWarningMsg, m_SmartP2PNotify; //ZD 101120 //ZD 100815
		private int m_MaxiControlRooms, m_WebExLaunch, m_EnableWETConference;//ZD 101098 //ZD 100513
        private int m_AVWOAlertTime, m_CatWOAlertTime, m_FacilityWOAlertTime; //ZD 101228
        private int m_EnableTemplateBooking, m_EnableSetupTimeDisplay, m_EnableTeardownTimeDisplay;//ZD 101562 //ZD 101755
		private int m_EnableActMsgDelivery, m_EnablePartyCode;//ZD 101562 //ZD 101757 //ZD 101446
        private int m_EnableRPRMRoomSync, m_RoomSyncAuto, m_RoomSyncPollTime;//ZD 101527
        private int m_DefCodianLO, m_ShowVideoLayout, m_EnableCalDefaultDisplay;//ZD 101562 //ZD 101931 //ZD 102356
        private string m_EWSConfAdmins, m_PersonnelAlert;//ZD 102085 //ZD 103046
        private int m_EnableWaitList;//ZD 102532
        private int m_VideoRefreshTimer;// ZD 103398
        private int m_OnSiteAVSupportBuffer, m_CallMonitoringBuffer, m_DedicatedVNOCOperatorBuffer;//ZD 102514
        private int m_EnableTravelAvoidTrack; // ZD 103216
        private int m_EnableBJNIntegration, m_BJNMeetingType, m_BJNSelectOption; //ZD 104021
        private int m_BJNDisplay, m_EnablePoolOrderSelection, m_EnableBlockUserDI, m_EnableSendRoomResourceiCal;//ZD 104221 //ZD 104116 //ZD 104862 //ALLDEV-828
        private int m_EmptyConferencePush, m_AssignPartyToRoom, m_EnableAudbridgefreebusy, m_EnableEWSPartyFreeBusy;// ZD 104151 //ZD 102916 //ZD 104854-Disney //ALLDEV-833
		private int m_EnableiControlLogin, m_EnableFindAvailRm, m_AllowRequestortoEdit;// ALLDEV-503 //ALLDEV-839
		private int m_EnableHostGuestPwd; //ALLDEV-826
		private int m_EnableSyncLaunchBuffer, m_SyncLaunchBuffer; //ALLDEV-837
        private string m_ConfAdministrator;  //ALLDEV-498
		private int m_EnableRoomSelection,m_EnablePexipIntegration, m_PexipMeetingType, m_PexipDisplay, m_PexipSelectOption; //ALLDEV-834 //ALLDEV-782 
        private int m_EnableS4BEWS; //ALLDEV-834	//ALLDEV-862	
        #endregion

        #region Public Properties
        
        public DateTime LastModified
        {
            get { return m_LastModified; }
            set { m_LastModified = value; }
        }
        public int LastModifiedUser
        {
            get { return m_LastModifiedUser; }
            set { m_LastModifiedUser = value; }
        }
        public int tzsystemid
        {
            get { return m_tzsystemid; }
            set { m_tzsystemid = value; }
        }
        public string Logo
        {
            get { return m_Logo; }
            set { m_Logo = value; }
        }
        public string CompanyTel
        {
            get { return m_CompanyTel; }
            set { m_CompanyTel = value; }
        }
        public string CompanyEmail
        {
            get { return m_CompanyEmail; }
            set { m_CompanyEmail = value; }
        }
        public string CompanyURL
        {
            get { return m_CompanyURL; }
            set { m_CompanyURL = value; }
        }
        public int Connect2
        {
            get { return m_Connect2; }
            set { m_Connect2 = value; }
        }
        public int DialOut
        {
            get { return m_DialOut; }
            set { m_DialOut = value; }
        }
        public int AccountingLogic
        {
            get { return m_AccountingLogic; }
            set { m_AccountingLogic = value; }
        }
        public int BillPoint2Point
        {
            get { return m_BillPoint2Point; }
            set { m_BillPoint2Point = value; }
        }
        public int AllLocation
        {
            get { return m_AllLocation; }
            set { m_AllLocation = value; }
        }
        public int RealtimeStatus
        {
            get { return m_RealtimeStatus; }
            set { m_RealtimeStatus = value; }
        }
        public int BillRealTime
        {
            get { return m_BillRealTime; }
            set { m_BillRealTime = value; }
        }
        public int MultipleDepartments
        {
            get { return m_MultipleDepartments; }
            set { m_MultipleDepartments = value; }
        }
        public int overAllocation
        {
            get { return m_overAllocation; }
            set { m_overAllocation = value; }
        }
        public int Threshold
        {
            get { return m_Threshold; }
            set { m_Threshold = value; }
        }
        public int autoApproveImmediate
        {
            get { return m_autoApproveImmediate; }
            set { m_autoApproveImmediate = value; }
        }
        public int adminEmail1
        {
            get { return m_adminEmail1; }
            set { m_adminEmail1 = value; }
        }
        public int adminEmail2
        {
            get { return m_adminEmail1; }
            set { m_adminEmail2 = value; }
        }
        public int adminEmail3
        {
            get { return m_adminEmail1; }
            set { m_adminEmail3 = value; }
        }
        public int AutoAcceptModConf
        {
            get { return m_AutoAcceptModConf; }
            set { m_AutoAcceptModConf = value; }
        }
        public int recurEnabled
        {
            get { return m_recurEnabled; }
            set { m_recurEnabled = value; }
        }
        public int responsetime
        {
            get { return m_responsetime; }
            set { m_responsetime = value; }
        }
        public string responsemessage
        {
            get { return m_responsemessage; }
            set { m_responsemessage = value; }
        }
        public DateTime SystemStartTime
        {
            get { return m_SystemStartTime; }
            set { m_SystemStartTime = value; }
        }
        public DateTime SystemEndTime
        {
            get { return m_SystemEndTime; }
            set { m_SystemEndTime = value; }
        }
        public int Open24hrs
        {
            get { return m_Open24hrs; }
            set { m_Open24hrs = value; }
        }
        public string Offdays
        {
            get { return m_Offdays; }
            set { m_Offdays = value; }
        }
        public float ISDNLineCost
        {
            get { return m_ISDNLineCost; }
            set { m_ISDNLineCost = value; }
        }
        public float ISDNPortCost
        {
            get { return m_ISDNPortCost; }
            set { m_ISDNPortCost = value; }
        }
        public float IPLineCost
        {
            get { return m_IPLineCost; }
            set { m_IPLineCost = value; }
        }
        public float IPPortCost
        {
            get { return m_IPPortCost; }
            set { m_IPPortCost = value; }
        }
        public int ISDNTimeFrame
        {
            get { return m_ISDNTimeFrame; }
            set { m_ISDNTimeFrame = value; }
        }
        public int dynamicinviteenabled
        {
            get { return m_dynamicinviteenabled; }
            set { m_dynamicinviteenabled = value; }
        }
        public int doublebookingenabled
        {
            get { return m_doublebookingenabled; }
            set { m_doublebookingenabled = value; }
        }
        public int IMEnabled
        {
            get { return m_IMEnabled; }
            set { m_IMEnabled = value; }
        }
        public float IMRefreshConn
        {
            get { return m_IMRefreshConn; }
            set { m_IMRefreshConn = value; }
        }
        public int IMMaxUnitConn
        {
            get { return m_IMMaxUnitConn; }
            set { m_IMMaxUnitConn = value; }
        }
        public int IMMaxSysConn
        {
            get { return m_IMMaxSysConn; }
            set { m_IMMaxSysConn = value; }
        }
        public int DefaultToPublic
        {
            get { return m_DefaultToPublic; }
            set { m_DefaultToPublic = value; }
        }
        public int RoomLimit
        {
            get { return m_RoomLimit; }
            set { m_RoomLimit = value; }
        }
        public int MCULimit
        {
            get { return m_MCULimit; }
            set { m_MCULimit = value; }
        }

        public int MCUEnchancedLimit //FB 2486
        {
            get { return m_MCUEnchancedLimit; }
            set { m_MCUEnchancedLimit = value; }
        }
        
        public int UserLimit
        {
            get { return m_UserLimit; }
            set { m_UserLimit = value; }
        }
        public int ExchangeUserLimit
        {
            get { return m_ExchangeUserLimit; }
            set { m_ExchangeUserLimit = value; }
        }
        //FB 2426 Start
        public int GuestRoomLimit
        {
            get { return m_GuestRoomLimit; }
            set { m_GuestRoomLimit = value; }
        }
        public int GuestRoomPerUser
        {
            get { return m_GuestRoomPerUser; }
            set { m_GuestRoomPerUser = value; }
        }
        //FB 2426 End
        public int DominoUserLimit
        {
            get { return m_DominoUserLimit; }
            set { m_DominoUserLimit = value; }
        }
        public int MobileUserLimit // FB 1979
        {
            get { return m_MobileUserLimit; }
            set { m_MobileUserLimit = value; }
        }
        public int WebexUserLimit //ZD 100221
        {
            get { return m_WebexUserLimit; }
            set { m_WebexUserLimit = value; }
        }
		//ZD 104021 Start
        public int BlueJeansUserLimit 
        {
            get { return m_BlueJeansUserLimit; }
            set { m_BlueJeansUserLimit = value; }
        }
        public int EnableBlueJeans 
        {
            get { return m_EnableBlueJeans; }
            set { m_EnableBlueJeans = value; }
        }
		//ZD 104021 End
        public int DefaultConferenceType
        {
            get { return m_DefaultConferenceType; }
            set { m_DefaultConferenceType = value; }
        }
        public int RFIDTagValue //FB 2724
        {
            get { return m_RFIDTagValue; }
            set { m_RFIDTagValue = value; }
        }
        public int EnableRoomConference
        {
            get { return m_EnableRoomConference; }
            set { m_EnableRoomConference = value; }
        }
        public int EnableAudioVideoConference
        {
            get { return m_EnableAudioVideoConference; }
            set { m_EnableAudioVideoConference = value; }
        }
        public int EnableAudioOnlyConference
        {
            get { return m_EnableAudioOnlyConference; }
            set { m_EnableAudioOnlyConference = value; }
        }
        //ZD 100719 - Start
        public int EnableHotdeskingConference
        {
            get { return m_EnableHotdeskingConference; }
            set { m_EnableHotdeskingConference = value; }
        }
        //ZD 100719 - End
        public int DefaultCalendarToOfficeHours
        {
            get { return m_DefaultCalendarToOfficeHours; }
            set { m_DefaultCalendarToOfficeHours = value; }
        }
        public string RoomTreeExpandLevel
        {
            get { return m_RoomTreeExpandLevel; }
            set { m_RoomTreeExpandLevel = value; }
        }
        public string EnableCustomOption
        {
            get { return m_EnableCustomOption; }
            set { m_EnableCustomOption = value; }
        }
        public string EnableBufferZone
        {
            get { return m_EnableBufferZone; }
            set { m_EnableBufferZone = value; }
        }
        
        public int OrgId
        {
            get { return m_OrgId; }
            set { m_OrgId = value; }
        }
        public int MaxNonVideoRooms
        {
            get { return m_MaxNonVideoRooms; }
            set { m_MaxNonVideoRooms = value; }
        }
        public int MaxVideoRooms
        {
            get { return m_MaxVideoRooms; }
            set { m_MaxVideoRooms = value; }
        }
        //FB 2586 Start
        public int MaxVMRRooms
        {
            get { return m_MaxVMRRooms; }
            set { m_MaxVMRRooms = value; }
        }
        //FB 2586 End
        public int MaxEndpoints
        {
            get { return m_MaxEndpoints; }
            set { m_MaxEndpoints = value; }
        }
        public int MaxCDRs
        {
            get { return m_MaxCDRs; }
            set { m_MaxCDRs = value; }
        }
        public int EnableFacilities
        {
            get { return m_EnableFacilities; }
            set { m_EnableFacilities = value; }
        }
        public int EnableCatering
        {
            get { return m_EnableCatering; }
            set { m_EnableCatering = value; }
        }
        public int EnableHousekeeping
        {
            get { return m_EnableHousekeeping; }
            set { m_EnableHousekeeping = value; }
        }
        public int EnableAPI
        {
            get { return m_EnableAPI; }
            set { m_EnableAPI = value; }
        }
        public int Language
        {
            get { return m_Language; }
            set { m_Language = value; }
        }
        public int LogoImageId //Image Project
        {
            get { return m_LogoImageId; }
            set { m_LogoImageId = value; }
        }
        public int LobytopHighImageId//Image Project
        {
            get { return m_LobytopHighImageId; }
            set { m_LobytopHighImageId = value; }
        }
        public int LobytopImageId //Image Project
        {
            get { return m_LobytopImageId; }
            set { m_LobytopImageId = value; }
        }

        public string LogoImageName //Image Project
        {
            get { return m_LogoImageName; }
            set { m_LogoImageName = value; }
        }
        public string LobytopImageName //Image Project
        {
            get { return m_LobytopImageName; }
            set { m_LobytopImageName = value; }
        }
        public string LobytopHighImageName //Image Project
        {
            get { return m_LobytopHighImageName; }
            set { m_LobytopHighImageName = value; }
        }

        public int  MailLogo//FB 1610
        {
            get { return m_MailLogoID; }
            set { m_MailLogoID = value; }
        }
        public int DefaultCssId
        {
            get { return m_DefaultCssId; }
            set { m_DefaultCssId = value; }
        }
        public int MirrorCssId
        {
            get { return m_MirrorCssId; }
            set { m_MirrorCssId = value; }
        }
        public int ArtifactsCssId
        {
            get { return m_ArtifactsCssId; }
            set { m_ArtifactsCssId = value; }
        }
        public int TextchangeId
        {
            get { return m_TextchangeId; }
            set { m_TextchangeId = value; }
        }
        //Audio Add On..
        public int ConferenceCode
        {
            get { return m_ConferenceCode; }
            set { m_ConferenceCode = value; }
        }

        public int LeaderPin
        {
            get { return m_LeaderPin; }
            set { m_LeaderPin = value; }
        }

        public int AdvAvParams
        {
            get { return m_AdvAvParams; }
            set { m_AdvAvParams = value; }
        }

        public int AudioParams
        {
            get { return m_AudioParams; }
            set { m_AudioParams = value; }
        }
		
        public string FooterMessage //Added for FB 1710
        {
            get { return m_FooterMessage; }
            set { m_FooterMessage = value; }
        }

        public int FooterImage //Added for FB 1710
        {
            get { return m_FooterImage; }
            set { m_FooterImage = value; }
        }
        public string IcalReqEmailID //FB 1786
        {
            get { return m_IcalReqEmailID; }
            set { m_IcalReqEmailID = value; }
        }
        public int CustomAttributeLimit //FB 1779
        {
            get { return m_CustomAttributeLimit; }
            set { m_CustomAttributeLimit = value; }
        }
		public int SendIcal //FB 1782
        {
            get { return m_sendIcal; }
            set { m_sendIcal = value; }
        }
        public int SendApprovalIcal //FB 1782
        {
            get { return m_sendApprovalIcal; }
            set { m_sendApprovalIcal = value; }
        }
        public int EmailLangId //FB 1830
        {
            get { return m_EmailLangId; }
            set { m_EmailLangId = value; }
        }

        public int isVIP // FB 1864
        {
            get { return m_isVIP; }
            set { m_isVIP = value; }
        }

        public int isUniquePassword // FB 1865
        {
            get { return m_isUniquePassword; }
            set { m_isUniquePassword = value; }
        }

        public int MailBlocked// FB 1860
         {
             get { return m_mailBlocked; }
             set { m_mailBlocked = value; }
         }

        public DateTime MailBlockedDate// FB 1860
         {
             get { return m_mailBlockedDate; }
             set { m_mailBlockedDate = value; }
         }

        public int isAssignedMCU// FB 1901
        {
            get { return m_isAssignedMCU; }
            set { m_isAssignedMCU = value; }
        }

        public int ReminderMask// FB 1926
        {
            get { return m_ReminderMask; }
            set { m_ReminderMask = value; }
        }

        public int isMultiLingual// FB 1926
        {
            get { return m_isMultiLingual; }
            set { m_isMultiLingual = value; }
        }

		public int PluginConfirmations// FB 2141
        {
            get { return m_PluginConfirmations; }
            set { m_PluginConfirmations = value; }
        }

        public int SendAttachmentsExternal// FB 2154
        {
            get { return m_SendAttachmentsExternal; }
            set { m_SendAttachmentsExternal = value; }
        }

        public int TelepresenceFilter// FB 2170
        {
            get { return m_TelepresenceFilter; }
            set { m_TelepresenceFilter = value; }
        }
        public int EnableRoomServiceType// FB 2219
        {
            get { return m_EnableRoomServiceType; }
            set { m_EnableRoomServiceType= value; }
        }
        public int SpecialRecur// FB 2052
        {
            get { return m_SpecialRecur; }
            set { m_SpecialRecur = value; }
        }

        public int isDeptUser //FB 2269
        {
            get { return m_isDeptUser; }
            set { m_isDeptUser = value; }
        }
		public int EnablePIMServiceType// FB 2038
        {
            get { return m_EnablePIMServiceType; }
            set { m_EnablePIMServiceType = value; }
        }
        public int EnableImmediateConference// FB 2036
        {
            get { return m_EnableImmediateConference; }
            set { m_EnableImmediateConference = value; }
        }
        public int DefaultConfDuration//FB 2501
        {
            get { return m_DefaultConfDuration; }
            set { m_DefaultConfDuration = value; }
        }
        public int EnableAudioBridges// FB 2023
        {
            get { return m_EnableAudioBridges; }
            set { m_EnableAudioBridges = value; }
        }
        //FB 2359 Start
        public int EnablePublicConf 
        {
            get { return m_EnablePublicConf; }
            set { m_EnablePublicConf = value; }
        }
        public int EnableConfPassword
        {
            get { return m_EnableConfPassword; }
            set { m_EnableConfPassword = value; }
        }
        public int EnableRoomParam 
        {
            get { return m_EnableRoomParam; }
            set { m_EnableRoomParam = value; }
        }
        //FB 2359 End
        public int EnableSecurityBadge// FB 2136
        {
            get { return m_EnableSecurityBadge; }
            set { m_EnableSecurityBadge = value; }
        }
        public int SecurityBadgeType// FB 2136
        {
            get { return m_SecurityBadgeType; }
            set { m_SecurityBadgeType = value; }
        }
        public string SecurityDeskEmailId//FB 2136
        {
            get { return m_SecurityDeskEmailId; }
            set { m_SecurityDeskEmailId = value; }
        }
        public int EnablePasswordRule// FB 2339
        {
            get { return m_EnablePasswordRule; }
            set { m_EnablePasswordRule = value; }
        }

        public int DefPolycomRMXLO// FB 2335
        {
            get { return m_DefPolycomRMXLO; }
            set { m_DefPolycomRMXLO = value; }
        }
        public int DefPolycomMGCLO// FB 2335
        {
            get { return m_DefPolycomMGCLO; }
            set { m_DefPolycomMGCLO = value; }
        }
        public int DefCiscoTPLO// FB 2335
        {
            get { return m_DefCiscoTPLO; }
            set { m_DefCiscoTPLO = value; }
        }
        public int DefCTMSLO// FB 2335
        {
            get { return m_DefCTMSLO; }
            set { m_DefCTMSLO = value; }
        }
        public int DedicatedVideo// FB 2334
        {
            get { return m_DedicatedVideo; }
            set { m_DedicatedVideo = value; }
        }       
		public int WorkingHours //FB 2343
        {
            get { return m_WorkingHours; }
            set { m_WorkingHours = value; }
        }
        public int EnableVMR //VMR
        {
            get { return m_EnableVMR; }
            set { m_EnableVMR = value; }
        }
//FB 2348
        public int EnableSurvey
        {
            get { return m_EnableSurvey; }
            set { m_EnableSurvey = value; }
        }
        public int SurveyOption
        {
            get { return m_SurveyOption; }
            set { m_SurveyOption = value; }
        }
        public string SurveyURL
        {
            get { return m_SurveyURL; }
            set { m_SurveyURL = value; }
        }
        public int TimeDuration
        {
            get { return m_TimeDuration; }
            set { m_TimeDuration = value; }
        }
        //public int EnablePCModule //FB 2347 //FB 2693
        //{
        //    get { return m_EnablePCModule; }
        //    set { m_EnablePCModule = value; }
        //}
        public int EnableEPDetails //FB 2401
        {
            get { return m_EnableEPDetails; }
            set { m_EnableEPDetails = value; }
        }
        public int SetupTime  //FB 2398
        {
            get { return m_SetupTime; }
            set { m_SetupTime = value; }
        }
        public int TearDownTime
        {
            get { return m_TearDownTime; }
            set { m_TearDownTime = value; }
        }
        //FB 2363
        public string CustomerName
        {
            get { return m_customerName; }
            set { m_customerName = value; }
        }
        public string CustomerID
        {
            get { return m_customerID; }
            set { m_customerID = value; }
        }
        public int EnableAcceptDecline //FB 2419
        {
            get { return m_EnableAcceptDecline; }
            set { m_EnableAcceptDecline = value; }
        }
		public int DefLinerate //FB 2429
        {
            get { return m_DefLinerate; }
            set { m_DefLinerate = value; }
        }
        public int McuSetupTime //FB 2440
        {
            get { return m_McuSetupTime; }
            set { m_McuSetupTime = value; }
        }
        public int MCUTeardonwnTime //FB 2440
        {
            get { return m_MCUTeardonwnTime; }
            set { m_MCUTeardonwnTime = value; }
        }
        public int MCUBufferPriority //FB 2440
        {
            get { return m_MCUBufferPriority; }
            set { m_MCUBufferPriority = value; }
        }
        //FB 2426 Start
        public string TopTier
        {
            get { return m_TopTier; }
            set { m_TopTier = value; }
        }
        public string MiddleTier
        {
            get { return m_MiddleTier; }
            set { m_MiddleTier = value; }
        }
        public int OnflyTopTierID
        {
            get { return m_OnflyTopTierID; }
            set { m_OnflyTopTierID = value; }
        }
        public int OnflyMiddleTierID
        {
            get { return m_OnflyMiddleTierID; }
            set { m_OnflyMiddleTierID = value; }
        }
        //FB 2426 End
        //FB 2469 - Starts
        public int EnableConfTZinLoc
        {
            get { return m_EnableConfTZinLoc; }
            set { m_EnableConfTZinLoc = value; }
        }
        public int SendConfirmationEmail
        {
            get { return m_SendConfirmationEmail; }
            set { m_SendConfirmationEmail = value; }
        }
        //FB 2469 - Starts

        public int EnableSmartP2P //FB 2430
        {
            get { return m_EnableSmartP2P; }
            set { m_EnableSmartP2P = value; }
        }

        public int MCUAlert //FB 2472
        {
            get { return m_MCUAlert; }
            set { m_MCUAlert = value; }
        }

        public string DefaultTxtMsg
        {
            get { return m_DefaultTxtMsg; }
            set { m_DefaultTxtMsg = value; }
        }
        //FB 2501 EM7 starts
        public int EM7OrgId
        {
            get { return m_EM7OrgID; }
            set { m_EM7OrgID = value; }
        }
        public string EM7UserName
        {
            get { return m_EM7UserName; }
            set { m_EM7UserName = value; }
        }
        public string EM7Password
        {
            get { return m_EM7Password; }
            set { m_EM7Password = value; }
        }
        //FB 2501 EM7 Ends
		//FB 2550 Starts
        public int MaxPublicVMRParty
        {
            get { return m_MaxPublicVMRParty; }
            set { m_MaxPublicVMRParty = value; }                
        }
        //FB 2550 Ends
		public int EmailDateFormat   //FB 2555
        {
            get { return m_EmailDateFormat; }
            set { m_EmailDateFormat = value; }
        }
        //FB 2571 Start
        public int EnableFECC
        {
            get { return m_EnableFECC; }
            set { m_EnableFECC = value; }
        }
        public int DefaultFECC
        {
            get { return m_DefaultFECC; }
            set { m_DefaultFECC = value; }
        }
        //FB 2571 End
        //FB 2598 Starts
        //DD Feature
        public int EnableCallmonitor
        {
            get { return m_EnableCallmonitor; }
            set { m_EnableCallmonitor = value; }
        }
        public int EnableEM7
        {
            get { return m_EnableEM7; }
            set { m_EnableEM7 = value; }
        }
        public int EnableCDR
        {
            get { return m_EnableCDR; }
            set { m_EnableCDR = value; }
        }
        //FB 2598 Ends

        //FB 2636 Starts
        public int EnableDialPlan
        {
            get { return m_EnableDialPlan; }
            set { m_EnableDialPlan = value; }
        }
        //FB 2636 Ends

        public int IsBridgeExtNo //FB 2610
        {
            get { return m_IsBridgeExtNo; }
            set { m_IsBridgeExtNo = value; }
        }
        //FB 2609 Starts
        public int MeetandGreetBuffer
        {
            get { return m_MeetandGreetBuffer; }
            set { m_MeetandGreetBuffer = value; }
        }
        //FB 2609 Ends
        //FB 2632 Starts
        public int EnableCncigSupport
        {
            get { return m_EnableCncigSupport; }
            set { m_EnableCncigSupport = value; }
        }
        public int AVOnsiteinEmail
        {
            get { return m_AVOnsiteinEmail; }
            set { m_AVOnsiteinEmail = value; }
        }
        public int MeetandGreetinEmail
        {
            get { return m_MeetandGreetinEmail; }
            set { m_MeetandGreetinEmail = value; }
        }
        public int CncigMoniteringinEmail
        {
            get { return m_CncigMoniteringinEmail; }
            set { m_CncigMoniteringinEmail = value; }
        }
        public int VNOCinEmail
        {
            get { return m_VNOCinEmail; }
            set { m_VNOCinEmail = value; }
        }
        //FB 2632 Ends
		//FB 2599 Start
		public int EnableCloud //FB 2262 - J
        {
            get { return m_EnableCloud; }
            set { m_EnableCloud = value; }
        }
        //FB 2599 End
		public int EnablePublicRoomService //FB 2594//FB 2645
        {
            get { return m_EnablePublicRoomService; }
            set { m_EnablePublicRoomService = value; }
        }
        //FB 2631
        public int EnableRoomAdminDetails
        {
            get { return m_EnableRoomAdminDetails; }
            set { m_EnableRoomAdminDetails = value; }
        }
		public string AlertforTier1 //FB 2637
        {
            get { return m_AlertforTier1; }
            set { m_AlertforTier1 = value; }
        }
        //FB 2595 Starts
        public String HardwareAdminEmail
        {
            get { return m_HardwareAdminEmail; }
            set { m_HardwareAdminEmail = value; }
        }
        public int SecureSwitch
        {
            get { return m_SecureSwitch; }
            set { m_SecureSwitch = value; }
        }
        public int NetworkSwitching
        {
            get { return m_NetworkSwitching; }
            set { m_NetworkSwitching = value; }
        }
        public int NetworkCallLaunch
        {
            get { return m_NetworkCallLaunch; }
            set { m_NetworkCallLaunch = value; }
        }
        public int SecureLaunchBuffer
        {
            get { return m_SecureLaunchBuffer; }
            set { m_SecureLaunchBuffer = value; }
        }
        //FB 2595 Ends
        public int EnableZulu//FB 2588
        {
            get { return m_EnableZulu; }
            set { m_EnableZulu = value; }
        }
        // FB 2641 Start
        public int EnableLinerate
        {
            get { return m_EnableLinerate; }
            set { m_EnableLinerate = value; }
        }
        public int EnableStartMode
        {
            get { return m_EnableStartMode; }
            set { m_EnableStartMode = value; }
        }

       
        // FB 2641 End
 	    //FB 2670 START
        public int EnableOnsiteAV
        {
            get { return m_EnableOnsiteAV; }
            set { m_EnableOnsiteAV = value; }
        }
        public int EnableMeetandGreet
        {
            get { return m_EnableMeetandGreet; }
            set { m_EnableMeetandGreet = value; }
        }
        public int EnableConciergeMonitoring
        {
            get { return m_EnableConciergeMonitoring; }
            set { m_EnableConciergeMonitoring = value; }
        }
        public int EnableDedicatedVNOC
        {
            get { return m_EnableDedicatedVNOC; }
            set { m_EnableDedicatedVNOC = value; }
        }
        //FB 2670 END
		//FB 2693 Starts
        public int PCUserLimit
        {
            get { return m_PCUserLimit; }
            set { m_PCUserLimit = value; }
        }
       
        public int EnableJabber
        {
            get { return m_EnableJabber; }
            set { m_EnableJabber = value; }
        }
        public int EnableLync
        {
            get { return m_EnableLync; }
            set { m_EnableLync = value; }
        }
        public int EnableVidtel
        {
            get { return m_EnableVidtel; }
            set { m_EnableVidtel = value; }
        }
        //FB 2693 Ends
		//FB 2694 Starts
        public int MaxROHotdesking
        {
            get { return m_MaxROHotdesking; }
            set { m_MaxROHotdesking = value; }
        }
        public int MaxVCHotdesking
        {
            get { return m_MaxVCHotdesking; }
            set { m_MaxVCHotdesking = value; }
        }
        public int ThemeType // FB 2719 Theme
        {
            get { return m_ThemeType; }
            set { m_ThemeType = value; }
        }
		//FB 2694 End

		public int SingleRoomConfMail //FB 2817
        {
            get { return m_SingleRoomConfMail; }
            set { m_SingleRoomConfMail = value; }
        }
		
		//FB 2659 Starts
        public int Seats
        {
            get { return m_Seats; }
            set { m_Seats = value; }
        }
        public string DefaultSubject
        {
            get { return m_DefaultSubject; }
            set { m_DefaultSubject = value; }
        }
        public string DefaultInvitation
        {
            get { return m_DefaultInvitation; }
            set { m_DefaultInvitation = value; }
        }
        //FB 2659 Ends
		//FB 2724 Start
        public int iControlTimeout
        {
            get { return m_iControlTimeout; }
            set { m_iControlTimeout = value; }
        }
        //FB 2724 End
        //FB 2593 Start
        public int EnableAdvancedReport
        {
            get { return m_EnableAdvancedReport; }
            set { m_EnableAdvancedReport = value; }
        }
        //FB 2593 End
        //FB 2839 Start
        public int EnableProfileSelection
        {
            get { return m_EnableProfileSelection; }
            set { m_EnableProfileSelection = value; }
        }
        //FB 2839 End
        //FB 2870 Start
        public int EnableNumericID
        {
            get { return m_EnableNumericID; }
            set { m_EnableNumericID = value; }
        }
        //FB 2870 End
        //FB 2998
        public int MCUSetupDisplay
        {
            get { return m_MCUSetupDisplay; }
            set { m_MCUSetupDisplay = value; }
        }
        public int MCUTearDisplay
        {
            get { return m_MCUTearDisplay; }
            set { m_MCUTearDisplay = value; }
        }
        //FB 2993 start
        public int ResponseTimeout
        {
            get { return m_ResponseTimeout; }
            set { m_ResponseTimeout = value; }
        }
        // FB 2993 End
        //ZD 100263
        public int EnableFileWhiteList
        {
            get { return m_EnableFileWhiteList; }
            set { m_EnableFileWhiteList = value; }
        }
        public string FileWhiteList
        {
            get { return m_FileWhiteList; }
            set { m_FileWhiteList = value; }
        }
		//ZD 100151
        public int ShowCusAttInCalendar
        {
            get { return m_ShowCusAttInCalendar; }
            set { m_ShowCusAttInCalendar = value; }
        }
        //ZD 100164 START
        public int EnableAdvancedUserOption 
        {
            get { return m_EnableAdvancedUserOption; }
            set { m_EnableAdvancedUserOption = value; }
        }
        //ZD 100164 END
		 //ZD 100068 Start
       
        public int VMRTopTierID
        {
            get { return m_VMRTopTierID; }
            set { m_VMRTopTierID = value; }
        }
        public int VMRMiddleTierID
        {
            get { return m_VMRMiddleTierID; }
            set { m_VMRMiddleTierID = value; }
        }
       
        //ZD 100068 End
        //ZD 100518 Start
        public int MaxParticipants
        {
            get { return m_MaxParticipants; }
            set { m_MaxParticipants = value; }
        }
        public int MaxConcurrentCall
        {
            get { return m_MaxConcurrentCall; }
            set { m_MaxConcurrentCall = value; }
        }
        //ZD 100518 End
        //ZD 100707 START
        public int ShowHideVMR
        {
            get { return m_ShowHideVMR; }
            set { m_ShowHideVMR = value; }
        }
        public int EnablePersonaVMR
        {
            get { return m_EnablePersonaVMR; }
            set { m_EnablePersonaVMR = value; }
        }
        public int EnableRoomVMR
        {
            get { return m_EnableRoomVMR; }
            set { m_EnableRoomVMR = value; }
        }
        public int EnableExternalVMR
        {
            get { return m_EnableExternalVMR; }
            set { m_EnableExternalVMR = value; }
        }
        //ZD 100707 END
		//ZD 100221 Starts
        public string WebExURL
        {
            get { return m_WebExURL; }
            set { m_WebExURL = value; }
        }
        public string WebExSiteID
        {
            get { return m_WebExSiteID; }
            set { m_WebExSiteID = value; }
        }
        public string WebExPartnerID
        {
            get { return m_WebExPartnerID; }
            set { m_WebExPartnerID = value; }
        }
        //ZD 100221 Ends
        //ZD 100704 Starts
        public int EnableExpressConfType
        {
            get { return m_EnableExpressConfType; }
            set { m_EnableExpressConfType = value; }
        }
        //ZD 100704 Ends
        //ZD 100834 Starts
        public int EnableDetailedExpressForm
        {
            get { return m_EnableDetailedExpressForm; }
            set { m_EnableDetailedExpressForm = value; }
        }
        // ZD 100834 End
 		public int EnableWebExIntg //ZD 100935
        {
            get { return m_EnableWebExIntg; }
            set { m_EnableWebExIntg = value; }
        }
		//ZD 100899 start
        public int ScheduleLimit
        {
            get { return m_ScheduleLimit; }
            set { m_ScheduleLimit = value; }
        }
        //ZD 100899 End
        //ZD 100781 Starts
        public int EnablePasswordExp 
        {
            get { return m_EnablePasswordExp; }
            set { m_EnablePasswordExp = value; }
        }
        public int PasswordMonths
        {
            get { return m_PasswordMonths; }
            set { m_PasswordMonths = value; }
        }
        //ZD 100781 Ends

		//ZD 100963 Starts
        public int EnableRoomCalendarView
        {
            get { return m_EnableRoomCalendarView; }
            set { m_EnableRoomCalendarView = value; }
        }
        //ZD 100963 Ends
        
        //ZD 101019 START
        public string VideoSourceURL1
        {
            get { return m_VideoSourceURL1; }
            set { m_VideoSourceURL1 = value; }
        }

        public string ScreenPosition1
        {
            get { return m_ScreenPosition1; }
            set { m_ScreenPosition1 = value; }
        }

        public string VideoSourceURL2
        {
            get { return m_VideoSourceURL2; }
            set { m_VideoSourceURL2 = value; }
        }

        public string ScreenPosition2
        {
            get { return m_ScreenPosition2; }
            set { m_ScreenPosition2 = value; }
        }

        public string VideoSourceURL3
        {
            get { return m_VideoSourceURL3; }
            set { m_VideoSourceURL3 = value; }
        }

        public string ScreenPosition3
        {
            get { return m_ScreenPosition3; }
            set { m_ScreenPosition3 = value; }
        }

        public string VideoSourceURL4
        {
            get { return m_VideoSourceURL4; }
            set { m_VideoSourceURL4 = value; }
        }

        public string ScreenPosition4
        {
            get { return m_ScreenPosition4; }
            set { m_ScreenPosition4 = value; }
        }
        //ZD 101019 END
        //ZD 101120 Starts
        public int EnableGuestLocWarningMsg
        {
            get { return m_EnableGuestLocWarningMsg; }
            set { m_EnableGuestLocWarningMsg = value; }
        }
        public int GuestLocApprovalTime
        {
            get { return m_GuestLocApprovalTime; }
            set { m_GuestLocApprovalTime = value; }
        }
        //ZD 101120 Ends
		//ZD 100522 Starts
        public int VMRPINChange
        {
            get { return m_VMRPINChange; }
            set { m_VMRPINChange = value; }
        }
        //ZD 101445 - Start
        public int RoomDenialCommt
        {
            get { return m_RoomDenialCommt; }
            set { m_RoomDenialCommt = value; }
        }
        //ZD 101445 - End
        public int PasswordCharLength
        {
            get { return m_PasswordCharLength; }
            set { m_PasswordCharLength = value; }
        }
        //ZD 100522 Ends
		//ZD 101098 Start
        public int MaxiControlRooms
        {
            get { return m_MaxiControlRooms; }
            set { m_MaxiControlRooms = value; }
        }
        //ZD 101098 End
		//ZD 100815 Start
        public int SmartP2PNotify
        {
            get { return m_SmartP2PNotify; }
            set { m_SmartP2PNotify = value; }
        }
        //ZD 100815 End
        //ZD 101228 Starts
        public int FacilityWOAlertTime
        {
            get { return m_FacilityWOAlertTime; }
            set { m_FacilityWOAlertTime = value; }
        }
        public int AVWOAlertTime
        {
            get { return m_AVWOAlertTime; }
            set { m_AVWOAlertTime = value; }
        }
        public int CatWOAlertTime
        {
            get { return m_CatWOAlertTime; }
            set { m_CatWOAlertTime = value; }
        }
        //ZD 101228 Ends
		//ZD 100513 Starts
        public int EnableWETConference
        {
            get { return m_EnableWETConference; }
            set { m_EnableWETConference = value; }
        }
        public int WebExLaunch
        {
            get { return m_WebExLaunch; }
            set { m_WebExLaunch = value; }
        }
        //ZD 100513 Ends
        
        //ZD 101562 - Start
        public int EnableTemplateBooking
        {
            get { return m_EnableTemplateBooking; }
            set { m_EnableTemplateBooking = value; }
        }
        //ZD 101562 - End
        
        //ZD 101755 start
        public int EnableSetupTimeDisplay
        {
            get { return m_EnableSetupTimeDisplay; }
            set { m_EnableSetupTimeDisplay = value; }
        }
        public int EnableTeardownTimeDisplay
        {
            get { return m_EnableTeardownTimeDisplay; }
            set { m_EnableTeardownTimeDisplay = value; }
        }
        //ZD 101755 end
		//ZD 101757
        public int EnableActMsgDelivery
        {
            get { return m_EnableActMsgDelivery; }
            set { m_EnableActMsgDelivery = value; }
        }
		//ZD 101527 - Start
        public int EnableRPRMRoomSync
        {
            get { return m_EnableRPRMRoomSync; }
            set { m_EnableRPRMRoomSync = value; }
        }
        public int RoomSyncAuto
        {
            get { return m_RoomSyncAuto; }
            set { m_RoomSyncAuto = value; }
        }
        public int RoomSyncPollTime
        {
            get { return m_RoomSyncPollTime; }
            set { m_RoomSyncPollTime = value; }
        }
        //ZD 101527 - End
		public int EnablePartyCode //ZD 101446
        {
            get { return m_EnablePartyCode; }
            set { m_EnablePartyCode = value; }
        }
        //ZD 101869 - Start
        public int DefCodianLO
        {
            get { return m_DefCodianLO; }
            set { m_DefCodianLO = value; }
        }
        //ZD 101869 - End
        //ZD 101931 start   
        public int ShowVideoLayout
        {
            get{return m_ShowVideoLayout;}
            set{m_ShowVideoLayout = value;}
        }
        //ZD 101931 End
        public string EWSConfAdmins //ZD 102085
        {
            get { return m_EWSConfAdmins; }
            set { m_EWSConfAdmins = value; }
        }
        //ZD 102356
        public int EnableCalDefaultDisplay
        {
            get { return m_EnableCalDefaultDisplay; }
            set { m_EnableCalDefaultDisplay = value; }
        }
        
		//ZD 102532
        public int EnableWaitList 
        {
            get { return m_EnableWaitList; }
            set { m_EnableWaitList = value; }
        }
        //ZD 103046 start
        public string PersonnelAlert 
        {
            get { return m_PersonnelAlert; }
            set { m_PersonnelAlert = value; }
        }
        //ZD 103046 End
        //ZD 103398
        public int VideoRefreshTimer
        {
            get { return m_VideoRefreshTimer; }
            set { m_VideoRefreshTimer = value; }
        }
        //ZD 102514 Starts
        public int OnSiteAVSupportBuffer
        {
            get { return m_OnSiteAVSupportBuffer; }
            set { m_OnSiteAVSupportBuffer = value; }
        }
        public int CallMonitoringBuffer
        {
            get { return m_CallMonitoringBuffer; }
            set { m_CallMonitoringBuffer = value; }
        }
        public int DedicatedVNOCOperatorBuffer
        {
            get { return m_DedicatedVNOCOperatorBuffer; }
            set { m_DedicatedVNOCOperatorBuffer = value; }
        }
        //ZD 102514 End
        // ZD 103216 Start
        public int EnableTravelAvoidTrack
        {
            get { return m_EnableTravelAvoidTrack; }
            set { m_EnableTravelAvoidTrack = value; }
        }
        // ZD 103216 End
        //ZD 104021 Start
        public int EnableBJNIntegration
        {
            get { return m_EnableBJNIntegration; }
            set { m_EnableBJNIntegration = value; }
        }        
        public int BJNMeetingType
        {
            get { return m_BJNMeetingType; }
            set { m_BJNMeetingType = value; }
        }
        public int BJNSelectOption
        {
            get { return m_BJNSelectOption; }
            set { m_BJNSelectOption = value; }
        }        
        //ZD 104021 End
        // ZD 104151 Start
        public int EmptyConferencePush
        {
            get { return m_EmptyConferencePush; }
            set { m_EmptyConferencePush = value; }
        }
        // ZD 104151 End
        //ZD 104116 - Start
        public int BJNDisplay
        {
            get { return m_BJNDisplay; }
            set { m_BJNDisplay = value; }
        }
        //ZD 104116 - End
        //ZD 104221 start
        public int EnablePoolOrderSelection
        {
            get { return m_EnablePoolOrderSelection; }
            set { m_EnablePoolOrderSelection = value; }
        }
        //ZD 104221  End

        //ZD 102916 Start 
        public int AssignPartyToRoom
        {
            get { return m_AssignPartyToRoom; }
            set { m_AssignPartyToRoom = value; }
        }
        //ZD 102916 End

        // ZD 104854-Disney Start 
        public int EnableAudbridgefreebusy
        {
            get { return m_EnableAudbridgefreebusy; }
            set { m_EnableAudbridgefreebusy = value; }
        }
        // ZD 104854-Disney End
 		//ZD 104862 Start
        public int EnableBlockUserDI
        {
            get { return m_EnableBlockUserDI; }
            set { m_EnableBlockUserDI = value; }
        }
        //ZD 104862 End
        //ALLDEV-828 START
        public int EnableSendRoomResourceiCal
        {
            get { return m_EnableSendRoomResourceiCal; }
            set { m_EnableSendRoomResourceiCal = value; }
        }
        //ALLDEV-828 END
        //ALLDEV-503 Start
        public int EnableiControlLogin
        {
            get { return m_EnableiControlLogin; }
            set { m_EnableiControlLogin = value; }
        }
        public int EnableFindAvailRm
        {
            get { return m_EnableFindAvailRm; }
            set { m_EnableFindAvailRm = value; }
        }
        //ALLDEV-503 End

        //ALLDEV-826 Starts
        public int EnableHostGuestPwd
        {
            get { return m_EnableHostGuestPwd; }
            set { m_EnableHostGuestPwd = value; }
        }
        //ALLDEV-826 Ends
		//ALLDEV-837 Starts
        public int EnableSyncLaunchBuffer
        {
            get { return m_EnableSyncLaunchBuffer; }
            set { m_EnableSyncLaunchBuffer = value; }
        }
        public int SyncLaunchBuffer
        {
            get { return m_SyncLaunchBuffer; }
            set { m_SyncLaunchBuffer = value; }
        }
        //ALLDEV-837 Ends
        //ALLDEV-498 Starts
        public string ConfAdministrator
        {
            get { return m_ConfAdministrator; }
            set { m_ConfAdministrator = value; }
        }
        //ALLDEV-498 Ends

		//ALLDEV-839 start
        public int AllowRequestortoEdit
        {
            get { return m_AllowRequestortoEdit;}
            set { m_AllowRequestortoEdit = value;}
        }
        //ALLDEV-839 End
        //ALLDEV-834 Starts
        public int EnableRoomSelection
        {
            get { return m_EnableRoomSelection; }
            set { m_EnableRoomSelection = value; }
        }
        //ALLDEV-834 Ends
		//ALLDEV-833 START
        public int EnableEWSPartyFreeBusy
        {
            get { return m_EnableEWSPartyFreeBusy; }
            set { m_EnableEWSPartyFreeBusy = value; }
        }
        //ALLDEV-833 END
		 //ALLDEV-782 Starts
        public int EnablePexipIntegration
        {
            get { return m_EnablePexipIntegration; }
            set { m_EnablePexipIntegration = value; }
        }
        public int PexipMeetingType
        {
            get { return m_PexipMeetingType; }
            set { m_PexipMeetingType = value; }
        }
        public int PexipDisplay
        {
            get { return m_PexipDisplay; }
            set { m_PexipDisplay = value; }
        }
        public int PexipSelectOption
        {
            get { return m_PexipSelectOption; }
            set { m_PexipSelectOption = value; }
        }
        //ALLDEV-782 Ends
        //ALLDEV-862 START
        public int EnableS4BEWS
        {
            get { return m_EnableS4BEWS; }
            set { m_EnableS4BEWS = value; }
        }
        //ALLDEV-862 END
        #endregion
    }
    #endregion

           
    #region sysApprover

    public class sysApprover
    {
        private int m_uid, m_approverid;
        private int m_OrgId;

        public int UID
        {
            get { return m_uid; }
            set { m_uid = value; }
        }
        public int approverid
        {
            get { return m_approverid; }
            set { m_approverid = value; }
        }
        public int OrgId
        {
            get { return m_OrgId; }
            set { m_OrgId = value; }
        }
    }

    #endregion

    #region sysTechData
    public class sysTechData
    {
        #region Private Internal Members
        private int m_id;
        private string m_name, m_email, m_phone;
        private string m_info, m_feedback;
        private int m_OrgId;
        #endregion

        #region Public Properties
        public int id
        {
            get { return m_id; }
            set { m_id = value; }
        }
        public string name
        {
            get { return m_name; }
            set { m_name = value; }
        }
        public string email
        {
            get { return m_email; }
            set { m_email = value; }
        }
        public string phone
        {
            get { return m_phone; }
            set { m_phone = value; }
        }
        public string info
        {
            get { return m_info; }
            set { m_info = value; }
        }
        public string feedback
        {
            get { return m_feedback; }
            set { m_feedback = value; }
        }
        public int OrgId
        {
            get { return m_OrgId; }
            set { m_OrgId = value; }
        }
        #endregion
    }
    #endregion

    #region Class Diagnostics
    /// <summary>
    /// Summary description for system diagnostics.
    /// </summary>
    public class Diagnostics
    {
        #region Private Internal Members
        private int m_uid, m_OrgId;
        private int m_DayofWeek, m_purgeType, m_ServiceStatus, m_PurgeData;
        private DateTime m_UpdatedDate, m_TimeofDay;

        #endregion

        #region Public Properties
        public int UID
        {
            get { return m_uid; }
            set { m_uid = value; }
        }
        public int OrgId
        {
            get { return m_OrgId; }
            set { m_OrgId = value; }
        }
        public int PurgeType
        {
            get { return m_purgeType; }
            set { m_purgeType = value; }
        }
        public int DayofWeek
        {
            get { return m_DayofWeek; }
            set { m_DayofWeek = value; }
        }
        public DateTime UpdatedDate
        {
            get { return m_UpdatedDate; }
            set { m_UpdatedDate = value; }
        }
        public DateTime TimeofDay
        {
            get { return m_TimeofDay; }
            set { m_TimeofDay = value; }
        }
        public int ServiceStatus
        {
            get { return m_ServiceStatus; }
            set { m_ServiceStatus = value; }
        }
        public int PurgeData
        {
            get { return m_PurgeData; }
            set { m_PurgeData = value; }
        }
        #endregion
    }
    #endregion

    #region holidays 
    /// <summary>
    /// FB 1861
    /// </summary>
    public class holidays
    {
        private int m_uid;
        private DateTime m_Date;
        private int m_OrgId, m_EntityID, m_EnitityType;
        private int m_HolidayType;

        public int UID
        {
            get { return m_uid; }
            set { m_uid = value; }
        }
        public int OrgId
        {
            get { return m_OrgId; }
            set { m_OrgId = value; }
        }
        public int EntityID
        {
            get { return m_EntityID; }
            set { m_EntityID = value; }
        }
        public int EnitityType
        {
            get { return m_EnitityType; }
            set { m_EnitityType = value; }
        }
        public int HolidayType
        {
            get { return m_HolidayType; }
            set { m_HolidayType = value; }
        }
        public DateTime Date
        {
            get { return m_Date; }
            set { m_Date = value; }
        }

    }

    public class holidaysType
    {
        
        private int m_OrgId, m_Priority;
        private string m_HolidayDescription;
        private int m_HolidayType;
        private String m_Color;

        public int OrgId
        {
            get { return m_OrgId; }
            set { m_OrgId = value; }
        }
        public int Priority
        {
            get { return m_Priority; }
            set { m_Priority = value; }
        }
        public int HolidayType
        {
            get { return m_HolidayType; }
            set { m_HolidayType = value; }
        }
        public String HolidayDescription
        {
            get { return m_HolidayDescription; }
            set { m_HolidayDescription = value; }
        }
        public String Color
        {
            get { return m_Color; }
            set { m_Color = value; }
        }

    }

    #endregion

    //FB 2154
    #region EmailDoamin
    public class vrmEmailDomain
    {
        #region Private Internal Members

        private int m_DomainID, m_Active, m_orgId;
        private string m_Companyname, m_Emaildomain;
        #endregion

        #region Public Properties
        public int DomainID
        {
            get { return m_DomainID; }
            set { m_DomainID = value; }
        }

        public int Active
        {
            get { return m_Active; }
            set { m_Active = value; }
        }
        public int orgId
        {
            get { return m_orgId; }
            set { m_orgId = value; }
        }
        public string Companyname
        {
            get { return m_Companyname; }
            set { m_Companyname = value; }
        }
        public string Emaildomain
        {
            get { return m_Emaildomain; }
            set { m_Emaildomain = value; }
        }
        #endregion

    }
    #endregion


    //FB 2337
    #region VrmOrgLicAgreement
    /// <summary>
    /// Summary description for organization's EndUser License Agreement Text.
    /// </summary>
    public class VrmOrgLicAgreement
    {
        #region Private Internal Members

        private String m_EndUserLicAgrmnt;
        private int m_UID, m_OrgID;

        #endregion

        #region Public Properties
        public int UID
        {
            get { return m_UID; }
            set { m_UID = value; }
        }
        public int OrgID
        {
            get { return m_OrgID; }
            set { m_OrgID = value; }
        }
        public string EndUserLicAgrmnt
        {
            get { return m_EndUserLicAgrmnt; }
            set { m_EndUserLicAgrmnt = value; }
        }
        #endregion
    }
    #endregion


    //FB 2501 EM7 Starts
    #region vrmEM7OrgSilo
    /// <summary>
    /// Summary description for organization's EndUser License Agreement Text.
    /// </summary>
    public class vrmEM7OrgSilo
    {
        #region Private Internal Members

        private String m_EM7Orgname, m_EM7OrgEmail,m_UserName,m_Password;
        private int m_UID, m_OrgID;

        #endregion

        #region Public Properties
        public int UID
        {
            get { return m_UID; }
            set { m_UID = value; }
        }
        public int OrgID
        {
            get { return m_OrgID; }
            set { m_OrgID = value; }
        }
        public string EM7Orgname
        {
            get { return m_EM7Orgname; }
            set { m_EM7Orgname = value; }
        }
        public string EM7OrgEmail
        {
            get { return m_EM7OrgEmail; }
            set { m_EM7OrgEmail = value; }
        }
        public string EM7UserName
        {
            get { return m_UserName; }
            set { m_UserName = value; }
        }
        public string EM7Password
        {
            get { return m_Password; }
            set { m_Password = value; }
        }
        #endregion
    }
    #endregion
       
    //FB 2501 EM7 Ends

    //FB 2599 Start
    //FB 2262
    #region VrmVidyoSettings
    public class VrmVidyoSettings
    {
        #region Private Internal Members

        private int m_id, m_orgID,m_Port;
        private string m_VidyoURL;
        private string m_AdminLogin;
        private string m_Password;
        private string m_ProxyAddress;
        private DateTime m_LastModified = DateTime.Now;
        private DateTime m_PollTime = DateTime.Now;

        #endregion

        #region Public Properties
        public int Id
        {
            get { return m_id; }
            set { m_id = value; }
        }

        public int orgID
        {
            get { return m_orgID; }
            set { m_orgID = value; }
        }

        public string VidyoURL
        {
            get { return m_VidyoURL; }
            set { m_VidyoURL = value; }
        }

        public string AdminLogin
        {
            get { return m_AdminLogin; }
            set { m_AdminLogin = value; }
        }

        public string Password
        {
            get { return m_Password; }
            set { m_Password = value; }
        }

        public int Port
        {
            get { return m_Port; }
            set { m_Port = value; }
        }

        public string ProxyAddress
        {
            get { return m_ProxyAddress; }
            set { m_ProxyAddress = value; }
        }

        public DateTime PollTime
        {
            get { return m_PollTime; }
            set { m_PollTime = value; }
        }

        public DateTime Lastmodifieddatetime
        {
            get { return m_LastModified; }
            set { m_LastModified = value; }
        }
        #endregion
    }
    #endregion
    //FB 2599 End

    //ZD 101835
    #region vrmConfArchiveSettings
    public class vrmConfArchiveSettings
    {
        #region Private Internal Members

        private int m_ID, m_ArchiveConfOlderThan, m_ArchivePeriod, m_UserID, m_OrgID;
        private DateTime m_ArchiveTime, m_LastRunDate, m_ConfigurationDate;

        #endregion

        #region Public Properties

        public int ID
        {
            get { return m_ID; }
            set { m_ID = value; }
        }
        public int ArchiveConfOlderThan
        {
            get { return m_ArchiveConfOlderThan; }
            set { m_ArchiveConfOlderThan = value; }
        }
        public int ArchivePeriod
        {
            get { return m_ArchivePeriod; }
            set { m_ArchivePeriod = value; }
        }
        public DateTime ArchiveTime
        {
            get { return m_ArchiveTime; }
            set { m_ArchiveTime = value; }
        }
        public int UserID
        {
            get { return m_UserID; }
            set { m_UserID = value; }
        }
        public int OrgID
        {
            get { return m_OrgID; }
            set { m_OrgID = value; }
        }

        public DateTime LastRunDate
        {
            get { return m_LastRunDate; }
            set { m_LastRunDate = value; }
        }

        public DateTime ConfigurationDate
        {
            get { return m_ConfigurationDate; }
            set { m_ConfigurationDate = value; }
        }
        
        #endregion
    }
    #endregion

    #region vrmConfArchiveDetails
    public class vrmConfArchiveDetails
    {
        #region Private Internal Members

        private int m_ID, m_ArchiveID, m_ConfCount;
        private DateTime m_RunDate;

        #endregion

        #region Public Properties

        public int ID
        {
            get { return m_ID; }
            set { m_ID = value; }
        }

        public int ArchiveID
        {
            get { return m_ArchiveID; }
            set { m_ArchiveID = value; }
        }
        public int ConfCount
        {
            get { return m_ConfCount; }
            set { m_ConfCount = value; }
        }

        public DateTime RunDate
        {
            get { return m_RunDate; }
            set { m_RunDate = value; }
        }
        #endregion
    }
    #endregion

}
