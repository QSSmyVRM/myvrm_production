/* Copyright (C) 2015 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
//ZD 100147 ZD 100886
using System;
using System.Text;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Xml;
using System.Linq;
using NHibernate;
using NHibernate.Criterion;

using log4net;


namespace myVRM.DataLayer
{
    /// <summary>
    /// Summary description for user table General Static/Dynamic tables 
    /// </summary>
    public class vrmVideoSession
    {
        #region Private Internal Members
        private int m_Id;
        private string m_sessionType;
        #endregion

        #region Public Properties
        public int Id
        {
            get { return m_Id; }
            set { m_Id = value; }
        }
        public string VideoSessionType
        {
            get { return m_sessionType; }
            set { m_sessionType = value; }
        }

        #endregion


    }
    public class vrmAddressType
    {
        #region Private Internal Members
        private int m_Id;
        private string m_name;
        #endregion

        #region Public Properties
        public int Id
        {
            get { return m_Id; }
            set { m_Id = value; }
        }
        public string name
        {
            get { return m_name; }
            set { m_name = value; }
        }

        #endregion
    }
    public class vrmLineRate
    {
        #region Private Internal Members
        private int m_Id;
        private string m_lineRate;
        #endregion

        #region Public Properties
        public int Id
        {
            get { return m_Id; }
            set { m_Id = value; }
        }
        public string LineRateType
        {
            get { return m_lineRate; }
            set { m_lineRate = value; }
        }

        #endregion


    }
    public class vrmLanguage
    {
        #region Private Internal Members
        private int m_Id;
        private string m_Name, m_Country, m_LanguageFolder; //FB 1830
        #endregion

        #region Public Properties
        public int Id
        {
            get { return m_Id; }
            set { m_Id = value; }
        }
        public string Name
        {
            get { return m_Name; }
            set { m_Name = value; }
        }
        public string Country
        {
            get { return m_Country; }
            set { m_Country = value; }
        }
        public string LanguageFolder    //FB 1830
        {
            get { return m_LanguageFolder; }
            set { m_LanguageFolder = value; }
        }
        #endregion


    }
    public class vrmVideoEquipment
    {
        #region Private Internal Members
        private int m_Id;
        private string m_veName, m_DisplayName;//ZD 100736
        private int m_Familyid;//FB 2390
        #endregion

        #region Public Properties
        public int Id
        {
            get { return m_Id; }
            set { m_Id = value; }
        }
        public string VEName
        {
            get { return m_veName; }
            set { m_veName = value; }
        }
        //FB 2390
        public int Familyid
        {
            get { return m_Familyid; }
            set { m_Familyid = value; }
        }
        //ZD 100736 START
        public string DisplayName
        {
            get { return m_DisplayName; }
            set { m_DisplayName = value; }
        }
        //ZD 100736 END
        #endregion


    }
    //ZD 100736 - Start
    public class vrmManufacturer
    {
        #region Private Internal Members
        private string m_ManufacturerName;
        private int m_MID;
        #endregion

        #region Public Properties
        public string ManufacturerName
        {
            get { return m_ManufacturerName; }
            set { m_ManufacturerName = value; }
        }
        public int MID
        {
            get { return m_MID; }
            set { m_MID = value; }
        }
        #endregion
    }
    //ZD 100736 - End
    public class vrmAudioAlg
    {
        #region Private Internal Members
        private int m_Id;
        private string m_audioType;
        #endregion

        #region Public Properties
        public int Id
        {
            get { return m_Id; }
            set { m_Id = value; }
        }
        public string AudioType
        {
            get { return m_audioType; }
            set { m_audioType = value; }
        }

        #endregion
    }
    public class vrmInterfaceType
    {
        #region Private Internal Members
        private int m_Id;
        private string m_interfaceType;
        #endregion

        #region Public Properties
        public int Id
        {
            get { return m_Id; }
            set { m_Id = value; }
        }
        public string Interface
        {
            get { return m_interfaceType; }
            set { m_interfaceType = value; }
        }

        #endregion
    }
    public class vrmMediaType
    {
        #region Private Internal Members
        private int m_Id;
        private string m_mediaType;
        #endregion

        #region Public Properties
        public int Id
        {
            get { return m_Id; }
            set { m_Id = value; }
        }
        public string mediaType
        {
            get { return m_mediaType; }
            set { m_mediaType = value; }
        }

        #endregion
    }

    public class vrmVideoMode
    {
        #region Private Internal Members
        private int m_id;
        private string m_modeName;
        #endregion

        #region Public Properties
        public int Id
        {
            get { return m_id; }
            set { m_id = value; }
        }
        public string modeName
        {
            get { return m_modeName; }
            set { m_modeName = value; }
        }

        #endregion
    }
    public class vrmVideoProtocol
    {
        #region Private Internal Members
        private int m_VideoProtocolID;
        private string m_VideoProtocolType;
        #endregion

        #region Public Properties
        public int Id
        {
            get { return m_VideoProtocolID; }
            set { m_VideoProtocolID = value; }
        }
        public string VideoProtocolType
        {
            get { return m_VideoProtocolType; }
            set { m_VideoProtocolType = value; }
        }

        #endregion


    }
    public class vrmDialingOption
    {
        #region Private Internal Members
        private int m_ID;
        private string m_Option;
        #endregion

        #region Public Properties
        public int ID
        {
            get { return m_ID; }
            set { m_ID = value; }
        }
        public string DialingOption
        {
            get { return m_Option; }
            set { m_Option = value; }
        }

        #endregion


    }
    //NewLobby
    public class vrmIconsRef
    {
        #region Private Internal Members
        private int m_IconID;
        private string m_IconUri, m_Label, m_IconTarget;
        #endregion

        #region Public Properties
        public int IconID
        {
            get { return m_IconID; }
            set { m_IconID = value; }
        }
        public string IconUri
        {
            get { return m_IconUri; }
            set { m_IconUri = value; }
        }
        public string Label
        {
            get { return m_Label; }
            set { m_Label = value; }
        }
        public string IconTarget
        {
            get { return m_IconTarget; }
            set { m_IconTarget = value; }
        }
        #endregion

    }
    //FB 2027 - Start
    public class vrmLogPref
    {

        #region Private Internal Members

        private string m_modulename;
        private int m_loglevel;
        private int m_loglife;
        private string m_LogModule;
        private int m_UID;

        #endregion

        #region Public Properties

        public string modulename
        {
            get { return m_modulename; }
            set { m_modulename = value; }
        }

        public int loglevel
        {
            get { return m_loglevel; }
            set { m_loglevel = value; }
        }

        public int loglife
        {
            get { return m_loglife; }
            set { m_loglife = value; }
        }

        public string LogModule
        {
            get { return m_LogModule; }
            set { m_LogModule = value; }
        }
        public int UID
        {
            get { return m_UID; }
            set { m_UID = value; }
        }

        #endregion
    }
    //FB 2027 - End
    public class vrmServiceType//FB 2219
    {
        #region Private Internal Members
        private int m_Id;
        private string m_ServiceType;
        #endregion

        #region Public Properties
        public int Id
        {
            get { return m_Id; }
            set { m_Id = value; }
        }
        public string ServiceType
        {
            get { return m_ServiceType; }
            set { m_ServiceType = value; }
        }

        #endregion
    }

    //FB 2136 - Starts
    public class vrmSecurityBadgeType
    {
        #region Private Internal Members
        private int m_Id;
        private string m_SecBadgeType;
        #endregion

        #region Public Properties
        public int Id
        {
            get { return m_Id; }
            set { m_Id = value; }
        }

        public string SecBadgeType
        {
            get { return m_SecBadgeType; }
            set { m_SecBadgeType = value; }
        }
        #endregion
    }
    //FB 2136 - End

    //FB 2693 - Starts
    #region vrmPCDetails
    public class vrmPCDetails
    {
        #region Private Internal Members
        private int m_Id;
        private string m_PCType, m_Absolutepath;
        #endregion

        #region Public Properties
        public int Id
        {
            get { return m_Id; }
            set { m_Id = value; }
        }
        public string PCType
        {
            get { return m_PCType; }
            set { m_PCType = value; }
        }
        public string Absolutepath
        {
            get { return m_Absolutepath; }
            set { m_Absolutepath = value; }
        }
        #endregion
    }
    #endregion
    //FB 2693 - End

    //ZD 10040 start
    public class VrmEptResolution
    {
        #region Private Internal Members
        private int m_RID;
        private string m_Resolution;
        #endregion

        #region Public Properties
        public string Resolution
        {
            get { return m_Resolution; }
            set { m_Resolution = value; }
        }
        public int RID
        {
            get { return m_RID; }
            set { m_RID = value; }
        }
        #endregion
    }
    //ZD 10040 END

    /// <summary>
    /// Summary description for static General Information Tables.
    /// </summary>
    public class vrmGen
    {
        protected static List<vrmAddressType> m_AddressTypeList = null;
        protected static List<vrmVideoSession> m_VideoSessionList = null;
        protected static List<vrmLineRate> m_LineRateList = null;
        protected static List<vrmLanguage> m_LanguageList = null;
        protected static List<vrmVideoEquipment> m_VideoEquipmentList = null;
        protected static List<vrmManufacturer> m_ManufacturerList = null;//ZD 100736
        protected static List<vrmAudioAlg> m_AudioAlgList = null;
        protected static List<vrmInterfaceType> m_InterfaceTypeList = null;
        protected static List<vrmVideoMode> m_VideoModeList = null;
        protected static List<vrmMediaType> m_MediaTypeList = null;
        protected static List<vrmVideoProtocol> m_VideoProtocolList = null;
        protected static List<vrmDialingOption> m_DialingOptionList = null;
        protected static List<vrmMCUCardDetail> m_MCUCardDetail = null;
        protected static List<vrmMCUVendor> m_MCUVendor = null;
        protected static List<vrmMCUStatus> m_MCUStatus = null;
        protected static List<vrmLogPref> m_LogPrefList = null;//FB 2027
        protected static List<vrmServiceType> m_ServiceTypeList = null;//FB 2219
        protected static List<vrmSecurityBadgeType> m_SecurityBadgeTypeList = null; //FB 2136
        protected static List<vrmPCDetails> m_PCDetails = null; //FB 2693
        protected static List<VrmEptResolution> m_EptResolutionList = null;//ZD 100040
        protected static Hashtable m_EptResolutions = null; //ZD 100040

        //
        // gen_tables are static and loaded only once
        //
        public vrmGen(ref ISession session)
        {
            if (m_VideoSessionList == null)
                Init(ref session);
        }
        public vrmGen(string configPath)
        {
            try
            {
                if (m_VideoSessionList == null)
                {
                    SessionManagement.GetSession(configPath);
                    ISession session = SessionManagement.GetSession(configPath);
                    Init(ref session);
                }
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public vrmGen() { }

        public static bool Init(string configPath)
        {
            try
            {
                if (m_VideoSessionList == null)
                {
                    SessionManagement.GetSession(configPath);
                    ISession session = SessionManagement.GetSession(configPath);
                    Init(ref session);
                }
                return true;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        //
        // force time zone init
        public static bool Init(ref ISession session)
        {
            try
            {
                m_AddressTypeList = new List<vrmAddressType>();
                m_VideoSessionList = new List<vrmVideoSession>();
                m_LineRateList = new List<vrmLineRate>();
                m_VideoEquipmentList = new List<vrmVideoEquipment>();
                m_ManufacturerList = new List<vrmManufacturer>();
                m_AudioAlgList = new List<vrmAudioAlg>();
                m_LanguageList = new List<vrmLanguage>();
                m_InterfaceTypeList = new List<vrmInterfaceType>();
                m_VideoModeList = new List<vrmVideoMode>();
                m_MediaTypeList = new List<vrmMediaType>();
                m_VideoProtocolList = new List<vrmVideoProtocol>();
                m_DialingOptionList = new List<vrmDialingOption>();
                m_MCUCardDetail = new List<vrmMCUCardDetail>();
                m_MCUVendor = new List<vrmMCUVendor>();
                m_MCUStatus = new List<vrmMCUStatus>();
                m_LogPrefList = new List<vrmLogPref>();
                m_ServiceTypeList = new List<vrmServiceType>();
                m_SecurityBadgeTypeList = new List<vrmSecurityBadgeType>();
                m_PCDetails = new List<vrmPCDetails>();
                m_EptResolutionList = new List<VrmEptResolution>();//ZD 100040

                if (!session.IsConnected)
                {
                    session.Reconnect();
                }
                m_AddressTypeList = (session.CreateCriteria(typeof(vrmAddressType)).AddOrder(Order.Asc("Id")).List<vrmAddressType>()).ToList();
                if (m_AddressTypeList == null || m_AddressTypeList.Count == 0)
                    throw new Exception("Error in Address Type List initialization");

                m_MCUVendor = session.CreateCriteria(typeof(vrmMCUVendor)).AddOrder(Order.Asc("id")).List<vrmMCUVendor>().ToList();
                if (m_MCUVendor == null || m_MCUVendor.Count == 0)
                    throw new Exception("Error in MCU Vendor Detail initialization");

                m_VideoSessionList = session.CreateCriteria(typeof(vrmVideoSession)).AddOrder(Order.Asc("Id")).List<vrmVideoSession>().ToList();
                if (m_VideoSessionList == null || m_VideoSessionList.Count == 0)
                    throw new Exception("Error in Video Session List initialization");

                m_LineRateList = session.CreateCriteria(typeof(vrmLineRate)).AddOrder(Order.Asc("Id")).List<vrmLineRate>().ToList();
                if (m_LineRateList == null || m_LineRateList.Count == 0)
                    throw new Exception("Error in Line Rate List initialization");

                m_LanguageList = session.CreateCriteria(typeof(vrmLanguage)).AddOrder(Order.Asc("Id")).List<vrmLanguage>().ToList();
                if (m_LanguageList == null || m_LanguageList.Count == 0)
                    throw new Exception("Error in Language List initialization");

                m_VideoEquipmentList = session.CreateCriteria(typeof(vrmVideoEquipment)).AddOrder(Order.Asc("Familyid")).AddOrder(Order.Asc("VEName")).List<vrmVideoEquipment>().ToList();
                if (m_VideoEquipmentList == null || m_VideoEquipmentList.Count == 0)
                    throw new Exception("Error in Video Equipment List initialization");

                //ZD 100736
                m_ManufacturerList = session.CreateCriteria(typeof(vrmManufacturer)).AddOrder(Order.Asc("MID")).AddOrder(Order.Asc("ManufacturerName")).List<vrmManufacturer>().ToList();
                if (m_ManufacturerList == null || m_ManufacturerList.Count == 0)
                    throw new Exception("Error in Manufacturer List initialization");

                m_AudioAlgList = session.CreateCriteria(typeof(vrmAudioAlg)).AddOrder(Order.Asc("Id")).List<vrmAudioAlg>().ToList();
                if (m_AudioAlgList == null || m_AudioAlgList.Count == 0)
                    throw new Exception("Error in Audio Ald List initialization");

                m_InterfaceTypeList = session.CreateCriteria(typeof(vrmInterfaceType)).AddOrder(Order.Asc("Id")).List<vrmInterfaceType>().ToList();
                if (m_InterfaceTypeList == null || m_InterfaceTypeList.Count == 0)
                    throw new Exception("Error in Interface Type List initialization");

                m_VideoModeList = session.CreateCriteria(typeof(vrmVideoMode)).AddOrder(Order.Asc("Id")).List<vrmVideoMode>().ToList();
                if (m_VideoModeList == null || m_VideoModeList.Count == 0)
                    throw new Exception("Error in Video Mode List initialization");

                m_MediaTypeList = session.CreateCriteria(typeof(vrmMediaType)).AddOrder(Order.Asc("Id")).List<vrmMediaType>().ToList();
                if (m_MediaTypeList == null || m_MediaTypeList.Count == 0)
                    throw new Exception("Error in Media Type List initialization");

                m_VideoProtocolList = session.CreateCriteria(typeof(vrmVideoProtocol)).AddOrder(Order.Asc("Id")).List<vrmVideoProtocol>().ToList();
                if (m_VideoProtocolList == null || m_VideoProtocolList.Count == 0)
                    throw new Exception("Error in Video Protocol List initialization");

                m_DialingOptionList = session.CreateCriteria(typeof(vrmDialingOption)).AddOrder(Order.Asc("ID")).List<vrmDialingOption>().ToList();
                if (m_DialingOptionList == null || m_DialingOptionList.Count == 0)
                    throw new Exception("Error in Dialing Option List initialization");

                m_MCUCardDetail = session.CreateCriteria(typeof(vrmMCUCardDetail)).AddOrder(Order.Asc("id")).List<vrmMCUCardDetail>().ToList();
                if (m_MCUCardDetail == null || m_MCUCardDetail.Count == 0)
                    throw new Exception("Error in MCU Card Detail initialization");

                m_MCUStatus = session.CreateCriteria(typeof(vrmMCUStatus)).AddOrder(Order.Asc("id")).List<vrmMCUStatus>().ToList();
                if (m_MCUStatus == null || m_MCUStatus.Count == 0)
                    throw new Exception("Error in MCU Status Detail initialization");

                m_LogPrefList = session.CreateCriteria(typeof(vrmLogPref)).AddOrder(Order.Asc("UID")).List<vrmLogPref>().ToList();//FB 2027
                if (m_LogPrefList == null || m_LogPrefList.Count == 0)
                    throw new Exception("Error in Address Type List initialization");

                m_ServiceTypeList = session.CreateCriteria(typeof(vrmServiceType)).AddOrder(Order.Asc("Id")).List<vrmServiceType>().ToList();//FB 2219
                if (m_ServiceTypeList == null || m_ServiceTypeList.Count == 0)
                    throw new Exception("Error in Media Type List initialization");

                m_SecurityBadgeTypeList = session.CreateCriteria(typeof(vrmSecurityBadgeType)).AddOrder(Order.Asc("Id")).List<vrmSecurityBadgeType>().ToList();//FB 2136
                if (m_SecurityBadgeTypeList == null || m_SecurityBadgeTypeList.Count == 0)
                    throw new Exception("Error in Security Badge Type List initialization");

                //FB 2693 Starts
                m_PCDetails = session.CreateCriteria(typeof(vrmPCDetails)).AddOrder(Order.Asc("Id")).List<vrmPCDetails>().ToList();
                if (m_PCDetails == null || m_PCDetails.Count == 0)
                    throw new Exception("Error in PC Type List initialization");
                //FB 2693 Ends
                //ZD 100040 start
                m_EptResolutionList = session.CreateCriteria(typeof(VrmEptResolution)).AddOrder(Order.Asc("RID")).AddOrder(Order.Asc("Resolution")).List<VrmEptResolution>().ToList();
                if (m_EptResolutionList == null || m_EptResolutionList.Count == 0)
                    throw new Exception("Error in Endpoint  Resolution List initialization");
                //ZD 100040 End
                
                session.Disconnect();
            }
            catch (Exception e)
            {
                session.Disconnect();
                throw e;
            }
            return true;
        }
        public static List<vrmVideoSession> getVideoSession()
        {
            return m_VideoSessionList;
        }
        public static List<vrmLineRate> getLineRate()
        {
            return m_LineRateList;
        }
        public static List<vrmVideoEquipment> getVideoEquipment()
        {
            return m_VideoEquipmentList;
        }
        //ZD 100736
        public static List<vrmManufacturer> GetManufacturer()
        {
            return m_ManufacturerList;
        }
        public static List<vrmAudioAlg> getAudioAlg()
        {
            return m_AudioAlgList;
        }
        public static List<vrmInterfaceType> getInterfaceType()
        {
            return m_InterfaceTypeList;
        }
        public static List<vrmAddressType> getAddressType()
        {
            return m_AddressTypeList;
        }
        public static List<vrmLanguage> getLanguage()
        {
            return m_LanguageList;
        }
        public static List<vrmVideoMode> getVideoMode()
        {
            return m_VideoModeList;
        }
        public static List<vrmMediaType> getMediaTypes()
        {
            return m_MediaTypeList;
        }
        public static List<vrmVideoProtocol> getVideoProtocols()
        {
            return m_VideoProtocolList;
        }
        public static List<vrmDialingOption> getDialingOption()
        {
            return m_DialingOptionList;
        }
        public static List<vrmMCUCardDetail> getMCUCardDetail()
        {
            return m_MCUCardDetail;
        }
        public static List<vrmMCUVendor> getMCUVendor()
        {
            return m_MCUVendor;
        }
        public static List<vrmMCUStatus> getMCUStatus()
        {
            return m_MCUStatus;
        }
        public static List<vrmLogPref> getLogPref()//FB 2027
        {
            return m_LogPrefList;
        }
        public static List<vrmServiceType> getServiceTypes()//FB 2219
        {
            return m_ServiceTypeList;
        }
        public static List<vrmSecurityBadgeType> getSecurityBadgeTypes() //FB 2136
        {
            return m_SecurityBadgeTypeList;
        }
        //FB 2693 Starts
        public static List<vrmPCDetails> getPCTypes()
        {
            return m_PCDetails;
        }
        //FB 2693 Ends
        //ZD 100040 start
        public static List<VrmEptResolution> getEptResolution()
        {
            return m_EptResolutionList;
        }
        //ZD 100040 End
    }
}
