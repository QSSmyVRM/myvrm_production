/* Copyright (C) 2015 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
//ZD 100147 ZD 100886
using System;
using System.Text;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Xml;


namespace myVRM.DataLayer
{
    //ZD 100040 - Added this rights checking during Smart MCU codes
    #region Check User Hardware menu rights
    /// <summary>
    /// Summary ported from asp this will check the user rolse and return true/false
    /// if the user has the MCU authority
    /// Added user hardware menu rights checking while coding ZD 100040 
    /// </summary>
    public class checkHardwareAuthority
    {
        public bool hasEndpoints { get { return m_hasEndpoints; } }
        public bool hasDiagnostics { get { return m_hasDiagnostics; } }
        public bool hasMCUs { get { return m_hasMCUs; } }
        public bool hasMCUsLoadBalance { get { return m_hasMCUsLoadBalance; } }
        public bool hasAudioBridge { get { return m_hasAudioBridge; } }
        public bool hasEM7 { get { return m_hasEM7; } }

        private string m_role;
        private bool m_hasEndpoints = false;
        private bool m_hasDiagnostics = false;
        private bool m_hasMCUs = false;
        private bool m_hasMCUsLoadBalance = false;
        private bool m_hasAudioBridge = false;
        private bool m_hasEM7 = false;

        public checkHardwareAuthority(string role)
        {
            m_role = role;

            try
            {
                string[] mary = m_role.ToString().Split('-');
                string[] mmary = mary[1].Split('+');
                string[] ccary = mary[0].Split('*');

                int topMenu = Convert.ToInt32(ccary[1]);
                int adminMenu = Convert.ToInt32(mmary[10].Split('*')[1]);
                int orgMenu = Convert.ToInt32(mmary[11].Split('*')[1]);
                int subMenu = 0;

                if (Convert.ToBoolean(orgMenu & 128))
                {
                    subMenu = Convert.ToInt32(mmary[12].Split('*')[1]);
                    m_hasEndpoints = Convert.ToBoolean(subMenu & 32);
                    m_hasDiagnostics = Convert.ToBoolean(subMenu & 16);
                    m_hasMCUs = Convert.ToBoolean(subMenu & 8);
                    m_hasMCUsLoadBalance = Convert.ToBoolean(subMenu & 4);
                    m_hasAudioBridge = Convert.ToBoolean(subMenu & 2);
                    m_hasEM7 = Convert.ToBoolean(subMenu & 1);
                }
            }
            catch (Exception) { }
        }
    }
#endregion

	/// <summary>
	/// Summary description for MCU_List_D and related tables
	/// </summary>
    ///
    public class MCUType
    {
        public const int POLYCOM = 1;
        public const int CODIAN  = 3;
        public const int TANDBERG = 4;
        public const int RADVISION = 5;
        public const int MSE8000 = 6;//FB 2008
        public const int LIFESIZE = 7;//FB 2261
        public const int CISCO = 8;//FB 2501 Call Monitoring
        public const int iVIEW = 9; //FB 2556
        public const int PEXIP = 10; //ZD 101217 
        public const int BLUEJEANS = 11; //ZD 104021
        public const int RPRM = 13; //ZD 104430
    }

    public class eptDetails
    {
        public const int isAudio = 1;
        public const int isVideo = 2;
        public const int isBoth = 3;
        public const int isPrivate = 2;
        public const int isPublic = 1;
        public const int typeIP = 1;
        public const int typeISDN = 4;
        public const int typeMPI = 5;
        public const int typeSIP = 6;// 101147
    }
    public class dialingOption 
    {
        public static int dialIn = 1;
        public static int dialOut = 2;
        public static int direct = 3;
        public int getDialingOption(string option)
        {
            if (String.Compare(option, "in", false) > 0) return dialIn;
            else if (String.Compare(option, "out", false) > 0) 
                return dialOut; 
            else 
                return direct;
        }
    }
    public class vrmMCUApprover
    {
        int m_mcuid, m_approverid,m_id;
        public int id
        {
            get { return m_id; }
            set { m_id = value; }
        }
        public int mcuid
		{
			get {return m_mcuid;}
			set {m_mcuid = value;}
		}	public int approverid
		{
			get {return m_approverid;}
			set {m_approverid = value;}
		}	
	}
    public class vrmMCUResources
    {
        private int m_bridgeId, m_bridgeType, m_weight;
     //   private bool m_bAUDIO_PLUS, m_bVIDEO_PLUS, m_bMUX_PLUS;
        private bool m_bHDLC, m_bISDN, m_bATM, m_bIP, m_bMUX, m_bAUDIO, m_bVIDEO, m_bDATA, m_bISDN_8, m_bMPI;
        int m_adminId;
        private string m_bridgeName;
        public class cValues
        {
   
            private int m_calls, m_used;
            private string m_tag;
        
            public int available
            {
                get { return m_calls - m_used; }
            }

            public int calls
            {
                get { return m_calls; }
                set { m_calls = value; }
            }
            public int used
            {
                get { return m_used; }
                set { m_used = value; }
            }
            public string tag
            {
                get { return m_tag; }
                set { m_tag = value; }
            }
        }
        public string bridgeName
        {
            get { return m_bridgeName; }
            set { m_bridgeName = value; }
        }
        public int weight
        {
            get { return m_weight; }
            set { m_weight = value; }
        }
        public class m_HDLC : cValues { };
        public class m_ISDN : cValues { };
        public class m_ATM : cValues { };
        public class m_IP : cValues { };
        public class m_MUX : cValues { };
        public class m_AUDIO : cValues { };
        public class m_VIDEO : cValues { };
        public class m_DATA : cValues { };
        public class m_ISDN_8 : cValues { };
        public class m_MPI : cValues { };
     
        public m_HDLC HDLC;
        public m_ISDN ISDN;
        public m_ATM ATM;
        public m_IP IP;
        public m_MUX MUX;
        public m_AUDIO AUDIO;
        public m_VIDEO VIDEO;
        public m_DATA DATA;
        public m_ISDN_8 ISDN_8;
        public m_MPI MPI;

        public int bridgeType
        {
            get { return m_bridgeType; }
            set { m_bridgeType = value; }
        }
        public int bridgeId
        {
            get { return m_bridgeId; }
            set { m_bridgeId = value; }
        }
        public int adminId
        {
            get { return m_adminId; }
            set { m_adminId = value; }
        }

         public bool needHDLC
         {set {m_bHDLC = true;}}
         public bool needISDN
         { set { m_bISDN = true; } }
         public bool needATM   
         {set { m_bATM = true;}}
         public bool needIP   
         {set {m_bIP = true;}}
         public bool needMUX    
         {set {m_bMUX = true;}}
         public bool needAUDIO   
         {set {m_bAUDIO = true;}}
         public bool needVIDEO   
         {set {m_bVIDEO = true;}}
         public bool needDATA   
         {set {m_bDATA = true;}}
         public bool needISDN_8   
         {set {m_bISDN_8 = true;}}
         public bool needMPI
         { set { m_bMPI = true; } }


         public bool hasHDLC
         { get { return m_bHDLC; } }
         public bool hasISDN
         { get { return m_bISDN; } }
         public bool hasATM
         { get { return m_bATM; } }
         public bool hasIP
         { get { return m_bIP; } }
         public bool hasMUX
         { get { return m_bMUX; } }
         public bool hasAUDIO
         { get { return m_bAUDIO; } }
         public bool hasVIDEO
         { get { return m_bVIDEO; } }
         public bool hasDATA
         { get { return m_bDATA; } }
         public bool hasISDN_8
         { get { return m_bISDN_8; } }
         public bool hasMPI
         { get { return m_bMPI; } }
         
        public vrmMCUResources(int bridgeId)
        {
            m_bridgeId = bridgeId;
            //m_bAUDIO_PLUS = false;
            //m_bVIDEO_PLUS = false; 
            //m_bMUX_PLUS   = false;
            m_bHDLC = false;
            m_bISDN = false;
            m_bATM = false;
            m_bIP = false;
            m_bMUX = false;
            m_bAUDIO = false;
            m_bVIDEO = false;
            m_bDATA = false;
            m_bISDN_8 = false;
            m_bMPI = false;
            
            HDLC   = new m_HDLC();
            HDLC.tag = "HDLC";
            ISDN   = new m_ISDN();
            ISDN.tag = "ISDN";
            ATM    = new m_ATM();
            ATM.tag = "ATM";
            IP = new m_IP();
            IP.tag = "IP";
            MUX    = new m_MUX();
            MUX.tag = "MUX";
            AUDIO  = new m_AUDIO();
            AUDIO.tag = "AUDIO";
            VIDEO  = new m_VIDEO();
            VIDEO.tag = "VIDEO";
            DATA   = new m_DATA();
            DATA.tag = "DATA";
            ISDN_8 = new m_ISDN_8();
            ISDN_8.tag = "ISDN-8";
            MPI    = new m_MPI(); 
            MPI.tag = "MPI";            
        }
    }
	public class vrmMCU
	{
        #region Public Definitions
        
        public const int BRIDGEID_POLYCOM = 1;
        public const int BRIDGEID_CODIAN  = 3;
        public const int BRIDGEID_TANBERG = 4;
        #endregion

        #region Private Internal Members

        private int m_BridgeID, m_orgID, m_LastModifiedUser;//Code added for organization //ZD 100664
        private string m_BridgeLogin, m_AccessURL;//ZD 101078
        private string m_BridgePassword;
		private string m_BridgeAddress;
		private int m_Status;
		private int m_Admin;
		private DateTime m_LastModified;
		private string m_BridgeName;
		private int m_ChainPosition;
		private int m_updateInBridge;
		private int m_newBridge;
		private string m_ISDNPhonePrefix;
		private int m_BridgeTypeId;
		private string m_SoftwareVer;
		private int m_UpdateFromBridge;
		private int m_VirtualBridge;
		private int m_isdnlinerate;
		private int m_isdnmaxcost;
		private int m_isdnthreshold;
        private int m_isdnportrate;
		private int m_timezone;
		private int m_syncflag;
		private int m_isdnthresholdalert;
		private int m_malfunctionalert;
		private int m_reservedportperc;
		private int m_responsetime;
		private string m_responsemessage;
        private int m_MaxConcurrentAudioCalls;
        private int m_MaxConcurrentVideoCalls;
        private int m_deleted;
        private int m_ApiPortNo; //API Port...

        //FB 1642 - DTMF Commented for FB 2655
        //private string m_PreConfCode;
        //private string m_PreLeaderPin;
        //private string m_PostLeaderPin;
        private int m_EnableIVR;//FB 1766
        private string m_IVRServiceName;//FB 1766
        private int m_EnableRecording;//FB 1907
		private int m_isPublic;//FB 1920
		private string m_isdngateway;//FB 2008
        //FB 2003
        private string m_ISDNAudioPrefix;
        private string m_ISDNVideoPrefix;
        private int m_ConfServiceID;// FB 2016

        private int m_LPR;//FB 1907
        private DateTime m_CurrentDateTime;//FB 2448
        private int m_EnableMessage;//FB 2486
        private int m_setFavourite; // FB 2501 CallMonitor Favourite
        private int m_portsVideoTotal, m_portsVideoFree, m_portsAudioTotal, m_portsAudioFree;//FB 2501 Dec6
        private string m_BridgeExtNo, m_MultipleAddress, m_BridgeDomain; //FB 2610 ZD 100369_MCU //ZD 100522
		private int m_E164Dialing, m_EnhancedMCU, m_H323Dialing; //FB 2636
        //FB 2441 Starts
        private int m_MonitorMCU, m_SendMail, m_DMAPort, m_Synchronous, m_loginCount; //FB 2709 
        private string m_DMADomain,m_RPRMDomain, m_DMAdialinprefix, m_LoginName, m_RPRMEmailaddress; //FB 2689 //FB 2709 //FB 2441 II
        private string m_DMAName, m_DMALogin, m_DMAPassword, m_DMAURL;//FB 2441 RPRM
        
        //FB 2441 Ends
        private int m_EnableCDR, m_DeleteCDRDays; //FB 2660
        private int m_IsMultitenant, m_URLAccess, m_PollCount, m_EnablePollFailure; //FB 2556 //ZD 100113 //ZD 100369_MCU
        private int m_SysLocationId, m_PoolOrderID;//ZD 101522//ZD 104256
        private string m_BJNAppKey, m_BJNAppSecret; //ZD 104021
        private string m_Alias2, m_Alias3, m_ConferenceDialinName; //ALLDEV-854 //C20 S4B
        private int m_EnableAlias; //ALLDEV-854
		#endregion
		
		#region Public Properties

		public int BridgeID
		{
			get {return m_BridgeID;}
			set {m_BridgeID = value;}
		}	
		public string BridgeLogin 
		{
			get {return m_BridgeLogin;}
			set {m_BridgeLogin = value;}
		}	
		public string BridgePassword 
		{
			get {return m_BridgePassword;}
			set {m_BridgePassword = value;}
		}	
		public string BridgeAddress 
		{
			get {return m_BridgeAddress;}
			set {m_BridgeAddress = value;}
		}	
		public int Status 
		{
			get {return m_Status;}
			set {m_Status = value;}
		}	
		public int Admin 
		{
			get {return m_Admin;}
			set {m_Admin = value;}
		}	
		public DateTime LastModified 
		{
			get {return m_LastModified;}
			set {m_LastModified = value;}
		}	
		public string BridgeName 
		{
			get {return m_BridgeName;}
			set {m_BridgeName = value;}
		}	
		public string SoftwareVer
		{
			get {return m_SoftwareVer;}
			set {m_SoftwareVer = value;}
		}	
		public int ChainPosition 
		{
			get {return m_ChainPosition;}
			set {m_ChainPosition = value;}
		}	
		public int updateInBridge 
		{
			get {return m_updateInBridge;}
			set {m_updateInBridge = value;}
		}	
		public int newBridge 
		{
			get {return m_newBridge;}
			set {m_newBridge = value;}
		}	
		public string ISDNPhonePrefix 
		{
			get {return m_ISDNPhonePrefix;}
			set {m_ISDNPhonePrefix = value;}
		}	
		public int BridgeTypeId 
		{
			get {return m_BridgeTypeId;}
			set {m_BridgeTypeId = value;}
		}	
		public int UpdateFromBridge 
		{
			get {return m_UpdateFromBridge;}
			set {m_UpdateFromBridge = value;}
		}	
		public int VirtualBridge 
		{
			get {return m_VirtualBridge;}
			set {m_VirtualBridge = value;}
		}	
		public int isdnlinerate 
		{
			get {return m_isdnlinerate ;}
			set {m_isdnlinerate  = value;}
		}	
		public int isdnmaxcost 
		{
			get {return m_isdnmaxcost;}
			set {m_isdnmaxcost = value;}
		}	
		public int isdnthreshold 
		{
			get {return m_isdnthreshold;}
			set {m_isdnthreshold = value;}
		}	
		public int timezone 
		{
			get {return m_timezone;}
			set {m_timezone = value;}
		}	
		public int syncflag 
		{
			get {return m_syncflag;}
			set {m_syncflag = value;}
		}	
		public int isdnthresholdalert 
		{
			get {return m_isdnthresholdalert;}
			set {m_isdnthresholdalert = value;}
		}
        public int isdnportrate
        {
            get { return m_isdnportrate; }
            set { m_isdnportrate = value; }
        }
        public int malfunctionalert 
		{
			get {return m_malfunctionalert;}
			set {m_malfunctionalert = value;}
		}	
		public int reservedportperc
		{
			get {return m_reservedportperc;}
			set {m_reservedportperc = value;}
		}	
		public int responsetime
		{
			get {return m_responsetime;}
			set {m_responsetime = value;}
		}	
		public string responsemessage
		{
			get {return m_responsemessage;}
			set {m_responsemessage = value;}
		}
        public int MaxConcurrentAudioCalls
		{
            get { return m_MaxConcurrentAudioCalls; }
            set { m_MaxConcurrentAudioCalls = value; }
		}
        public int MaxConcurrentVideoCalls
        {
            get { return m_MaxConcurrentVideoCalls; }
            set { m_MaxConcurrentVideoCalls = value; }
        }
	
		public int deleted
		{
			get {return m_deleted;}
			set {m_deleted = value;}
        }

        public int orgId
        {
            get { return m_orgID; }
            set { m_orgID = value; }
        }
        public int ApiPortNo
        {
            get { return m_ApiPortNo; }
            set { m_ApiPortNo = value; }
        }

        //FB 1642 - DTMF - Start commented for FB 2655
        //public string PreConfCode
        //{
        //    get { return m_PreConfCode; }
        //    set { m_PreConfCode = value; }
        //}

        //public string PreLeaderPin
        //{
        //    get { return m_PreLeaderPin; }
        //    set { m_PreLeaderPin = value; }
        //}

        //public string PostLeaderPin
        //{
        //    get { return m_PostLeaderPin; }
        //    set { m_PostLeaderPin = value; }
        //}
        //FB 1642 - DTMF - End
        //FB 1766 - Start
        public int EnableIVR
        {
            get { return m_EnableIVR; }
            set { m_EnableIVR = value; }
        }
        public string IVRServiceName
        {
            get { return m_IVRServiceName; }
            set { m_IVRServiceName = value; }
        }
        //FB 1766 - End
        //FB 1907 
        public int EnableRecording
        {
            get { return m_EnableRecording; }
            set { m_EnableRecording = value; }
		}


        public int LPR
        {
            get { return m_LPR; }
            set { m_LPR = value; }
        }
        //FB 2486
        public int EnableMessage
        {
            get { return m_EnableMessage; }
            set { m_EnableMessage = value; }
        }
            
        //2448 Start
        public DateTime CurrentDateTime
        {
            get { return m_CurrentDateTime; }
            set { m_CurrentDateTime = value; }
        }
        //2448 End
		//FB 1920 Starts
        public int isPublic
        {
            get { return m_isPublic; }
            set { m_isPublic = value; }
        }
        //FB 1920 Ends

        public String ISDNGateway//FB 2008
        {
            get { return m_isdngateway; }
            set { m_isdngateway = value; }
        }
        //FB 2003 Start
        public string ISDNAudioPrefix
        {
            get { return m_ISDNAudioPrefix; }
            set { m_ISDNAudioPrefix = value; }
        }
        public string ISDNVideoPrefix
        {
            get { return m_ISDNVideoPrefix; }
            set { m_ISDNVideoPrefix = value; }
        }
        //FB 2003 End
        //FB 2016 Starts
        public int ConfServiceID
        {
            get { return m_ConfServiceID; }
            set { m_ConfServiceID = value; }
        }
        //FB 2016 Ends
        #endregion

        //FB 2501 CallMonitor Favourite Starts
        public int setFavourite
        {
            get { return m_setFavourite; }
            set { m_setFavourite = value; }
        }
        //FB 2501 Ends
        //FB 2501 CallMonitor Dec6
        public int portsVideoTotal
        {
            get { return m_portsVideoTotal; }
            set { m_portsVideoTotal = value; }
        }
        public int portsVideoFree
        {
            get { return m_portsVideoFree; }
            set { m_portsVideoFree = value; }
        }
        public int portsAudioTotal
        {
            get { return m_portsAudioTotal; }
            set { m_portsAudioTotal = value; }
        }
        public int portsAudioFree
        {
            get { return m_portsAudioFree; }
            set { m_portsAudioFree = value; }
        }
        //FB 2501 Ends

        //FB 2610 Start
        public string BridgeExtNo
        {
            get { return m_BridgeExtNo; }
            set { m_BridgeExtNo = value; }
        }
        //FB 2610 Ends

        //ZD 100522 Start
        public string BridgeDomain
        {
            get { return m_BridgeDomain; }
            set { m_BridgeDomain = value; }
        }
        //ZD 100522 Ends

        //c20 S4B Starts
        public string ConferenceDialinName
        {
            get { return m_ConferenceDialinName; }
            set { m_ConferenceDialinName = value; }
        }
        //c20 S4B Ends

        //FB 2636 Start
        public int E164Dialing
        {
            get { return m_E164Dialing; }
            set { m_E164Dialing = value; }
        }
        public int EnhancedMCU
        {
            get { return m_EnhancedMCU; }
            set { m_EnhancedMCU = value; }
        }
        public int H323Dialing
        {
            get { return m_H323Dialing; }
            set { m_H323Dialing = value; }
        }
        //FB 2636 End

        //FB 2441 Starts
        public int DMAMonitorMCU
        {
            get { return m_MonitorMCU; }
            set { m_MonitorMCU = value; }
        }
        public int DMASendMail
        {
            get { return m_SendMail; }
            set { m_SendMail = value; }
        }
        public string DMADomain
        {
            get { return m_DMADomain; }
            set { m_DMADomain = value; }
        }
        //FB 2441 RPRM Starts
        public string DMAName
        {
            get { return m_DMAName; }
            set { m_DMAName = value; }
        }
        public string DMALogin
        {
            get { return m_DMALogin; }
            set { m_DMALogin = value; }
        }
        public string DMAPassword
        {
            get { return m_DMAPassword; }
            set { m_DMAPassword = value; }
        }
        public string DMAURL
        {
            get { return m_DMAURL; }
            set { m_DMAURL = value; }
        }
        public int DMAPort
        {
            get { return m_DMAPort; }
            set { m_DMAPort = value; }
        }
        public int Synchronous
        {
            get { return m_Synchronous; }
            set { m_Synchronous = value; }
        }
		// FB 2441 II Starts
	    public string RPRMDomain
		{
            get { return m_RPRMDomain; }
            set { m_RPRMDomain = value; }
        }
		// FB 2441 II Starts
		//FB 2689 Starts
		 public string DialinPrefix
		{
            get { return m_DMAdialinprefix; }
            set { m_DMAdialinprefix = value; }            
        }
		//FB 2689 Ends
		 //FB 2709
        public string LoginName
        {
            get { return m_LoginName; }
            set { m_LoginName = value; }
        }
        public int loginCount
        {
            get { return m_loginCount; }
            set { m_loginCount = value; }
        }
        public string RPRMEmailaddress
        {
            get { return m_RPRMEmailaddress; }
            set { m_RPRMEmailaddress = value; }
        }
             
        
        //FB 2709
        //FB 2441 Ends
        //FB 2660 Starts
        public int EnableCDR
        {
            get { return m_EnableCDR; }
            set { m_EnableCDR = value; }
        }
        public int DeleteCDRDays
        {
            get { return m_DeleteCDRDays; }
            set { m_DeleteCDRDays = value; }
        }
        //FB 2660 Ends
        //ZD 100113 Start
        public int URLAccess
        {
            get { return m_URLAccess; }
            set { m_URLAccess = value; }
        }
        //ZD 100113 End
        //ZD 100369_MCU Start
        public int EnablePollFailure
        {
            get { return m_EnablePollFailure; }
            set { m_EnablePollFailure = value; }
        }
        public int PollCount
        {
            get { return m_PollCount; }
            set { m_PollCount = value; }
        }
        public string MultipleAddress
        {
            get { return m_MultipleAddress; }
            set { m_MultipleAddress = value; }
        }
        //ZD 100369_MCU End
		//FB 2556 - Starts
        public int IsMultitenant
        {
            get { return m_IsMultitenant; }
            set { m_IsMultitenant = value; }
        }
        //FB 2556 - Ends
        //ZD 100664
        public int LastModifiedUser
        {
            get { return m_LastModifiedUser; }
            set { m_LastModifiedUser = value; }
        }
        //ZD 101078 Start
        public string AccessURL
        {
            get { return m_AccessURL; }
            set { m_AccessURL = value; }
        }
        //ZD 101078 End
         //ZD 101522 Start
        public int SysLocationId
        {
            get { return m_SysLocationId; }
            set { m_SysLocationId = value; }
        }
        //ZD 101522 End
        //ZD 104021 Start
        public string BJNAppKey
        {
            get { return m_BJNAppKey; }
            set { m_BJNAppKey = value; }
        }
        public string BJNAppSecret
        {
            get { return m_BJNAppSecret; }
            set { m_BJNAppSecret = value; }
        }
        //ZD 104021 End
        public int PoolOrderID //ZD 104256
        {
            get { return m_PoolOrderID; }
            set { m_PoolOrderID = value; }
        }
        //ALLDEV-854 start
        public string Alias2
        {
            get { return m_Alias2; }
            set { m_Alias2 = value; }
        }
        public string Alias3
        {
            get { return m_Alias3; }
            set { m_Alias3 = value; }
        }
        public int EnableAlias 
        {
            get { return m_EnableAlias; }
            set { m_EnableAlias = value; }
        }
        //ALLDEV-854 End
    

        #region Private Relationships
        private vrmMCUType m_MCUType;
        private IList<vrmMCUCardList> m_MCUCardList;
        private IList<vrmMCUApprover> m_MCUApprover;
        private IList<vrmMCURPRMLoginList> m_MCURPRMLoginList;//FB 2709
     
        #endregion
        #region Public Relationships
        public vrmMCUType MCUType
		{
			get {return m_MCUType;}
			set {m_MCUType = value;}
		}
        public IList<vrmMCUCardList> MCUCardList
        {
            get { return m_MCUCardList; }
            set { m_MCUCardList = value; }
        }
        public IList<vrmMCUApprover> MCUApprover
        {
            get { return m_MCUApprover; }
            set { m_MCUApprover = value; }
        }
        //FB 2709
        public IList<vrmMCURPRMLoginList> MCURPRMLoginList
        {
            get { return m_MCURPRMLoginList; }
            set { m_MCURPRMLoginList = value; }
        }
        //FB 2709
	    #endregion
        public vrmMCU()
        {
            m_MCUCardList = new List<vrmMCUCardList>();
            m_MCUApprover = new List<vrmMCUApprover>();
            m_MCURPRMLoginList = new List<vrmMCURPRMLoginList>(); //FB 2709
        }
	}
    public class vrmMCUIPServices
    {
        #region Private Internal Members

        private int m_id, m_bridgeId, m_SortID;
        private string m_ServiceName;
        private int m_addressType;
        private string m_ipAddress;
        private int m_networkAccess;       
        private int m_usage;
      
        #endregion

        #region Public Properties

        public int uId
        {
            get { return m_id; }
            set { m_id = value; }
        }
        public int bridgeId
        {
            get { return m_bridgeId; }
            set { m_bridgeId = value; }
        }
        public int SortID
        {
            get { return m_SortID; }
            set { m_SortID = value; }
        }
        public string ServiceName
        {
            get { return m_ServiceName; }
            set { m_ServiceName = value; }
        }
        public int addressType
        {
            get { return m_addressType; }
            set { m_addressType = value; }
        }
        public string ipAddress
        {
            get { return m_ipAddress; }
            set { m_ipAddress = value; }
        }
        public int networkAccess
        {
            get { return m_networkAccess; }
            set { m_networkAccess = value; }
        }
        public int usage
        {
            get { return m_usage; }
            set { m_usage = value; }
        }
    

        #endregion
    }
    public class vrmMCUMPIServices
    {
        #region Private Internal Members

        private int m_id, m_bridgeId, m_SortID;
        private string m_ServiceName;
        private int m_addressType;
        private string m_ipAddress;
        private int m_networkAccess;
        private int m_usage;

        #endregion

        #region Public Properties

        public int ID
        {
            get { return m_id; }
            set { m_id = value; }
        }
        public int bridgeId
        {
            get { return m_bridgeId; }
            set { m_bridgeId = value; }
        }
        public int SortID
        {
            get { return m_SortID; }
            set { m_SortID = value; }
        }
        public string ServiceName
        {
            get { return m_ServiceName; }
            set { m_ServiceName = value; }
        }
        public int addressType
        {
            get { return m_addressType; }
            set { m_addressType = value; }
        }
        public string ipAddress
        {
            get { return m_ipAddress; }
            set { m_ipAddress = value; }
        }
        public int networkAccess
        {
            get { return m_networkAccess; }
            set { m_networkAccess = value; }
        }
        public int usage
        {
            get { return m_usage; }
            set { m_usage = value; }
        }


        #endregion
    }
    public class vrmMCUStatus
    {
        private int m_id;
        private string m_status;

        public int id
        {
            get { return m_id; }
            set { m_id = value; }
        }
        public string status
        {
            get { return m_status; }
            set { m_status = value; }
        }
    }
    public class vrmMCUVendor
    {
        private int m_id, m_BridgeInterfaceId, m_videoparticipants, m_audioparticipants;
        private string m_name;

        public int id
        {
            get { return m_id; }
            set { m_id = value; }
        }
        public string name
        {
            get { return m_name; }
            set { m_name = value; }
        }
        public int BridgeInterfaceId
        {
            get { return m_BridgeInterfaceId; }
            set { m_BridgeInterfaceId = value; }
        }
        public int videoparticipants
        {
            get { return m_videoparticipants; }
            set { m_videoparticipants = value; }
        }
        public int audioparticipants
        {
            get { return m_audioparticipants; }
            set { m_audioparticipants = value; }
        }
    }


    // FB 2636 Start

    public class vrmMCUE164Services
    {
        #region Private Internal Members

        private int m_uId, m_bridgeId, m_SortID;
        private string m_StartRange, m_EndRange;
        #endregion

        #region Public Properties

        public int uId
        {
            get { return m_uId; }
            set { m_uId = value; }
        }
        public int SortID
        {
            get { return m_SortID; }
            set { m_SortID = value; }
        }
        public int BridgeID
        {
            get { return m_bridgeId; }
            set { m_bridgeId = value; }
        }
        public string StartRange
        {
            get { return m_StartRange; }
            set { m_StartRange = value; }
        }
        public string EndRange
        {
            get { return m_EndRange; }
            set { m_EndRange = value; }
        }
        #endregion
    }

    // FB 2636 End

    public class vrmMCUISDNServices
    {
        #region Private Internal Members

        private int m_id, m_BridgeId, m_SortID;
        private string m_ServiceName;
        private string m_prefix;
        private long m_startNumber;
        private long m_endNumber;
        private int m_networkAccess;
        private int m_usage;
        private int m_RangeSortOrder;
        #endregion

        #region Public Properties

        public int uId
        {
            get { return m_id; }
            set { m_id = value; }
        }
        public int SortID
        {
            get { return m_SortID; }
            set { m_SortID = value; }
        }
        public int BridgeId
        {
            get { return m_BridgeId; }
            set { m_BridgeId = value; }
        }
        public string ServiceName
        {
            get { return m_ServiceName; }
            set { m_ServiceName = value; }
        }
        public string prefix
        {
            get { return m_prefix; }
            set { m_prefix = value; }
        }
        public long startNumber
        {
            get { return m_startNumber; }
            set { m_startNumber = value; }
        }
        public long endNumber
        {
            get { return m_endNumber; }
            set { m_endNumber = value; }
        }
        public int networkAccess
        {
            get { return m_networkAccess; }
            set { m_networkAccess = value; }
        }
        public int usage
        {
            get { return m_usage; }
            set { m_usage = value; }
        }
        public int RangeSortOrder
        {
            get { return m_RangeSortOrder; }
            set { m_RangeSortOrder = value; }
        }
        #endregion
  
    }
    public class vrmMCUType
    {
        #region Private Internal Members

        private int m_id;
        private string m_name;
        private int m_bridgeInterfaceId;
        private int m_videoParticipants;
        private int m_audioParticipants;

        #endregion

        #region Public Properties

        public int id
        {
            get { return m_id; }
            set { m_id = value; }
        }
        public int bridgeInterfaceId
        {
            get { return m_bridgeInterfaceId; }
            set { m_bridgeInterfaceId = value; }
        }
        public string name
        {
            get { return m_name; }
            set { m_name = value; }
        }
        public int videoParticipants
        {
            get { return m_videoParticipants; }
            set { m_videoParticipants = value; }
        }
        public int audioParticipants
        {
            get { return m_audioParticipants; }
            set { m_audioParticipants = value; }
        }

        #endregion

    }
    public class vrmMCUCardDetail
    {
       	private int m_id;
		private string m_CardName, m_Description;
        public int id
        {
            get{return m_id;}
            set{m_id = value;}
        }
        public string CardName
        {
            get {return m_CardName;}
            set {m_CardName = value;}
        }
        public string Description
        {
            get {return m_Description;}
            set {m_Description = value;}
        }
    }
    public class vrmMCUCardList
    {
        private int m_id, m_bridgeId, m_cardTypeId, m_maxCalls;
        public int id
        {
            get { return m_id; }
            set { m_id = value; }
        }
        public int bridgeId
        {
            get { return m_bridgeId; }
            set { m_bridgeId = value; }
        }
        public int cardTypeId
        {
            get { return m_cardTypeId; }
            set { m_cardTypeId = value; }
        }
        public int maxCalls
        {
            get { return m_maxCalls; }
            set { m_maxCalls = value; }
        }
        private vrmMCUCardDetail m_MCUCardDetail;
        public vrmMCUCardDetail MCUCardDetail
        {
            get { return m_MCUCardDetail; }
            set { m_MCUCardDetail = value; }
        }
    }

    //FB 2709
    public class vrmMCURPRMLoginList
    {
        private int m_id, m_bridgeId, m_BridgeInterfaceId, m_Deleted; //FB 2441 II
        private string m_Login, m_Domain, m_Password;
        public int id
        {
            get { return m_id; }
            set { m_id = value; }
        }
        public int bridgeId
        {
            get { return m_bridgeId; }
            set { m_bridgeId = value; }
        }
        public int BridgeInterfaceId
        {
            get { return m_BridgeInterfaceId; }
            set { m_BridgeInterfaceId = value; }
        }
        public string Login
        {
            get { return m_Login; }
            set { m_Login = value; }
        }
        public string Password
        {
            get { return m_Password; }
            set { m_Password = value; }
        }
        public string Domain
        {
            get { return m_Domain; }
            set { m_Domain = value; }
        }
        public int Deleted //FB 2441 II
        {
            get { return m_Deleted; }
            set { m_Deleted = value; }
        }
    }
    //FB 2709
    //FB 2486
    public class vrmMessage
    {
        private int m_ID, m_orgId, m_Type, m_Languageid, m_msgId;
        private string m_Message;
        public int ID
        {
            get { return m_ID; }
            set { m_ID = value; }
        }
        public string Message
        {
            get { return m_Message; }
            set { m_Message = value; }
        }
        public int orgId
        {
            get { return m_orgId; }
            set { m_orgId = value; }
        }
        public int Type
        {
            get { return m_Type; }
            set { m_Type = value; }
        }
        public int Languageid
        {
            get { return m_Languageid; }
            set { m_Languageid = value; }
        }
        public int msgId
        {
            get { return m_msgId; }
            set { m_msgId = value; }
        }
    }

    public class GetEndPointService : vrmConfBasicEntity
    {
        private int m_id, m_usage;
        private string m_sType;
        private int m_isDefault;
        private string m_address;
        private int m_singleDialin;
        private string m_MCUAddress;
        private int m_MCUAddressType;
        private int m_EPTAPIPortNo; //API Port No Fix
        private string m_ExchangeID; //Fixed during API Port No
        private string m_EPTURL, m_email; //Fixed during API Port No //ZD 102195
        private int m_Videoequipment; //ZD 102195

        private int m_endpointId, m_profileId, m_BridgeprofileId, m_Resolution, m_invitee, m_PoolOrderID,m_partyProfileID;//FB 2839 //ZD 100040 //ZD //ZD 104256 //ALLDEV-814
        /// <summary>
        /// did - Direct Inward Dial (ISDN number) = bridgeIPISDNAddress
        /// </summary>

        public int id
        {
            get { return m_id; }
            set { m_id = value; }
        }
        public int usage
        {
            get { return m_usage; }
            set { m_usage = value; }
        }
        public int isDefault
        {
            get { return m_isDefault; }
            set { m_isDefault = value; }
        }
       
        public string sType
        {
            get { return m_sType; }
            set { m_sType = value; }
        }
        public string address
        {
            get { return m_address; }
            set { m_address = value; }
        }
        public int endpointId
        {
            get { return m_endpointId; }
            set { m_endpointId = value; }
        }
        public int profileId
        {
            get { return m_profileId; }
            set { m_profileId = value; }
        }
        public int singleDialin
        {
            get { return m_singleDialin; }
            set { m_singleDialin = value; }
        }
        public string MCUAddress
        {
            get { return m_MCUAddress; }
            set { m_MCUAddress = value; }
        }
        public int MCUAddressType
        {
            get { return m_MCUAddressType; }
            set { m_MCUAddressType = value; }
        }
        // Code added for API Port NO start
        public int EPTAPIPortNo
        {
            get { return m_EPTAPIPortNo; }
            set { m_EPTAPIPortNo = value; }
        }
        public string ExchangeID
        {
            get { return m_ExchangeID; }
            set { m_ExchangeID = value; }
        }
        public string EPTURL
        {
            get { return m_EPTURL; }
            set { m_EPTURL = value; }
        }
        // Code added for API Port NO end
        public int BridgeprofileId //FB 2839
        {
            get { return m_BridgeprofileId; }
            set { m_BridgeprofileId = value; }
        }
        public int PoolOrderID //ZD 104256
        {
            get { return m_PoolOrderID; }
            set { m_PoolOrderID = value; }
        }
        public int Videoequipment //ZD 102195
        {
            get { return m_Videoequipment; }
            set { m_Videoequipment = value; }
        }
        public string email //ZD 102195
        {
            get { return m_email; }
            set { m_email = value; }
        }

        public int Resolution //ZD 100040
        {
            get { return m_Resolution; }
            set { m_Resolution = value; }
        }
        public int Invitee //ZD 100040
        {
            get { return m_invitee; }
            set { m_invitee = value; }
        }

        public int partyProfileID //ALLDEV-814
        {
            get { return m_partyProfileID; }
            set { m_partyProfileID = value; }
        }

        public GetEndPointService()
        {
        }
    }
    
    //FB 2501 - Call Monitoring
    public class vrmMCUParams
    {
        private int m_UID, m_BridgeTypeid, m_ConfMessage, m_PartyMessage, m_ConfLockUnLock;
        private string m_BridgeName, m_LoginURL, m_PassPhrase;//ZD 100113
        private int m_partyVideoTx,m_partyVideoRx,m_partyAudioTx,m_partyAudioRx,m_confAudioTx,m_confAudioRx,m_confVideoTx,m_confVideoRx;
        private int m_confLayout,m_confCamera,m_confPacketloss,m_confRecord,m_partyBandwidth,m_partySetfocus,m_partyLayout,m_partyCamera,m_partyPacketloss;
        private int m_partyImagestream, m_partyLockUnLock, m_partyRecord, m_partyLecturemode, m_confUnmuteAll, m_confMuteAll, m_partyLeader, m_activeSpeaker, m_snapshotSupport;//FB 2553-RMX //FB 2652//ZD 100174 //ZD 101042

        public int UID
        {
            get { return m_UID; }
            set { m_UID = value; }
        }
        public string BridgeName
        {
            get { return m_BridgeName; }
            set { m_BridgeName = value; }
        }
        public int BridgeTypeid
        {
            get { return m_BridgeTypeid; }
            set { m_BridgeTypeid = value; }
        }
        public int ConfMessage
        {
            get { return m_ConfMessage; }
            set { m_ConfMessage = value; }
        }
        public int PartyMessage
        {
            get { return m_PartyMessage; }
            set { m_PartyMessage = value; }
        }
        public int ConfLockUnLock
        {
            get { return m_ConfLockUnLock; }
            set { m_ConfLockUnLock = value; }
        }
        public int partyVideoTx
        {
            get { return m_partyVideoTx; }
            set { m_partyVideoTx = value; }
        }
        public int partyVideoRx
        {
            get { return m_partyVideoRx; }
            set { m_partyVideoRx = value; }
        }
        public int partyAudioTx
        {
            get { return m_partyAudioTx; }
            set { m_partyAudioTx = value; }
        }
        public int partyAudioRx
        {
            get { return m_partyAudioRx; }
            set { m_partyAudioRx = value; }
        }
        public int confAudioTx
        {
            get { return m_confAudioTx; }
            set { m_confAudioTx = value; }
        }
        public int confAudioRx
        {
            get { return m_confAudioRx; }
            set { m_confAudioRx = value; }
        }
        public int confVideoTx
        {
            get { return m_confVideoTx; }
            set { m_confVideoTx = value; }
        }
        public int confVideoRx
        {
            get { return m_confVideoRx; }
            set { m_confVideoRx = value; }
        }        
        public int confLayout
        {
            get { return m_confLayout; }
            set { m_confLayout = value; }
        }
        public int confCamera
        {
            get { return m_confCamera; }
            set { m_confCamera = value; }
        }
        public int confPacketloss
        {
            get { return m_confPacketloss; }
            set { m_confPacketloss = value; }
        }
        public int confRecord
        {
            get { return m_confRecord; }
            set { m_confRecord = value; }
        }
        public int partyLockUnLock
        {
            get { return m_partyLockUnLock; }
            set { m_partyLockUnLock = value; }
        }
        public int partyImagestream
        {
            get { return m_partyImagestream; }
            set { m_partyImagestream = value; }
        }
        public int partyPacketloss
        {
            get { return m_partyPacketloss; }
            set { m_partyPacketloss = value; }
        }
        public int partyCamera
        {
            get { return m_partyCamera; }
            set { m_partyCamera = value; }
        }
        public int partyLayout
        {
            get { return m_partyLayout; }
            set { m_partyLayout = value; }
        }
        public int partySetfocus
        {
            get { return m_partySetfocus; }
            set { m_partySetfocus = value; }
        }
        public int partyBandwidth
        {
            get { return m_partyBandwidth; }
            set { m_partyBandwidth = value; }
        }
        public int partyRecord
        {
            get { return m_partyRecord; }
            set { m_partyRecord = value; }
        }
        public int partyLecturemode
        {
            get { return m_partyLecturemode; }
            set { m_partyLecturemode = value; }
        }
      
        // FB 2652 Starts
        public int confMuteAllExcept
        {
            get { return m_confMuteAll; }
            set { m_confMuteAll = value; }
        }
        public int confUnmuteAll
        {
            get { return m_confUnmuteAll; }
            set { m_confUnmuteAll = value; }
        }
        //FB 2652 Ends
        public int partyLeader //FB 2553-RMX
        {
            get { return m_partyLeader; }
            set { m_partyLeader = value; }
        }
        //ZD 100113 Start
        public string LoginURL
        {
            get { return m_LoginURL; }
            set { m_LoginURL = value; }
        }
        public string PassPhrase 
        {
            get { return m_PassPhrase; }
            set { m_PassPhrase = value; }
        }
        //ZD 100113 End
        public int activeSpeaker //ZD 100174
        {
            get { return m_activeSpeaker; }
            set { m_activeSpeaker = value; }
        }
        public int snapshotSupport //ZD 101042
        {
            get { return m_snapshotSupport; }
            set { m_snapshotSupport = value; }
        }
    }

    //FB 2591
    public class vrmMCUProfiles
    {
        private int m_UID, m_MCUId, m_ProfileId;
        private  string m_ProfileName;

        public int UID
        {
            get { return m_UID; }
            set { m_UID = value; }
        }
        public int MCUId
        {
            get { return m_MCUId; }
            set { m_MCUId = value; }
        }
        public int ProfileId
        {
            get { return m_ProfileId; }
            set { m_ProfileId = value; }
        }
        public string ProfileName
        {
            get { return m_ProfileName; }
            set { m_ProfileName = value; }
        }
    }
	//FB 2556-TDB
    public class vrmMCUOrgSpecificDetails
    {
        int m_orgid, m_bridgeid, m_uid;
        string m_propertyvalue, m_propertyname;

        public int uID
        {
            get { return m_uid; }
            set { m_uid = value; }
        }
        public int bridgeID
        {
            get { return m_bridgeid; }
            set { m_bridgeid = value; }
        }
        public int orgID
        {
            get { return m_orgid; }
            set { m_orgid = value; }
        }
        
        public string propertyValue
        {
            get { return m_propertyvalue; }
            set { m_propertyvalue = value; }
        }
        public string propertyName
        {
            get { return m_propertyname; }
            set { m_propertyname = value; }
        }
    }


    public class vrmExtMCUSilo
    {
        int m_UID, m_BridgeId, m_MemberId, m_BridgeTypeId;
        string m_Name, m_Alias, m_BillingCode;

        public int UID
        {
            get { return m_UID; }
            set { m_UID = value; }
        }
        public int BridgeId
        {
            get { return m_BridgeId; }
            set { m_BridgeId = value; }
        }
        public int BridgeTypeId
        {
            get { return m_BridgeTypeId; }
            set { m_BridgeTypeId = value; }
        }
        public int MemberId
        {
            get { return m_MemberId; }
            set { m_MemberId = value; }
        }
        public string BillingCode
        {
            get { return m_BillingCode; }
            set { m_BillingCode = value; }
        }
        public string Name
        {
            get { return m_Name; }
            set { m_Name = value; }
        }
        public string Alias
        {
            get { return m_Alias; }
            set { m_Alias = value; }
        }
    }

    public class vrmExtMCUService
    {
        int m_UID, m_BridgeId, m_BridgeTypeId, m_MemberId, m_ServiceId;
        string m_ServiceName, m_Description, m_Prefix;

        public int UID
        {
            get { return m_UID; }
            set { m_UID = value; }
        }
        public int BridgeTypeId
        {
            get { return m_BridgeTypeId; }
            set { m_BridgeTypeId = value; }
        }
        public int BridgeId
        {
            get { return m_BridgeId; }
            set { m_BridgeId = value; }
        }
        public int MemberId
        {
            get { return m_MemberId; }
            set { m_MemberId = value; }
        }
        public int ServiceId
        {
            get { return m_ServiceId; }
            set { m_ServiceId = value; }
        }
        public string Prefix
        {
            get { return m_Prefix; }
            set { m_Prefix = value; }
        }
        public string ServiceName
        {
            get { return m_ServiceName; }
            set { m_ServiceName = value; }
        }
        public string Description
        {
            get { return m_Description; }
            set { m_Description = value; }
        }
    }

    //ZD 100040 starts
    public class vrmMCUGroupDetails
    {
        #region Private Internal Members

        private int m_MCUGroupID, m_LoadBalance, m_Pooled, m_LCR, m_OrgID, m_CreatedBy, m_ModifiedBy;
        private string m_GroupName, m_Description;
        private DateTime m_CreateTime, m_LastUpdateTime;

        #endregion

        #region Public Properties
        public int MCUGroupID
        {
            get { return m_MCUGroupID; }
            set { m_MCUGroupID = value; }
        }
        public string GroupName
        {
            get { return m_GroupName; }
            set { m_GroupName = value; }
        }
        public string Description
        {
            get { return m_Description; }
            set { m_Description = value; }
        }
        public int LoadBalance
        {
            get { return m_LoadBalance; }
            set { m_LoadBalance = value; }
        }
        public int Pooled
        {
            get { return m_Pooled; }
            set { m_Pooled = value; }
        }
        public int LCR
        {
            get { return m_LCR; }
            set { m_LCR = value; }
        }
        public int OrgID
        {
            get { return m_OrgID; }
            set { m_OrgID = value; }
        }
        public DateTime CreateTime
        {
            get { return m_CreateTime; }
            set { m_CreateTime = value; }
        }
        public DateTime LastUpdateTime
        {
            get { return m_LastUpdateTime; }
            set { m_LastUpdateTime = value; }
        }
        public int CreatedBy
        {
            get { return m_CreatedBy; }
            set { m_CreatedBy = value; }
        }
        public int ModifiedBy
        {
            get { return m_ModifiedBy; }
            set { m_ModifiedBy = value; }
        }

        #endregion
    }

    public class vrmMCUGrpAssignment
    {
        #region Private Internal Members
        private int m_ID, m_MCUGroupID, m_MCUID, m_LoadBalance, m_Overflow, m_LeadMCU;
        #endregion

        #region Public Properties
        public int ID
        {
            get { return m_ID; }
            set { m_ID = value; }
        }
        public int MCUGroupID
        {
            get { return m_MCUGroupID; }
            set { m_MCUGroupID = value; }
        }
        public int MCUID
        {
            get { return m_MCUID; }
            set { m_MCUID = value; }
        }
        public int LoadBalance
        {
            get { return m_LoadBalance; }
            set { m_LoadBalance = value; }
        }
        public int Overflow
        {
            get { return m_Overflow; }
            set { m_Overflow = value; }
        }
        public int LeadMCU
        {
            get { return m_LeadMCU; }
            set { m_LeadMCU = value; }
        }
        #endregion
    }

    public class vrmMCUPortResolution
    {
        #region Private Internal Members
        private int m_ID, m_bridgeId, m_resolutionId, m_port;
        #endregion

        #region Public Properties
        public int ID
        {
            get { return m_ID; }
            set { m_ID = value; }
        }
        public int BridgeId
        {
            get { return m_bridgeId; }
            set { m_bridgeId = value; }
        }
        public int ResolutionId
        {
            get { return m_resolutionId; }
            set { m_resolutionId = value; }
        }
        public int Port
        {
            get { return m_port; }
            set { m_port = value; }
        }
        #endregion
    }

    #region LoadBalanced MCU
    /// <summary>
    /// LoadBalanced MCU
    /// </summary>
    public class VRMLoadBalancedMCU
    {
        public vrmMCU BaseMCU { get; set; }
        public List<vrmMCUPortResolution> AvailablePortResolution { get; set; }
        public int LeadMCU { get; set; }
    }
    #endregion

    //ZD 100040 ends
	//ZD 104221 start
    public class vrmMCUPoolOrders
    {
        private int m_UID, m_MCUId, m_PoolOrderId;
        private string m_PoolOrderName;

        public int UID
        {
            get { return m_UID; }
            set { m_UID = value; }
        }
        public int MCUId
        {
            get { return m_MCUId; }
            set { m_MCUId = value; }
        }
        public int PoolOrderId
        {
            get { return m_PoolOrderId; }
            set { m_PoolOrderId = value; }
        }
        public string PoolOrderName
        {
            get { return m_PoolOrderName; }
            set { m_PoolOrderName = value; }
        }
    }
   //ZD 104221 End
}
