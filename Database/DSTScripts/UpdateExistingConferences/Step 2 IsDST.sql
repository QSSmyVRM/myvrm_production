
/****** Object:  UserDefinedFunction [dbo].[IsDST]    Script Date: 12/6/2017 5:23:03 AM ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
ALTER FUNCTION 
[dbo].[IsDST] 
(@tzid INTEGER, @time DATETIME)  
RETURNS INTEGER 
AS  
BEGIN 

	DECLARE @dstflag AS INTEGER
	DECLARE @return AS INTEGER
	DECLARE @startDST AS DATETIME
	DECLARE @endDST AS DATETIME
	SET @return = 0
	/* First check if the given timezone observes DST anytime of the year */
	SELECT @dstflag = dst FROM GEN_TIMEZONE_S WHERE timezoneid = @tzid
	IF @dstflag = 1
	BEGIN
		/* Get the start date for DST for this timezone; Second parameter = 1 */
		SET @startDST = dbo.GetDSTDate(@tzid, @time, 1)
		/* Get the end date for DST for this timezone: Second parameter = 2 */
		SET @endDST = dbo.GetDSTDate(@tzid, @time, 2)
		/* If the given time is in the DST window ... */
		IF @time BETWEEN @startDST AND @endDST
			SET @return = 1
		GOTO QUIT
	END
	ELSE
		GOTO QUIT	
QUIT:
	
	RETURN @return
END





