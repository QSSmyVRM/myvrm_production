/****** Object:  UserDefinedFunction [dbo].[changeToGMTTime]    Script Date: 12/6/2017 5:18:49 AM ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
ALTER FUNCTION [dbo].[changeToGMTTime]
   (@tzid integer, @time datetime )
RETURNS datetime 
AS
BEGIN
	/* The time we get in @time is a time in the timezone 
	 * specified by @tzid. We should first apply the DST
	 * correction to this time and next convert it to GMT.
	 */
	DECLARE @offset INTEGER
	DECLARE @return DATETIME
	IF dbo.IsDST(@tzid, @time) = 1
	BEGIN
		/* The time is in DST and a DST correction
		 * needs to be applied
		 */
		SELECT @offset = daylightbias FROM GEN_TIMEZONE_S
			WHERE timezoneid = @tzid
		SET @time = dateadd(minute, @offset, @time)
	END
	SELECT @offset = bias FROM GEN_TIMEZONE_S 
		WHERE timezoneid = @tzid
	SET @return = dateadd(minute, @offset, @time)
	RETURN @return
END
/* 
 * Old code for userpreferedtime is commented because it does
 * not account for Daylight Saving Time (DST). New code above
 * does just that
 */
/*
CREATE FUNCTION dbo.changeToGMTTime
   (@timezoneid integer, @time datetime )
RETURNS datetime 
AS
BEGIN
	declare @offset integer
	set @offset=0
	select @offset=offset  from timezone where timezoneid=@timezoneid 
	RETURN dateadd(hour,@offset,@time)
END
*/



