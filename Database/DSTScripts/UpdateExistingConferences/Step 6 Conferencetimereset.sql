
Declare @tzone as int, @confid as int, @instanceid as int, @chDate as datetime
Declare @chTime as datetime, @setupTime as datetime, @tearTime as datetime, @confDate as datetime

DECLARE getDSTCursor CURSOR for

select a.timezone, a.confid, a.instanceid
from Conf_Conference_D a , gen_timezone_s t where a.timezone = t.timezoneid and t.DST = 1
and confnumname in (3532, 3724, 3706,3548, 3668,3587,3722,3721,3559,3723,3691,3637,3657,3610,3669)
OPEN getDSTCursor

FETCH NEXT FROM getDSTCursor INTO  @tzone, @confid, @instanceid
WHILE @@FETCH_STATUS = 0
BEGIN

Select @confDate = confdate, @tzone = timezone from Conf_Conference_D where confid = @confid  and instanceid = @instanceid

IF dbo.isDST(@tzone,@confDate) = 1

	select 
	@chDate =  dateadd(minute, (-60), a.confdate)
	, @chTime =  dateadd(minute, (-60), a.conftime)
	, @setupTime =  dateadd(minute, (-60), a.SetupTime) 
	, @tearTime =  dateadd(minute, (-60), a.TearDownTime) 
	from Conf_Conference_D a , gen_timezone_s t where a.timezone = t.timezoneid and t.DST = 1
	and status in (0,1,9)  and confid = @confid  and instanceid = @instanceid





update Conf_Conference_D set confdate = @chDate, conftime = @chTime, SetupTime = @setupTime, TearDownTime = @tearTime
where confid = @confid  and instanceid = @instanceid

update Conf_Room_D set StartDate = @chDate where ConfID = @confid and instanceID = @instanceid

if(@instanceid = 1)
Begin
	update Conf_RecurInfo_D set startTime = @chDate where confid = @confid 
End

FETCH NEXT FROM getDSTCursor INTO @tzone, @confid, @instanceid
END
CLOSE getDSTCursor
DEALLOCATE getDSTCursor
