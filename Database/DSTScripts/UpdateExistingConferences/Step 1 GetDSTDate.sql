USE [RMT]
GO
/****** Object:  UserDefinedFunction [dbo].[GetDSTDate]    Script Date: 12/12/2017 3:59:07 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER FUNCTION 
[dbo].[GetDSTDate]
(@tzid INTEGER, @time DATETIME, @flag INTEGER)  
RETURNS DATETIME 
AS  
BEGIN 

	DECLARE @year AS INTEGER, @startYear as INTEGER
	DECLARE @return AS DATETIME
	select @startYear = year(CurDSTstart) from Gen_TimeZone_S where TimeZoneID = 8
	SELECT TOP 1 @year = year(@time) - @startYear  FROM SYS_SETTINGS_D
	IF @year = 0
	BEGIN
		IF @flag = 1
		BEGIN
			/* Return the start date of DST */
			SELECT @return = curdststart FROM GEN_TIMEZONE_S  WHERE timezoneid = @tzid
			RETURN @return
		END
		ELSE IF @flag = 2
		BEGIN
			/* Return the end date of DST */
			SELECT @return = curdstend FROM GEN_TIMEZONE_S WHERE timezoneid = @tzid
			RETURN @return
		END
	END
	ELSE IF @year = 1
	BEGIN
		IF @flag = 1
		BEGIN
			/* Return the start date of DST */
			SELECT @return = curdststart1 FROM GEN_TIMEZONE_S WHERE timezoneid = @tzid
			RETURN @return
		END
		ELSE IF @flag = 2
		BEGIN
			/* Return the end date of DST */
			SELECT @return = curdstend1 FROM GEN_TIMEZONE_S WHERE timezoneid = @tzid
			RETURN @return
		END
	END
	ELSE IF @year = 2
	BEGIN
		IF @flag = 1
		BEGIN
			/* Return the start date of DST */
			SELECT @return = curdststart2 FROM GEN_TIMEZONE_S WHERE timezoneid = @tzid
			RETURN @return
		END
		ELSE IF @flag = 2
		BEGIN
			/* Return the end date of DST */
			SELECT @return = curdstend2 FROM GEN_TIMEZONE_S WHERE timezoneid = @tzid
			RETURN @return
		END
	END
	ELSE IF @year = 3
	BEGIN
		IF @flag = 1
		BEGIN
			/* Return the start date of DST */
			SELECT @return = curdststart3 FROM GEN_TIMEZONE_S WHERE timezoneid = @tzid
			RETURN @return
		END
		ELSE IF @flag = 2
		BEGIN
			/* Return the end date of DST */
			SELECT @return = curdstend3 FROM GEN_TIMEZONE_S WHERE timezoneid = @tzid
			RETURN @return
		END
	END
	ELSE IF @year = 4
	BEGIN
		IF @flag = 1
		BEGIN
			/* Return the start date of DST */
			SELECT @return = curdststart4 FROM GEN_TIMEZONE_S WHERE timezoneid = @tzid
			RETURN @return
		END
		ELSE IF @flag = 2
		BEGIN
			/* Return the end date of DST */
			SELECT @return = curdstend4 FROM GEN_TIMEZONE_S WHERE timezoneid = @tzid
			RETURN @return
		END
	END
	ELSE IF @year = 5
	BEGIN
		IF @flag = 1
		BEGIN
			/* Return the start date of DST */
			SELECT @return = curdststart5 FROM GEN_TIMEZONE_S WHERE timezoneid = @tzid
			RETURN @return
		END
		ELSE IF @flag = 2
		BEGIN
			/* Return the end date of DST */
			SELECT @return = curdstend5 FROM GEN_TIMEZONE_S WHERE timezoneid = @tzid
			RETURN @return
		END
	END
	ELSE IF @year = 6
	BEGIN
		IF @flag = 1
		BEGIN
			/* Return the start date of DST */
			SELECT @return = curdststart6 FROM GEN_TIMEZONE_S WHERE timezoneid = @tzid
			RETURN @return
		END
		ELSE IF @flag = 2
		BEGIN
			/* Return the end date of DST */
			SELECT @return = curdstend6 FROM GEN_TIMEZONE_S WHERE timezoneid = @tzid
			RETURN @return
		END
	END
	ELSE IF @year = 7
	BEGIN
		IF @flag = 1
		BEGIN
			/* Return the start date of DST */
			SELECT @return = curdststart7 FROM GEN_TIMEZONE_S WHERE timezoneid = @tzid
			RETURN @return
		END
		ELSE IF @flag = 2
		BEGIN
			/* Return the end date of DST */
			SELECT @return = curdstend7 FROM GEN_TIMEZONE_S WHERE timezoneid = @tzid
			RETURN @return
		END
	END
	ELSE IF @year = 8
	BEGIN
		IF @flag = 1
		BEGIN
			/* Return the start date of DST */
			SELECT @return = curdststart8 FROM GEN_TIMEZONE_S WHERE timezoneid = @tzid
			RETURN @return
		END
		ELSE IF @flag = 2
		BEGIN
			/* Return the end date of DST */
			SELECT @return = curdstend8 FROM GEN_TIMEZONE_S WHERE timezoneid = @tzid
			RETURN @return
		END
	END
	ELSE IF @year = 9
	BEGIN
		IF @flag = 1
		BEGIN
			/* Return the start date of DST */
			SELECT @return = curdststart9 FROM GEN_TIMEZONE_S WHERE timezoneid = @tzid
			RETURN @return
		END
		ELSE IF @flag = 2
		BEGIN
			/* Return the end date of DST */
			SELECT @return = curdstend9 FROM GEN_TIMEZONE_S WHERE timezoneid = @tzid
			RETURN @return
		END
	END
	RETURN @return
END
