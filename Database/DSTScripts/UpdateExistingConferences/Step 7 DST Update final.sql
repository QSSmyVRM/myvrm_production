
Declare @tzone as int, @confid as int, @instanceid as int, @chDate as datetime
Declare @chTime as datetime, @setupTime as datetime, @tearTime as datetime, @confDate as datetime

DECLARE getDSTCursor CURSOR for

select a.timezone, a.confid, a.instanceid
from Conf_Conference_D a , gen_timezone_s t where a.timezone = t.timezoneid and t.DST = 1
and status in (0,1,9)and confdate >= getdate() and a.timezone in (7,8,13,23,69) and a.settingtime < '2017-12-09 00:00:00.000'
order by a.confid , a.instanceid
OPEN getDSTCursor

FETCH NEXT FROM getDSTCursor INTO  @tzone, @confid, @instanceid
WHILE @@FETCH_STATUS = 0
BEGIN

Select @confDate = confdate, @tzone = timezone from Conf_Conference_D where confid = @confid  and instanceid = @instanceid


	select 
	@chDate = dbo.changeToGMTTime(a.timezone,dateadd(minute, (-t.Bias), a.confdate)) 
	, @chTime = dbo.changeToGMTTime(a.timezone,dateadd(minute, (-t.Bias), a.conftime)) 
	, @setupTime = dbo.changeToGMTTime(a.timezone,dateadd(minute, (-t.Bias), a.SetupTime)) 
	, @tearTime = dbo.changeToGMTTime(a.timezone,dateadd(minute, (-t.Bias), a.TearDownTime)) 
	from Conf_Conference_D a , gen_timezone_s t where a.timezone = t.timezoneid and t.DST = 1
	and status in (0,1,9)and confdate >= getdate() and confid = @confid  and instanceid = @instanceid and a.settingtime < '2017-12-09 00:00:00.000'


update Conf_Conference_D set confdate = @chDate, conftime = @chTime, SetupTime = @setupTime, TearDownTime = @tearTime
where confid = @confid  and instanceid = @instanceid

update Conf_Room_D set StartDate = @chDate where ConfID = @confid and instanceID = @instanceid

if(@instanceid = 1)
Begin
	update Conf_RecurInfo_D set startTime = @chDate where confid = @confid 
End

FETCH NEXT FROM getDSTCursor INTO @tzone, @confid, @instanceid
END
CLOSE getDSTCursor
DEALLOCATE getDSTCursor
