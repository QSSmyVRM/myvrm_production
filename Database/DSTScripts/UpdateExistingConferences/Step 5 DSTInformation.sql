update gen_timezone_s set CurDSTstart = CurDSTstart4, CurDSTend = CurDSTend4,
CurDSTstart1 = CurDSTstart5, CurDSTend1 =CurDSTend5, CurDSTstart2 = CurDSTstart6, CurDSTend2 =CurDSTend6, 
CurDSTstart3 = CurDSTstart7, CurDSTend3 =CurDSTend7, CurDSTstart4 = CurDSTstart8, CurDSTend4 =CurDSTend8, 
CurDSTstart5 = CurDSTstart9, CurDSTend5 =CurDSTend9



--Id: 76 -> Monrovia, Reykjavik -> Greenwich Standard Time
--Id: 102 -> Casablanca -> Morocco Standard Time
update gen_timezone_s set 
CurDSTstart6 = '2018-04-29 02:00:00.000', CurDSTend6 = '2018-09-30 03:00:00.000',
CurDSTstart7 = '2019-04-28 02:00:00.000', CurDSTend7 = '2019-09-29 03:00:00.000',
CurDSTstart8 = '2020-04-26 02:00:00.000', CurDSTend8 = '2020-09-27 03:00:00.000',
CurDSTstart9 = '2021-04-25 02:00:00.000', CurDSTend9 = '2021-09-26 03:00:00.000'
where TimeZoneID in (76,102)

--Id: 5 -> Baghdad -> Arabic Standard Time
--Id: 27 -> Cairo -> Egypt Standard Time
--Id: 28 -> Ekaterinburg -> Ekaterinburg Standard Time
--Id: 41 -> Mid-Atlantic -> Mid-Atlantic Standard Time
--Id: 44 -> Almaty, Novosibirsk -> N. Central Asia Standard Time
--Id: 48 -> Irkutsk -> North Asia East Standard Time
--Id: 49 -> Krasnoyarsk -> North Asia Standard Time
--Id: 53 -> Moscow, St. Petersburg, Volgograd -> Russian Standard Time
--Id: 68 -> Vladivostok -> Vladivostok Standard Time
--Id: 74 -> Yakutsk -> Yakutsk Standard Time
update gen_timezone_s set 
CurDSTstart6 = '1980-01-01 00:00:00.000', CurDSTend6 = '1980-01-01 00:00:00.000',
CurDSTstart7 = '1980-01-01 00:00:00.000', CurDSTend7 = '1980-01-01 00:00:00.000',
CurDSTstart8 = '1980-01-01 00:00:00.000', CurDSTend8 = '1980-01-01 00:00:00.000',
CurDSTstart9 = '1980-01-01 00:00:00.000', CurDSTend9 = '1980-01-01 00:00:00.000'
where TimeZoneID in (28,44,48,49,53,68,74,5,27,41)

--Id: 37 -> Tehran -> Iran Standard Time
update gen_timezone_s set 
CurDSTstart6 = '2018-03-22 00:00:00.000', CurDSTend6 = '2018-09-22 00:00:00.000',
CurDSTstart7 = '2019-03-22 00:00:00.000', CurDSTend7 = '2019-09-22 00:00:00.000',
CurDSTstart8 = '2020-03-21 00:00:00.000', CurDSTend8 = '2020-09-21 00:00:00.000',
CurDSTstart9 = '2021-03-22 00:00:00.000', CurDSTend9 = '2021-09-22 00:00:00.000'
where TimeZoneID in (37)

--SECOND SUNDAY OF MARCH, FIRST SUNDAY OF NOVEMBER 
--Id: 2 -> Alaska -> Alaskan Standard Time
--Id: 6 -> Atlantic Time (Canada) -> Atlantic Standard Time
--Id: 19 -> Central Time (US and Canada) -> Central Standard Time
--Id: 26 -> Eastern Time (US and Canada) -> Eastern Standard Time
--Id: 42 -> Mountain Time (US and Canada) -> Mountain Standard Time
--Id: 47 -> Newfoundland -> Newfoundland Standard Time
--Id: 51 -> Pacific Time (US and Canada) -> Pacific Standard Time
update gen_timezone_s set 
CurDSTstart6 = '2018-03-11 02:00:00.000', CurDSTend6 = '2018-11-04 02:00:00.000',
CurDSTstart7 = '2019-03-10 02:00:00.000', CurDSTend7 = '2019-11-03 02:00:00.000',
CurDSTstart8 = '2020-03-08 02:00:00.000', CurDSTend8 = '2020-11-01 02:00:00.000',
CurDSTstart9 = '2021-03-14 02:00:00.000', CurDSTend9 = '2021-11-07 02:00:00.000'
where TimeZoneID in (19,26,42,51,47,2,6)

--LAST SUNDAY OF MARCH, LAST SUNDAY OF OCTOBER
--Id: 24 -> Minsk -> E. Europe Standard Time
--Id: 30 -> Helsinki, Kiev, Riga, Sofia, Tallinn, Vilnius -> FLE Standard Time
--Id: 34 -> Athens, Bucharest -> GTB Standard Time
update gen_timezone_s set 
CurDSTstart6 = '2018-03-25 03:00:00.000', CurDSTend6 = '2018-10-28 04:00:00.000',
CurDSTstart7 = '2019-03-31 03:00:00.000', CurDSTend7 = '2019-10-27 04:00:00.000',
CurDSTstart8 = '2020-03-29 03:00:00.000', CurDSTend8 = '2020-10-25 04:00:00.000',
CurDSTstart9 = '2021-03-28 03:00:00.000', CurDSTend9 = '2021-10-31 04:00:00.000'
where TimeZoneID in (24,34,30)

--LAST SUNDAY OF MARCH, LAST SUNDAY OF OCTOBER
--Id: 31 -> Dublin, Edinburgh, Lisbon, London -> GMT Standard Time
update gen_timezone_s set 
CurDSTstart6 = '2018-03-25 01:00:00.000', CurDSTend6 = '2018-10-28 02:00:00.000',
CurDSTstart7 = '2019-03-31 01:00:00.000', CurDSTend7 = '2019-10-27 02:00:00.000',
CurDSTstart8 = '2020-03-29 01:00:00.000', CurDSTend8 = '2020-10-25 02:00:00.000',
CurDSTstart9 = '2021-03-28 01:00:00.000', CurDSTend9 = '2021-10-31 02:00:00.000'
where TimeZoneID in (31)

--LAST SUNDAY OF MARCH, LAST SUNDAY OF OCTOBER
--Id: 9 -> Azores -> Azores Standard Time
update gen_timezone_s set 
CurDSTstart6 = '2018-03-25 00:00:00.000', CurDSTend6 = '2018-10-28 01:00:00.000',
CurDSTstart7 = '2019-03-31 00:00:00.000', CurDSTend7 = '2019-10-27 01:00:00.000',
CurDSTstart8 = '2020-03-29 00:00:00.000', CurDSTend8 = '2020-10-25 01:00:00.000',
CurDSTstart9 = '2021-03-28 00:00:00.000', CurDSTend9 = '2021-10-31 01:00:00.000'
where TimeZoneID in (9)

--LAST SUNDAY OF MARCH, LAST SUNDAY OF OCTOBER
--Id: 12 -> Yerevan -> Caucasus Standard Time
update gen_timezone_s set 
CurDSTstart6 = '2018-03-25 04:00:00.000', CurDSTend6 = '2018-10-28 05:00:00.000',
CurDSTstart7 = '2019-03-31 04:00:00.000', CurDSTend7 = '2019-10-27 05:00:00.000',
CurDSTstart8 = '2020-03-29 04:00:00.000', CurDSTend8 = '2020-10-25 05:00:00.000',
CurDSTstart9 = '2021-03-28 04:00:00.000', CurDSTend9 = '2021-10-31 05:00:00.000'
where TimeZoneID in (12)

--LAST SUNDAY OF MARCH, LAST SUNDAY OF OCTOBER
--Id: 16 -> Belgrade, Bratislava, Budapest, Ljubljana, Prague -> Central Europe Standard Time
--Id: 17 -> Sarajevo, Skopje, Warsaw, Zagreb -> Central European Standard Time
--Id: 52 -> Brussels, Copenhagen, Madrid, Paris -> Romance Standard Time
--Id: 71 -> Amsterdam, Berlin, Bern, Rome, Stockholm, Vienna -> W. Europe Standard Time
update gen_timezone_s set 
CurDSTstart6 = '2018-03-25 02:00:00.000', CurDSTend6 = '2018-10-28 03:00:00.000',
CurDSTstart7 = '2019-03-31 02:00:00.000', CurDSTend7 = '2019-10-27 03:00:00.000',
CurDSTstart8 = '2020-03-29 02:00:00.000', CurDSTend8 = '2020-10-25 03:00:00.000',
CurDSTstart9 = '2021-03-28 02:00:00.000', CurDSTend9 = '2021-10-31 03:00:00.000'
where TimeZoneID in (16,17,52,71,75)

--LAST SATURDAY OF MARCH, LAST SATURDAY OF OCTOBER
--Id: 32 -> Greenland -> Greenland Standard Time
update gen_timezone_s set 
CurDSTstart6 = '2018-03-24 22:00:00.000', CurDSTend6 = '2018-10-27 23:00:00.000',
CurDSTstart7 = '2019-03-30 22:00:00.000', CurDSTend7 = '2019-10-26 23:00:00.000',
CurDSTstart8 = '2020-03-28 22:00:00.000', CurDSTend8 = '2020-10-31 23:00:00.000',
CurDSTstart9 = '2021-03-27 22:00:00.000', CurDSTend9 = '2021-10-30 23:00:00.000'
where TimeZoneID in (32)

--FIRST SUNDAY OF APRIL, LAST SUNDAY OF OCTOBER
--Id: 40 -> Mexico -> Mexico Standard Time
update gen_timezone_s set 
CurDSTstart6 = '2018-04-01 02:00:00.000', CurDSTend6 = '2018-10-28 03:00:00.000',
CurDSTstart7 = '2019-04-07 02:00:00.000', CurDSTend7 = '2019-10-27 03:00:00.000',
CurDSTstart8 = '2020-04-05 02:00:00.000', CurDSTend8 = '2020-10-25 03:00:00.000',
CurDSTstart9 = '2021-04-04 02:00:00.000', CurDSTend9 = '2021-10-31 03:00:00.000'
where TimeZoneID in (40)

-- LAST SUNDAY OF SEPTEMBER, FIRST SUNDAY OF APRIL
--Id: 46 -> Auckland, Wellington -> New Zealand Standard Time
update gen_timezone_s set 
CurDSTstart6 = '2017-09-24 02:00:00.000', CurDSTend6 = '2018-04-01 03:00:00.000',
CurDSTstart7 = '2018-09-30 02:00:00.000', CurDSTend7 = '2019-04-07 03:00:00.000',
CurDSTstart8 = '2019-09-29 02:00:00.000', CurDSTend8 = '2020-04-05 03:00:00.000',
CurDSTstart9 = '2020-09-27 02:00:00.000', CurDSTend9 = '2021-04-04 03:00:00.000'
where TimeZoneID in (46)

-- FIRST SUNDAY OF OCTOBER, FIRST SUNDAY OF APRIL
--Id: 8 -> Canberra, Melbourne, Sydney -> AUS Eastern Standard Time
--Id: 13 -> Adelaide -> Cen. Australia Standard Time
--Id: 63 -> Hobart -> Tasmania Standard Time
UPDATE gen_timezone_s SET 
CurDSTstart6 = '2017-10-01 02:00:00.000', CurDSTend6 = '2018-04-01 03:00:00.000',
CurDSTstart7 = '2018-10-07 02:00:00.000', CurDSTend7 = '2019-04-07 03:00:00.000',
CurDSTstart8 = '2019-10-06 02:00:00.000', CurDSTend8 = '2020-04-05 03:00:00.000',
CurDSTstart9 = '2020-10-04 02:00:00.000', CurDSTend9 = '2021-04-04 03:00:00.000'
WHERE TimeZoneID IN (63,8,13)

-- SECOND SUNDAY OF OCTOBER, SECOND SUNDAY OF MARCH
--Id: 50 -> Santiago -> Pacific SA Standard Time
update gen_timezone_s set 
CurDSTstart6 = '2017-10-08 00:00:00.000', CurDSTend6 = '2018-03-11 00:00:00.000',
CurDSTstart7 = '2018-10-14 00:00:00.000', CurDSTend7 = '2019-03-10 00:00:00.000',
CurDSTstart8 = '2019-10-13 00:00:00.000', CurDSTend8 = '2020-03-08 00:00:00.000',
CurDSTstart9 = '2020-10-11 00:00:00.000', CurDSTend9 = '2021-03-14 00:00:00.000'
where TimeZoneID in (50)

-- THIRD SUNDAY OF OCTOBER, THIRD SUNDAY OF FEB
--Id: 25 -> Brasilia -> E. South America Standard Time
update gen_timezone_s set 
CurDSTstart6 = '2017-10-15 00:00:00.000', CurDSTend6 = '2018-02-18 00:00:00.000',
CurDSTstart7 = '2018-10-21 00:00:00.000', CurDSTend7 = '2019-02-17 00:00:00.000',
CurDSTstart8 = '2019-10-20 00:00:00.000', CurDSTend8 = '2020-02-16 00:00:00.000',
CurDSTstart9 = '2020-10-18 00:00:00.000', CurDSTend9 = '2021-02-21 00:00:00.000'
where TimeZoneID in (25)
