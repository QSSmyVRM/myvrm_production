Directory: \DSTScripts
File "DSTInformation.sql" contains master data of DST date time for coming years for all time zones.

Directory: \DSTScripts\UpdateExistingConferences (not recommended)
It contains SQL scripts to update date time of existing conferences step by step.
Please omit step 5 (file "Step 5 DSTInformation.sql") as master data script is already present in parent directory. 
