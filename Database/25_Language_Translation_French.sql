﻿
delete from Launguage_Translation_Text where LanguageID =5

insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'(Open for Registration)','(Ouvert pour l''enregistrement)')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'(Work orders deliver by date/time do not match with conference date/time.)','(La date/l''heure de livraison des ordres de travail ne correspond pas à la date/l''heure de la conférence.)')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'(Work orders start/end date/time do not match with conference start/end date/time.)','(La date/l''heure de début/fin des ordres de travail ne correspond pas à la date/heure de début/fin de la conférence.)')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'(No custom options)','(Aucune option personnalisée)')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'(No participants)','(Aucun participant)')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'(No rooms)','(Aucune salle)')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'(No work orders)','(Aucun ordre de travail)')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Address Type and Protocol don''t match. Please verify','Type d''adresse et protocole incompatibles. Veuillez vérifier')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Aggregate Usage Report','Rapport sur l''usage total')

insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'All Audio/Visual Work Orders','Tous les ordres de travail audio/visuel')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'All Housekeeping Work Orders','Tous les ordres de travail de ménage')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Apply Theme','Appliquer le thème')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'At least one item should have a requested quantity greater than 0.','Au moins un élément devrait une quantité demandée supérieure à 0..')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'At least one menu should have a requested quantity > 0.','Au moins un menu devrait avoir une quantité demandée supérieure à 0.')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'At most 5 profiles can be created.','Au plus 5 profils peuvent être créés.')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Attach Files','Joindre des fichiers')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Audio Only Conference','Conférence avec audio seulement')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Audio/Video Conference','Conférence Audio/Vidéo')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Audio/Visual','Audio/visuel ')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Audio/Visual Work Orders','Ordres de travail audio/visuel')
Insert into Launguage_Translation_Text ( LanguageID, Text, TranslatedText) values (5,'Auto select...','Sélection automatique...')
Insert into Launguage_Translation_Text ( LanguageID, Text, TranslatedText) values (5,'Billing Code already exists in the list','Le code de facturation existe déjà dans la liste')
Insert into Launguage_Translation_Text ( LanguageID, Text, TranslatedText) values (5,'Billing Code description should be within 35 characters','La description du code de facturation ne doit pas dépasser 35 caractères')
Insert into Launguage_Translation_Text ( LanguageID, Text, TranslatedText) values (5,'Cancel','Annuler')
Insert into Launguage_Translation_Text ( LanguageID, Text, TranslatedText) values (5,'Cascade','Chevauchement')
Insert into Launguage_Translation_Text ( LanguageID, Text, TranslatedText) values (5,'Catering ','Service de restauration ')
Insert into Launguage_Translation_Text ( LanguageID, Text, TranslatedText) values (5,'Catering Providers','Fournisseurs de service de restauration')
Insert into Launguage_Translation_Text ( LanguageID, Text, TranslatedText) values (5,'Catering Work Orders','Ordres de travail pour le service de restauration')
Insert into Launguage_Translation_Text ( LanguageID, Text, TranslatedText) values (5,'CC','CC')
Insert into Launguage_Translation_Text ( LanguageID, Text, TranslatedText) values (5,'Click Edit/Create button to save these changes.','Cliquez sur le bouton Modifier/Créer pour enregistrer ces changements.')
Insert into Launguage_Translation_Text ( LanguageID, Text, TranslatedText) values (5,'Click Update in order to save changes. Otherwise click Cancel to return to the catering work orders summary page for this conference.','Cliquez sur Mise à jour pour enregistrer ces changements. Sinon, cliquez sur Annuler pour retourner sur la page récapitulative des ordres de travail pour le service de restauration de cette conférence.')
Insert into Launguage_Translation_Text ( LanguageID, Text, TranslatedText) values (5,'Click Update to re-calculate total OR click Edit to update this Workorder.','Cliquez sur Mise à jour pour recalculer le total OU cliquez sur Modifier pour mettre à jour cet Ordre de travail.')
Insert into Launguage_Translation_Text ( LanguageID, Text, TranslatedText) values (5,'Clone All','Tout cloner')


Insert into Launguage_Translation_Text ( LanguageID, Text, TranslatedText) values (5,'Close','Fermer')
Insert into Launguage_Translation_Text ( LanguageID, Text, TranslatedText) values (5,'Codian MCU Configuration','Configuration du MCU Codian')
Insert into Launguage_Translation_Text ( LanguageID, Text, TranslatedText) values (5,'Compare Conference Room Resource','Comparer les moyens des salles de conférence')
Insert into Launguage_Translation_Text ( LanguageID, Text, TranslatedText) values (5,'Completed','Terminé')
Insert into Launguage_Translation_Text ( LanguageID, Text, TranslatedText) values (5,'Conference Details','Détails de la conférence')
Insert into Launguage_Translation_Text ( LanguageID, Text, TranslatedText) values (5,'Conference End','Fin de la conférence')
Insert into Launguage_Translation_Text ( LanguageID, Text, TranslatedText) values (5,'Conference Room Resource','Moyens de la salle de conférence')
Insert into Launguage_Translation_Text ( LanguageID, Text, TranslatedText) values (5,'Conference Start','Commencement de la conférence')
Insert into Launguage_Translation_Text ( LanguageID,Text, TranslatedText) values (5,'Conference Title','Titre de la conférence')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Your Conference has been scheduled successfully.','Votre conférence a été programmée avec succès.')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Your conference has been scheduled successfully. Email notifications have been sent to participants.','Votre conférence a été programmée avec succès. Les notifications par courriel ont été envoyés aux participants.')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Congratulations! Your conference has been scheduled successfully. Email notifications have been sent to participants.','Félicitations! Votre conférence a été programmée avec succès. Les notifications par courriel ont été envoyés aux participants.')

Insert into Launguage_Translation_Text ( LanguageID, Text, TranslatedText) values (5,'Connect','Connecter')
Insert into Launguage_Translation_Text ( LanguageID, Text, TranslatedText) values (5,'Create','Créer')
Insert into Launguage_Translation_Text ( LanguageID, Text, TranslatedText) values (5,'Create Group','Créer un groupe')
Insert into Launguage_Translation_Text ( LanguageID, Text, TranslatedText) values (5,'Create New','Créer un nouveau')
Insert into Launguage_Translation_Text ( LanguageID, Text, TranslatedText) values (5,'Create New A/V Set','Créer un nouvel ensemble A/V')
Insert into Launguage_Translation_Text ( LanguageID, Text, TranslatedText) values (5,'Create New Catering Provider','Créer un nouveau fournisseur de service de restauration')
Insert into Launguage_Translation_Text ( LanguageID, Text, TranslatedText) values (5,'Create New Endpoint','Créer un nouveau point terminal')
Insert into Launguage_Translation_Text ( LanguageID, Text, TranslatedText) values (5,'Create New H/K Group','Créer un nouveau groupe H/K')
Insert into Launguage_Translation_Text ( LanguageID, Text, TranslatedText) values (5,'Create New MCU','Créer un nouveau MCU')
Insert into Launguage_Translation_Text ( LanguageID, Text, TranslatedText) values (5,'Create New Organization','Créer une nouvelle organisation')
Insert into Launguage_Translation_Text ( LanguageID, Text, TranslatedText) values (5,'Create New User','Créer un nouvel utilisateur')
Insert into Launguage_Translation_Text ( LanguageID, Text, TranslatedText) values (5,'Create New User Template','Créer un nouveau modèle d''utilisateur')
Insert into Launguage_Translation_Text ( LanguageID, Text, TranslatedText) values (5,'Custom Date Conferences Total Usage Report','Rapport sur l''usage total des conférences à la date indiquée')
Insert into Launguage_Translation_Text ( LanguageID, Text, TranslatedText) values (5,'Custom option description should be within 35 characters','La description de l''option personnalisée ne doit pas dépasser 35 caractères')
Insert into Launguage_Translation_Text ( LanguageID, Text, TranslatedText) values (5,'Custom option title already exists in the list','Le titre de l''option personnalisée existe déjà dans la liste')
Insert into Launguage_Translation_Text ( LanguageID, Text, TranslatedText) values (5,'Custom Option Value','Valeur de l''option personnalisée')
Insert into Launguage_Translation_Text ( LanguageID, Text, TranslatedText) values (5,'Default Protocol does not match with selected Address Type.','Le protocole par défaut est incompatible avec le type d''adresse sélectionné.')
Insert into Launguage_Translation_Text ( LanguageID, Text, TranslatedText) values (5,'Delete All','Tout effacer')
Insert into Launguage_Translation_Text ( LanguageID, Text, TranslatedText) values (5,'Delete Log','Effacer le fichier-journal')
Insert into Launguage_Translation_Text ( LanguageID, Text, TranslatedText) values (5,'Deleted','Effacé')
Insert into Launguage_Translation_Text ( LanguageID, Text, TranslatedText) values (5,'Disconnected','Déconnecté')
Insert into Launguage_Translation_Text ( LanguageID, Text, TranslatedText) values (5,'DLL Name','Noms du DLL')
Insert into Launguage_Translation_Text ( LanguageID, Text, TranslatedText) values (5,'Edit','Modifier')
Insert into Launguage_Translation_Text ( LanguageID, Text, TranslatedText) values (5,'Edit Existing User','Modifier un utilisateur existant')
Insert into Launguage_Translation_Text ( LanguageID, Text, TranslatedText) values (5,'Edit Group','Modifier un groupe')
Insert into Launguage_Translation_Text ( LanguageID, Text, TranslatedText) values (5,'Edit Organization Profile','Modifier un profil d''organisation')
Insert into Launguage_Translation_Text ( LanguageID, Text, TranslatedText) values (5,'Edit User Template','Modifier un modèle d''utilisateur')
Insert into Launguage_Translation_Text ( LanguageID, Text, TranslatedText) values (5,'Enabled','Activé')
Insert into Launguage_Translation_Text ( LanguageID, Text, TranslatedText) values (5,'Enter requested quantity for corresponding items.','Saisir la quantité voulue pour les éléments correspondants.')
Insert into Launguage_Translation_Text ( LanguageID, Text, TranslatedText) values (5,'Error deleteting work order.','Erreur lors de l''effacement de l''ordre de travail.')
Insert into Launguage_Translation_Text ( LanguageID, Text, TranslatedText) values (5,'Error Uploading Image. Please check your VRM Administrator.','Erreur lors de la mise en ligne de l''image. Veuillez vérifier votre administrateur VRM.')
Insert into Launguage_Translation_Text ( LanguageID, Text, TranslatedText) values (5,'Event level to be logged','Niveau d''évènement devant être enregistré')
Insert into Launguage_Translation_Text ( LanguageID, Text, TranslatedText) values (5,'External Attendee','Participant externe')
Insert into Launguage_Translation_Text ( LanguageID, Text, TranslatedText) values (5,'File attachment is greater than 10MB. File has not been uploaded.','La pièce jointe excède les 10 Mo. Le fichier n''a pas été mis en ligne.')
Insert into Launguage_Translation_Text ( LanguageID, Text, TranslatedText) values (5,'File trying to upload already exists','Le fichier dont on souhaite la mise en ligne existe déjà')
Insert into Launguage_Translation_Text ( LanguageID, Text, TranslatedText) values (5,'Guest','Invité')
Insert into Launguage_Translation_Text ( LanguageID, Text, TranslatedText) values (5,'Guest Address Book','Carnet d''adresse des invités')
Insert into Launguage_Translation_Text ( LanguageID, Text, TranslatedText) values (5,'Housekeeping ','Ménage')
Insert into Launguage_Translation_Text ( LanguageID, Text, TranslatedText) values (5,'Housekeeping Groups','Groupes de Ménage')
Insert into Launguage_Translation_Text ( LanguageID, Text, TranslatedText) values (5,'Housekeeping Work Orders','Ordres de travail de ménage')
Insert into Launguage_Translation_Text ( LanguageID, Text, TranslatedText) values (5,'Illegal Search','Recherche interdite')
Insert into Launguage_Translation_Text ( LanguageID, Text, TranslatedText) values (5,'Image size is greater than 5MB. File has not been uploaded.','L''image fait plus de 5 Mo. Le fichier n''a pas été mis en ligne.')
Insert into Launguage_Translation_Text ( LanguageID, Text, TranslatedText) values (5,'Immediate Conference','Conférence immédiate')
Insert into Launguage_Translation_Text ( LanguageID, Text, TranslatedText) values (5,'Include in Email','Inclure dans l''e-mail')
Insert into Launguage_Translation_Text ( LanguageID, Text, TranslatedText) values (5,'Invalid Date Range: FROM date should be prior to TO date.','Plage de dates invalide : La date DU devrait précéder la date AU.')
Insert into Launguage_Translation_Text ( LanguageID, Text, TranslatedText) values (5,'Invalid duration','Durée invalide')
Insert into Launguage_Translation_Text ( LanguageID, Text, TranslatedText) values (5,'Invalid Duration.Conference duration should be minimum of 15 mins.','Durée invalide. La durée de la conférence devrait être d''au minimum 15 min.')
Insert into Launguage_Translation_Text ( LanguageID, Text, TranslatedText) values (5,'Invalid Interface Type and Address Type.','Type d''interface et type d''adresse invalides.')
Insert into Launguage_Translation_Text ( LanguageID, Text, TranslatedText) values (5,'Invalid IP Address.','Adresse IP invalide.')

Insert into Launguage_Translation_Text ( LanguageID, Text, TranslatedText) values (5,'Invalid MPI Address','Adresse MPI invalide')
Insert into Launguage_Translation_Text ( LanguageID, Text, TranslatedText) values (5,'Invalid selection for Address Type, Protocol and Connection Type.','Sélection invalide pour le type d''adresse, de protocole et de connexion.')
Insert into Launguage_Translation_Text ( LanguageID, Text, TranslatedText) values (5,'Invalid selection for connection type and protocol. MCU is mandatory for direct calls with MPI proto','Sélection invalide pour le type de connexion et de protocole. Le MCU est obligatoire pour les appels directs avec le protocole MPI.')
Insert into Launguage_Translation_Text ( LanguageID, Text, TranslatedText) values (5,'Invalid Selection.','Sélection invalide.')
Insert into Launguage_Translation_Text ( LanguageID, Text, TranslatedText) values (5,'Invalid Start and End Range','Le début et la fin de la plage sont invalides')
Insert into Launguage_Translation_Text ( LanguageID, Text, TranslatedText) values (5,'Invalid Start Date or Time. It should be greater than current time in','La date ou l''heure de début est invalide. Elle devrait être postérieure à l''heure actuelle à')
Insert into Launguage_Translation_Text ( LanguageID, Text, TranslatedText) values (5,'Invalid User Type','Type d''utilisateur invalide')
Insert into Launguage_Translation_Text ( LanguageID, Text, TranslatedText) values (5,'Inventory Set has been changed and no longer belongs to the selected room.','L''inventaire a été changé et n''est plus assigné à la salle sélectionnée.')
Insert into Launguage_Translation_Text ( LanguageID, Text, TranslatedText) values (5,'Inventory Sets','Inventaires')

Insert into Launguage_Translation_Text ( LanguageID, Text, TranslatedText) values (5,'Item Name already exists in the list','Le nom d''élément existe déjà dans la liste')
Insert into Launguage_Translation_Text ( LanguageID, Text, TranslatedText) values (5,'Last Month Conferences Total Usage Report','Rapport sur l''usage total des conférences du mois dernier')
Insert into Launguage_Translation_Text ( LanguageID, Text, TranslatedText) values (5,'Last Updated Date','Dernière date mise à jour')
Insert into Launguage_Translation_Text ( LanguageID, Text, TranslatedText) values (5,'Last Week Conferences Total Usage Report','Rapport sur l''usage total des conférences de la semaine dernière')
Insert into Launguage_Translation_Text ( LanguageID, Text, TranslatedText) values (5,'Manage Departments','Gérer les départements')
Insert into Launguage_Translation_Text ( LanguageID, Text, TranslatedText) values (5,'Manage Guests','Gérer les invités')
Insert into Launguage_Translation_Text ( LanguageID, Text, TranslatedText) values (5,'Manage Inactive Users','Gérer les utilisateurs inactifs')
Insert into Launguage_Translation_Text ( LanguageID, Text, TranslatedText) values (5,'Manage MCUs','Gérer les MCU')
Insert into Launguage_Translation_Text ( LanguageID, Text, TranslatedText) values (5,'Manage Tiers','Gérer les niveaux')
Insert into Launguage_Translation_Text ( LanguageID, Text, TranslatedText) values (5,'Manage Users','Gérer les utilisateurs')
Insert into Launguage_Translation_Text ( LanguageID, Text, TranslatedText) values (5,'Max. length for start and end range is 22 characters.','La longueur max. pour la plage de début et de fin est de 22 caractères.')
Insert into Launguage_Translation_Text ( LanguageID, Text, TranslatedText) values (5,'MCU','MCU')
Insert into Launguage_Translation_Text ( LanguageID, Text, TranslatedText) values (5,'MCU is mandatory for MPI protocol','Le MCU est obligatoire pour le protocole MPI')
Insert into Launguage_Translation_Text ( LanguageID, Text, TranslatedText) values (5,'Modify work order to conform to your room changes. Work orders and Room names are as follows:','Modifier l''ordre de travail pour l''ajuster à vos modifications de la salle. Les ordres de travail et les noms de salle sont les suivants :')
Insert into Launguage_Translation_Text ( LanguageID, Text, TranslatedText) values (5,'Module Type','Type de module')
Insert into Launguage_Translation_Text ( LanguageID, Text, TranslatedText) values (5,'More than 10 IP Services can not be added.','Impossible d''ajouter plus de 10 services IP.')
Insert into Launguage_Translation_Text ( LanguageID, Text, TranslatedText) values (5,'More than 10 ISDN Services can not be added.','Impossible d''ajouter plus de 10 services ISDN.')
Insert into Launguage_Translation_Text ( LanguageID, Text, TranslatedText) values (5,'My Incomplete Audio/Visual Work Orders','Mes ordres de travail audio/visuel incomplets')

Insert into Launguage_Translation_Text ( LanguageID, Text, TranslatedText) values (5,'My Incomplete Catering Work Orders','Mes ordres de travail pour le service de restauration incomplets')
Insert into Launguage_Translation_Text ( LanguageID, Text, TranslatedText) values (5,'My Incomplete Housekeeping Work Orders','Mes ordres de travail pour le ménage incomplets')
Insert into Launguage_Translation_Text ( LanguageID, Text, TranslatedText) values (5,'My Pending Audio/Visual Work Orders','Mes ordres de travail audio/visuel prévus')
Insert into Launguage_Translation_Text ( LanguageID, Text, TranslatedText) values (5,'My Pending Catering Work Orders','Mes ordres de travail pour le service de restauration prévus')
Insert into Launguage_Translation_Text ( LanguageID, Text, TranslatedText) values (5,'My Pending Housekeeping Work Orders','Mes ordres de travail pour le ménage prévus')
Insert into Launguage_Translation_Text ( LanguageID, Text, TranslatedText) values (5,'My Preferences','Mes préférences')
Insert into Launguage_Translation_Text ( LanguageID, Text, TranslatedText) values (5,'myVRM Address Book','Carnet d''adresse myVRM')

Insert into Launguage_Translation_Text ( LanguageID, Text, TranslatedText) values (5,'No Conference-AV','Aucune conférence-AV')
Insert into Launguage_Translation_Text ( LanguageID, Text, TranslatedText) values (5,'No Custom Options found.','Aucune option personnalisée trouvée.')
Insert into Launguage_Translation_Text ( LanguageID, Text, TranslatedText) values (5,'No Data Found','Aucune information trouvée')
Insert into Launguage_Translation_Text ( LanguageID, Text, TranslatedText) values (5,'No Logs Found.','Aucun fichier-journal trouvé.')
Insert into Launguage_Translation_Text ( LanguageID, Text, TranslatedText) values (5,'No Recurrence','Aucune récurrence')
Insert into Launguage_Translation_Text ( LanguageID, Text, TranslatedText) values (5,'None','Rien')
Insert into Launguage_Translation_Text ( LanguageID, Text, TranslatedText) values (5,'Not Started','Non commencé')
Insert into Launguage_Translation_Text ( LanguageID, Text, TranslatedText) values (5,'On MCU','Sur le MCU')
Insert into Launguage_Translation_Text ( LanguageID, Text, TranslatedText) values (5,'Ongoing','En cours')
Insert into Launguage_Translation_Text ( LanguageID, Text, TranslatedText) values (5,'Only one image can be uploaded for footer information.','Une seule image peut être mise en ligne pour les informations du pied de page.')
Insert into Launguage_Translation_Text ( LanguageID, Text, TranslatedText) values (5,'Only two Endpoints can be selected for Point-To-Point Conference.','Seuls deux points terminaux peuvent être sélectionnés pour la conférence point à point.')
Insert into Launguage_Translation_Text ( LanguageID, Text, TranslatedText) values (5,'Operation Successful!','Opération réalisée avec succès!')
Insert into Launguage_Translation_Text ( LanguageID, Text, TranslatedText) values (5,'Operation Successful! Please logout and re-login to see the changes.','Opération réalisée avec succès! Veuillez vous déconnecter puis vous reconnecter pour voir les changements.')
Insert into Launguage_Translation_Text ( LanguageID, Text, TranslatedText) values (5,'Operation Successful! Reminder email has been sent to selected participant.','Opération réalisée avec succès! Un e-mail de rappel a été envoyé aux participants sélectionnés.')
Insert into Launguage_Translation_Text ( LanguageID, Text, TranslatedText) values (5,'Operation Successful! Reminder email has been sent to the person-in-charge of the work order.','Opération réalisée avec succès! Un e-mail de rappel a été envoyé à la personne responsable de l''ordre de travail.')
Insert into Launguage_Translation_Text ( LanguageID, Text, TranslatedText) values (5,'Option name already exists in the list','Le nom de l''option existe déjà dans la liste')
Insert into Launguage_Translation_Text ( LanguageID, Text, TranslatedText) values (5,'Overwrite events older than','Écrire par-dessus les évènements plus anciens que')
Insert into Launguage_Translation_Text ( LanguageID, Text, TranslatedText) values (5,'Page(s):','Page(s):')
Insert into Launguage_Translation_Text ( LanguageID, Text, TranslatedText) values (5,'Paused','En pause')
Insert into Launguage_Translation_Text ( LanguageID, Text, TranslatedText) values (5,'Pending','prévu(e)(s)')
Insert into Launguage_Translation_Text ( LanguageID, Text, TranslatedText) values (5,'Please add atleast one option to the custom option.','Veuillez ajouter au moins une option à l''option personnalisée.')
Insert into Launguage_Translation_Text ( LanguageID, Text, TranslatedText) values (5,'Please associate at least one menu with each work order.','Veuillez associer au moins un menu à chaque ordre de travail.')
Insert into Launguage_Translation_Text ( LanguageID, Text, TranslatedText) values (5,'Please check Start and Completed Date/Time for this workorder.','Veuillez vérifier la date/l''heure de début et d''achèvement pour cet ordre de travail.')
Insert into Launguage_Translation_Text ( LanguageID, Text, TranslatedText) values (5,'Please check the password. It must be unique','Veuillez vérifier le mot de passe. Il doit être unique')
Insert into Launguage_Translation_Text ( LanguageID, Text, TranslatedText) values (5,'Please check the start and end time for this workorder.','Veuillez vérifier la date/l''heure de début et de fin pour cet ordre de travail.')
Insert into Launguage_Translation_Text ( LanguageID, Text, TranslatedText) values (5,'Please enter a quantity for each selected item. Each item should have a quantity > 0','Veuillez saisir une valeur pour chaque élément sélectionné. La quantité de chaque élément doit être supérieure à 0')
Insert into Launguage_Translation_Text ( LanguageID, Text, TranslatedText) values (5,'Please enter email address for test mail','Veuillez saisir une adresse e-mail pour tester la messagerie')
Insert into Launguage_Translation_Text ( LanguageID, Text, TranslatedText) values (5,'Please enter the ExchangeID for the profile name ','Veuillez saisir l''ExchangeID pour le nom du profil ')
Insert into Launguage_Translation_Text ( LanguageID, Text, TranslatedText) values (5,'Please enter TO and FROM dates.','Veuillez saisir les dates DU et AU.')
Insert into Launguage_Translation_Text ( LanguageID, Text, TranslatedText) values (5,'Please enter valid date range of at most a week.','Veuillez saisir une plage de date valide ne dépassant pas une semaine.')
Insert into Launguage_Translation_Text ( LanguageID, Text, TranslatedText) values (5,'Please fill in all the values for Change Request','Veuillez remplir toutes les valeurs de la demande de changement')
Insert into Launguage_Translation_Text ( LanguageID, Text, TranslatedText) values (5,'Please respond to the following conference','Veuillez répondre à la conférence suivante')
Insert into Launguage_Translation_Text ( LanguageID, Text, TranslatedText) values (5,'Please select a Housekeeping Group now.','Veuillez sélectionner un groupe de ménage maintenant.')
Insert into Launguage_Translation_Text ( LanguageID, Text, TranslatedText) values (5,'Please select a Room','Veuillez sélectionner une Salle')
Insert into Launguage_Translation_Text ( LanguageID, Text, TranslatedText) values (5,'Please select a valid IP Address.','Veuillez sélectionner une adresse IP valide.')
Insert into Launguage_Translation_Text ( LanguageID, Text, TranslatedText) values (5,'Please select a valid ISDN Address.','Veuillez sélectionner une adresse ISDN valide.')
Insert into Launguage_Translation_Text ( LanguageID, Text, TranslatedText) values (5,'Please select an Address Type.','Veuillez sélectionner un type d''adresse.')
Insert into Launguage_Translation_Text ( LanguageID, Text, TranslatedText) values (5,'Please save or cancel current workorder prior to editing or deleting existing workorders.','Veuillez enregistrer ou annuler l''ordre de travail actuel avant de modifier ou d''effacer les ordres de travail existants.')
Insert into Launguage_Translation_Text ( LanguageID, Text, TranslatedText) values (5,'Please save/update or cancel current workorder prior to editing or deleting existing workorders.','Veuillez enregistrer/mettre à jour ou annuler l''ordre de travail actuel avant de modifier ou d''effacer les ordres de travail existants.')
Insert into Launguage_Translation_Text ( LanguageID, Text, TranslatedText) values (5,'Please select an entity','Veuillez sélectionner une entité')

Insert into Launguage_Translation_Text ( LanguageID, Text, TranslatedText) values (5,'Please select atleast one Department.','Veuillez sélectionner au moins un département.')
Insert into Launguage_Translation_Text ( LanguageID, Text, TranslatedText) values (5,'Please select atleast one Endpoint.','Veuillez sélectionner au moins un point terminal. ')
Insert into Launguage_Translation_Text ( LanguageID, Text, TranslatedText) values (5,'Please select atleast one MCU.','Veuillez sélectionner au moins un MCU.')
Insert into Launguage_Translation_Text ( LanguageID, Text, TranslatedText) values (5,'Please select atleast one Room.','Veuillez sélectionner au moins une salle.')
Insert into Launguage_Translation_Text ( LanguageID, Text, TranslatedText) values (5,'Please select atleast one Tier.','Veuillez sélectionner au moins un niveau.')
Insert into Launguage_Translation_Text ( LanguageID, Text, TranslatedText) values (5,'Please select MCU.','Veuillez sélectionner un MCU.')
Insert into Launguage_Translation_Text ( LanguageID, Text, TranslatedText) values (5,'Please select the corresponding menu for a valid quantity.','Veuillez sélectionner le menu correspondant pour une quantité valide.')
Insert into Launguage_Translation_Text ( LanguageID, Text, TranslatedText) values (5,'Please select...','Veuillez sélectionner...')
Insert into Launguage_Translation_Text ( LanguageID, Text, TranslatedText) values (5,'Please specify Quantity and either click update to calculate total price for this work order or click create to add workorder.','Veuillez spécifier la quantité puis cliquer sur Mise à jour pour calculer le prix total pour cet ordre de travail ou cliquez sur Créer pour ajouter un ordre de travail.')
Insert into Launguage_Translation_Text ( LanguageID, Text, TranslatedText) values (5,'Please visit the Advanced Audio/Video Settings tab','Veuillez vous rendre sur l''onglet Réglages avancés Audio/Vidéo ')
Insert into Launguage_Translation_Text ( LanguageID, Text, TranslatedText) values (5,'Point-To-Point Conference','Conférence point à point')
Insert into Launguage_Translation_Text ( LanguageID, Text, TranslatedText) values (5,'Polycom MGC Configuration','Configuration du MGC Polycom')
Insert into Launguage_Translation_Text ( LanguageID, Text, TranslatedText) values (5,'Polycom RMX Configuration','Configuration du RMX Polycom')
Insert into Launguage_Translation_Text ( LanguageID, Text, TranslatedText) values (5,'Post Conference End','Fin de l''après-conférence')
Insert into Launguage_Translation_Text ( LanguageID, Text, TranslatedText) values (5,'Pre Conference Start','Commencement de l''avant-conférence')
Insert into Launguage_Translation_Text ( LanguageID, Text, TranslatedText) values (5,'PRI Allocation Report','Rapport d''allocation de PRI')
Insert into Launguage_Translation_Text ( LanguageID, Text, TranslatedText) values (5,'Radvision MCU Configuration','Configuration du MCU Radvision')
Insert into Launguage_Translation_Text ( LanguageID, Text, TranslatedText) values (5,'Report is not ready to view.','Le rapport n''est pas prêt à être vu.')
Insert into Launguage_Translation_Text ( LanguageID, Text, TranslatedText) values (5,'Room','Salle')
Insert into Launguage_Translation_Text ( LanguageID, Text, TranslatedText) values (5,'Room Attendee','Participant de la salle')
Insert into Launguage_Translation_Text ( LanguageID, Text, TranslatedText) values (5,'Room Conference','Conférence de la salle')
Insert into Launguage_Translation_Text ( LanguageID, Text, TranslatedText) values (5,'Rooms Found :','Salles trouvées :')
Insert into Launguage_Translation_Text ( LanguageID, Text, TranslatedText) values (5,'Running','En cours')
Insert into Launguage_Translation_Text ( LanguageID, Text, TranslatedText) values (5,'Scheduled','Planifiée')
Insert into Launguage_Translation_Text ( LanguageID, Text, TranslatedText) values (5,'Search Result(s) for Audio/Visual Work Orders','Résultat(s) de recherche pour les ordres de travail audio/visuels')
Insert into Launguage_Translation_Text ( LanguageID, Text, TranslatedText) values (5,'Search Result(s) for Catering Work Orders','Résultat(s) de recherche pour les ordres de travail de service de restauration')
Insert into Launguage_Translation_Text ( LanguageID, Text, TranslatedText) values (5,'Search Result(s) for Housekeeping Work Orders','Résultat(s) de recherche pour les ordres de travail de ménage')
Insert into Launguage_Translation_Text ( LanguageID, Text, TranslatedText) values (5,'Search Results','Résultats de recherche')
Insert into Launguage_Translation_Text ( LanguageID, Text, TranslatedText) values (5,'See Recurring Pattern','Voir les modes récurrents')
Insert into Launguage_Translation_Text ( LanguageID, Text, TranslatedText) values (5,'Select a Set from the Drop down list.','Sélectionnez un ensemble dans la liste déroulante.')
Insert into Launguage_Translation_Text ( LanguageID, Text, TranslatedText) values (5,'Select Caller/Callee','Sélectionnez l''appelant/l''appelé')
Insert into Launguage_Translation_Text ( LanguageID, Text, TranslatedText) values (5,'Selected bridge for this template does not support MPI calls.','Le pont sélectionné pour ce modèle ne prend pas en charge les appels MPI.')
Insert into Launguage_Translation_Text ( LanguageID, Text, TranslatedText) values (5,'Set Customized Instances','Configurer des instances personnalisées')
Insert into Launguage_Translation_Text ( LanguageID, Text, TranslatedText) values (5,'Show Uploaded Files','Afficher les fichiers mis en ligne')
Insert into Launguage_Translation_Text ( LanguageID, Text, TranslatedText) values (5,'Some Additional Comments fields are missing. Please contact your VRM Administrator.','Certains champs de commentaires supplémentaires sont absents. Veuillez contacter votre administrateur VRM.')
Insert into Launguage_Translation_Text ( LanguageID, Text, TranslatedText) values (5,'Status being Updated.','État en cours de mise à jour.')
Insert into Launguage_Translation_Text ( LanguageID, Text, TranslatedText) values (5,'Stopped','Arrêté')
Insert into Launguage_Translation_Text ( LanguageID, Text, TranslatedText) values (5,'Submit','Soumettre')
Insert into Launguage_Translation_Text ( LanguageID, Text, TranslatedText) values (5,'Submit Conference','Créer la Conférence')
Insert into Launguage_Translation_Text ( LanguageID, Text, TranslatedText) values (5,'Tandberg MCU Configuration','Configuration du MCU Tandberg')
Insert into Launguage_Translation_Text ( LanguageID, Text, TranslatedText) values (5,'Terminate','Terminer')
Insert into Launguage_Translation_Text ( LanguageID, Text, TranslatedText) values (5,'Terminated','Terminé')
Insert into Launguage_Translation_Text ( LanguageID, Text, TranslatedText) values (5,'There are no Audio/Visual work orders associated with this conference.','Aucun ordre de travail audio/visuel n''est associé à cette conférence.')
Insert into Launguage_Translation_Text ( LanguageID, Text, TranslatedText) values (5,'There are no Catering work orders associated with this conference.','Aucun ordre de travail de service de restauration n''est associé à cette conférence.')
Insert into Launguage_Translation_Text ( LanguageID, Text, TranslatedText) values (5,'There are no Housekeeping Groups available for this room. Please select another room.','Aucun groupe de ménage n''est disponible pour cette salle. Veuillez sélectionner une autre salle.')
Insert into Launguage_Translation_Text ( LanguageID, Text, TranslatedText) values (5,'There are no Housekeeping work orders associated with this conference.','Aucun ordre de travail de ménage n''est associé à cette conférence.')
Insert into Launguage_Translation_Text ( LanguageID, Text, TranslatedText) values (5,'There are no menus associated with the selected Service Type for this location. Please select a new Service Type for this location OR click Cancel to return to the catering work orders summary page for this conference','Aucun menu n''est associé au type de service sélectionné pour cet endroit. Veuillez sélectionner un nouveau type de service pour cet endroit OU cliquez sur Annuler pour retourner sur la page récapitulative des ordres de travail pour le service de restauration de cette conférence.')
Insert into Launguage_Translation_Text ( LanguageID, Text, TranslatedText) values (5,'There are no Workorders associated with this conference','Aucun ordre de travail n''est associé à cette conférence')
Insert into Launguage_Translation_Text ( LanguageID, Text, TranslatedText) values (5,'This value has already been taken.','Cette valeur est déjà prise.')
Insert into Launguage_Translation_Text ( LanguageID, Text, TranslatedText) values (5,'This Week Conferences Total Usage Report','Rapport sur l''usage total des conférences de cette semaine')
Insert into Launguage_Translation_Text ( LanguageID, Text, TranslatedText) values (5,'Title','Titre')
Insert into Launguage_Translation_Text ( LanguageID, Text, TranslatedText) values (5,'To add a new work order, click the Add New Work Order button. To modify an existing work order, clic','Pour ajouter un nouvel ordre de travail, cliquez sur le bouton Ajouter un nouvel ordre de travail. Pour modifier un ordre de travail existant, cliquez sur le lien Modifier OU le lien Supprimer associé à cet ordre de travail.')
Insert into Launguage_Translation_Text ( LanguageID, Text, TranslatedText) values (5,'To add a new work order, click the button below.','Pour ajouter un nouvel ordre de travail, cliquez sur le bouton ci-dessous.')
Insert into Launguage_Translation_Text ( LanguageID, Text, TranslatedText) values (5,'To add a new work order, click the button below. OR click EDIT to edit an existing work order.','Pour ajouter un nouvel ordre de travail, cliquez sur le bouton ci-dessous. OU cliquez sur MODIFIER pour modifier un ordre de travail existant.')
Insert into Launguage_Translation_Text ( LanguageID, Text, TranslatedText) values (5,'To create a new work order, click on Add New Work Order button.','Pour créer un nouvel ordre de travail, cliquez sur le bouton Ajouter un nouvel ordre de travail.')
Insert into Launguage_Translation_Text ( LanguageID, Text, TranslatedText) values (5,'To create a new work order, click on Add New Work Order button. To modify an existing work order.','Pour créer un nouvel ordre de travail, cliquez sur le bouton Ajouter un nouvel ordre de travail. Pour modifier un ordre de travail existant, cliquez sur le lien Modifier OU le lien Supprimer associé à cet ordre de travail.')
Insert into Launguage_Translation_Text ( LanguageID, Text, TranslatedText) values (5,'To create a new work order, click the button below.','Pour créer un nouvel ordre de travail, cliquez sur le bouton ci-dessous.')
Insert into Launguage_Translation_Text ( LanguageID, Text, TranslatedText) values (5,'To create a new work order, please click on the button below.','Pour créer un nouvel ordre de travail, veuillez cliquer sur le bouton ci-dessous.')
Insert into Launguage_Translation_Text ( LanguageID, Text, TranslatedText) values (5,'Totals:','Totaux :')
Insert into Launguage_Translation_Text ( LanguageID, Text, TranslatedText) values (5,'Un Mute','Remettre le son')
Insert into Launguage_Translation_Text ( LanguageID, Text, TranslatedText) values (5,'UnBlock','Débloquer')
Insert into Launguage_Translation_Text ( LanguageID, Text, TranslatedText) values (5,'Undefined','Indéfini')
Insert into Launguage_Translation_Text ( LanguageID, Text, TranslatedText) values (5,'Undelete','Restaurer')
Insert into Launguage_Translation_Text ( LanguageID, Text, TranslatedText) values (5,'Update','Mettre à jour')
Insert into Launguage_Translation_Text ( LanguageID, Text, TranslatedText) values (5,'Usage By Room','Usage par salle')
Insert into Launguage_Translation_Text ( LanguageID, Text, TranslatedText) values (5,'User','Utilisateur')
Insert into Launguage_Translation_Text ( LanguageID, Text, TranslatedText) values (5,'Version Number','Numéro de version')
Insert into Launguage_Translation_Text ( LanguageID, Text, TranslatedText) values (5,'View Conference','Voir la conférence')
Insert into Launguage_Translation_Text ( LanguageID, Text, TranslatedText) values (5,'View Conflict','Voir le conflit')
Insert into Launguage_Translation_Text ( LanguageID, Text, TranslatedText) values (5,'View Menus','Voir les menus')
Insert into Launguage_Translation_Text ( LanguageID, Text, TranslatedText) values (5,'VRM Service is not installed','Le service VRM n''est pas installé')
Insert into Launguage_Translation_Text ( LanguageID, Text, TranslatedText) values (5,'VTC Calendar','Calendrier du VTC')
Insert into Launguage_Translation_Text ( LanguageID, Text, TranslatedText) values (5,'VTC Contact List','Liste de contact du VTC')
Insert into Launguage_Translation_Text ( LanguageID, Text, TranslatedText) values (5,'VTC Daily Schedule','Emploi du temps quotidien du VTC')
Insert into Launguage_Translation_Text ( LanguageID, Text, TranslatedText) values (5,'VTC Resource Allocation Report','Rapport d''allocation des ressources du VTC')
Insert into Launguage_Translation_Text ( LanguageID, Text, TranslatedText) values (5,'We are sorry that you will not be able to join the following conference','Nous sommes désolés que vous ne puissiez participer à la conférence suivante')
Insert into Launguage_Translation_Text ( LanguageID, Text, TranslatedText) values (5,'Workorders can only be created with rooms selected for this conference.','Les ordres de travail ne peuvent être créés qu''avec les salles sélectionnées pour cette conférence.')
Insert into Launguage_Translation_Text ( LanguageID, Text, TranslatedText) values (5,'Year To Date Conferences Total Usage Report','Rapport sur l''usage total des conférences de l''année en cours')
Insert into Launguage_Translation_Text ( LanguageID, Text, TranslatedText) values (5,'Yes','Oui')
Insert into Launguage_Translation_Text ( LanguageID, Text, TranslatedText) values (5,'Yesterday Conference Total Usage Report','Rapport sur l''usage total des conférences d''hier')
Insert into Launguage_Translation_Text ( LanguageID, Text, TranslatedText) values (5,'You have no Pending or Future Conferences.','Vous n''avez pas de conférences prévu ou à venir.')
Insert into Launguage_Translation_Text ( LanguageID, Text, TranslatedText) values (5,'No Items...','Aucun élément...')
Insert into Launguage_Translation_Text ( LanguageID, Text, TranslatedText) values (5,'previous page','page précédente')
Insert into Launguage_Translation_Text ( LanguageID, Text, TranslatedText) values (5,'Already on last page','Déjà sur la dernière page')
Insert into Launguage_Translation_Text ( LanguageID, Text, TranslatedText) values (5,'next page','page suivante')
Insert into Launguage_Translation_Text ( LanguageID, Text, TranslatedText) values (5,'Already on first page','Déjà sur la première page')
Insert into Launguage_Translation_Text ( LanguageID, Text, TranslatedText) values (5,'N/A','N/A')
Insert into Launguage_Translation_Text ( LanguageID, Text, TranslatedText) values (5,'Session expired','Session expirée')
Insert into Launguage_Translation_Text ( LanguageID, Text, TranslatedText) values (5,'VRM system is unavailable during the chosen date and time. Please check VRM system availability and ','Le système VRM n''est pas disponible à la date et à l''heure choisies. Veuillez vérifier la disponibilité du système VRM et essayer à nouveau. Heures d''opération :')
Insert into Launguage_Translation_Text ( LanguageID, Text, TranslatedText) values (5,'Closed','Fermé')
Insert into Launguage_Translation_Text ( LanguageID, Text, TranslatedText) values (5,'Any','N''importe')
Insert into Launguage_Translation_Text ( LanguageID, Text, TranslatedText) values (5,'Connecting','En cours de connexion')
Insert into Launguage_Translation_Text ( LanguageID, Text, TranslatedText) values (5,'Disconnecting','En cours de déconnexion')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Total Records:','Total des entrées :')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Please select a Room first from the drop down below.','Veuillez d''abord sélectionner une salle à partir du menu déroulant ci-dessous.')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'If there are no rooms in the list, click on ''Select Rooms'' option available on your left.','S''il n''y a pas de salles dans la liste, cliquez sur l''option « Sélectionner des salles » disponible sur la gauche.')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Click Cancel before you proceed to another step.','Cliquez sur Annuler avant de passer à l''étape suivante.')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Select a Set from the A/V Set list.','Sélectionnez un ensemble dans la liste des ensembles A/V.')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'If there are no items in the list, then there are no sets associated with this room.','S''il n''y a pas d''éléments dans la liste, aucun ensemble n''est associé à cette salle. ')
insert into Launguage_Translation_Text (LanguageID, [Text], TranslatedText) values (5,'Duration:','Durée :')

insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'for assistance.<br />Once settings are confirmed please click Preview tab and resubmit the conference.','pour recevoir de l''assistance.<br />Une fois que les réglages sont confirmés, veuillez cliquer sur l''onglet Aperçu et resoumettre la conférence.')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Please confirm settings below. If changes are required, please edit the fields accordingly OR contact technical support ','Veuillez confirmer les réglages ci-dessous. Si des changements sont requis, veuillez modifier les champs en conséquence OU contacter l''assistance technique ')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'There are no Audio/Visual work orders associated with this conference.<br />To create a new work order, click the button below.','Aucun ordre de travail audio/visuel n''est associé à cette conférence.<br /> Pour créer un nouvel ordre de travail, cliquez sur le bouton ci-dessous.')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Item','Élément')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,' is duplicated!',' est dupliqué(e) !')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Operation failed: Invalid security permissions on ','Échec de l''opération : Permissions de sécurité invalides sur ')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'You have successfully logged out of myVRM.','Vous êtes déconnecté avec succès de myVRM.')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Error 122: Please contact your VRM Administrator','Erreur 122 : Veuillez contacter votre administrateur VRM')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Restore User','Restaurer l''utilisateur')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Floor / room number','Numéro d''étage/de salle')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Address and Directions','Adresse et plan d''accès')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Parking directions','Plan d''accès du parking')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Map Link','Associer le lien')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Time Zone','Fuseau horaire')

insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Room Phone number','Numéro de téléphone de la salle')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Maximum capacity','Capacité maximale')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Setup time buffer','Configuration d''une durée tampon')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Teardown time buffer','Durée tampon du démontage')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Projector Available','Projecteur disponible')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Room images','Images de la salle')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Assistant-in-charge','Assistant responsible')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Map Images','Associer les images')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Security images','Images de sécurité')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Misc. Attachments','Divers Pièces jointes')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Approvers','Approbateurs')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Endpoints','Points terminaux')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Departments','Départements')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Top > Middle Tier','Tiers supérieur > intermédiaire')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Records per page','Entrées par page')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Max Allowed Video Rooms','Nombre max. de salles avec vidéo permis')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Max Allowed Non-Video Rooms','Nombre max. de salles sans vidéo permis')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Max Allowed MCU','Nombre max. de MCU permis')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Max Allowed Endpoints','Nombre max. de points terminaux permis')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Max Allowed Users','Nombre max. d''utilisateurs permis')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Max Allowed Exchange Users','Nombre max. d''utilisateurs Exchange permis')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Max Allowed Domino Users','Nombre max. d''utilisateurs Domino permis')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Max Allowed Mobile Users','Nombre max. d''utilisateurs Mobile permis')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Max Allowed Catering','Nombre max. de services de restauration permis')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Max Allowed HouseKeeping','Nombre max. de ménages permis')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Max Allowed APIs','Nombre max. d''API permis')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Max Allowed Facilities','Nombre max. d''installation permis')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Your Email has been blocked since ','Votre e-mail a été bloqué depuis ')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Organization Email has been blocked since ','L''email de l''organisation a été bloqué depuis ')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'. Please contact administrator for further assistance.','Veuillez contacter votre administrateur pour une assistance supplémentaire.')

insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Audio/Video','Audio/Vidéo')

insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Are you sure you want to delete this MCU?','Êtes-vous sûr de vouloir supprimer ce MCU ?')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Are you sure you want to delete this conference?','Êtes-vous sûr de vouloir supprimer cette conférence ?')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Are you sure you want to delete and Clone this conference?','Êtes-vous sûr de vouloir supprimer et cloner cette conférence ?')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Are you sure you want to delete this Workorder?','Êtes-vous sûr de vouloir supprimer cet ordre de travail ?')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Are you sure you want to delete this department?','Êtes-vous sûr de vouloir supprimer ce département ?')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Are you sure you want to delete this Menu?','Êtes-vous sûr de vouloir supprimer ce menu ?')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Are you sure you want to delete this Billing Code?','Êtes-vous sûr de vouloir supprimer ce Code de facturation ?')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Are you sure you want to delete this inventory?','Êtes-vous sûr de vouloir supprimer cet inventaire ?')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Are you sure you want to delete this Item?','Êtes-vous sûr de vouloir supprimer cet élément ?')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Are you sure you want to delete this user?','Êtes-vous sûr de vouloir supprimer cet utilisateur ?')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Are you sure you want to delete this endpoint?','Êtes-vous sûr de vouloir supprimer ce point terminal ?')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Are you sure you want to mute/unmute this endpoint?','Êtes-vous sûr de vouloir couper/remettre le son de ce point terminal?')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Are you sure you want to Connect/disconnect this endpoint?','Êtes-vous sûr de vouloir connecter/déconnecter ce point terminal ?')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Are you sure you want to delete this Option?','Êtes-vous sûr de vouloir supprimer cette option ?')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Are you sure you want to delete this Group?','Êtes-vous sûr de vouloir supprimer ce groupe ?')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Are you sure you want to delete this SP Guest?','Êtes-vous sûr de vouloir supprimer cet invité SP ?')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Are you sure you want to delete this organization profile?','Êtes-vous sûr de vouloir supprimer ce profil d''organisation ?')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Are you sure you want to delete this Template?','Êtes-vous sûr de vouloir supprimer ce modèle ?')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Are you sure you want to delete this user template?','Êtes-vous sûr de vouloir supprimer ce modèle d''utilisateur ?')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Are you sure you want to delete this middle tier?','Êtes-vous sûr de vouloir supprimer ce tiers intermédiaire ?')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Are you sure you want to delete this tier ?','Êtes-vous sûr de vouloir supprimer ce tiers ?')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Are you sure you want to ACTIVATE this user account ?','Êtes-vous sûr de vouloir ACTIVER ce compte utilisateur ?')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Are you sure you want to DELETE this user account. This user will not longer be available to restore ?','Êtes-vous sûr de vouloir SUPPRIMER ce compte utilisateur ? Il ne sera plus possible de restaurer cet utilisateur.?')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Are you sure you want to delete this Custom Option?','Êtes-vous sûr de vouloir supprimer cette Option Habituelle ?')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Are you sure you want to edit this Custom Option?','Êtes-vous sûr de vouloir modifier cette option personnalisée ?')

insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'To create a new work order, click the Add New Work Order button.','Pour créer un nouvel ordre de travail, cliquer sur le bouton Ajouter un nouvel ordre de travail.')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Last','Dernier')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'First','Premier')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Prev','Précédent')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Next','Suivant')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Activate','Activer')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Deactivate','Désactiver')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Please select MCU for user:','Veuillez sélectionner un MCU pour l''utilisateur :')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Selected bridge for user ','Le pont sélectionné pour l''utilisateur ')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,' does not support MPI calls.',' ne prend pas en charge les appels MPI.')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'An audio or video conference cannot be created with room ','Une conférence audio ou vidéo ne peut pas être créée pour cette salle ')

insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,' since there is no endpoint associated with it',' puisque qu''aucun point terminal n''y est associé')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Please select a Profile for room: ','Veuillez sélectionner un profil pour la salle : ')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Edit All','Tout modifier')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'No conflict','Aucun conflit')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Catering Facility','installation du service de restauration')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Endpoint','Point terminal')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'No SP Guest found','Aucun invité SP n''a été trouvé')

insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Create New Work Order','Créer un nouvel ordre de travail')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Rejected','Refusé')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Accepted','Accepté')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Undecided','Sans décision')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Request for Reschedule','Demande de replanification')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Page','Page')

insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Unreachable','Injoignable')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Online','En ligne')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Disconnect','Déconnecter')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Connected','Connecté')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Total Endpoints:','Total des points terminaux :')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Last Updated:','Dernière mise à jour :	')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'hour(s)','heure(s)')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'min(s)','minute(s)')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Delete','Effacer')


insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Catering Provider','Fournisseurs de service de restauration')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Entity Codes displayed in','Codes d''entité affichés dans')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Manage Day Color','Gestion de la couleur de jour')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Invalid Date.','Date invalide.')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Activated','Activé')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Deactivated','Désactivé')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Manage','Gérer')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'A/V Workorders','Ordres de travail A/V')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Bulk Tool Facility','Installation des divers outils')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Customize Organization Emails','Personnaliser les e-mails de l''organisation')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Cutomize my Emails','Personnaliser mes emails')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Dashboard','Tableau de bord')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Express form','Formulaire court')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'LDAP Directory Import','Importation du répertoire LDAP')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Manage Active Users','Gérer les utilisateurs actifs')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Manage Catering items','Gérer les éléments du service de restauration')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Manage Email Queue','Gérer la file d''attente des e-mails')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Manage Endpoints','Gérer les points terminaux')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Manage Groups','Gérer les groupes')

insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Manage Housekeeping items','Gérer les éléments du ménage')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Manage Inventory Sets','Gérer les jeux d''inventaire')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Manage MCU','Gérer le MCU')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Manage my Lobby UI','Gérer l''IU de l''accueil')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Manage Organization Custom Options','Gérer les options personnalisées de l''organisation')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Manage Organization Set-up','Gérer la configuration de l''organisation')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Manage Organization UI Text','Gérer le texte de l''IU de l''organisation')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Manage Roles','Gérer les rôles')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Manage Rooms','Gérer les salles')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Manage Templates','Gérer les modèles')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'My Advance Reports','Mes rapports de progrès')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'My Default Search','Ma recherche par défaut')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'My Organization Options','Mes options d''organisation')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'My Organization UI','Mon IU d''organisation')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'My Reports','Mes rapports')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'My Settings','Mes réglages')

insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Restore Users','Restaurer les utilisateurs')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Settings','Réglages')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Site Settings','Réglages du site')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Systems Diagnostics','Diagnostics du système')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'View Approval Pending','Voir les approbations en attente')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'View Catering Workorder','Voir l''ordre de travail du service de restauration')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'View Housekeeping workorders','Voir les ordres de travail de ménage')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'View My Reservations','Voir mes réservations')

insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'View Ongoing','Afficher les tâches en cours')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'View Pending','Voir les tâches prévus')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'View Personal Calendar','Voir le calendrier personnel')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'View Public Conferences','Voir les conférences publiques')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'View Room Calendar','Voir le calendrier des salles')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'View Scheduled Calls','Voir les appels planifiés')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Updated for Spanish','Mise à jour pour l''espagnol')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'% usage (Avail. Hrs)','% usage (heures. disp.)')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'(Applicable for all instances)','(Valable pour toutes les instances)')

insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'account expiry','expiration du compte')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Active','Actif')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Actual','Actuel')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Additional Information','Autres informations')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'address','adresse')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'address type','type d''adresse')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'admin','admin')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Aggregate Usage','Usage total')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'ago','ago')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'All','Tout')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'and','et')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Approval','Approbation')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'April','Avril')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'are invalid characters.','sont des caractères invalides.')

insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'are linked to the selected entity option and data will be lost.','sont liés à l''option d''entité sélectionnée et les données seront perdues.')

insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Are you sure you want to BLOCK this user account?','Êtes-vous sûr de vouloir BLOQUER ce compte utilisateur ?')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Are you sure you want to delete this Day Color?','Êtes-vous sûr de vouloir supprimer cette couleur de jour ?')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Are you sure you want to delete this Entity Code?','Êtes-vous sûr de vouloir supprimer ce Code d''entité ?')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Are you sure you want to DELETE this user account?','Êtes-vous sûr de vouloir SUPPRIMER ce compte utilisateur ?')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Are you sure you want to edit this Entity Code?','Êtes-vous sûr de vouloir modifier ce Code d''entité ?')

insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Are you sure you want to UNBLOCK this user account?','Êtes-vous sûr de vouloir DÉBLOQUER ce compte utilisateur ?')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Are you sure you want to UNDELETE this user account?','Êtes-vous sûr de vouloir RESTAURER ce compte utilisateur ?')

insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'At least one Inventory item should NOT be marked for deletion.','Au moins un élément d''inventaire NE devrait PAS être sélectionné pour la suppression.')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Attachment 1 is greater than 10MB. File has not been uploaded.','La pièce jointe 1 excède 10 Mo. Le fichier n''a pas été mis en ligne.')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Attachment 2 has already been uploaded.','La pièce jointe 2 a déjà été mise en ligne.')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Attachment 3 has already been uploaded.','La pièce jointe 3 a déjà été mise en ligne.')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Attachment 3 is greater than 10MB. File has not been uploaded.','La pièce jointe 3 excède 10 Mo. Le fichier n''a pas été mis en ligne.')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Attachments','Pièces jointes')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Audio Info','Info Audio')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Audio-Only','Audio seulement')

insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'August','Août')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'available hrs','h disponibles')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Bill to host','Facturer l''hôte')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Bill to individual','Facturer l''individuel')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Blocked ','Bloqué  ')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Both','Ensemble')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Breakfast','Petit-déjeuner')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'bridge name','nom de pont')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Browser Agent','Client de navigation')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Browser Language','Langue du navigateur')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Catering Admin','Admin du service de restauration')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Catering Remainder','Restes du service de restauration')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'cell #','cellule n°')

insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Choose Add Audio Bridge button and select the desired audio bridge','Choisissez le bouton Ajouter un pont Audio et sélectionner le pont audio souhaité')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Click to show room resources','Cliquez pour afficher les ressources de la salle')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Coffee Service','Service de café')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Comment','Commentaire')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Completed Conference(s)','Conférence(s) terminée(s)')

insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'concierge support search','recherche de l''assistance du concierge')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'conf #','N° de Conf.')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Conf Datetime','Dateheure de conf')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Conference','Conférence')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'conference date','Date de conférence')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Conference Name','Nom de conférence')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Conference Report','Rapport de la conférence')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Conference Room B','Salle de conférence B')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Conference Status','État de la conférence')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Conference Type','Type de conférence')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Conferences','Conférences')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Conferences Usage Report','Rapport sur l''usage des conférences')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Connection Details','Détails de la connexion')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'contact name','nom du contact')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'contact no','n° du contact')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Create New Color Day','Créer un nouveau jour en couleur')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Currency Format','Format de la devise')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Current Status','Statut Actuel')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'CurrentIsdnCost','CoûtlsdnActuel')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Custom','Personnalisé')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Custom Date','Date personnalisé')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Custom Options','Options personnalisées')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Customized','Personnalisé')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Daily','Quotidien')

insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Data Table has been generated successfully!','Le tableau de données a été généré avec succès!')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'date','date')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'day','jour')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Debug','Déboguer')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'December','Décembre')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'dedicated VNOC operator','opérateur VNOC dédié')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'default profile','profil par défaut')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'deleted conference','conférence supprimée')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Delivery and Pickup','Livraison et récupération')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Delivery Cost','Coût de livraison')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Delivery Datetime','Dateheure de livraison')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Delivery Only','Livraison uniquement')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Delivery Type','Type de livraison')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Equipment pickup','récupération des équipements')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Description','Description')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'dialing option','option pour composer')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Dinner','Dîner')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Disabled','Désactiver ')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Drag a column header here to group by that column','Faites glisser un en-tête de colonne ici pour regrouper en fonction de cette colonne')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Duration','Durée')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'duration(min)','durée (min)')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Edit Audio Bridge','Modifier le pont audio')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Edit Color Day Details','Modifier les détails du jour en couleur')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'end','fin')

insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'End_by Datetime','Dateheure Fin')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'endpoint name','Nom du point terminal')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Endpoint Reports','Rapports du point terminal')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Endpoint Selection','Sélection du point terminal')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'entity code','code de l''entité')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Express Conference','Conférence courte')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'February','Février')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Feedback','Commentaire')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'File attachment is greater than 100kB. File has not been uploaded.','La pièce jointe excède les 100 Mo. Le fichier n''a pas été mis en ligne.')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'firmware version','version de firmware')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Forgot Password','Mot de passe oublié')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'fourth','quatrième')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Friday','Vendredi')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Future','à venir')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Future Conference(s)','Conférence(s) à venir')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Gold','Gold')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Halal','Halal')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'High Resolution Banner exceeds the Resolution Limit','La haute résolution de la bannière dépasse la limite de résolution ')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'HK Admin','Admin HK')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'HK Remainder','HK restant')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'hours per working day','heures de travail par jour')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Ical_Emails','Ical_Emails')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Inactive ','Inactif ')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Information','Informations')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'instances','instances')

insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Invalid Conference Service ID','Indentification de service de la conférence invalide')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Invalid IP Address for user','Adresse IP de l''utilisateur invalide')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Invalid ISDN Address for user','Adresse ISDN de l''utilisateur invalide')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Invalid Time (HH:mm)','Heure invalide (HH:mm)')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Inventory Admin','Admin de l''inventaire')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Inventory Remainder','Reste de l''inventaire')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Inventory Set Used','Ensemble Inventaire Utilisé')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'IsdnThresholdCost','IsdnCoûtseuil')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'January','Janvier')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'July','Juillet')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'June','Juin')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Kosher','Casher')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Last Month','Le mois dernier')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Last Week','La semaine dernière')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Lobby Management','Gestion de l''accueil')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Location','Emplacement')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Location Info','Info sur l''emplacement')

insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Location(s)','Emplacement (s)')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Location(s) Report','Rapport sur le(s) emplacement(s)')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Locations','Emplacements')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Locations(Country/State) Report','Rapport sur les emplacements (Pays/État)')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Locations(Zip Code) Report','Rapport sur les emplacements (Code postal)')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Login','Identifiant')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Low-Cal.','Basse-Cal.')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Lunch','Déjeuner')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Mail Logo','Logo de la messagerie')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Mail Server Test Connection','Connexion de test du serveur de messagerie')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Maintenance','Entretien')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Manage Conference','Gestion de la conférence')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Manage Email Domains','Gestion des domaines e-mail')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'March','Mars')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Max Allowed Notes Users','Utilisateurs Notes max. autorisés')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Max Allowed Outlook Users','Utilisateurs Outlook max. autorisés')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Max Allowed PC','PC max. autorisés')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'May','Mai')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'MCU Admin','Admin du MCU')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'MCU Alert To Host','Alerte du MCU à l''hôte')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'MCU Alert To MCU Admin','Alerte du MCU à l''admin du MCU')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'MCU Approval Request','Demande d''approbation du MCU')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'MCU Details','Détails du MCU')

insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'MCU Info','Info sur le MCU')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'MCU Names','Noms de MCU')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'MCU Reports','Rapports du MCU')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'MCU Selection','Sélection du MCU')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'mcu usage','usage du MCU')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'MCU(All) Report','Rapport des MCU')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'media','media')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'meet and greet','accueil')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'meeting tilte','intitulé de la réunion')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Meeting Type','Type de réunion')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'middle tier','niveau intermédiaire')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'min','min')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'model','modèle')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Monday','Lundi')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Month(s)','Mois')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Monthly','Mensuel')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Monthly Room Usage','Utilisation mensuelle de la salle')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Name','Nom')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'No Endpoint(s) associated with room: ','Aucun point terminal associé à la salle : ')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'No Endpoint(s) associated with this room','Aucun point terminal n''est associé à cette salle')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'No Endpoints selected','Aucun point terminal sélectionné')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'No Records','Aucune entrées')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'No room layouts defined','Aucun plan de salle défini')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'None...','Rien...')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Note: to add an audio bridge to your conference, 1) complete the information on this page, 2) under the Select Participants tab, ','Remarque : pour ajouter un pont audio à votre conférence, 1) remplissez les informations de cette page, 2) sous l''onglet Sélectionner les participants')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Note: to complete request to add an audio bridge to your conference, ','Remarque : pour terminer la demande, ajoutez un pont audio à votre conférence. ')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Note: to complete request to add an audio bridge to your conference, choose Add Audio Add-On Bridge button and select the desired audio bridge','Remarque : pour terminer la demande, ajoutez un pont audio à votre conférence, choisissez le bouton Ajouter audio Ajouter pont audio et sélectionnez celui souhaité')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'November','Novembre')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'October','Octobre')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'office phone #','N° téléphone bureau')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Ongoing conference','Conférence en cours')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Operation Successful! Please switch to respective organization to see the changes.','Opération réalisée avec succès! Veuillez passer à l''organisation concernée pour visualiser les modifications.')

insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Organization Reports','Rapports sur l''organisation')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Participant','Participant')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Participant Acceptance Email','E-mail d''acceptation du participant')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Participant Remainder','Reste des participants')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Participant Request Change','Changement de la demande du participant')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Password','Mot de passe')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Past Conference(s)','Conférence(s) passée(s)')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Pending conference','Conférence en suspens')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Pending Status','État prévu')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'percentage of total audio conferences','pourcentage de l''ensemble des conférences audio')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'percentage of total audio-video conferences','pourcentage de l''ensemble des conférences audio-vidéo')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'percentage of total point-to-point conferences','pourcentage de l''ensemble des conférences point à point')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'percentage of total room conferences','pourcentage de l''ensemble des salles de conférence')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'percentage of usage','pourcentage de l''usage')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Person_in_charge','Responsable')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Personal report','Rapport personnel')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Pick up only','Récupération uniquement')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Platinum','Platinum')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Please add option Name to Language English.','Veuillez ajouter le nom de l''option à Langue anglaise.')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Please contact administrator for further assistance.','Veuillez contacter votre administrateur pour une assistance supplémentaire.')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Please select one...','Veuillez sélectionner un...')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Please set the values for the mandatory custom options.','Veuillez définir les valeurs des options personnalisées obligatoires.')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Please Upload the File','Veuillez mettre en ligne le fichier')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Point to Point','Point à point')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Preferred Login ID','Identifiant de session privilégié')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'PRI','PRI')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'primary approver','Approbateur principal')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Private','Privé(e)')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'protocol','protocole')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Public','Publique')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'public conference','conférence publique')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Recipient','Destinataire')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Recurrence Pattern','Mode récurrence')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Recurring Hearing','Session récurrente')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'remaining','restante')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'remarks','remarques')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Report name is already exists.','Le nom de rapport existe déjà.')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Request Account','Demande de compte')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Requested Datetime','Dateheure requise')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Requested Duration','Durée sollicitée')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Requested Item','Élément requis')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Required','Requis')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Reservation conference','Réservation de conférence ')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'role','rôle')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Room Approval Request','Demande d''approbation de la salle')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Room In-charge','Responsable de la salle')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'room name','nom de la salle')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Room Only','Salle uniquement')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'room phone #','N° de téléphone de la salle')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Room Reports','Rapports de la salle')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Room Selection','Sélection de la salle')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'room type','type de la salle')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'room usage','utilisation de salle')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'ROOM USAGE BY SITE','UTILISATION DE LA SALLE PAR SITE')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Rooms Assigned','Salles attribuées')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Saturday','Samedi')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Saved Successfully.','Enregistrement réussi.')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Scheduled Conference(s)','Conférence(s) programmée(s)')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Scheduler','Planificateur')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'second','deuxième')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Security Desk Only','Bureau de la sécurité uniquement')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'September','Septembre')

insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Service Charge','Frais de service')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Service Type','Type de service')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Setup','Configuration')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Silver','Silver')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Site Logo  exceeds the Resolution Limit','La résolution du logo du site dépasse la limite')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Site Logo attachment is greater than 10MB. File has not been uploaded.','La taille de la pièce jointe du logo du site est supérieure à 10 Mo. Le fichier n''a pas été mis en ligne.')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Site Reports','Rapports sur le site')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Snack','Collation')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'special instructions','instructions spéciales')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Standard Resolution Banner attachment is greater than 100kB. File has not been uploaded.','La pièce jointe avec la bannière de résolution standard dépasse 100 ko. Le fichier n''a pas été mis en ligne.')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Standard Resolution Banner exceeds the Resolution Limit','La résolution de bannière standard dépasse la limite')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Standard Resolution Banner exceeds the Resolution Limit','La résolution de bannière standard dépasse la limite ')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'start','début')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Start - End','Début - Fin')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Start Datetime','Dateheure de début')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Start_by Datetime','Dateheure de début')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Sunday','Dimanche')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Survey Email','e-mail pour le sondage')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Survey URL','URL de l''enquête')
/*==========================================================================================*/
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'System','Système')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'System Approval Request','Demande d''approbation du système')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'System Error.Please contact your VRM Administrator and supply this error code.','Erreur système. Veuillez contacter votre administrateur VRM et communiquez-lui ce code d''erreur.')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Teardown','Démontage')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Tech Contact ','Contact du technicien ')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Tech Email','E-mail du technicien')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Tech Phone','Téléphone du technicien')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'terminated conference','conférence terminée')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Test Mail Connection','Tester la connexion à la messagerie')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Thank you for the feedback.','Merci pour les commentaires.')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'There are no Audio/Visual work orders associated with the selected Room','Aucun ordre de travail audiovisuel n''est associé à la Salle sélectionnée')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'There are no Catering work orders associated with the selected Room','Aucun ordre de travail de service de restauration n''est associé à la Salle sélectionnée')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'There are no Housekeeping work orders associated with the Selected Room','Aucun ordre de travail d''Entretien n''est associé à la Salle sélectionnée')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'There are no user templates available. Please create at least one user template and proceed with LDAP Import','Aucun modèle utilisateur n''est disponible. Veuillez créer au moins un modèle utilisateur et procéder à l''importation LDAP')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'There is no location for this conference','Aucun emplacement n''est disponible pour cette conférence')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'There were too many matches in your search,please narrow your search','Il y a trop de résultats dans votre recherche, veuillez la préciser')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'third','troisième')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'This Week','Cette Semaine')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Thursday','Jeudi')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'tier1','niveaux 1')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'tier2','niveaux 2')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Time Remaining','Temps restant')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'to','à')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'To be completed','À terminer')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'to Week','à la semaine')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'top tier','niveau supérieur')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Top Tier(s) are imported successfully!','Le(s) Niveau supérieurs ont été importés avec succès!')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Total','Total')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Total Audio Conferences','Total des conférences audio')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Total Audio-Video Conferences','Total des conférences Audio-Vidéo')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Total Conferences','Total des conférences')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Total Cost','Coût total')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Total Duration(Hours)','Durée totale (en heures)')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'total hrs','nombre total d''heures')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Total Minutes','Total des minutes')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Total Participants','Total des participants')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Total Point-to-Point Conferences','Total des conférences point à point')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Total Room Conferences','Total des salles de conférence')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Total(Week ','Total des semaines  ')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Tuesday','Mardi')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'type','taper')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Type instructions here','Tapez les instructions ici')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Update Search Parameters for Template:','Actualiser les paramètres de recherches du Modèle :')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Update Search Parameters for Template: ','Actualiser les paramètres de recherches du Modèle : ')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Usage By Room - ','Usage par salle ')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'User only','Utilisateurs uniquement')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'User Selection','Sélection d''un utilisateur')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Users Datetime','Dateheure de l''utilisateur')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Users IP Address','Adresse IP de l''utilisateur')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Vegetarian','Végétarien')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'video conferences','Conférences vidéo')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'View Inventory workorders','Voir les ordres de travail de l''inventaire')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'View Public','Voir le public')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'View Reservations','Voir les réservations')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Virtual','Virtuel')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Warning','Avertissement')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Warning: This is an IRREVERSIBLE process. Once purged the data cannot be recovered.Are you sure you want to continue ?','Avertissement : Il s''agit d''un processus IRRÉVERSIBLE. Une fois éliminées, les informations ne peuvent pas être restaurées. Êtes-vous sûr de vouloir continuer ?')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Web Page','Page Web')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Website URL','URL du site Web')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Wednesday','Mercredi')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Week ','Semaine ')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'weekday','jour de la semaine')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'weekend','fin de semaine')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Weekly','Hebdomadaire')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Weekly Room Usage','Usage hebdomadaire de la salle')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Work Order Name','Nom de l''ordre de travail')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'work orders','ordres de travail')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Work.Dys','Jours.Travail')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Workorder Comment','Commentaire sur l''ordre de travail')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Year to Date','Année en cours')

insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Yearly','Annuellement')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Yesterday','Hier')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'You must UNBLOCK this user before editing their profile.','Vous devez DÉBLOQUER cet utilisateur avant de modifier son profil.')
Insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Invalid IP Address:','Adresse IP invalide:')
-- V2.8
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Protocol does not match with selected Address Type.','Le protocole est incompatible avec le type d''adresse sélectionné')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Invalid Address.','Adresse invalide')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'validation','validation')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'CancelEndpoint','Annuler point terminal')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Submit Endpoint ','Soumettre point terminal')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Call Monitor','Appeler le Moniteur')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Call Monitor (P2P)','Appel du moniteur (P2P)')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'View VNOC Conference','Afficher la conférence VNOC')

insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'English','Anglais')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'System Approver','Approbateurs du système')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Send Reminder','Envoyer des rappels')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Total MCUs','Total des MCU')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Send Survey','Envoyer l''enquête')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'are invalid characters','sont des caractères invalides')


insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'invalid','invalide')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'MCU pre end time is not allowed more than 15 mins','La durée de pré-arrêt du MCU ne doit pas excéder 15 min')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'MCU Pre End by','Pré-arrêt du MCU par')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'MCU pre start time is not allowed more than 15 mins','La durée du prédémarrage du MCU ne doit pas excéder 15 min')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'MCU Pre Start by','Prédémarrage du MCU par')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Tear-Down Time','Durée du démontage')


insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Conference duration range 15 to 1440 minutes.','Durée de la conférence comprise entre 15 et 1440 minutes.')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Default Conference Duration','Durée de la conférence par défaut')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Enable Smart Point-to-Point','Activer le point à point intelligent')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Send MCU Alert Mail','Envoyer un message d''alerte au MCU')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Default LineRate','VitessedeLigne par défaut')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'External','Externe')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Conference Summary','Récapitulatif de la conférence')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'The selected time is already defined for other message','L''horaire sélectionné est déjà défini pour un autre message')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Action','Action')



insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Audio Settings','Réglages audio')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Add Audio Bridge','Ajouter un pont Audio')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'instances/occurrences in the recurring series','instances/événements dans les séries récurrentes')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Note: Maximum limit of','Remarque : limite maximale de')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'of','de')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'hours including buffer period','heures y compris la période tampon')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Manual','Manuel')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Automatic','Automatique')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Start Mode','Mode de démarrage')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'This conference type is being changed to point-to-point and will not use an MCU connection','Ce type de conférence est modifié pour être en point à point et ne se servira pas de la connexion MCU')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'day(s)','jour(s)')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Please click Create/Edit or Cancel prior to clicking Next','Veuillez cliquer sur Créer/Modifier ou Annuler avant de cliquer sur Suivant')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Less','Inférieur')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Date/Time','Date/Heure')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Work Orders Management','Gestion des Ordres de Travail')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'UNSUCCESS','ÉCHEC')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'SUCCESS','RÉUSSITE')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Join','Rejoindre')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Recurring Conference','Conférence récurrente')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Conf Mode','Mode de Conf.')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Silo Name','Nom du Silo')





Insert into Launguage_Translation_Text(LanguageID,Text,TranslatedText) values(5,'Meeting Title','Intitulé de la réunion')
Insert into Launguage_Translation_Text(LanguageID,Text,TranslatedText) values(5,'No Data to display','Aucune donnée à afficher')
insert into Launguage_Translation_Text(LanguageID,Text,TranslatedText) values(5,'Message','Message')
insert into Launguage_Translation_Text(LanguageID,Text,TranslatedText) values(5,'Duration (Min)','Durée (min)')
insert into Launguage_Translation_Text(LanguageID,Text,TranslatedText) values(5,'Call Detail Records','Appeler les enregistrements détaillés')
insert into Launguage_Translation_Text(LanguageID,Text,TranslatedText) values(5,'Requestor','Demandeur')  
insert into Launguage_Translation_Text(LanguageID,Text,TranslatedText) values(5,'VNOC operator','Opérateur VNOC')
insert into Launguage_Translation_Text(LanguageID,Text,TranslatedText) values(5,'Call URI','Appeler l''URI')
insert into Launguage_Translation_Text(LanguageID,Text,TranslatedText) values(5,'Hours','heures')
insert into Launguage_Translation_Text(LanguageID,Text,TranslatedText) values(5,'Minutes','minutes')
insert into Launguage_Translation_Text(LanguageID,Text,TranslatedText) values(5,'Connects','Connexions')
insert into Launguage_Translation_Text(LanguageID,Text,TranslatedText) values(5,'Max Allowed Guest Rooms','Nombre max. de salles d''invités permis')
insert into Launguage_Translation_Text(LanguageID,Text,TranslatedText) values(5,'More','plus')
insert into Launguage_Translation_Text(LanguageID,Text,TranslatedText) values(5,'User Reports','Rapports sur les utilisateurs')
insert into Launguage_Translation_Text(LanguageID,Text,TranslatedText) values(5,'Callee','Appelé')
insert into Launguage_Translation_Text(LanguageID,Text,TranslatedText) values(5,'Caller','Appelant')
insert into Launguage_Translation_Text(LanguageID,Text,TranslatedText) values(5,'Select','Sélectionner')
insert into Launguage_Translation_Text(LanguageID,Text,TranslatedText) values(5,'Timezone','Fuseau horaire')
insert into Launguage_Translation_Text(LanguageID, Text, TranslatedText) values (5,'Unique ID','Identifiant unique')

insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Status','État')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Conference ID','Identification de la conférence')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Task Type <br/> Trans Id','Type de tâche <br/> Id de Trans')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Customer Id','Identifiant du client')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Issued By <br/> Issued On','Publié par <br/> Publié le')

insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Invalid account expiration date.','Date d''expiration du compte invalide')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Invalid account expiration date. Please check the license.','Date d''expiration du compte invalide. Veuillez vérifier la licence. ')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Please enter a valid Duration.','Veuillez saisir une durée valide.')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Error Event Report','Rapport sur les instances d''erreur')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Event Failure Mail','E-mail sur l''échec de l''évènement')

insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'AVOnsiteSupportEmail','AssistanceEmailAVSursite')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'User Report Email','E-mail du rapport utilisateur')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'MCU ISDN Threshold alert','Alerte du seuil ISDN du MCU')

insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Room/Attendee','Salle/Participant')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Email','e-mail')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'First Name','prénom')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Last Name','nom')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Requestor Name','Nom du demandeur')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Personal Audio Info','Infos Audio Personnelles')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Accept/Decline','Accepter/refuser')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Post Conf Scheduling','Publier la planification des conférences')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'User Name','Nom d''utilisateur')

insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Dial-in to MCU','Numéro du MCU à composer')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Dial-out from MCU','Numéroter à partir du MCU')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Direct to MCU','Directement vers le MCU')

insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'General User','Utilisateur général')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Organization Administrator','Administrateur de l''organisation')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Site Administrator','Administrateur du site')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Catering Administrator','Administrateur du service de restauration')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Inventory Administrator','Administrateur de l''inventaire')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Housekeeping Administrator','Administrateur du ménage')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Express Profile','Profil court')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Express Profile Manage','Gestion du profil court')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Express Profile Advanced','Profil court avancé')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'View-Only','Affichage uniquement')


insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Host Name','Nom de l''hôte')

insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'OS Manufacturer','vendeur du SE')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'OS Configuration','configuration du SE')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'OS Build Type','version du SE')

insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Registered Owner','propriétaire inscrit')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Registered Organization','organisation enregistrée')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Product ID','Identifiant du produit')

insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Original Install Date','Date d''installation originelle')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'System Manufacturer','fabricant du système')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'System Model','modèle du système')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'System Type','Type du système')

insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Processor(s)','processeur(s)')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'BIOS Version','version du BIOS')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Windows Directory','Répertoire Windows')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'System Directory','répertoire du système')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Boot Device','dispositif de démarrage')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'System Locale','Paramètres régionaux du système')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Input Locale','Entrer les paramètres régionaux')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Total Physical Memory','Total des mémoire physique')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Page File Location(s)','Emplacements des fichiers de page')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Domain','Domaine')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Logon Server','Serveur de l''ouverture de session')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Network Card(s)','Cartes réseau')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'IP address(es)','Adresse IP')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'World Wide Time Zones','Fuseaux horaires universels')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Hotfix(s)','Correctif(s)')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'U.S Time Zones','Fuseaux horaires des Ëtats-Unis')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Please associate at least one Menu Item with all Menus.','Veuillez associer au moins un élément de menu à tous les menus.')
insert into Launguage_Translation_Text(LanguageID,Text,TranslatedText) values(5,'Computer Name','Nom de l''ordinateur')
insert into Launguage_Translation_Text(LanguageID,Text,TranslatedText) values(5,'Housekeeping Group','Groupes de Ménage')
insert into Launguage_Translation_Text(LanguageID,Text,TranslatedText) values(5,'OS Name','Nom du SE')
insert into Launguage_Translation_Text(LanguageID,Text,TranslatedText) values(5,'OS Version','Version du SE')
insert into Launguage_Translation_Text(LanguageID,Text,TranslatedText) values(5,'.Net Version','Version .NET')
insert into Launguage_Translation_Text(LanguageID,Text,TranslatedText) values(5,'Computer Manufacturer','Fabricant de l''ordinateur')
insert into Launguage_Translation_Text(LanguageID,Text,TranslatedText) values(5,'Computer Model','Modèle d''ordinateur')
insert into Launguage_Translation_Text(LanguageID,Text,TranslatedText) values(5,'Not Specified','Non spécifié')
insert into Launguage_Translation_Text(LanguageID,Text,TranslatedText) values(5,'Off','Désactivé(e)')
insert into Launguage_Translation_Text(LanguageID,Text,TranslatedText) values(5,'1 day','1 jour')
insert into Launguage_Translation_Text(LanguageID,Text,TranslatedText) values(5,'3 days','3 jours')
insert into Launguage_Translation_Text(LanguageID,Text,TranslatedText) values(5,'7 days','7 jours')
insert into Launguage_Translation_Text(LanguageID,Text,TranslatedText) values(5,'Sytem Errors','Erreurs système')
insert into Launguage_Translation_Text(LanguageID,Text,TranslatedText) values(5,'System and User Errors','Erreurs système et utilisateur')
insert into Launguage_Translation_Text(LanguageID,Text,TranslatedText) values(5,'Warnings (and all above)','Avertissements (et tout ce qui précède)')
insert into Launguage_Translation_Text(LanguageID,Text,TranslatedText) values(5,'Information (and all above)','Informations (et tout ce qui précède)')
insert into Launguage_Translation_Text(LanguageID,Text,TranslatedText) values(5,'Debug (and all above)','Débogage (et tout ce qui précède)')
insert into Launguage_Translation_Text(LanguageID,Text,TranslatedText) values(5,'Audio-Only Conference','Conférence audio uniquement')

-- ZD 100288                                  

insert into Launguage_Translation_Text(LanguageID,Text,TranslatedText) values(5,'Ongoing Conferences','Conférences en cours')
insert into Launguage_Translation_Text(LanguageID,Text,TranslatedText) values(5,'Reservations','Réservations')
insert into Launguage_Translation_Text(LanguageID,Text,TranslatedText) values(5,'Public Conferences','Conférences publique')
insert into Launguage_Translation_Text(LanguageID,Text,TranslatedText) values(5,'Pending Conferences','Conférences prévues')
insert into Launguage_Translation_Text(LanguageID,Text,TranslatedText) values(5,'Approval Pending','En attente d''approbation')

insert into Launguage_Translation_Text(LanguageID,Text,TranslatedText) values(5,'Bulk User Management','Gestion en bloc des utilisateurs')
insert into Launguage_Translation_Text(LanguageID,Text,TranslatedText) values(5,'Audiovisual Work Orders','Ordres de travail audiovisuel')
insert into Launguage_Translation_Text(LanguageID,Text,TranslatedText) values(5,'Facility Services Work Orders','Ordres de travail des services de l''installation')
insert into Launguage_Translation_Text(LanguageID,Text,TranslatedText) values(5,'Audiovisual Inventories','Inventaires audiovisuels')
insert into Launguage_Translation_Text(LanguageID,Text,TranslatedText) values(5,'Catering Menus','Menus du service de restauration')
insert into Launguage_Translation_Text(LanguageID,Text,TranslatedText) values(5,'Facility Services','Services de l''installation')
insert into Launguage_Translation_Text(LanguageID,Text,TranslatedText) values(5,'New Conference','Nouvelle conférence')
insert into Launguage_Translation_Text(LanguageID,Text,TranslatedText) values(5,'Conference Dashboard','Tableau de bord de la conférence')
insert into Launguage_Translation_Text(LanguageID,Text,TranslatedText) values(5,'Customize My Email','Personnaliser mes e-mails')
insert into Launguage_Translation_Text(LanguageID,Text,TranslatedText) values(5,'Diagnostics','Diagnostic')
insert into Launguage_Translation_Text(LanguageID,Text,TranslatedText) values(5,'Express New Conference','Nouvelle conférence urgente')
insert into Launguage_Translation_Text(LanguageID,Text,TranslatedText) values(5,'Reports','Rapports')
insert into Launguage_Translation_Text(LanguageID,Text,TranslatedText) values(5,'Organization Options','Options de l''organisation')
insert into Launguage_Translation_Text(LanguageID,Text,TranslatedText) values(5,'Manage Blocked Email','Gérer les e-mails bloqués')
insert into Launguage_Translation_Text(LanguageID,Text,TranslatedText) values(5,'Manage Organizations','Gérer les organisations')

insert into Launguage_Translation_Text(LanguageID,Text,TranslatedText) values(5,'Manage User Roles','Gérer les rôles d''utilisateurs')
insert into Launguage_Translation_Text(LanguageID,Text,TranslatedText) values(5,'Organization Custom Options','Options personnalisées de l''organisation')
insert into Launguage_Translation_Text(LanguageID,Text,TranslatedText) values(5,'Organization Settings','Paramètres de l''organisation')
insert into Launguage_Translation_Text(LanguageID,Text,TranslatedText) values(5,'Personal Calendar','Calendrier personnel')
insert into Launguage_Translation_Text(LanguageID,Text,TranslatedText) values(5,'Room Calendar','Calendrier des salles')
insert into Launguage_Translation_Text(LanguageID,Text,TranslatedText) values(5,'GuestLocations','EmplacementsInvités')
insert into Launguage_Translation_Text(LanguageID,Text,TranslatedText) values(5,'Automatic Call Launch','Lancement automatic d''appel')


insert into Launguage_Translation_Text(LanguageID,Text,TranslatedText) values(5,'Personal/Room/External VMR','Personnel/Salle/VMR externe')
insert into Launguage_Translation_Text(LanguageID,Text,TranslatedText) values(5,'Concierge Support','Assistance du concierge')
insert into Launguage_Translation_Text(LanguageID,Text,TranslatedText) values(5,'Internal Number','Numéro interne')
insert into Launguage_Translation_Text(LanguageID,Text,TranslatedText) values(5,'External Number','Numéro externe')
insert into Launguage_Translation_Text(LanguageID,Text,TranslatedText) values(5,'StartDate','DateDébut')
insert into Launguage_Translation_Text(LanguageID,Text,TranslatedText) values(5,'EndDate','DateFin')
insert into Launguage_Translation_Text(LanguageID,Text,TranslatedText) values(5,'BatchReportName','NomRapportParlot')
insert into Launguage_Translation_Text(LanguageID,Text,TranslatedText) values(5,'PC Details','Détails PC')
insert into Launguage_Translation_Text(LanguageID,Text,TranslatedText) values(5,'VMR Room Info','Infos de la salle du VMR')
insert into Launguage_Translation_Text(LanguageID,Text,TranslatedText) values(5,'Desktop Link','Lien du bureau')
insert into Launguage_Translation_Text(LanguageID,Text,TranslatedText) values(5,'The room does not have any Endpoint associated with it. An audio or video Conference cannot be created with this room.','La chambre n''a pas de point d''extrémité qui lui est associée. Une conférence audio ou vidéo ne peut pas être créé avec cette pièce.')
insert into Launguage_Translation_Text(LanguageID,Text,TranslatedText) values(5,'User Endpoint','Point terminal de l''utilisateur')
insert into Launguage_Translation_Text(LanguageID,Text,TranslatedText) values(5,'Guest Room','Salle des invités')
insert into Launguage_Translation_Text(LanguageID,Text,TranslatedText) values(5,'Guest Endpoint Address','Adresse des points terminaux des invités')
insert into Launguage_Translation_Text(LanguageID,Text,TranslatedText) values(5,'Edit Existing MCU','Modifier un MCU existant')
insert into Launguage_Translation_Text(LanguageID,Text,TranslatedText) values(5,'Meet Type','Type de réunion')
insert into Launguage_Translation_Text(LanguageID,Text,TranslatedText) values(5,'The server is not operational','Le serveur n''est pas opérationnel')
insert into Launguage_Translation_Text(LanguageID,Text,TranslatedText) values(5,'Thank you! Your response has been recorded','Merci ! Votre réponse est enregistrée')
insert into Launguage_Translation_Text(LanguageID,Text,TranslatedText) values(5,'Please select a valid H323 ID Address.','Veuillez sélectionner un identifiant d''adresse H323 valide.')

insert into Launguage_Translation_Text(LanguageID,Text,TranslatedText) values(5,'SetupTIme','Tempsconfiguration')
insert into Launguage_Translation_Text(LanguageID,Text,TranslatedText) values(5,'TearDownTime','Temps de démontage')
insert into Launguage_Translation_Text(LanguageID,Text,TranslatedText) values(5,'Max Allowed Standard MCU','Nombre max. de MCU standards autorisé')
insert into Launguage_Translation_Text(LanguageID,Text,TranslatedText) values(5,'Max Allowed Enhanced MCU','Nombre max. de MCU améliorés autorisé')
insert into Launguage_Translation_Text(LanguageID,Text,TranslatedText) values(5,'Maintanance','Entretien')
insert into Launguage_Translation_Text(LanguageID,Text,TranslatedText) values(5,'Please check the start and end time for the conference','Veuillez vérifier l''heure de début et de fin de la conférence')
insert into Launguage_Translation_Text(LanguageID,Text,TranslatedText) values(5,'The user is In-Active. Please make sure the user is Active.','L''utilisateur est en-actif. S''il vous plaît assurez l''utilisateur est active.')
insert into Launguage_Translation_Text(LanguageID,Text,TranslatedText) values(5,'Invalid Protocol, Connection Type and Address Type selected for user: ','Protocole, type de connexion et type d''adresse sélectionnés pour l''utilisateur invalides:')
insert into Launguage_Translation_Text(LanguageID,Text,TranslatedText) values(5,'Invalid Conference Code for user','Code de conférence pour l''utilisateur invalide')
insert into Launguage_Translation_Text(LanguageID,Text,TranslatedText) values(5,'Outside office hours.','En-dehors des heures de bureau.')
insert into Launguage_Translation_Text(LanguageID,Text,TranslatedText) values(5,'Room(s) not available.','Salle(s) non disponible(s).')
insert into Launguage_Translation_Text(LanguageID,Text,TranslatedText) values(5,'No of Occurrence Exceeds color dates','Le nombre d''évènements dépasse les dates en couleur')
insert into Launguage_Translation_Text(LanguageID,Text,TranslatedText) values(5,'Special reccurence failed : Building reccurence','Échec de la récurrence spéciale : Récurrence en construction')
insert into Launguage_Translation_Text(LanguageID,Text,TranslatedText) values(5,'Please select Dedicated VNOC Operator.','Veuillez sélectionner l''opérateur VNOC dédié.')
insert into Launguage_Translation_Text(LanguageID,Text,TranslatedText) values(5,'Status not available','État non disponible')
insert into Launguage_Translation_Text(LanguageID,Text,TranslatedText) values(5,'MSE 8000 MCU Configuration','Configuration du MCU MSE 8000')
insert into Launguage_Translation_Text(LanguageID,Text,TranslatedText) values(5,'Conference should have atleast two Endpoints','La conférence doit disposer d''au moins deux points terminaux')
insert into Launguage_Translation_Text(LanguageID,Text,TranslatedText) values(5,'At least one endpoint is required for a Remote meeting.','Au moins un point terminal est requis pour une réunion à distance.')
insert into Launguage_Translation_Text(LanguageID,Text,TranslatedText) values(5,'Operation UnSuccessful','Échec de l''opération')
insert into Launguage_Translation_Text(LanguageID,Text,TranslatedText) values(5,'Status Date','Date de l''état')
insert into Launguage_Translation_Text(LanguageID,Text,TranslatedText) values(5,'Status Message','Message de l''état')
insert into Launguage_Translation_Text(LanguageID,Text,TranslatedText) values(5,'In-Active','Non actif')
insert into Launguage_Translation_Text(LanguageID,Text,TranslatedText) values(5,'System Boot Time','Temps de démarrage du système')
insert into Launguage_Translation_Text(LanguageID,Text,TranslatedText) values(5,'Available Physical Memory','Mémoire physique disponible')
insert into Launguage_Translation_Text(LanguageID,Text,TranslatedText) values(5,'Virtual Memory','Mémoire virtuelle')
insert into Launguage_Translation_Text(LanguageID,Text,TranslatedText) values(5,'DHCP Enabled','DHCP activé')
insert into Launguage_Translation_Text(LanguageID,Text,TranslatedText) values(5,'Connection Name','Nom de la connexion')
insert into Launguage_Translation_Text(LanguageID,Text,TranslatedText) values(5,'Please enter the Whygo Integration Settings.','Veuillez saisir les paramètres d''intégration Whygo.')
insert into Launguage_Translation_Text(LanguageID,Text,TranslatedText) values(5,'Create New Catering Menus','Créer de nouveaux menus de service de restauration')
insert into Launguage_Translation_Text(LanguageID,Text,TranslatedText) values(5,'Create New Audiovisual Inventories','Créer de nouveaux inventaires audiovisuels')
insert into Launguage_Translation_Text(LanguageID,Text,TranslatedText) values(5,'Create New Facility Services','Créer de nouveaux services d''installation')
insert into Launguage_Translation_Text(LanguageID,Text,TranslatedText) values(5,'User Interface Banner &amp; Theme','Bannière et thème de l''interface utilisateur')
insert into Launguage_Translation_Text(LanguageID,Text,TranslatedText) values (5,'Participants','Participant')
insert into Launguage_Translation_Text(LanguageID,Text,TranslatedText) values(5,'Room(s)','Salle(s)')
insert into Launguage_Translation_Text(LanguageID,Text,TranslatedText) values(5,'Create New Hotdesking','Créer une nouvelle salle en libre-service')
insert into Launguage_Translation_Text(LanguageID,Text,TranslatedText) values(5,'Edit Hotdesking','Modifier le libre-service')
insert into Launguage_Translation_Text(LanguageID,Text,TranslatedText) values(5,'Concierge Conferences','Conférences de conciergerie')
insert into Launguage_Translation_Text(LanguageID,Text,TranslatedText) values(5,'File type is invalid. Please select a new file and try again.','Le type de fichier est invalide. Veuillez sélectionner un nouveau fichier et essayer de nouveau.')
insert into Launguage_Translation_Text(LanguageID,Text,TranslatedText) values(5,'MCU Connect / Disconnect','Connecter/Déconnecter le MCU')
insert into Launguage_Translation_Text(LanguageID,Text,TranslatedText) values(5,'MCU Connect','Connexion au MCU')
insert into Launguage_Translation_Text(LanguageID,Text,TranslatedText) values(5,'MCU Disconnect','Déconnexion du MCU')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'On-Site A/V Support','assistance A/V sur site')
insert into Launguage_Translation_Text(LanguageID,Text,TranslatedText) values(5,'Welcome Mail','E-mail de bienvenue')
insert into Launguage_Translation_Text(LanguageID,Text,TranslatedText) values(5,'Password Request Change','Password Change')
insert into Launguage_Translation_Text(LanguageID, Text, TranslatedText) values (5,'View','Voir')
insert into Launguage_Translation_Text(LanguageID, Text, TranslatedText) values (5,'Import','Importer')
insert into Launguage_Translation_Text(LanguageID, Text, TranslatedText) values (5,'Start Date/Time','Date/Heure de commencement')

insert into Launguage_Translation_Text(LanguageID, Text, TranslatedText) values (5,'SecurityDesk Only','Bureaudesécurité uniquement')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'(Custom)','(Coutume)')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'(System)','(Système)')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Rooms','salles')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Actions','actions')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Organization Administrator 1','Administrateur de l''organisation 1')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Organization Administrator 2','Administrateur de l''organisation 2')
insert into Launguage_Translation_Text(LanguageID, Text, TranslatedText) values (5,'Manage Batch Report','Gérer le rapport par lot')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Public Participant','Participant publique')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Network Switching','Commutation du réseau')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Conference Cancellation','Annulation de la conférence')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Conference Creation Failed','Échec de création de la conférence')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Batch Report','Rapport par lot')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Participant Multi Request Change','Changement des multiples demandes du participant')

insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Exportable Conference Report','Rapport de conférence exportable')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Contact List','Liste de contact')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'The selected role is currently assigned to a group template and cannot be edited or deleted.','Le rôle sélectionné est actuellement assigné à un modèle de groupe et ne peut être ni modifié ni supprimé.')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Call Monitoring','Surveillance des appels')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'RoomName','Nom de la salle')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Edit Conference','Modifier la conférence')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Conference Support','Assistance de la conférence')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'There are no Audiovisual work orders associated with this conference.','Aucun ordre de travail audiovisuel n''est associé à cette conférence.')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Delivery','Livraison')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Cost','Coût')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Service','Service')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Charge','Chargez')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Price','Prix')


insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Set-up (Minutes)','Configuration (Minutes)')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Tear Down (Minutes)','Démontage (Minutes)')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'To create a new catering work order, click on Add New Work Order button. To modify an existing work order, click the Edit OR Delete link associated with that work order.','Pour créer un nouvel ordre de travail de service restauration, cliquer sur le bouton Ajouter un nouvel ordre de travail. Pour modifier un ordre de travail existant, cliquez sur le lien Modifier OU le lien Supprimer associé à cet ordre de travail.')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Confirmation','Confirmation')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Number','Nombre')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Reservation','Réservation')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Operation unsuccessful.','Échec de l''opération')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Create New Day Color','Créer une nouvelle couleur de jour')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Invalid ISDN Address.','Adresse ISDN invalide.')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Invalid ISDN Address','Adresse ISDN invalide')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Mute','Enlever le son')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'No Events','Aucun évènement')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Please select at least one room from ''Select Rooms'' tab in order to create a work order.','Veuillez sélectionner au moins une salle à partir de l''onglet « Sélectionner des salles » avant de pouvoir créer un ordre de travail.')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Other','autres')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Advanced Reports','Rapports avancés')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'InternalNumber','Numéro interne')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'ExternalNumber','Numéro externe')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'No','Aucune')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Map','Carte')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Select one...','Sélectionnez un(e)...')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'No Sets','Aucun ensemble')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'No Rooms Selected','Aucune salle sélectionnée.')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,' rooms',' salles')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'No Items','Aucun élément')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'No Groups','Aucun groupe')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'External VMR','VMR externo')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Request Change','Demande de modification')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Lotus Plugin','Module Lotus')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Outlook Plugin','Module Outlook')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Services','Services')

insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Website','Site Internet')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'System Up Time','temps de démarrage du système')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Page File','Fichier de page')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'HotDesking Reservation','Réservation en libre-service')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Radio Button','Bouton radio')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'List Box','Zone de liste')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Drop-Down List','Liste déroulante')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'URL','URL')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Text-Multiline','Texte multiligne')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Check Box','Case à cocher')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Mute All','Couper le son pour tout')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Clone','Cloner')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Click to see more details','Cliquer pour afficher plus de détails')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Insufficient Seats. Please Contact Your VRM Administrator.','Places en nombre insuffisant. Veuillez contacter votre administrateur VRM.')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Available Seats','Places disponibles')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Image dimension too large','La taille de l''image est trop grande')

insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Sorry the required log file is not found','Désolé, le fichier journal requis est introuvable')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'EmailService is not installed','Le ServiceE-mail n''est pas installé.')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Service is not installed','Le service n''est pas installé')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'The number of minutes prior to the start of the conference that will be used to prepare the room for the meeting','Le nombre de minutes avant le début de la conférence qui sera utilisé pour préparer la salle de réunion')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'The number of minutes after the conference has ended to prepare the room for the next meeting','Le nombre de minutes après la fin de la conférence a pris fin utilisé pour préparer la salle pour la réunion suivante')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'The number of minutes that the MCU is to connect the endpoints, prior to the arrival of the participants into the conference','Le nombre de minutes pendant lequel le MCU doit connecter les points terminaux, avant l''arrivée des participants à la conférence')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Edit Day Color Details','Modifier les détails de la couleur du jour')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Please select at least one user to import','Veuillez sélectionner au moins un utilisateur à importer')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Please select a date','Veuillez sélectionner une date')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Please select a bridge with at least one associated MPI Service.','Veuillez sélectionner un pont avec au moins un service MPI associé.')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Invalid Account Expiration Date','Date d''expiration du compte invalide.')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Invalid MPI Address - Alphanumeric characters only','Adresse MPI invalide - Caractères alphanumériques uniquement')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Invalid conflict date','Date de conflit invalide')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'No data to print','Aucune donnée à imprimer')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Please enter a valid date time','Veuillez entrer une date/heure valide')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Special reccurence failed : Holidays not defined','Échec des récurrences spéciales : Vacances non définies')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Special reccurence failed : Reccurence string is not in right format','Échec des récurrences spéciales : La chaîne des récurrences n''est pas au bon format')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Please select the ticker background color','Veuillez choisir la couleur de fond du téléscripteur')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Please enter the RSS Feed link','Veuillez saisir le lien du flux RSS')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Both Tickers cannot have the same background color','Les deux téléscripteurs ne peuvent pas avoir la même couleur de fond')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Hotdesking','Libre-service')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Point-to-Point','Point à point')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Audio Only','Audio seulement')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Conference Timezone','Fuseau horaire de la conférence')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'The number of minutes prior to the start of the conference that will be used to prepare the room for the meeting.','Le nombre de minutes avant le début de la conférence qui sera utilisé pour préparer la salle de réunion.')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'The number of minutes after the conference has ended to prepare the room for the next meeting.','Le nombre de minutes après la fin de la conférence a pris fin utilisé pour préparer la salle pour la réunion suivante.')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'The number of minutes that the MCU is to connect the endpoints, prior to the arrival of the participants into the conference.','Le nombre de minutes pendant lequel le MCU doit connecter les points terminaux, avant l''arrivée des participants à la conférence.')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'The number of minutes that the MCU is to disconnect the endpoints, prior to the departure of the participants from the conference.','Le nombre de minutes pendant lequel le MCU doit déconnecter les points terminaux, avant le départ des participants de la conférence.')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'View User Details','Voir les détails de l''utilisateur')

insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'View Virtual Meeting Rooms','Voir réunions virtuelles Chambres')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'View Custom Option Details','Voir options personnalisées Détails')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'View Endpoint Details','Voir Endpoint Détails')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'View MCU Details','Afficher les détails du MCU')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'View Audio Bridge Details','Afficher les détails du pont audio')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'View User Template Details','Voir l''utilisateur Détails du modèle')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Video Conferencing Systems','Systèmes de vidéoconférence')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'VMR Attendee','VMR Participant')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Dial-in IP','Numérotation IP')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Meeting ID','Identifiant de réunion')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Phone Conferencing','Conférence téléphonique')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Dial-in Toll Number','Numéro d''appel payant')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Dial-in Toll Free Number','Dial-in Numéro sans frais')

insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'First time joining a Lync Video Meeting','Première participation à une réunion vidéo Lync')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'For detailed instructions','Pour des instructions détaillées :')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'First time joining a Blue Jeans Video Meeting','Première participation à une réunion vidéo Blue Jean ?')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'First time joining a Jabber Video Meeting','Première participation à une réunion vidéo Jabber ?')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Dial-in H.323','Numérotation H.323')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Dial-in SIP','Numérotation SIP')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Issued By','Publié par')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Issued On','Publié le')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Audiovisual Administrator','Administrateur de l''audiovisuel')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Facility Administrator','Administrateur de l''installation')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Audiovisual Admin','Administrateur de l''audiovisuel')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Facility Admin','Administrateur de l''installation')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Audiovisual Remainder','Reste de l''audiovisuel')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Facility Remainder','Installation restante')
--100288
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'hour','heure');
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'HotDesking Conference','Conférence en libre-service');
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Insufficient Audio/Video ports.','Ports audio/vidéo insuffisants.');
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'UnMute','Remettre le son');
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Search Result(s) for Audiovisual Work Orders','Rechercher le ou les résultats des ordres de travail audiovisuel');
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Search Result(s) for Facility Services Work Orders','Rechercher le ou les résultats des ordres de travail de services d''installation');
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Design','apparence')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Preview','aperçu')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Your conference has been submitted successfully and is currently in pending status awaiting administrative approval.','Félicitations ! Votre conférence a été soumise avec succès et est en attente d''acceptation d''une approbation administrative.')
insert into Launguage_Translation_Text(LanguageID,Text,TranslatedText) values(5,'Outside','Extérieur')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Mon','Lun')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Tue','Mar')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Wed','Cas')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Thu','Jue')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Fri','Vie')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Sat','Sáb')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Sun','Sol')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Existing','Existants')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'NA','N/A')
insert into Launguage_Translation_Text(LanguageID,Text,TranslatedText) values(5,'IP(Hours)	','IP(Heures)')

insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Facility','Installation')

insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Are you sure you want to delete this job name?','Êtes-vous sûr de vouloir supprimer ce nom de tâches ?')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Spanish','Espagnol')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Specific','Spécifiques')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'No Information','Aucune Information')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'The server is not operational.','Le serveur n''est pas opérationnel.')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Active Directory or LDAP Directory not Configured in Site Settings','Active Directory ou répertoire LDAP non configuré dans les Réglages du site')

insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'An operation error occurred.','Une erreur de fonctionnement est survenue.')

insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Text Box','Zone de texte')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'RadioButton List','Liste BoutonRadio')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'AudioVideo','AudioVídéo')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Entity_Code','Code_Entité')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'ISDN Dial-out (Hours)','Appels ISDN sortants (Heures)')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'ISDN Dial-out %','Appels ISDN sortants en %')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'ISDN Dial-in (Hours)','Appels ISDN entrants(Heures)')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'RoomOnly','Salle uniquement')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Confirmed','Confirmé')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'AudioOnly','Audio seulement')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'IP Address','Adresse IP')
                                
                         
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'ISDN Phone Number','Numéro de téléphone ISDN')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Invalid MCU Address Type, Connection Type or Protocol Selected for profile #:','Type d''adresse de MCU, type de connexion ou protocole sélectionné invalides pour le profil n° :')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Invalid Address Type, Connection Type or Protocol Selected for profile #:','Type d''adresse, type de connexion ou protocole sélectionné invalides pour le profil n° :')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Invalid MCU Address Type, Address Type or Protocol Selected for profile #:','Type d''adresse de MCU, type d''adresse ou protocole sélectionné invalides pour le profil n° :')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Invalid Address Types, Connection Type or MCU Address Type Selected for profile #:','Type d''adresse, type de connexion ou types d''adresse de MCU invalides pour le profil n° :')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'You cannot attend this conference as a room attendee since there are no rooms associated with this conference','Vous ne pouvez pas assister à cette conférence en tant que participant en salle car il n''y a pas de salles associées à cette conférence')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Conference Dial-in Number','Conférence numéro d''appel')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Point to Point Conference should not have Telepresence endpoint.. Telepresence Room Name:','La conférence point à point ne devrait pas avoir de point terminal de téléprésence. Nom de la salle de téléprésence:')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Authentication Failed','L''authentification a échoué')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'TestConnection failed','Tester la connexion a échoué.')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Date and Time','Date et heure')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'State','état')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'City','Ville')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Inbound','Inbound')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Outbound','Sortant')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Conf Start','Conf Démarrer')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Audio,Video','Audio, Vidéo')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Video','vidéo')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Audio Conferences','Conférences audio')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Room Conferences','Chambre Conférences')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Organization','Organisation')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Department Name','nom du département')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'CTS Numeric ID','Identifiant Numérique CTS')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Start Time','Heure de début')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Duration (Mins)','Durée (mins)')

insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Conf Speed','Vitesse de Conf')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Conf.Title','Conf Titre')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Conf.ID','Conf ID')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Date Of Conf.','Date de Conf')

insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Endpoints in Conf','Points terminaux dans la conf.')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Rooms in Conf','Salles dans la conf.')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'MCU Used','MCU Utilisé')

insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Host Role','Rôle de l''hôte')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Host First Name','Hôte Prénom')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Host Last Name','Hôte Nom')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Host Email','Hôte Email')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Conf Protocol','Protocole de conf.')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Occurrence','événements')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'WO Person-in-charge','Responsable WO')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Rm.Asst.in-Charge','Chambre adjoint Incharge')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Rm.Pri.Appr.Name','Nom de la cham.Appr.Pri')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Rm.Sec.Appr.Name 1','Nom de la cham.Appr.Sec 1')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Rm.Sec.Appr.Name 2','Nom de la cham.Appr.Sec 2')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'MCU.Pri.Appr.Name','Nom MCU Appr.Pri')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'MCU.Sec.Appr.Name 1','Nom MCU Appr.Sec 1')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'MCU.Sec.Appr.Name 2','Nom MCU Appr.Sec 2')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'On-Site A/V Support(Mins)','Assistance A/V sur site(min)')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Meet and Greet(Mins)','Accueil(min)')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Call Monitoring(Mins)','Surveillance des appels(min)')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Dedicated VNOC Operator(Mins)','Opérateur VNOC dédié(min)')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Secondary email','E-mail secondaire')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Role Name','Nom de rôle')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Account Expiration','Expiration du compte')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Exchange','Échange')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Domino','Domino')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'RoomPhone','Téléphone de la salle')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'capacity','capacité')

insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Address1','Adresse 1')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Address2','Adresse 2')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'RoomFloor','Plancher')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'RoomNumber','Nombre de chambre')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Country','pays')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Zipcode','Code postal')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Conf ID','Conf ID')
                                      
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Single/Recuring','Unique/Récurrente')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Default Profile Name','Nom de profil par défaut')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Number of Profiles','Nombre de profils')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Preferred Bandwidth','Bande passante privilégiée')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Pre.Dialing Option','Option de numérotation pré')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Default Protocol','Protocole par défaut')

insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Web Access URL','URL de l''accès Web')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Network Location','Emplacement du réseau')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Telnet Enabled','Telnet activé')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Email ID','Identifiant d''e-mail')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'API port','Port API')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'ICAL Invite','Invitation ICAL')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Tier 1','niveau 1')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Tier 2','niveau 2')

insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Endpoints in Conf > Name','Points terminaux dans la conf.>Nom')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Endpoints in Conf > Name - IP ','Points terminaux dans la conf.>Nom - IP ')

insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Endpoints in Conf > Name - IP  - Add ','Points terminaux dans la conf.>Nom - IP - Ajouter')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Endpoints in Conf > Name - IP  - Add  - Dial.Opt. ','Points terminaux dans la conf.>Nom - IP - Ajouter - option pour composer.')
                                                               
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'MCU Used in Conf','MCU Utilisé dans Conf')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'MCU Used in Conf > Address','MCU Utilisé dans Conf > Adresse')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Default MCU','Par défaut MCU')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'MCU Assignment','MCU Affectation')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'MCU Name','Nom du MCU')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Org Name','Nom org')

insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Next Poll in Mins','Sondage Suivant en mins')

insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'MCU Conf ID','MCU Conf ID')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Scheduled Start','Début prévu')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Scheduled End','Fin prévue')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Connect Time','Durée de connexion')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Disconnect Time','Durée de déconnexion')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Disconnect Reason','Cause de la déconnexion')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Connection Type','Type de connexion')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'CDR Scheduled Report','CDR rapport planifié')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Part. First Name','Part. Prénom')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Part. Last Name','Part. Nom')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Single','Unique')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Recurring','Récurrente')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Part. Email','Part. Email')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Part. Role','Part. Rôle')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Company Relationship','Société relation')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Custom Role','rôle personnalisé')

insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Deleted Successfully.','Supprimé avec succès.')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Pt-to-Pt','Point à point')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Dial-Out','Appel sortant')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Dial-In','Appel entrant')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Sec. Approver 1','Spprobateur Sec. 1')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Sec. Approver 2','Spprobateur Sec. 2')


insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Other...','Autre...')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Please Enter the Comments','Veuillez saisir les commentaires')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Protocol and Address type doesn''t match','Le protocole et le type d''adresse sont incompatibles')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'View Hotdesking Room Details','Afficher les détails de la salle en libre-service')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'View Entity Code Details','Afficher les détails du code de l''entité')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'View Color Day Details','Afficher les détails du jour en couleur')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'View Room Details','Afficher les détails de la salle')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'conference already deleted or expired','conférence déjà supprimée ou expirée')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'please associate at least one Catering Service with all Menus','Veuillez associer au moins un service de restauration avec tous les menus')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Characters Left','Caractères restants')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Continuous Presence','Présence continue')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'pending decision at SYSTEM level','en attendant la décision au niveau du système')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'pending decision at MCU level','décision en attente au niveau du MCU')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'pending decision at ROOM level','en attendant la décision au niveau de la salle')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'pending decision at Department level','en attendant la décision au niveau Département')

update Launguage_Translation_Text set TranslatedText='Mensuel' where LanguageID=5 and TEXT='Monthly'

insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,' Open 24 hours a day. ',' Ouvert 24 heures par jour. ')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,' Hours of operation: ',' Heures d''ouverture: ')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,' Closed: ',' Fermé: ')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,' To ',' à ')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Room Limit','Limite de chambre')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Enhanced MCU Limit','Limite MCU amélioré')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Max Domino User','Max Domino utilisateur')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'API Module','Module API')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Organization Limit','Limite organisation')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'End Point Limit','Fin Limite point')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Max Mobile User','Max Utilisateur mobile')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Max Video Rooms','Max vidéo Chambres')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Public Room Service','Service de salle publique')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'User Limit','Limite de l''utilisateur')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Max PC User','Max PC utilisateur')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Max VMR Rooms','Max VMR Chambres')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Catering Module','Module du service de restauration')

insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Max Guests Per User','Max personnes par utilisateur')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Max RO Hotdesking Rooms','Max RO HotDesking Chambres')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Facilities Module','module des installations')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Max VC Hotdesking Rooms','Max VC HotDesking Chambres')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'HouseKeeping Module','Module de ménage')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Max Exchange User','Max utilisateur Exchange')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Guest Room Limit','Chambre Limite')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Standard MCU Limit','Limite MCU standard')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Expiration Date','date d''expiration')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Max Non Video Rooms','Max Non Vidéo Chambres')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Adv Rep Mod','Mod. Av. de Rep.')


insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'To join or start the meeting,go to','Pour rejoindre ou commencer la réunion, rendez-vous à :')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Or join directly with the following options:','Ou participez directement avec les options suivantes :')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Test your video connection,talk to our video tester by clicking here','Testez votre connexion vidéo, parlez à notre testeur de vidéo en cliquant ici')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'(c) Blue Jeans Network 2012','(c) Bleu Jeans Réseau 2012')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'(c) Jabber Network 2012','(c) Jabber réseau 2012')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'(c) Lync Network 2012','(c) Lync réseau 2012')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'(c) Vidtel Network 2012','(c) Vidtel réseau 2012')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Please select a Facility Services now.','S''il vous plaît sélectionner un Facility Services maintenant.')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Edit Work Order','Modifier l''ordre de travail')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Switched Video','Vidéo commutée')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Transcoding','Transcodage')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Modify requested quantity and click EDIT to edit this work order.','Modifier la quantité demandée et cliquez sur Modifier pour modifier cet ordre de travail.')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'No details available','Pas de données disponibles')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Summary','Sommaire')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Phone','Téléphone')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Entity Name','Nom entité')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Need Entity Code','Besoin Code de l''entité')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Report Title','Titre du rapport')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Duration Total','Durée totale')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Point-to-Point Conferences','Conférences point-à-point')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Audio-Video Conferences','Conférences audio-vidéo')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Maximum limit is 24 hours including buffer period. Please enter a valid duration.','La limite maximale est de 24 heures, y compris la période de tampon. Veuillez saisir une durée valide.')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Invalid MPI Address - Alphanumeric characters only.','Invalide Adresse MPI - Seuls des caractères alphanumériques.')

insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Today','aujourd''hui')

insert into Launguage_Translation_Text(LanguageID,Text,TranslatedText) values(5,' does not have any Endpoint associated with it. An audio or video Conference cannot be created with this room.','aucun point terminal n''y est associé. Aucune conférence audio ou vidéo ne peut être créée pour cette salle.')




insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Total Audio/Video Seats','Total des places audio/vidéo')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Total Audio Only Seats','Total des places avec audio uniquement')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Invalid Time (HHmmZ)','Temps invalide (HHmmZ)')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Polycom CMA Configuration','Configuration du CMA Polycom')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Polycom RPRM Configuration','Configuration du RPRM Polycom')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Lifesize MCU Configuration','Configuration du MCU Lifesize')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'iView MCU Configuration','iView Configuration MCU')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Invalid Address Type selected for IP Service #','Le type d''adresse sélectionné est invalide pour le service IP n°')

insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,' min',' min')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,' ago ','ago')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Partially Connected','Partiellement connecté')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Status not available.','État non disponible.')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Please select a room to join this conference as a VMR attendee.','Veuillez sélectionner une salle pour rejoindre cette conférence au titre de participant VMR.')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Please select a room to join this conference as a room attendee.','Veuillez sélectionner une salle pour rejoindre cette conférence au titre de participant.')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'My Pending Audiovisual Work Orders','Mes ordres de travail audiovisuel en attente')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'My Pending Facility Services Work Orders','Mes ordres de travail de services d''installation en attente')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'My Incomplete Audiovisual Work Orders','Mes ordres de travail audiovisuel incomplets')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'My Incomplete Facility Services Work Orders','Mes ordres de travail de services d''installation incomplets')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Today''s Audiovisual Work Orders','Ordres de travail audiovisuel d''aujourd''hui ')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Today''s Catering Work Orders','Ordres de travail pour le service de restauration d''aujourd''hui')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Today''s Facility Services Work Orders','Ordres de travail des services d''installation d''aujourd''hui')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'E164/SIP Address','Adresse E164/SIP')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Click ''Cancel'' before you proceed to another step.','Cliquez sur Annuler avant de passer à l''étape suivante.')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Either you are not entitled to use the selected room or room no longer belongs to the conference.','Soit vous n''êtes pas autorisé à utiliser la salle sélectionnée ou celle-ci n''appartient plus à la conférence')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'This audiovisual inventories no longer belongs to selected room.','Ces inventaires audiovisuels n''appartiennent plus à la salle sélectionnée.')


insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Select a Set from the Audiovisual Inventories list.','Effectuer une sélection dans la liste des inventaires audiovisuels.')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Please select a Facility Services now.','Veuillez sélectionner un service d''installation maintenant.')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'There are no Facility Services available for this room. Please select another room.','Aucun service d''installation n''est disponible pour cette salle. Veuillez sélectionner une autre salle.')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Please check the start and end Date/time for this workorder.','S''il vous plaît vérifier le début et la fin Date / heure pour ce bon de travail.')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,' (Applicable for all instances)','(Valable pour toutes les instances)')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Duration: ','Durée:')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,' (Work orders deliver by date/time do not match with conference date/time.)','(La date/l''heure de livraison des ordres de travail ne correspond pas à la date/l''heure de la conférence.)')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Workorders can only be created with rooms selected for this conference. ','Les ordres de travail ne peuvent être créés qu''avec les salles sélectionnées pour cette conférence.')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Please check the start and end time for the conference.','Veuillez vérifier l''heure de début et de fin de la conférence')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Invalid Duration. Conference duration should be minimum of 15 mins.','Durée invalide. La durée de la conférence devrait être d''au minimum 15 min.')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Invalid Duration. Conference duration should be maximum of 24 hours.','Durée non valide. La durée de la conférence doit être au maximum de 24 heures.')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Invalid Setup Time','Heure de création invalide')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Invalid Teardown Time','Temps de démontage invalide')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Invalid Setup/Teardown Time','Temps de mise en place/démontage invalide')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'is In-Active. Please make sure the user is Active.','est inactif. Veuillez-vous assurer que l''utilisateur est actif.')

insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Invalid Leader Pin for user','Pin de Leader invalide pour l''utilisateur')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Invalid Network Protocol selected for user','Le protocole réseau sélectionné est invalide pour l''utilisateur+C3207:C3229')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Network access is restricted to IP only.','L''accès au réseau est limité aux adresses IP')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'There are no Audiovisual work orders associated with the selected Room','Aucun ordre de travail audiovisuel n''est associé à la salle sélectionnée')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'There are no Facility work orders associated with the Selected Room','Aucun ordre de travail d''installation n''est associé à la salle sélectionnée')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'There are no Facility work orders associated with this conference.','Aucun ordre de travail d''installation n''est associé à cette conférence.')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Room Name has already been used','Nom de la chambre a déjà été utilisée')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Video Conferencing Systems:','Sistemas de video-conferencias:')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'PIN','PIN')


insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Maximum limit is 4000 characters.','La limite maximale est de 4000 caractères')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Un Mute All','Remettre le son pour tout')

insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Audiovisual ','Audiovisuel ')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Room No longer belongs to this conference','La salle n ''appartient plus à cette conférence')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'There are no work orders currently associated with this conference.','Aucun ordre de travail n''est associé à cette conférence')

insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Please check the telepresence status of all profiles','Por favor, compruebe el estado de todos los perfiles de telepresencia')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Please associate at least one location with this Provider.','Veuillez associer au moins un emplacement à ce fournisseur.')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Please associate at least one Menu with this Provider.','Veuillez associer au moins un menu à ce fournisseur.')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Please associate at least one Catering Service with all Menus.','Veuillez associer au moins un service de restauration avec tous les menus.')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Please enter unique menu names for all menus.','Veuillez associer au moins un service de restauration avec tous les menus.')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Billing Code Name should be within 35 characters','Le nom du code de facturation ne doit pas dépasse les 35 caractères')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Task Type','Type de tâche')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Type ID','Saisir ID')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Operation failed: Invalid security permission. Please contact your VRM Administrator','L''opération a échoué: autorisation de sécurité valide. Se il vous plaît contactez votre administrateur VRM')

insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'automatically on daily basis.','automatiquement sur ​​une base quotidienne.')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'at','à')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'automatically.','automatiquement')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'every Month End at','chaque fin de mois à')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Hardware Interaction layer','Couche d''interaction matérielle')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Data Access Layer','Couche d''accès aux données')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Business Logic Layer','Couche métier logique')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Business Facade layer','Couche métier de Façade')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Front-End','Partie frontale')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Please select....','Veuillez sélectionner....')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Setup time cannot be less than the MCU pre start time.','Le temps d''installation ne peut être inférieur à celui du prédémarrage du MCU.')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Teardown time cannot be less than the MCU pre end time.','Le temps de démontage ne peut être inférieur à celui du pré-arrêt du MCU.')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Ishost','Ishost')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Are you sure you want to delete this user ?','Êtes-vous sûr de vouloir supprimer ce rôle utilisateur ?')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'PC Attendee','Participant PC')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Collapse','Réduire')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Expand','Développer')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Custom option name should be within 35 characters','Personnalisé nom de l''option doit être à moins de 35 caractères')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Option name already exists in the list for Language ','Le nom de l''option existe déjà dans la liste')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Option Name already exists in the list for Language','Le nom de l''option existe déjà dans la liste')


insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Are you sure you want to delete this Custom Option ?','Êtes-vous sûr de vouloir supprimer cette Option Habituelle?')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Both Tickers cannot have the same background color.','Les deux Tickers ne peut pas avoir la même couleur de fond.')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Please enter the RSS Feed link.','Veuillez saisir le lien du flux RSS.')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Please select the ticker background color.','Veuillez choisir la couleur de fond du téléscripteur.')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'No Tiers found.','Pas de Tiers trouvé.')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Are you sure you want to delete this middle tier ?','Êtes-vous sûr de vouloir supprimer ce niveau intermédiaire?')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Login Background Image attachment is greater than 100kB. File has not been uploaded.','La pièce jointe de l''image de fond de l''identifiant dépasse 100 ko. Le fichier n''a pas été mis en ligne.')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Site Logo attachment is greater than 100kB. File has not been uploaded.','La taille de la pièce jointe du logo du site est supérieure à 100 Mo. Le fichier n''a pas été mis en ligne.')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Please enter a valid date time.','Veuillez entrer une date/heure valide.')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'TearDown Dur','Démontage Dur')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Setup Dur','Configuration Dur')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,' to Week','à la semaine')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,' Conferences Usage Report',' Rapport sur l''usage des conférences')

insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Invalid IP Address','Adresse IP invalide')

insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Invalid selection for connection type and protocol. MCU is mandatory for direct calls with MPI protocol.','Sélection invalide pour le type de connexion et de protocole. Le MCU est obligatoire pour les appels directs avec le protocole MPI.')


update Launguage_Translation_Text set TranslatedText ='Fin de la conférence' where Text ='Conference End' and LanguageID=5

insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Conference Host','Hôte de la conférence')


insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'<error><errorCode>264</errorCode><message>Your VRM account is about to expire on %s. Please contact your VRM Administrator for further assistance Your account expires in {0} days.</message><Description>VRM Account Expiration warning</Description><level>M         </level></error>','<error><errorCode>264</errorCode><message>Votre compte VRM arrivera à expiration le %s. Veuillez contacter votre administrateur VRM local pour recevoir plus d''assistance. Votre compte VRM arrivera à expiration dans {0} jours.</message><Description>Avertissement d''expiration de compte VRM</Description><level>M         </level></error>')

-------------------------------- ZD 100622 (23 Dec 2013) starts -------------------------------------------------
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Click to see Map','Cliquer pour afficher la carte')

-------------------------------- ZD 100622 (23 Dec 2013) ends-------------------------------------------------
--ZD 100507 (06 Jan 2013)
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Room Layout','Agencement de la salle')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Room Layout Image','Disposition de la salle image')

-------------------------------- ZD 100777 (29 Jan 2014) starts -------------------------------------------------


insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'External Scheduling Number','Externe Nombre de planification')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Expiry Date','Date d''expiration')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Expiry Remaining Days','Expiration jours restants')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Organization Name','Nom de l''organisation')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Alernate Location','Alernate Lieu')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Alernate MCU','Alernate MCU')

insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'MCU Fail Status Alert','Alerte de l''état Échec du MCU')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'MCU Pass Status Alert','Alerte de l''état Succès du MCU ')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Event Edit Notification','Notification de modification d''un évènement')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Event Delete Notification','Notification de suppression d''évènements')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Site Expiration','Expiration du site')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Client Site Expiration','Expiration du site du client ')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Organization Expiry','Expiration de l''organisation ')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Room Alternate Host Mail','Messagerie alternative de l''hôte de la salle')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Room Alternate Schedular Mail','Messagerie alternative du planificateur de la salle ')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'MCU Alternate Host Mail','Messagerie alternative de l''hôte du MCU')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'MCU Alternate Schedular Mail','Messagerie alternative du planificateur du MCU')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Deny Room Host Mail','Interdire la messagerie de l''hôte de la salle ')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Deny Room Schedular Mail','Interdire la messagerie du planificateur de la salle')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Conf.Type','Type de Conf')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,' min(s)',' minute(s)')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Address Book','Carnet d''adresses')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'New','Nouvelle')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'VMR Password','VMR Mot de passe')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Company Logo Banner exceeds the Resolution Limit','La résolution de la bannière du logo de la société dépasse la limite')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Company logo attachment is greater than 100KB. File has not been uploaded','La pièce jointe du logo de la société dépasse 100 ko. Le fichier n''a pas été mis en ligne')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Banner attachment is greater than 500KB. File has not been uploaded.','Fixation de la bannière est supérieure à 500 Ko. Fichier n''a pas été téléchargé')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Banner attachment for high resolution is greater than 500KB. File has not been uploaded.','Fixation de la bannière pour une grande résolution est supérieure à 500 Ko. Fichier n''a pas été téléchargé.')

-------------------------------- ZD 100777 (29 Jan 2014) ends-------------------------------------------------

-------------------------------- ZD 100788 (4 Feb 2013) starts -------------------------------------------------
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Telepresence','Téléprésence ')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Hotdesking Audio','Audio du libre-service')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Hotdesking Video','Vidéo du libre-service')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Media Type is None','Type de support est Aucun')

-------------------------------- ZD 100788 (4 Feb 2014) ends-------------------------------------------------

-------------------------------- ZD 100909 (6 Feb 2014) starts -------------------------------------------------

update Launguage_Translation_Text set TranslatedText='Administrar Departamentos' where Text = 'Manage Departments' and languageid= 5
update Launguage_Translation_Text set TranslatedText='Administrar Invitados' where Text = 'Manage Guests' and languageid= 5
update Launguage_Translation_Text set TranslatedText='Administrar Usuarios Inactivos' where Text = 'Manage Inactive Users' and languageid= 5
update Launguage_Translation_Text set TranslatedText='Administrar MCUs' where Text = 'Manage MCUs' and languageid= 5
update Launguage_Translation_Text set TranslatedText='Administrar Niveles' where Text = 'Manage Tiers' and languageid= 5
update Launguage_Translation_Text set TranslatedText='Administrar Usuarios' where Text = 'Manage Users' and languageid= 5

insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Search','Rechercher')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'FAIL','ÉCHOUER')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'PASS','PASSE')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Host URL','URL de l''hôte')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Participant URL','Participant URL')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'WebEx Password','WebEx Mot de passe')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Failure Message','message d''échec')

-------------------------------- ZD 100909 (6 Feb 2014) ends-------------------------------------------------
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Max Allowed  Desktop Video ','Maximum autorisé Desktop Video')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Max Virtual Meeting Room ','Max virtuelle Salle de réunion')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Virtual Meeting Room','Salle de réunion virtuelle')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Max Desktop Video User','Max Desktop Video utilisateur')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Virtual Meeting Room Conference' ,'Conférence de la salle de réunion virtuelle')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Virtual Meeting Room Attendee','Participant de la salle de réunion virtuelle');
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Please select a room to join this conference as a Virtual Meeting Room attendee.','Veuillez sélectionner une salle pour rejoindre cette conférence au titre de participant de salle de réunion virtuelle.');

insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Event Edit failure Notification','[Notification de l''échec de la modification d''un évènement]')

--ZD 100825
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values 
(5,'Not Monitored','Non surveillé')

-- ZD 100909 Starts --
update Launguage_Translation_Text set TranslatedText='Gérer les départements' where Text = 'Manage Departments' and languageid= 5
update Launguage_Translation_Text set TranslatedText='Gérer les invités' where Text = 'Manage Guests' and languageid= 5
update Launguage_Translation_Text set TranslatedText='Gérer les utilisateurs inactifs' where Text = 'Manage Inactive Users' and languageid= 5
update Launguage_Translation_Text set TranslatedText='Gérer les MCU' where Text = 'Manage MCUs' and languageid= 5
update Launguage_Translation_Text set TranslatedText='Gérer les niveaux' where Text = 'Manage Tiers' and languageid= 5
update Launguage_Translation_Text set TranslatedText='Gérer les utilisateurs' where Text = 'Manage Users' and languageid= 5
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Deny MCU Host Mail','Refuser MCU hôte courrier')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Deny MCU Scheduler Mail','Refuser MCU Scheduler mail')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Started','Commencé')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Home','Accueil')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'User interface banner and Theme','Interface utilisateur bannière et le thème')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Virtual Meeting Room Link','Salle de réunion virtuelle Lien')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Note','Remarque')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'User doesn''t have first name','L''utilisateur n''a pas le prénom')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'User doesn''t have Last name','L''utilisateur n''a pas Nom')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'User doesn''t have Email Id','L''utilisateur n''a pas Email Id')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'User doesn''t have Password','L''utilisateur n''a pas le mot de passe')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'User doesn''t have Timezone','L''utilisateur n''a pas Fuseau horaire')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'User doesn''t have valid Email Id','L''utilisateur n''est pas valide Email Id')

insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Room doesn''t have the name','Chambre n''a pas le nom')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Room doesn''t have the Tier 1','Chambre n''a pas le Niveau 1')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Room doesn''t have the Tier 2','Chambre doesn avoir le niveau 2')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Given Tier 1 doesn''t exists.','Niveau 1 Compte tenu de ne pas existe.')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Given Tier 2 doesn''t exists.','Niveau 2 Compte tenu de ne pas existe.')


insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Endpoint doesn''t have name','Endpoint ne porte pas de nom')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Endpoint doesn''t have address','Endpoint n''ont pas d''adresse')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Endpoint doesn''t have address type','Endpoint n''a pas le type d''adresse')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Endpoint name exceeds 20 character','Nom Endpoint dépasse 20 caractères')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'MCU doesn''t have name','MCU ne porte pas de nom')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'MCU doesn''t have IP Address','MCU n''a pas d''adresses IP')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'For New Entry :  Leave the first column (''ID'') value as blank (ie., no need to enter any value for the ''ID'' field).'
,'Pour une nouvelle entrée: Laissez la première colonne («ID») comme valeur vide (ie, pas besoin d''entrer une valeur pour le champ ''ID''.).')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5
,'For Modifying Existing Record : Please never modify /remove the value in the first column (''ID'').'
,'Pour Modification enregistrement existant: S''il vous plaît jamais modifier / supprimer la valeur dans la première colonne («ID»).')

insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Room Data','Les données sur les chambres')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'MCU Data','MCU données')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Endpoint Data','Endpoint données')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'User Data','Les données de l''utilisateur')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'User Role (User/Admin/Super Admin etc)','Rôle de l''utilisateur (User / Administration / Super Administrateur etc)')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Assigned MCU (only in case of multiple)','MCU affecté (seulement en cas de multiple)')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Notes','Remarques')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Vendor Type','Type de fournisseur')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Control Port IP Address','Adresse IP du port de contrôle')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Tier One','Niveau un')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Tier Two','Niveau Deux')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Floor','Étage')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Room Phone','Téléphone de la salle')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Room Administrator','Administrateur de chambre')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Media (None Audio-only Audio-Video)','Médias (Aucun Audio uniquement Audio-Vidéo)')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Department','département')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Preferred Dialing Option','Option de numérotation préférée')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Located Outside the Network','Situé à l''extérieur du réseau')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Data Import','Outil d''importation de données')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Initial Password','Mot de passe initial')


insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Desktop Video Details','Bureau Détails de la vidéo')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Virtual Meeting Room Password','Mot de passe de la salle de réunion virtuelle')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'External Virtual Meeting Room','Salle de réunion virtuelle externe')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Virtual Meeting Room Info','Salle de réunion virtuelle Infos')

insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Total Duration(Hrs)','Durée totale (H)')
insert into Launguage_Translation_Text(LanguageID,Text,TranslatedText) values(5,'hrs','h')
insert into Launguage_Translation_Text(LanguageID,Text,TranslatedText) values(5,'mins','min')
insert into Launguage_Translation_Text(LanguageID,Text,TranslatedText) values(5,'Outside office hrs.','En dehors de heures de bureau.')
insert into Launguage_Translation_Text(LanguageID,Text,TranslatedText) values(5,'IP(hrs)','IP (heures)')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'ISDN Dial-out (Hrs)','Appels ISDN sortants (H)')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'ISDN Dial-in (Hrs)','Appels ISDN entrants(H)')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,' Open 24 hrs a day. ',' Ouvert 24 heures par jour. ')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,' Hrs of operation: ',' Durée (en h) des opérations :  ')

insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Maximum limit is 24 hrs including buffer period. Please enter a valid duration.','La limite maximum est 24 h y comprise la période tampon. Veuillez saisir une durée valide.')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Invalid Duration. Conference duration should be maximum of 24 hrs.','Durée valide. Durée de la Conférence doit être au maximum de 24 heures.')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'VRM system is unavailable during the chosen date and time. Please check VRM system availability and try again. Hrs of operation:','Système VRM n''est pas disponible lors de la date et l''heure choisie. S''il vous plaît vérifier VRM disponibilité du système et essayer à nouveau. Heures de fonctionnement:' )

--ZD 100959
Insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Conference Dial-in Information','Détails du numéro de téléphone de la conférence')

/* **********************************ZD 100781 Starts 26 Feb 2014************************** */
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'You will receive the password shortly. Administrator(s) are notified about your request.','Vous recevrez le mot de passe dans de brefs délais. Le ou les administrateurs sont au courant de votre demande.')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'User password has been changed and email notification has been sent to user.','Le mot de passe de l''utilisateur a été changé et la notification par e-mail a été envoyée à l''utilisateur.')

insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Request Password','Demande de mot de passe')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Password Change Admin Notification','Notification de l''admin sur le changement du mot de passe')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'New Password Notification ','Notification de nouveau mot de passe')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'New Password Admin Notification','Nueva notificación contraseña de administrador')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Password Change Request','Contraseña Solicitud de Cambio')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'<error><errorCode>724</errorCode><message>Your VRM account password is about to expire on %s. Please contact your VRM Administrator for further assistance.</message><Description>Your VRM account password is about to expire on %s. Please contact your VRM Administrator for further assistance Your account password expires in {0} days.</Description><level>E         </level></error>'
,'<error><errorCode>724</errorCode><message>Le mot de passe de votre compte VRM arrivera à expiration le %s. Veuillez contacter votre Administrateur VRM pour plus d''assistance.</message><Description>Votre mot de passe VRM est sur ​​le point d''expirer sur% s. S''il vous plaît contactez votre administrateur VRM pour obtenir de l''aide Votre mot de passe expire dans {0} jours.</Description><level>E         </level></error>')


/* **********************************ZD 100781 Ends 26 Feb 2014************************** */


/* **********************************ZD 100963 START 28 Feb 2014************************** */
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Reserved','Réservé')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Host','Hôte')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Work','Travail')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Cell','cellule')
/* **********************************ZD 100963 END 28 FEB 2014************************** */



insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Apr','Avril')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Aug','Août')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Dec','Décembre')

--ZD 100036
Insert into Launguage_Translation_Text (LanguageID, [Text], TranslatedText) values ( 5, 'On MCU Conferences', 'Sur les conférences du MCU')


/* **********************************ZD 100973 START 05 MAR 2014************************** */

insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Google Authentication Failed','L''authentification Google a échoué')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Invalid user Authentication','Authentification de l''utilisateur invalide')

/* **********************************ZD 100973 END 05 MAR 2014************************** */


/* **********************************ZD 101026 Starts 13 MAR 2014************************** */

Insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Changes Made','Changement effectués')

Insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'No Changes Made','Aucun changement effectué')

Insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'New Profile Created','Nouveau profil créé')

Insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Created','Créé')

/* **********************************ZD 101026 End 13 MAR 2014************************** */

/*********************************** ZD 100619  Starts 10 Apr 2014 **************************/
Insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Dial-in to Conference','Composer le numéro de la conférence')

Insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Dial-out to Location','Appel sortant vers l''emplacement')

/*********************************** ZD 100619  Ends 10 Apr 2014 **************************/

--ZD 101304
Insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'An e-mail has been sent to the address entered with instructions to reset your password','Un e-mail a été envoyé à l''adresse indiquée avec les instructions pour réinitialiser votre mot de passe.')

--ZD 101322
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'External Participant(s) Info','Info sur le(s) participant(s) externe(s)')

--ZD 101344


Insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Resource br Availability','Disponibilité br des ressources')

Insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Workorders br Calendar','Workorders br Calendrier')

Insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Search br Workorders','Recherche br WorkOrders')

Insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Basic br Details','Détails br de base')
Insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Select br Rooms','Sélectionnez br Chambres')
Insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Select br Audiovisual','Sélectionnez br l''audiovisuel')
Insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Select br Catering','Sélectionnez br Restauration')
Insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Select br Facility','Sélectionner br l''établissement')
Insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Additional br Options','Options br Supplémentaires')
Insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Review amp;br Submit','Revoir amp; br Soumettre')
Insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Workorders br Details','Workorders br Détails')

insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'CISCO TMS Configuration','Configuration du TMS CISCO')

insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'MGC Accord MCU Configuration','MGC Accord Configuration MCU')

Insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'No Rooms','Aucune salle')

--ZD 101176
Delete from Launguage_Translation_Text where [Text]='entity code' and LanguageID =5
Delete from Launguage_Translation_Text where [Text]='special instructions' and LanguageID =5


insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Entity Code','Code de l''entité')

insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Special Instructions','instructions spéciales')


insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Assigned to MCU','Attribué au MCU')

insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'The caller endpoint must be a certified endpoint.','Le point terminal de l''appelant doit être un point terminal certifié.')

insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'The caller endpoint must use an IP address as the address type.','Le point terminal de l''appelant doit utiliser une adresse IP comme type d''adresse.')

insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'The caller endpoint cannot be a Guest Attendee.','Le point terminal de l''appelant ne peut pas être un participant invité.')

insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Only an IP address is permitted for this profile type.','Seule une adresse IP est permise pour ce type de profil.')

insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'A Default Profile should be selected.','Un profil par défaut devrait être sélectionné.')

insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Profile type cannot be the same.','Le type de profil ne peut pas être le même.')


Insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Max iControl Room','Nombre max. de salle iControl')

-- ZD 101344
Insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Minimum Length is','La longueur minimale est.')

Insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'First digit should be non-zero.',' Le premier chiffre devrait être différent de 0.')

Insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Excel is not Uploaded','Excel n''est pas mis en ligne')

Insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Middle Tier(s) are imported successfully!','Le ou les niveaux intermédiaires ont été importés avec succès !')

Insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Department(s) are imported successfully!','Le ou les départements ont été importés avec succès !')

Insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Room(s) are imported successfully!','Le ou les salles ont été importées avec succès !')

Insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Failed to create Room''s are listed below','Impossible de créer la chambre de sont énumérés ci-dessous')

Insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'User(s) are imported successfully!','Le ou les utilisateurs ont été importés avec succès !')

Insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Failed to create User''s are listed below','Impossible de créer l''utilisateur de sont énumérés ci-dessous')

Insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'MCU(s) are imported successfully!','Le ou les MCU ont été importés avec succès !')

Insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Failed to create/update MCU''s are listed below','Impossible de créer / mettre à jour MCU y sont énumérés ci-dessous')

Insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'EndPoint(s) are imported successfully!','Le ou les points terminaux ont été importés avec succès !')

Insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Failed to create/update Endpoint''s are listed below','Impossible de créer / mettre à jour Endpoint y sont énumérés ci-dessous')

Insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Conference(s) are imported successfully!','Le ou les conférences ont été importées avec succès !')

Insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Purge Completed','Purge terminée')

Insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'(Fly Room)','(Salle volante)')

Insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Set-up (mins)','Configuration (min)')

Insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Tear Down (mins)','Démontage (min)')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'A Default Profile should be selected','Un profil par défaut devrait être sélectionné')

insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Please check the profile type','Veuillez vérifier le type de profil')
Insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Login Background Image exceeds the Resolution Limit','La taille de l''image de fond de l''identifiant dépasse la limite de résolution')

Insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Total Audio/Video Conferences','Total Audio / Vidéo Conférences')

Insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Percentage of Total Audio/Video Conferences','Pourcentage du total des audio conférences / vidéo')
Insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'OBTP Conference','Conférence OBTP')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Security Email','Sécurité Email')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Acceptance Security Email','Acceptation de sécurité e-mail')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Invited Partcipant','Invité Partcipant')

insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Active directory enabled','Active Directory activé')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Whygo Portal Error','Portail Whygo erreur')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Connection Detail','Détails de la connexion')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Party Address','Parti d''adresses')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'VMR Room Link','VMR Chambre lien')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Bridge Address','pont Adresse')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Protocol Type','Type de protocole')
insert into Launguage_Translation_Text(LanguageID,Text,TranslatedText) values (5,'Participant(s)','Participant (s)')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Room image width and height should not exceed 250 pixels.','Chambre largeur de l''image et la hauteur ne doit pas dépasser 250 pixels')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Conferences must be scheduled at least 20 minutes from the current time. Use the ''Start Now'' option or ''Instant Conference'' to schedule a meeting immediately'
,'Les conférences doivent être organisées au moins 20 minutes à l''heure actuelle. Utilisez l''option "Démarrer maintenant" ou "Conférence instantanée» pour planifier immédiatement une réunion')

--ZD 101730
Insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'No file selected','Aucun fichier sélectionné')


--ZD 101808

Insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Notification on Pending Rooms/Endpoints','Notification relative attente chambres / Endpoints')


Insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Please select a record for import.','S''il vous plaît sélectionner un dossier pour l''importation.')

Insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'No RPRM MCUs to Poll Rooms/Endpoints. Error Code : 100','Pas RPRM MCU au vote chambres / Endpoints. Code d''erreur: 100')

Insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'No RPRM MCUs to Poll Rooms/Endpoints.','Pas RPRM MCU au vote chambres / Endpoints.')

insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'No records found','Aucun enregistrement trouvé')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Invalid Participant code for user','Code de participant valide pour l''utilisateur')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Manage My Lobby','Gérer de Ma salle d''accueil')

--ZD 101803
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Default Family','Par défaut famille')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Family 1','Famille 1')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Family 2','Famille 2')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Family 3','Famille 3')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Family 4','Famille 4')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Family 5','Famille 5')

Insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'VMR Type','Type VMR')

Insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Note: The selected video display layout is not supported by the MCU in conference.','Remarque: La mise en page de l''affichage vidéo sélectionné n''est pas supporté par le MCU à la conférence.')

--ZD 102032
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Pexip MCU Configuration','Pexip Configuration MCU')

Insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Active Directory or LDAP Directory not configured in Organization Settings','Active Directory ou LDAP n''est pas configuré dans les paramètres de l''Organisation')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'# Confs','N° de Conf.')
insert into Launguage_Translation_Text(LanguageID,Text,TranslatedText) values(5,'Account Settings','Paramètres du compte')
insert into Launguage_Translation_Text(LanguageID,Text,TranslatedText) values(5,'Floor Plans','Plans d''étage')
insert into Launguage_Translation_Text(LanguageID,Text,TranslatedText) values(5,'Max WebEx User','Max WebEx utilisateur')
insert into Launguage_Translation_Text(LanguageID,Text,TranslatedText) values(5,'Profile Type','Type de profil')
insert into Launguage_Translation_Text(LanguageID,Text,TranslatedText) values(5,'French','Français')
insert into Launguage_Translation_Text(LanguageID,Text,TranslatedText) values(5,'Please select a record for reassigning.','S''il vous plaît sélectionner un dossier de réaffectation.')
insert into Launguage_Translation_Text(LanguageID,Text,TranslatedText) values(5,'Administrator','Administrateur')

insert into Launguage_Translation_Text(LanguageID,Text,TranslatedText) values(5,'conference(s) in the past','conférence (s) dans le passé')
insert into Launguage_Translation_Text(LanguageID,Text,TranslatedText) values(5,'conference in the past','conférence dans le passé')
insert into Launguage_Translation_Text(LanguageID,Text,TranslatedText) values(5,'conference(s) in the future','conférence (s) à l''avenir')
insert into Launguage_Translation_Text(LanguageID,Text,TranslatedText) values(5,'conference in the future','conférence à l''avenir')
insert into Launguage_Translation_Text(LanguageID,Text,TranslatedText) values(5,'are linked to the selected custom option and data will be lost','sont liés à l''option de personnalisation sélectionné et les données seront perdues')
insert into Launguage_Translation_Text(LanguageID,Text,TranslatedText) values(5,'End Date/Time','Date/Heure de fin')
insert into Launguage_Translation_Text(LanguageID,Text,TranslatedText) values(5,'Static','Statique')
insert into Launguage_Translation_Text(LanguageID,Text,TranslatedText) values(5,'Dynamic','Dynamique')
insert into Launguage_Translation_Text(LanguageID,Text,TranslatedText) values(5,'SSHS Connection failed for Endpoint','SSHS connexion a échoué pour point final')
insert into Launguage_Translation_Text(LanguageID,Text,TranslatedText) values(5,'Approver','Approbateur')
insert into Launguage_Translation_Text(LanguageID,Text,TranslatedText) values(5,'Delivery By Date/Time','Livraison par date/heure')
insert into Launguage_Translation_Text(LanguageID,Text,TranslatedText) values(5,'To create a new work order, click on Add New Work Order button. To modify an existing work order, click the Edit OR Delete link associated with that work order.','Pour créer un nouvel ordre de travail, cliquez sur le bouton Ajouter un nouvel ordre de travail. Pour modifier un ordre de travail existant, cliquez sur le lien Modifier OU le lien Supprimer associé à cet ordre de travail.');

insert into Launguage_Translation_Text(LanguageID,Text,TranslatedText) values(5,'Connection successful. However, no matching ldap records were found.','Connexion établie. Cependant, aucun enregistrement Idap correspondant n''a été trouvé.')
insert into Launguage_Translation_Text(LanguageID,Text,TranslatedText) values(5,'Connection successful. However, multiple matching ldap records were found.','Connexion établie. Cependant, plusieurs enregistrements Idap correspondants ont été trouvés.')
--ZD 102054
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Advanced Form','Formulaire avancée')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'You have chosen to attend the conference','Vous avez choisi de participer à la conférence')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'from the conference room :','à partir de la salle de conférence:')

--ZD 102085
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Conference failure Notification','Conférence défaut de notification')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Error Code','Code d''erreur')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Error Message','Message d''erreur')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'New/Edit Conference','Nouveau / Modifier Conférence')


--ZD 102333
insert into Launguage_Translation_Text ( LanguageID, Text, TranslatedText) values (5,'To add a new work order, click the Add New Audiovisual Work Order button.','Pour ajouter un nouvel ordre de travail, cliquez sur le bouton Nouveau audiovisuel Work Order Ajouter.')
insert into Launguage_Translation_Text ( LanguageID, Text, TranslatedText) values (5,'To add a new work order, click the Add New Facility Work Order button.','Pour ajouter un nouvel ordre de travail, cliquez sur le bouton Nouveau Fonds travail de commande Ajouter.')
insert into Launguage_Translation_Text ( LanguageID, Text, TranslatedText) values (5,'To edit an existing work order, click the Edit button.','Pour modifier un ordre de travail existant, cliquez sur le bouton Modifier.')
insert into Launguage_Translation_Text ( LanguageID, Text, TranslatedText) values (5,'To add a new work order, click the Add New Catering Work Order button.','Pour ajouter un nouvel ordre de travail, cliquez sur le bouton Nouveau Fonds travail de commande Ajouter.')

--ZD 102263
Insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Invalid login background image dimension.','Connexion fond non valide dimension de l''image.')
--ZD 102331
insert into Launguage_Translation_Text ( LanguageID, Text, TranslatedText) values (5,'Please specify the Requested Quantity of each item and select either Update to calculate the total price of this work order or select Create to add the work order to the conference.','Se il vous plaît préciser la quantité demandée de chaque élément et sélectionnez Mise à jour pour calculer le prix total de cette commande de travail ou sélectionnez Créer pour ajouter l''ordre de travail à la conférence.')

--ZD 102432
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'A system error has occurred. Please contact your myVRM system administrator and give them the following error code.','Une erreur système se est produite. Se il vous plaît contactez votre administrateur système de myVRM et de leur donner le code d''erreur suivant.')
--ZD 102332
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Click Create button to save these changes.','Cliquez sur le bouton Créer pour enregistrer ces changements.')
Insert into Launguage_Translation_Text ( LanguageID, Text, TranslatedText) values (5,'Click Edit button to save these changes.','Cliquez sur le bouton Modifier pour enregistrer ces changements.')

--ZD 102337
insert into Launguage_Translation_Text(LanguageID,Text,TranslatedText) values(5,'To create a new catering work order, click on the Add New Catering Work Order button. To modify an existing work order, click the Edit button. To delete an existing work order, click the Delete button.','Pour créer un nouvel ordre de travail de restauration, cliquez sur le bouton Ajouter un nouveau traiteur ordre de travail. Pour modifier un ordre de travail existant, cliquez sur le bouton Modifier. Pour supprimer un ordre de travail existant, cliquez sur le bouton Supprimer.')


--ZD 102334

insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Are you sure you want to delete this work order?','Êtes-vous sûr de vouloir supprimer cet ordre de travail?')
--French
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'To create a new facility work order, click on the Add New Facility Work Order button. To modify an existing work order, click the Edit button. To delete an existing work order, click the Delete button.'
,'Pour créer un nouvel ordre de travail de l''installation, cliquez sur le bouton Ajouter Facilité d''ordre de travail. Pour modifier un ordre de travail existant, cliquez sur le bouton Modifier. Pour supprimer un ordre de travail existant, cliquez sur le bouton Supprimer.')

--ZD 102499

Insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Work Orders br Details','Ordres de travail br Détails')
Insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Search br Work Orders','Commandes Recherche br de travail')
Insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Work Orders br Calendar','Ordres de travail br Calendrier')

insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Conference Dial-in Information(Customized)','Détails du numéro de téléphone de la conférence (Personnalisé)')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Report type','Type de rapport')

-- ZD 102727
update Launguage_Translation_Text set TranslatedText='conférences' where LanguageID=5 and TEXT='Conferences'
update Launguage_Translation_Text set TranslatedText='automatiquement sur une base quotidienne.' where LanguageID=5 and TEXT='automatically on daily basis.'
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'every friday at','chaque vendredi à')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'at the end of every month at','à la fin de chaque mois à')

--ZD 102723

insert into Launguage_Translation_Text(LanguageID,Text,TranslatedText) values(5,'Internal Number Label','Numéro interne étiquette')
insert into Launguage_Translation_Text(LanguageID,Text,TranslatedText) values(5,'External Number Label','Numéro externe etiqueta')

-- ZD 102473
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'IBM Notes Forms','IBM Notes de formes')

--ZD 102647
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Custom Options Report','Rapport sur les options personnalisé')

--ZD 102710

Update Launguage_Translation_Text Set Text='Please check the start and end Date/time for this work order.',
 TranslatedText='S''il vous plaît vérifier le début et la fin Date / heure pour ce bon de travail.' 
Where Text='Please check the start and end Date/time for this workorder.' and LanguageID=5

Update Launguage_Translation_Text Set Text='work orders can only be created with rooms selected for this conference.',
 TranslatedText='Les ordres de travail ne peuvent être créés qu''avec les salles sélectionnées pour cette conférence.' 
Where Text='Workorders can only be created with rooms selected for this conference.' and LanguageID=5

Update Launguage_Translation_Text Set Text='Please save or cancel current work order prior to editing or deleting existing work orders.',
 TranslatedText='Veuillez enregistrer ou annuler l''ordre de travail actuel avant de modifier ou d''effacer les ordres de travail existants.' 
Where Text='Please save or cancel current workorder prior to editing or deleting existing workorders.' and LanguageID=5

Update Launguage_Translation_Text Set Text='Please save/update or cancel current work order prior to editing or deleting existing work orders.',
 TranslatedText='Aucun ordres de travail n''est associé à cette Veuillez enregistrer/mettre à jour ou annuler l''ordre de travail actuel avant de modifier ou d''effacer les ordres de travail existants.' 
Where Text='Please save/update or cancel current workorder prior to editing or deleting existing workorders.' and LanguageID=5

Update Launguage_Translation_Text Set Text='Please specify Quantity and either click update to calculate total price for this work order or click create to add work order.',
 TranslatedText='Veuillez spécifier la quantité puis cliquer sur Mise à jour pour calculer le prix total pour cet ordre de travail ou cliquez sur Créer pour ajouter un ordre de travail.' 
Where Text='Please specify Quantity and either click update to calculate total price for this work order or click create to add workorder.' and LanguageID=5

Update Launguage_Translation_Text Set Text='There are no work orders associated with this conference',
 TranslatedText='Aucun ordre de travail n''est associé à cette conférence' 
Where Text='There are no Workorders associated with this conference' and LanguageID=5

Update Launguage_Translation_Text Set Text='Please check Start and Completed Date/Time for this work order.',
 TranslatedText='Veuillez vérifier la date/l''heure de début et d''achèvement pour cet ordre de travail.' 
Where Text='Please check Start and Completed Date/Time for this workorder.' and LanguageID=5

Update Launguage_Translation_Text Set Text='Please check the start and end time for this work order.',
 TranslatedText='Veuillez vérifier la date/l''heure de début et de fin pour cet ordre de travail.' 
Where Text='Please check the start and end time for this workorder.' and LanguageID=5

Update Launguage_Translation_Text Set Text='Are you sure you want to delete this work order?',
 TranslatedText='Êtes-vous sûr de vouloir supprimer cet ordre de travail ?' 
Where Text='Are you sure you want to delete this Workorder?' and LanguageID=5

Update Launguage_Translation_Text Set Text='A/V work orders',
 TranslatedText='Ordres de travail A/V' 
Where Text='A/V Workorders' and LanguageID=5

Update Launguage_Translation_Text Set Text='There are no Work orders associated with this conference',
 TranslatedText='Aucun ordres de travail n''est associé à cette conférence' 
Where Text='There are no Workorders associated with this conference' and LanguageID=5

Update Launguage_Translation_Text Set Text='View Catering work order',
 TranslatedText='Voir l''ordre de travail du service de restauration' 
Where Text='View Catering Workorder' and LanguageID=5

Update Launguage_Translation_Text Set Text='View Housekeeping work orders',
 TranslatedText='Voir les ordres de travail de ménage' 
Where Text='View Housekeeping workorders' and LanguageID=5

Update Launguage_Translation_Text Set Text='work orders can only be created with rooms selected for this conference.',
 TranslatedText='Les ordres de travail ne peuvent être créés qu''avec les salles sélectionnées pour cette conférence.' 
Where Text='Workorders can only be created with rooms selected for this conference.' and LanguageID=5

Update Launguage_Translation_Text Set Text='Click Update to re-calculate total OR click Edit to update this work order.',
 TranslatedText='Cliquez sur Mise à jour pour recalculer le total OU cliquez sur Modifier pour mettre à jour cet Ordre de travail.' 
Where Text='Click Update to re-calculate total OR click Edit to update this Workorder.' and LanguageID=5

Update Launguage_Translation_Text Set Text='View Inventory work orders',
 TranslatedText='Voir les ordres de travail de l''inventaire' 
Where Text='View Inventory workorders' and LanguageID=5

Update Launguage_Translation_Text Set Text='Work order Comment',
 TranslatedText='Commentaire sur l''ordre de travail' 
Where Text='Workorder Comment' and LanguageID=5

Update Launguage_Translation_Text Set Text='Work orders br Calendar',
 TranslatedText='Work orders br Calendrier' 
Where Text='Workorders br Calendar' and LanguageID=5

Update Launguage_Translation_Text Set Text='Search br work orders',
 TranslatedText='Recherche brwork orders' 
Where Text='Search br Workorders' and LanguageID=5

Update Launguage_Translation_Text Set Text='work orders br Details',
 TranslatedText='work orders br Détails' 
Where Text='Workorders br Details' and LanguageID=5

--ZD 102706

insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Username','Nom d''utilisateur')

--ZD 102943
Delete from Launguage_Translation_Text where LanguageID = 5 and Text='New Password Admin Notification'
Delete from Launguage_Translation_Text where LanguageID = 5 and Text='Password Change Request'

insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'New Password Admin Notification','Notification de l''admin sur le nouveau mot de passe') 
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Password Change Request','Demande de changement de mot de passe')

insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Conference #','Conférence #')
insert into Launguage_Translation_Text(LanguageID,Text,TranslatedText) values(5,'Requester','Demandeur') 

-- ZD 103095 - HD
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Please select a valid Image Format like JPG GIF or PNG','Se il vous plaît sélectionner un format d''image valide comme JPG GIF ou PNG')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Your conference has been submitted successfully and is currently in wait list status.','Votre conférence vous avez repéré un succès et est actuellement en état de la liste d''attente.')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Wait List Conference Notification','Attendez notification Conférence Liste')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Wait List Conferences','Liste d''attente Conférences')

-- ZD 102835
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) 
values (5,'WO Admin-in-charge','WO Admin-responsable')

insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) 
values (5,'WO Type','WO Tapez')

insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) 
values (5,'Room Host','Salle Hôte')

insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) 
values (5,'Inventory Item','Item d''inventaire')

insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) 
values (5,'Quantity','Quantité')

insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) 
values (5,'WO Name','WO Nom')

insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) 
values (5,'Work Orders Report','Ordres de travail Rapport')

--ZD 103181

insert into Launguage_Translation_Text(LanguageID,Text,TranslatedText) values(5,'No items found','Aucun article trouvé') 
insert into Launguage_Translation_Text(LanguageID,Text,TranslatedText) values(5,'Mentioned entity code could not be linked with conference as it does not match.','Code de l''entité mentionnée ne pouvait pas être lié à la conférence car elle ne correspond pas.') 
insert into Launguage_Translation_Text(LanguageID,Text,TranslatedText) values(5,'Please enter specific entity code.','Se il vous plaît entrer entité spécifique de code.') 
insert into Launguage_Translation_Text(LanguageID,Text,TranslatedText) values(5,'Please enter the valid entity code as it does not match.','Se il vous plaît entrez le code de l''entité valide car il ne correspond pas.') 

--ZD 103174

Update Launguage_Translation_Text set TranslatedText ='Cellule' where TranslatedText='cellule'
Update Launguage_Translation_Text set TranslatedText ='Instructions spéciales' where TranslatedText='instructions spéciales'
Update Launguage_Translation_Text set TranslatedText ='Sélectionner Salles' where TranslatedText='Sélectionnez br Chambres'
Update Launguage_Translation_Text set TranslatedText ='Nouvelle conférence de base' where TranslatedText='Nouvelle conférence urgente'
Update Launguage_Translation_Text set TranslatedText ='Gérer ma salle d’accueil' where TranslatedText='Gérer de Ma salle d''accueil'


--ZD 103268 starts
update Launguage_Translation_Text set text = 'HK Reminder' , TranslatedText = 'HK Rappel'
where text = 'HK Remainder' and LanguageID = 5


update Launguage_Translation_Text set text = 'Inventory Reminder' , TranslatedText = 'Inventaire Rappel'
where text = 'Inventory Remainder' and LanguageID = 5


update Launguage_Translation_Text set text = 'Participant Reminder' , TranslatedText = 'participant Rappel'
where text = 'Participant Remainder' and LanguageID = 5


update Launguage_Translation_Text set text = 'Audiovisual Reminder' , TranslatedText = 'Rappel de l''audiovisuel'
where text = 'Audiovisual Remainder' and LanguageID = 5



update Launguage_Translation_Text set text = 'Facility Reminder' , TranslatedText = 'Facilité Rappel'
where text = 'Facility Remainder' and LanguageID = 5


update Launguage_Translation_Text set text = 'Catering Reminder' , TranslatedText = 'Restauration Rappel'
where text = 'Catering Remainder' and LanguageID = 5

--ZD 103268 End
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Please select at least one room to the conference.','Se il vous plaît sélectionner au moins une pièce à la conférence.' )

--ZD 103263

insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Please enter Blue Jeans password','Se il vous plaît entrez le mot de passe Blue Jeans')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Please enter Virtual Meeting Room password','S''il vous plaît entrez le mot de passe Virtual Meeting Room')
Insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'BlueJeans Meeting ID','BlueJeans Réunion ID')
Insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'BlueJeans Meeting Passcode','BlueJeans Réunion Passcode')
Insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'BlueJeans Meeting URL','BlueJeans URL de la réunion')

--ZD 103471
update Launguage_Translation_Text set TranslatedText = 'Formulaire avancé' where TranslatedText = 'Formulaire avancée'

--103493

insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Personnel Alert','Alerte personnel')

--French
Insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'An e-mail has been sent to the address entered with instructions to reset your password.','Un courriel contenant des instructions pour réinitialiser votre mot de passe a été envoyé à l’adresse indiquée.')
Insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Please provide the information below and select a room to attend this conference.','Veuillez fournir l''information ci-dessous et choisir une salle pour participer à cette conférence.')
Insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Please provide the information below to attend this conference.','Veuillez fournir les informations ci-dessous pour participer à cette conférence.')
Insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Your session expired. Please sign in to continue...','Votre session a expiré. Se il vous plaît vous connecter pour continuer')
update Launguage_Translation_Text set  TranslatedText = 'Vous avez quitté myVRM.' where Text = 'You have successfully logged out of myVRM.' and LanguageID = 5

--ZD 103692

insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) 
values (5, 'The value in the # of Attendees field must be greater than zero per room', 'La valeur dans le champ Participants de # doit être supérieur à zéro par chambre')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) 
values (5, 'A room MUST be selected as the host location of this conference', 'Une salle doit être sélectionné comme l''emplacement de l''hôte de cette conférence')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5, 'Total Travel Avoided', 'Voyage total a été évité')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5, 'Room Name', 'Vnom de la salle')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5, '# of Attendees', 'Nombre de participants')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5, 'Room Name', 'Vnom de la salle')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5, 'Rooms In Conf.', 'Salles dans la conf.')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5, 'Host Room', 'Vnom de la salle')

--French
update Launguage_Translation_Text set TranslatedText = 'Nbre de participants'
where LanguageID = 5 and Text = '# of Attendees'

update Launguage_Translation_Text set TranslatedText = 'Total des voyages évités'
where LanguageID = 5 and Text = 'Total Travel Avoided'

update Launguage_Translation_Text set TranslatedText = 'Une salle DOIT être sélectionnée comme lieu d''accueil pour cette conférence'
where LanguageID = 5 and Text = 'A room MUST be selected as the host location of this conference'

update Launguage_Translation_Text set TranslatedText = 'La valeur dans le champ du Nbre de participants doit être plus grande que zéro'
where LanguageID = 5 and Text = 'The value in the # of Attendees field must be greater than zero per room'

insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) 
values (5, 'Rooms In Conf - # of Attendees', 'Salles dans la conf - Nbre de participants')


--ZD 102700

insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5, 'Map 1 attachment width and height should not exceed 250 pixels.', 'Carte 1 largeur de fixation et la hauteur ne doit pas dépasser 250 pixels.')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5, 'Map 2 attachment width and height should not exceed 250 pixels.', 'Carte 2 largeur de fixation et la hauteur ne doit pas dépasser 250 pixels.')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5, 'Misc 1 attachment width and height should not exceed 250 pixels.', 'Divers 1 largeur de fixation et la hauteur ne doit pas dépasser 250 pixels.')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5, 'Misc 2 attachment width and height should not exceed 250 pixels.', 'Divers 2 largeur de fixation et la hauteur ne doit pas dépasser 250 pixels.')

insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Requester Phone','Téléphone du demandeur')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Requester Cell','Cellule du demandeur')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Requester Email','E-mail du demandeur')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Numeric values only','Valeurs numériques uniquement' )
Update Launguage_Translation_Text set Text='Unblock' Where Text='UnBlock'

--ZD 104116
Insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Blue Jeans License does not permit this operation','La licence Blue Jeans ne permet pas cette opération')
Insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Limited to Active Users','Limité aux utilisateurs actifs')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Max Blue Jeans User','Max Utilisateurs Blue Jeans')


--French 104265
Insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Invalid Start Date or Time. It should be greater than current time in host timezone.','La date ou l’heure de début est invalide. Elle doit être postérieure à l’heure courante dans le fuseau horaire de l’hôte.')
Insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Invalid Start Date or Time. It should be greater than current time in selected conference timezone.','La date ou l’heure de début est invalide. Elle doit être postérieure à l’heure courante dans le fuseau horaire de la conférence choisie.')


--French
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText,TextType) 
values (5,'Create MCU Load Balance Group','Créer le groupe d’équilibrage de charge de MCU',1)

insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText,TextType) 
values (5,'Edit MCU Load Balance Group','Modifier le groupe d’équilibrage de charge de MCU',1)

insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText,TextType) 
values (5,'Please select the lead MCU.','S''il vous plaît sélectionner le MCU plomb.',1)


update Launguage_Translation_Text set Text = 'Are you sure you want to delete this Group ?' where
LanguageID = 5 and Text ='Are you sure you want to delete this Group?'

Insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText, TextType) values (5,'BlueJeans Meeting ID(Customized)','BlueJeans Réunion ID(Personnalisé)',1)
Insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText, TextType) values (5,'BlueJeans Meeting Passcode(Customized)','BlueJeans Réunion Passcode(Personnalisé)',1)

insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText,TextType) 
values (5,'Conference Room Removal Notification','Notification du retrait de la salle de conférence',1)
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText,TextType) values (5,'Please enter a valid email address','Veuillez entrer une addrese email vlaide',1)



--ZD 102131 104474
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText,TextType) 
values (5,'Date/Time Signed-In','Date/heure de la connexion',1)

insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText,TextType) 
values (5,'Attended Conference','Conférence jointe',1)

--ZD 104169
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (5,'Conference Time Zone','Fuseau horaire de la conférence')

--ZD 104163

insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText,TextType) values (5,'Invalid Time (HH:MM)','Heure invalide (HH:MM)', 1)

insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText,TextType) values (5,'Invalid Date','Date invalide', 1)

update Launguage_Translation_Text set TranslatedText ='Heure invalide (HH:MM)', TextType = 1 where Text ='Invalid Time (HH:mm)' and languageid = 5

--ZD 104542

insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText, TextType) values (5,'The myVRM system is unavailable during the date and time selected. Please check the myVRM system availability and try again. Hours of operation: ','Ce système myVRM est indisponible à la date et à l’heure sélectionnées. Veuillez vérifier la disponibilité du système myVRM et réessayer. Heures de fonctionnement: ', 1)

insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText, TextType) values (5,' Error Code : ',' Code d’erreur : ', 1)



-- French

insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText,TextType) 
values (5,'Maximum Capacity between 0 and 10000','Capacité maximale comprise entre 0 et 10000.',1)

insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText,TextType) 
values (5,'Image missing for','Image manquante pour',1)

insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText,TextType) 
values (5,'List of rooms are imported without images.','La liste des salles est importée sans images.',1)

--ZD 104697

insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText,TextType) 
values (5,'Room Participants Label','Étiquette des participants de la salle',1)
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText,TextType) 
values (5,'Room Participants(Customized)','Participants de la salle (personnalisé)',1)
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText,TextType) 
values (5,'Invitees List Label','Étiquette de la liste des invités',1)
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText,TextType) 
values (5,'Invitees List(Customized)','Liste des invités (personnalisé)',1)

insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText,TextType) 
values (5,'Conference Approver','Conférence approbateur',1)


--French
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText,TextType) values (5,'Audio Dial In #(Customized)','N° de l''appel audio(personnalisé)',1)
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText,TextType) values (5,'Conference Code(Customized)','Code de la conférence(personnalisé)',1)
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText,TextType) values (5,'Participant Code(Customized)','Code Participant(personnalisé)',1)
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText,TextType) values (5,'Leader Pin(Customized','Pin du leader(personnalisé)',1)
Insert into Launguage_Translation_Text ( LanguageID, Text, TranslatedText,TextType) values (5,'System Location','Emplacement du système',1)
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText,TextType) 
values (5,'If users are removed from spreadsheet and the spreadsheet uploaded, those users will be blocked.','Les utilisateurs supprimés de la feuille de calcul téléversée seront bloqués.',1)


-- ALLDEV-543
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText,TextType) values (5,'AD/LDAP Login contain amp; lt; and gt; invalid characters.','L’identifiant AD/LDAP contient les caractères invalides amp;, lt; et gt;.',1)

insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText,TextType) values (5,'AD/LDAP Domain contain amp; lt; and  gt;  invalid characters."','Le domaine AD/LDAP contient les caractères invalides amp;, lt; et gt;.',1)

insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText,TextType) values (5,'AD/LDAP Login','Inicio de sesión AD/LDAP',1)
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText,TextType) values (5,'AD/LDAP Domain','Domaine AD/LDAP',1)
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText,TextType) values (5,'System Location','Emplacement du système',1)
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText,TextType) values (5,'Participant Assignment','Affectation du participant',1)


--French

insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText,TextType) 
values (5,'Hotdesking Rooms','Salles en libre-service',1)

insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText,TextType) 
values (5,'Users','Utilisateurs',1)

insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText,TextType) 
values (5,'Room Image','Image de la salle',1)

insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText,TextType) 
values (5,'State/Province','État/Province',1)

insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText,TextType) 
values (5,'Map 1','Mapa 1',1)

insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText,TextType) 
values (5,'Map 2','Mapa 2',1)

insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText,TextType) 
values (5,'Postal Code','Code postal',1)

insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText,TextType) 
values (5,'Misc. 1','Divers 1',1)

insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText,TextType) 
values (5,'Misc. 2','Divers 2',1)

insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText,TextType) 
values (5,'iControl Room','iControl Chambre',1)

insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText,TextType) 
values (5,'Street Address 1','Nom de la rue  1',1)

insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText,TextType) 
values (5,'Street Address 2','Nom de la rue  2',1)

insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText,TextType) 
values (5,'Room Queue','File d''attente des salle',1)

insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText,TextType) 
values (5,'Room #','Salle #',1)



-- ALLDEV- 644 start
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText,TextType) 
values (5,'Conf. Type','Type de Conf',1)

insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText ,TextType) values 
(5,'No Room','Aucune salle', 1)

insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText,TextType) 
values (5,'Setup time cannot be less than the MCU pre-start time.',N'Le temps d''installation ne peut être inférieur à celui du prédémarrage du MCU.',1)


insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText,TextType) 
values (5,'Teardown time cannot be less than the MCU pre-end time.','Le temps de démontage ne peut être inférieur à celui du pré-arrêt du MCU.',1)


-- ALLDEV- 644 End

--ALLDEV-524
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText,TextType) 
values (5,'Printer Instructions','Instructions pour les imprimantes',1)

insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText,TextType) 
values (5,'Printer Instructions attachment is greater than 5MB. File has not been uploaded.','La pièce jointe contenant les instructions pour les imprimantes fait plus de 5 Mo. Le fichier n’a pas été téléversé.',1)


--ALLDEV-826

insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText,TextType) values (5,'Numeric Password','Mot de passe numérique',1)
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText,TextType) values (5,'Guest Password','Mot de passe « Invité »',1)
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText,TextType) values (5,'Please check the guest password. It must be unique','Veuillez vérifier le mot de passe invité, qui doit être unique',1)
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText,TextType) values (5,'Please check the host password. It must be unique','Veuillez vérifier le mot de passe hôte, qui doit être unique',1)



insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText,TextType) values (5,'Conference administrator cannot be deleted.','L’administrateur de conférence ne peut pas être supprimé.',1)
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText,TextType) values (5,'Conference administrator cannot be blocked.','L’administrateur de conférence ne peut pas être bloqué.',1)
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText,TextType) values (5,'Please set Conference Administrator before deleting the user.','Veuillez définir l’Administrateur de conférence avant de supprimer l’utilisateur.',1)
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText,TextType) values (5,'Others','Autres',1)
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText,TextType) values (5,'Room Details','Détails de la salle',1)
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText,TextType) values (5,'Group Details','Détails Groupe',1)
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText,TextType) values (5,'Template Details','Détails modèles',1)
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText,TextType) values (5,'Work Order Details','Ordres de travail Détails',1)
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText,TextType) values (5,'Audiovisual','Audiovisuel',1)
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText,TextType) values (5,'Group Name','Nom de groupe',1)
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText,TextType) values (5,'Template Name','Nom du modèle',1)
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText,TextType) values (5,'Inventory Type','Tipo de inventario',1)
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText,TextType) values (5,'Manage My Hotdesk(s)','Gérer mon/mes Hotdesk(s)',1)
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText,TextType) values (5,'My Hotdesk(s)','Mon/Mes Hotdesk(s)',1)
--ALLDEV-854
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText,TextType) values (5,'Alias cannot conatin {5} placeholder alone.',
'Le pseudonyme ne peut pas contenir que {5} caractères.',1)
--ALLBUGS-204
update Launguage_Translation_Text set TranslatedText = 'La pièce jointe excède les 10 MB. Le fichier n''a pas été mis en ligne.'
where LanguageID = 5 and TranslatedText = 'La pièce jointe excède les 10 Mo. Le fichier n''a pas été mis en ligne.'